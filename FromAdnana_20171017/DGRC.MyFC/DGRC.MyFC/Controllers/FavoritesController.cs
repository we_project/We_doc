﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DGRC.MyFC.Repository;

namespace DGRC.MyFC.Controllers
{
    public class FavoritesController : ApiController
    {
        [HttpGet]
        [Route("api/favorites")]
        public List<AppFavorite> Get()
        {
            FinancialManagmenetContext context = new FinancialManagmenetContext();
            return context.AppFavorites
                .OrderBy(f => f.User)
                .ThenBy(f => f.Sort)
                .ThenBy(f => f.Title).ToList();
        }


        [HttpGet]
        [Route("api/favorites/{userName}")]
        public List<AppFavorite> Get(string userName)
        {
            FinancialManagmenetContext context = new FinancialManagmenetContext();
            return context.AppFavorites
                .Where(f => f.User.Equals(userName, StringComparison.InvariantCultureIgnoreCase))
                .OrderBy(f => f.Sort)
                .ThenBy(f => f.Title).ToList();
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
