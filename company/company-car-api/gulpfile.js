const gulp = require("gulp");
const sourcemaps = require('gulp-sourcemaps');
const ts = require("gulp-typescript");
const mocha = require("gulp-mocha");
const istanbul = require('gulp-istanbul');
const remapIstanbul = require('remap-istanbul/lib/gulpRemapIstanbul');
const del = require('del');
const runSequence = require("run-sequence");
const _ = require('lodash');
const exit = require('gulp-exit');

gulp.task("clean", function () {
  return del(_.concat('dist', 'coverage'));
});

const tsProject = ts.createProject('./tsconfig.json', { module: "commonjs", typescript: require("typescript") });
gulp.task('build', function () {
  return gulp.src(['src/**/*.ts', 'test/**/*.ts', 'test/**/*.json'], { base: '.' })
    .pipe(sourcemaps.init()) //get ready for some maps...
    .pipe(tsProject())
    .on("error", function (err) {
      process.exit(1);
    })
    .js.pipe(sourcemaps.write()) //BAM
    .pipe(gulp.dest(tsProject.config.compilerOptions.outDir));
});

gulp.task('watch', function() {
  gulp.watch('src/**/*.ts', ['build']);
});


gulp.task('test:instrument', function () {
  return gulp.src('dist/src/**/*.js', { base: '.' })
    .pipe(istanbul())
    .pipe(sourcemaps.write("."))
    .pipe(istanbul.hookRequire()); //this forces any call to 'require' to return our instrumented files
});

gulp.task('test', ['test:instrument'], function () {
  return gulp.src([
    './dist/test/**/*.spec.js'
  ]) //take our transpiled test source
    .pipe(mocha({ ui: "bdd", timeout: 120000 }))//runs tests
    .on("error", function (err) {
      console.log('err is ', err);
      process.exit(1);
    })
    .pipe(istanbul.writeReports({
      reporters: ['json'] //this yields a basic non-sourcemapped coverage.json file
    }))
    .on('end', remapCoverageFiles) //perform a remap
    .pipe(istanbul.enforceThresholds({ thresholds: { global: 10 } }))
    .pipe(exit());
});

//using remap-istanbul we can point our coverage data back to the original ts files
function remapCoverageFiles () {
  return gulp.src('./coverage/coverage-final.json')
    .pipe(remapIstanbul({
      basePath: '.',
      reports: {
        'html': './coverage',
        'text-summary': null,
        'lcovonly': './coverage/lcov.info'
      }
    }));
}

gulp.task("default", function (cb) {
  runSequence(
    "clean",
    "build",
    "test",
    cb);
});