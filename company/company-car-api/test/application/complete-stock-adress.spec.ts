import { resetDB } from '../db.spec';
import { registerVehicleStock } from '../../src/application/register-vehicle-stock-service';
import { importBaseData } from '../fixture/import-base-data';
import { completeTheStockAddress } from '../../src/application/complete-stock-address';
import { VehicleStock } from '../../src/models/VehicleStock';
import { stockExampleMaker } from '../fixture/stock-example';
import * as should from 'should';
import * as nock from 'nock';
import { UserMappingModel } from '../../src/models/UserMapping';
import { VehicleModel } from '../../src/models/Vehicle';
import { mockBaiduGeoApi } from '../extern-service/baidu-map-geo-locate-api-stub';
import { createMockCarOwnerEmployee } from '../fixture/create-system-admin';
import { VehicleStockDetail } from '../../src/application/dto/VehicleStockDetail';

describe('fulfill stock address by location', () => {
    afterEach(async () => {
        await resetDB();
        nock.cleanAll();
        nock.enableNetConnect();
    });

    let currentUserMapping: UserMappingModel;
    let userVehicle: VehicleModel;
    let stockExampleData: VehicleStockDetail;

    beforeEach(async () => {
        const {userMapping, vehicle} = await importBaseData();
        stockExampleData = stockExampleMaker();
        currentUserMapping = userMapping;
        userVehicle = vehicle;
        nock.disableNetConnect();
    });

    it('should fulfill the address by the correct china location', async () => {
        const userVehicleStock = await registerVehicleStock(userVehicle.id, stockExampleData, await createMockCarOwnerEmployee(userVehicle));

        const latitude: number = 39.940193;
        const longitude: number = 116.432458;
        const province = '北京市';
        const city = '北京市';
        const district = '东城区';
        const addressDetail = '北京市东城区东直门外大街3号';

        mockBaiduGeoApi(province, city, district, addressDetail, latitude, longitude);

        await completeTheStockAddress();

        const latestStock = await VehicleStock.findById(userVehicleStock.id);

        should(latestStock.locationPosition.province).be.equal(province);
        should(latestStock.locationPosition.city).be.equal(city);
        should(latestStock.locationPosition.district).be.equal(district);
        should(latestStock.locationPosition.detail).be.equal(addressDetail);
    });

    it('should fulfill the address by the wrong china location', async () => {

        const latitude: number = undefined;
        const longitude: number = undefined;
        const province = '';
        const city = '';
        const district = '';
        const addressDetail = '';

        mockBaiduGeoApi(province, city, district, addressDetail, latitude, longitude);


        stockExampleData.location = {latitude: latitude, longitude: longitude};

        const wrongVehicleStock = await registerVehicleStock(userVehicle.id, stockExampleData, await createMockCarOwnerEmployee(userVehicle));
        await completeTheStockAddress();

        const latestStock = await VehicleStock.findById(wrongVehicleStock.id);

        should(latestStock.locationPosition.province).be.equal(province);
        should(latestStock.locationPosition.city).be.equal(city);
        should(latestStock.locationPosition.district).be.equal(district);
        should(latestStock.locationPosition.detail).be.equal(addressDetail);
    });

    it('should fulfill the address by the correct location but not in china', async () => {
        const province = 'Auckland';
        const city = 'Auckland';
        const district = '';
        const longitude = 174.719075;
        const latitude: number = -36.865972;
        const addressDetail = '';
        mockBaiduGeoApi(province, city, district, addressDetail, latitude, longitude);

        stockExampleData.location = {latitude: latitude, longitude: longitude};
        const wrongVehicleStock = await registerVehicleStock(userVehicle.id, stockExampleData, await createMockCarOwnerEmployee(userVehicle));
        await completeTheStockAddress();

        const latestStock = await VehicleStock.findById(wrongVehicleStock.id);

        should(latestStock.locationPosition.province).be.equal(province);
        should(latestStock.locationPosition.city).be.equal(city);
        should(latestStock.locationPosition.district).be.equal(district);
    });
});
