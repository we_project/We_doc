import { resetDB } from '../db.spec';
import { UserMappingModel } from '../../src/models/UserMapping';
import { VehicleModel, Vehicle } from '../../src/models/Vehicle';
import { importBaseData } from '../fixture/import-base-data';
import { registerVehicleStock } from '../../src/application/register-vehicle-stock-service';
import { stockExampleMaker } from '../fixture/stock-example';
import { stockJsonBody } from '../fixture/stock-json-example';
import { VehicleStock } from '../../src/models/VehicleStock';
import * as should from 'should';
import { createMockCarOwnerEmployee } from '../fixture/create-system-admin';



describe('register vehicle stock', () => {
    afterEach(async () => {
        await resetDB();
    });

    let currentUserMapping: UserMappingModel;
    let userVehicle: VehicleModel;

    beforeEach(async () => {
        const {userMapping, vehicle} = await importBaseData();
        currentUserMapping = userMapping;
        userVehicle = vehicle;
    });

    it('should update vehicle when the stock is registered', async () => {
        await registerVehicleStock(userVehicle.id, stockExampleMaker(), await createMockCarOwnerEmployee(userVehicle));
        const latestStock = await VehicleStock.findOne({'vin': userVehicle.VIN});
        const latestVehicle = await Vehicle.findById(userVehicle.id);
        should(latestStock).be.not.null();
        should(latestVehicle.plateNo).be.equal(stockJsonBody.provincialCapital + latestVehicle.legacyPlateNo);
        should(latestVehicle.stockRecord.toString()).be.equal(latestStock.id);
        should(latestStock.damages.damageLevel).be.equal(stockJsonBody.damages.damageLevel);
    });

    it('should error when the stock have been registered', async () => {
        const currentUser = await createMockCarOwnerEmployee(userVehicle);
        await registerVehicleStock(userVehicle.id, stockExampleMaker(), currentUser);
        let error: string;
        try {
            await registerVehicleStock(userVehicle.id, stockExampleMaker(), currentUser);
        }
        catch (exception) {
            error = exception;
        }
        should(error).be.equal('HasSubmitted');

    });
});