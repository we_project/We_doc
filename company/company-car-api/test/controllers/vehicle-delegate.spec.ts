import { ExternalDelegate } from './../../src/models/ExternalDelegate';
import { UserMapping } from './../../src/models/UserMapping';
import { resetDB } from '../db.spec';
import nock = require('nock');
import { Vehicle, VehicleModel } from '../../src/models/Vehicle';
import { UserModel } from '../../src/models/User';
import { importBaseData } from '../fixture/import-base-data';
import { UserRole } from '../../src/models/user-role';
import { agent } from 'supertest';
import { createWebApp } from '../../src/web-app';
import { generateJwt } from '../../src/midwares/passportJwt';
import { VehicleListItem } from '../../src/controllers/dto/VehicleListItem';
import {  User  } from '../../src/models/User';
import { UserMappingModel } from '../../src/models/UserMapping';
import { daimlerSSOSuccessApi } from '../extern-service/daimler-sso-api-stub';
import { createUserMapping } from '../fixture/create-user-mapping';
import { createVehicle } from '../fixture/create-vehicle';
import { createEmployee } from '../fixture/create-system-admin';
import * as should from 'should';

describe('get all delegation ship', () => {
  afterEach(async () => {
    await resetDB();
    nock.cleanAll();
  });

  let externalDelegate: UserModel;
  let internalDelegate: UserMappingModel;
  let admin: UserModel;
  let firstVehicle: VehicleModel;
  let secondVehicle: VehicleModel;

  beforeEach(async () => {
    const { systemAdmin, vehicle } =  await importBaseData();
    admin = systemAdmin;
    firstVehicle = vehicle;

    const otherUserMapping = await createUserMapping('otherUser', 'otherUserPay');
    secondVehicle = (await createVehicle('legacyPlateNo', otherUserMapping)).vehicle;

    externalDelegate = await new User({loginId: 'external', password: '$5~%&*', role: UserRole.ExternalDelegateRoleString, mobile: '13111111111'}).save();
    internalDelegate = await createUserMapping('internalDelegate', 'internalDelegatePayId');
  });

  it('should return delegation ships by page', async () => {
    firstVehicle.internalDelegate = {
      'empId': internalDelegate.empId
    };
    firstVehicle.externalDelegate = {
      'loginId': externalDelegate.loginId
    };
    await firstVehicle.save();

    secondVehicle.externalDelegate = {
      'loginId': externalDelegate.loginId
    };
    await secondVehicle.save();

    await agent(createWebApp())
    .get('/api/vehicle-delegates')
    .query({pageSize: 1})
    .query({pageNumber: 1})
    .set('Authorization', `JWT ${generateJwt({loginId: admin.loginId})}`)
    .expect(200)
    .then(response => {
      const body = response.body;
      should(body.recordCount).be.equal(2);
      should(body.data.length).be.equal(1);

      should(body.data[0].plateNo).be.equal(firstVehicle.plateNo);
      should(body.data[0].vin).be.equal(firstVehicle.VIN);
      should(body.data[0].externalDelegate.loginId).be.equal(externalDelegate.loginId);
      should(body.data[0].externalDelegate.mobilePhone).be.equal(externalDelegate.mobile);
      should(body.data[0].internalDelegate.empId).be.equal(internalDelegate.empId);
      should(body.data[0].internalDelegate.mobilePhone).be.equal(internalDelegate.mobilePhone);
      should(body.data[0].internalDelegate.businessPhone).be.equal(internalDelegate.businessPhone);
    });
  });
});

describe('update external delegate relation on vehicle', () => {
  afterEach(async () => {
    await resetDB();
    nock.cleanAll();
  });

  let vehicleSaved: VehicleModel;
  let systemAdminSaved: UserModel;
  let externalDelegate: UserModel;

  beforeEach(async () => {
    const {vehicle, systemAdmin} = await importBaseData();
    vehicleSaved = vehicle;
    systemAdminSaved = systemAdmin;

    externalDelegate = await new User({loginId: 'external', password: '$5~%&*', role: UserRole.ExternalDelegateRoleString}).save();
  });

  it('should build relation between vehicle and user', async () => {
    await agent(createWebApp())
      .post('/api/vehicle-external-delegates')
      .send({
        'vehicleStr': `${vehicleSaved.legacyPlateNo}`,
        'externalDelegate': `${externalDelegate.loginId}`
      })
      .set('Authorization', `JWT ${generateJwt({loginId: systemAdminSaved.loginId})}`)
      .expect(201);

    const vehicleUpdated: VehicleModel[] = await Vehicle.find();
    should(vehicleUpdated[0].externalDelegate.loginId).be.equal(`${externalDelegate.loginId}`);
  });
});

describe('update internal delegate relation on vehicle', () => {
  afterEach(async () => {
    await resetDB();
    nock.cleanAll();
  });

  let vehicleSaved: VehicleModel;
  let systemAdminSaved: UserModel;
  let internalDelegate: UserModel;
  let vehicleUserMapping: UserMappingModel;

  beforeEach(async () => {
    const {vehicle, systemAdmin, userMapping} = await importBaseData();

    vehicleSaved = vehicle;
    systemAdminSaved = systemAdmin;
    vehicleUserMapping = userMapping;

    internalDelegate = await new User({empId: 'empId', fullName: 'empName', empName: 'empName', dcPayId: 'dcPayId', role: UserRole.EmployeeRoleString}).save();
  });

  it('should update internal delegate on user', async () => {
    await agent(createWebApp())
      .post('/api/vehicle-internal-delegates')
      .send({
        'vehicleStr': `${vehicleSaved.legacyPlateNo}`,
        'internalDelegate': `${internalDelegate.empId}`
      })
      .set('Authorization', `JWT ${generateJwt({loginId: systemAdminSaved.loginId})}`)
      .expect(201);

    const vehicles: Array<VehicleModel> = await Vehicle.find({'internalDelegate.empId': internalDelegate.empId});
    should(vehicles.length).be.equal(1);
  });

  it('should forbidden when set internal delegate is vehicle user himself', async () => {
    await createEmployee(vehicleUserMapping.empId, vehicleUserMapping.empName, vehicleUserMapping.dcPayId);

    await agent(createWebApp())
      .post('/api/vehicle-internal-delegates')
      .send({
        'vehicleStr': `${vehicleSaved.legacyPlateNo}`,
        'internalDelegate': `${vehicleUserMapping.empId}`
      })
      .set('Authorization', `JWT ${generateJwt({loginId: systemAdminSaved.loginId})}`)
      .expect(400)
      .then(res => {
        should(res.body.message).be.equal('Delegate and vehicle user could not be same.');
      });

    const vehicles: Array<VehicleModel> = await Vehicle.find({'internalDelegate.empId': vehicleUserMapping.empId});
    should(vehicles.length).be.equal(0);
  });
});

describe('query vehicles by employee', () => {
  afterEach(async () => {
    await resetDB();
    nock.cleanAll();
  });

  let userVehicle: VehicleModel;
  let otherVehicle: VehicleModel;

  let savedUserMapping: UserMappingModel;
  let otherUserMapping: UserMappingModel;
  beforeEach(async () => {
    otherUserMapping = await createUserMapping('otherUser', 'otherUserPay');
    otherVehicle = (await createVehicle('legacyPlateNo', otherUserMapping)).vehicle;

    const {userMapping, vehicle}  = await importBaseData();
    userVehicle = vehicle;
    savedUserMapping = userMapping;

  });


  it('should query own vehicles and delegate vehicles belong to current user', async () => {
    otherUserMapping.empName = 'test employee2';
    otherUserMapping = await otherUserMapping.save();

    userVehicle.internalDelegate = {
      empId: otherUserMapping.empId
    };
    userVehicle = await userVehicle.save();

    otherVehicle.internalDelegate = {
      empId: savedUserMapping.empId
    };
    otherVehicle = await otherVehicle.save();

    const samlToken = 'e1089f2ae1bd43f883a449855d4f50d1_B02AAB7CADB4E5C2B9D330B0B76477B7';
    daimlerSSOSuccessApi(samlToken, savedUserMapping.empId);

    await agent(createWebApp())
      .get('/api/vehicles')
      .set('Authorization', samlToken)
      .expect(200)
      .then(res => {
        const body: Array<VehicleListItem> = res.body;

        should(body.length).be.equal(2);
        should(body[0].id).be.equal(userVehicle._id.toString());
        should(body[0].plateNo).be.equal(userVehicle.plateNo);
        should(body[0].legacyPlateNo).be.equal(userVehicle.legacyPlateNo);
        should(body[0].stockStatus).be.equal(userVehicle.getStockStatus());
        should(body[0].usage).be.equal(userVehicle.usage);
        should(body[0].model).be.equal(userVehicle.carType.model);
        should(body[0].delegateTo).be.equal(otherUserMapping.empName);
        should.not.exists(body[0].delegateFrom);

        should(body[1].id).be.equal(otherVehicle._id.toString());
        should(body[1].plateNo).be.equal(otherVehicle.plateNo);
        should(body[1].legacyPlateNo).be.equal(otherVehicle.legacyPlateNo);
        should(body[1].stockStatus).be.equal(otherVehicle.getStockStatus());
        should(body[1].usage).be.equal(otherVehicle.usage);
        should(body[1].model).be.equal(otherVehicle.carType.model);
        should.not.exists(body[1].delegateTo);
        should(body[1].delegateFrom).be.equal(otherUserMapping.empName);
      });
  });

  it('should query vehicles when current user not exists in DB before', async () => {
    const mockToken = 'mocktoken';
    const mockEmpId = 'mockEmpId';
    daimlerSSOSuccessApi(mockToken, mockEmpId);

    userVehicle.internalDelegate = {
      empId: mockEmpId
    };
    await userVehicle.save();

    await agent(createWebApp())
      .get('/api/vehicles')
      .set('Authorization', mockToken)
      .expect(200)
      .then(res => {
        const body: Array<VehicleListItem> = res.body;

        should(body.length).be.equal(1);
        should(body[0].id).be.equal(userVehicle._id.toString());
        should(body[0].plateNo).be.equal(userVehicle.plateNo);
        should(body[0].legacyPlateNo).be.equal(userVehicle.legacyPlateNo);
        should(body[0].stockStatus).be.equal(userVehicle.getStockStatus());
        should(body[0].usage).be.equal(userVehicle.usage);
        should(body[0].model).be.equal(userVehicle.carType.model);
        should.not.exists(body[0].delegateTo);
        should(body[0].delegateFrom).be.equal(savedUserMapping.empName);
      });
  });

  it('should query vehicles usage when vehicle user is null', async () => {
    const mockToken = 'mocktoken';
    const mockEmpId = 'mockEmpId';
    daimlerSSOSuccessApi(mockToken, mockEmpId);

    userVehicle.user.CNNumber = null;
    userVehicle.internalDelegate = {
      empId: mockEmpId
    };
    await userVehicle.save();

    await agent(createWebApp())
      .get('/api/vehicles')
      .set('Authorization', mockToken)
      .expect(200)
      .then(res => {
        const body: Array<VehicleListItem> = res.body;

        should(body.length).be.equal(1);
        should(body[0].id).be.equal(userVehicle._id.toString());
        should(body[0].plateNo).be.equal(userVehicle.plateNo);
        should(body[0].legacyPlateNo).be.equal(userVehicle.legacyPlateNo);
        should(body[0].stockStatus).be.equal(userVehicle.getStockStatus());
        should(body[0].usage).be.equal(userVehicle.usage);
        should(body[0].model).be.equal(userVehicle.carType.model);
        should.not.exists(body[0].delegateTo);
        should(body[0].delegateFrom).be.equal(userVehicle.usage);
      });
  });
});
