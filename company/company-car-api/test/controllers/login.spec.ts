import { UserModel } from './../../src/models/User';
import { importBaseData } from './../fixture/import-base-data';
import { resetDB } from './../db.spec';
import { createWebApp } from './../../src/web-app';
import * as agent from 'supertest';
import * as nock from 'nock';

describe('login', () => {
    afterEach(async () => {
        await resetDB();
        nock.cleanAll();
    });

    let user: UserModel;
    beforeEach(async () => {
        const { systemAdmin } = await importBaseData();
        user = systemAdmin;
    });

    it('should login successfully', async () => {
        await agent(createWebApp())
        .post('/api/login')
        .send({loginId: 'WY1234', password: 'WY1234'})
        .expect(200)
        .then((resp) => {
            should(resp.body.fullName).be.equal(user.fullName);
            should.exist(resp.body.token);
            should(resp.body.role).be.equal(user.role);
        });
    });

    it('should login failed', async () => {
        await agent(createWebApp())
        .post('/api/login')
        .send({loginId: 'WY1234', password: 'wrong password'})
        .expect(401);
    });
});