import { resetDB } from '../db.spec';
import { createWebApp } from '../../src/web-app';
import { agent } from 'supertest';
import { generateJwt } from '../../src/midwares/passportJwt';
import { importBaseData } from '../fixture/import-base-data';
import { VehicleStock } from '../../src/models/VehicleStock';
import * as should from 'should';
import { UserModel } from '../../src/models/User';
import { UserRole } from '../../src/models/user-role';
import { createEmployee } from '../fixture/create-system-admin';
import { registerStockToVehicle } from '../fixture/register-stock-to-vehicle';
import nock = require('nock');
import { stockExampleMaker } from '../fixture/stock-example';
import { read }  from 'xlsx-style';
import { getCell } from '../../src/external-service/xls-wrapper';
import { VehicleModel, Vehicle } from '../../src/models/Vehicle';

describe('export vehicle stock finial record', () => {
    afterEach(async () => {
        await resetDB();
        nock.cleanAll();
        nock.enableNetConnect();
    });

    let admin: UserModel;
    let vehicleStock: VehicleStock;
    let vehicleUser: UserModel;
    let userVehicle: VehicleModel;

    beforeEach(async () => {
        const {vehicle, systemAdmin, userMapping} = await importBaseData();
        userVehicle = vehicle;
        vehicleUser = await createEmployee(userMapping.empId, userMapping.empName, userMapping.dcPayId);

        admin = systemAdmin;

        vehicleStock = await registerStockToVehicle(vehicleStock, vehicle, systemAdmin, stockExampleMaker(), {
            latitude: 39.940193,
            longitude: 116.432458,
            province: '北京市',
            city: '北京市',
            district: '东城区',
            addressDetail: '北京市东城区东直门外大街3号'
        });
    });


    it('should generate the all vehicles report when system admin call to get', async () => {
        const latestVehicle = await Vehicle.findById(userVehicle.id);
        await agent(createWebApp())
            .get('/api/vehicles/stock-finial-report')
            .set('Authorization', `JWT ${generateJwt({loginId: admin.loginId})}`)
            .responseType('arraybuffer')
            .expect(200)
            .then(response => {
                const arr = [];
                for (let i = 0; i != response.body.length; ++i) arr[i] = String.fromCharCode(response.body[i]);
                const bstr = arr.join('');


                const workbook = read(bstr, {type: 'binary'});

                const first_sheet_name = workbook.SheetNames[0];
                should(first_sheet_name).be.equal('Detail Report');
                const first_sheet = workbook.Sheets[first_sheet_name];
                should(first_sheet['K1'].v).be.equal('DAIMLER COMPANY VEHICLE STOCK TAKE RECORD');

                const a1Cell = getCell(3, 1, first_sheet);
                should(a1Cell.v).be.equal(latestVehicle.plateNo);
            });
    });
});