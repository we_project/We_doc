import { importBaseData } from '../fixture/import-base-data';
import { resetDB } from '../db.spec';
import { UserMappingModel } from '../../src/models/UserMapping';
import { VehicleModel } from '../../src/models/Vehicle';
import { createWebApp } from '../../src/web-app';
import * as agent from 'supertest';
import { UserModel } from '../../src/models/User';
import { generateJwt } from '../../src/midwares/passportJwt';
import * as nock from 'nock';
import { daimlerSSOSuccessApi } from '../extern-service/daimler-sso-api-stub';
import { mockUser } from '../../src/utils/Constants';
import * as should from 'should';
import { createExternalDelegate, createFleetAdmin, createMockCarOwnerEmployee, createEmployee } from '../fixture/create-system-admin';
import { batchAssignFleetAdminToVehicle } from '../../src/application/batch-assign-fleet-admin-to-vehicle-service';
import { createVehicle } from '../fixture/create-vehicle';
import { registerVehicleStock } from '../../src/application/register-vehicle-stock-service';
import { stockExampleMaker } from '../fixture/stock-example';
import { rejectVehicleStockStatus } from '../../src/application/reject-vehicle-stock-status';
import { submitStock } from '../fixture/submit-stock';
import { approveVehicleStockStatus } from '../../src/application/approve-vehicle-stock-status';
import { createUserMapping } from '../fixture/create-user-mapping';

describe('super admin should see vehicle', () => {
    afterEach(async () => {
        await resetDB();
        nock.cleanAll();
    });

    let employeeUser: UserMappingModel;
    let userVehicle: VehicleModel;
    let currentUser: UserModel;

    beforeEach(async () => {
        const {userMapping, vehicle, systemAdmin} = await importBaseData();
        employeeUser = userMapping;
        userVehicle = vehicle;
        currentUser = systemAdmin;
    });

    it('should system admin could access all the vehicle detail', async () => {
        await agent(createWebApp())
            .get(`/api/vehicle/${userVehicle.id}`)
            .set('Authorization', `JWT ${generateJwt({loginId: currentUser.loginId})}`)
            .expect(200);
    });

    it('should forbidden fake system admin access the vehicle detail', async () => {
        await agent(createWebApp())
            .get(`/api/vehicle/${userVehicle.id}`)
            .set('Authorization', `JWT ${generateJwt({loginId: '123'})}`)
            .expect(401);
    });
});

describe('employee should see vehicle', () => {
    afterEach(async () => {
        await resetDB();
        nock.cleanAll();
    });

    let employeeUser: UserMappingModel;
    let userVehicle: VehicleModel;
    let currentUserMapping: UserMappingModel;

    const samlToken = 'e1089f2ae1bd43f883a449855d4f50d1_B02AAB7CADB4E5C2B9D330B0B76477B7';
    beforeEach(async () => {
        const {userMapping, vehicle} = await importBaseData();
        employeeUser = userMapping;
        userVehicle = vehicle;
        currentUserMapping = userMapping;
        daimlerSSOSuccessApi(samlToken, currentUserMapping.empId);
    });

    it('should get vehicle detail successfully after employee login', async () => {
        await agent(createWebApp())
            .get(`/api/vehicle/${userVehicle.id}`)
            .set('Authorization', samlToken)
            .expect(200);
    });
});

describe('no authorization token', () => {
    afterEach(async () => {
        await resetDB();
        nock.cleanAll();
    });

    let employeeUser: UserMappingModel;
    let userVehicle: VehicleModel;
    let currentUser: UserModel;

    beforeEach(async () => {
        const {userMapping, vehicle, systemAdmin} = await importBaseData();
        employeeUser = userMapping;
        userVehicle = vehicle;
        currentUser = systemAdmin;
    });

    it('should forbidden no token the vehicle detail', async () => {
        await agent(createWebApp())
            .get(`/api/vehicle/${userVehicle.id}`)
            .expect(403);
    });
});

describe('search vehicle', () => {
   afterEach(async () => {
       await resetDB();
       nock.cleanAll();
   });

   const token = generateJwt({ loginId : 'WY1234' });
   let userVehicle: VehicleModel;
   let adminUser: UserModel;
   beforeEach(async () => {
       const {userMapping, vehicle, systemAdmin} = await importBaseData();
       userVehicle = vehicle;
       adminUser = systemAdmin;

   });

   it('should search vehicle by legacyPlateNo', async () => {
       await agent(createWebApp())
           .get('/api/vehicles/search')
           .query({q: 'W17002'})
           .set('Authorization', 'JWT ' + token)
           .expect(200)
           .then(response => {
               const body = response.body;
               should(body[0].id).be.equal(userVehicle.id);
               should(body[0].plateNo).be.equal('');
               should(body[0].legacyPlateNo).be.equal('W17002');
               should(body[0].stockStatus).be.equal('TASK');
               should(body[0].model).be.equal('CLS 200 ');
               should(body[0].vin).be.equal('WYMAKE20170822022');
               should(body[0].externalDelegate).be.not.null();
               should(body[0].internalDelegate).be.not.null();
           });
   });

    it('should search vehicle by full plate no', async () => {
        userVehicle.plateNo = '京W17002';
        await userVehicle.save();

        await agent(createWebApp())
            .get('/api/vehicles/search')
            .query({q: '京W17002'})
            .set('Authorization', 'JWT ' + token)
            .expect(200)
            .then(response => {
                const body = response.body;
                should(body[0].id).be.equal(userVehicle.id);
                should(body[0].plateNo).be.equal('京W17002');
                should(body[0].legacyPlateNo).be.equal('W17002');
                should(body[0].stockStatus).be.equal('TASK');
                should(body[0].model).be.equal('CLS 200 ');
                should(body[0].vin).be.equal('WYMAKE20170822022');
                should(body[0].externalDelegate).be.not.null();
                should(body[0].internalDelegate).be.not.null();
            });
    });

    it('should search vehicle by vin', async () => {
        userVehicle.VIN = '12345678901234567';
        await userVehicle.save();
        await agent(createWebApp())
            .get('/api/vehicles/search')
            .query({q: userVehicle.VIN})
            .set('Authorization', 'JWT ' + token)
            .expect(200)
            .then(response => {
                const body = response.body;
                should(body[0].id).be.equal(userVehicle.id);
                should(body[0].plateNo).be.equal('');
                should(body[0].legacyPlateNo).be.equal('W17002');
                should(body[0].stockStatus).be.equal('TASK');
                should(body[0].model).be.equal('CLS 200 ');
                should(body[0].vin).be.equal('12345678901234567');
                should(body[0].externalDelegate).be.not.null();
                should(body[0].internalDelegate).be.not.null();
            });
    });

    it('should search vehicle no query', async () => {
        await agent(createWebApp())
            .get('/api/vehicles/search')
            .query({q: '123'})
            .set('Authorization', 'JWT ' + token)
            .expect(200)
            .then(response => {
                const body = response.body;
                should(body.length).be.equal(0);
            });
    });

    it('should search vehicle query with no correct data', async () => {
        await agent(createWebApp())
            .get('/api/vehicles/search')
            .set('Authorization', 'JWT ' + token)
            .expect(200)
            .then(response => {
                const body = response.body;
                should(body.length).be.equal(0);
            });
    });

    it('should find vehicle which assign to the fleet admin when the filter by is assign to me', async () => {
        const firstFleetAdmin = await createFleetAdmin('FleetWY1234', '$5~%&*', 'bFleetAdmin');
        const assignee = {id: firstFleetAdmin.id, role: firstFleetAdmin.role, loginId: firstFleetAdmin.loginId, fullName: firstFleetAdmin.fullName};
        await batchAssignFleetAdminToVehicle([{vehicleId: userVehicle.id, assignee: assignee}]);

        await agent(createWebApp())
            .get('/api/vehicles/search')
            .query({q: 'W17002'})
            .query({'is-assign-to-me': true})
            .set('Authorization', 'JWT ' + generateJwt({ loginId: firstFleetAdmin.loginId }))
            .expect(200)
            .then(response => {
                const body = response.body;
                should(body[0].id).be.equal(userVehicle.id);
                should(body[0].plateNo).be.equal('');
                should(body[0].legacyPlateNo).be.equal('W17002');
                should(body[0].stockStatus).be.equal('TASK');
                should(body[0].model).be.equal('CLS 200 ');
            });
    });

    it('should not find vehicle which not assign to the fleet admin when the filter by is assign to me', async () => {
        const firstFleetAdmin = await createFleetAdmin('FleetWY1234', '$5~%&*', 'bFleetAdmin');

        await agent(createWebApp())
            .get('/api/vehicles/search')
            .query({q: '京W17002'})
            .query({'is-assign-to-me': true})
            .set('Authorization', 'JWT ' + generateJwt({ loginId: firstFleetAdmin.loginId }))
            .expect(200)
            .then(response => {
                const body = response.body;
                should(body.length).be.equal(0);
            });
    });

    it('should find vehicle which not assign to the fleet admin when not filter by is assign to me', async () => {
        const firstFleetAdmin = await createFleetAdmin('FleetWY1234', '$5~%&*', 'bFleetAdmin');

        await agent(createWebApp())
            .get('/api/vehicles/search')
            .query({q: 'W17002'})
            .query({'is-assign-to-me': false})
            .set('Authorization', 'JWT ' + generateJwt({ loginId: firstFleetAdmin.loginId }))
            .expect(200)
            .then(response => {
                const body = response.body;
                should(body.length).be.equal(1);
            });
    });

    it('should show vehicle status is submit when the searched vehicle has submit stock', async () => {
        const stockExampleData = stockExampleMaker();
        userVehicle.VIN = '12345678901234567';
        await userVehicle.save();
        const userVehicleStock = await registerVehicleStock(
            userVehicle.id,
            stockExampleData,
            await createMockCarOwnerEmployee(userVehicle));

        await agent(createWebApp())
            .get('/api/vehicles/search')
            .query({q: userVehicle.VIN})
            .set('Authorization', 'JWT ' + token)
            .expect(200)
            .then(response => {
                const body = response.body;
                should(body[0].id).be.equal(userVehicle.id);
                should(body[0].legacyPlateNo).be.equal('W17002');
                should(body[0].stockStatus).be.equal('SUBMIT');
                should(body[0].model).be.equal('CLS 200 ');
            });
    });

    it('should show vehicle status is task when the submit stock has rejected', async () => {
        userVehicle.VIN = '12345678901234567';
        const stockExampleData = stockExampleMaker();
        stockExampleData.vin = userVehicle.VIN;
        await userVehicle.save();
        const vehicleUser = await createMockCarOwnerEmployee(userVehicle);
        const userVehicleStock = await registerVehicleStock(
            userVehicle.id,
            stockExampleData,
            vehicleUser);
        await rejectVehicleStockStatus(adminUser, userVehicleStock.id);

        await agent(createWebApp())
            .get('/api/vehicles/search')
            .query({q: userVehicle.VIN})
            .set('Authorization', 'JWT ' + token)
            .expect(200)
            .then(response => {
                const body = response.body;
                should(body[0].id).be.equal(userVehicle.id);
                should(body[0].legacyPlateNo).be.equal('W17002');
                should(body[0].stockStatus).be.equal('TASK');
                should(body[0].model).be.equal('CLS 200 ');
            });
    });

    it('should show nothing when search vehicle by external delegate', async () => {
        const externalDelegate = await createExternalDelegate('external delegate', 'password', 'fullName');

        await agent(createWebApp())
          .get('/api/vehicles/search')
          .query({q: userVehicle.VIN})
          .set('Authorization', `JWT ${generateJwt({loginId: externalDelegate.loginId})}`)
          .expect(200)
          .then(response => {
            const body = response.body;
            should(body.length).be.equal(0);
          });
    });

    it('should show nothing when search delegated vehicle by external delegate', async () => {
      const externalDelegate = await createExternalDelegate('external delegate', 'password', 'fullName');
      userVehicle.externalDelegate = {loginId: externalDelegate.loginId};
      await userVehicle.save();

      await agent(createWebApp())
        .get('/api/vehicles/search')
        .query({q: userVehicle.VIN})
        .set('Authorization', `JWT ${generateJwt({loginId: externalDelegate.loginId})}`)
        .expect(200)
        .then(response => {
          const body = response.body;
          should(body.length).be.equal(0);
        });
    });

});



describe('query all vehicles by page', () => {
    afterEach(async () => {
        await resetDB();
        nock.cleanAll();
    });

    const token =  generateJwt({ loginId: 'WY1234' });
    let userVehicle: VehicleModel;
    let admin: UserModel;
    let employeeUser: UserMappingModel;

    beforeEach(async () => {
        const {userMapping, vehicle, systemAdmin} = await importBaseData();
        userVehicle = vehicle;
        admin = systemAdmin;
        employeeUser = userMapping;
    });

    it('should query all vehicle by page', async () => {
        await agent(createWebApp())
            .get('/api/vehicles/all?pageSize=2&pageNumber=1')
            .set('Authorization', 'JWT ' + token)
            .expect(200)
            .then(response => {
                const body = response.body;
                should(body.totalRecordCount).be.equal(1);
                should(body.data[0].id).be.equal(userVehicle.id);
                should(body.data[0].plateNo).be.equal('');
                should(body.data[0].legacyPlateNo).be.equal('W17002');
                should(body.data[0].stockStatus).be.equal('TASK');
                should(body.data[0].model).be.equal('CLS 200 ');
            });
    });

    it('should query vehicles belong to external delegate', async () => {
      const familyMember: UserModel = await createExternalDelegate('familyMember', 'password', 'fullName');
      userVehicle.externalDelegate = {loginId: familyMember.loginId};
      await userVehicle.save();

      await agent(createWebApp())
        .get('/api/vehicles/all?pageSize=5&pageNumber=1')
        .set('Authorization', `JWT ${generateJwt({loginId: familyMember.loginId})}`)
        .expect(200)
        .then(rep => {
          const body = rep.body;

          should(body.totalRecordCount).be.equal(1);
          should(body.data[0].plateNo).be.equal(userVehicle.plateNo);
          should(body.data[0].legacyPlateNo).be.equal(userVehicle.legacyPlateNo);
          should(body.data[0].stockStatus).be.equal(userVehicle.getStockStatus());
          should(body.data[0].usage).be.equal(userVehicle.usage);
          should(body.data[0].model).be.equal(userVehicle.carType.model);
        });
    });

    it('should query all vehicle with the order as stockRecord status(TASK SUBMIT DONE) then as legacyPlateNo when the approved vehicle stock is submitted after the unapproved vehicle stock', async () => {
        const {insurance: submittedinsurance,   vehicle: submittedVehicle } = await createVehicle('0bst-car', employeeUser, '567890123');
        const submitter = await createEmployee('mockEmployee', 'mockEmployee', submittedVehicle.user.CNNumber);
        await submitStock(submittedVehicle, submitter);

        const otherEmployee = await createUserMapping('otherEmployee', 'otherEmployeePay');
        const {insurance: approvedinsurance,   vehicle: approvedVehicle } = await createVehicle('0ast-car', otherEmployee, '567890179');
        const toApproveVehicleOwner = await createEmployee('approvedEmployee', 'approvedEmployee', approvedVehicle.user.CNNumber);
        const toApproveStock = await submitStock(approvedVehicle, toApproveVehicleOwner);
        await approveVehicleStockStatus(toApproveStock.id, admin);


        await agent(createWebApp())
            .get('/api/vehicles/all?pageSize=3&pageNumber=1')
            .set('Authorization', 'JWT ' + token)
            .expect(200)
            .then(response => {
                const body = response.body;
                should(body.totalRecordCount).be.equal(3);
                should(body.data[0].id).be.equal(userVehicle.id);
                should(body.data[0].stockStatus).be.equal('TASK');
                should(body.data[1].id).be.equal(submittedVehicle.id);
                should(body.data[1].stockStatus).be.equal('SUBMIT');
                should(body.data[2].id).be.equal(approvedVehicle.id);
                should(body.data[2].stockStatus).be.equal('DONE');
            });
    });

    it('should query all vehicle with the order as stockRecord status(TASK SUBMIT DONE) then as legacyPlateNo when the approved vehicle stock is submitted before the unapproved vehicle stock', async () => {
        const otherEmployee = await createUserMapping('otherEmployee', 'otherEmployeePay');
        const {insurance: approvedinsurance,   vehicle: approvedVehicle } = await createVehicle('0ast-car', otherEmployee, '567890179');
        const toApproveVehicleOwner = await createEmployee('approvedEmployee', 'approvedEmployee', approvedVehicle.user.CNNumber);
        const toApproveStock = await submitStock(approvedVehicle, toApproveVehicleOwner);
        await approveVehicleStockStatus(toApproveStock.id, admin);

        const {insurance: submittedinsurance,   vehicle: submittedVehicle } = await createVehicle('0bst-car', employeeUser, '567890123');
        const submitter = await createEmployee('mockEmployee', 'mockEmployee', submittedVehicle.user.CNNumber);
        await submitStock(submittedVehicle, submitter);


        await agent(createWebApp())
            .get('/api/vehicles/all?pageSize=3&pageNumber=1')
            .set('Authorization', 'JWT ' + token)
            .expect(200)
            .then(response => {
                const body = response.body;
                should(body.totalRecordCount).be.equal(3);
                should(body.data[0].id).be.equal(userVehicle.id);
                should(body.data[0].stockStatus).be.equal('TASK');
                should(body.data[1].id).be.equal(submittedVehicle.id);
                should(body.data[1].stockStatus).be.equal('SUBMIT');
                should(body.data[2].id).be.equal(approvedVehicle.id);
                should(body.data[2].stockStatus).be.equal('DONE');
            });
    });

    it('should only return assignee to me data', async () => {
        const firstFleetAdmin = await createFleetAdmin('FleetWY1234', '$5~%&*', 'bFleetAdmin');
        const {insurance,  vehicle } = await createVehicle('Z-last-car', employeeUser);
        const lastVehicleByPlantNumber = vehicle;

        const assignee = {id: firstFleetAdmin.id, role: firstFleetAdmin.role, loginId: firstFleetAdmin.loginId, fullName: firstFleetAdmin.fullName};
        await batchAssignFleetAdminToVehicle([{vehicleId: lastVehicleByPlantNumber.id, assignee: assignee}]);


        await agent(createWebApp())
            .get('/api/vehicles/all?pageSize=2&pageNumber=1&is-assign-to-me=true')
            .set('Authorization', 'JWT ' +  generateJwt({ loginId: firstFleetAdmin.loginId }))
            .expect(200)
            .then(response => {
                const body = response.body;
                should(body.data.length).be.equal(1);
                should(body.data[0].id).be.equal(lastVehicleByPlantNumber.id);
            });
    });

    it('should show all vehicles when is-assign-to-me as false ', async () => {
        const firstFleetAdmin = await createFleetAdmin('FleetWY1234', '$5~%&*', 'bFleetAdmin');
        const {insurance,  vehicle } = await createVehicle('Z-last-car', employeeUser);
        const lastVehicleByPlantNumber = vehicle;

        const assignee = {id: firstFleetAdmin.id, role: firstFleetAdmin.role, loginId: firstFleetAdmin.loginId, fullName: firstFleetAdmin.fullName};
        await batchAssignFleetAdminToVehicle([{vehicleId: lastVehicleByPlantNumber.id, assignee: assignee}]);


        await agent(createWebApp())
            .get('/api/vehicles/all?pageSize=2&pageNumber=1')
            .query({'is-assign-to-me': false})
            .set('Authorization', 'JWT ' +  generateJwt({ loginId: firstFleetAdmin.loginId }))
            .expect(200)
            .then(response => {
                const body = response.body;
                should(body.data.length).be.equal(2);
            });
    });

    it('should show all non-stock vehicles when counted as true ', async () => {
        const firstFleetAdmin = await createFleetAdmin('FleetWY1234', '$5~%&*', 'bFleetAdmin');

        const otherEmployee = await createUserMapping('otherEmployee', 'otherEmployeePay');
        const {insurance: approvedinsurance,   vehicle: approvedVehicle } = await createVehicle('0ast-car', otherEmployee, '567890179');
        const toRejectVehicleOwner = await createEmployee('approvedEmployee', 'approvedEmployee', approvedVehicle.user.CNNumber);
        const toRejectStock = await submitStock(approvedVehicle, toRejectVehicleOwner);
        await rejectVehicleStockStatus(admin, toRejectStock.id);

        const {insurance: submittedinsurance,   vehicle: submittedVehicle } = await createVehicle('0bst-car', employeeUser, '567890123');
        const submitter = await createEmployee('mockEmployee', 'mockEmployee', submittedVehicle.user.CNNumber);
        await submitStock(submittedVehicle, submitter);



        await agent(createWebApp())
            .get('/api/vehicles/all?pageSize=9&pageNumber=1')
            .query({'counted': true})
            .set('Authorization', 'JWT ' +  generateJwt({ loginId: firstFleetAdmin.loginId }))
            .expect(200)
            .then(response => {
                const body = response.body;
                should(body.data.length).be.equal(1);
                should(body.data[0].id).be.equal(submittedVehicle.id);
                should(body.data[0].stockStatus).be.equal('SUBMIT');
            });
    });

    it('should show all vehicles when counted as false', async () => {
        const firstFleetAdmin = await createFleetAdmin('FleetWY1234', '$5~%&*', 'bFleetAdmin');

        const otherEmployee = await createUserMapping('otherEmployee', 'otherEmployeePay');
        const {insurance: approvedinsurance,   vehicle: approvedVehicle } = await createVehicle('0ast-car', otherEmployee, '567890179');
        const toApproveVehicleOwner = await createEmployee('approvedEmployee', 'approvedEmployee', approvedVehicle.user.CNNumber);
        const toApproveStock = await submitStock(approvedVehicle, toApproveVehicleOwner);
        await rejectVehicleStockStatus(admin, toApproveStock.id);


        const {insurance: submittedinsurance,   vehicle: submittedVehicle } = await createVehicle('0bst-car', employeeUser, '567890123');
        const submitter = await createEmployee('mockEmployee', 'mockEmployee', submittedVehicle.user.CNNumber);
        await submitStock(submittedVehicle, submitter);



        await agent(createWebApp())
            .get('/api/vehicles/all?pageSize=9&pageNumber=1')
            .query({'counted': false})
            .set('Authorization', 'JWT ' +  generateJwt({ loginId: firstFleetAdmin.loginId }))
            .expect(200)
            .then(response => {
                const body = response.body;
                should(body.data.length).be.equal(3);
            });
    });
});