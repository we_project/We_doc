import { ExamineTypes } from './../../src/models/VehicleStock';
import { rejectVehicleStockStatus } from './../../src/application/reject-vehicle-stock-status';
import { resetDB } from '../db.spec';
import nock = require('nock');
import { createWebApp } from '../../src/web-app';
import { agent } from 'supertest';
import { generateJwt } from '../../src/midwares/passportJwt';
import { importBaseData } from '../fixture/import-base-data';
import { registerVehicleStock } from '../../src/application/register-vehicle-stock-service';
import { stockExampleMaker } from '../fixture/stock-example';
import { mockBaiduGeoApi } from '../extern-service/baidu-map-geo-locate-api-stub';
import { completeTheStockAddress } from '../../src/application/complete-stock-address';
import { VehicleStock } from '../../src/models/VehicleStock';
import * as should from 'should';
import { UserModel } from '../../src/models/User';
import { UserRole }  from '../../src/models/user-role';
import { createEmployee } from '../fixture/create-system-admin';

describe('query stocktake record', () => {
   afterEach(async () => {
       await resetDB();
       nock.cleanAll();
       nock.enableNetConnect();
   });

   let admin: UserModel;
   let vehicleStock: VehicleStock;
   let vehicleUser: UserModel;

   beforeEach(async () => {
       const {vehicle, systemAdmin, userMapping} = await importBaseData();
       vehicleUser = await createEmployee(userMapping.empId, userMapping.empName, userMapping.dcPayId);

       admin = systemAdmin;

       const latitude: number = 39.940193;
       const longitude: number = 116.432458;
       const province = '北京市';
       const city = '北京市';
       const district = '东城区';
       const addressDetail = '北京市东城区东直门外大街3号';

       vehicleStock = await registerVehicleStock(vehicle.id, stockExampleMaker(), systemAdmin);

       mockBaiduGeoApi(province, city, district, addressDetail, latitude, longitude);

       await completeTheStockAddress();
   });


   it('should query all stocktake record', async () => {
       await agent(createWebApp())
           .get('/api/stocktake-record')
           .query({pageSize: 5})
           .query({pageNumber: 1})
           .set('Authorization', `JWT ${generateJwt({loginId: admin.loginId})}`)
           .expect(200)
           .then(response => {
               const recordCount = response.body.recordCount;
               should(recordCount).be.equal(1);

               const data = response.body.data[0];
               should(data.stockId).be.equal(vehicleStock.id);
               should(data.commitTime).be.equal(vehicleStock.commitTime);
               should(data.plateNo).be.equal('京W17002') ;
               should(data.vin).be.equal('WYMAKE20170822022');
               should(data.make).be.equal('china');
               should(data.model).be.equal('CLS 200 ');
               should(data.usage).be.equal('Employee Entitlement Car');
               should(data.province).be.equal('北京市');
               should(data.city).be.equal('北京市');
               should(data.zone).be.equal('东城区');
               should(data.detailAddress).be.equal('北京市东城区东直门外大街3号');
               should(data.exteriorColour).be.equal('Black');
               should(data.interiorColour).be.equal('Red');
               should(data.insuranceValid).be.false();
               should(data.annualCheckOverdue).be.true();
               should(data.warningTriangle).be.equal('YES');
               should(data.fireExtinguisher).be.equal('YES');
               should(data.floorMats).be.equal('YES');
               should(data.mileage).be.equal('345435345');
               should(data.damageLevel).be.equal('PERFECT');
               should(data.carIsMobileAndUsable).be.equal('YES');

               should(data.leftFrontImage).be.equal('59a001603ebcd400066718ac');
               should(data.rightBackImage).be.equal('59a001623ebcd400066718ae');
               should(data.vehicleLicenseImage).be.equal('59a0016a3ebcd400066718b0');
               should(data.odometerImage).be.equal('59a001733ebcd400066718b2');

               should(data.damagePhotos[0].site).be.equal('BACK');
               should(data.damagePhotos[0].imageId).be.equal('59a001793ebcd400066718b4');
               should(data.submitterFullName).be.equal(admin.fullName);
               should(data.submitterRole).be.equal(UserRole.SystemAdminRoleString);
               should(data.vehicleUserName).be.equal(vehicleUser.empName);
           });
     });

    it('should query all stocktake record and display dcPayId on vehicleUserName when no empName', async () => {
      vehicleStock.vehicleUser.empName = null;
      await vehicleStock.save();

      await agent(createWebApp())
        .get('/api/stocktake-record')
        .query({pageSize: 5})
        .query({pageNumber: 1})
        .set('Authorization', `JWT ${generateJwt({loginId: admin.loginId})}`)
        .expect(200)
        .then(response => {
          const recordCount = response.body.recordCount;
          should(recordCount).be.equal(1);

          const data = response.body.data[0];
          should(data.stockId).be.equal(vehicleStock.id);
          should(data.commitTime).be.equal(vehicleStock.commitTime);
          should(data.plateNo).be.equal('京W17002') ;
          should(data.vin).be.equal('WYMAKE20170822022');
          should(data.make).be.equal('china');
          should(data.model).be.equal('CLS 200 ');
          should(data.usage).be.equal('Employee Entitlement Car');
          should(data.province).be.equal('北京市');
          should(data.city).be.equal('北京市');
          should(data.zone).be.equal('东城区');
          should(data.detailAddress).be.equal('北京市东城区东直门外大街3号');
          should(data.exteriorColour).be.equal('Black');
          should(data.interiorColour).be.equal('Red');
          should(data.insuranceValid).be.false();
          should(data.annualCheckOverdue).be.true();
          should(data.warningTriangle).be.equal('YES');
          should(data.fireExtinguisher).be.equal('YES');
          should(data.floorMats).be.equal('YES');
          should(data.mileage).be.equal('345435345');
          should(data.damageLevel).be.equal('PERFECT');
          should(data.carIsMobileAndUsable).be.equal('YES');

          should(data.leftFrontImage).be.equal('59a001603ebcd400066718ac');
          should(data.rightBackImage).be.equal('59a001623ebcd400066718ae');
          should(data.vehicleLicenseImage).be.equal('59a0016a3ebcd400066718b0');
          should(data.odometerImage).be.equal('59a001733ebcd400066718b2');

          should(data.damagePhotos[0].site).be.equal('BACK');
          should(data.damagePhotos[0].imageId).be.equal('59a001793ebcd400066718b4');
          should(data.submitterFullName).be.equal(admin.fullName);
          should(data.submitterRole).be.equal(UserRole.SystemAdminRoleString);
          should(data.vehicleUserName).be.equal(vehicleUser.dcPayId);
        });
    });

    it('should query all stocktake record and display null when no dcPayId', async () => {
      vehicleStock.vehicleUser = null;
      await vehicleStock.save();

      await agent(createWebApp())
        .get('/api/stocktake-record')
        .query({pageSize: 5})
        .query({pageNumber: 1})
        .set('Authorization', `JWT ${generateJwt({loginId: admin.loginId})}`)
        .expect(200)
        .then(response => {
          const recordCount = response.body.recordCount;
          should(recordCount).be.equal(1);

          const data = response.body.data[0];
          should(data.stockId).be.equal(vehicleStock.id);
          should(data.commitTime).be.equal(vehicleStock.commitTime);
          should(data.plateNo).be.equal('京W17002') ;
          should(data.vin).be.equal('WYMAKE20170822022');
          should(data.make).be.equal('china');
          should(data.model).be.equal('CLS 200 ');
          should(data.usage).be.equal('Employee Entitlement Car');
          should(data.province).be.equal('北京市');
          should(data.city).be.equal('北京市');
          should(data.zone).be.equal('东城区');
          should(data.detailAddress).be.equal('北京市东城区东直门外大街3号');
          should(data.exteriorColour).be.equal('Black');
          should(data.interiorColour).be.equal('Red');
          should(data.insuranceValid).be.false();
          should(data.annualCheckOverdue).be.true();
          should(data.warningTriangle).be.equal('YES');
          should(data.fireExtinguisher).be.equal('YES');
          should(data.floorMats).be.equal('YES');
          should(data.mileage).be.equal('345435345');
          should(data.damageLevel).be.equal('PERFECT');
          should(data.carIsMobileAndUsable).be.equal('YES');

          should(data.leftFrontImage).be.equal('59a001603ebcd400066718ac');
          should(data.rightBackImage).be.equal('59a001623ebcd400066718ae');
          should(data.vehicleLicenseImage).be.equal('59a0016a3ebcd400066718b0');
          should(data.odometerImage).be.equal('59a001733ebcd400066718b2');

          should(data.damagePhotos[0].site).be.equal('BACK');
          should(data.damagePhotos[0].imageId).be.equal('59a001793ebcd400066718b4');
          should(data.submitterFullName).be.equal(admin.fullName);
          should(data.submitterRole).be.equal(UserRole.SystemAdminRoleString);
          should(data.vehicleUserName).be.equal(null);
        });
    });

    it('should display rejection comment when sysadmin reject the vehicle stock', async () => {
      const rejectionComment = 'this is comment';
      await rejectVehicleStockStatus(admin, vehicleStock.id, rejectionComment);

      await agent(createWebApp())
      .get('/api/stocktake-record')
      .query({pageSize: 5})
      .query({pageNumber: 1})
      .expect(200)
      .set('Authorization', `JWT ${generateJwt({loginId: admin.loginId})}`)
      .then((response) => {
        const recordCount = response.body.recordCount;
        should(recordCount).be.equal(1);

        const data = response.body.data[0];
        should(data.stockId).be.equal(vehicleStock.id);
        should(data.commitTime).be.equal(vehicleStock.commitTime);
        should(data.examineStatusType).be.equal(ExamineTypes.Rejection);
        should(data.rejectComment).be.equal(rejectionComment);
        should(data.plateNo).be.equal('京W17002') ;
        should(data.vin).be.equal('WYMAKE20170822022');
        should(data.make).be.equal('china');
        should(data.model).be.equal('CLS 200 ');
        should(data.usage).be.equal('Employee Entitlement Car');
        should(data.province).be.equal('北京市');
        should(data.city).be.equal('北京市');
        should(data.zone).be.equal('东城区');
        should(data.detailAddress).be.equal('北京市东城区东直门外大街3号');
        should(data.exteriorColour).be.equal('Black');
        should(data.interiorColour).be.equal('Red');
        should(data.insuranceValid).be.false();
        should(data.annualCheckOverdue).be.true();
        should(data.warningTriangle).be.equal('YES');
        should(data.fireExtinguisher).be.equal('YES');
        should(data.floorMats).be.equal('YES');
        should(data.mileage).be.equal('345435345');
        should(data.damageLevel).be.equal('PERFECT');
        should(data.carIsMobileAndUsable).be.equal('YES');

        should(data.leftFrontImage).be.equal('59a001603ebcd400066718ac');
        should(data.rightBackImage).be.equal('59a001623ebcd400066718ae');
        should(data.vehicleLicenseImage).be.equal('59a0016a3ebcd400066718b0');
        should(data.odometerImage).be.equal('59a001733ebcd400066718b2');

        should(data.damagePhotos[0].site).be.equal('BACK');
        should(data.damagePhotos[0].imageId).be.equal('59a001793ebcd400066718b4');
        should(data.submitterFullName).be.equal(admin.fullName);
        should(data.submitterRole).be.equal(UserRole.SystemAdminRoleString);
        should(data.vehicleUserName).be.equal(vehicleUser.empName);
      });
    });
});