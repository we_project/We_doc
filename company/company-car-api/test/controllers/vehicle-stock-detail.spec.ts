import { importBaseData } from '../fixture/import-base-data';
import { resetDB } from '../db.spec';
import { UserMappingModel } from '../../src/models/UserMapping';
import { VehicleModel, Vehicle } from '../../src/models/Vehicle';
import { createWebApp } from '../../src/web-app';
import * as agent from 'supertest';
import { stockJsonBody } from '../fixture/stock-json-example';
import { daimlerSSOSuccessApi } from '../extern-service/daimler-sso-api-stub';
import * as nock from 'nock';
import { createVehicle } from '../fixture/create-vehicle';
import { createUserMapping } from '../fixture/create-user-mapping';
import { UserModel } from '../../src/models/User';
import { generateJwt } from '../../src/midwares/passportJwt';
import { registerVehicleStock } from '../../src/application/register-vehicle-stock-service';
import { stockExampleMaker } from '../fixture/stock-example';
import { VehicleStockDetail } from '../../src/application/dto/VehicleStockDetail';
import {
  createEmployee, createExternalDelegate, createFleetAdmin,
  createMockCarOwnerEmployee
} from '../fixture/create-system-admin';
import { rejectVehicleStockStatus } from '../../src/application/reject-vehicle-stock-status';
import { ExamineTypes, VehicleStock } from '../../src/models/VehicleStock';
import * as should from 'should';
import { approveVehicleStockStatus } from '../../src/application/approve-vehicle-stock-status';
import { submitStock } from '../fixture/submit-stock';
import { InsuranceModel } from '../../src/models/Insurance';

describe('save the stock', () => {
    afterEach(async () => {
        await resetDB();
        nock.cleanAll();
    });

    let currentUserMapping: UserMappingModel;
    let userVehicle: VehicleModel;
    let currentUser: UserModel;
    let userVehicleInsurance: InsuranceModel;

    const samlToken = 'e1089f2ae1bd43f883a449855d4f50d1_B02AAB7CADB4E5C2B9D330B0B76477B7';
    beforeEach(async () => {
        const {userMapping, vehicle, systemAdmin, insurance} = await importBaseData();
        currentUserMapping = userMapping;
        userVehicle = vehicle;
        currentUser = systemAdmin;
        userVehicleInsurance = insurance;
        daimlerSSOSuccessApi(samlToken, currentUserMapping.empId);
    });

    it('should save the stock successfully when the employee owner save to non-stocked vehicle', async () => {
        await agent(createWebApp())
            .post(`/api/vehicle-stocks/vehicle/${userVehicle.id}`)
            .set('Authorization', samlToken)
            .send(stockJsonBody)
            .expect(201);
    });

    it('should save the stock successfully when the external delegate save stock', async () => {
        await createEmployee(currentUserMapping.empId, currentUserMapping.empName, currentUserMapping.dcPayId);
        currentUser = await createExternalDelegate('external', 'externalDelegate', '123456');
        userVehicle.externalDelegate = {loginId: currentUser.loginId};
        await userVehicle.save();

        await agent(createWebApp())
          .post(`/api/vehicle-stocks/vehicle/${userVehicle.id}`)
          .set('Authorization', `JWT ${generateJwt({loginId: currentUser.loginId})}`)
          .send(stockJsonBody)
          .expect(201);
    });

    it('should save the stock successfully when the internal delegate save stock', async () => {
        daimlerSSOSuccessApi('mockToken', 'mockEmpID');

        const internalDelegate = await createEmployee('mockEmpID', 'mockEmpName', 'dcpid');
        userVehicle.internalDelegate.empId = internalDelegate.empId;
        await userVehicle.save();

        await agent(createWebApp())
          .post(`/api/vehicle-stocks/vehicle/${userVehicle.id}`)
          .set('Authorization', 'mockToken')
          .send(stockJsonBody)
          .expect(201);
    });

    it('should return forbidden the employee save the other employee vehicle', async () => {
        const otherEmployee = await createUserMapping('otherEmployee', 'otherEmployeePay');
        const otherVehicle = await createVehicle('W17003', otherEmployee);
        await agent(createWebApp())
            .post(`/api/vehicle-stocks/vehicle/${otherVehicle.vehicle.id}`)
            .set('Authorization', samlToken)
            .send(stockJsonBody)
            .expect(403);
    });

    it('should return created when admin save any vehicle', async () => {
        const otherVehicle = await createVehicle('W17003', currentUser);
        await agent(createWebApp())
            .post(`/api/vehicle-stocks/vehicle/${otherVehicle.vehicle.id}`)
            .set('Authorization', `JWT ${generateJwt({loginId: currentUser.loginId})}`)
            .send(stockJsonBody)
            .expect(201);
    });

    it('should return created when fleet admin save vehicle stock', async () => {
      const fleetAdmin = await createFleetAdmin('fleetAdminName', 'password', 'fullName');

      const { vehicle } = await createVehicle('W17003', currentUser);

      await agent(createWebApp())
        .post(`/api/vehicle-stocks/vehicle/${vehicle.id}`)
        .set('Authorization', `JWT ${generateJwt({loginId: fleetAdmin.loginId})}`)
        .send(stockJsonBody)
        .expect(201);
    });

    it('should not exists empId and empName and dcPayId when vehicle of this stock have not CNNumber', async () => {
      userVehicle.user.CNNumber = null;
      await userVehicle.save();

      await agent(createWebApp())
        .post(`/api/vehicle-stocks/vehicle/${userVehicle.id}`)
        .set('Authorization', `JWT ${generateJwt({loginId: currentUser.loginId})}`)
        .send(stockJsonBody)
        .expect(201);

      const stocks: Array<VehicleStock> = await VehicleStock.find();
      should.not.exist(stocks[0].vehicleUser.empId);
      should.not.exist(stocks[0].vehicleUser.empName);
      should.not.exist(stocks[0].vehicleUser.dcPayId);
    });

    it('should not exists empId and empName only dcPayId when vehicle of this stock have not vehicle user in DB', async () => {
      userVehicle.user.CNNumber = 'NO THIS USER';
      await userVehicle.save();

      await agent(createWebApp())
        .post(`/api/vehicle-stocks/vehicle/${userVehicle.id}`)
        .set('Authorization', `JWT ${generateJwt({loginId: currentUser.loginId})}`)
        .send(stockJsonBody)
        .expect(201);

      const stocks: Array<VehicleStock> = await VehicleStock.find();
      should.not.exist(stocks[0].vehicleUser.empId);
      should.not.exist(stocks[0].vehicleUser.empName);
      should(stocks[0].vehicleUser.dcPayId).be.equal(userVehicle.user.CNNumber);
    });


    it('should mark Insurance sticker valid as NO and Annual check overdue as YES when the insuranceEndDate is overdue and annualInspectionDueDate is overdue', async () => {
        userVehicle.user.CNNumber = 'NO THIS USER';
        await userVehicle.save();

        await agent(createWebApp())
            .post(`/api/vehicle-stocks/vehicle/${userVehicle.id}`)
            .set('Authorization', `JWT ${generateJwt({loginId: currentUser.loginId})}`)
            .send(stockJsonBody)
            .expect(201);

        const stocks: Array<VehicleStock> = await VehicleStock.find();
        should(stocks[0].insuranceStickerValid).be.equal('NO');
        should(stocks[0].annualCheckOverdue).be.equal('YES');
    });

    it('should mark Insurance sticker valid as YES when the insuranceEndDate is not overdue', async () => {

        userVehicleInsurance.insuranceEndDate = new Date('2020-01-30');
        await userVehicleInsurance.save();

        await agent(createWebApp())
            .post(`/api/vehicle-stocks/vehicle/${userVehicle.id}`)
            .set('Authorization', `JWT ${generateJwt({loginId: currentUser.loginId})}`)
            .send(stockJsonBody)
            .expect(201);

        const stocks: Array<VehicleStock> = await VehicleStock.find();
        should(stocks[0].insuranceStickerValid).be.equal('YES');

    });

    it('should mark Annual check overdue as NO when the annualCheckOverdue is longer than current date', async () => {

        userVehicleInsurance.annualInspectionDueDate = new Date('2020-01-30');
        await userVehicleInsurance.save();

        await agent(createWebApp())
            .post(`/api/vehicle-stocks/vehicle/${userVehicle.id}`)
            .set('Authorization', `JWT ${generateJwt({loginId: currentUser.loginId})}`)
            .send(stockJsonBody)
            .expect(201);

        const stocks: Array<VehicleStock> = await VehicleStock.find();
        should(stocks[0].annualCheckOverdue).be.equal('NO');

    });
});

describe('reject the stock', () => {
    afterEach(async () => {
        await resetDB();
        nock.cleanAll();
    });

    let currentUserMapping: UserMappingModel;
    let userVehicle: VehicleModel;
    let currentUser: UserModel;
    let stockExampleData: VehicleStockDetail;

    const samlToken = 'e1089f2ae1bd43approvedVehiclef883a449855d4f50d1_B02AAB7CADB4E5C2B9D330B0B76477B7';
    beforeEach(async () => {
        const {userMapping, vehicle, systemAdmin} = await importBaseData();
        currentUserMapping = userMapping;
        userVehicle = vehicle;
        currentUser = systemAdmin;
        daimlerSSOSuccessApi(samlToken, currentUserMapping.empId);
        stockExampleData = stockExampleMaker();
    });

    it('should save successfully when the system admin reject the stock', async () => {

        const userVehicleStock = await registerVehicleStock(userVehicle.id, stockExampleData, await createMockCarOwnerEmployee(userVehicle));

        await agent(createWebApp())
            .post(`/api/vehicle-stocks/vehicle/${userVehicleStock.id}/status/rejection`)
            .set('Authorization', `JWT ${generateJwt({loginId: currentUser.loginId})}`)
            .expect(201);

        const stock = await VehicleStock.findById(userVehicleStock.id);
        should(stock.examineStatus.userId).be.equal(currentUser.loginId);
        should(stock.examineStatus.examineType).be.equal(ExamineTypes.Rejection);

        const latestVehicle = await Vehicle.findById(userVehicle.id);
        should(latestVehicle.isSubmitStocked()).be.false();
        should(latestVehicle.getStockStatus()).be.equal('TASK');
    });

    it('should forbidden when the system admin reject the stock', async () => {
        const userVehicleStock = await registerVehicleStock(userVehicle.id, stockExampleData, await createMockCarOwnerEmployee(userVehicle));
        await rejectVehicleStockStatus(currentUser, userVehicleStock.id);

        // registerVehicleStock(userVehicle.id, );
        await agent(createWebApp())
            .post(`/api/vehicle-stocks/vehicle/${userVehicleStock.id}/status/rejection`)
            .set('Authorization', `JWT ${generateJwt({loginId: currentUser.loginId})}`)
            .expect(403);
    });

    it('should forbidden when the system admin reject the stock after approve the stock', async () => {
        const userVehicleStock = await registerVehicleStock(userVehicle.id, stockExampleData, await createMockCarOwnerEmployee(userVehicle));
        await approveVehicleStockStatus(userVehicleStock.id, currentUser);

        // registerVehicleStock(userVehicle.id, );
        await agent(createWebApp())
            .post(`/api/vehicle-stocks/vehicle/${userVehicleStock.id}/status/rejection`)
            .set('Authorization', `JWT ${generateJwt({loginId: currentUser.loginId})}`)
            .expect(403);
    });

    it('should get 200 success when system admin reject the stock just created in a batch', async () => {
        const userVehicleStock = await registerVehicleStock(userVehicle.id, stockExampleData, await createMockCarOwnerEmployee(userVehicle));

        const otherEmployee = await createUserMapping('otherEmployee', 'otherEmployeePay');
        const {insurance: submittedinsurance,   vehicle: submittedVehicle } = await createVehicle('0bst-car', otherEmployee, '567890123');
        const submitter = await createEmployee(otherEmployee.empId, otherEmployee.empName, submittedVehicle.user.CNNumber);
        const submitOtherVehicleStock = await submitStock(submittedVehicle, submitter);


        const userVehicleStockComment = 'this is comment1';
        const submitOtherVehicleStockComment = 'this is comment2';

        const jsonBody = [
            {
              'id': userVehicleStock.id,
              'comment': userVehicleStockComment
            }, {
              'id': submitOtherVehicleStock.id,
              'comment': submitOtherVehicleStockComment
            }
        ];
        // registerVehicleStock(userVehicle.id, );
        await agent(createWebApp())
            .post(`/api/vehicle-stocks/vehicle/status/reject-batch`)
            .set('Authorization', `JWT ${generateJwt({loginId: currentUser.loginId})}`)
            .send(jsonBody)
            .expect(200);

        const stock = await VehicleStock.findById(userVehicleStock.id);
        should(stock.examineStatus.userId).be.equal(currentUser.loginId);
        should(stock.examineStatus.examineType).be.equal(ExamineTypes.Rejection);
        should(stock.examineStatus.comment).be.equal(userVehicleStockComment);

        const latestVehicle = await Vehicle.findById(userVehicle.id);
        should(latestVehicle.isSubmitStocked()).be.false();
        should(latestVehicle.getStockStatus()).be.equal('TASK');

        const lastestSubmitstock = await VehicleStock.findById(submitOtherVehicleStock.id);
        should(lastestSubmitstock.examineStatus.userId).be.equal(currentUser.loginId);
        should(lastestSubmitstock.examineStatus.examineType).be.equal(ExamineTypes.Rejection);
        should(lastestSubmitstock.examineStatus.comment).be.equal(submitOtherVehicleStockComment);

        const latestSubmittedVehicle = await Vehicle.findById(submittedVehicle.id);
        should(latestSubmittedVehicle.isSubmitStocked()).be.false();
        should(latestSubmittedVehicle.getStockStatus()).be.equal('TASK');
    });


    it('should get 200 success when system admin reject the stock in a batch just some stock even rejected/approved', async () => {
        const userVehicleStock = await registerVehicleStock(userVehicle.id, stockExampleData, await createMockCarOwnerEmployee(userVehicle));

        const otherEmployee = await createUserMapping('otherEmployee', 'otherEmployeePay');
        const {insurance: submittedinsurance,   vehicle: submittedVehicle } = await createVehicle('0bst-car', otherEmployee, '567890123');
        const submitter = await createEmployee(otherEmployee.empId, otherEmployee.empName, submittedVehicle.user.CNNumber);
        const submitOtherVehicleStock = await submitStock(submittedVehicle, submitter);
        await approveVehicleStockStatus(submitOtherVehicleStock.id, currentUser);

        const userVehicleStockComment = 'this is comment1';
        const submitOtherVehicleStockComment = 'this is comment2';

        const jsonBody = [
            {
              'id': userVehicleStock.id,
              'comment': userVehicleStockComment
            }, {
              'id': submitOtherVehicleStock.id,
              'comment': submitOtherVehicleStockComment
            }
        ];
        // registerVehicleStock(userVehicle.id, );
        await agent(createWebApp())
            .post(`/api/vehicle-stocks/vehicle/status/reject-batch`)
            .set('Authorization', `JWT ${generateJwt({loginId: currentUser.loginId})}`)
            .send(jsonBody)
            .expect(200);

        const stock = await VehicleStock.findById(userVehicleStock.id);
        should(stock.examineStatus.userId).be.equal(currentUser.loginId);
        should(stock.examineStatus.examineType).be.equal(ExamineTypes.Rejection);
        should(stock.examineStatus.comment).be.equal(userVehicleStockComment);

        const latestVehicle = await Vehicle.findById(userVehicle.id);
        should(latestVehicle.isSubmitStocked()).be.false();
        should(latestVehicle.getStockStatus()).be.equal('TASK');

        const lastestSubmitstock = await VehicleStock.findById(submitOtherVehicleStock.id);
        should(lastestSubmitstock.examineStatus.userId).be.equal(currentUser.loginId);
        should(lastestSubmitstock.examineStatus.examineType).be.equal(ExamineTypes.Approval);

        const latestSubmittedVehicle = await Vehicle.findById(submittedVehicle.id);
        should(latestSubmittedVehicle.isSubmitStocked()).be.True();
        should(latestSubmittedVehicle.getStockStatus()).be.equal('DONE');
    });
});

describe('approve the stock', () => {
    afterEach(async () => {
        await resetDB();
        nock.cleanAll();
    });

    let currentUserMapping: UserMappingModel;
    let userVehicle: VehicleModel;
    let currentUser: UserModel;
    let stockExampleData: VehicleStockDetail;

    const samlToken = 'e1089f2ae1bd43f883a449855d4f50d1_B02AAB7CADB4E5C2B9D330B0B76477B7';
    beforeEach(async () => {
        const {userMapping, vehicle, systemAdmin} = await importBaseData();
        currentUserMapping = userMapping;
        userVehicle = vehicle;
        currentUser = systemAdmin;
        daimlerSSOSuccessApi(samlToken, currentUserMapping.empId);
        stockExampleData = stockExampleMaker();
    });

    it('should save successfully when the system admin approve the stock', async () => {

        const userVehicleStock = await registerVehicleStock(userVehicle.id, stockExampleData, await createMockCarOwnerEmployee(userVehicle));

        await agent(createWebApp())
            .post(`/api/vehicle-stocks/vehicle/${userVehicleStock.id}/status/approval`)
            .set('Authorization', `JWT ${generateJwt({loginId: currentUser.loginId})}`)
            .expect(201);

        const stock = await VehicleStock.findById(userVehicleStock.id);
        should(stock.examineStatus.userId).be.equal(currentUser.loginId);
        should(stock.examineStatus.examineType).be.equal(ExamineTypes.Approval);

        const latestVehicle = await Vehicle.findById(userVehicle.id);
        should(latestVehicle.isSubmitStocked()).be.true();
        should(latestVehicle.getStockStatus()).be.equal('DONE');
    });

    it('should forbidden when the system admin approve the stock after reject the stock', async () => {
        const userVehicleStock = await registerVehicleStock(userVehicle.id, stockExampleData, await createMockCarOwnerEmployee(userVehicle));
        await rejectVehicleStockStatus(currentUser, userVehicleStock.id);

        // registerVehicleStock(userVehicle.id, );
        await agent(createWebApp())
            .post(`/api/vehicle-stocks/vehicle/${userVehicleStock.id}/status/approval`)
            .set('Authorization', `JWT ${generateJwt({loginId: currentUser.loginId})}`)
            .expect(403);
    });

    it('should forbidden when the system admin approve the stock after approve the stock', async () => {
        const userVehicleStock = await registerVehicleStock(userVehicle.id, stockExampleData, await createMockCarOwnerEmployee(userVehicle));
        await approveVehicleStockStatus(userVehicleStock.id, currentUser);

        // registerVehicleStock(userVehicle.id, );
        await agent(createWebApp())
            .post(`/api/vehicle-stocks/vehicle/${userVehicleStock.id}/status/approval`)
            .set('Authorization', `JWT ${generateJwt({loginId: currentUser.loginId})}`)
            .expect(403);
    });

    it('should get 200 success when system admin approve the stock just created in a batch', async () => {
        const userVehicleStock = await registerVehicleStock(userVehicle.id, stockExampleData, await createMockCarOwnerEmployee(userVehicle));

        const otherEmployee = await createUserMapping('otherEmployee', 'otherEmployeePay');
        const {insurance: submittedinsurance,   vehicle: submittedVehicle } = await createVehicle('0bst-car', otherEmployee, '567890123');
        const submitter = await createEmployee(otherEmployee.empId, otherEmployee.empName, submittedVehicle.user.CNNumber);
        const submitOtherVehicleStock = await submitStock(submittedVehicle, submitter);


        const jsonBody = [userVehicleStock.id, submitOtherVehicleStock.id];
        // registerVehicleStock(userVehicle.id, );
        await agent(createWebApp())
            .post(`/api/vehicle-stocks/vehicle/status/approve-batch`)
            .set('Authorization', `JWT ${generateJwt({loginId: currentUser.loginId})}`)
            .send(jsonBody)
            .expect(200);

        const stock = await VehicleStock.findById(userVehicleStock.id);
        should(stock.examineStatus.userId).be.equal(currentUser.loginId);
        should(stock.examineStatus.examineType).be.equal(ExamineTypes.Approval);

        const latestVehicle = await Vehicle.findById(userVehicle.id);
        should(latestVehicle.isSubmitStocked()).be.true();
        should(latestVehicle.getStockStatus()).be.equal('DONE');

        const lastestSubmitstock = await VehicleStock.findById(submitOtherVehicleStock.id);
        should(lastestSubmitstock.examineStatus.userId).be.equal(currentUser.loginId);
        should(lastestSubmitstock.examineStatus.examineType).be.equal(ExamineTypes.Approval);

        const latestSubmittedVehicle = await Vehicle.findById(submittedVehicle.id);
        should(latestSubmittedVehicle.isSubmitStocked()).be.true();
        should(latestSubmittedVehicle.getStockStatus()).be.equal('DONE');
    });

    it('should get 200 success when system admin approve the stock in a batch just some stock even rejected/approved', async () => {
        const userVehicleStock = await registerVehicleStock(userVehicle.id, stockExampleData, await createMockCarOwnerEmployee(userVehicle));

        const otherEmployee = await createUserMapping('otherEmployee', 'otherEmployeePay');
        const {insurance: submittedinsurance,   vehicle: submittedVehicle } = await createVehicle('0bst-car', otherEmployee, '567890123');
        const submitter = await createEmployee(otherEmployee.empId, otherEmployee.empName, submittedVehicle.user.CNNumber);
        const submitOtherVehicleStock = await submitStock(submittedVehicle, submitter);
        await rejectVehicleStockStatus(currentUser, submitOtherVehicleStock.id);



        const jsonBody = [userVehicleStock.id, submitOtherVehicleStock.id];
        // registerVehicleStock(userVehicle.id, );
        await agent(createWebApp())
            .post(`/api/vehicle-stocks/vehicle/status/approve-batch`)
            .set('Authorization', `JWT ${generateJwt({loginId: currentUser.loginId})}`)
            .send(jsonBody)
            .expect(200);

        const stock = await VehicleStock.findById(userVehicleStock.id);
        should(stock.examineStatus.userId).be.equal(currentUser.loginId);
        should(stock.examineStatus.examineType).be.equal(ExamineTypes.Approval);

        const latestVehicle = await Vehicle.findById(userVehicle.id);
        should(latestVehicle.isSubmitStocked()).be.true();
        should(latestVehicle.getStockStatus()).be.equal('DONE');

        const lastestSubmitstock = await VehicleStock.findById(submitOtherVehicleStock.id);
        should(lastestSubmitstock.examineStatus.userId).be.equal(currentUser.loginId);
        should(lastestSubmitstock.examineStatus.examineType).be.equal(ExamineTypes.Rejection);

        const latestSubmittedVehicle = await Vehicle.findById(submittedVehicle.id);
        should(latestSubmittedVehicle.isSubmitStocked()).be.False();
        should(latestSubmittedVehicle.getStockStatus()).be.equal('TASK');
    });
});