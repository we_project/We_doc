import { resetDB } from '../db.spec';
import * as nock from 'nock';
import { createWebApp } from '../../src/web-app';
import { agent } from 'supertest';
import { generateJwt } from '../../src/midwares/passportJwt';
import { User, UserModel } from '../../src/models/User';
import { UserRole } from '../../src/models/user-role';

describe('query all users', () => {
    afterEach(async () => {
        await resetDB();
        nock.cleanAll();
    });

    let systemAdmin: UserModel;
    beforeEach(async () => {
      systemAdmin = await new User({loginId: 'WY1234', password: '$5~%&*', role: UserRole.SystemAdminRoleString}).save();
      const user = await new User({loginId: 'test', password: 'password', role: UserRole.SystemAdminRoleString, fullName: 'fullName', empId: 'empId', dcPayId: 'dcPay'}).save();
      const user2 = await new User({loginId: 'test2', password: 'password2', role: UserRole.EmployeeRoleString, fullName: 'fullName2', empId: 'empId2', dcPayId: 'dcPay2'}).save();
    });

    it('should return all users', async () => {
      await agent(createWebApp())
        .get('/api/users')
        .set('Authorization', `JWT ${generateJwt({ loginId : systemAdmin.loginId})}`)
        .expect(200)
        .then(response => {
          const firstUser = response.body[1];
          should(firstUser.loginId).be.equal('test');
          should(firstUser.role).be.equal('System Admin');
          should(firstUser.fullName).be.equal('fullName');

          const secondUser = response.body[2];
          should(secondUser.loginId).be.equal('test2');
          should(secondUser.role).be.equal('Employee');
          should(secondUser.fullName).be.equal('fullName2');
        });


    });

});

describe('add new user', () => {
  afterEach(async () => {
    await  resetDB();
    nock.cleanAll();
  });

  let userModel: UserModel;
  beforeEach(async () => {
      userModel = await new User({loginId: 'WY1234', password: '$5~%&*', role: UserRole.SystemAdminRoleString}).save();
  });

  it('should add new user', async () => {
    const payload = {
      'loginId': 'test',
      'password': 'password',
      'fullName': 'fullName',
      'dcPayId': 'dcPayId',
      'empId': 'test',
      'role': 'System Admin'
    };
    await agent(createWebApp())
      .post('/api/user')
      .set('Authorization', `JWT ${generateJwt({loginId : userModel.loginId})}`)
      .send(payload)
      .expect(200)
      .then(r => {
        should(r.body.loginId).be.equal('test');
      });

    const result: UserModel[] = await User.find();
    should(result.length).be.equal(2);
    should(result[1].loginId).be.equal('test');
    should(result[1].password).be.equal('password');
    should(result[1].fullName).be.equal('fullName');
    should(result[1].dcPayId).be.equal('dcPayId');
    should(result[1].empId).be.equal('test');
    should(result[1].role).be.equal('System Admin');
  });
});

describe('should delete a user', () => {
  afterEach(async () => {
    await resetDB();
    nock.cleanAll();
  });

  let user: UserModel;
  beforeEach(async () => {
    user = await new User({loginId: 'WY1234', password: '$5~%&*', role: UserRole.SystemAdminRoleString}).save();
  });

  it('should delete a user', async () => {
    await agent(createWebApp())
      .delete(`/api/user/${user._id}`)
      .set('Authorization', `JWT ${generateJwt({loginId: 'WY1234'})}`)
      .expect(200)
      .then(r => {
        should(r.body.message).be.equal('deleted');
      });

    should(await User.find().count()).be.equal(0);
  });
});