import { VehicleModel } from '../../src/models/Vehicle';
import { UserModel } from '../../src/models/User';
import { importBaseData } from '../fixture/import-base-data';
import { agent } from 'supertest';
import { createWebApp } from '../../src/web-app';
import { generateJwt } from '../../src/midwares/passportJwt';
import * as should from 'should';
import { UserMappingModel } from '../../src/models/UserMapping';
import { resetDB } from '../db.spec';
import nock = require('nock');
import { createVehicle } from '../fixture/create-vehicle';
import { createFleetAdmin } from '../fixture/create-system-admin';
import { batchAssignFleetAdminToVehicle } from '../../src/application/batch-assign-fleet-admin-to-vehicle-service';
import { AssigneeUser } from '../../src/models/AssigneeUser';

describe('given super admin get vehicles by page to delegate', async () => {

    afterEach(async () => {
        await resetDB();
        nock.cleanAll();
    });

    let employeeUser: UserMappingModel;
    let userVehicle: VehicleModel;
    let currentUser: UserModel;

    beforeEach(async () => {
        const {userMapping, vehicle, systemAdmin} = await importBaseData();
        employeeUser = userMapping;
        userVehicle = vehicle;
        currentUser = systemAdmin;
    });


    it('should super admin get vehicle with fields', async () => {
        await createVehicle('legacyPlateNo', employeeUser);

        await agent(createWebApp())
            .get('/api/vehicles/assignees?page-size=10&page-number=1')
            .set('Authorization', 'JWT ' + generateJwt({ loginId: currentUser.loginId }))
            .expect(200)
            .then(response => {
                const totalCount = response.body.vehicleDelegationTotalCount;
                const vehicleDelegations = response.body.datas;
                should(totalCount).be.equal(2);
                should(vehicleDelegations.length).be.equal(2);
                const vehicleDelegation = vehicleDelegations[0];
                should(vehicleDelegation.plateNumber).be.equal(userVehicle.legacyPlateNo);
                should(vehicleDelegation.vin).be.equal(userVehicle.VIN);
                should(vehicleDelegation.usage).be.equal(userVehicle.usage);
                should(vehicleDelegation.counted).be.equal('TASK');
                should(vehicleDelegation.assignee).be.empty();
            });
    });

    it('should super admin get all vehicles when search content is null', async () => {
        userVehicle.legacyPlateNo = 'legacyPlateNo';
        userVehicle.plateNo = 'plateNo';
        userVehicle.assignee.fullName = 'fullName';
        userVehicle.VIN = 'VIN';
        await userVehicle.save();

        await createVehicle('legacyPlateNo', employeeUser);

        await agent(createWebApp())
            .get('/api/vehicles/assignees?page-size=10&page-number=1')
            .set('Authorization', 'JWT ' + generateJwt({ loginId: currentUser.loginId }))
            .expect(200)
            .then(response => {
                const totalCount = response.body.vehicleDelegationTotalCount;
                const vehicleDelegations = response.body.datas;
                should(totalCount).be.equal(2);
                should(vehicleDelegations.length).be.equal(2);
                const vehicleDelegation = vehicleDelegations[0];
                should(vehicleDelegation.plateNumber).be.equal(userVehicle.legacyPlateNo);
                should(vehicleDelegation.vin).be.equal(userVehicle.VIN);
                should(vehicleDelegation.usage).be.equal(userVehicle.usage);
                should(vehicleDelegation.counted).be.equal('TASK');
                should(vehicleDelegation.assignee.fullName).be.equal(userVehicle.assignee.fullName);
            });
    });

    it('should super admin get vehicles with plateNo fuzzy search', async () => {
        userVehicle.plateNo = 'QWERT';
        await userVehicle.save();

        await createVehicle('legacyPlateNo', employeeUser);

        await agent(createWebApp())
            .get('/api/vehicles/assignees?page-size=10&page-number=1&q=ER')
            .set('Authorization', 'JWT ' + generateJwt({ loginId: currentUser.loginId }))
            .expect(200)
            .then(response => {
                const totalCount = response.body.vehicleDelegationTotalCount;
                const vehicleDelegations = response.body.datas;
                should(totalCount).be.equal(1);
                should(vehicleDelegations.length).be.equal(1);
                const vehicleDelegation = vehicleDelegations[0];
                should(vehicleDelegation.plateNumber).be.equal(userVehicle.legacyPlateNo);
                should(vehicleDelegation.vin).be.equal(userVehicle.VIN);
                should(vehicleDelegation.usage).be.equal(userVehicle.usage);
                should(vehicleDelegation.counted).be.equal('TASK');
                should(vehicleDelegation.assignee).be.empty();
            });
    });

    it('should super admin get vehicles with vin fuzzy search', async () => {
        userVehicle.VIN = 'PO';
        await userVehicle.save();

        await createVehicle('legacyPlateNo', employeeUser);

        await agent(createWebApp())
            .get('/api/vehicles/assignees?page-size=10&page-number=1&q=PO')
            .set('Authorization', 'JWT ' + generateJwt({ loginId: currentUser.loginId }))
            .expect(200)
            .then(response => {
                const totalCount = response.body.vehicleDelegationTotalCount;
                const vehicleDelegations = response.body.datas;
                should(totalCount).be.equal(1);
                should(vehicleDelegations.length).be.equal(1);
                const vehicleDelegation = vehicleDelegations[0];
                should(vehicleDelegation.plateNumber).be.equal(userVehicle.legacyPlateNo);
                should(vehicleDelegation.vin).be.equal(userVehicle.VIN);
                should(vehicleDelegation.usage).be.equal(userVehicle.usage);
                should(vehicleDelegation.counted).be.equal('TASK');
                should(vehicleDelegation.assignee).be.empty();
            });
    });

    it('should super admin get vehicles with assignee fullName fuzzy search', async () => {
        userVehicle.assignee.fullName = 'NAME';
        await userVehicle.save();

        await createVehicle('legacyPlateNo', employeeUser);

        await agent(createWebApp())
            .get('/api/vehicles/assignees?page-size=10&page-number=1&q=NAME')
            .set('Authorization', 'JWT ' + generateJwt({ loginId: currentUser.loginId }))
            .expect(200)
            .then(response => {
                const totalCount = response.body.vehicleDelegationTotalCount;
                const vehicleDelegations = response.body.datas;
                should(totalCount).be.equal(1);
                should(vehicleDelegations.length).be.equal(1);
                const vehicleDelegation = vehicleDelegations[0];
                should(vehicleDelegation.plateNumber).be.equal(userVehicle.legacyPlateNo);
                should(vehicleDelegation.vin).be.equal(userVehicle.VIN);
                should(vehicleDelegation.usage).be.equal(userVehicle.usage);
                should(vehicleDelegation.counted).be.equal('TASK');
                should(vehicleDelegation.assignee.fullName).be.equal(userVehicle.assignee.fullName);
            });
    });

    it('should super admin get vehicles with assignee fullName fuzzy search', async () => {
        userVehicle.legacyPlateNo = 'daimler';
        await userVehicle.save();

        await createVehicle('legacyPlateNo', employeeUser);

        await agent(createWebApp())
            .get('/api/vehicles/assignees?page-size=10&page-number=1&q=daimler')
            .set('Authorization', 'JWT ' + generateJwt({ loginId: currentUser.loginId }))
            .expect(200)
            .then(response => {
                const totalCount = response.body.vehicleDelegationTotalCount;
                const vehicleDelegations = response.body.datas;
                should(totalCount).be.equal(1);
                should(vehicleDelegations.length).be.equal(1);
                const vehicleDelegation = vehicleDelegations[0];
                should(vehicleDelegation.plateNumber).be.equal(userVehicle.legacyPlateNo);
                should(vehicleDelegation.vin).be.equal(userVehicle.VIN);
                should(vehicleDelegation.usage).be.equal(userVehicle.usage);
                should(vehicleDelegation.counted).be.equal('TASK');
                should(vehicleDelegation.assignee.fullName).be.equal(userVehicle.assignee.fullName);
            });
    });

    it('should super admin get vehicle by plate number order', async () => {
        let lastVehicleByPlantNumber: VehicleModel;
        const {insurance,  vehicle } = await createVehicle('Z-last-car', employeeUser);
        lastVehicleByPlantNumber = vehicle;

        await agent(createWebApp())
            .get('/api/vehicles/assignees?page-size=2&page-number=1')
            .set('Authorization', 'JWT ' + generateJwt({ loginId: currentUser.loginId }))
            .expect(200)
            .then(response => {
                const totalCount = response.body.vehicleDelegationTotalCount;
                const vehicleDelegations = response.body.datas;
                should(totalCount).be.equal(2);
                should(vehicleDelegations.length).be.equal(2);
                should(vehicleDelegations[0].plateNumber).be.equal(userVehicle.legacyPlateNo);
                should(vehicleDelegations[0].vin).be.equal(userVehicle.VIN);
                should(vehicleDelegations[1].plateNumber).be.equal(lastVehicleByPlantNumber.legacyPlateNo);
                should(vehicleDelegations[1].vin).be.equal(lastVehicleByPlantNumber.VIN);
            });
    });


    it('should super admin get vehicle by pages', async () => {
        let lastVehicleByPlantNumber: VehicleModel;
        const {insurance,  vehicle } = await createVehicle('Z-last-car', employeeUser);
        lastVehicleByPlantNumber = vehicle;

        await agent(createWebApp())
            .get('/api/vehicles/assignees?page-size=1&page-number=2')
            .set('Authorization', 'JWT ' + generateJwt({ loginId: currentUser.loginId }))
            .expect(200)
            .then(response => {
                const totalCount = response.body.vehicleDelegationTotalCount;
                const vehicleDelegations = response.body.datas;
                should(totalCount).be.equal(2);
                should(vehicleDelegations.length).be.equal(1);
                const vehicleDelegation = vehicleDelegations[0];
                should(vehicleDelegation.plateNumber).be.equal(lastVehicleByPlantNumber.legacyPlateNo);
                should(vehicleDelegation.vin).be.equal(lastVehicleByPlantNumber.VIN);
                should(vehicleDelegation.usage).be.equal(lastVehicleByPlantNumber.usage);
                should(vehicleDelegation.counted).be.equal('TASK');
                should(vehicleDelegation.assignee).be.empty();
            });
    });

    it('should super admin get empty vehicle when no any cars', async () => {
        await userVehicle.remove();
        await agent(createWebApp())
            .get('/api/vehicles/assignees?page-size=1&page-number=10')
            .set('Authorization', 'JWT ' + generateJwt({ loginId: currentUser.loginId }))
            .expect(200)
            .then(response => {
                const totalCount = response.body.vehicleDelegationTotalCount;
                const vehicleDelegations = response.body.datas;
                should(totalCount).be.equal(0);
                should(vehicleDelegations.length).be.equal(0);
            });
    });

    it('should get assignee info when the vehicle has assigned to fleet admin', async () => {
        const firstFleetAdmin = await createFleetAdmin('FleetWY1234', '$5~%&*', 'bFleetAdmin');
        const assignee = {id: firstFleetAdmin.id, role: firstFleetAdmin.role, loginId: firstFleetAdmin.loginId, fullName: firstFleetAdmin.fullName};
        await batchAssignFleetAdminToVehicle([{vehicleId: userVehicle.id, assignee: assignee}]);

        await agent(createWebApp())
            .get('/api/vehicles/assignees?page-size=1&page-number=1')
            .set('Authorization', 'JWT ' + generateJwt({ loginId: currentUser.loginId }))
            .expect(200)
            .then(response => {
                const totalCount = response.body.vehicleDelegationTotalCount;
                const vehicleDelegations = response.body.datas;
                should(totalCount).be.equal(1);
                should(vehicleDelegations.length).be.equal(1);
                should(vehicleDelegations[0].assignee.fullName).equal(assignee.fullName);
                should(vehicleDelegations[0].assignee.id).equal(assignee.id);
                should(vehicleDelegations[0].assignee.role).equal(assignee.role);
                should(vehicleDelegations[0].assignee.loginId).equal(assignee.loginId);
            });
    });

});


describe('given super admin save vehicle assignee', async () => {

    afterEach(async () => {
        await resetDB();
        nock.cleanAll();
    });

    let employeeUser: UserMappingModel;
    let userVehicle: VehicleModel;
    let currentUser: UserModel;

    beforeEach(async () => {
        const {userMapping, vehicle, systemAdmin} = await importBaseData();
        employeeUser = userMapping;
        userVehicle = vehicle;
        currentUser = systemAdmin;
    });


    it('should save fleet admin to vehicle assignee', async () => {
        const fleetAdmin = await createFleetAdmin('FleetWY1234', '$5~%&*', 'bFleetAdmin');
        const assignee = {id: fleetAdmin.id, role: fleetAdmin.role, loginId: fleetAdmin.loginId, fullName: fleetAdmin.fullName};
        const app = createWebApp();

        await agent(app)
            .post('/api/vehicles/assignees/batch-submission')
            .send([{vehicleId: userVehicle.id, assignee: assignee}])
            .set('Authorization', 'JWT ' + generateJwt({ loginId: currentUser.loginId }))
            .expect(201);

        await agent(app)
            .get('/api/vehicles/assignees?page-size=1&page-number=1')
            .set('Authorization', 'JWT ' + generateJwt({ loginId: currentUser.loginId }))
            .expect(200)
            .then(response => {
                const totalCount = response.body.vehicleDelegationTotalCount;
                const vehicleDelegations = response.body.datas;
                should(totalCount).be.equal(1);
                should(vehicleDelegations.length).be.equal(1);
                should(vehicleDelegations[0].assignee.fullName).equal(fleetAdmin.fullName);
                should(vehicleDelegations[0].assignee.id).equal(fleetAdmin.id);
                should(vehicleDelegations[0].assignee.role).equal(fleetAdmin.role);
                should(vehicleDelegations[0].assignee.loginId).equal(fleetAdmin.loginId);
            });
    });

    it('should return 201 even vehicle not exists', async () => {
        let lastVehicleByPlantNumber: VehicleModel;
        const {insurance,  vehicle } = await createVehicle('Z-last-car', employeeUser);
        lastVehicleByPlantNumber = vehicle;
        const lastVehicleId: string = lastVehicleByPlantNumber.id;
        await lastVehicleByPlantNumber.remove();
        const fleetAdmin = await createFleetAdmin('FleetWY1234', '$5~%&*', 'bFleetAdmin');
        await agent(createWebApp())
            .post('/api/vehicles/assignees/batch-submission')
            .send([
                {vehicleId: userVehicle.id, assignee: fleetAdmin},
                {vehicleId: lastVehicleId, assignee: fleetAdmin}
            ])
            .set('Authorization', 'JWT ' + generateJwt({ loginId: currentUser.loginId }))
            .expect(201);
    });

    it('should clear fleet admin to vehicle assignee', async () => {
        const fleetAdmin = await createFleetAdmin('FleetWY1234', '$5~%&*', 'bFleetAdmin');
        const assignee = {id: fleetAdmin.id, role: fleetAdmin.role, loginId: fleetAdmin.loginId, fullName: fleetAdmin.fullName};
        const app = createWebApp();

        await agent(app)
            .post('/api/vehicles/assignees/batch-submission')
            .send([{vehicleId: userVehicle.id, assignee: null}])
            .set('Authorization', 'JWT ' + generateJwt({ loginId: currentUser.loginId }))
            .expect(201);

        await agent(app)
            .get('/api/vehicles/assignees?page-size=1&page-number=1')
            .set('Authorization', 'JWT ' + generateJwt({ loginId: currentUser.loginId }))
            .expect(200)
            .then(response => {
                const totalCount = response.body.vehicleDelegationTotalCount;
                const vehicleDelegations = response.body.datas;
                should(totalCount).be.equal(1);
                should(vehicleDelegations.length).be.equal(1);
                should(vehicleDelegations[0].assignee).be.null();
            });
    });
});