import { importBaseData } from '../fixture/import-base-data';
import { resetDB } from '../db.spec';
import * as nock from 'nock';
import { agent } from 'supertest';
import { createWebApp } from '../../src/web-app';
import { generateJwt } from '../../src/midwares/passportJwt';
import { UserModel } from '../../src/models/User';
import { createFleetAdmin } from '../fixture/create-system-admin';
import * as should from 'should';

describe('when get fleet admin', () => {
    let currentUser: UserModel;

    afterEach(async () => {
        await resetDB();
        nock.cleanAll();
    });

    beforeEach(async () => {
        const {userMapping, vehicle, systemAdmin} = await importBaseData();
        currentUser = systemAdmin;
    });

    const assertResponse = function (firstInResponse: any, secondFleetAdmin: UserModel) {
        should(firstInResponse.fullName).be.equal(secondFleetAdmin.fullName);
        should(firstInResponse.loginId).be.equal(secondFleetAdmin.loginId);
        should(firstInResponse.id).be.equal(secondFleetAdmin.id);
        should(firstInResponse.role).be.equal('Fleet Admin');
    };

    it('should get fleet-admin list when system admin query by full name asc order', async () => {
        const firstFleetAdmin = await createFleetAdmin('FleetWY1234', '$5~%&*', 'bFleetAdmin');
        const secondFleetAdmin = await createFleetAdmin('FleetWY1235', '$5~%&*', 'aFleetAdmin');
        await agent(createWebApp())
            .get(`/api/users/fleet-admins`)
            .set('Authorization', `JWT ${generateJwt({loginId: currentUser.loginId})}`)
            .expect(200)
            .then(res => {
                should(res.body.length).be.equal(2);
                assertResponse(res.body[0], secondFleetAdmin);
                assertResponse(res.body[1], firstFleetAdmin);
            });
    });

    it('should get empty fleet-admin list when system admin query but no fleet-admin', async () => {
        await agent(createWebApp())
            .get(`/api/users/fleet-admins`)
            .set('Authorization', `JWT ${generateJwt({loginId: currentUser.loginId})}`)
            .expect(200)
            .then(res => {
                should(res.body.length).be.equal(0);
            });
    });


});