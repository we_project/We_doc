import { importBaseData } from '../fixture/import-base-data';
import { resetDB } from '../db.spec';
import { UserMappingModel } from '../../src/models/UserMapping';
import { VehicleModel } from '../../src/models/Vehicle';
import { createWebApp } from '../../src/web-app';
import * as agent from 'supertest';
import { UserModel } from '../../src/models/User';
import { generateJwt } from '../../src/midwares/passportJwt';
import * as nock from 'nock';
import { daimlerSSOSuccessApi } from '../extern-service/daimler-sso-api-stub';

describe('when get secret', () => {
    afterEach(async () => {
        await resetDB();
        nock.cleanAll();
    });

    let employeeUser: UserMappingModel;
    let userVehicle: VehicleModel;
    let currentUser: UserModel;

    beforeEach(async () => {
        const {userMapping, vehicle, systemAdmin} = await importBaseData();
        employeeUser = userMapping;
        userVehicle = vehicle;
        currentUser = systemAdmin;
    });

    it('should system admin successfully get when user is login as admin', async () => {
        await agent(createWebApp())
            .get(`/api/secret`)
            .set('Authorization', `JWT ${generateJwt({loginId: currentUser.loginId})}`)
            .expect(200);
    });

    it('should system admin successfully forbidden when user is not login', async () => {
        await agent(createWebApp())
            .get(`/api/secret`)
            .expect(401);
    });

    it('should system admin successfully forbidden when user is login as employee', async () => {
        const samlToken = 'e1089f2ae1bd43f883a449855d4f50d1_B02AAB7CADB4E5C2B9D330B0B76477B7';
        daimlerSSOSuccessApi(samlToken, employeeUser.empId);
        await agent(createWebApp())
            .get(`/api/secret`)
            .set('Authorization', samlToken)
            .expect(401);
    });
});
