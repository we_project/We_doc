import * as nock from 'nock';

export const daimlerSSOSuccessApi = function (samlToken: string, empId: any) {
    nock('https://anywhere.i.daimler.com:443')
        .get(`/any/anywhere/ssoLogin?stoken=${samlToken}`)
        .reply(200, JSON.stringify({'status': 'success', 'msg': '', 'userinfo': {'EMPLID': `${empId}`}}));
};

export const daimlerSSOForbiddenApi = function (samlToken: string) {
    nock('https://anywhere.i.daimler.com:443')
        .get(`/any/anywhere/ssoLogin?stoken=${samlToken}`)
        .reply(403, 'Forbidden');
};

