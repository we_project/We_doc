import * as nock from 'nock';

export const mockBaiduGeoApi = (province: string, city: string, district: string,
                                addressDetail: string, latitude: number, longitude: number) => {
    nock('https://api.map.baidu.com', )
        .get(`/geocoder/v2/?location=${`${latitude},${longitude}`}&output=json&pois=1&ak=dkdP7sq755ZGtjtmnpw9WLvaUY1jxst4&coordtype=wgs84ll`)
        .reply(200,
            {result: {addressComponent: {province: province, city: city, district: district}, formatted_address: addressDetail}});
};