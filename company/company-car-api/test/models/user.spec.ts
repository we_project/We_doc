import { User } from '../../src/models/User';
import * as should from 'should';
import { resetDB } from '../db.spec';

describe('test/app.test.js', () => {
    afterEach(async () => {
        await resetDB();
    });

    it('should test happy path', async () => {
        const user = new User({loginId: 'aa', empName: 'bb'});
        user.dcPayId = 'abc';
        const savedUser = await user.save();
        const lastestUser = await User.findById(savedUser.id);
        should(lastestUser.loginId).be.equal('aa');
    });

    it('should test happy path2', async () => {
        const user = new User({loginId: 'aa', empName: 'bb'});
        user.dcPayId = 'abc';
        const savedUser = await user.save();
        const lastestUser = await User.findById(savedUser.id);
        should(lastestUser.loginId).be.equal('aa');
    });
});
