import { User } from '../../src/models/User';
import * as should from 'should';
import { resetDB } from '../db.spec';

describe('to make sure the reset db is clear other test data', () => {
    afterEach(async () => {
        await resetDB();
    });
    it('should test happy path', async () => {
        const user = new User({loginId: 'aa', empName: 'bb'});
        user.dcPayId = 'abc';
        const savedUser = await user.save();
        const lastestUser = await User.findById(savedUser.id);
        should(lastestUser.loginId).be.equal('aa');
    });
});
