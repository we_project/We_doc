import * as mockgooses from 'mockgoose';
import mongoose = require('mongoose');
import * as log4js from 'log4js';

mongoose.Promise = global.Promise;
const mockGoose = mockgooses.Mockgoose;
const mockGooseInstance = new mockGoose(mongoose);
before(async () => {
    await mockGooseInstance.prepareStorage();
    await mongoose.connect('mongodb://example.com/TestingDB', { useMongoClient: true });
    log4js.configure('config/log4js-test.json');

});

after(async () => {
    await resetDB();
    await closeConnection();
});

const closeConnection = async() => {
    await mongoose.disconnect();
};

export const resetDB = async () => {
    await mockGooseInstance.mongooseObj.connection.dropDatabase();
};

