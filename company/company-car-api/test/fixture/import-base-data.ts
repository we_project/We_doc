import { createUserMapping } from './create-user-mapping';
import { createVehicle } from './create-vehicle';
import { createSystemAdmin }  from './create-system-admin';

export const importBaseData = async () => {
    const legacyPlateNo = 'W17002';

    const userMapping = await createUserMapping('20170829', '20170829pay');


    const systemAdmin = await createSystemAdmin('WY1234', 'V1kxMjM0');

    const {insurance, vehicle} = await createVehicle(legacyPlateNo, userMapping);

    return {userMapping, vehicle, insurance, systemAdmin};
};


