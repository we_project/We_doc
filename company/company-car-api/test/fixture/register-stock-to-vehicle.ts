import { VehicleStock } from '../../src/models/VehicleStock';
import { VehicleModel } from '../../src/models/Vehicle';
import { UserModel } from '../../src/models/User';
import { registerVehicleStock } from '../../src/application/register-vehicle-stock-service';
import { stockExampleMaker } from './stock-example';
import { mockBaiduGeoApi } from '../extern-service/baidu-map-geo-locate-api-stub';
import { completeTheStockAddress } from '../../src/application/complete-stock-address';
import { VehicleStockDetail } from '../../src/application/dto/VehicleStockDetail';

export const registerStockToVehicle = async function (vehicleStock: VehicleStock, vehicle: VehicleModel,
                                                      systemAdmin: UserModel, stockData: VehicleStockDetail,
                                                      baiduGeoApiData: { latitude: number; longitude: number; province: string; city: string; district: string; addressDetail: string }) {
    vehicleStock = await registerVehicleStock(vehicle.id, stockData, systemAdmin);

    mockBaiduGeoApi(baiduGeoApiData.province, baiduGeoApiData.city, baiduGeoApiData.district, baiduGeoApiData.addressDetail,
        baiduGeoApiData.latitude, baiduGeoApiData.longitude);

    await completeTheStockAddress();
    return vehicleStock;
};