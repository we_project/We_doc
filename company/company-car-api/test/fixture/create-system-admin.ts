import { User } from '../../src/models/User';
import { UserRole } from '../../src/models/user-role';
import { VehicleModel } from '../../src/models/Vehicle';

export const createSystemAdmin = async (usename: string, password: string) => {
    const user = await new User({loginId: usename, password: password, role: UserRole.SystemAdminRoleString}).save();
    return user;
};

export const createFleetAdmin = async (usename: string, password: string, fullName: string) => {
    const user = await new User({loginId: usename, password: password, fullName: fullName, role: UserRole.FleetAdminRole}).save();
    return user;
};

export const createExternalDelegate = async (usename: string, password: string, fullName: string) => {
  const user = await new User({loginId: usename, password: password, fullName: fullName, role: UserRole.ExternalDelegateRoleString}).save();
  return user;
};

export const createEmployee = async (empId: string, empName: string, dcPayId: string ) => {
    const user = await new User({empId: empId, empName: empName, dcPayId: dcPayId, role: UserRole.EmployeeRoleString}).save();
    return user;
};


export const createMockCarOwnerEmployee = async (vehicle: VehicleModel) => {
    const mockEmployee = await createEmployee('mockEmployee', 'mockEmployee', vehicle.user.CNNumber);

    return mockEmployee;
};

