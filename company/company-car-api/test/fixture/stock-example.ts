import { VehicleStockDetail } from '../../src/application/dto/VehicleStockDetail';
import { stockJsonBody } from './stock-json-example';

export const stockExampleMaker =  function () {
    const stockExample: VehicleStockDetail = Object.assign(new VehicleStockDetail, stockJsonBody);
    return stockExample;
};
