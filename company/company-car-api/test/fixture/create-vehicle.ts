import { UserMappingModel } from '../../src/models/UserMapping';
import { Insurance } from '../../src/models/Insurance';
import { Vehicle } from '../../src/models/Vehicle';

export const createVehicle = async (legacyPlateNo: string, userMapping: any, vin: String = 'WYMAKE20170822022') => {
    const insurance = await new Insurance({
        insuranceEndDate: new Date('2017-08-28'),
        insuranceAccidentTel: '4008833663-9 EN / 95511 CN',
        roadsideAssistance: '4006551889',
        insuranceCompany: 'myCompany',
        annualInspectionDueDate: new Date('2017-01-21'),
    }).save();

    const vehicle = await new Vehicle({
        plateNo: '',
        legacyPlateNo: legacyPlateNo,
        VIN: vin,
        engineNo: '20170822000002',
        registrationLocation: 'beijing',
        registrationDate: new Date('2018-07-28'),
        registrationCompany: 'MBLC',
        carType: {
            make: 'china',
            model: 'CLS 200 ',
        },
        mileage: {
            displayValue: 74,
            tokenDate: new Date('2018-08-28'),
        },
        location: 'beijing',
        user: {
            CNNumber: userMapping.dcPayId,
            details: ''
        },
        insurance: insurance,
        stockTakenDates: [],
        usage: 'Employee Entitlement Car',
        contractType: 'Activated Inventory',
    }).save();
    return {insurance, vehicle};
};