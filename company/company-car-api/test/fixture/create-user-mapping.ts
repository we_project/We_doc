import { UserMapping } from '../../src/models/UserMapping';

export const createUserMapping = async (empId: string, dcPayId: string) => {
    return await new UserMapping({
        empId: empId,
        empName: 'test-employee',
        dcPayId: dcPayId,
        mobilePhone: '13511111111',
        businessPhone: '13622222222',
    }).save();
};