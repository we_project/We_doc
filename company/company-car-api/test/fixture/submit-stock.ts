import { VehicleModel } from '../../src/models/Vehicle';
import { stockExampleMaker } from './stock-example';
import { registerVehicleStock } from '../../src/application/register-vehicle-stock-service';
import { createMockCarOwnerEmployee } from './create-system-admin';
import { UserModel } from '../../src/models/User';
import { isNullOrUndefined } from 'util';

export const submitStock = async function (submittedVehicle: VehicleModel, submitter?: UserModel) {
    if (isNullOrUndefined(submitter)) {
        submitter = await createMockCarOwnerEmployee(submittedVehicle);
    }
    const stockExampleData = stockExampleMaker();
    stockExampleData.vin = submittedVehicle.VIN;
    return await registerVehicleStock(
        submittedVehicle.id,
        stockExampleData,
        submitter);
};