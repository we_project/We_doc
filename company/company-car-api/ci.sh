#!/usr/bin/env bash

company-car-api-tslint() {
    echo 'tslint begin'
    npm install
    npm run tslint
    echo 'tslint end'
}

company-car-api-test() {
    echo 'test begin'
    npm install
    npm run test
    echo 'test end'
}

company-car-build() {
    echo 'build begin'
    docker build -t company-car-api:latest .
    echo 'build end'
}

company-car-deploy() {
    echo 'build begin'
    docker stop company-car-api || true && docker rm company-car-api || true
    docker run -d -p 3000:3000 -v /upload:/upload --link mongodb:mongodb --name company-car-api company-car-api
    echo 'build end'
}

case "$1" in
    company-car-api-tslint)
        company-car-api-tslint
        ;;
    company-car-api-test)
        company-car-api-test
        ;;
    company-car-build)
        company-car-build
        ;;
    company-car-deploy)
        company-car-deploy
        ;;
    *)
        display-usage
        exit -1
esac