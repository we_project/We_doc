import * as express from 'express';
import { NextFunction, Request, Response } from 'express';
import { setupBodyParser } from './config/config-body-parser';
import { setupServer } from './config/config-server';
import { setupApiRoute } from './config/config-api-route';
import { setupStaticResource } from './config/config-static-resource';
import { logError } from './log-writter/logger';

export const createWebApp = () => {
    const app = express();

    setupBodyParser(app);

    setupServer(app);

    setupApiRoute(app);

    setupStaticResource(app);

    app.use((err: any, req: Request, res: Response, next: NextFunction) => {
        logError(err);
        next(err);
    });
    app.use((err: any, req: Request, res: Response, next: NextFunction) => {
        res.status(500).json(err).send();
    });

    return app;
};


