export const encryptPassword = (password: string): string => {
  return Buffer.from(password).toString('base64').replace(/%([0-9A-F]{2})/g, (match, p1) => String.fromCharCode(Number(`0x${p1}`)));
};
