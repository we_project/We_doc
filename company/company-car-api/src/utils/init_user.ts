import { encryptPassword } from './password-utils';
import { User } from './../models/User';
import { config } from 'dotenv';
import * as mongoose from 'mongoose';

interface User {
  loginId?: string;
  password?: string;
  fullName: string;
  empId: string;
  empName: string;
  dcPayId: string;
  role: string;
}

const userJson: User = require('../../data/init_user.json');

config();

mongoose.connect(process.env.MONGODB_URI, async () => {
  userJson.password = encryptPassword(userJson.password);
  const user = new User({
    loginId: userJson.loginId,
    password: userJson.password,
    fullName: userJson.fullName,
    empId: userJson.empId,
    empName: userJson.empName,
    dcPayId: userJson.dcPayId,
    role: userJson.role
  });

  await user.save();

  mongoose.disconnect(() => {
    console.log('disconnected');
  });
});