import { User, UserModel } from '../models/User';

export const mockUser = (id: string): UserModel => {
    return new User({
        loginId: 'test',
        password: 'test',
        empName: 'AA',
        empId: '1234',
        dcPayId: id,
        role: ''
    });
};