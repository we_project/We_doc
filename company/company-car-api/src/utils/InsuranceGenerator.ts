import { config } from 'dotenv';
import * as mongoose from 'mongoose';
import { Vehicle, VehicleModel } from '../models/Vehicle';
import { Insurance } from '../models/Insurance';
import * as _ from 'lodash';



interface IInsurance {
  plateNumber: string;
  lnsur_End: Date;
  lnsur_accident_tel: string;
  roadside_assistance: string;
  insurance_company: string;
  annual_inspect_due_date: Date;
}


const insurances: IInsurance[] = require('../../data/insurance.json');
const vehicles: VehicleModel[] = require('../../data/vehicles.json');

config();

mongoose.connect(process.env.MONGODB_URI, function () {
  let done = 0;
/*  insurances.forEach(async (i) => {
    const vehicle = new Vehicle(_.find<VehicleModel>(vehicles, v => v.legacyPlateNo == i.plateNumber));
    const insurance = new Insurance({
      insuranceEndDate: i.lnsur_End,
      insuranceAccidentTel: i.lnsur_accident_tel,
      roadsideAssistance: i.roadside_assistance,
      insuranceCompany: i.insurance_company,
      annualInspectionDueDate: i.annual_inspect_due_date
    });
    await insurance.save();
    vehicle.insurance = insurance;
    await vehicle.save();
    done++;
    if (done === insurances.length) {
      mongoose.disconnect(() => {
        console.log(done);
        console.log('disconnected');
      });
    }
  });*/


  vehicles.forEach(async(v) => {
    const vehicle = new Vehicle(v);
    const get_insurance = _.find<IInsurance>(insurances, i => i.plateNumber == vehicle.legacyPlateNo);
    if (get_insurance) {
      const insurance = new Insurance({
        insuranceEndDate: get_insurance.lnsur_End,
        insuranceAccidentTel: get_insurance.lnsur_accident_tel,
        roadsideAssistance: get_insurance.roadside_assistance,
        insuranceCompany: get_insurance.insurance_company,
        annualInspectionDueDate: get_insurance.annual_inspect_due_date
      });
      await insurance.save();
      vehicle.insurance = insurance;
    } else {
      vehicle.insurance = null;
    }

    await vehicle.save();
    done++;

    if (done === insurances.length) {
      mongoose.disconnect(() => {
        console.log(done);
        console.log('disconnected');
      });
    }
  });

});