import { config } from 'dotenv';
import * as mongoose from 'mongoose';
import { Vehicle, VehicleModel } from '../models/Vehicle';
import { Insurance } from '../models/Insurance';
import * as _ from 'lodash';



interface IInsurance {
  plateNumber: string;
  lnsur_End: Date;
  lnsur_accident_tel: string;
  roadside_assistance: string;
  insurance_company: string;
  annual_inspect_due_date: Date;
}


const insurances: IInsurance[] = require('../../data/insuranceMock.json');
const vehicles: VehicleModel[] = require('../../data/vehiclesMock.json');

config();

mongoose.connect(process.env.MONGODB_URI, function () {
  let done = 0;
  insurances.forEach(async (i) => {
    const vehicle = new Vehicle(_.find<VehicleModel>(vehicles, v => v.legacyPlateNo == i.plateNumber));
    const insurance = new Insurance({
      insuranceEndDate: i.lnsur_End,
      insuranceAccidentTel: i.lnsur_accident_tel,
      roadsideAssistance: i.roadside_assistance,
      insuranceCompany: i.insurance_company,
      annualInspectionDueDate: i.annual_inspect_due_date
    });
    await insurance.save();
    vehicle.insurance = insurance;
    await vehicle.save();
    done++;
    if (done === insurances.length) {
      mongoose.disconnect(() => {
        console.log(done);
        console.log('disconnected');
      });
    }
  });
});