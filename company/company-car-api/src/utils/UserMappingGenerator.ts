import { config } from 'dotenv';
import * as Reader from 'line-by-line';
import * as mongoose from 'mongoose';
import { UserMapping } from '../models/UserMapping';

config();

const userMappingFiles: Array<string> = ['data/20170926-195030-CHN_PERSON.TXT'];
const filedArrayObject: Array<string> = [
    'DC_PAY_ID',
    'DC_BUSN_PHONE',
    'DC_CELL_PHONE',
    'DC_PVT_CELL_PHONE',
    'EXTENSION',
    'EMAIL_ADDR',
    'EMPLID',
    'NAME_DISPLAY',
];
const indexInObject: any = {};

function mapDataByKeys(data: any, key: string): any {
    const index = indexInObject[key];
    return data[index];
}

userMappingFiles.forEach(file => {
    generateUserMappingsForFile(file);
});

function generateUserMappingsForFile(file: string) {
    mongoose.connect(process.env.MONGODB_URI, function (err: Error) {
        const reader = new Reader(file);

        reader.on('error', function (err) {
            // 'err' contains error object
        });

        reader.on('line', function (line) {
            // 'line' contains the current line without the trailing newline character.
            const columns: Array<string> = line.split('|');

            if (indexInObject['DC_PAY_ID'] && indexInObject['EMPLID']) {
                if (columns.length > 3 && mapDataByKeys(columns, 'DC_PAY_ID') != ' ') {
                    new UserMapping({
                        empId: mapDataByKeys(columns, 'EMPLID'),
                        empName: mapDataByKeys(columns, 'NAME_DISPLAY'),
                        dcPayId: mapDataByKeys(columns, 'DC_PAY_ID'),
                        email: mapDataByKeys(columns, 'EMAIL_ADDR'),
                        mobilePhone: mapDataByKeys(columns, 'DC_CELL_PHONE'),
                        businessPhone: mapDataByKeys(columns, 'DC_BUSN_PHONE'),
                    }).save((err) => {
                        if (err) {
                            console.log(err);
                        }
                    });
                }
            } else {
                for (let i = 0; i < columns.length; i++) {
                    const column: string = columns[i].trim();
                    if (filedArrayObject.includes(column)) {
                        indexInObject[column] = i;
                    }
                }
            }
        });

        reader.on('end', function () {
            mongoose.disconnect();
        });

        console.log(err);
    });
}
