import * as log4js from 'log4js';

const errorLogger = log4js.getLogger('error');
export const logError = (error: any, meesage?: string) => {
    errorLogger.error(meesage || '', error);
};

const warnLogger = log4js.getLogger('warn');
export const logWarn = (error: any, meesage?: string) => {
    warnLogger.warn(meesage || ''
        , error);
};