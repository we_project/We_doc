import { VehicleStock } from '../../models/VehicleStock';
import { UserModel } from '../../models/User';

export class VehicleStockDetail {
    signature: string;
    plateNo: string;
    model: string;
    vin: string;
    provincialCapital: string;
    plateNumber: string;
    location: {
        latitude: number;
        longitude: number;
    };
    carIsMobileAndInUse: string;
    exteriorColour: string;
    interiorColour: string;
    leftFrontImage: {
        id: string;
    };
    rightBackImage: {
        id: string;
    };
    licenseImage: {
        id: string
    };
    warningTriangleInCar: string;
    fireExtinguisher: string;
    floorMat: string;
    mileage: string;
    damages: {
        damageLevel: string,
        damageImages: [{
            site: string,
            id: string,
        }]
    };
    odometerImage: {
        id: string
    };

    convertToDomain = () => {
        return new VehicleStock({
            model: this.model,
            vin: this.vin,
            provincialCapital: this.provincialCapital,
            location: {
                latitude: this.location.latitude,
                longitude: this.location.longitude
            },
            carIsMobileAndInUse: this.carIsMobileAndInUse,
            locationPosition: undefined,
            exteriorColour: this.exteriorColour,
            interiorColour: this.interiorColour,
            leftFrontImage: {
                id: this.leftFrontImage.id
            },
            rightBackImage: {
                id: this.rightBackImage.id
            },
            licenseImage: {
                id: this.licenseImage.id
            },
            warningTriangleInCar: this.warningTriangleInCar,
            fireExtinguisher: this.fireExtinguisher,
            floorMat: this.floorMat,
            mileage: this.mileage,
            odometerImage: {
                id: this.odometerImage.id
            },
            damages: {
                damageLevel: this.damages.damageLevel,
                damageImages: this.damages.damageImages.map((damageImage) => {
                    return {site: damageImage.site, imageId: damageImage.id};
                }),
            },
            signature: this.signature,
            commitTime: Date.now(),
        });
    };
}
