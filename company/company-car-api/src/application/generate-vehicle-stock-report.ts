import { Vehicle } from '../models/Vehicle';
import { VehicleQueryConditionBuilder } from '../models/vehicle-query-condition-builder';
import { VehicleStockFinialReport } from '../models/vehicle-stock-finial-report';
import { IWorkSheet, readFile, utils, write } from 'xlsx-style';
import { isBoolean, isDate, isNullOrUndefined, isNumber } from 'util';
import {_ as otherMatch, match } from 'kasai';
import * as moment from 'moment';
import { getCell, writeToCell } from '../external-service/xls-wrapper';


const TEMPLATE_PATH = 'report-template/company_car_management_report_sample.xlsx';
const vehicleSystemRecordColumnStartIndex = 0;
const vehicleSystemRecordColumnEndIndex = 11;
const vehicleSystemRecordRowStartIndex = 1;
const vehicleSystemRecordRowEndIndex = 2;
const vehicleSystemRecord = 'D9D9D9';

export const generateVehicleStockReport = async () => {

    const vehicleStockFinialReports = (await
        Vehicle.find(new VehicleQueryConditionBuilder().result())
        .populate({'path': 'stockRecord', options: { sort: { 'commitTime': -1 } }})
            .sort({ 'stockApprovedTime': -1, 'plateNo': -1, 'legacyPlateNo': -1 }))
        .map((vehicle, index) => {
            return new VehicleStockFinialReport(vehicle, vehicle.stockRecord, index + 1);
        });

    const wb = readFile(TEMPLATE_PATH, {type: 'file', cellStyles: true});
    const detailSheet = wb.Sheets[wb.SheetNames[0]];

    reFullVehicleSystemRecordLabelToGray(detailSheet);

    vehicleStockFinialReports.forEach((stock, resultIndex) => {
        stock.results.forEach((result, colIndex) => {

            const dataCellRowIndex = getDataCellRowIndex(resultIndex);
            writeToCell(dataCellRowIndex, colIndex, detailSheet, result);
        });
    });

    return write(wb, {type: 'binary' });
};

const reFullVehicleSystemRecordLabelToGray = (sheet: IWorkSheet) => {
    for (let rowIndex = vehicleSystemRecordRowStartIndex; rowIndex <= vehicleSystemRecordRowEndIndex; rowIndex = rowIndex + 1) {
        for (let colIndex = vehicleSystemRecordColumnStartIndex; colIndex <= vehicleSystemRecordColumnEndIndex; colIndex = colIndex + 1) {
            const cell = getCell(rowIndex, colIndex, sheet);
            cell.s.fill = {
                fgColor: { rgb: vehicleSystemRecord }
            };
        }
    }
};


const getDataCellRowIndex = (dataIndex: number) => {
    const labelOffset = vehicleSystemRecordRowEndIndex + 1;
    return dataIndex + labelOffset ;
};

