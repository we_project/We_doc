import { VehicleStock, IAddress } from '../models/VehicleStock';
import { isNullOrUndefined } from 'util';
import { queryAddress } from '../external-service/baidu-map-geo-locate-service';

export const completeTheStockAddress = async () => {
    const unAccessedStocks = await VehicleStock.find({locationPosition: null}).limit(10);
    await Promise.all(unAccessedStocks.map(async (unAccessedStock) => {
        const address: IAddress = await queryAddress(
            unAccessedStock.location.latitude,
            unAccessedStock.location.longitude);

        const stock = await VehicleStock.findById(unAccessedStock.id);

        if (isNullOrUndefined(stock.locationPosition) || isNullOrUndefined(stock.locationPosition.province)) {
            stock.locationPosition = address;
            await stock.save();
        }
    }));
};