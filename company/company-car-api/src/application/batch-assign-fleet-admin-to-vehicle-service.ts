import { Vehicle, VehicleModel } from '../models/Vehicle';
import { AssigneeUser } from '../models/AssigneeUser';

export const batchAssignFleetAdminToVehicle = async (assigneeTasks: [{vehicleId: string, assignee: AssigneeUser}] ) => {
    const vehicleIds = assigneeTasks.map(task => task.vehicleId);
    const vehicles = await Vehicle.find({_id: {$in: vehicleIds}});
    await Promise.all(assigneeTasks.map(async(task) => {
        const vehicle = vehicles.find((ve) => { return ve.id === task.vehicleId; });
        if (!!vehicle) {
            vehicle.assignToFleetAdmin(task.assignee);
            await vehicle.save();
        }

    }));
};