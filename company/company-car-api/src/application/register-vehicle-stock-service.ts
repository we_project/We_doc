import { AnnualCheckOverdue, InsuranceStickerValid, VehicleStock } from '../models/VehicleStock';
import { Vehicle, VehicleModel } from '../models/Vehicle';
import { isNullOrUndefined } from 'util';
import { VehicleStockDetail } from './dto/VehicleStockDetail';
import { UserModel } from '../models/User';
import { UserRole } from '../models/user-role';
import { UserMapping } from '../models/UserMapping';

export const vehicleStockHasSubmittedException = 'HasSubmitted';
export const vehicleStockNoVehicleException = 'vehicle not exits';
export const vehicleStockNoPermissionException = 'Forbidden';

export const registerVehicleStock =
    async (vehicleId: String, stockDetail: VehicleStockDetail, currentUser: UserModel)
    : Promise<VehicleStock> => {

    const vehicle: VehicleModel = await Vehicle.findById(vehicleId).populate('insurance');

    if (isNullOrUndefined(vehicle)) {
        throw vehicleStockNoVehicleException;
    }

    if (!UserRole.IsSuperAdminRole(currentUser) && !UserRole.IsFleetAdminRole(currentUser)) {
      if (invalidAuthorization(currentUser, vehicle)) {
            throw vehicleStockNoPermissionException;
      }
    }

    if (vehicle.isSubmitStocked()) {
        throw vehicleStockHasSubmittedException;
    }

    const stock = stockDetail.convertToDomain();

    stock.insuranceStickerValid = !!vehicle.insurance && vehicle.insurance.insuranceEndDate >= new Date(stock.commitTime) ? InsuranceStickerValid.valid : InsuranceStickerValid.invalid;
    stock.annualCheckOverdue = !vehicle.insurance || vehicle.insurance.annualInspectionDueDate < new Date(stock.commitTime) ? AnnualCheckOverdue.overdue : AnnualCheckOverdue.schedule;

    setSubmitter(stock, currentUser);

    await setVehicleUser(vehicle.user.CNNumber, stock);

    const updatedStock = await stock.save();
    vehicle.registerStockRecord(updatedStock);
    await vehicle.save();

    return updatedStock;
};

const setSubmitter = (stock: VehicleStock, currentUser: UserModel) => {
    stock.submitter.submitterId = currentUser.loginId;
    stock.submitter.submitterFullName = currentUser.fullName;
    stock.submitter.submitterRole = currentUser.role;
};

const setVehicleUser = async (cnNumber: string, stock: VehicleStock) => {
    if (cnNumber) {
        const userMapping = await UserMapping.findOne({'dcPayId': cnNumber});
        if (userMapping) {
          stock.vehicleUser.empId = userMapping.empId;
          stock.vehicleUser.empName = userMapping.empName;
          stock.vehicleUser.dcPayId = userMapping.dcPayId;
        } else {
          stock.vehicleUser.dcPayId = cnNumber;
        }
    }
};

const invalidAuthorization = (currentUser: UserModel, vehicle: VehicleModel): boolean => {
    return !isExternalDelegate(currentUser, vehicle)
          && !isVehicleOwner(currentUser, vehicle)
          && !isInternalDelegate(currentUser, vehicle);
};

const isExternalDelegate = (currentUser: UserModel, vehicle: VehicleModel) => {
    return !!vehicle.externalDelegate.loginId && vehicle.externalDelegate.loginId === currentUser.loginId;
};

const isVehicleOwner = (currentUser: UserModel, vehicle: VehicleModel) => {
    return !!vehicle.user.CNNumber && !!currentUser.dcPayId && vehicle.user.CNNumber === currentUser.dcPayId;
};

const isInternalDelegate = (currentUser: UserModel, vehicle: VehicleModel) => {
    return vehicle.internalDelegate.empId === currentUser.empId;
};
