import { UserModel } from '../models/User';
import { UserRole } from '../models/user-role';
import { ExamineTypes, VehicleStock } from '../models/VehicleStock';
import { Vehicle } from '../models/Vehicle';
import { NotExistsStockErrorMessage, OnlySystemAdminToReject } from './reject-vehicle-stock-status';
import { VehicleQueryConditionBuilder } from '../models/vehicle-query-condition-builder';

export const approveVehicleStockStatus = async (stockId: string, currentUser: UserModel) => {
    if (!UserRole.IsSuperAdminRole(currentUser)) {
        throw new Error(OnlySystemAdminToReject);
    }

    const stock = await VehicleStock.findById(stockId);

    if (stock == null) {
        throw new Error(NotExistsStockErrorMessage);
    }

    const approvalTime = Date.now();
    stock.examine({
        examineTime: approvalTime,
        userId: currentUser.loginId,
        role: currentUser.role,
        examineType: ExamineTypes.Approval
    });

    await stock.save();

    const vehicle = await Vehicle.findOne(new VehicleQueryConditionBuilder().appendWithVin(stock.vin).result());

    if (vehicle == null) {
        return;
    }

    vehicle.approveStockRecord(approvalTime);
    await vehicle.save();
};