import { reject } from './../controllers/vehicle-stock-detail';
import { VehicleStockBatchReject } from './../controllers/dto/VehicleStockBatchReject';
import { ExamineTypes, VehicleStock } from '../models/VehicleStock';
import { Vehicle } from '../models/Vehicle';
import { UserModel } from '../models/User';
import { UserRole } from '../models/user-role';
import { VehicleQueryConditionBuilder } from '../models/vehicle-query-condition-builder';

export const NotExistsStockErrorMessage = 'stock not exists';
export const OnlySystemAdminToReject = 'only system admin could reject the stock';


export const rejectVehicleStockStatus = async (currentUser: UserModel, rejectionStockId: string, rejectionComment = '') => {
    if (!UserRole.IsSuperAdminRole(currentUser)) {
        throw new Error(OnlySystemAdminToReject);
    }

    const stock = await VehicleStock.findById(rejectionStockId);

    if (stock == null) {
        throw new Error(NotExistsStockErrorMessage);
    }

    stock.examine({examineTime: Date.now(),
        userId: currentUser.loginId,
        role: currentUser.role,
        examineType: ExamineTypes.Rejection,
        comment: rejectionComment});

    await stock.save();

    const vehicle = await Vehicle.findOne(new VehicleQueryConditionBuilder().appendWithVin(stock.vin).result());

    if (vehicle == null) {
        return;
    }

    vehicle.clearStockRecord();
    await vehicle.save();
};
