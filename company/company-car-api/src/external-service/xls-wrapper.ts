import { IWorkSheet, readFile, utils, write } from 'xlsx-style';
import { isBoolean, isDate, isNullOrUndefined, isNumber } from 'util';
import {_ as otherMatch, match } from 'kasai';
import * as moment from 'moment';

export const getCell = function (rowIndex: number, colIndex: number, sheet: IWorkSheet) {
    const cellRef = utils.encode_cell({r: rowIndex, c: colIndex});
    let cell = sheet[cellRef];

    if (isNullOrUndefined(cell)) {
        cell = sheet[cellRef] = {};
    }

    return cell;
};


export const writeToCell = function (rowIndex: number, columnIndex: number, sheet: IWorkSheet, value: any) {
    const cell = getCell(rowIndex, columnIndex, sheet);
    const {cellType, cellValue, cellFormat} = getCellTypedValue(value);
    cell.v = cellValue;
    cell.t = cellType;
    cell.s = cellFormat;
};
export const borderStyle = { style: 'thin', color: { rgb: '000000'}};
export const commonDataCellStyle = Object.assign({},  {
    border: {
        top: borderStyle,
        left: borderStyle,
        right: borderStyle,
        bottom: borderStyle,
    },
    font: {
        name: 'DengXian'
    }
});

export const numberDataCellStyle = Object.assign({}, commonDataCellStyle, {numFmt: '0'});

export const getCellTypedValue = (cellValue: any) => {
    return match(cellValue, [
        [isBoolean, (x: boolean) => { return { cellType: 'b', cellValue: x, cellFormat: commonDataCellStyle }; }],
        [isNumber, (x: number) => { return { cellType: 'n', cellValue: x , cellFormat: numberDataCellStyle}; }],
        [isDate,  (x: Date) => { return { cellType: 'd', cellValue:  exactTheDateWithTimeZoneToExcelLocalDate(x), cellFormat: commonDataCellStyle }; }],
        [otherMatch, (x: any) => { return { cellType: 's', cellValue:  x, cellFormat: commonDataCellStyle}; } ]
    ]);
};

export const exactTheDateWithTimeZoneToExcelLocalDate = (date: Date) => {
    return moment(date).add(8, 'h').toDate().toISOString();
};