import { RequestResponse } from 'request';
const request = require('request');

export const queryAddress = (latitude: number, longitude: number) => {
    const apiKey = 'dkdP7sq755ZGtjtmnpw9WLvaUY1jxst4';
    const locationApi = `https://api.map.baidu.com/geocoder/v2/?location=${latitude},${longitude}&output=json&pois=1&ak=${apiKey}&coordtype=wgs84ll`;
    return new Promise<{province: string, city: string, district: string, detail: string}>((resolve, reject) => {
        request(locationApi, (error: any, response: RequestResponse, body: any) => {
            if (!error && response.statusCode == 200) {
                const locationJson = JSON.parse(body);
                resolve( {
                    province: locationJson.result.addressComponent.province,
                    city: locationJson.result.addressComponent.city,
                    district: locationJson.result.addressComponent.district,
                    detail: locationJson.result.formatted_address
                });
            } else {
                reject(error.message);
            }
        });
    });
};
