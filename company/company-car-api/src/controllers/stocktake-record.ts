import { RequestWithCurrentUser } from '../midwares/authModels';
import { Response, NextFunction } from 'express';
import { AnnualCheckOverdue, InsuranceStickerValid, VehicleStock } from '../models/VehicleStock';
import { Vehicle } from '../models/Vehicle';

export let getStocktakeRecordsByPage = async (req: RequestWithCurrentUser, res: Response, next: NextFunction) => {
    try {
        const pageSize = parseInt(req.query['pageSize']);
        const pageNumber = parseInt(req.query['pageNumber']);
        const recordCount = await VehicleStock.find().count();
        const stockList = await  VehicleStock.paginate({}, {sort: {commitTime: 'desc'}, page: pageNumber, limit: pageSize});

        const dtos = await Promise.all(stockList.docs.map(async s => {
            const vehicle = await Vehicle.findOne({VIN: s.vin}).populate('insurance');

            return {
                stockId: s.id,
                commitTime: s.commitTime,
                examineStatusType: s.examineStatus.examineType,
                rejectComment: s.examineStatus.comment,
                plateNo: vehicle.plateNo,
                vin: s.vin,
                make: vehicle.carType.make,
                model: vehicle.carType.model,
                usage: vehicle.usage,
                carIsMobileAndInUse: s.carIsMobileAndInUse,
                province: s.locationPosition.province,
                city: s.locationPosition.city,
                zone: s.locationPosition.district,
                detailAddress: s.locationPosition.detail,
                exteriorColour: s.exteriorColour,
                interiorColour: s.interiorColour,
                insuranceValid: s.insuranceStickerValid == InsuranceStickerValid.valid,
                annualCheckOverdue: s.annualCheckOverdue == AnnualCheckOverdue.overdue,
                warningTriangle: s.warningTriangleInCar,
                fireExtinguisher: s.fireExtinguisher,
                floorMats: s.floorMat,
                mileage: s.mileage,
                carIsMobileAndUsable: s.carIsMobileAndInUse,
                leftFrontImage: s.leftFrontImage.id,
                rightBackImage: s.rightBackImage.id,
                vehicleLicenseImage: s.licenseImage.id,
                odometerImage: s.odometerImage.id,
                damageLevel: s.damages.damageLevel,
                damagePhotos: s.damages.damageImages,
                submitterFullName: s.submitter.submitterFullName,
                submitterRole: s.submitter.submitterRole,
                vehicleUserName: setVehicleUserName(s.vehicleUser)
            };
        }));

        res.send({recordCount: recordCount, data: dtos});
    }
    catch (err) {
        err.status = 500;
        next(err);
    }
};

const setVehicleUserName = (vehicleUser: any): string => {
    if (vehicleUser.dcPayId) {
        if (vehicleUser.empName) {
            return vehicleUser.empName;
        } else {
            return vehicleUser.dcPayId;
        }
    } else {
        return null;
    }
};