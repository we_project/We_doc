import { Response, NextFunction } from 'express';
import { RequestWithCurrentUser } from '../midwares/authModels';
import { Vehicle, VehicleModel } from '../models/Vehicle';
import { VehicleListItem } from './dto/VehicleListItem';
import { VehicleDetail } from './dto/VehicleDetail';
import { Insurance } from '../models/Insurance';
import { UserRole } from '../models/user-role';
import { VehicleSearchParamsExtractor } from './request-query-handler/vehicle-search-params-extractor';
import { VehicleQueryConditionTransformer } from '../models/utils/vehicle-query-condition-transformer';
import { UserMapping } from '../models/UserMapping';

export let queryVehiclesByEmployee = async (req: RequestWithCurrentUser, res: Response, next: NextFunction) => {
  try {
      const cnNumber = req.currentUser.dcPayId;
      const empId = req.currentUser.empId;

      const conditions: Array<Object> = [];
      if (cnNumber) {
          const ownerCondition = new VehicleQueryConditionTransformer()
              .combineWithVehicleOwnerCNNumber(cnNumber).result();
          conditions.push(ownerCondition);
      }
      const internalDelegateCondition = new VehicleQueryConditionTransformer().combineWithInternalDelegate(empId)
          .result();
      conditions.push(internalDelegateCondition);

      const result: Array<VehicleModel> = (await Vehicle.find({$or: conditions}))
          .sort((a, b) => a.user.CNNumber === cnNumber ? -1 : 1);

      const resultList: Array<VehicleListItem> =   await Promise.all(result.map(async v => {
          try {
              let delegateTo: string;
              let delegateFrom: string;

              if (v.user.CNNumber) {
                  delegateTo = v.user.CNNumber === req.currentUser.dcPayId ? v.internalDelegate.empId : null;
                  if (delegateTo) {
                      const userMappingModel = await UserMapping.find({'empId': delegateTo});
                      delegateTo = userMappingModel.length > 0 ? userMappingModel[0].empName : delegateTo;
                  }

                  if (v.user.CNNumber !== req.currentUser.dcPayId) {
                      delegateFrom = v.user.CNNumber;
                      delegateFrom = (await UserMapping.find({'dcPayId': delegateFrom}))[0].empName;
                  } else {
                      delegateFrom = null;
                  }
              } else {
                  delegateFrom = v.usage;
              }

              return {
                  id: v._id,
                  plateNo: v.plateNo,
                  legacyPlateNo: v.legacyPlateNo,
                  stockStatus: v.getStockStatus(),
                  usage: v.usage,
                  model: v.carType.model,
                  delegateTo: delegateTo,
                  delegateFrom: delegateFrom,
              };
          } catch (error) {
              console.log(error);
          }
      }));

      res.send(resultList);
  } catch (err) {
      err.status = 500;
      next(err);
  }
};


export let queryAllVehiclesByFleetAdmin = async (req: RequestWithCurrentUser, res: Response, next: NextFunction) => {
    try {
        const paramsExtractor = new VehicleSearchParamsExtractor(req.query);

        const pageSize: number = paramsExtractor.extractToPageSize();
        const pageNumber: number = paramsExtractor.extractToPageNumber();
        const isAssignToMe: boolean = paramsExtractor.extractToIsAssignWithMe();
        const counted: boolean = paramsExtractor.extractToCounted();

        const role = req.currentUser.role;

        let queryCondition;
        if (role === UserRole.ExternalDelegateRoleString) {
            queryCondition = new VehicleQueryConditionTransformer()
                .combineWithExternalDelegate(req.currentUser.loginId)
                .result();
        } else {
            queryCondition = new VehicleQueryConditionTransformer()
                .combineWithIsAssignToMe(isAssignToMe, req.currentUser)
                .combineWithCounted(counted)
                .result();
        }

        const list = await Vehicle.paginate(
            queryCondition
            , {sort: {stockApprovedTime: 'asc', stockRecord: 'asc', legacyPlateNo: 'asc'}
                , page: pageNumber, limit: pageSize});

        const result = list.docs.map(l => {
            return {
                id: l._id,
                plateNo: l.plateNo,
                legacyPlateNo: l.legacyPlateNo,
                stockStatus: l.getStockStatus(),
                usage: l.usage,
                model: l.carType.model,
            };
        });

        res.send({totalRecordCount: list.total, data: result});
    } catch (err) {
        err.status = 500;
        next(err);
    }
};

export let vehicleDetail = async (req: RequestWithCurrentUser, res: Response, next: NextFunction) => {
  try {
      const vehicleId: String = req.params.id;

      const vehicle: VehicleModel = await Vehicle.findById(vehicleId);

      const userDetail = await UserMapping.findOne({dcPayId: vehicle.user.CNNumber}).exec();
      const insurance = await Insurance.findOne({ _id: vehicle.insurance }).select('-_id -__v').exec();
      const vehicleDetail: VehicleDetail = {
          id: vehicle.id,
          vin: vehicle.VIN,
          engineNo: vehicle.engineNo,
          plateNo: vehicle.plateNo,
          legacyPlateNo: vehicle.legacyPlateNo,
          stockStatus: vehicle.getStockStatus() ,
          usage: 'Employ Entitled',
          model: vehicle.carType.model,
          registerUnder: vehicle.registrationCompany,
          registerArea: vehicle.registrationLocation,
          registerDate: vehicle.registrationDate ? vehicle.registrationDate.toDateString() : '',
          insurance: insurance && {
              insuranceEndDate: insurance.insuranceEndDate ? insurance.insuranceEndDate.toDateString() : '',
              insuranceAccidentTel: insurance.insuranceAccidentTel,
              roadsideAssistance: insurance.roadsideAssistance,
              insuranceCompany: insurance.insuranceCompany,
              annualInspectionDueDate: insurance.annualInspectionDueDate ? insurance.annualInspectionDueDate.toDateString() : '',
          },
          userDetail: userDetail,
          userCompany: vehicle.userCompany,
          businessCarUserCompany: vehicle.businessCarUserCompany,
      };

      res.send(vehicleDetail);
  } catch (err) {
      err.status = 500;
      next(err);
  }
};

export let search = async (req: RequestWithCurrentUser, res: Response , next: NextFunction) => {
  try {

      const paramsExtractor = new VehicleSearchParamsExtractor(req.query);
      const plateNoOrVIN = paramsExtractor.extractToPlateNoOrVIN();
      const isAssignToMe: boolean = paramsExtractor.extractToIsAssignWithMe();

      const isFuzzy = paramsExtractor.isFuzzySearch();
      if (UserRole.IsExternalDelegateRole(req.currentUser)) {
          res.send([]);
          return;
      }

      if (!plateNoOrVIN) {
          res.send([]);
          return;
      }

      const condition = new VehicleQueryConditionTransformer()
          .combineWithPlateNoOrVIN(plateNoOrVIN, isFuzzy)
          .combineWithIsAssignToMe(isAssignToMe, req.currentUser)
          .result();

      const queryResult = await Vehicle.find(condition);

      res.send(queryResult.map(q => {
          return {
              id: q._id,
              plateNo: q.plateNo,
              legacyPlateNo: q.legacyPlateNo,
              stockStatus: q.getStockStatus(),
              usage: q.usage,
              model: q.carType.model,
              externalDelegate: {
                  loginId: q.externalDelegate.loginId
              },
              internalDelegate: {
                  empId: q.internalDelegate.empId
              },
              vin: q.VIN
          };
      }));
  }  catch (err) {
      err.status = 500;
      next(err);
  }

};