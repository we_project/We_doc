import { Request, Response } from 'express';
import { User, UserModel } from '../models/User';
import { UserRole } from '../models/user-role';
import { NextFunction } from 'express-serve-static-core';
import { isNullOrUndefined } from 'util';

export let getFleetAdminList = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const fleetAdmins: UserModel[] = await User.find({'role': UserRole.FleetAdminRole}).sort('fullName');

        res.status(200).json(fleetAdmins.map((admin) => {
            return {id: admin.id,
                loginId: admin.loginId,
                fullName: isNullOrUndefined(admin.fullName) ? '' : admin.fullName,
                role: admin.role
            };
        }));
    }
    catch (err) {
        err.status = 500;
        next(err);
    }

};