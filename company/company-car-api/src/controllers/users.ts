import { Request, Response } from 'express';
import { User, UserModel } from '../models/User';
import { logError } from '../log-writter/logger';


export let listUsers = async(req: Request, res: Response) => {
  const users: UserModel[] = await User.where('loginId').ne(undefined).where('password').ne(undefined).select('loginId fullName role mobile').exec();
  res.json(users.map(u => {
    return {loginId: u.loginId, fullName: u.fullName, id: u._id, role: u.role, mobile: u.mobile};
  }));
};

export let newUser = async(req: Request, res: Response) => {
  const user: UserModel = req.body;
  try {
    const userModel: UserModel = await new User(user).save();
    res.json({ loginId: userModel.loginId });
  } catch (err) {
    logError(err);
    res.status(400).json({ error: err});
  }
};

export let deleteUser = async(req: Request, res: Response) => {
  try {
    await User.remove({_id: req.params.id}).exec();
    res.json({message: 'deleted'});
  } catch (err) {
    logError(err);
    res.status(404).json({ error: err});
  }
};
