import { RequestWithCurrentUser } from '../midwares/authModels';
import { Vehicle } from '../models/Vehicle';
import { NextFunction, Response } from 'express';
import { AssigneeUser } from '../models/AssigneeUser';
import { batchAssignFleetAdminToVehicle } from '../application/batch-assign-fleet-admin-to-vehicle-service';

export let todoList = async (req: RequestWithCurrentUser, res: Response, next: NextFunction) => {
    try {
        const pageSize = parseInt(req.query['page-size']);
        const pageNumber = parseInt(req.query['page-number']);
        const searchContents = req.query['q'];

        const searchConditions = [{'plateNo': new RegExp(searchContents, 'i')},
            {'legacyPlateNo': new RegExp(searchContents, 'i')},
            {'VIN': new RegExp(searchContents, 'i')},
            {'assignee.fullName': new RegExp(searchContents, 'i')}];

        const vehicleAssigneesPaginate = await Vehicle.paginate({$or: searchConditions},
            {sort: {legacyPlateNo: 'asc'}, page: pageNumber, limit: pageSize});

        const vehicleTasks = vehicleAssigneesPaginate.docs;
        const datas = vehicleTasks.map( (vehicleDelegation) => {
            return {
                id: vehicleDelegation.id,
                plateNumber: vehicleDelegation.legacyPlateNo,
                vin: vehicleDelegation.VIN,
                make: vehicleDelegation.carType.make,
                model: vehicleDelegation.carType.model,
                usage: vehicleDelegation.usage,
                counted: vehicleDelegation.getStockStatus(),
                assignee: !!vehicleDelegation.assignee ? vehicleDelegation.assignee : null
            };

        });
        res.send({datas: datas, vehicleDelegationTotalCount: vehicleAssigneesPaginate.total});
    } catch (err) {
        next(err);
    }
};


export let batchUpdate = async(req: RequestWithCurrentUser, res: Response, next: NextFunction) => {
    try {
        const taskParams: [{vehicleId: string, assignee: AssigneeUser}] = req.body;
        await batchAssignFleetAdminToVehicle(taskParams);
        res.status(201).json('successfully');
    } catch (err) {
        next(err);
    }
};