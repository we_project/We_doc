import { encryptPassword } from './../utils/password-utils';
import { Request, Response, NextFunction } from 'express';
import { User } from '../models/User';
import { mockUser } from '../utils/Constants';
import { generateJwt } from '../midwares/passportJwt';

export let login = async(req: Request, res: Response, next: NextFunction) => {
  try {
      const loginId = req.body.loginId;
      const password = encryptPassword(req.body.password);

      const user = await User.findOne({loginId: loginId, password: password});

      if (!!user) {
          const userName = user.fullName;
          const role = user.role;

          const payload = { loginId: user.loginId };
          const token = generateJwt(payload);
          res.status(200).json({fullName: userName, token: token, role: role });
          return;
      } else {
          res.status(401).json({ message: 'passwords did not match' });
          return;
      }
    } catch (err) {
      err.status = 500;
      next(err);
  }
};
