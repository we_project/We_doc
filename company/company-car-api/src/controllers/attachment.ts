import { Response, NextFunction } from 'express';
import { RequestWithCurrentUser } from '../midwares/authModels';
import { Attachment, AttachmentModel } from '../models/Attachment';
import * as fs from 'fs';
import { logError } from '../log-writter/logger';

export let upload = async (req: RequestWithCurrentUser, res: Response, next: NextFunction) => {
  try {
      const files: Array<Express.Multer.File> = <Express.Multer.File[]> req.files;

      const items: any[] = await Promise.all(files.map(async (file) => {
          const attachment = {
              filename: file.filename,
              path: file.path,
              encoding: file.encoding,
              mimetype: file.mimetype,
              size: file.size,
              type: 'photo',
              modifiedBy: req.currentUser,
          };

          return {id: (await new Attachment(attachment).save())._id};
      }));

      res.send(items);
  }
  catch (err) {
      err.status = 500;
      next(err);
  }
};

export let show = async (req: RequestWithCurrentUser, res: Response) => {
  try {
    const attachment: AttachmentModel = await Attachment.findById(req.params.id);

    if (!attachment) {
      res.sendStatus(404);
      return;
    }

    res.setHeader('Content-Type', attachment.mimetype);
    fs.createReadStream(attachment.path).pipe(res);
  } catch (err) {
    logError(err);
    res.sendStatus(400);
  }
};