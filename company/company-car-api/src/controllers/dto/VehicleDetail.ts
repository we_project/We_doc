export interface VehicleDetail {
  id: string;
  vin: string;
  engineNo: string;
  plateNo: string;
  legacyPlateNo: string;
  stockStatus: string;
  model: string;
  usage: string;
  registerUnder: string;
  registerDate: string;
  registerArea: string;
  insurance: any;
  userDetail: any;
  userCompany: string;
  businessCarUserCompany: string;
}