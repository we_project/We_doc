export interface InsuranceDetail {
  insuranceEndDate: Date;
  insuranceAccidentTel: string;
  roadsideAssistance: string;
  insuranceCompany: string;
  annualInspectionDueDate: Date;
}