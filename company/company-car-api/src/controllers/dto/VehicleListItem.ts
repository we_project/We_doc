export interface VehicleListItem {
  id: string;
  plateNo: string;
  legacyPlateNo: string;
  stockStatus: string;
  usage: string;
  model: string;
  delegateTo: string;
  delegateFrom: string;
}