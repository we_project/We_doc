export interface StocktakeRecord {
    commitTime: number;
    plateNo: string;
    vin: string;
    make: string;
    model: string;
    usage: string;
    province: string;
    city: string;
    zone: string;
    detaileAddress: string;
    exteriorColor: string;
    interiorColor: string;
    insuranceValid: boolean;
    annualCheckOverdue: boolean;
    warningTriangle: boolean;
    fireExtinguisher: boolean;
    floorMats: boolean;
    mileage: number;
    damageLevel: string;
    carIsMobileAndUsable: boolean;
    leftFrontImage: string;
    rightBackImage: string;
    vehicleLicenseImage: string;
    odometerImage: string;
    damagePhotos: {
        site: string;
        imageId: string
    }[];
}