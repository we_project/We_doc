export interface VehicleStockBatchReject {
  id: string;
  comment: string;
}