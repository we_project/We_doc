import { RequestWithCurrentUser } from '../midwares/authModels';
import { NextFunction, Response } from 'express';
import { generateVehicleStockReport } from '../application/generate-vehicle-stock-report';
import { logWarn } from '../log-writter/logger';
import { isUndefined } from 'util';

export const generateVehicleStockFinialReport = async (req: RequestWithCurrentUser, res: Response, next: NextFunction) => {
    try {
        const reportBinaryData = await generateVehicleStockReport();
        res.setHeader('Content-Type', 'application/vnd.openxmlformates');
        res.setHeader('Content-Disposition', 'attachment;filename=' + 'company_car_management_report.xlsx');
        res.status(200).end(reportBinaryData, 'binary');
    }
    catch (error) {
        next(error);
    }
};