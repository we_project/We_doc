import { User, UserModel } from '../models/User';
import { VehicleSearchParamsExtractor } from './request-query-handler/vehicle-search-params-extractor';
import { ExternalDelegate } from './../models/ExternalDelegate';
import { Response } from 'express';
import { VehicleQueryConditionTransformer } from '../models/utils/vehicle-query-condition-transformer';
import { Vehicle, VehicleModel } from '../models/Vehicle';
import { NextFunction } from 'express';
import { RequestWithCurrentUser } from '../midwares/authModels';
import { UserMapping, UserMappingModel } from '../models/UserMapping';

export let findAllDelegates = async(req: RequestWithCurrentUser, res: Response, next: NextFunction) => {
  const paramsExtractor = new VehicleSearchParamsExtractor(req.query);
  const pageNumber: number = paramsExtractor.extractToPageNumber();
  const pageSize: number = paramsExtractor.extractToPageSize();

  const vehicles = await Vehicle.paginate({$or: [{externalDelegate: {$ne: null}}, {internalDelegate: {$ne: null}}]}, {page: pageNumber, limit: pageSize});

  const result = await Promise.all(vehicles.docs.map(async v => {
    const employee: UserMappingModel = await UserMapping.findOne({empId: v.internalDelegate.empId});
    const internalDelegate: any = {
        empId: v.internalDelegate.empId,
    };
    if (employee) {
      internalDelegate.mobilePhone = employee.mobilePhone;
      internalDelegate.businessPhone = employee.businessPhone;
    }

    const user: UserModel = await User.findOne({loginId: v.externalDelegate.loginId});
    const externalDelegate: any = {
      loginId: v.externalDelegate.loginId,
    };
    if (user) {
      externalDelegate.mobilePhone = user.mobile;
    }

    return {
      plateNo: v.plateNo,
      legacyPlateNo: v.legacyPlateNo,
      vin: v.VIN,
      externalDelegate: externalDelegate,
      internalDelegate: internalDelegate
    };
  }));

  res.send({recordCount: vehicles.total, data: result});
};

export let updateExternalDelegate = async(req: RequestWithCurrentUser, res: Response, next: NextFunction) => {
  try {
    const externalDelegate = req.body.externalDelegate;
    const vehicleStr = req.body.vehicleStr.toUpperCase();

    const vehicles: Array<VehicleModel> = await Vehicle.find(new VehicleQueryConditionTransformer().combineWithPlateNoOrVIN(vehicleStr, false).result());

    vehicles[0].externalDelegate = {loginId: externalDelegate};
    await vehicles[0].save();

    res.sendStatus(201);
  } catch (err) {
    err.status = 500;
    next(err);
  }
};

export let updateInternalDelegate = async(req: RequestWithCurrentUser, res: Response, next: NextFunction) => {
  try {
    const internalDelegate = req.body.internalDelegate;
    const vehicleStr = req.body.vehicleStr.toUpperCase();

    const vehicles: Array<VehicleModel> = await Vehicle.find(new VehicleQueryConditionTransformer().combineWithPlateNoOrVIN(vehicleStr, false).result());

    const users: Array<UserMappingModel> = await UserMapping.find({'empId': internalDelegate});
    if (users.length !== 0) {
        if (vehicles[0].user.CNNumber === users[0].dcPayId) {
          res.status(400).json({'message': 'Delegate and vehicle user could not be same.'});
          return;
        }
    }

    vehicles[0].internalDelegate = {
      'empId': internalDelegate
    };

    await vehicles[0].save();

    res.send(201);
  } catch (err) {
    err.status = 500;
    next(err);
  }
};
