export class VehicleSearchParamsExtractor {
    constructor(private queryParams: any) {
        this.queryParams = queryParams;
    }

    extractToIsAssignWithMe(): boolean {
        if (!this.queryParams['is-assign-to-me']) {
            return null;
        }
        return JSON.parse(this.queryParams['is-assign-to-me']);
    }

    extractToPageSize(): number {
        return parseInt(this.queryParams['pageSize']);
    }

    extractToPageNumber(): number {
        return parseInt(this.queryParams['pageNumber']);
    }

    isFuzzySearch(): boolean {
        const isFuzzy = this.queryParams['isFuzzy'];
        return !!isFuzzy;
    }

    extractToPlateNoOrVIN(): string {
        const searchKey = this.queryParams['q'];
        if (!searchKey) {
            return null;
        }
        if (this.isFuzzySearch()) {
            return this.extractToPlateNoOrFuzzyVIN();
        }

        if (searchKey.length !== 6 && searchKey.length !== 7 && searchKey.length !== 17) {
            return null;
        }

        return searchKey.toUpperCase();
    }

    extractToPlateNoOrFuzzyVIN(): string {
        const searchKey = this.queryParams['q'];
        if (searchKey.length > 17 || searchKey.length === 0) {
            return null;
        }
        return searchKey.toUpperCase();
    }

    extractToCounted(): boolean {
        if (!this.queryParams['counted']) {
            return null;
        }
        return JSON.parse(this.queryParams['counted']);
    }

}