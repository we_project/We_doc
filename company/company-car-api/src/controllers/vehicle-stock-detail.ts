import { VehicleStockBatchReject } from './dto/VehicleStockBatchReject';
import { RequestWithCurrentUser } from '../midwares/authModels';
import { NextFunction, Response } from 'express';
import { VehicleStockDetail } from '../application/dto/VehicleStockDetail';
import {
    registerVehicleStock, vehicleStockHasSubmittedException, vehicleStockNoPermissionException,
    vehicleStockNoVehicleException
} from '../application/register-vehicle-stock-service';
import { rejectVehicleStockStatus, NotExistsStockErrorMessage } from '../application/reject-vehicle-stock-status';
import { StockHasBeenExamineErrorMessage } from '../models/VehicleStock';
import { approveVehicleStockStatus } from '../application/approve-vehicle-stock-status';
import { logWarn } from '../log-writter/logger';

export  let saveVehicleStocks = async (req: RequestWithCurrentUser, res: Response, next: NextFunction) => {
    try {
        const vehicleId: String = req.params.id;
        const stockDetail: VehicleStockDetail = Object.assign(new VehicleStockDetail, req.body);
        await registerVehicleStock(vehicleId, stockDetail, req.currentUser);

        res.status(201).json({message: 'created'});
    }
    catch (error) {
        if (typeof error === 'string') {
            if (error === vehicleStockHasSubmittedException) {
                logWarn(error, 'saveVehicleStocks');
                res.status(400).json({ error: error});
                return;
            }
            if (error === vehicleStockNoPermissionException) {
                logWarn(error, 'saveVehicleStocks');
                res.status(403).json({ error: error});
                return;
            }
            if (error === vehicleStockNoVehicleException) {
                logWarn(error, 'saveVehicleStocks');
                res.status(400).json({ error: error});
                return;
            }
        }
        else {
            error.status = 500;
            next(error);
        }
    }
};

export let reject = async (req: RequestWithCurrentUser, res: Response, next: NextFunction) => {
    try {
        const stockId: string = req.params.stockId;

        await rejectVehicleStockStatus(req.currentUser, stockId);

        res.status(201).json({message: 'created'});
    }
    catch (error) {
        if (error.message == NotExistsStockErrorMessage) {
            logWarn(error, 'reject stock');
            res.status(400).json({ error: error.message});
        }
        else if (error.message == StockHasBeenExamineErrorMessage) {
            logWarn(error, 'reject stock');
            res.status(403).json({ error: error.message});
        }
        else {
            error.status = 500;
            next(error);
        }
    }
};

export let batchReject = async (req: RequestWithCurrentUser, res: Response, next: NextFunction) => {
    try {
        const rejections: Array<VehicleStockBatchReject> = req.body;

        await Promise.all(rejections.map(async (rejection) =>  {
            try {
                await rejectVehicleStockStatus(req.currentUser, rejection.id,  rejection.comment);
            }
            catch (error) {
                logWarn(error, 'the batch reject:');
            }
        }));
        res.status(200).json({message: 'successful'});
    }
    catch (error) {
        next(error);
    }
};

export let approve = async (req: RequestWithCurrentUser, res: Response, next: NextFunction) => {
    try {
        const stockId: string = req.params.stockId;
        await approveVehicleStockStatus(stockId, req.currentUser);

        res.status(201).json({message: 'created'});
    }
    catch (error) {
        if (error.message == NotExistsStockErrorMessage) {
            logWarn(error, 'approve stock');
            res.status(400).json({ error: error.message});
        }
        else if (error.message == StockHasBeenExamineErrorMessage) {
            logWarn(error, 'approve stock');
            res.status(403).json({ error: error.message});
        }
        else {
            error.status = 500;
            next(error);
        }
    }
};

export let batchApprove = async (req: RequestWithCurrentUser, res: Response, next: NextFunction) => {
    try {
        const stockIds: [string] = req.body;

        await Promise.all(stockIds.map(async (stockId) =>  {
            try {
                await approveVehicleStockStatus(stockId, req.currentUser);
            }
            catch (error) {
                logWarn(error, 'the batch approve:');
            }
        }));
        res.status(200).json({message: 'successful'});
    }
    catch (error) {
        next(error);
    }
};


