import { NextFunction, Response } from 'express';
import * as requestify from 'requestify';
import { isNullOrUndefined } from 'util';
import { User, UserModel } from '../models/User';
import { UserMapping, UserMappingModel } from '../models/UserMapping';
import { mockUser } from '../utils/Constants';
import { AuthResponseEntity, RequestWithCurrentUser } from './authModels';
import { UserRole } from '../models/user-role';
import { RequestResponse } from 'request';
const request = require('request');


export let validateToken = async (req: RequestWithCurrentUser, res: Response, next: NextFunction) => {
  try {
    if (isNullOrUndefined(req.headers.authorization)) {
      res.status(403).send('Forbidden');
      return;
    }

    const stoken = req.headers.authorization;
    if (process.env.DEV) {
      req.currentUser = mockUser(stoken);
      next();
      return;
    }
    const entity: AuthResponseEntity = await getSSOEntity(stoken);

    if (entity.status != 'success') {
      res.status(403).send('Forbidden');
      return;
    }

    const users: UserModel[] = await User.find({ 'empId': entity.userinfo.EMPLID }).exec();
    if (users && users.length > 0) {
      req.currentUser = users[0];
      next();
      return;
    }

    const userMappings: UserMappingModel[] = await UserMapping.find({ 'empId': entity.userinfo.EMPLID }).exec();
    if (userMappings && userMappings.length > 0) {
      const { empId, empName, dcPayId } = userMappings[0];
      req.currentUser = await new User({empId: empId, fullName: empName, empName: empName, dcPayId: dcPayId, role: UserRole.EmployeeRoleString}).save();
      next();
    }
    else {
      req.currentUser = await new User({empId: entity.userinfo.EMPLID, fullName: null, empName: null, dcPayId: null, role: UserRole.EmployeeRoleString}).save();
      next();
    }
  } catch (err) {
    res.status(500).send('Something went wrong!');
  }
};

const getSSOEntity = (stoken: string) => {
    const locationApi = `https://anywhere.i.daimler.com/any/anywhere/ssoLogin?stoken=${stoken}`;
    return new Promise<any>((resolve, reject) => {
        request(locationApi, (error: any, response: RequestResponse, body: any) => {
            if (!error && response.statusCode == 200) {
                const parse = JSON.parse(body);
                resolve(parse);
            } else {
                reject(error.message);
            }
        });
    });
};