import { RequestWithCurrentUser } from './authModels';
import { User } from '../models/User';
import { mockUser } from '../utils/Constants';
import * as jwt from 'jsonwebtoken';
import { NextFunction, Response } from 'express';
import { isNullOrUndefined } from 'util';
import { validateToken } from './authMidware';
import * as async from 'async';

export const secretOrKey = () => {
    return 'tasmanianDevil';
};

export function generateJwt(payload: { loginId: string }) {
    return jwt.sign(payload, secretOrKey());
}

const TOKEN_PREFIX = 'JWT ';

export const adminTokenValidatorFactory = (seeOtherTokeTypeHandler: () => void, noAuthHandler: () => void) => {
    return async (req: RequestWithCurrentUser, res: Response, next: NextFunction) => {
        const authorization = req.headers.authorization;
        if (isNullOrUndefined(authorization) || !authorization.startsWith(TOKEN_PREFIX)) {
            seeOtherTokeTypeHandler();
        } else {
            const token = authorization.substring(TOKEN_PREFIX.length);
            try {
                const payload: any = jwt.verify(token, secretOrKey());
                const user = await User.findOne({loginId: payload.loginId});

                if (user == null) {
                    noAuthHandler();
                } else {
                    req.currentUser = user;
                    next();
                }

            } catch (error) {
                noAuthHandler();
            }

        }
    };
};

export const onlyAdminTokenValidator = async (req: RequestWithCurrentUser, res: Response, next: NextFunction) => {
    const adminTokenValidator = adminTokenValidatorFactory(
        async () => {res.status(401).send(); },
        async () => {res.status(401).send(); });

    await adminTokenValidator(req, res, next);
};

export const combineTokenValidator = async (req: RequestWithCurrentUser, res: Response, next: NextFunction) => {
    const adminTokenValidator = adminTokenValidatorFactory(
        async () => {await validateToken(req, res, next); },
        async () => {res.status(401).send(); });

    await adminTokenValidator(req, res, next);
};