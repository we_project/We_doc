import { Request } from 'express';
import { UserModel } from '../models/User';

export interface AuthResponseEntity {
  status: string;
  userinfo: Staff;
}

export interface Staff {
  DC_EMPLMNTTYPE_ID: number;
  DC_EMPLMNTTYPE_DL: string;
  IsEnable: boolean;
  UID: number;
  UName: string;
  UNO: number;
  EMPLID: string;
  Supervisor_ID: string;
  supervisor_name: string;
  DeptID: string;
  Phone: string;
  Busn_Phone: string;
  Email: string;
  Avatar: string;
  DC_Location: string;
  Company: string;
  SM_UserID: string;
  SPID: string;
  SPName: string;
}
export interface RequestWithCurrentUser extends Request {
  currentUser: UserModel;
}