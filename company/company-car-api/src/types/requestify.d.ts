export function Requestify(): any;
export function get(str: string): any;
export function get(str: string, cache: object): any;

interface IResponse {
  getBody(): any;
}
declare interface Response extends IResponse {}