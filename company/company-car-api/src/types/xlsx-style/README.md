# Installation
> `npm install --save @types/xlsx`

# Summary
This package contains type definitions for xlsx (https://github.com/SheetJS/js-xlsx).

# Details
Files were exported from https://www.github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/xlsx

Additional Details
 * Last updated: Mon, 21 Aug 2017 22:03:22 GMT
 * Dependencies: none
 * Global values: none

# Credits
These definitions were written by themauveavenger <https://github.com/themauveavenger>.
