import * as mongoose from 'mongoose';
import * as mongoosePaginate from 'mongoose-paginate';
import { UserModel } from './User';
import { isNullOrUndefined } from 'util';

export const StockHasBeenExamineErrorMessage = 'stock has been rejected or submitted';

export interface IAddress {
    province: string;
    city: string;
    district: string;
    detail: string;
}

export class ExamineTypes {
    public static Rejection = 'Reject';
    public static Approval = 'Approve';
}

export interface IExaminationStatus {
    examineTime: number;
    userId: string;
    role: string;
    examineType: string;
    comment?: string;
}

export interface Submitter {
    submitterId: string;
    submitterFullName: string;
    submitterRole: string;
}

export interface VehicleUser {
    empId: string;
    empName: string;
    dcPayId: string;
}

export class InsuranceStickerValid {
    static valid: string = 'YES';
    static invalid: string = 'NO';
}

export class AnnualCheckOverdue {
    static overdue: string = 'YES';
    static schedule: string = 'NO';
}

export type VehicleStock = mongoose.Document & {
    model: string;
    vin: string;
    id: string;
    provincialCapital: string;
    location: {
        latitude: number;
        longitude: number;
    };
    locationPosition: IAddress;
    exteriorColour: string;
    interiorColour: string;
    leftFrontImage: {
        id: string;
    };
    rightBackImage: {
        id: string;
    };
    licenseImage: {
        id: string;
    };
    warningTriangleInCar: string;
    fireExtinguisher: string;
    floorMat: string;
    mileage: string;
    odometerImage: {
        id: string;
    };
    carIsMobileAndInUse: string;
    damages: {
        damageLevel: string;
        damageImages: {
            site: string;
            imageId: string;
        }[];
    };
    examineStatus: IExaminationStatus;
    signature: string;
    commitTime: number;
    insuranceStickerValid: string;
    annualCheckOverdue: string;
    submitter: Submitter;
    vehicleUser: VehicleUser;
    isExamined(): boolean;
    examine(result: IExaminationStatus): void
};

const vehicleStockSchema = new mongoose.Schema({
    model: String,
    vin: String,
    provincialCapital: String,
    location: {
        latitude: Number,
        longitude: Number
    },
    locationPosition: {
        province: String,
        city: String,
        district: String,
        detail: String
    },
    exteriorColour: String,
    interiorColour: String,
    leftFrontImage: {
        id: String
    },
    rightBackImage: {
        id: String
    },
    licenseImage: {
        id: String
    },
    warningTriangleInCar: String,
    fireExtinguisher: String,
    floorMat: String,
    mileage: String,
    odometerImage: {
        id: String
    },
    carIsMobileAndInUse: String,
    damages: {
        damageLevel: String,
        damageImages: [{
            site: String,
            imageId: String,
        }],
    },
    examineStatus: {
        examineTime: Number,
        userId: String,
        role: String,
        examineType: String,
        comment: String
    },
    submitter: {
        submitterId: String,
        submitterFullName: String,
        submitterRole: String
    },
    vehicleUser: {
      empId: String,
      dcPayId: String,
      empName: String
    },
    signature: String,
    commitTime: Number,
    insuranceStickerValid: String,
    annualCheckOverdue: String
});

vehicleStockSchema.methods.isExamined = function (): boolean {
    return !((isNullOrUndefined(this.examineStatus) || isNullOrUndefined(this.examineStatus.userId)));
};

vehicleStockSchema.methods.examine = function(result: IExaminationStatus): void {
    if (this.isExamined()) {
        throw new Error(StockHasBeenExamineErrorMessage);
    }

    this.examineStatus = result;
};

vehicleStockSchema.plugin(mongoosePaginate);

export const VehicleStock = mongoose.model<VehicleStock>('VehicleStock', vehicleStockSchema);