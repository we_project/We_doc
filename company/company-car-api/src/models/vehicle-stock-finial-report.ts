import { VehicleModel } from './Vehicle';
import { VehicleStock } from './VehicleStock';
import {_ as otherMatch, match } from 'kasai';
import { isNullOrUndefined } from 'util';

export class VehicleStockFinialReport {
    index: number;
    vehiclePlateNumber: string;
    registerUnder: string;
    vehicleMake: string;
    vehicleModel: string;
    stockColor: string;
    stockTrimColor: string;
    stockProvince: string;
    stockCity: string;
    stockDistrict: string;
    stockLocationDetail: string;
    vehicleUsage: string;
    stockMileage: string;
    stockInsuranceEndStatus: string;
    stockAnnualStatus: string;
    stockWarningTriangle: string;
    stockFireExtinguisher: string;
    stockFloorMat: string;
    stockDamagesMinor: string;
    stockDamagesMedium: string;
    stockDamagesMajor: string;
    stockCarInUser: string;
    stockCheckBy: string;
    stockSubmitDate: Date | string;

    results: any[];

    constructor(vehicle: VehicleModel, stock: VehicleStock, index: number) {
        this.index = index;
        this.vehiclePlateNumber = isNullOrUndefined(vehicle.plateNo) || vehicle.plateNo === '' ? vehicle.legacyPlateNo : vehicle.plateNo;
        this.registerUnder = !!vehicle.registrationCompany ? vehicle.registrationCompany : '';
        this.vehicleMake = !!vehicle.carType ? vehicle.carType.make : '';
        this.vehicleModel = !!vehicle.carType ? vehicle.carType.model : '';
        this.vehicleUsage = vehicle.usage;

        this.stockColor = vehicle.hasApprovedStock() ? stock.exteriorColour : '';
        this.stockTrimColor = vehicle.hasApprovedStock() ? stock.interiorColour : '';

        this.stockProvince = vehicle.hasApprovedStock() && !!stock.locationPosition ? stock.locationPosition.province : '';
        this.stockCity = vehicle.hasApprovedStock() && !!stock.locationPosition ? stock.locationPosition.city : '';
        this.stockDistrict = vehicle.hasApprovedStock() && !!stock.locationPosition ? stock.locationPosition.district : '';
        this.stockLocationDetail = vehicle.hasApprovedStock() && !!stock.locationPosition ? stock.locationPosition.detail : '';

        this.stockMileage = vehicle.hasApprovedStock() ? stock.mileage : '';
        this.stockInsuranceEndStatus = vehicle.hasApprovedStock() ? stock.insuranceStickerValid : '';
        this.stockAnnualStatus = vehicle.hasApprovedStock() ? stock.annualCheckOverdue : '';
        this.stockWarningTriangle = vehicle.hasApprovedStock() ? stock.warningTriangleInCar : '';
        this.stockFireExtinguisher = vehicle.hasApprovedStock() ? stock.fireExtinguisher : '';
        this.stockFloorMat = vehicle.hasApprovedStock() ? stock.floorMat : '';
        this.setDamagesLevel(vehicle.hasApprovedStock() && !!stock.damages ? stock.damages.damageLevel : '');
        this.stockCarInUser = vehicle.hasApprovedStock() ? stock.carIsMobileAndInUse : '';
        this.stockCheckBy = vehicle.hasApprovedStock() && !!stock.submitter ? stock.submitter.submitterFullName : '';
        this.stockSubmitDate = vehicle.hasApprovedStock() ? new Date(stock.commitTime) : '';

        this.setupResultCollection();
    }

    private setupResultCollection() {
        this.results = [];
        this.results.push(this.index);
        this.results.push(this.vehiclePlateNumber);
        this.results.push(this.registerUnder);
        this.results.push(this.vehicleMake);
        this.results.push(this.vehicleModel);
        this.results.push(this.stockColor);
        this.results.push(this.stockTrimColor);
        this.results.push(this.stockProvince);
        this.results.push(this.stockCity);
        this.results.push(this.stockDistrict);
        this.results.push(this.stockLocationDetail);
        this.results.push(this.vehicleUsage);
        this.results.push(this.stockMileage);
        this.results.push(this.stockInsuranceEndStatus);
        this.results.push(this.stockAnnualStatus);
        this.results.push(this.stockWarningTriangle);
        this.results.push(this.stockFireExtinguisher);
        this.results.push(this.stockFloorMat);
        this.results.push(this.stockDamagesMinor);
        this.results.push(this.stockDamagesMedium);
        this.results.push(this.stockDamagesMajor);
        this.results.push(this.stockCarInUser);
        this.results.push(this.stockCheckBy);
        this.results.push(this.stockSubmitDate);
    }

    setDamagesLevel = (damageLevel: string) => {
        return match(damageLevel, [
            [(damageLevel: string) => {
                return damageLevel === 'PERFECT';
            },
                () => {
                    this.stockDamagesMajor = '';
                    this.stockDamagesMedium = '';
                    this.stockDamagesMinor = '';
                }],
            [(damageLevel: string) => {
                return damageLevel === 'MINOR';
            },
                () => {
                    this.stockDamagesMajor = '';
                    this.stockDamagesMedium = '';
                    this.stockDamagesMinor = 'YES';
                }],
            [(damageLevel: string) => {
                return damageLevel === 'MEDIUM';
            },
                () => {
                    this.stockDamagesMajor = '';
                    this.stockDamagesMedium = 'YES';
                    this.stockDamagesMinor = '';
                }],
            [(damageLevel: string) => {
                return damageLevel === 'MAJOR';
            },
                () => {
                    this.stockDamagesMajor = 'YES';
                    this.stockDamagesMedium = '';
                    this.stockDamagesMinor = '';
                }],
            [otherMatch, () => {
                this.stockDamagesMajor = '';
                this.stockDamagesMedium = '';
                this.stockDamagesMinor = '';
            }]
        ]);
    };

}