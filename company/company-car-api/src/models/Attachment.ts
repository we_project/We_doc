import * as mongoose from 'mongoose';
import * as mongooseLogs from 'mongoose-activitylogs';

export type AttachmentModel = mongoose.Document & {
  filename: string,
  path: string,
  encoding: string,
  mimetype: string,
  size: number,
  type: string,
};

const attachmentSchema = new mongoose.Schema({
  filename: String,
  path: String,
  encoding: String,
  mimetype: String,
  size: Number,
  type: String,
}, { timestamps: true });

attachmentSchema.plugin(mongooseLogs, {
  schemaName: 'Attachment',
  createAction: 'created',
  updateAction: 'updated',
  deleteAction: 'removed'
});

export const Attachment = mongoose.model<AttachmentModel>('Attachment', attachmentSchema);