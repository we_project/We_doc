import { UserModel } from './User';

export class VehicleQueryConditionBuilder {
    queryParams = {};

    appendWithIsAssignWithMe = (user: UserModel) => {
        this.queryParams = Object.assign({}, this.queryParams, {'assignee.loginId': user.loginId});
        return this;
    };

    appendWithLegacyPlateNo = (legacyPlateNo: string) => {
        this.queryParams = Object.assign({}, this.queryParams, {'legacyPlateNo': legacyPlateNo});
        return this;
    };

    appendWithExternalDelegate = (externalDelegateLoginId: string) => {
        this.queryParams = Object.assign({}, this.queryParams, {'externalDelegate.loginId': externalDelegateLoginId});
        return this;
    };

    appendWithPlateNo = (plateNo: string) => {
      this.queryParams = Object.assign({}, this.queryParams, {'plateNo': plateNo});
      return this;
    };

    appendWithVin = (vin: string) => {
        this.queryParams = Object.assign({}, this.queryParams, {'VIN': vin});
        return this;
    };

    appendWithFuzzyVinOrPlateNo = (no: string) => {
        this.queryParams = Object.assign({}, this.queryParams,
            {'$or': [
                {'VIN': new RegExp(no, 'i')},
                {'plateNo': new RegExp(no, 'i')},
                {'legacyPlateNo': new RegExp(no, 'i')}
            ]});
        return this;
    };

    appendWithCounted = () => {
        this.queryParams = Object.assign({}, this.queryParams, {'stockRecord': {$exists: true}});
        return this;
    };


    appendWithVehicleOwner(cnNumber: string) {
        this.queryParams = Object.assign({}, this.queryParams, {'user.CNNumber': cnNumber});
        return this;
    }

    result = () => {
        return this.queryParams;
    };

  appendWithInternalDelegate(empId: string) {
        this.queryParams = Object.assign({}, this.queryParams, {'internalDelegate.empId': empId});
        return this;
  }

    appendWithStockApproved() {
        this.queryParams = Object.assign({}, this.queryParams, {'stockApprovedTime': {$exists: true}});
        return this;
    }
}