import * as mongoose from 'mongoose';
import { Insurance } from './Insurance';
import * as mongoosePaginate from 'mongoose-paginate';
import { VehicleStock } from './VehicleStock';
import { AssigneeUser } from './AssigneeUser';
import { ExternalDelegate } from './ExternalDelegate';
import { InternalDelegate } from './InternalDelegate';

export type VehicleModel = mongoose.Document & {
    plateNo: string,
    legacyPlateNo: string,
    VIN: string,
    engineNo: string,
    registrationLocation: string,
    registrationDate: Date,
    registrationCompany: string,
    carType: {
        make: string,
        model: string,
    },
    mileage: {
        displayValue: number,
        tokenDate: Date,
    },
    location: any,
    user: {
        CNNumber: string,
        details: any
    },
    insurance: any,
    stockTakenDates: Array<Date>,
    usage: string,
    contractType: string,
    stockRecord: any,
    stockApprovedTime: number,
    assignee: AssigneeUser,
    registerStockRecord(stock: VehicleStock): void,
    externalDelegate: ExternalDelegate,
    internalDelegate: InternalDelegate,
    getStockStatus(): string,
    isSubmitStocked(): boolean;
    assignToFleetAdmin(fleetAdmin: AssigneeUser): void;
    clearStockRecord(): void;
    approveStockRecord(approvedTime: number): void;
    hasApprovedStock(): boolean;
    userCompany: string;
    businessCarUserCompany: string;
};


const vehicleSchema = new mongoose.Schema({
    plateNo: {type: String},
    legacyPlateNo: {type: String},
    VIN: String,
    engineNo: String,
    registrationLocation: String,
    registrationDate: Date,
    registrationCompany: String,
    carType: {
        make: String,
        model: String,
    },
    mileage: {
        displayValue: String,
        tokenDate: Date,
    },
    location: String,
    user: {
        CNNumber: String,
        details: String
    },
    stockTakenDates: Array,
    usage: String,
    contractType: String,
    externalDelegate: {
      loginId: String
    },
    internalDelegate: {
      empId: String
    },
    insurance: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Insurance'
    },
    stockRecord: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'VehicleStock',
        required: false
    },
    stockApprovedTime: Number,
    assignee: {
        loginId: String,
        fullName: String,
        id: String,
        role: String
    },
    userCompany: String,
    businessCarUserCompany: String,
});


vehicleSchema.methods.isSubmitStocked = function(): boolean {
    return !!this.stockRecord;
};

vehicleSchema.methods.getStockStatus = function(): string {
    if (!this.isSubmitStocked()) {
        return 'TASK';
    }

    if (this.hasApprovedStock()) {
        return 'DONE';
    }

    return 'SUBMIT';

};

vehicleSchema.methods.isSubmitStocked = function(): boolean {
    return !!this.stockRecord;
};

vehicleSchema.methods.registerStockRecord = function(stock: VehicleStock): void {
    this.plateNo = stock.provincialCapital + this.legacyPlateNo;
    this.stockRecord = stock.id;
};

vehicleSchema.methods.assignToFleetAdmin = function(fleetAdmin: AssigneeUser): void {
    this.assignee = fleetAdmin;
};

vehicleSchema.methods.hasApprovedStock = function(): boolean {
    return !!this.stockApprovedTime;
};

vehicleSchema.methods.clearStockRecord = function(): void {
    this.stockRecord = undefined;
};

vehicleSchema.methods.approveStockRecord = function(approvedTime: number): void {
    this.stockApprovedTime = approvedTime;
};

vehicleSchema.plugin(mongoosePaginate);
export const Vehicle = mongoose.model<VehicleModel>('Vehicle', vehicleSchema);
