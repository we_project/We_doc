import { UserModel } from '../../models/User';
import { VehicleQueryConditionBuilder } from '../../models/vehicle-query-condition-builder';

export class VehicleQueryConditionTransformer {
    private vehicleQueryConditionBuilder = new VehicleQueryConditionBuilder();

    combineWithIsAssignToMe = (isAssignToMe: boolean, queryUser: UserModel) => {
        if (isAssignToMe) {
            this.vehicleQueryConditionBuilder.appendWithIsAssignWithMe(queryUser);
        }
        return this;
    };

    combineWithVehicleOwnerCNNumber = (cnNumber: string) => {
      if (cnNumber) {
          this.vehicleQueryConditionBuilder.appendWithVehicleOwner(cnNumber);
      }
      return this;
    };

    combineWithExternalDelegate = (externalDelegateLoginId: string) => {
        this.vehicleQueryConditionBuilder.appendWithExternalDelegate(externalDelegateLoginId);
        return this;
    };

    combineWithPlateNoOrVIN = (plateNoOrVIN: string, isFuzzy: boolean) => {
        if (!!plateNoOrVIN) {
            if (!isFuzzy) {
                if (plateNoOrVIN.length === 6) {
                    this.vehicleQueryConditionBuilder.appendWithLegacyPlateNo(plateNoOrVIN);
                } else if (plateNoOrVIN.length === 7) {
                    this.vehicleQueryConditionBuilder.appendWithPlateNo(plateNoOrVIN);
                } else if (plateNoOrVIN.length === 17) {
                    this.vehicleQueryConditionBuilder.appendWithVin(plateNoOrVIN);
                }
            } else {
                if (plateNoOrVIN.length <= 17 && plateNoOrVIN.length > 0) {
                    this.vehicleQueryConditionBuilder.appendWithFuzzyVinOrPlateNo(plateNoOrVIN);
                }
            }
        }
        return this;
    };

    combineWithInternalDelegate = (empId: string) => {
        if (empId) {
            this.vehicleQueryConditionBuilder.appendWithInternalDelegate(empId);
        }
        return this;
    };

    combineWithCounted = (counted: boolean) => {
        if (counted) {
            this.vehicleQueryConditionBuilder.appendWithCounted();
        }
        return this;
    };

    result = () => {
        return this.vehicleQueryConditionBuilder.result();
    };
}