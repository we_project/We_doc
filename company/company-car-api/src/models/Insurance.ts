import * as mongoose from 'mongoose';

export type InsuranceModel = mongoose.Document & {
  insuranceEndDate: Date,
  insuranceAccidentTel: string,
  roadsideAssistance: string,
  insuranceCompany: string,
  annualInspectionDueDate: Date,
};

const insuranceSchema = new mongoose.Schema({
  insuranceEndDate: Date,
  insuranceAccidentTel: String,
  roadsideAssistance: String,
  insuranceCompany: String,
  annualInspectionDueDate: Date,
});

export const Insurance = mongoose.model<InsuranceModel>('Insurance', insuranceSchema);