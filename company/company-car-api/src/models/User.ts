import * as mongoose from 'mongoose';

export type UserModel = mongoose.Document & {
  loginId?: string,
  password?: string,
  fullName: string,
  empId: string,
  empName: string,
  dcPayId: string,
  role: string,
  mobile: string,
};


const userSchema = new mongoose.Schema({
  loginId: { type: String, unique: true, sparse: true },
  password: String,
  fullName: String,
  empId: { type: String, unique: true, sparse: true },
  dcPayId: { type: String, unique: true, sparse: true },
  empName: String,
  role: String,
  mobile: String,
  }, { timestamps: true });

export const User = mongoose.model<UserModel>('User', userSchema);