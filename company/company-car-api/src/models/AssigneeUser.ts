export class AssigneeUser {
    loginId: string;
    fullName: string;
    id: string;
    role: string;
}