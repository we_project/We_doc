import { UserModel } from './User';

export class UserRole {
    public static EmployeeRoleString: String = 'Employee';
    public static SystemAdminRoleString: String = 'System Admin';
    public static ExternalDelegateRoleString: String = 'External Delegate';
    static FleetAdminRole: String = 'Fleet Admin';

    static IsEmployeeRole(user: UserModel) {
        return user.role === UserRole.EmployeeRoleString;
    }

    static IsExternalDelegateRole(user: UserModel) {
        return user.role === UserRole.ExternalDelegateRoleString;
    }

    static IsSuperAdminRole(user: UserModel) {
        return user.role === UserRole.SystemAdminRoleString;
    }

    static IsFleetAdminRole(user: UserModel) {
        return user.role === UserRole.FleetAdminRole;
    }
}