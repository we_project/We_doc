import * as mongoose from 'mongoose';


export type UserMappingModel = mongoose.Document & {
  empId: string,
  dcPayId: string,
  empName: string,
  email: string,
  mobilePhone: string,
  businessPhone: string,
};

const userMappingSchema = new mongoose.Schema({
  empId: { type: String, unique: true },
  dcPayId: { type: String, unique: true },
  empName: String,
  email: String,
  mobilePhone: String,
  businessPhone: String,
});


export const UserMapping = mongoose.model<UserMappingModel>('UserMapping', userMappingSchema);