import * as dotenv from 'dotenv';
import * as jobs from './cron-jobs/stock-location-job';
import { setupMongoose } from './config/config-mongo';
import { createWebApp } from './web-app';
import * as log4js from 'log4js';
import * as globalTunnel from 'global-tunnel';

dotenv.config({ path: '.env' });

setupMongoose();

log4js.configure(process.env.LOG_CONFIG_FILE);

if (process.env.HTTPS_PROXY) {
  globalTunnel.initialize({
    host: process.env.HTTPS_PROXY.split(':')[0],
    port: process.env.HTTPS_PROXY.split(':')[1]
  });

}

const app = createWebApp();


process.on('uncaughtException', (error: Error) => {
  console.error(error);
});

process.on('unhandledRejection', (err: Error, promise: Promise<Object>) => {
  console.error('Unhandled rejection (promise: ', promise, ').');
  throw err;
});

app.set('port', process.env.PORT || 3000);
app.listen(app.get('port'), () => {
  console.log(('  App is running at http://localhost:%d in %s mode'), app.get('port'), app.get('env'));
  console.log('  Press CTRL-C to stop\n');
});



jobs.stockJob();

module.exports = app;