import { Response } from 'express';
import { validateToken } from '../midwares/authMidware';
import * as multer from 'multer';
import { combineTokenValidator, onlyAdminTokenValidator } from '../midwares/passportJwt';
import * as vehiclesAssignees from '../controllers/vehicles-assignees';
import * as loginAPIController from '../controllers/login';
import * as usersController from '../controllers/users';
import * as vehicleAPIController from '../controllers/vehicle';
import * as vehicleDelegateAPIController from '../controllers/vehicle-delegate';
import * as attachmentAPIController from '../controllers/attachment';
import * as vehicleStockDetailController from '../controllers/vehicle-stock-detail';
import * as stocktakeRecordController from '../controllers/stocktake-record';
import * as fleetAdminController from '../controllers/fleet-admin-users';
import { generateVehicleStockFinialReport } from '../controllers/vehicle-stock-finial-report';

export const setupApiRoute = function (express: any) {
    express.post('/api/login', loginAPIController.login);

    express.get('/api/secret', onlyAdminTokenValidator, function (req: any, res: Response) {
        res.json({message: 'Success! You can not see this without a token'});
    });
    express.get('/api/users', onlyAdminTokenValidator, usersController.listUsers);
    express.post('/api/user', onlyAdminTokenValidator, usersController.newUser);
    express.delete('/api/user/:id', onlyAdminTokenValidator, usersController.deleteUser);

    express.get('/api/vehicles/all', onlyAdminTokenValidator, vehicleAPIController.queryAllVehiclesByFleetAdmin);
    express.get('/api/vehicles/search', onlyAdminTokenValidator, vehicleAPIController.search);
    express.get('/api/stocktake-record', onlyAdminTokenValidator, stocktakeRecordController.getStocktakeRecordsByPage);

    express.get('/api/vehicles', validateToken, vehicleAPIController.queryVehiclesByEmployee);
    express.get('/api/vehicle/:id', combineTokenValidator, vehicleAPIController.vehicleDetail);

    express.get('/api/vehicle-delegates', onlyAdminTokenValidator, vehicleDelegateAPIController.findAllDelegates);
    express.post('/api/vehicle-external-delegates', onlyAdminTokenValidator, vehicleDelegateAPIController.updateExternalDelegate);
    express.post('/api/vehicle-internal-delegates', onlyAdminTokenValidator, vehicleDelegateAPIController.updateInternalDelegate);

    const upload = multer({dest: process.env.FILE_UPLOAD});
    express.post('/api/att/upload', upload.array('file'), combineTokenValidator, attachmentAPIController.upload);

    express.get('/api/att/:id', attachmentAPIController.show);

    express.post('/api/vehicle-stocks/vehicle/:id', combineTokenValidator, vehicleStockDetailController.saveVehicleStocks);
    express.post('/api/vehicle-stocks/vehicle/:stockId/status/rejection', onlyAdminTokenValidator, vehicleStockDetailController.reject);
    express.post('/api/vehicle-stocks/vehicle/:stockId/status/approval', onlyAdminTokenValidator, vehicleStockDetailController.approve);
    express.post('/api/vehicle-stocks/vehicle/status/approve-batch', onlyAdminTokenValidator, vehicleStockDetailController.batchApprove);
    express.post('/api/vehicle-stocks/vehicle/status/reject-batch', onlyAdminTokenValidator, vehicleStockDetailController.batchReject);

    express.get('/api/vehicles/assignees', onlyAdminTokenValidator, vehiclesAssignees.todoList);

    express.get('/api/users/fleet-admins', onlyAdminTokenValidator, fleetAdminController.getFleetAdminList);

    express.post('/api/vehicles/assignees/batch-submission', onlyAdminTokenValidator, vehiclesAssignees.batchUpdate);

    express.get('/api/vehicles/stock-finial-report', generateVehicleStockFinialReport);
    // express.get('/api/vehicles/stock-final-report', generateVehicleStockFinialReport);
};


