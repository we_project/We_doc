import * as express from 'express';
import * as path from 'path';

export function setupStaticResource(expressApp: any) {
    const maxAge = -1;
    expressApp.use(express.static(path.join(__dirname, '../../public/admin'), {maxAge: maxAge}));
    expressApp.get('*', (req: any, res: any) => {
        res.sendFile(path.join(__dirname, '../../public/admin/index.html'));
    });
}