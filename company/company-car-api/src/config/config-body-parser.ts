import * as bodyParser from 'body-parser';

export function setupBodyParser(express: any) {
    express.use(bodyParser.json());
    express.use(bodyParser.urlencoded({extended: true}));
}