import mongoose = require('mongoose');

export function setupMongoose() {
    const mongodbUrl = process.env.MONGODB_URI || process.env.MONGOLAB_URI;

    mongoose.Promise = global.Promise;
    mongoose.connect(mongodbUrl, {
        useMongoClient: true
    });

    mongoose.connection.on('error', () => {
        console.log('MongoDB connection error. Please make sure MongoDB is running.');
        process.exit();
    });
}