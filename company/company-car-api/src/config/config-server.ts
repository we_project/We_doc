import * as compression from 'compression';
import * as flash from 'express-flash';
import * as lusca from 'lusca';
import * as errorHandler from 'errorhandler';
import * as dotenv from 'dotenv';
import * as log4js from 'log4js';
import { Request, Response, NextFunction } from 'express';

export const setupServer = function (express: any) {

    express.use(compression());
    express.use(flash());
    express.use(lusca.xframe('SAMEORIGIN'));
    express.use(lusca.xssProtection(true));
    express.disable('etag');
    express.use(log4js.connectLogger(log4js.getLogger('http'), { level: 'auto' }));
};