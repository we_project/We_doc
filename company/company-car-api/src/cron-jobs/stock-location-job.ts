const cron = require('cron');
import { completeTheStockAddress } from '../application/complete-stock-address';

export let stockJob = () => {
    console.log('load job');
    const job = new cron.CronJob('*/1 * * * *', async () => {
        await completeTheStockAddress();
    });
    job.start();
};
