# Company Car API

## Execution environment requirements


- Node.js  LTS 6.11.1
- npm/yarn latest stable version
- mongodb server



## How to run
### Please setup the mongodb firstly
host is ```localhost``` port is ```27017```  
and create the database named as ```company_car```

### configuration

copy `.env.example` to `.env` in the same folder, and update configure in `MONGODB_URI` field regarding your mongodb. and remove the `MONGOLAB_URI`.

run following commands in terminal in the project root directory.

```

$ yarn install
$ yarn run start

```

and also need to do the yarn install under directory admin-site

### Initial Data

the JSON file with initial data is under `data/vehicles.json.zip`, please find and unzip it then import to your mongodb.
DB_NAME is the database name which need be align with the MONGODB_URI last part

Please set the default data by import the json

```
yarn run init_user # init user loginId: admin, password: admin
yarn run user-mapping-generator # to import user info
yarn run insurance-generator #to import insurance and vehicles data
```

### Import the Mock Data for demo daimler employee account
Please set the default data by import the json

```
yarn run insurance-mock-generator #to import insurance and vehicles data
```


## CI

CI Dashboard `http://210.13.247.131:446/`   
~~API for Frontend Developer: `http://192.168.8.106:3000`~~