import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LoginComponent } from './views/login-component/login.component';
import { UsersComponent } from './views/users-component/users.component';
import { StocktakeRecordComponent } from './views/stocktake-record-component/stocktake-record.component';
import { VehiclesAssigneesComponent } from './views/vehicles-assignees/vehicles-assignees.component';
import { VehiclesDelegateComponent } from './views/vehicles-delegate-component/vehicles-delegate.component';
import { ReportComponent } from './views/report-component/report.component';
import { ImportDataComponent } from './views/import-data-component/import-data.component';
import { RequireLoginGuardService } from './services/require-login-guard.service';

const routes = [
  {
    path: '',
    canActivateChild: [RequireLoginGuardService],
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: '/stocktake-record',
      },
      {
        path: 'users',
        pathMatch: 'full',
        component: UsersComponent,
      },
      {
        path: 'stocktake-record',
        pathMatch: 'full',
        component: StocktakeRecordComponent,
      },
      {
        path: 'vehicles-assignees',
        pathMatch: 'full',
        component: VehiclesAssigneesComponent,
      },
      {
        path: 'vehicles-delegate',
        pathMatch: 'full',
        component: VehiclesDelegateComponent,
      },
      {
        path: 'import-data',
        pathMatch: 'full',
        component: ImportDataComponent,
      },
      {
        path: 'report',
        pathMatch: 'full',
        component: ReportComponent,
      }
    ]
  },
  {
    path: 'login',
    pathMatch: 'full',
    component: LoginComponent,
  }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
