import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { UserAPI } from '../apis/user.api';


@Injectable()
export class RequireLoginGuardService {

  constructor(private route: Router, private userService: UserAPI) {
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.userService.auth()
      .do(i => {
        if (i.status == 200) {
          return true;
        }
        this.route.navigate['login'];
      })
      .map(i => !!i)
      .catch(() => {
        this.route.navigate(['login']);
        return Observable.of(false);
      });
  }
}
