import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class MessageBoxService {
  message: any;
  showMessage: Boolean;
  updateMessage: Subject<string> = new Subject<string>();
  constructor() {
    this.reset();
  }

  alert(m){
    this.message = m;
    this.showMessage=true;
    this.updateMessage.next(this.message);
  }

  reset() {
    this.message = null;
    this.showMessage = false;
    this.updateMessage.next(this.message);
  }
}
