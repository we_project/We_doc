import { TestBed, inject } from '@angular/core/testing';

import { RequireLoginGuardService } from './require-login-guard.service';

describe('RequireLoginGuardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RequireLoginGuardService]
    });
  });

  it('should be created', inject([RequireLoginGuardService], (service: RequireLoginGuardService) => {
    expect(service).toBeTruthy();
  }));
});
