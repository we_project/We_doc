import { RequestOptionsArgs } from '@angular/http';
import { Headers } from '@angular/http';

export class TokenStore {
  static wrapTokenToHeaders(): RequestOptionsArgs {
    const token = localStorage.getItem('token') || '';
    const headers = new Headers();
    headers.append('Cache-control', 'no-cache');
    headers.append('Cache-control', 'no-store');
    headers.append('Expires', '0');
    headers.append('Pragma', 'no-cache');
    headers.append('Authorization', `JWT ${token}`);
    return { headers: headers };
  }
}

export const authToken = function (): Headers {
  const token = localStorage.getItem('token') || '';
  const headers = new Headers();
  headers.append('Cache-control', 'no-cache');
  headers.append('Cache-control', 'no-store');
  headers.append('Expires', '0');
  headers.append('Pragma', 'no-cache');
  headers.append('Authorization', `JWT ${token}`);
  return headers;
};

