import { Component, OnInit, Output,Input, EventEmitter } from '@angular/core';
import { VehiclesAssignee } from '../../models/vehicles-assignee.model';

@Component({
  selector: 'handle-assignee',
  templateUrl: './handle-assignee.component.html',
  styleUrls: ['./handle-assignee.component.scss'],
})

export class HandleAssigneeComponent {
  @Input('selectedVehiclesAssignee')
  vehiclesAssignee : Array<VehiclesAssignee>;
  @Output() clearAssigneeEvent = new EventEmitter<string>();
  @Output('refreshData') refreshData = new EventEmitter<string>();
  showSelectAssignee: boolean;

  constructor() {
  }

  ngOnInit() {
  }

  closeModal(){
    this.showSelectAssignee=false;
    document.body.classList.remove('show-modal');
  }

  openModal(){
  if(this.hasAssignee()) {
    this.showSelectAssignee = true;
    document.body.classList.add('show-modal');
    }
  }

  isAllSelectedTaskNoAssignee() {
    return this.vehiclesAssignee && this.vehiclesAssignee.every(task => {
        return !task.assignee.id;
      });
  }

  hasAssignee() {
    return this.vehiclesAssignee && this.vehiclesAssignee.length > 0;
  }

  confirmClear(){
    this.clearAssigneeEvent.next();
  }

  afterSuccess(){
    this.closeModal();
    this.refreshData.next();
  }
}
