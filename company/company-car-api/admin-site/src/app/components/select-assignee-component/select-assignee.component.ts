import { Component, OnInit, Output,Input, EventEmitter } from '@angular/core';
import { FleetAdminApi } from '../../apis/fleet-admin.api';
import { VehiclesAssigneeApi } from '../../apis/vehicles-assignee-api';
import { FleetAdmin } from '../../models/user.model';
import { VehiclesAssignee } from '../../models/vehicles-assignee.model';

@Component({
  selector: 'select-assignee',
  templateUrl: './select-assignee.component.html',
  styleUrls: ['./select-assignee.component.scss'],
})

export class SelectAssigneeComponent {
  @Input('vehiclesAssignee')
  vehiclesAssignee : Array<VehiclesAssignee>;
  @Output()
  cancelEvent = new EventEmitter<string>();
  @Output()
  afterSuccess = new EventEmitter<string>();

  fleetAdmins: FleetAdmin[];
  selectedFleetAdmin: FleetAdmin;

  constructor(private vehiclesAssigneeApi: VehiclesAssigneeApi,  private fleetAdminApi: FleetAdminApi) {
  }

  onOkBtnPressed() {
    const toAssigneVehicles = this.vehiclesAssignee;
    this.vehiclesAssigneeApi.save(toAssigneVehicles, this.selectedFleetAdmin).subscribe(res => {
      if (res.status == 201) {
        this.afterSuccess.next();
      }
    });
  }
  onCancelBtnPressed() {
    this.cancelEvent.next();
  }

  ngOnInit() {
    this.fetchFleetAdminList();
  }

  private fetchFleetAdminList() {
    this.fleetAdminApi.getFleetAdminList().subscribe(datas => {
      this.fleetAdmins = datas.map(data => Object.assign(new FleetAdmin(), data));
    });
  }

  selectFleetAdmin(fleetAdmin: FleetAdmin) {
    this.selectedFleetAdmin = fleetAdmin;
  }

}
