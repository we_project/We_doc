import { Component, OnInit, Output,Input, EventEmitter } from '@angular/core';

@Component({
  selector: 'button-modalbox',
  templateUrl: './modal-box.component.html',
  styleUrls: ['./modal-box.component.scss'],
})

export class ModalBoxComponent {
  @Input() disabled : boolean;
  @Input() isValid : boolean;
  @Input() buttonTitle : String;
  @Input() title : String;
  @Input() description : String;
  @Input() leftButton: String;
  @Input() rightButton: String;
  @Output() validateBeforeSubmit =  new EventEmitter<string>();
  @Output() cancelEvent = new EventEmitter<string>();
  @Output() submitEvent = new EventEmitter<string>();


  isVisable : Boolean;
  constructor() {
  }

  onYesBtnPressed() {
    if(this.validateBeforeSubmit.observers.length>0){
      this.validateBeforeSubmit.next();
      if(!this.isValid){
        return;
      }
    }

    this.submitEvent.next();
    this.isVisable = false;
    document.body.classList.remove('show-modal');
  }

  cancel() {
    document.body.classList.remove('show-modal');
    this.isVisable = false;
    this.cancelEvent.next();
  }

  triggerModalBox(){
    if(!this.disabled){
      document.body.classList.add('show-modal');
      this.isVisable = true;
    }
  }

  ngOnInit() {
  }
}
