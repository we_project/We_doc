import { Component, OnInit, Output,Input, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

const RouteTitleMapper = {
  "users": "Users",
  "stocktake-record": "Stocktake Records",
  "vehicles-assignees": "Assignments",
  "vehicles-delegate": "Delegations",
  "report": "Report",
  "login": "Login",
  "import-data": "Import Data",
}

@Component({
  selector: 'tab-title',
  templateUrl: './tab-title.component.html',
})

export class TabTitleComponent {
  constructor(public route: Router) {
  }

  ngOnInit() {
  }

  getTitle(){
    return RouteTitleMapper[this.route.url.substr(1)]+" - VehicleButler";
  }
}
