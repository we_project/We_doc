import { Component, OnInit, Output,Input, EventEmitter } from '@angular/core';
import { MessageBoxService} from '../../services/message-box.service';

@Component({
  selector: 'message-box',
  templateUrl: './message-box.component.html',
  styleUrls: ['./message-box.component.scss'],
})

export class MessageBoxComponent {
  message:String;
  showMessage:Boolean;
  _subscription:any;

  constructor(private messageBoxService:MessageBoxService) {
    this.message = messageBoxService.message;
    this.showMessage = messageBoxService.showMessage;
    this._subscription = messageBoxService.updateMessage.subscribe((value) => {
      this.message = value;
      this.showMessage = value ? true : false
    });
  }

  onOKBtnPressed() {
    this.messageBoxService.reset()
  }

  ngOnInit() {
  }
  ngOnDestroy() {
    //prevent memory leak when component destroyed
    this._subscription.unsubscribe();
  }
}
