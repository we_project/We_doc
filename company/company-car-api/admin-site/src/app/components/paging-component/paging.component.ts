import { Component, OnInit, Output,Input, EventEmitter } from '@angular/core';
@Component({
  selector: 'paging',
  templateUrl: './paging.component.html',
  styleUrls: ['./paging.component.scss']
})
export class PagingComponent {
  @Input() fetchDataPromise:any;
  @Input() pageSize:number;
  @Output() updateStatus = new EventEmitter<any[]>();
  @Output() updateCurrentPageNumber = new EventEmitter<number>();

  recordCount: number;
  currentPageNumber: number;


  @Input() searchContext: string;

  constructor() { }
  ngOnInit() {
    this.currentPageNumber = 1;
    this.getNewPageData(this.pageSize, this.currentPageNumber);
    this.searchContext = '';
  }

  search() {
    this.currentPageNumber = 1;
    this.getNewPageData(this.pageSize, this.currentPageNumber);
  }

  getNewPageData(pageSize, pageNumber) {
    this.currentPageNumber = parseInt(pageNumber);
    if (this.searchContext === undefined) {
      this.fetchDataPromise(pageSize, pageNumber).subscribe(res => {
        this.updateStatus.emit(res);
        this.updateCurrentPageNumber.emit(this.currentPageNumber);
        this.recordCount = res.recordCount || res.vehicleDelegationTotalCount;
      });
    } else {
      this.fetchDataPromise(pageSize, pageNumber, this.searchContext).subscribe(res => {
        this.updateStatus.emit(res);
        this.updateCurrentPageNumber.emit(this.currentPageNumber);
        this.recordCount = res.recordCount || res.vehicleDelegationTotalCount;
      });
    }
  }

  public goTo(targetPageNumber) {
    if (targetPageNumber <= 0) {
      return;
    }
    if (targetPageNumber >this.getPageCount()) {
      return;
    }
    this.getNewPageData(this.pageSize, targetPageNumber);
  }

  public getPageCount = function () {
    return Math.ceil(this.recordCount / this.pageSize);
  }

  public generatePageNumberArray() {
    return Array.from({length: this.getPageCount()}, (item, index)=>index)
  }

  public showPageNumber(pageId) {
    return Math.abs(pageId - this.currentPageNumber) < 3;
  }

}
