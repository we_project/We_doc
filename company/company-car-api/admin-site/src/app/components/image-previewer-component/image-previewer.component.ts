import { Component, OnInit, Output,Input, EventEmitter } from '@angular/core';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'image-previewer',
  templateUrl: './image-previewer.component.html',
  styleUrls: ['./image-previewer.component.scss']
})
export class ImagePreviewerComponent {
  @Input() imageUrl:String;
  @Input() imageName:String;
  imageDisplayUrl:String;

  constructor() { }

  ngOnInit() {
  }

  showImage() {
    this.imageDisplayUrl = this.imageUrl ? `${environment.baseUrl}/api/att/${this.imageUrl}` : null
  }

  exitImagePreview() {
    this.imageDisplayUrl = null
  }

}
