import {Component} from '@angular/core';
import { StocktakeRecordApi} from "../../apis/stocktake-record.api";
import {StocktakeRecord} from "../../models/stocktake-record.model";
import 'rxjs/Rx';
import * as FileSaver from 'file-saver';
import { environment } from '../../../environments/environment';


@Component({
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})


export class ReportComponent {
  exportUrl:string;
  
  constructor(private stocktakeRecordService:StocktakeRecordApi) {
    this.exportUrl = `${environment.baseUrl}/api/vehicles/stock-finial-report`
  }

  ngOnInit() {

  }

  exportExcel() {
    this.stocktakeRecordService.exportFile()
  }
}

