import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { UserAPI } from '../../apis/user.api';
import { LoginBody } from '../../models/user.model';
import 'rxjs/Rx';


@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})


export class LoginComponent {
  constructor(private route: Router, private userService: UserAPI) {
  }

  loginBody: LoginBody = {};
  isLoginFailed: boolean = false;
  isNotSystemAdminLogin: boolean = false;

  ngOnInit() {
    this.userService.auth().subscribe(i => {
        if (i.status == 200) {
          this.route.navigate(['']);
        }
      }
    );
  }

  submitLogin() {
    this.isLoginFailed = false;
    this.userService.loginWithUsernameAndPassword(this.loginBody)
      .subscribe(i => {
        if(i.role!='System Admin'){
          this.isNotSystemAdminLogin = true;
          return;
        }
        localStorage.setItem('token', i.token);
        localStorage.setItem('fullName', i.fullName);
        this.route.navigate(['']);
      }, error => {
        this.isLoginFailed = true;
        console.log(error);
      });

  }
}

