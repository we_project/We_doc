import {Component} from "@angular/core";
import {StocktakeRecordApi} from "../../apis/stocktake-record.api";
import {StocktakeRecord} from "../../models/stocktake-record.model";

@Component({
  templateUrl: './stocktake-record.component.html',
  styleUrls: ['./stocktake-record.component.scss']
})
export class StocktakeRecordComponent {
  stocktakeRecords:StocktakeRecord[];
  currentPageNumber:number;
  pageSize:number;
  varIsAllChecked:boolean;
  showErrorMsg:boolean;
  rejectComment:string;

  constructor(private stocktakeRecordService:StocktakeRecordApi) {
  }

  get isAllChecked():boolean {
    return this.varIsAllChecked;
  }

  set isAllChecked(value:boolean) {
    this.varIsAllChecked = value;
    this.stocktakeRecords.forEach(record=>
      this.isNeedOperation(record) ? record.isChecked = this.varIsAllChecked : null)
  }

  toggleRecord() {
    this.varIsAllChecked = this.stocktakeRecords.every(record => {
      return record.isChecked || !this.isNeedOperation(record);
    });
  }

  noSubmitStocktakeRecords(){
    return !this.stocktakeRecords||this.getCheckedRecord().length == 0;
  }

  getCheckedRecord(){
    return this.stocktakeRecords.filter(record =>record.isChecked);
  }

  ngOnInit() {
    this.pageSize = 20;
    this.currentPageNumber = 1;
  }

  getfetchDataPromise() {
    return this.stocktakeRecordService.getStocktakeRecordByPage;
  }

  updateData(res) {
    this.stocktakeRecords = res.data;
  }

  updateCurrentPageNumber(pageNumber) {
    this.currentPageNumber = pageNumber;
  }

  approve() {
    var selectStockIds=this.getCheckedRecord().map(value => value.stockId)
    this.stocktakeRecordService
      .batchApproveStock(selectStockIds)
      .subscribe(res=> {
        this.updateTheCurrentPageData();
      });
  }

  reject() {
    var selectStockIds=this.getCheckedRecord().map(value => value.stockId)
    var comment = this.rejectComment;
    var data = selectStockIds.map(function (item) {
        return {id: item, comment: comment}
    });
    this.stocktakeRecordService
      .batchRejectStock(data)
      .subscribe(res=> {
        this.updateTheCurrentPageData();
        this.rejectComment = null;
      });
  }

  clearErrorMsg() {
    this.showErrorMsg = false;
  }

  validateHasFillComment(){
    var hasComment = !!this.rejectComment;
    this.showErrorMsg = !hasComment;
  }

  hasFillComment(){
    return !!this.rejectComment;
  }

  cancelReject() {
    this.showErrorMsg = false;
    this.rejectComment = null;
  }

  getDate(value) {
    return new Date(value)
  }

  getStatueByType(type){
    if(type=='Approve'){
      return 'DONE';
    }else if(type=='Reject'){
      return 'REJECT';
    }else{
      return 'SUBMIT'
    }
  }

  isNeedOperation(record){
    return this.getStatueByType(record.examineStatusType) == 'SUBMIT';
  }

  private updateTheCurrentPageData() {
    this.getfetchDataPromise()(this.pageSize, this.currentPageNumber)
      .subscribe(res => {
        this.updateData(res)
      });
  }
}
