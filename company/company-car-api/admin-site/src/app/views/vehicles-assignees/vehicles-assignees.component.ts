import { search } from './../../../../../src/controllers/vehicle';
import { PagingComponent } from './../../components/paging-component/paging.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { VehiclesAssigneeApi } from '../../apis/vehicles-assignee-api';
import { assign } from 'rxjs/util/assign';
import { FleetAdminApi } from '../../apis/fleet-admin.api';
import { FleetAdmin } from '../../models/user.model';
import { VehiclesAssignee } from '../../models/vehicles-assignee.model';
import {$undefined} from 'ramda/templates/type';

@Component({
  selector: 'app-vehicles-assignees',
  templateUrl: './vehicles-assignees.component.html',
  styleUrls: ['./vehicles-assignees.component.scss']
})
export class VehiclesAssigneesComponent implements OnInit {

  constructor(private vehiclesAssigneeApi: VehiclesAssigneeApi, private fleetAdminApi: FleetAdminApi) {}

  @ViewChild(PagingComponent)
  pagingComponent: PagingComponent;

  vehiclesAssignees: VehiclesAssignee[];
  isAllChecked: boolean;
  fleetAdmins: FleetAdmin[];
  pageSize: number;
  currentPageNumber: number;
  searchContext: string;

  ngOnInit() {
    this.searchContext = '';
    this.vehiclesAssignees = [];
    this.initPage();
  }

  private initPage() {
    this.pageSize = 20;
    this.currentPageNumber = 1;
  }

  get IsAllChecked(): boolean {
    return this.isAllChecked;
  }

  set IsAllChecked(value: boolean) {
    this.isAllChecked = value;
    this.vehiclesAssignees.forEach(assigneeRecord => assigneeRecord.isChecked = this.isAllChecked);
  }

  search() {
    this.pagingComponent.search();
  }

  toggleRoleActive() {
    this.isAllChecked = this.vehiclesAssignees.every(assignee => {return assignee.isChecked; });
  }

  getSelectedVehiclesAssignees(): VehiclesAssignee[] {
    return this.vehiclesAssignees.filter(record => record.isChecked);
  }

  private updateTheCurrentPageData() {
    this.getfetchDataPromise()(this.pageSize, this.currentPageNumber)
      .subscribe(res => {
        this.updateData(res);
      });
  }

  clearAssigneeRecord() {
    const toAssigneVehicles = this.vehiclesAssignees.filter(record => record.isChecked);
    this.vehiclesAssigneeApi.save(toAssigneVehicles, undefined).subscribe(res => {
      if (res.status == 201) {
        this.updateTheCurrentPageData();
      }
    });
  }

  getfetchDataPromise() {
    return this.vehiclesAssigneeApi.listAssignee;
  }

  updateData(res) {
    this.vehiclesAssignees = res.datas.map(data => {
      return Object.assign(new VehiclesAssignee(), data);
    });
    this.isAllChecked = false;
  }

  updateCurrentPageNumber(pageNumber) {
    this.currentPageNumber = pageNumber;
  }
}
