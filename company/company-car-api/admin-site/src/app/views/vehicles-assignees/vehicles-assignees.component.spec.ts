import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehiclesAssigneesComponent } from './vehicles-assignees.component';

describe('VehiclesAssigneesComponent', () => {
  let component: VehiclesAssigneesComponent;
  let fixture: ComponentFixture<VehiclesAssigneesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehiclesAssigneesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehiclesAssigneesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
