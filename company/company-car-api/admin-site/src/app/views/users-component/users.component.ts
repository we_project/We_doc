import { Component } from '@angular/core';
import { UserAPI } from '../../apis/user.api';
import { UserModel,User } from '../../models/user.model';
import { MessageBoxService} from '../../services/message-box.service';
import 'rxjs/add/operator/catch';
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent {
  users: User[];
  constructor(private userService: UserAPI,private messageBoxService:MessageBoxService) { }
  isShowAddUserForm: boolean = false;
  ngOnInit() {
    this.updateUserList();
  }

  private updateUserList() {
    this.userService.listUsers().subscribe(users => {
      this.users = users;
    });
  }

  deleteUser(id: String) {
    this.userService.deleteUser(id).subscribe(r => {
      if (r.status == 200) {
        this.updateUserList();
      }
    });
  }

  submitForm(user:UserModel) {
    this.userService.newUser(user).subscribe(r => {
        if (r.status == 200) {
          this.updateUserList();
          this.toggleAddUserForm();
        }
      }, err => {
        this.messageBoxService.alert("Failed to add user.");
        this.toggleAddUserForm();
      },
      () => console.log('yay'))

  }

  toggleAddUserForm() {
    this.isShowAddUserForm = !this.isShowAddUserForm;
  }
}
