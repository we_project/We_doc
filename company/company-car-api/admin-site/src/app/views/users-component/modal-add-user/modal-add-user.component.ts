import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { UserModel } from '../../../models/user.model';

@Component({
  selector: 'app-modal-add-user',
  templateUrl: './modal-add-user.component.html',
  styleUrls: ['./modal-add-user.component.scss'],
})

export class ModalAddUser implements OnInit {
  @Output() closeEvent = new EventEmitter<string>();
  @Output() submitEvent = new EventEmitter<UserModel>();
  selectedRole: string = undefined;

  userModel: UserModel = {};
  showErrorMsg: Boolean = false;
  showMobileErrorMsg: Boolean = false;

  onCloseBtnPressed() {
    this.closeEvent.next();
  }

  private getPassword() {
    return btoa(encodeURIComponent(this.userModel.password).replace(/%([0-9A-F]{2})/g, (match, p1) => String.fromCharCode(Number(`0x${p1}`))));
  }
  onSubmitBtnPressed() {
    if (!this.isValidMobile() || !this.isValid()) {
      this.showErrorMsg = true;
      return;
    }

    this.userModel.dcPayId = this.userModel.loginId;
    this.userModel.empId = this.userModel.loginId;
    this.userModel.role = this.selectedRole;

    this.userModel.mobile = this.selectedRole === 'External Delegate'?this.userModel.mobile:null;


    this.submitEvent.next(Object.assign({}, this.userModel, {password: this.getPassword()}));
  }

  isValidMobile() {
    if (this.selectedRole === 'External Delegate' && !(/^\d{11}$/.test(this.userModel.mobile))) {
      this.showMobileErrorMsg = true;
      return false;
    }
    return true;
  }

  isValid() {
    return !!(this.selectedRole &&
      this.userModel.fullName &&
      this.userModel.password &&
      this.userModel.loginId);
  }

  clearErrorMsg() {
    this.showErrorMsg = false;
  }

  clearMobileErrorMsg() {
    this.showMobileErrorMsg = false;
  }

  constructor() {
  }

  ngOnInit(): void {
  }

}
