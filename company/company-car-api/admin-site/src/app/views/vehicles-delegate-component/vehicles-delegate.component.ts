import { PagingComponent } from './../../components/paging-component/paging.component';
import { VehicleDelegation } from './../../models/vehicle-delegation.model';
import { Component, ViewChild } from '@angular/core';
import { VehicleDelegateApi} from '../../apis/vehicle-delegate.api';
import { Vehicle} from '../../models/vehicle.model';
import { MessageBoxService} from '../../services/message-box.service';
import 'rxjs/add/operator/catch';

@Component({
  selector: 'vehicles-delegate',
  templateUrl: './vehicles-delegate.component.html',
  styleUrls: ['./vehicles-delegate.component.scss']
})
export class VehiclesDelegateComponent {
  vehicle: Vehicle;
  searchKeyWord: string;
  externalDelegateViewStatus: Boolean;
  internalDelegateViewStatus: Boolean;
  emptyResult: Boolean;
  vehicleDelegations: VehicleDelegation[];
  currentPageNumber: number;
  pageSize: number;

  @ViewChild(PagingComponent)
  pagingComponent: PagingComponent;

  ngOnInit() {
    this.searchKeyWord = '';
    this.externalDelegateViewStatus = true;
    this.externalDelegateViewStatus = true;
    this.emptyResult = false;
    this.pageSize = 8;
  }

  constructor(private vehicleDelegateApi: VehicleDelegateApi, private messageBoxService: MessageBoxService) {}

  search() {
    this.vehicleDelegateApi.searchVehicle(this.searchKeyWord)
      .subscribe( res => {
        this.emptyResult = !res[0];
        this.externalDelegateViewStatus = true;
        this.internalDelegateViewStatus = true;
        this.vehicle = res[0];
      });
  }

  updateExternalDelegate(externalDelegateLoginId) {
    this.vehicleDelegateApi
      .updateExternalDelegate(externalDelegateLoginId, this.vehicle.plateNo || this.vehicle.legacyPlateNo || this.vehicle.vin)
      .subscribe(res => {
        this.vehicle.externalDelegate.loginId = externalDelegateLoginId;
        this.externalDelegateViewStatus = true;
        this.pagingComponent.getNewPageData(this.pageSize, this.currentPageNumber);
      });
  }

  updateInternalDelegate(internalDelegateCNNumber) {
    this.vehicleDelegateApi
      .updateInternalDelegate(internalDelegateCNNumber, this.vehicle.plateNo || this.vehicle.legacyPlateNo || this.vehicle.vin)
      .subscribe(res => {
        this.vehicle.internalDelegate.empId = internalDelegateCNNumber;
        this.internalDelegateViewStatus = true;
        this.pagingComponent.getNewPageData(this.pageSize, this.currentPageNumber);
      }, err => {
        this.messageBoxService.alert(err.json().message);
      });
  }

  getFetchDataPromise() {
    return this.vehicleDelegateApi.findAllDelegatesByPage;
  }

  updateData(res) {
    this.vehicleDelegations = res.data;
  }

  updateCurrentPageNumber(pageNumber) {
    this.currentPageNumber = pageNumber;
  }
}
