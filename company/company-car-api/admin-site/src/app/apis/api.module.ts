import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserAPI } from './user.api';
import { StocktakeRecordApi } from './stocktake-record.api';
import { VehiclesAssigneeApi } from './vehicles-assignee-api';
import { FleetAdminApi } from './fleet-admin.api';
import { VehicleDelegateApi } from './vehicle-delegate.api';

@NgModule({
  imports: [
    CommonModule,
  ],
  providers: [ UserAPI, StocktakeRecordApi, VehiclesAssigneeApi, FleetAdminApi, VehicleDelegateApi],
  declarations: []
})
export class ApiModule { }
