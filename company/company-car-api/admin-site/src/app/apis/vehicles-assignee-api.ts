import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { toJson } from '../utils/to-json';
import { TokenStore } from '../utils/token-store';
import { VehiclesAssignee } from '../models/vehicles-assignee.model';
import { FleetAdmin } from '../models/user.model';
import { environment } from '../../environments/environment';


@Injectable()
export class VehiclesAssigneeApi {
  constructor(private http: Http) { }

  listAssignee = (pageSize: number, pageNum: number, searchContext = ''): Observable<{vehicleDelegationTotalCount: number, datas: VehiclesAssignee[]}> => {
    let url = `${environment.baseUrl}/api/vehicles/assignees?page-size=${pageSize}&&page-number=${pageNum}`;
    if (searchContext.trim().length !== 0) {
      url = `${url}&&q=${searchContext}`;
    }
    return this.http.get(url, TokenStore.wrapTokenToHeaders()).map(toJson);
  }

  save(toAssignVehicles: VehiclesAssignee[], selectedFleetAdmin: FleetAdmin): Observable<Response> {
    const url = `${environment.baseUrl}/api/vehicles/assignees/batch-submission`;
    return this.http.post(url,
      toAssignVehicles.map((ve) =>  { return {vehicleId: ve.id, assignee: selectedFleetAdmin}; }  ),
      TokenStore.wrapTokenToHeaders());
  }
}
