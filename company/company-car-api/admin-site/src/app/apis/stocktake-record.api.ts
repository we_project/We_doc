import { Injectable } from '@angular/core';
import { Http,  Headers, Response } from '@angular/http';
import * as FileSaver from 'file-saver';
import { Observable } from 'rxjs/Observable';
import { toJson } from '../utils/to-json';
import { StocktakeRecord,StocktakeRecordResponse, StocktakeRecordRejectItem } from '../models/stocktake-record.model';
import { TokenStore } from 'app/utils/token-store';
import { environment } from '../../environments/environment';


@Injectable()
export class StocktakeRecordApi {
  constructor(private http: Http) { }

  getStocktakeRecordByPage = (pageSize, pageNumber): Observable<StocktakeRecordResponse> => {
    return this.http.get(`${environment.baseUrl}/api/stocktake-record?pageSize=${pageSize}&pageNumber=${pageNumber}`,
      TokenStore.wrapTokenToHeaders()).map(toJson);
  }

  rejectStock(stockId: string): Observable<Response> {
    const url = `${environment.baseUrl}/api/vehicle-stocks/vehicle/${stockId}/status/rejection`;
    return this.http.post(url, {}, TokenStore.wrapTokenToHeaders());
  }

  batchRejectStock(stockIds: StocktakeRecordRejectItem[]): Observable<Response> {
    const url = `${environment.baseUrl}/api/vehicle-stocks/vehicle/status/reject-batch`;
    return this.http.post(url, stockIds, TokenStore.wrapTokenToHeaders());
  }

  approveStock(stockId: string): Observable<Response> {
    const url = `${environment.baseUrl}/api/vehicle-stocks/vehicle/${stockId}/status/approval`;
    return this.http.post(url, {}, TokenStore.wrapTokenToHeaders());
  }

  batchApproveStock(stockIds: string[]): Observable<Response> {
    const url = `${environment.baseUrl}/api/vehicle-stocks/vehicle/status/approve-batch`;
    return this.http.post(url, stockIds, TokenStore.wrapTokenToHeaders());
  }

  exportFile(){
    const headers = new Headers();
    this.http.get(`${environment.baseUrl}/api/vehicles/stock-finial-report`, { headers: headers, responseType: 3 })
      .map((response) => {
        return{
          data : response['_body'],
          filename : 'company_car_management_report.xlsx',
        };
      })
      .subscribe(
        (res) => {
          FileSaver.saveAs(res.data, res.filename);
        }
      );
  }
}
