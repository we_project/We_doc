import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { FleetAdmin } from '../models/user.model';
import { Observable } from 'rxjs/Observable';
import { TokenStore } from '../utils/token-store';
import { toJson } from '../utils/to-json';
import { environment } from '../../environments/environment';


@Injectable()
export class FleetAdminApi {
  constructor(private http: Http) { }

  getFleetAdminList(): Observable<FleetAdmin[]> {
    const url = `${environment.baseUrl}/api/users/fleet-admins`;
    return this.http.get(url, TokenStore.wrapTokenToHeaders()).map(toJson);
  }
}
