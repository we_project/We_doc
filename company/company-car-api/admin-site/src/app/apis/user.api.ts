import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { TokenStore } from 'app/utils/token-store';
import { toJson } from '../utils/to-json';
import { UserModel, LoginResponse, LoginBody, User } from '../models/user.model';
import { environment } from '../../environments/environment';

@Injectable()
export class UserAPI {

  constructor(private http: Http) { }

  loginWithUsernameAndPassword(loginBody: LoginBody): Observable<LoginResponse> {
    return this.http.post(`${environment.baseUrl}/api/login`, loginBody).map(toJson);
  }

  auth(): Observable<Response> {
    return this.http.get(`${environment.baseUrl}/api/secret`, TokenStore.wrapTokenToHeaders());
  }

  listUsers(): Observable<User[]> {
    return this.http.get(`${environment.baseUrl}/api/users`, TokenStore.wrapTokenToHeaders()).map(toJson);
  }

  deleteUser(id: String) {
    return this.http.delete(`${environment.baseUrl}/api/user/${id}`, TokenStore.wrapTokenToHeaders());
  }

  newUser(user: UserModel) {
    return this.http.post(`${environment.baseUrl}/api/user`, user, TokenStore.wrapTokenToHeaders());
  }
}
