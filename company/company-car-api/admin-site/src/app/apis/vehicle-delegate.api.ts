import { Injectable}  from '@angular/core';
import { Observable}  from 'rxjs/Observable';
import { Http, Response}  from '@angular/http';
import { toJson}  from '../utils/to-json';
import { TokenStore}  from '../utils/token-store';
import { Vehicle } from '../models/vehicle.model';
import { environment } from '../../environments/environment';


@Injectable()
export class VehicleDelegateApi {
  constructor(private http: Http) { }

  searchVehicle = (keyword): Observable<Vehicle> => {
    return this.http.get(`${environment.baseUrl}/api/vehicles/search?q=${keyword}`, TokenStore.wrapTokenToHeaders()).map(toJson);
  };

  findAllDelegatesByPage = (pageSize: number, pageNumber: number) => {
    return this.http.get(`${environment.baseUrl}/api/vehicle-delegates?pageSize=${pageSize}&&pageNumber=${pageNumber}`, TokenStore.wrapTokenToHeaders()).map(toJson);
  }

  updateExternalDelegate = (externalDelegateLoginId: string, vehicleStr: string): Observable<Response> => {
    const body = {
      vehicleStr: vehicleStr,
      externalDelegate: externalDelegateLoginId
    };

    return this.http.post(`${environment.baseUrl}/api/vehicle-external-delegates`, body, TokenStore.wrapTokenToHeaders());
  };

  updateInternalDelegate = (internalDelegateLoginId: string, vehicleStr: string): Observable<Response> => {
     const body = {
      vehicleStr: vehicleStr,
      internalDelegate: internalDelegateLoginId
    };

    return this.http.post(`${environment.baseUrl}/api/vehicle-internal-delegates`, body, TokenStore.wrapTokenToHeaders());
  }
}
