import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  constructor(public route: Router){}

  onNgInit() {

  }

  logout() {
    localStorage.removeItem('token');
    this.route.navigate(['login']);
  }

  isNavShow(): boolean {
    return !!localStorage.getItem('token');
  }

  isActive = (path)=> {
    return this.route.url.indexOf(path) >= 0;
  }
  
  getFullName = ()=> {
    return localStorage.getItem('fullName');
  }
 
}
