import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { MDBBootstrapModule } from './typescripts/angular-bootstrap-md/free';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-route';
import { ApiModule } from './apis/api.module';

import { LoginComponent } from './views/login-component/login.component';
import { ReportComponent } from './views/report-component/report.component';
import { UsersComponent } from './views/users-component/users.component';
import { StocktakeRecordComponent } from './views/stocktake-record-component/stocktake-record.component';
import { VehiclesDelegateComponent } from './views/vehicles-delegate-component/vehicles-delegate.component';
import { VehiclesAssigneesComponent } from './views/vehicles-assignees/vehicles-assignees.component';
import { ModalAddUser } from './views/users-component/modal-add-user/modal-add-user.component';
import { ImportDataComponent } from './views/import-data-component/import-data.component';

import { RequireLoginGuardService } from './services/require-login-guard.service';
import { MessageBoxService } from './services/message-box.service';

import { ModalBoxComponent } from './components/modal-box-component/modal-box.component';
import { SelectAssigneeComponent } from './components/select-assignee-component/select-assignee.component';
import { HandleAssigneeComponent } from './components/handle-assignee-component/handle-assignee.component';
import { PagingComponent } from './components/paging-component/paging.component';
import { ImagePreviewerComponent } from './components/image-previewer-component/image-previewer.component';
import { MessageBoxComponent } from './components/message-box-component/message-box.component';
import { TabTitleComponent } from './components/tab-title-component/tab-title.component';


@NgModule({
  declarations: [
    AppComponent,
    ModalAddUser,
    VehiclesAssigneesComponent,
    LoginComponent,
    UsersComponent,
    ReportComponent,
    StocktakeRecordComponent,
    VehiclesAssigneesComponent,
    VehiclesDelegateComponent,
    ImportDataComponent,
    ModalBoxComponent,
    SelectAssigneeComponent,
    HandleAssigneeComponent,
    PagingComponent,
    MessageBoxComponent,
    TabTitleComponent,
    ImagePreviewerComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    MDBBootstrapModule.forRoot(),
    HttpClientModule,
    AppRoutingModule,
    ApiModule
  ],
  providers: [RequireLoginGuardService,MessageBoxService],
  bootstrap: [AppComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule { }
