export interface VehicleDelegation {
  plateNo: string;
  vin: string;
  legacyPlateNo: string;
  externalDelegate: {
    loginId: string,
    mobilePhone: string,
  };
  internalDelegate: {
    empId: string,
    businessPhone: string,
    mobilePhone: string
  };
}
