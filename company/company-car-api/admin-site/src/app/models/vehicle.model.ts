export interface Vehicle {
  id: string,
  plateNo: string,
  legacyPlateNo: string,
  stockStatus: string,
  usage: string,
  model: string,
  vin: string,
  externalDelegate: {
    loginId: string
  },
  internalDelegate: {
    empId: string
  }
}
