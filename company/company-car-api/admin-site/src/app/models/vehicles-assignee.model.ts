import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { FleetAdmin } from './user.model';

export class VehiclesAssignee {
  id: string;
  isChecked: boolean;
  plateNumber: string;
  vin: string;
  make: string;
  model: string;
  usage: string;
  counted: string;
  assignee: FleetAdmin;

  AssignTo(selectedFleetAdmin: FleetAdmin) {
    this.assignee  = selectedFleetAdmin;
  }

}
