export interface UserModel {
  loginId?: string;
  empId?: string;
  dcPayId?: string;
  password?: string;
  fullName?: string;
  role?: string;
  mobile?: string;
}
export class FleetAdmin {
  loginId: string;
  fullName: string;
  id: string;
  role: string;
}

export class LoginResponse {
  token?: string;
  role?: string;
  fullName: string;
}

export interface LoginBody {
  loginId?: string;
  password?: string;
}


export interface User {
  loginId: string;
  fullName: string;
  id: string;
  role: string;
}
