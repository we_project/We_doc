export interface StocktakeRecord {
  stockId: string;
  commitTime: number;
  plateNo: string;
  vin: string;
  make: string;
  model: string;
  usage: string;
  province: string;
  city: string;
  zone: string;
  detaileAddress: string;
  exteriorColor: string;
  interiorColor: string;
  isChecked: boolean;
  insuranceValid: boolean;
  annualCheckOverdue: boolean;
  warningTriangle: boolean;
  fireExtinguisher: boolean;
  floorMats: boolean;
  mileage: number;
  damageLevel: string;
  carIsMobileAndUsable: boolean;
  leftFrontImage: string;
  rightBackImage: string;
  vehicleLicenseImage: string;
  odometerImage: string;
  damagePhotos: {
    site: string;
    imageId: string
  }[];
  examineStatusType: string;
  submitterFullName: string;
  submitterRole: string;
  vehicleUserName: string;
  rejectComment: string;
}

export interface StocktakeRecordResponse {
  data: StocktakeRecord[];
  recordCount: number;
}

export interface StocktakeRecordRejectItem {
  id: string;
  comment: string;
}
