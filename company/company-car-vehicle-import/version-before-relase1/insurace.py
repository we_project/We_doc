import csv
from json import dumps


def build_insurance(record):
    return {
        'plateNumber': record['Numberplate'],
        'lnsur_accident_tel': record['lnsur accident tel.'],
        'roadside_assistance': record['Roadside Assistance'],
        'lnsur_End': record['lnsur End'],
        'insurance_company': record['Insurance company'],
        'annual_inspect_due_date': record['Annual Inspection due  date'],
    }

def issurance_not_empty(record):
    return record and record['Insurance company'] != ''

with open('Test file to Wu Yang_20170926.csv', encoding='utf-8') as businessCarsFile:
    reader = csv.DictReader(businessCarsFile)
    with open('./insurance.json', mode='w', encoding='utf8') as json_file:
        json_file.write(dumps([build_insurance(i) for i in filter(issurance_not_empty ,reader)]))
