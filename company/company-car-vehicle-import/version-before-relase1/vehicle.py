import csv
from json import dumps

usage_map_dict={
'Chengdu Regional Pool (CH)':'Car Pool',
'Beijing Pool Car':'Car Pool',
'Shanghai Regional Pool (SH)':'Car Pool',
'Employee Entitlement Car':'Employee Entitlement Car',
'Department Car Pool':'Department Car',
'Family Car':'Family Car'
}

def get_usage(record):
    if record['Purpose of Usage'] not in usage_map_dict.keys():
        return 'Business Car'
    
    return usage_map_dict[record['Purpose of Usage']]


def build_item(record):
    v = {
        'usage': get_usage(record),
        'legacyPlateNo': record['Numberplate'],
        'plateNo': '',
        'VIN': record['VIN'],
        'engineNo': record['Engine No '],
        'registrationLocation': record['Registration Location'],
        'registrationDate': record['Registration Date'],
        'registrationCompany': record['Registered Under'],
        'carType': {
            'make': '',
            'model': record['Model'],
        },
        'mileage': {
            'displayValue': record['Mileage'],
            'takenDate': record['Mileage Date'],
        },
        'location': '',
        'user': {
            'CNNumber': record['CNNumber'],
            'details': '',
        },
        'contractType': record['Contract Type'],
    }
    return v


with open('Test file to Wu Yang_20170926.csv', encoding="utf-8") as file:
    reader = csv.DictReader(file)
    vehicles = [build_item(r) for r in reader]
    json_str = dumps(vehicles)
    with open('./vehicles.json', mode='w', encoding='utf8') as json_file:
        json_file.write(json_str)
