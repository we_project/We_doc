import csv
from json import dumps

business_car = {}


def get_usage(record):
    if 'Employee Entitlement Car' == record['Purpose of Usage']:
        return record['Purpose of Usage']
    if record['Purpose of Usage'] == 'Family Car':
        return record['Purpose of Usage']

    if record['Numberplate'] not in business_car.keys():
        return 'Car Pool'

    if business_car[record['Numberplate']]['Purpose Type'] == 'Traffic Regulation Vehicle(L3 and above)':
        return 'TRV'

    if business_car[record['Numberplate']]['Purpose Type'] == 'Department Car':
        return 'Department Car'

    return 'Business Car'


def build_item(record):
    v = {
        'usage': get_usage(record),
        'legacyPlateNo': record['Numberplate'],
        'plateNo': '',
        'VIN': record['VIN'],
        'engineNo': record['Engine No '],
        'registrationLocation': record['Registration Location'],
        'registrationDate': record['Registration Date'],
        'registrationCompany': record['Registered Under'],
        'carType': {
            'make': '',
            'model': record['Model'],
        },
        'mileage': {
            'displayValue': record['Mileage'],
            'takenDate': record['Mileage Date'],
        },
        'location': '',
        'user': {
            'CNNumber': record['CNNumber'],
            'details': '',
        },
        'contractType': record['Contract Type'],
    }
    return v


with open('data/VAFReport_2017-07-25_with Insurance/csv-Table 1.csv', encoding='utf-8') as businessCarsFile:
    reader = csv.DictReader(businessCarsFile)
    for i in reader:
        if i['Alloc. Plate No'] is not '':
            business_car[i['Alloc. Plate No']] = i

with open('data/CoCaMa_Cardummp20170724/csv-Table 1.csv', encoding="utf-8") as file:
    reader = csv.DictReader(file)
    vehicles = [build_item(r) for r in reader]
    json_str = dumps(vehicles)
    with open('./vehicles.json', mode='w', encoding='utf8') as json_file:
        json_file.write(json_str)
