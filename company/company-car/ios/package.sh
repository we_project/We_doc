#!/bin/sh

### Execute this script under project path. (../)

echo "===================Build for app store ======================="
PROVISIONING_PROFILE="ios/buildHelpers/mobleProvisioningProfile/OPS_Thoughtworks_Vehiclebutler_Dis.mobileprovision"
ExportOptionsPlist="ios/buildHelpers/OptionsPlist/enterpriseExportOptions.plist"

#Add git commit number
git=`sh /etc/profile; which git`
appBuild=`"$git" rev-parse --short HEAD`

WORKSPACE_NAME="ios/companycar.xcodeproj"
SCHEME_NAME="companycar"
OUTPUTDIR="$PWD/ios/build"

# Release | Debug
CONFIGURATION="Release"
PROJECT_FOLDER="../ "
BUILD_FOLDER="$PWD/ios/build"
BUNDLE_IDENTIFIER=""
INFO_PLIST=""

# DO NOT EDIT
Profile_UUID=`grep UUID -A1 -a $PROVISIONING_PROFILE | grep -io "[-A-Z0-9]\{36\}"`

COPY_PROVISIONING_PROFILE_COMMAND="cp $PROVISIONING_PROFILE ~/Library/MobileDevice/Provisioning\ Profiles/$Profile_UUID.mobileprovision"

now=$(date +"%Y_%m_%d_%H_%M_%S")

/usr/libexec/PlistBuddy -c "Set :CFBundleVersion ${appBuild}" ios/$SCHEME_NAME/Info.plist

XCODE_BUILD_WORKSPACE_COMMAND="xcodebuild \
-project '${WORKSPACE_NAME}' \
-destination 'generic/platform=iOS' \
-scheme '${SCHEME_NAME}' \
-sdk iphoneos \
-archivePath '${BUILD_FOLDER}/${SCHEME_NAME}.xcarchive' \
-config '${CONFIGURATION}' \
clean archive "


XCODE_EXPORT_IPA="xcodebuild -exportArchive \
-archivePath ${BUILD_FOLDER}/${SCHEME_NAME}.xcarchive \
-exportPath '${BUILD_FOLDER}/${SCHEME_NAME}-${now}' \
-exportOptionsPlist ${ExportOptionsPlist} "

BUNDLE_VERSION=$(/usr/libexec/PlistBuddy -c 'Print CFBundleVersion' "iOS-Startup/Info.plist")
BUNDLE_SHORT_VERSION=$(/usr/libexec/PlistBuddy -c 'Print CFBundleShortVersionString' "iOS-Startup/Info.plist")

# 组装Bugly默认识别的版本信息(格式为CFBundleShortVersionString(CFBundleVersion), 例如: 1.0(1))
BUGLY_APP_VERSION="${BUNDLE_SHORT_VERSION}(${BUNDLE_VERSION})"

UPLOAD_DSYM="sh ./BuglydSYMUploader/dSYMUpload.sh '6d4b269487' '95b5241a-9f61-4920-bbbd-1d1eb879c397' 'com.daimler.ready2share' '${BUGLY_APP_VERSION}' ./build/iOS-Startup.xcarchive/dSYMs ./build/archivedDSYMs"

rm -rf $BUILD_FOLDER

echo "====Create folder if needed==="
[ ! -d $OUTPUTDIR  ] && mkdir -p $OUTPUTDIR

echo "====Copy provisioning profile==="
eval $COPY_PROVISIONING_PROFILE_COMMAND

if [ ! -z "${BUNDLE_IDENTIFIER// }" ] && [ ! -z "${INFO_PLIST// }" ] ; then
    echo "====modify bundle identifier===="
    /usr/libexec/PlistBuddy -c "Set :CFBundleIdentifier ${BUNDLE_IDENTIFIER}" $INFO_PLIST
fi

echo "====Start build workspace===="
eval $XCODE_BUILD_WORKSPACE_COMMAND

echo "====Start code sign===="
eval $XCODE_EXPORT_IPA 

#echo "==============上传到FIR====================="
#fir p $OUTPUTDIR/${SCHEME_NAME}-${now}/${SCHEME_NAME}.ipa -T a29a5aa6b7b3e5c9146f10b646178923

echo "===Finished. Total time: ${SECONDS}s==="
