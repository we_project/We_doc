/**
 * @flow
 */
import React from 'react'
import { View, Text, StyleSheet } from 'react-native'

import { getRem } from '../styles/getRem'
import Localized from '../utils/Localized'

const strings = Localized.Strings

const styles = StyleSheet.create({
  vehicleCard: {
    borderTopWidth: getRem(1),
    borderTopColor: '#d8d8d8',
    paddingTop: getRem(18),
    paddingLeft: getRem(20),
    paddingRight: getRem(20),
    paddingBottom: getRem(2),
    backgroundColor: '#ffffff',
    marginBottom: getRem(12),
  },
  cardItem: {
    flexDirection: 'row',
    marginBottom: getRem(16),
  },
  cardLeft: {
    width: getRem(100),
    fontSize: getRem(14),
    color: '#333333',
  },
  cardRight: {
    flex: 1,
    textAlign: 'right',
    fontSize: getRem(14),
    fontWeight: '300',
    lineHeight: getRem(16),
    color: '#666666'
  },
})
type Props = {
  data: Array,
}

function VehicleInfoCard(props: Props) {
  const { plateNumber, model, vin, provincialCapital } = props.data
  return (
    <View style={styles.vehicleCard}>
      <View style={styles.cardItem}>
        <Text style={styles.cardLeft}>{strings.plateNoVehicleInfo}</Text>
        <Text style={styles.cardRight}>
          {`${provincialCapital}${plateNumber.slice(0, 1)} ${plateNumber.slice(1, plateNumber.length)}`}
        </Text>
      </View>
      <View style={styles.cardItem}>
        <Text style={styles.cardLeft}>{strings.carModel}</Text>
        <Text style={styles.cardRight}>{model}</Text>
      </View>
      <View style={styles.cardItem}>
        <Text style={[styles.cardLeft, { width: getRem(120) }]}>{strings.vinVehicleInfo}</Text>
        <Text style={styles.cardRight}>{vin}</Text>
      </View>
    </View>
  )
}

export default VehicleInfoCard
