/**
 * @flow
 */
import React, { Component } from 'react'
import { View, Text, Image, StyleSheet, TouchableWithoutFeedback } from 'react-native'

import Localized from '../utils/Localized'
import { getRem } from '../styles/getRem'
import images from '../images'

const strings = Localized.Strings
const styles = StyleSheet.create({
  violationInfoCard: {
    flex: 1,
    marginTop: getRem(5),
  },
  containerHeaderWithBg: {
    backgroundColor: '#ffffff',
  },
  containerDetailWithBg: {
    backgroundColor: 'rgba(244, 244, 244, 0.5)',
  },
  withMargin: {
    marginLeft: getRem(8),
    marginRight: getRem(8),
  },
  violationInfoHeader: {
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    flexDirection: 'row',
    borderBottomWidth: getRem(1),
    borderColor: '#e7e7e7',
  },
  violationBriefInfo: {
    flex: 12,
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    flexDirection: 'row',
    paddingBottom: getRem(10),
    paddingTop: getRem(10),
  },
  directionArrow: {
    marginLeft: getRem(10),
  },
  directionImage: {
    flex: 1,
    alignItems: 'flex-end',
    paddingRight: getRem(20),
    paddingTop: getRem(16),
  },
  leaveLeftSpace80: {
    flex: 1,
    alignItems: 'flex-end',
  },
  text: {
    marginLeft: getRem(5),
    paddingTop: getRem(3),
    paddingBottom: getRem(3),
    color: '#666666',
    fontSize: getRem(14),
  },
  withBorder: {
    paddingLeft: getRem(5),
    paddingRight: getRem(5),
    borderWidth: getRem(1),
    borderRadius: getRem(4),
    borderColor: '#9b9b9b',
    fontSize: getRem(12),

  },
  accordionItem: {
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    flexDirection: 'row',
    flex: 12,
    padding: getRem(12),
    borderBottomWidth: getRem(1),
    borderColor: '#e7e7e7',
    backgroundColor: 'rgba(244, 244, 244, 0.5)',
    borderBottomColor: 'rgba(33,33,33,0.1)',
  },
  itemTitle: {
    flex: 3,
    color: '#333333',
  },
  itemContent: {
    flex: 9,
    lineHeight: getRem(20),
    color: '#666',

  }
})
type Props = {
  data: any,
  children: any
}

class ViolationAccordionPanel extends Component {
  constructor(props: Props) {
    super(props)
    this.state = {
      isExpand: false,
    }
  }

  props: Props

  handleExpand = () => {
    this.setState({ isExpand: !this.state.isExpand })
  }

  render() {
    const { data } = this.props
    return (
      <View style={styles.violationInfoCard}>
        <TouchableWithoutFeedback style={styles.containerHeaderWithBg} onPress={this.handleExpand}>
          <View style={[styles.violationInfoHeader, styles.withMargin]}>
            <View style={styles.violationBriefInfo}>
              <View>
                <Text style={[styles.withBorder, styles.text]}>
                  {strings.violationPoint}: {data.demeritPoint}
                </Text>
              </View>
              <View>
                <Text style={[styles.withBorder, styles.text]}>
                  {strings.violationFines}:{data.fineAmount}
                </Text>
              </View>
              <View style={styles.leaveLeftSpace80}>
                <Text style={styles.text}>
                  {data.violationDate} {data.violationTime}
                </Text>
              </View>
            </View>
            <View style={styles.directionImage}>
              <Image
                style={styles.directionArrow}
                source={this.state.isExpand ? images.downArrow : images.rightArrow}
              />
            </View>
          </View>
        </TouchableWithoutFeedback>
        <View
          onLayout={(e) => { this.setState({ height: e.nativeEvent.layout.height }) }}
          ref='AccordionContent'
          style={styles.containerDetailWithBg}
        >
          {this.state.isExpand && <View style={styles.withMargin}>
            <View style={styles.accordionItem}>
              <Text style={styles.itemTitle}>{strings.violationType}</Text>
              <Text style={styles.itemContent} >{data.violationType}</Text>
            </View>
            <View style={styles.accordionItem}>
              <Text style={styles.itemTitle}>{strings.violationLocation}</Text>
              <Text style={styles.itemContent}>{data.violationLocation}</Text>
            </View>
            <View style={styles.accordionItem}>
              <Text style={styles.itemTitle}>{strings.violationDetail}</Text>
              <Text style={styles.itemContent}>{data.violationDetail}</Text>
            </View>
          </View>}
        </View>
      </View>
    )
  }
}

export default ViolationAccordionPanel
