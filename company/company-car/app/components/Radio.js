/**
 * @flow
 */
import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'

import { getRem } from '../styles/getRem'

const styles = StyleSheet.create({
  root: {
    flexDirection: 'row',
    borderBottomWidth: getRem(1),
    borderBottomColor: 'rgba(51, 51, 51, 0.1)',
    height: getRem(52),
    alignItems: 'center',
  },
  showText: {
    flex: 1,
    fontSize: getRem(14),
    color: '#333333',
  },
  radioView: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  radio: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  radioButton: {
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#d8d8d8',
    borderRadius: getRem(16),
    width: getRem(16),
    height: getRem(16),
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: getRem(10),
  },
  radioButtonActive: {
    borderRadius: getRem(8),
    width: getRem(8),
    height: getRem(8),
    backgroundColor: '#1f88d8',
  },
  label: {
    fontSize: getRem(14),
    fontWeight: '300',
    lineHeight: getRem(16),
    textAlign: 'right',
    color: '#666666',
  }
})
type Props = {
  data: Array,
  value: any,
  onChange: () => void,
  showText: string,
  noBorder: boolean,
}
function Radio(props: Props) {
  const { data, onChange, value, showText, noBorder } = props
  return (
    <View style={[styles.root, noBorder && { borderBottomWidth: 0 }]}>
      <Text style={styles.showText}>{showText}</Text>
      <View style={styles.radioView}>
        {data.map((v) => (
          <TouchableOpacity
            key={v.value}
            style={styles.radio}
            onPress={() => { onChange(v.value) }}
          >
            <View style={styles.radioButton}>
              {value === v.value ? <View style={styles.radioButtonActive} /> : null}
            </View>
            <Text style={styles.label}>{v.label}</Text>
          </TouchableOpacity>
        ))}
      </View>
    </View>
  )
}

export default Radio
