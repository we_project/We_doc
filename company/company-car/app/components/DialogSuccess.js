/**
 * @flow
 */
import React, { Component } from 'react'
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
  Modal
} from 'react-native'

import { getRem } from '../styles/getRem'
import Localized from '../utils/Localized'
import images from '../images'

const strings = Localized.Strings

const styles = StyleSheet.create({
  mask: {
    backgroundColor: 'rgba(0,0,0,0.7)',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  dialog: {
    width: getRem(295),
    height: getRem(306),
    backgroundColor: 'white',
    borderRadius: getRem(4),
    padding: getRem(20)
  },
  innTop: {
    alignItems: 'center',
  },
  imgSuccess: {
    width: getRem(80),
    height: getRem(80),
    marginTop: getRem(20),
  },
  txtSuccess: {
    fontWeight: 'bold',
    color: '#333333',
    fontSize: getRem(18),
    marginTop: getRem(20),
    marginBottom: getRem(16),
  },
  desc: {
    fontSize: getRem(12),
    textAlign: 'center',
    lineHeight: getRem(16),
    color: '#999999'
  },
  btn: {
    backgroundColor: '#1f88d9',
    height: getRem(40),
    borderRadius: getRem(4),
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: getRem(36),
  },
  btnTxt: {
    color: '#fff',
    fontSize: getRem(16),
  }
})


type Props = {
  onClose: () => void
}

class DialogSuccess extends Component {

  static navigationOptions = {
    header: null
  }

  constructor(props: Props) {
    super(props)
    this.state = {
      isShow: false,
    }
  }

  onClose = () => {
    this.props.onClose()
  }

  props: Props

  render() {
    return (
      <Modal
        transparent
        ref='modal'
        modalVisible
        onRequestClose={this.onClose}
        animationType={'fade'}
      >
        <View style={styles.mask}>
          <View style={styles.dialog}>
            <View style={styles.innTop}>
              <Image source={images.success} style={styles.imgSuccess} />
              <Text style={styles.txtSuccess}>{strings.stockSuccess}</Text>
            </View>
            <View style={styles.innDown}>
              <Text style={styles.desc}>{strings.stockSuccessDesc}</Text>
              <TouchableOpacity onPress={this.onClose} style={styles.btn}>
                <Text style={styles.btnTxt}>{strings.stockSuccessBtnTxt}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    )
  }
}

export default DialogSuccess
