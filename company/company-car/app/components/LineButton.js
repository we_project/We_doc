import React from 'react'
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native'

import { getRem } from '../styles/getRem'

const styles = StyleSheet.create({
  buttonContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    height: getRem(40),
    borderWidth: getRem(1),
    backgroundColor: '#ffffff',
    borderRadius: getRem(3),
  },
  buttonText: {
    fontSize: getRem(16),
    fontWeight: '300',
  },
  enabled: {
    color: 'rgb(31, 136, 216)',

  },
  disabled: {
    color: 'rgb(153, 153, 153)',
  },
  enabledBorder: {
    borderColor: '#1f88d8',
  },
  disabledBorder: {
    borderColor: '#d8d8d8',
  }
})

type Props = {
  onPress: () => void,
  text: string,
  disabled: boolean,
  style: any,
}

function LineButton(props: Props) {
  return (
    <TouchableOpacity
      onPress={() => { props.onPress() }}
    >
      <View style={[styles.buttonContainer,
        props.disabled ? styles.disabledBorder : styles.enabledBorder, props.style]}
      >
        <Text style={[styles.buttonText, props.disabled ? styles.disabled : styles.enabled]}>
          {props.text}</Text>
      </View>
    </TouchableOpacity>
  )
}

export default LineButton
