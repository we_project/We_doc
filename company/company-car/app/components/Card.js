/**
 * @flow
 */
import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'

import { getRem } from '../styles/getRem'

const styles = StyleSheet.create({
  vehicleInfoCard: {
    borderRadius: getRem(4),
    backgroundColor: '#ffffff',
    marginTop: getRem(12),
    marginLeft: getRem(8),
    marginRight: getRem(8),
  },
  vehicleInfoHeader: {
    paddingLeft: getRem(12),
    borderBottomWidth: getRem(1),
    borderBottomColor: '#e7e7e7',
    paddingTop: getRem(15),
    paddingBottom: getRem(12),
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
  carIcon: {
    marginRight: getRem(10),
  },
  title: {
    flex: 1,
  },
  mainTitle: {
    fontWeight: 'bold',
    color: '#333333',
    fontSize: getRem(16),
  },
  subTitle: {
    fontSize: getRem(10),
    fontWeight: '300',
    color: '#9b9b9b',
    lineHeight: getRem(16),
    marginTop: getRem(5),
  },
  vehicleInfoItemContainer: {
    paddingLeft: getRem(12),
    paddingRight: getRem(12),
  },
})
type Props = {
  title: string,
  icon: any,
  children: any,
  subTitle: string,
}
function Card(props: Props) {
  return (
    <View style={styles.vehicleInfoCard}>
      <View style={styles.vehicleInfoHeader}>
        <Image style={styles.carIcon} source={props.icon} />
        <View style={styles.title}>
          <Text style={styles.mainTitle}>{props.title}</Text>
          {props.subTitle && <Text style={styles.subTitle}>{props.subTitle}</Text>}
        </View>
      </View>
      <View style={styles.vehicleInfoItemContainer}>
        {props.children}
      </View>
    </View>
  )
}

export default Card
