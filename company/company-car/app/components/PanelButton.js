import React from 'react'
import { View, Text, TouchableHighlight, StyleSheet } from 'react-native'

import { getRem } from '../styles/getRem'

const styles = StyleSheet.create({
  root: {
    justifyContent: 'center',
    margin: getRem(20),
    borderRadius: getRem(3),
  },
  buttonContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    height: getRem(40),
    width: getRem(335),
    borderRadius: getRem(3),
  },
  buttonText: {
    color: '#ffffff',
    fontSize: getRem(16),
    fontWeight: '300',
  },
  disabled: {
    backgroundColor: '#999999',
  },
  enabled: {
    backgroundColor: 'rgba(31, 136, 216, 0.9)',
  }
})

type Props = {
  onPress: () => void,
  text: string,
  disabled: boolean,
  style: any,
  rootStyle: any,
}

function PanelButton(props: Props) {
  const handlePress = props.onPress ? props.onPress : null
  return (
    <TouchableHighlight
      style={[styles.root, props.rootStyle]}
      onPress={handlePress}
    >
      <View
        style={[styles.buttonContainer,
          props.disabled ? styles.disabled : styles.enabled,
          props.style]}
      >
        <Text style={styles.buttonText}>{props.text}</Text>
      </View>
    </TouchableHighlight>
  )
}

export default PanelButton
