import React, { Component } from 'react'
import Resizer from 'react-native-image-resizer'
import {
  View,
  Image,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Text } from 'react-native'

import AppNetworking from '../../utils/AppNetworking'
import images from '../../images/index'
import Localized from '../../utils/Localized'
import { normalStyles, licenseStyles } from './PhotoSquareSheetStyle'

const strings = Localized.Strings
const Networking = new AppNetworking()

type Props = {
  navigation: any,
  title: string,
  onChange: () => void,
  isLicense: boolean,
  tipImage: any,
  onCancel: () => void,
  value: any,
}

class PhotoSquare extends Component {
  static defaultProps = {
    isLicense: false,
    tipImage: images.cameraIcon,
  }
  constructor(props: Props) {
    const photo = props.value ? props.value.uri : null
    super()
    this.state = {
      photo,
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value && nextProps.value.uri && nextProps.value.uri !== this.state.photo) {
      this.setState({ photo: nextProps.value.uri })
    }
  }

  props: Props

  tackPhoto = () => {
    this.props.navigation.navigate('Camera', { photoInfo: this.photoInfo })
  }

  photoInfo = (data) => {
    Resizer.createResizedImage(data.path, 2000, 2000, 'JPEG', 80)
      .then(resizedImageUri => {
        this.setState({ photo: resizedImageUri })
        this.props.onChange({ uri: resizedImageUri })
        this.uploadImage(resizedImageUri)
      }).catch((err) => {
        console.log(err)
      })
  }

  uploadImage = imageUri => {
    Networking.uploadImage(imageUri).then(resp => {
      const imageId = resp[0]
      this.props.onChange(imageId)
    }, (err) => {
      console.log(err)
    })
  }

  handleCancel = () => {
    this.setState({ photo: null })
    this.props.onCancel()
  }

  render() {
    let { photo } = this.state
    photo = photo && { uri: photo }
    const { tipImage, isLicense } = this.props
    const styles = this.props.isLicense ? licenseStyles : normalStyles
    return (
      <View style={[styles.container]}>
        <TouchableWithoutFeedback onPress={!photo ? this.tackPhoto : null}>
          {isLicense ?
            <View style={[styles.imgContainer, styles.imgContainerBorder]}>
              {photo ?
                <Image
                  source={photo}
                  style={styles.img}
                /> :
                <View style={styles.photoTips}>
                  <Image source={tipImage} />
                  <Text style={styles.photoTipsText}>{strings.tapHereToAddPhotoOfYourLicense}</Text>
                </View>
              }
            </View> :
            <View style={[styles.imgContainer, styles.imgContainerBorder]}>
              <Image
                source={photo || tipImage}
                style={photo && styles.img}
              />
            </View>}
        </TouchableWithoutFeedback>
        {photo ?
          <TouchableOpacity
            onPress={this.handleCancel}
            style={styles.cancel}
          >
            <Image
              style={styles.imgCancel}
              source={images.close}
            />
          </TouchableOpacity> : null}
        <Text style={styles.title}>{this.props.title}</Text>
      </View>
    )
  }
}

export default PhotoSquare
