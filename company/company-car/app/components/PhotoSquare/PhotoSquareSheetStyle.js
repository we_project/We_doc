import { StyleSheet } from 'react-native'

import { getRem } from '../../styles/getRem'

const normalStyles = StyleSheet.create({
  container: {
    marginTop: getRem(10),
    marginBottom: getRem(10),
    marginLeft: 0,
    marginRight: getRem(20),
    position: 'relative',
  },
  imgContainer: {
    width: getRem(90),
    height: getRem(90),
    justifyContent: 'center',
    alignItems: 'center',
  },
  imgContainerBorder: {
    borderRadius: getRem(4),
    borderStyle: 'dotted',
    borderWidth: getRem(1),
    borderColor: '#999999',
  },
  img: {
    height: getRem(90),
    width: getRem(90),
    borderRadius: getRem(4),
  },
  cancel: {
    position: 'absolute',
    right: getRem(2),
    top: getRem(2),
  },
  title: {
    textAlign: 'center',
    color: '#b0b0b0',
    marginTop: getRem(5),
  },
  loadingView: {
    width: getRem(90),
    height: getRem(90),
    position: 'absolute',
    top: 0,
    left: 0,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.3)',
  },
  loadingGif: {
    width: getRem(40),
    height: getRem(40),
  },
  loadingText: {
    color: '#888888'
  }
})

const licenseStyles = StyleSheet.create({
  container: {
    marginTop: getRem(20),
    position: 'relative',
  },
  imgContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  imgContainerBorder: {
    height: getRem(128),
    borderRadius: getRem(4),
    borderStyle: 'dotted',
    borderWidth: getRem(1),
    borderColor: '#999999',
  },
  img: {
    height: getRem(128),
    width: getRem(335),
    borderRadius: getRem(4),
  },
  cancel: {
    position: 'absolute',
    right: getRem(3),
    top: getRem(3),
  },
  title: {
    textAlign: 'center',
    color: '#b0b0b0',
    marginTop: getRem(5),
  },
  photoTips: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  photoTipsText: {
    fontSize: getRem(12),
    fontWeight: '300',
    lineHeight: getRem(16),
    color: '#999999',
    marginTop: getRem(5),
  },
  loadingView: {
    height: getRem(128),
    width: getRem(335),
    position: 'absolute',
    top: 0,
    left: 0,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.3)',
  },
  loadingGif: {
    width: getRem(40),
    height: getRem(40),
  },
  loadingText: {
    color: '#888888'
  }
})

export { normalStyles, licenseStyles }
