/**
 * @flow
 */
import React, { PureComponent } from 'react'
import {
  StyleSheet,
  View,
  Image,
  Dimensions,
} from 'react-native'

import { getRem } from '../styles/getRem'
import images from '../images/index'

const { width } = Dimensions.get('window')
const styles = StyleSheet.create({
  container: {
    height: getRem(40),
    marginBottom: getRem(10),
  },
  imgReload: {
    flex: 1,
    marginLeft: width / 2 - getRem(15),
    width: getRem(30),
    height: getRem(30),
    resizeMode: 'contain'
  }
})


/* eslint-disable no-console */
class ListViewFooter extends PureComponent {
  render() {
    return (
      <View style={styles.container}>
        <Image source={images.loading} style={styles.imgReload} />
      </View>
    )
  }
}

module.exports = ListViewFooter
