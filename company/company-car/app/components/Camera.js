import React from 'react'
import {
  Image,
  StatusBar,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native'
import RNCamera from 'react-native-camera'
import images from '../images'
import { getRem, width, height } from '../styles/getRem'

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  backgroundImage: {
    flex: 1,
    width,
    height,
  },
  preview: {
    flex: 1,
  },
  overlay: {
    position: 'absolute',
    padding: getRem(16),
    right: 0,
    left: 0,
    alignItems: 'center',
  },
  topOverlay: {
    top: 0,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  bottomOverlay: {
    bottom: 0,
    backgroundColor: 'rgba(0,0,0,0.4)',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  captureButton: {
    padding: getRem(15),
    backgroundColor: 'white',
    borderRadius: getRem(40),
  },
  typeButton: {
    padding: getRem(5),
  },
  flashButton: {
    padding: getRem(5),
  },
})

type Props = {
  navigation: any,
}

export default class Camera extends React.Component {
  static navigationOptions = {
    header: null
  }

  constructor(props: Props) {
    super(props)

    this.camera = null

    this.state = {
      camera: {
        aspect: RNCamera.constants.Aspect.fill,
        captureTarget: RNCamera.constants.CaptureTarget.temp,
        type: RNCamera.constants.Type.back,
        orientation: RNCamera.constants.Orientation.auto,
        flashMode: RNCamera.constants.FlashMode.auto,
      },
    }
  }

  props: Props
  lock: Boolean = false

  takePicture = () => {
    if (this.camera && !this.lock) {
      this.lock = true
      this.camera.capture()
        .then((data) => {
          this.props.navigation.goBack()
          this.props.navigation.state.params.photoInfo(data)
        })
        .catch(err => console.error(err))
    }
  }

  switchFlash = () => {
    let newFlashMode
    const { auto, on, off } = RNCamera.constants.FlashMode

    if (this.state.camera.flashMode === auto) {
      newFlashMode = on
    } else if (this.state.camera.flashMode === on) {
      newFlashMode = off
    } else if (this.state.camera.flashMode === off) {
      newFlashMode = auto
    }
    this.setState({
      camera: {
        ...this.state.camera,
        flashMode: newFlashMode,
      },
    })
  }

  get flashIcon() {
    let icon
    const { auto, on, off } = RNCamera.constants.FlashMode

    if (this.state.camera.flashMode === auto) {
      icon = require('../images/camera/ic_flash_auto_white.png')
    } else if (this.state.camera.flashMode === on) {
      icon = require('../images/camera/ic_flash_on_white.png')
    } else if (this.state.camera.flashMode === off) {
      icon = require('../images/camera/ic_flash_off_white.png')
    }
    return icon
  }

  renderContent = () => (
    <View style={{ flex: 1 }}>
      <View style={[styles.overlay, styles.topOverlay]}>
        <TouchableOpacity
          style={styles.typeButton}
          onPress={() => { this.props.navigation.goBack() }}
        >
          <Image
            source={images.leftArrow}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.flashButton}
          onPress={this.switchFlash}
        >
          <Image
            source={this.flashIcon}
          />
        </TouchableOpacity>
      </View>
      <View style={[styles.overlay, styles.bottomOverlay]}>
        <TouchableOpacity
          style={styles.captureButton}
          onPress={this.takePicture}
        >
          <Image source={require('../images/camera/ic_photo_camera_36pt.png')} />
        </TouchableOpacity>
      </View>
    </View>
  )

  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          animated
          hidden
        />
        <RNCamera
          ref={(cam) => {
            this.camera = cam
          }}
          style={styles.preview}
          aspect={this.state.camera.aspect}
          captureTarget={this.state.camera.captureTarget}
          type={this.state.camera.type}
          flashMode={this.state.camera.flashMode}
          onFocusChanged={() => {
          }}
          onZoomChanged={() => {
          }}
          defaultTouchToFocus
          mirrorImage={false}
        >
          { this.renderContent() }
        </RNCamera>
      </View>
    )
  }
}
