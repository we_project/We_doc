/**
 * @flow
 */
import React, { Component } from 'react'
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
  Modal
} from 'react-native'

import { getRem } from '../styles/getRem'
import Localized from '../utils/Localized'
import images from '../images'

import ImageMapper from '../utils/CarImageMapper'

const strings = Localized.Strings

const styles = StyleSheet.create({
  mask: {
    backgroundColor: 'rgba(0,0,0,0.7)',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  dialog: {
    width: getRem(295),
    height: getRem(406),
    backgroundColor: 'white',
    borderRadius: getRem(4),
    paddingTop: getRem(20),
    paddingBottom: getRem(20)
  },
  title: {
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    flexDirection: 'row',
    height: getRem(30),
  },
  txtTitle: {
    width: getRem(250),
    marginLeft: getRem(10),
    textAlign: 'center',
    fontSize: getRem(16),
    fontWeight: '500',
    color: '#333333'
  },
  titleClose: {
    width: getRem(20),
    marginTop: getRem(1),

  },
  info: {
    alignItems: 'center',
    borderStyle: 'solid',
    borderBottomWidth: 0.5,
    borderTopWidth: 0.5,
    borderColor: '#e7e7e7',
    paddingBottom: getRem(20),
  },
  carImage: {
    width: getRem(266),
    height: getRem(170),
  },
  plateNumber: {
    paddingLeft: getRem(2),
    paddingRight: getRem(2),
    height: getRem(22),
    backgroundColor: '#f8f8f8',
    borderStyle: 'solid',
    borderWidth: getRem(1),
    borderColor: '#d8d8d8',
    fontSize: getRem(16),
    color: '#333333',
    fontWeight: '500',
    fontFamily: 'HelveticaNeue',
    letterSpacing: 1.0,
  },
  plateNumberContainer: {
    height: getRem(22),
  },
  carModelContainer: {
    marginTop: getRem(5)
  },
  carModel: {
    width: getRem(177),
    height: getRem(16),
    fontFamily: 'HelveticaNeue',
    fontSize: getRem(14),
    textAlign: 'center',
    color: '#333333'
  },
  buttons: {
    padding: getRem(10)
  },
  btn: {
    height: getRem(40),
    borderRadius: getRem(4),
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: getRem(10),
  },
  primaryBtn: {
    backgroundColor: '#1f88d9',
  },
  secondBtn: {
    backgroundColor: '#fff',
    borderColor: '#1f88d8',
    borderStyle: 'solid',
    borderWidth: getRem(1)
  },
  primaryBtnTxt: {
    color: '#fff',
    fontSize: getRem(16),
  },
  secondBtnTxt: {
    color: '#1f88d9',
    fontSize: getRem(16),
  }
})


type
Props = {
  onClose: () => void
}

class CountedVehicleDialog extends Component {
  static navigationOptions = {
    header: null
  }

  constructor(props: Props) {
    super(props)
    this.state = {
      isShow: false,
    }
  }

  onClose = () => {
    this.props.onClose()
  }

  props: Props

  render() {
    return (
      <Modal
        transparent
        ref='modal'
        modalVisible
        onRequestClose={this.onClose}
        animationType={'fade'}
      >
        <View style={styles.mask}>
          <View style={styles.dialog}>
            <View style={styles.title}>
              <Text style={styles.txtTitle}>Counted Vehicle</Text>
              <Image source={images.cancel} style={styles.titleClose} />
            </View>
            <View style={styles.info}>
              <Image source={ImageMapper('Maybach-SUV AMG')} style={styles.carImage} />
              <View style={styles.plateNumberContainer}>
                <Text style={styles.plateNumber}>京Q·1L2N5</Text>
              </View>
              <View style={styles.carModelContainer}>
                <Text style={styles.carModel}>Maybach-SUV AMG </Text>
              </View>
            </View>
            <View style={styles.buttons}>
              <TouchableOpacity onPress={this.onClose} style={[styles.secondBtn, styles.btn]}>
                <Text style={styles.secondBtnTxt}>View Details</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={this.onClose} style={[styles.primaryBtn, styles.btn]}>
                <Text style={styles.primaryBtnTxt}>Count Another Vehicle</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    )
  }
}

export default CountedVehicleDialog
