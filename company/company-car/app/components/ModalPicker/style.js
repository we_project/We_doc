import { StyleSheet, Dimensions } from 'react-native'

import { getRem } from '../../styles/getRem'

const { width } = Dimensions.get('window')
const PADDING = getRem(8)
const BORDER_RADIUS = getRem(5)
const FONT_SIZE = getRem(16)
const HIGHLIGHT_COLOR = 'rgba(0,118,255,0.9)'

export default StyleSheet.create({
  overlayStyle: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.7)'
  },

  optionContainer: {
    borderRadius: BORDER_RADIUS,
    width: width * 0.8,
    backgroundColor: 'rgba(255,255,255,0.8)',
    position: 'absolute',
    left: width * 0.1,
    bottom: getRem(60),
    maxHeight: getRem(400),
  },

  cancelContainer: {
    position: 'absolute',
    left: width * 0.1,
    bottom: getRem(10),
  },

  selectStyle: {
    flex: 1,
    borderColor: '#ccc',
    borderWidth: getRem(1),
    padding: getRem(8),
    borderRadius: BORDER_RADIUS
  },

  selectTextStyle: {
    textAlign: 'center',
    color: '#333',
    fontSize: FONT_SIZE
  },

  cancelStyle: {
    borderRadius: BORDER_RADIUS,
    width: width * 0.8,
    backgroundColor: 'rgba(255,255,255,0.8)',
    padding: PADDING
  },

  cancelTextStyle: {
    textAlign: 'center',
    color: '#333',
    fontSize: FONT_SIZE
  },

  optionStyle: {
    padding: PADDING,
    borderBottomWidth: getRem(1),
    borderBottomColor: '#ccc',
  },

  optionTextStyle: {
    textAlign: 'center',
    fontSize: FONT_SIZE,
    color: HIGHLIGHT_COLOR
  },

  sectionStyle: {
    padding: PADDING * 2,
    borderBottomWidth: getRem(1),
    borderBottomColor: '#ccc'
  },

  sectionTextStyle: {
    textAlign: 'center',
    fontSize: FONT_SIZE
  },
  optionsContainer: {
    flexDirection: 'column',
    justifyContent: 'center',
    paddingHorizontal: getRem(10),
  }
})
