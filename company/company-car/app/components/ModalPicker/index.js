/**
 * @flow
 */
import React, { Component } from 'react'
import {
  View,
  Modal,
  Text,
  ScrollView,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native'

import styles from './style'
import Localized from '../../utils/Localized'

const strings = Localized.Strings

type Props = {
  data: Array,
  onChange: () => any,
  initValue: string,
  cancelText: string,
  children: any,
}

export default class ModalPicker extends Component {
  static defaultProps = {
    data: [],
    onChange: () => {},
    initValue: 'Select me!',
    cancelText: strings.cancel,
  }
  constructor(props) {
    super(props)
    this.state = {
      animationType: 'slide',
      modalVisible: false,
      transparent: false,
      selected: props.initValue,
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.initValue !== this.props.initValue) {
      this.setState({ selected: nextProps.initValue })
    }
  }

  onChange = (item) => {
    this.props.onChange(item)
    this.setState({ selected: item.label })
    this.close()
  }

  props: Props

  close = () => {
    this.setState({
      modalVisible: false
    })
  }

  open = () => {
    this.setState({
      modalVisible: true
    })
  }

  renderSection = section => (
    <View key={section.key} style={[styles.sectionStyle]}>
      <Text style={[styles.sectionTextStyle]}>{section.label}</Text>
    </View>
  )

  renderOption = (option, isLast) => (
    <TouchableOpacity key={option.key} onPress={() => this.onChange(option)}>
      <View style={[styles.optionStyle, isLast ? { borderBottomWidth: 0 } : null]}>
        <Text style={[styles.optionTextStyle]}>{option.label}</Text>
      </View>
    </TouchableOpacity>
  )

  renderOptionList = () => {
    const options = this.props.data.map((item, index) => {
      if (item.section) {
        return this.renderSection(item)
      }
      return this.renderOption(item, index === this.props.data.length - 1)
    })

    return (
      <View style={[styles.overlayStyle]}>
        <View style={[styles.optionContainer]}>
          <ScrollView keyboardShouldPersistTaps='always'>
            <View style={styles.optionsContainer}>
              {options}
            </View>
          </ScrollView>
        </View>
        <View style={styles.cancelContainer}>
          <TouchableOpacity onPress={this.close}>
            <View style={[styles.cancelStyle]}>
              <Text style={[styles.cancelTextStyle]}>
                {this.props.cancelText}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>)
  }

  renderChildren = () => {
    if (this.props.children) {
      return this.props.children
    }
    return (
      <View style={[styles.selectStyle]}>
        <Text style={[styles.selectTextStyle]}>
          {this.state.selected}
        </Text>
      </View>
    )
  }

  render() {
    const dp = (
      <Modal
        transparent
        ref='modal'
        visible={this.state.modalVisible}
        onRequestClose={this.close}
        animationType={this.state.animationType}
      >
        <TouchableWithoutFeedback style={{ flex: 1 }} onPress={this.close}>
          {this.renderOptionList()}
        </TouchableWithoutFeedback>
      </Modal>
    )

    return (
      <View>
        {dp}
        <TouchableOpacity onPress={this.open}>
          {this.renderChildren()}
        </TouchableOpacity>
      </View>
    )
  }
}
