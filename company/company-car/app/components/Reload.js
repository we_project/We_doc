/**
 * @flow
 */
import React, { Component } from 'react'
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity
} from 'react-native'

import { getRem } from '../styles/getRem'
import Localized from '../utils/Localized'
import images from '../images'

const strings = Localized.Strings

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  reload: {
    flex:1,
    backgroundColor: '#f4f4f4',
    alignItems: 'center'
  },
  center: {
    paddingTop: getRem(100),
    justifyContent: 'center',
    alignItems: 'center'
  },
  txtReload: {
    color: '#d8d8d8'
  },
  imgReload: {
    width: getRem(50),
    resizeMode: 'contain'
  }
})

type Props = {
  navigation: any,
}
/* eslint-disable no-console */
class Reload extends Component {

  constructor(props: Props) {
    super(props)
  }

  onRefresh = () => {
    return this.props.onRefresh()
  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.reload} onPress={this.onRefresh.bind(this)}>
          <View style={styles.center}>
            <Image source={images.reload} style={styles.imgReload}/>
            <Text style={styles.txtReload}>{this.props.text || strings.networkErr}</Text>
          </View>
        </TouchableOpacity>
      </View>
    )
  }
}

module.exports = Reload
