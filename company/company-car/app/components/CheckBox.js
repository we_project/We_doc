/**
 * @flow
 */
import React, { Component } from 'react'
import {
  StyleSheet,
  Image,
  Text,
  View,
  TouchableHighlight
} from 'react-native'
import { getRem } from '../styles/getRem'
import images from '../images/index'

const styles = StyleSheet.create({
  root: {
    marginTop: getRem(5),
    marginBottom: getRem(5),
    alignSelf: 'center',
  },
  checkBoxItem: {
    alignItems: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
    margin: getRem(3),
    padding: getRem(2)
  },
  checkBoxContainer: {
    width: getRem(16),
    height: getRem(16),
    borderWidth: getRem(1),
    borderRadius: getRem(1),
    marginRight: getRem(6),
    borderColor: '#e5e5e5',
  },
  checkedBox: {
    backgroundColor: '#3990d8',
    borderWidth: 0
  },
  uncheckedBox: {
    backgroundColor: '#f8f8f8'
  },
  icon: {
    margin: getRem(4),
    marginLeft: getRem(3),
  },
  text: {
    color: '#666666',
    fontSize: 14,
  }
})

type Props = {
  checked: Boolean,
  onChange: () => any,
  style: any,
  children: any,
}

class CheckBox extends Component {
  constructor(props: Props) {
    super(props)
    this.state = {
      checked: props.checked
    }
    this.onChange = this.onChange.bind(this)
  }
  componentWillReceiveProps(newProps) {
    if (newProps.checked !== this.props.checked) {
      this.setState({ checked: newProps.checked })
    }
  }

  onChange() {
    this.setState({ checked: !this.state.checked }, () => {
      if (this.props.onChange) {
        this.props.onChange(this.state.checked)
      }
    })
  }

  props: Props

  render() {
    const { checked } = this.state
    return (
      <TouchableHighlight
        underlayColor={'#ffffff'}
        style={styles.root}
        onPress={this.onChange}
      >
        <View style={[styles.checkBoxItem, this.props.style]}>
          <View style={[styles.checkBoxContainer,
            checked ? styles.checkedBox : styles.uncheckedBox]}
          >
            <Image style={styles.icon} source={checked ? images.checked : null} />
          </View>
          <View><Text style={styles.text}>{this.props.children}</Text></View>
        </View>
      </TouchableHighlight>
    )
  }
}

export default CheckBox
