/**
 * @flow
 */
import React, { Component } from 'react'
import { View, Text, Image, StyleSheet, TouchableWithoutFeedback } from 'react-native'

import { getRem } from '../styles/getRem'
import images from '../images'

const styles = StyleSheet.create({
  vehicleInfoCard: {
    backgroundColor: '#ffffff',
  },
  vehicleInfoHeader: {
    height: getRem(59),
    paddingLeft: getRem(12),
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    borderBottomWidth: getRem(1),
    borderBottomColor: '#e7e7e7',
  },
  leftIconView: {
    width: getRem(45),
    alignItems: 'center',
  },
  directionArrow: {
    marginLeft: getRem(10),
  },
  directionImage: {
    flex: 1,
    alignItems: 'flex-end',
    paddingRight: getRem(20),
  },
  vehicleInfoHeaderTitle: {
    fontSize: getRem(16),
    fontWeight: '500',
    color: '#333333',
  },
  vehicleInfoItemContainer: {
    backgroundColor: 'rgba(244, 244, 244, 0.5)',
  },
})
type Props = {
  title: string,
  icon: any,
  children: any,
  rightButton: any,
  expandDisabled: boolean,
  isExpand: boolean,
}

class AccordionPanel extends Component {
  static defaultProps = {
    expandDisabled: false,
    rightButton: null,
  }
  constructor(props: Props) {
    super(props)
    this.state = {
      isExpand: props.isExpand || false,
    }
  }

  props: Props

  handleExpand = () => {
    if (!this.props.expandDisabled) {
      this.setState({ isExpand: !this.state.isExpand })
    }
  }

  render() {
    const { icon, title, children, rightButton, expandDisabled } = this.props
    return (
      <View style={styles.vehicleInfoCard}>
        <TouchableWithoutFeedback
          onPress={this.handleExpand}
        >
          <View style={styles.vehicleInfoHeader}>
            <View style={styles.leftIconView}>
              <Image source={icon} />
            </View>
            <Text style={styles.vehicleInfoHeaderTitle}>
              {title}
            </Text>
            {!expandDisabled ?
              <Image source={images.correct} style={{ marginLeft: getRem(10) }} /> : null}
            <View style={styles.directionImage}>
              {!expandDisabled ?
                <Image
                  style={styles.directionArrow}
                  source={this.state.isExpand ? images.downArrow : images.rightArrow}
                /> : rightButton}
            </View>
          </View>
        </TouchableWithoutFeedback>
        <View
          onLayout={(e) => { this.setState({ height: e.nativeEvent.layout.height }) }}
          ref='AccordionContent'
          style={styles.vehicleInfoItemContainer}
        >
          {this.state.isExpand && children}
        </View>
      </View>
    )
  }
}

export default AccordionPanel
