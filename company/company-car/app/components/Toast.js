import React, { Component } from 'react'
import {
  StyleSheet,
  View,
  Animated,
  Dimensions,
  Text,
  Image,
} from 'react-native'
import { getRem } from '../styles/getRem'
import images from '../images'

export const DURATION = {
  LENGTH_LONG: 2000,
  LENGTH_SHORT: 500,
  FOREVER: 0,
}

const { height, width } = Dimensions.get('window')

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    left: (width - getRem(200)) / 2,
    top: (height / 2) - getRem(120),
    width: getRem(200),
  },
  content: {
    backgroundColor: 'rgba(51, 51, 51, 0.9)',
    borderRadius: getRem(2),
    padding: getRem(10),
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontSize: getRem(14),
    textAlign: 'center',
    color: '#ffffff',
    marginLeft: getRem(20),
    marginRight: getRem(20),
    marginBottom: getRem(20),
    lineHeight: getRem(20),
  },
  image: {
    marginTop: getRem(20),
    marginBottom: getRem(14),
  }
})

type Props = {
  fadeInDuration: number,
  fadeOutDuration: number,
  opacity: number,
}

export default class Toast extends Component {
  static defaultProps = {
    position: 'center',
    positionValue: 120,
    fadeInDuration: 500,
    fadeOutDuration: 500,
    opacity: 1
  }
  constructor(props: Props) {
    super(props)
    this.state = {
      isShow: false,
      text: '',
      opacityValue: new Animated.Value(this.props.opacity),
    }
  }

  componentWillUnmount() {
    if (this.timer) {
      clearTimeout(this.timer)
    }
  }

  show(text, duration) {
    this.duration = typeof duration === 'number' ? duration : DURATION.LENGTH_SHORT

    this.setState({
      isShow: true,
      text,
    })

    Animated.timing(
      this.state.opacityValue,
      {
        toValue: this.props.opacity,
        duration: this.props.fadeInDuration,
      }
    ).start(() => {
      this.isShow = true
      if (duration !== DURATION.FOREVER) {
        this.close()
      }
    })
  }

  close(duration) {
    let delay = typeof duration === 'undefined' ? this.duration : duration

    if (delay === DURATION.FOREVER) {
      delay = 250
    }

    if (!this.isShow && !this.state.isShow) return
    if (this.timer) clearTimeout(this.timer)
    this.timer = setTimeout(() => {
      Animated.timing(
        this.state.opacityValue,
        {
          toValue: 0.0,
          duration: this.props.fadeOutDuration,
        }
      ).start(() => {
        this.setState({
          isShow: false,
        })
        this.isShow = false
      })
    }, delay)
  }

  render() {
    const customerStyle = this.props && this.props.style;
    const view = this.state.isShow ?
      (
        <View
          style={[customerStyle || styles.container]}
        >
          <Animated.View
            style={[styles.content, { opacity: this.state.opacityValue }]}
          >
            <Image source={images.warning} style={styles.image} />
            <Text style={styles.text}>{this.state.text}</Text>
          </Animated.View>
        </View>
      ) : null
    return view
  }
}
