/**
 * @flow
 */
import React, { Component } from 'react'
import {
  StyleSheet,
  View,
  Text,
  Image,
  Alert,
  TouchableOpacity,
  AppRegistry,
  CameraRoll,
} from 'react-native'

import Localized from '../utils/Localized'
import Resizer from 'react-native-image-resizer';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
})

type Props = {
  navigation: any,
}
/* eslint-disable no-console */
class ImageResizer extends Component {

  constructor(props: Props) {
    super(props)
    this.state = {
      imgUri: '',
      image: {uri: 'file:///Users/wuyang/Documents/pic/wuyang.jpg'}
    }
  }

  static navigationOptions = {
    header: null
  }

  componentDidMount() {
    //TODO use the latest image, loaded from device
    // CameraRoll.getPhotos({first: 1}).then((photos) => {
    //   if (!photos.edges || photos.edges.length === 0) {
    //     return Alert.alert('Unable to load camera roll',
    //       'Check that you authorized the access to the camera roll photos and that there is at least one photo in it');
    //   }
    //
    //   this.setState({
    //     image: photos.edges[0].node.image,
    //   })
    // }).catch(() => {
    //   return Alert.alert('Unable to load camera roll',
    //     'Check that you authorized the access to the camera roll photos');
    // });
  }

  resize() {
    Resizer.createResizedImage(this.state.image.uri, 800, 600, 'JPEG', 80)
      .then((resizedImageUri) => {
        this.setState({
          resizedImageUri,
        });
        this.setState({resizeSuccess : "uri: " + resizedImageUri})
      }).catch((err) => {
      console.log(err);
      this.setState({resizeSuccess : 'Err: ' + err})
      return Alert.alert('Unable to resize the photo',
        'Check the console for full the error message');
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <Image style={{width: 200, height:200, marginTop:100}} source={{uri: this.state.image.uri}}/>
        <TouchableOpacity onPress={() => this.resize()}>
          <Text style={{marginTop:10}}>
            Click me to resize the image
          </Text>
          <Text>{this.state.resizeSuccess}</Text>
        </TouchableOpacity>

      </View>
    )
  }
}

module.exports = ImageResizer
