import React from 'react'
import { View, Text, TouchableHighlight, StyleSheet } from 'react-native'

import { getRem } from '../styles/getRem'

const styles = StyleSheet.create({
  buttonContainer: {
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    height: getRem(48),
  },
  buttonText: {
    color: '#ffffff',
    fontSize: getRem(18),
    fontWeight: '300',
  },
  disabled: {
    backgroundColor: '#999999',
  },
  enabled: {
    backgroundColor: 'rgba(31, 136, 216, 0.9)',
  }
})

type Props = {
  onPress: () => void,
  text: string,
  disabled: boolean,
  style: any,
}

function BottomButton(props: Props) {
  const handlePress = props.onPress ? props.onPress : null
  return (
    <TouchableHighlight
      onPress={handlePress}
    >
      <View
        style={[styles.buttonContainer,
          props.disabled ? styles.disabled : styles.enabled,
          props.style]}
      >
        <Text style={styles.buttonText}>{props.text}</Text>
      </View>
    </TouchableHighlight>
  )
}

export default BottomButton
