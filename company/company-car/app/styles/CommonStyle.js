import {
  StyleSheet,
  Dimensions,
} from 'react-native'

const {
  width,
  height
} = Dimensions.get('window')

module.exports = {
  size: {
    width,
    height,
  },
  headerStyles: StyleSheet.create({
    left: {
      marginLeft:10,
      width: 100
    },
    right: {
      marginRight:10,
      width: 100
    }
  }),
  styles: StyleSheet.create({
    bgDefault: {
      backgroundColor: '#F8F8F8'
    },

    flex1: {
      flex: 1
    },
    flex2: {
      flex: 2
    },
    flex3: {
      flex: 3
    },

    f10: {
      fontSize: 10
    },
    f12: {
      fontSize: 12
    },
    f14: {
      fontSize: 14
    },
    f16: {
      fontSize: 16
    },
    f18: {
      fontSize: 18
    },
    f20: {
      fontSize: 20
    },


    tc: {
      textAlign: 'center',
    },
    fc: {
      justifyContent: 'center',
      alignItems: 'center',
    },

    absolute: {
      position: 'absolute'
    },
    relative: {
      position: 'relative'
    },

    ml5: {
      marginLeft: 5
    },
    ml10: {
      marginLeft: 10
    },
    ml15: {
      marginLeft: 15
    },
    ml20: {
      marginLeft: 20
    },
    ml25: {
      marginLeft: 25
    },

    mt5: {
      marginTop: 5
    },
    mt10: {
      marginTop: 10
    },
    mt15: {
      marginTop: 15
    },
    mt20: {
      marginTop: 20
    },
    mt25: {
      marginTop: 25
    },

    mb5: {
      marginBottom: 5
    },
    mb8: {
      marginBottom: 8
    },
    mb10: {
      marginBottom: 10
    },
    mb15: {
      marginBottom: 15
    },
    mb20: {
      marginBottom: 20
    },
    mb25: {
      marginBottom: 25
    },

    mr5: {
      marginRight: 5
    },
    mr10: {
      marginRight: 10
    },
    mr15: {
      marginRight: 15
    },
    mr20: {
      marginRight: 20
    },
    mr25: {
      marginRight: 25
    },


    pl5: {
      paddingLeft: 5
    },
    pl10: {
      paddingLeft: 10
    },
    pl15: {
      paddingLeft: 15
    },
    pl20: {
      paddingLeft: 20
    },
    pl25: {
      paddingLeft: 25
    },

    pt5: {
      paddingTop: 5
    },
    pt10: {
      paddingTop: 10
    },
    pt15: {
      paddingTop: 15
    },
    pt20: {
      paddingTop: 20
    },
    pt25: {
      paddingTop: 25
    },

    pb5: {
      paddingBottom: 5
    },
    pb10: {
      paddingBottom: 10
    },
    pb15: {
      paddingBottom: 15
    },
    pb20: {
      paddingBottom: 20
    },
    pb25: {
      paddingBottom: 25
    },

    pr5: {
      paddingRight: 5
    },
    pr10: {
      paddingRight: 10
    },
    pr15: {
      paddingRight: 15
    },
    pr20: {
      paddingRight: 20
    },
    pr25: {
      paddingRight: 25
    },
  })
}
