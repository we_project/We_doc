import { Dimensions } from 'react-native'

const { width, height } = Dimensions.get('window')
const ratio = width / 375
const getRem = (size) => Math.round(ratio * size)

export { getRem, width, height }
