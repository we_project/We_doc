import React from 'react-native'
import _ from 'lodash'

const {
  AsyncStorage
} = React

const KEY_USER = '@User'

function User() {
  if (typeof User.instance === 'object') {
    return User.instance
  }
  User.instance = this
}
/* eslint-disable no-console */
/*  eslint func-names: ["error", "never"] */
User.prototype.getUser = function () {
  return new Promise((resolve, reject) => {
    AsyncStorage.getItem(KEY_USER, (error, result) => {
      const rawData = JSON.parse(result)
      if (!_.isNull(rawData) && !_.isUndefined(rawData)) {
        resolve(rawData)
      } else {
        reject('not get user')
      }
    })
  })
}

User.prototype.setUser = function (_user) {
  return new Promise((resolve, reject) => {
    AsyncStorage.setItem(KEY_USER, JSON.stringify(_user))
      .then(() => {
        resolve(_user)
      }, error => {
        reject(error)
      })
  })
}

User.prototype.setUserKey = function (key, value) {
  return new Promise((resolve, reject) => {
    if (_.isUndefined(key) || _.isUndefined(value)
      || _.isNull(key) || _.isNull(value) || _.isEmpty(key)) {
      const error = new Error('setUserKey not invalid')
      reject(error)
    }

    this.getUser().then(user => {
      const newUser = user
      newUser[key] = value
      AsyncStorage.setItem(KEY_USER, JSON.stringify(newUser))
        .then(() => {
          resolve(newUser)
        }, error => {
          reject(error)
        })
    })
  })
}

User.prototype.deleteUser = function () {
  return new Promise((resolve, reject) => {
    AsyncStorage.removeItem(KEY_USER)
      .then(res => {
        resolve(res)
      }, error => {
        reject(error)
      })
  })
}

module.exports = User
