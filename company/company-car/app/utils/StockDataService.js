import { PROVINCIAL_CAPITAL_ABBREVIATION } from './PlateNumber'

export default class StockDataService {
  initData = () => ({
    plateNo: '',
    model: null,
    vin: null,
    id: null,
    provincialCapital: PROVINCIAL_CAPITAL_ABBREVIATION[0],
    plateNumber: '',
    location: null,
    exteriorColour: null,
    interiorColour: null,
    leftFrontImage: null,
    rightBackImage: null,
    licenseImage: null,
    warningTriangleInCar: null,
    fireExtinguisher: null,
    floorMat: null,
    mileage: null,
    odometerImage: null,
    carIsMobileAndInUse: null,
    damages: {
      damageNumber: null,
      damageLevel: null,
      damageImages: []
    },
    signature: null,
  })

  stockData = this.initData()

  get getStockData() {
    if (!this.stockData.plateNo) {
      this.stockData.provincialCapital = ''
    }
    return this.stockData
  }

  addStockData = (data) => {
    this.stockData = Object.assign(this.stockData, data)
  }

  clearAll = () => {
    this.stockData = this.initData()
  }

  vehicleFilter = {
    counted: false,
    assignedToMe: true,
    all: false,
  }

  changeVehicleFilter = (data) => {
    this.vehicleFilter = Object.assign(this.vehicleFilter, data)
  }
}
