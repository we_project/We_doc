import { Platform } from 'react-native'
import _ from 'lodash'
import Util from './Util'
import Constants from './Constants'
import UtilUser from './User'

const host = Constants.host
const User = new UtilUser()
const API_GET_USER_INFO = 'https://anywhere.i.daimler.com/any/anywhere/SsoLogin'
const API_ADMIN_LOGIN = `${host}/api/login`
const API_VEHICLE_LIST = `${host}/api/vehicles`
const API_ALL_VEHICLE_LIST = `${host}/api/vehicles/all`
const API_SEARCH_VEHICLE = `${host}/api/vehicles/search?isFuzzy=true&q=`
const API_VEHICLE_DETAIL = `${host}/api/vehicle`
const API_UPLOAD_IMAGE = `${host}/api/att/upload`
const API_UPLOAD_STOCK_DATA = `${host}/api/vehicle-stocks/vehicle`
const TIMEOUT_LIMIT = 10000
const NETWORK_ISSUE = new Error('Network')
/*  eslint func-names: ["error", "never"] */
function AppNetworking() {
  if (typeof AppNetworking.instance === 'object') {
    return AppNetworking.instance
  }
  AppNetworking.instance = this
}


AppNetworking.prototype.headers = function () {
  const header = {
    'Content-Type': 'application/json charset=utf-8',
    clientVersion: '0.1',
    clientDevice: Platform.OS,
  }

  return User.getUser()
    .then(res => {
      if (res !== null) {
        header.Authorization = res.Authorization
      }
      return header
    }, () => header
    )
}

/**
 * 将请求包装成 primise并返回
 * 并统一处理错误和成功
 */

AppNetworking.prototype.safeFetch = function (reqUrl:String, method:String, data:Object) {
  return new Promise((resolve, reject) => {
    this.headers().then((res) => {
      const fetchObj = {
        method: Util.isEmpty(method) ? 'POST' : method,
        headers: res
      }
      let newReqUrl
      // reqUrl = reqUrl
      if (fetchObj.method === 'GET') {
        newReqUrl = this.getParamUrl(reqUrl, data)
      } else {
        /* 判断要不要fetch body */
        fetchObj.body = data !== undefined ? JSON.stringify(data) : null
      }
      global.fetch(newReqUrl, fetchObj)
        .then((response) => {
          response.method = fetchObj.method
          if (response.status === 200) {
            return response.json()
            // resolve(response)
          }
          const error = new Error(response.status)
          error.info = { msg: 'err' }
          throw error
        })
        .then((response) => {
          resolve(response)
        })
        .catch((e) => {
          reject(e)
        })
    })
  })
}

AppNetworking.prototype.getParamUrl = function (oldUrl:String, params:Object) {
  const temp = []
  let newUrl = ''
  temp.push(oldUrl)
  temp.push('?')
  _.forIn(params, (value, key) => {
    temp.push(key)
    temp.push('=')
    temp.push(value)
    temp.push('&')
  })
  newUrl = temp.join('')
  return newUrl.substr(0, newUrl.length - 1)
}


AppNetworking.prototype.getUserInfo = function (token) {
  const fetchObj = {
    method: 'GET',
  }
  return new Promise((resolve, reject) => {
    this.safeFetch(API_GET_USER_INFO, fetchObj.method, { stoken: token })
      .then((res) => {
        resolve(res)
      }, (err) => {
        reject(err)
      })
  })
}

AppNetworking.prototype.adminLogin = function (data) {
  function timeoutPromise(timeout, err, promise) {
    return new Promise(((resolve, reject) => {
      promise.then(resolve, reject)
      setTimeout(reject.bind(null, err), timeout)
    }))
  }
  return new Promise((resolve, reject) => {
    timeoutPromise(TIMEOUT_LIMIT, NETWORK_ISSUE, fetch(API_ADMIN_LOGIN, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data)
    })).then((res) => {
      if (res.status === 200) {
        resolve(res.json())
      }
      reject(res)
    }).catch((e) => {
      reject(NETWORK_ISSUE)
    })
  })
}

AppNetworking.prototype.getVehicleList = function () {
  return new Promise((resolve, reject) => {
    this.safeFetch(API_VEHICLE_LIST, 'GET')
      .then((res) => {
        resolve(res)
      }, (err) => {
        reject(err)
      })
  })
}

AppNetworking.prototype.getAdminVehicleList = function (pageSize, pageNumber, counted, assignedToMe) {
  return new Promise((resolve, reject) => {
    this.safeFetch(`${API_ALL_VEHICLE_LIST}?pageSize=${pageSize}&pageNumber=${pageNumber}&counted=${counted}&is-assign-to-me=${assignedToMe}`, 'GET')
      .then((res) => {
        resolve(res)
      }, (err) => {
        reject(err)
      })
  })
}

AppNetworking.prototype.searchVehicleList = function (keyword) {
  return new Promise((resolve, reject) => {
    this.safeFetch(API_SEARCH_VEHICLE + keyword, 'GET')
      .then((res) => {
        resolve(res)
      }, (err) => {
        reject(err)
      })
  })
}


AppNetworking.prototype.getVehicleDetail = function (id) {
  return new Promise((resolve, reject) => {
    this.safeFetch(`${API_VEHICLE_DETAIL}/${id}`, 'GET')
      .then((res) => {
        resolve(res)
      }, (err) => {
        reject(err)
      })
  })
}

AppNetworking.prototype.uploadImage = function (uri) {
  const fileData = new FormData()
  const uriArray = uri.split('/')
  fileData.append('file', {
    uri,
    type: 'image/jpeg',
    name: uriArray[uriArray.length - 1]
  })
  return new Promise((resolve, reject) => {
    this.headers().then((res) => {
      const newRes = Object.assign(res, { 'Content-Type': 'multipart/form-data' })
      global.fetch(API_UPLOAD_IMAGE, {
        headers: newRes,
        method: 'POST',
        body: fileData,
      }).then((response) => {
        if (response.status === 200) {
          resolve(response.json())
          return
        }
        reject(response)
      }, (err) => {
        reject(err)
      })
    })
  })
}

AppNetworking.prototype.uploadStockData = function (id, data) {
  return new Promise((resolve, reject) => {
    this.headers().then((res) => {
      const newRes = Object.assign(res, { 'Content-Type': 'application/json', Accept: 'application/json' })
      global.fetch(`${API_UPLOAD_STOCK_DATA}/${id}`, {
        headers: newRes,
        method: 'POST',
        body: JSON.stringify(data),
      }).then((response) => {
        if (response.status === 201) {
          resolve(response.json())
        }
        reject(response)
      }).catch((e) => {
        reject(e)
      })
    })
  })
}

module.exports = AppNetworking
