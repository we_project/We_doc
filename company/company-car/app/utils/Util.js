import _ from 'lodash'

module.exports = {
  getParamValue(url, param) {
    const urlParamString = url.split(`${param}=`)
    if (urlParamString.length <= 1) {
      return ''
    }
    return urlParamString[1].split('&')[0]
  },

  /**
   * 根据数值类型判断是否为空
   * @param value
   * @returns {boolean}
   */

  isEmpty(value) {
    if (_.isNumber(value)) {
      if (_.isUndefined(value) || _.isNull(value) || _.isNaN(value)) {
        return true
      }
      return false
    } else if (_.isString(value)) {
      if (_.isUndefined(value) || _.isNull(value) || _.isEmpty(value)) {
        return true
      }
      return false
    } else if (_.isBoolean) {
      if (_.isUndefined(value) || _.isNull(value)) {
        return true
      }
      return false
    }
    if (_.isUndefined(value) || _.isEmpty(value) || _.isNull(value)) {
      return true
    }
    return false
  },
}
