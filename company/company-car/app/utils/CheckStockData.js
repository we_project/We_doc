import { isEmpty, findIndex } from 'lodash'

export default function checkStockData(stockData, checkData) {
  return findIndex(checkData, (v) => isEmpty(stockData[v])) === -1
}
