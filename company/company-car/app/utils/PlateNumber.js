import Localized from './Localized'

const strings = Localized.Strings

const PROVINCIAL_CAPITAL_ABBREVIATION = ['京', '津', '冀', '晋', '蒙',
  '辽', '吉', '黑', '沪', '苏', '浙', '皖', '闽', '赣', '鲁', '豫', '鄂', '湘',
  '粤', '桂', '琼', '渝', '川', '贵', '云', '藏', '陕', '甘', '青', '宁', '新']

const EXTERIOR_COLOUR = ['White', 'Silver', 'Grey', 'Blue', 'Green', 'Red', 'Yellow', 'Orange', 'Black', 'Brown', 'Other']

const INTERIOR_COLOUR = ['Grey', 'Black', 'Brown', 'Beige', 'Red', 'Blue', 'Other']

const YES_NO = [{ label: strings.YES, value: 'YES' }, { label: strings.NO, value: 'NO' }]

export {
  PROVINCIAL_CAPITAL_ABBREVIATION,
  EXTERIOR_COLOUR,
  INTERIOR_COLOUR,
  YES_NO,
}
