/**
 * @flow
 */
import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { format } from 'date-fns'

import Localized from '../../utils/Localized'
import { getRem } from '../../styles/getRem'

const strings = Localized.Strings
const styles = StyleSheet.create({
  root: {
    backgroundColor: '#ffffff',
  },
  rowItem: {
    paddingLeft: getRem(12),
    paddingRight: getRem(12),
    marginLeft: getRem(8),
    marginRight: getRem(8),
    flexDirection: 'row',
    borderBottomWidth: getRem(1),
    borderBottomColor: '#e7e7e7',
    height: getRem(52),
    alignItems: 'center',
  },
  rowTitle: {
    width: getRem(120),
    fontSize: getRem(14),
    textAlign: 'left',
    color: '#333333'
  },
  rowText: {
    flex: 1,
    fontSize: getRem(14),
    textAlign: 'right',
    fontWeight: '300',
    color: '#666666'
  }
})

type Props = {
  vehicleData: Object,
}
function VehicleDetailInfo(props: Props) {
  const { vehicleData } = props
  return (
    <View style={styles.root}>
      <View style={styles.rowItem} >
        <Text style={styles.rowTitle}>{strings.plateNoVehicleInfo}</Text>
        <Text style={styles.rowText}>
          {vehicleData && (vehicleData.plateNo || vehicleData.legacyPlateNo)}
        </Text>
      </View>
      <View style={styles.rowItem} >
        <Text style={styles.rowTitle}>{strings.vinVehicleInfo}</Text>
        <Text style={styles.rowText}>{vehicleData && vehicleData.vin}</Text>
      </View>
      <View style={styles.rowItem} >
        <Text style={styles.rowTitle}>{strings.carModel}</Text>
        <Text style={styles.rowText}>{vehicleData && vehicleData.model}</Text>
      </View>
      <View style={styles.rowItem} >
        <Text style={styles.rowTitle}>{strings.engineNoVehicleInfo}</Text>
        <Text style={styles.rowText}>{vehicleData && vehicleData.engineNo}</Text>
      </View>
      <View style={styles.rowItem} >
        <Text style={styles.rowTitle}>{strings.registerAreaVehicleInfo}</Text>
        <Text style={styles.rowText}>{vehicleData && vehicleData.registerArea}</Text>
      </View>
      <View style={styles.rowItem} >
        <Text style={styles.rowTitle}>{strings.registerUnderVehicleInfo}</Text>
        <Text style={styles.rowText}>{vehicleData && vehicleData.registerUnder}</Text>
      </View>
      <View style={styles.rowItem} >
        <Text style={styles.rowTitle}>{strings.registerDateVehicleInfo}</Text>
        <Text style={styles.rowText}>
          {vehicleData && vehicleData.registerDate && format(new Date(vehicleData.registerDate), 'YYYY-MM-DD')}
        </Text>
      </View>
      <View style={[styles.rowItem, { borderBottomWidth: 0 }]} >
        <Text style={[styles.rowTitle, { width: getRem(200) }]}>
          {strings.annualInspectionDueDateVehicleInfo}</Text>
        <Text style={styles.rowText}>
          {vehicleData && vehicleData.insurance && vehicleData.insurance.annualInspectionDueDate &&
          format(new Date(vehicleData.insurance.annualInspectionDueDate), 'YYYY-MM-DD')}
        </Text>
      </View>
    </View>
  )
}

export default VehicleDetailInfo
