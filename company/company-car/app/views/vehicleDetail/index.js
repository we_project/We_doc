/**
 * @flow
 */
import React, { Component } from 'react'
import {
  StyleSheet,
  View,
  Image,
  Text,
  TouchableOpacity,
  ScrollView
} from 'react-native'

import VehicleDetailInfo from './VehicleDetailInfo'
import VehicleDetailTab from './VehicleDetailTab'
import VehicleInsurance from './VehicleInsurance'
import Violations from './Violations'
import Localized from '../../utils/Localized'
import AppNetworking from '../../utils/AppNetworking'
import ImageMapper from '../../utils/CarImageMapper'
import { getRem } from '../../styles/getRem'
import BottomButton from '../../components/BottomButton'
import Reload from '../../components/Reload'
import Toast from '../../components/Toast'

const strings = Localized.Strings
const Networking = new AppNetworking()
const styles = StyleSheet.create({
  root: {
    flex: 1,
  },
  headerContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff',
    paddingBottom: getRem(20),
  },
  vehicleImage: {
    width: getRem(266),
    height: getRem(200),
  },
  vehicleContainer: {
    backgroundColor: '#f8f8f8',
    borderWidth: getRem(1),
    borderRadius: getRem(2),
    borderColor: '#d8d8d8',
    justifyContent: 'center',
    paddingLeft: getRem(2),
    paddingRight: getRem(2),
  },
  vehicleNumber: {
    fontWeight: 'bold',
    fontSize: getRem(16),
  },
  vehicleSeries: {
    fontSize: getRem(14),
    textAlign: 'center',
    color: '#333333',
    marginTop: getRem(8),
  },
  VehicleTab: {
    paddingTop: getRem(11),
  },
  incorrectText: {
    fontSize: getRem(12),
    fontWeight: '300',
    color: '#1f88d8',
    marginTop: getRem(30),
    marginBottom: getRem(30),
    textAlign: 'center',
    textDecorationLine: 'underline',
  },
  buttonContainer: {
    bottom: getRem(0),
    backgroundColor: 'rgba(31, 136, 216, 0.9)',
    alignItems: 'center',
    justifyContent: 'center',
    height: getRem(48),
  },
  buttonText: {
    color: '#ffffff',
    fontSize: getRem(16),
    fontWeight: '300',
  },
  referenceImage: {
    position: 'absolute',
    left: getRem(20),
    top: getRem(20),
    fontSize: getRem(12),
    fontWeight: '300',
    lineHeight: getRem(16),
    color: '#999999',
  },
  statusLabel: {
    position: 'absolute',
    top: getRem(35),
    right: getRem(0),
    backgroundColor: '#1f88d8',
    fontSize: getRem(9),
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#ffffff',
    paddingLeft: getRem(10),
    paddingRight: getRem(10),
    paddingVertical: getRem(3),
  },
  stocked: {
    backgroundColor: '#d8d8d8',
  }
})

type State = {
  vehicleDetail: object,
  tabIndex: number,
  id: string,
}

type Props = {
  navigation: any,
}

class VehicleDetail extends Component {
  static navigationOptions = () => ({
    title: strings.titleCarDetail,
  })

  constructor(props: Props) {
    super(props)
    this.state = {
      id: this.props.navigation.state.params.id,
      tabIndex: 0,
      hasLoad: false,
    }
  }

  state: State

  componentDidMount() {
    this.loadVehicleDetail(this.state.id)
  }

  getTabContent = () => {
    const { tabIndex, vehicleDetail } = this.state
    const carUserCompany = vehicleDetail.usage === 'Business Car' ?
      vehicleDetail.businessCarUserCompany :
      vehicleDetail.userCompany

    switch (tabIndex) {
      case 0:
        return (<VehicleDetailInfo vehicleData={vehicleDetail} />)
      case 1:
        return (
          <VehicleInsurance
            toast={this.refs.toast}
            insuranceData={vehicleDetail && vehicleDetail.insurance}
            userData={vehicleDetail && vehicleDetail.userDetail}
            userCompanyData={carUserCompany}
          />
        )
      case 2:
        return (<Violations />)
      default:
        return null
    }
  }

  handleActive = (tabIndex: number) => {
    this.setState({ tabIndex })
  }

  props: Props

  loadVehicleDetail(id) {
    Networking.getVehicleDetail(id)
      .then((res) => {
        this.setState({
          vehicleDetail: res
        })
      }, err => {
        this.setState({
          errString: err.message
        })
        this.setState({ hasLoad: true })
      })
  }

  handleNavigate = () => {
    const { vehicleDetail } = this.state
    const { navigate } = this.props.navigation
    global.stockDataService.clearAll()
    const stockData = {
      plateNo: vehicleDetail.plateNo || vehicleDetail.legacyPlateNo,
      model: vehicleDetail.model,
      vin: vehicleDetail.vin,
      id: vehicleDetail.id,
    }
    global.stockDataService.addStockData(stockData)
    if (!stockData.plateNo) {
      navigate('StockTakingOne')
    }
    if (stockData.plateNo && vehicleDetail) {
      navigate('InputVehiclePlate')
    }
  }

  renderStockTaken = (status) => {
    switch (status) {
      case 'TASK':
        return strings.takeStock
      case 'SUBMIT':
        return strings.submitted
      case 'DONE':
        return strings.counted
      default :
        return ''
    }
  }

  render() {
    const { vehicleDetail, hasLoad } = this.state
    const isStockTaken = vehicleDetail && vehicleDetail.stockStatus !== 'TASK'
    return (
      <View style={styles.root}>
        {vehicleDetail ?
          <View style={styles.root}>
            <ScrollView>
              <View style={styles.headerContainer}>
                <Image source={ImageMapper(vehicleDetail.model)} style={styles.vehicleImage} />
                <Text style={styles.referenceImage}>{strings.referenceImage}</Text>
                <Text
                  style={[styles.statusLabel,
                    vehicleDetail && vehicleDetail.stockStatus !== 'TASK' ? styles.stocked : null
                  ]}
                >{strings.stockStatus[vehicleDetail.stockStatus]}</Text>
                <View style={styles.vehicleContainer}>
                  <Text style={styles.vehicleNumber}>
                    {vehicleDetail && (vehicleDetail.plateNo || vehicleDetail.legacyPlateNo || strings.noPlateNumber)}
                  </Text>
                </View>
                <Text style={styles.vehicleSeries}>{ vehicleDetail && vehicleDetail.model }</Text>
              </View>
              <View style={styles.VehicleTab}>
                <VehicleDetailTab tabIndex={this.state.tabIndex} onActive={this.handleActive} />
                { this.getTabContent() }
                { false ? <TouchableOpacity>
                  <Text style={styles.incorrectText}>
                    {strings.reportLink}
                  </Text>
                </TouchableOpacity> : null }
              </View>
            </ScrollView>
            <BottomButton
              disabled={isStockTaken}
              text={this.renderStockTaken(vehicleDetail.stockStatus)}
              onPress={!isStockTaken && this.handleNavigate}
            />
          </View> :
          <View style={{ flex: 1 }}>{hasLoad ?
            <Reload
              onRefresh={() => this.loadVehicleDetail(this.state.id)}
            /> : null }
          </View>
        }
        <Toast ref='toast' />
      </View>
    )
  }
}

module.exports = VehicleDetail
