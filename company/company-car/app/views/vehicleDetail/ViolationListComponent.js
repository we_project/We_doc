/**
 * @flow
 */
import React, { Component } from 'react'
import {
  StyleSheet,
  View,
  Text,
  ListView,
} from 'react-native'
import ViolationAccordionPanel from '../../components/ViolationAccordionPanel'

import Localized from '../../utils/Localized'
import CommonStyle from '../../styles/CommonStyle'
import { getRem } from '../../styles/getRem'

const commonStyle = CommonStyle.styles
const strings = Localized.Strings

const styles = StyleSheet.create({
  RowContainer: {
    height: getRem(120),
    flexDirection: 'row',
    backgroundColor: '#fff',
  },
  root:{
    backgroundColor: '#ffffff',
  }
})

const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })

type State = {
  dataSource: object,
}

type Props = {
  data: object
}

class ViolationListComponent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      dataSource: ds.cloneWithRows(this.props.data),
      refreshing: false
    }
  }

  state: State

  onRefresh = () => {
    if (this.props.onRefresh) {
      this.setState({ refreshing: true })
      this.props.onRefresh().then(() => {
        this.setState({ refreshing: false })
      })
    }
  }

  props: Props

  loadMoreContentAsync = () => {
    if (this.props.onEndReached) {
      this.props.onEndReached()
    }
  }

  handleNavigate = (data) => {
    this.props.onItemClick(data.id)
  }
  renderRow = (data: Object) => (
    <View style={styles.root}>
      <ViolationAccordionPanel data={data}>
        <View>
          <Text>{data.violationLocation}</Text>
          <Text>{data.violationDetail}</Text>
        </View>
      </ViolationAccordionPanel>
    </View>
  )

  render() {
    return (
      <ListView
        onEndReachedThreshold={1}
        onEndReached={this.loadMoreContentAsync}
        style={commonStyle.bgDefault}
        dataSource={ds.cloneWithRows(this.props.data)}
        renderRow={this.renderRow}
      />
    )
  }
}

module.exports = ViolationListComponent
