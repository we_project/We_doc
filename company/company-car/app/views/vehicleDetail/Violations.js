/**
 * @flow
 */
import React, { Component } from 'react'
import { StyleSheet, View, Text, Platform } from 'react-native'
import { getRem } from '../../styles/getRem'
import Localized from '../../utils/Localized'
import ViolationListComponent from './ViolationListComponent'

const strings = Localized.Strings

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  rowItem: {
    flex: 1,
    justifyContent: 'center',
    ...Platform.select({
      ios: {
        height: getRem(208),
      },
      android: {
        height: getRem(170),
      },
    }),
  },

  text: {
    alignSelf: 'center',
    color: '#9b9b9b',
  }
})

class Violations extends Component {
  render() {
    const data = [{ fineAmount: 100,
      demeritPoint: 1,
      violationDate: '2011-01-01',
      violationTime: '12:20',
      violationType: 'ANY',
      violationLocation: 'Beijing Dongzhimen Road',
      violationDetail: '驾驶中型以上载客载货汽车、危险物品运输车辆以外的其他机动车行驶超过规定时速10%未达20%的',
    },
    { fineAmount: 100,
      demeritPoint: 1,
      violationDate: '2011-01-01',
      violationTime: '12:20',
      violationType: 'ANY',
      violationLocation: 'Beijing Dongzhimen Road',
      violationDetail: '驾驶中型以上载客载货汽车、危险物品运输车辆以外的其他机动车行驶超过规定时速10%未达20%的',
    }]
    return (
      <View style={styles.root}>
        <ViolationListComponent data={data} />
      </View>
    )
  }
}

module.exports = Violations
