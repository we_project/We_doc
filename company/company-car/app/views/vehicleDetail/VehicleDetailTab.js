/**
 * @flow
 */
import React from 'react'
import { View, TouchableOpacity, Text, StyleSheet } from 'react-native'

import Localized from '../../utils/Localized'
import { getRem } from '../../styles/getRem'

const strings = Localized.Strings
const styles = StyleSheet.create({
  tabContainer: {
    height: getRem(50),
    backgroundColor: '#ffffff',
    flexDirection: 'row',
    alignItems: 'center',
  },
  tabItem: {
    flex: 1,
    justifyContent: 'center',
  },
  tabItemText: {
    fontSize: getRem(16),
    lineHeight: getRem(17),
    color: '#9b9b9b',
    textAlign: 'center',
  },
  activeTabItemText: {
    fontWeight: 'bold',
    color: '#216ab3',
  },
  activeTabItemView: {
    borderBottomColor: 'rgba(33, 106, 179, 0.8)',
    borderBottomWidth: getRem(2),
  },
  normalTabItemView: {
    borderBottomWidth: getRem(2),
    borderBottomColor: '#f8f8f8',
  }
})

type Props = {
  onActive: (id: string) => void,
  tabIndex: number,
}

function VehicleTab(props: Props) {
  const { onActive, tabIndex } = props
  return (
    <View style={styles.tabContainer}>
      <TouchableOpacity style={styles.tabItem} onPress={() => { onActive(0) }}>
        <View
          style={[styles.tabItem, tabIndex === 0 ?
            styles.activeTabItemView : styles.normalTabItemView]}
        >
          <Text style={[styles.tabItemText, tabIndex === 0 ? styles.activeTabItemText : null]}>
            {strings.vehicle}
          </Text>
        </View>
      </TouchableOpacity>
      <TouchableOpacity style={styles.tabItem} onPress={() => { onActive(1) }}>
        <View
          style={[styles.tabItem, tabIndex === 1 ?
            styles.activeTabItemView : styles.normalTabItemView]}
        >
          <Text style={[styles.tabItemText, tabIndex === 1 ? styles.activeTabItemText : null]}>
            {strings.related}
          </Text>
        </View>
      </TouchableOpacity>
      <TouchableOpacity style={styles.tabItem} onPress={() => { onActive(2) }}>
        <View
          style={[styles.tabItem, tabIndex === 2 ?
            styles.activeTabItemView : styles.normalTabItemView]}
        >
          <Text style={[styles.tabItemText, tabIndex === 2 ? styles.activeTabItemText : null]}>
            {strings.violation}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  )
}

export default VehicleTab
