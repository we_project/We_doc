/**
 * @flow
 */
import React from 'react'
import { View, Text, StyleSheet, Linking, TouchableOpacity } from 'react-native'
import { format } from 'date-fns'
import { words, split, isEmpty, uniqueId, findIndex } from 'lodash'
import { Clipboard } from 'react-native'

import Localized from '../../utils/Localized'
import { getRem } from '../../styles/getRem'

const strings = Localized.Strings

const styles = StyleSheet.create({
  root: {
    backgroundColor: '#ffffff',
  },
  rowItem: {
    paddingTop: getRem(10),
    paddingBottom: getRem(10),
    paddingLeft: getRem(12),
    paddingRight: getRem(12),
    marginLeft: getRem(8),
    marginRight: getRem(8),
    flexDirection: 'row',
    borderBottomWidth: getRem(1),
    borderBottomColor: '#e7e7e7',
    height: getRem(52),
    alignItems: 'center',
  },
  rowTitle: {
    width: getRem(120),
    fontSize: getRem(14),
    textAlign: 'left',
    color: '#333333',
  },
  rowText: {
    flex: 1,
    fontSize: getRem(14),
    textAlign: 'right',
    fontWeight: '300',
    color: '#666666',
  },
  rowTextWithColor: {
    color: '#1f88d8',
  },
  telContainer: {
    flex: 1,
    paddingTop: getRem(8),
    justifyContent: 'center',
  },
  insuranceTel: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
})

type Props = {
  insuranceData: Object,
  userData: Object,
  userCompanyData: string,
  toast: any,
}


class VehicleInsurance extends React.Component {
  props: Props
  handleTel = (tel) => {
    if (!tel) { return }
    const url = `tel:${tel}`
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        return Linking.openURL(url)
      }
      this.props.toast.show(strings.nonsupport)
      return null
    }).catch(() => {
      this.props.toast.show(strings.nonsupport)
    })
  }

  setClipboardContent = (value) => {
    if (!value) { return }
    Clipboard.setString(value)
  }

  parseTelNumber = (value) => {
    const mobilePattern = /(13|14|15|18|17)[0-9]{9}/
    const phonePattern = /[0-9]{2}\s[0-9]{8}/

    const mobileNumber = mobilePattern.exec(value.replace(/\s/g, ''))
    if (mobileNumber) {
      return mobileNumber[0]
    }
    if (phonePattern.exec(value)) {
      return phonePattern.exec(value)[0]
    }

    return value
  }


  renderAccidentTel = (tel) => {
    const telArray = words(tel, /\d{5,}/g)
    let telString = split(tel, /\d{5,}/g)
    telString = telString.filter((v) => !isEmpty(v))
    const telElements = telArray.map((v, index) => (
      <View style={{ flexDirection: 'row' }} key={uniqueId()}>
        <TouchableOpacity>
          <Text
            onPress={() => { this.handleTel(v) }}
            selectable
            onLongPress={() => this.setClipboardContent(v)}
            style={[styles.rowText, styles.rowTextWithColor]}
          >
            {v}
          </Text>
        </TouchableOpacity>
        <Text style={[{
          textAlign: 'right',
          fontWeight: '300',
          color: '#666666',
        }]}
        >
          {telString[index]}
        </Text>
      </View>
    ))
    return telElements ?
      <View style={styles.insuranceTel}>
        {telElements}
      </View>
      : <Text style={styles.rowText}>{tel}</Text>
  }

  render() {
    const { insuranceData, userData, userCompanyData } = this.props
    return (
      <View style={styles.root}>
        <View style={styles.rowItem} >
          <Text style={styles.rowTitle}>{strings.carUserName}</Text>
          <Text style={styles.rowText}>{userData && userData.empName}</Text>
        </View>
        <View style={styles.rowItem} >
          <Text style={styles.rowTitle}>{strings.carUserEmail}</Text>
          <Text style={styles.rowText}>{userData && userData.email}</Text>
        </View>
        <View style={styles.rowItem} >
          <Text style={styles.rowTitle}>{strings.carUserPhone}</Text>
          <TouchableOpacity style={styles.telContainer}>
            <Text
              style={[styles.rowText, styles.rowTextWithColor]}
              selectable
              onPress={() => { this.handleTel(this.parseTelNumber(userData.businessPhone)) }}
              onLongPress={() => this.setClipboardContent(userData && this.parseTelNumber(userData.businessPhone))}
            >
              {userData && this.parseTelNumber(userData.businessPhone)}
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.rowItem} >
          <Text style={styles.rowTitle}>{strings.carUserMobilePhone}</Text>
          <TouchableOpacity style={styles.telContainer}>
            <Text
              style={[styles.rowText, styles.rowTextWithColor]}
              selectable
              onPress={() => { this.handleTel(this.parseTelNumber(userData.mobilePhone)) }}
              onLongPress={() => this.setClipboardContent(userData && this.parseTelNumber(userData.mobilePhone))}
            >
              {userData && this.parseTelNumber(userData.mobilePhone)}
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.rowItem} >
          <Text style={styles.rowTitle}>{strings.carUserCompany}</Text>
          <Text style={styles.rowText}>{userCompanyData}</Text>
        </View>
        <View style={styles.rowItem} >
          <Text style={[styles.rowTitle, { width: getRem(180) }]}>
            {strings.insuranceCompanyVehicleInfo}</Text>
          <Text style={styles.rowText}>
            {insuranceData && (insuranceData.insuranceCompany)}
          </Text>
        </View>
        <View style={styles.rowItem} >
          <Text style={styles.rowTitle}>{strings.insuranceAccidentTelVehicleInfo}</Text>
          {insuranceData && (this.renderAccidentTel(insuranceData.insuranceAccidentTel))}
        </View>
        <View style={styles.rowItem} >
          <Text style={styles.rowTitle}>{strings.insuranceEndDateVehicleInfo}</Text>
          <Text style={styles.rowText}>
            {insuranceData && insuranceData.insuranceEndDate && format(new Date(insuranceData.insuranceEndDate), 'YYYY-MM-DD')}
          </Text>
        </View>
        <View style={[styles.rowItem, { borderBottomWidth: 0 }]} >
          <Text style={styles.rowTitle}>{strings.roadsideAssistanceVehicleInfo}</Text>
          <TouchableOpacity
            style={styles.telContainer}
          >
            <Text
              style={[styles.rowText, styles.rowTextWithColor]}
              selectable
              onPress={() => { this.handleTel(insuranceData.roadsideAssistance) }}
              onLongPress={() => this.setClipboardContent(insuranceData.roadsideAssistance)}
            >
              {insuranceData && insuranceData.roadsideAssistance}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

export default VehicleInsurance
