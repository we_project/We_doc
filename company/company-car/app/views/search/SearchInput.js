import React, { Component } from 'react'
import {
  View,
  TextInput,
  Image,
  StyleSheet,
  TouchableWithoutFeedback,
  Keyboard
} from 'react-native'
import { getRem } from '../../styles/getRem'
import Localized from '../../utils/Localized'
import images from '../../images/index'

const strings = Localized.Strings
const styles = StyleSheet.create({
  inputRow: {
    top: 0,
    flex: 1,
    height: getRem(28),
    paddingLeft: getRem(10),
    alignItems: 'flex-start',
    flexDirection: 'row',
    borderColor: '#ccc',
    borderRadius: getRem(4),
    backgroundColor: '#f4f4f4'
  },
  icon: {
    alignItems: 'center',
    margin: getRem(7)
  },
  input: {
    flex: 1,
    paddingVertical: getRem(0),
    height: getRem(17),
    fontSize: getRem(14),
    color: '#333333',
    fontWeight: '300',
    fontFamily: 'HelveticaNeue-Light',
    margin: getRem(6),
    marginBottom: getRem(5)
  },
})

type Props = {
  style: any,
  onSubmitEditing: any,
}

type State = {
  searchKeyword: string,
}

class SearchInput extends Component {
  state: State
  props: Props
  constructor(props: Props) {
    super(props)
    this.state = {
      searchKeyword: ''
    }
  }
  onChangeText=(searchKeyword) => {
    this.setState({ searchKeyword })
    if (this.props.onChangeText && typeof this.props.onChangeText === 'function') {
      this.props.onChangeText(searchKeyword)
    }
  }
  render() {
    const { searchKeyword } = this.state
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.inputRow}>
          <Image source={images.search} style={styles.icon} />
          <TextInput
            style={styles.input}
            autoFocus
            placeholderTextColor='#999999'
            placeholder={strings.searchPlaceholder}
            value={searchKeyword}
            onChangeText={this.onChangeText}
            returnKeyType='search'
            onSubmitEditing={() => { this.props.onSubmitEditing(searchKeyword) }}
            underlineColorAndroid={'rgba(0,0,0,0)'}
          />
        </View>
      </TouchableWithoutFeedback>)
  }
}

module.exports = SearchInput
