/**
 * @flow
 */
import React, { Component } from 'react'
import { StyleSheet, Platform,View, Dimensions, Text, TouchableWithoutFeedback, Keyboard } from 'react-native'

import CommonStyle from '../../styles/CommonStyle'
import Localized from '../../utils/Localized'
import AppNetworking from '../../utils/AppNetworking'
import VehicleListComponent from '../../views/vehicleList/VehicleListComponent'
import { getRem } from '../../styles/getRem'
import SearchInput from './SearchInput'

const strings = Localized.Strings
const { height, width } = Dimensions.get('window')
const Networking = new AppNetworking()

const styles = StyleSheet.create({
  topBg: {
    width: CommonStyle.size.width,
    height: getRem(5),
  },
  searchContainer: {
    paddingLeft: getRem(20),
    paddingRight: getRem(10),
    paddingBottom: getRem(8),
    backgroundColor: '#ffffff',
    alignItems: 'flex-start',
    flexDirection: 'row',
    borderBottomColor: '#d8d8d8',
    borderBottomWidth: getRem(1),
    borderStyle: 'solid',
    ...Platform.select({
      ios: {
        paddingTop: getRem(28),
      },
      android: {
        paddingTop: getRem(8),
      },
    }),
  },
  loading: {
    alignSelf: 'center',
    width: getRem(20),
    height: getRem(20)
  },
  root: {
    marginTop: getRem(20),
    backgroundColor: '#f4f4f4',
  },
  wrapper: {
    backgroundColor: '#f4f4f4',
    height
  },
  cancel: {
    fontWeight: '300',
    fontSize: getRem(14),
    marginTop: getRem(6),
    marginBottom: getRem(5),
    marginLeft: getRem(15),
    marginRight: getRem(10),
    ...Platform.select({
      android: {
        marginTop: getRem(3),
      } }),
    height: getRem(17),
    alignSelf: 'center',
    color: '#999999'
  },
  emptyResult: {
    marginTop: getRem(260),
    marginLeft: (width / 2) - getRem(80)
  },
  emptyResultText: {
    width: 159.8,
    fontFamily: 'HelveticaNeue',
    fontSize: getRem(16),
    height: getRem(20),
    textAlign: 'center',
    color: '#999999'
  }
})

type State = {
  vehicle: any,
  errString: any,
  isLoading: any,
  photoInfo: any,
  imageUri: any
}

type Props = {
  navigation: any,
}

class SearchVehicle extends Component {
  static navigationOptions = ({ navigation }) => ({
    header: null
  })

  constructor(props: Props) {
    super(props)
    this.state = {
      vehicle: [],
      errString: null,
      isLoading: false,
      keyword: this.props.navigation.state.params && this.props.navigation.state.params.keyword,
      hasSearched: false
    }
  }

  state: State
  props: Props

  componentWillMount() {
    this.setState({
      isLoading: true
    })
  }

  handleItemClick = (id: string) => {
    this.props.navigation.navigate('VehicleDetail', { id })
  }

  onSubmitEditing = (keyword) => {
    this.vehicleListRequest(keyword)
    Keyboard.dismiss()
  }
  cancel =() => {
    this.props.navigation.goBack()
  }

  getEmptyResultView =() => (
    <View style={styles.emptyResult}>
      <Text style={styles.emptyResultText}>{strings.noResultFound}</Text>
    </View>
  )

  vehicleListRequest(keyword) {
    const self = this
    Networking.searchVehicleList(keyword)
      .then((res) => {
        self.setState({
          vehicle: res,
          isLoading: false,
          errString: null,
          hasSearched: true
        })
      }, err => {
        self.setState({
          errString: err.message,
          isLoading: false,
          hasSearched: true
        })
      })
  }

  onChangeText =(keyword) => {
    this.setState({ keyword })
    this.setState({ hasSearched: false })
  }
  render() {
    return (
      <TouchableWithoutFeedback style={styles.root} onPress={Keyboard.dismiss}>
        <View style={styles.wrapper}>
          <View style={styles.searchContainer}>
            <SearchInput
              onSubmitEditing={this.onSubmitEditing}
              onChangeText={this.onChangeText}
            />
            <Text style={styles.cancel} onPress={this.cancel}>{strings.cancel}</Text>
          </View>
          {this.state.vehicle.length > 0 ?
            <VehicleListComponent
              disableRefresh
              withHeader
              onItemClick={this.handleItemClick}
              data={this.state.vehicle}
            /> :
            this.state.keyword && this.state.hasSearched ? this.getEmptyResultView() : null}
        </View>
      </TouchableWithoutFeedback>
    )
  }
}

module.exports = SearchVehicle
