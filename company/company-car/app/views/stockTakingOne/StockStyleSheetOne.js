import { StyleSheet } from 'react-native'

import { getRem } from '../../styles/getRem'

const styles = StyleSheet.create({
  headerTitle: {
    fontSize: getRem(18),
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#000000'
  },
  headerSmallTitle: {
    fontSize: getRem(12),
    fontWeight: '300',
    lineHeight: getRem(16),
    color: '#9b9b9b',
    textAlign: 'center',
  },
  root: {
    flex: 1,
  },
  vehicleInfoItem: {
    height: getRem(52),
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    borderBottomWidth: getRem(1),
    borderBottomColor: 'rgba(33, 33, 33, 0.1)',
  },
  vehicleInfoLeft: {
    flex: 1,
    fontSize: getRem(14),
    color: '#333333',
  },
  vehicleInfoRight: {
    fontSize: getRem(14),
    fontWeight: '300',
    lineHeight: getRem(16),
    textAlign: 'right',
    color: '#999999',
  },
  exist: {
    color: '#666666',
  },
  grayLocationIcon: {
    marginRight: getRem(10),
  },
  leftArrowIcon: {
    width: getRem(6),
    height: getRem(12),
    marginLeft: getRem(10),
    marginTop: getRem(3)
  },
  exteriorPhotoTitle: {
    textAlign: 'left',
    marginTop: getRem(20),
    marginBottom: getRem(6),
    color: '#333333',
  },
  exteriorPhotoTips: {
    fontSize: getRem(11),
    fontWeight: '300',
    lineHeight: getRem(16),
    color: '#9b9b9b',
    textAlign: 'left',
  },
  photoView: {
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
})

export default styles
