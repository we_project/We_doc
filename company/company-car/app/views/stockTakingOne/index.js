import React from 'react'
import { View, Text, ScrollView, Image, TouchableOpacity } from 'react-native'

import PanelButton from '../../components/PanelButton'
import { getRem } from '../../styles/getRem'
import Localized from '../../utils/Localized'
import styles from './StockStyleSheetOne'
import { EXTERIOR_COLOUR, INTERIOR_COLOUR } from '../../utils/PlateNumber'
import Toast from '../../components/Toast'
import images from '../../images'
import ModalPicker from '../../components/ModalPicker'
import Card from '../../components/Card'
import PhotoSquare from '../../components/PhotoSquare/index'
import VehicleInfoCard from '../../components/VehicleInfoCard'
import checkStockData from '../../utils/CheckStockData'

const strings = Localized.Strings

type Props = {
  navigation: object,
}
export default class StockTakingOne extends React.Component {
  static navigationOptions = () => (
    {
      headerStyle: {
        height: getRem(70),
        backgroundColor: '#ffffff',
      },
      headerTitle: (
        <View style={{ alignSelf: 'center' }}>
          <Text style={styles.headerTitle}>{strings.stockTaking}</Text>
          <Text style={styles.headerSmallTitle}>{strings.stockTakingSmallTitleOne}</Text>
        </View>
      ),
    }
  )

  constructor() {
    super()
    const {
      location,
      exteriorColour,
      interiorColour,
      leftFrontImage,
      rightBackImage,
      licenseImage,
    } = global.stockDataService.getStockData
    this.state = {
      location,
      exteriorColour,
      interiorColour,
      leftFrontImage,
      rightBackImage,
      licenseImage,
    }
  }

  getLocation = () => {
    global.navigator.geolocation.getCurrentPosition((position) => {
      const location = {
        latitude: position.coords.latitude,
        longitude: position.coords.longitude,
      }
      this.setState({ location })
    }, (error) => {
      console.log(error)
      this.refs.toast.show(strings.getLocationFailure)
    }, {
      timeout: 50000,
      maximumAge: 0,
    })
  }

  props: Props

  render() {
    const { location, exteriorColour, interiorColour, leftFrontImage,
      rightBackImage, licenseImage } = this.state
    global.stockDataService.addStockData({
      location,
      exteriorColour,
      interiorColour,
      leftFrontImage,
      rightBackImage,
      licenseImage,
    })
    const isEnable = checkStockData(global.stockDataService.getStockData, [
      'location',
      'exteriorColour',
      'interiorColour',
      'leftFrontImage',
      'rightBackImage',
      'licenseImage'])
    return (
      <View style={styles.root}>
        <ScrollView>
          <VehicleInfoCard data={global.stockDataService.getStockData} />
          <Card title={strings.vehicleBasicInformation} icon={images.carIcon}>
            <View style={styles.vehicleInfoItem}>
              <Text style={styles.vehicleInfoLeft}>{strings.location}</Text>
              <TouchableOpacity style={{ flexDirection: 'row' }} onPress={() => { this.getLocation() }}>
                <Image
                  style={styles.grayLocationIcon}
                  source={location ? images.blueLocation : images.grayLocation}
                />
                <Text
                  style={[styles.vehicleInfoRight,
                    location ? styles.exist : null]}
                >{strings.myLocation}</Text>
              </TouchableOpacity>
            </View>
            <ModalPicker
              data={EXTERIOR_COLOUR.map((v) =>
                ({ key: v, label: strings.color[v] }))}
              initValue={exteriorColour}
              onChange={option => {
                this.setState({ exteriorColour: option.key })
              }}
            >
              <View style={styles.vehicleInfoItem}>
                <Text style={styles.vehicleInfoLeft}>{strings.exteriorColour}</Text>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.vehicleInfoRight,
                    exteriorColour ? styles.exist : null]}
                  >
                    {strings.color[exteriorColour] || strings.required}</Text>
                  <Image
                    source={exteriorColour ? images.rightArrow : images.rightGrayArrow}
                    style={styles.leftArrowIcon}
                  />
                </View>
              </View>
            </ModalPicker>
            <ModalPicker
              data={INTERIOR_COLOUR.map((v) =>
                ({ key: v, label: strings.color[v] }))}
              initValue={interiorColour}
              onChange={option => {
                this.setState({ interiorColour: option.key })
              }}
            >
              <View style={styles.vehicleInfoItem}>
                <Text style={styles.vehicleInfoLeft}>{strings.interiorColour}</Text>
                <View style={{ flexDirection: 'row' }} >
                  <Text style={[styles.vehicleInfoRight,
                    interiorColour ? styles.exist : null]}
                  >
                    {strings.color[interiorColour] || strings.required}</Text>
                  <Image
                    source={interiorColour ? images.rightArrow : images.rightGrayArrow}
                    style={styles.leftArrowIcon}
                  />
                </View>
              </View>
            </ModalPicker>
            <Text style={styles.exteriorPhotoTitle}>{strings.exteriorPhoto}</Text>
            <Text style={styles.exteriorPhotoTips}>{strings.pleaseTakePhotosOfTheEntireCar}</Text>
            <View style={styles.photoView}>
              <PhotoSquare
                value={leftFrontImage}
                title={strings.leftFront}
                navigation={this.props.navigation}
                tipImage={images.leftFront}
                onCancel={() => this.setState({ leftFrontImage: null })}
                onChange={(leftFront) => this.setState({
                  leftFrontImage: Object.assign({}, leftFrontImage, leftFront) })}
              />
              <PhotoSquare
                value={rightBackImage}
                title={strings.rightBack}
                tipImage={images.rightBack}
                navigation={this.props.navigation}
                onCancel={() => this.setState({ rightBackImage: null })}
                onChange={rightBack => this.setState({
                  rightBackImage: Object.assign({}, rightBackImage, rightBack) })}
              />
            </View>
          </Card>
          <Card title={strings.vehicleLicense} icon={images.license}>
            <PhotoSquare
              value={licenseImage}
              isLicense
              navigation={this.props.navigation}
              onCancel={() => this.setState({ licenseImage: null })}
              onChange={license => this.setState({
                licenseImage: Object.assign({}, licenseImage, license) })}
            />
          </Card>
          <PanelButton
            status='enabled'
            text={strings.next}
            disabled={!isEnable}
            onPress={isEnable ?
              () => { this.props.navigation.navigate('StockTakingTwo') } : null}
          />
          <Toast ref='toast' />
        </ScrollView>
      </View>
    )
  }
}
