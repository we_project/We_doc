/**
 * @flow
 */
import React, { Component } from 'react'
import {
  StyleSheet,
  View,
  Image,
  Text,
  ListView,
  RefreshControl,
  TouchableOpacity
} from 'react-native'
import ListViewFooter from '../../components/ListViewFooter'

import Localized from '../../utils/Localized'
import CommonStyle from '../../styles/CommonStyle'
import ImageMapper from '../../utils/CarImageMapper'
import { getRem } from '../../styles/getRem'

const commonStyle = CommonStyle.styles
const strings = Localized.Strings

const styles = StyleSheet.create({
  RowContainer: {
    height: getRem(120),
    flexDirection: 'row',
    backgroundColor: '#fff',
  },
  left: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  right: {
    flex: 1,
    paddingTop: getRem(20),
    paddingBottom: getRem(20),
  },
  vehicle: {
    resizeMode: 'contain',
    flex: 1,
  },
  stockContainer: {
    alignItems: 'flex-end',
    flex: 1
  },
  stock: {
    backgroundColor: '#288AD5',
    paddingVertical: getRem(3),
    width: getRem(65),
    alignItems: 'center',
    justifyContent: 'center',
  },
  stocked: {
    backgroundColor: '#D8D8D8',
  },
  txtStock: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: getRem(9),
  },
  plate: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
  },
  noContainer: {
    marginTop: getRem(-1),
    borderWidth: getRem(1),
    borderRadius: getRem(2),
    borderColor: '#D8D8D8',
    backgroundColor: '#F8F8F8',
    paddingLeft: getRem(2),
    paddingRight: getRem(2),
    justifyContent: 'center',
  },
  no: {
    fontWeight: 'bold',
    color: '#333',
  },
  detail: {
    marginTop: getRem(10),
    marginBottom: getRem(10),
    flex: 2,
  },
  txtMode: {
    color: '#333'
  },
  txtTag: {
    color: '#fff',
    fontSize: getRem(10)
  },
  tag: {
    borderRadius: getRem(8),
    height: getRem(16),
    backgroundColor: '#484E63',
    marginTop: getRem(5),
    paddingLeft: getRem(8),
    paddingRight: getRem(8),
    paddingBottom: getRem(2),
    paddingTop: getRem(1),
    justifyContent: 'center',
  },
  delegate: {
    color: '#9b9b9b',
    fontSize: getRem(12),
    marginTop: getRem(5),
    paddingRight: getRem(12),
    marginRight: getRem(2),
    lineHeight: getRem(12),
  },
  header: {
    flex: 1,
    height: getRem(10)
  }
})

const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })

type State = {
  dataSource: object,
}

type Props = {
  data: object,
  disableRefresh: Boolean,
  withHeader: Boolean,
  onEndReached: () => void,
  onRefresh: () => Promise,
  onItemClick: (id: string) => any,
  needRenderFooter:Boolean
}

class VehicleListComponent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      dataSource: ds.cloneWithRows(this.props.data),
      refreshing: false
    }
  }

  state: State

  onRefresh = () => {
    if (this.props.onRefresh) {
      this.setState({ refreshing: true })
      this.props.onRefresh().then(() => {
        this.setState({ refreshing: false })
      })
    }
  }

  props: Props

  loadMoreContentAsync = () => {
    if (this.props.onEndReached) {
      this.props.onEndReached()
    }
  }

  handleNavigate = (data) => {
    this.props.onItemClick(data.id)
  }
  renderRow = (data: Object) => (
    <TouchableOpacity onPress={() => { this.handleNavigate(data) }} style={commonStyle.mb8}>
      <View style={[styles.RowContainer]}>
        <View style={styles.left}>
          <Image source={ImageMapper(data.model)} style={styles.vehicle} />
        </View>
        <View style={styles.right}>
          <View style={[styles.plate]}>
            <View style={[styles.noContainer]}>
              <Text style={[styles.no, commonStyle.f16]}>
                {data.plateNo || data.legacyPlateNo || strings.noPlateNumber}
              </Text>
            </View>
            <View style={styles.stockContainer}>
              <View style={[styles.stock, data.stockStatus !== 'TASK' ? styles.stocked : null]}>
                <Text style={[styles.txtStock]}>
                  {strings.stockStatus[data.stockStatus]}
                </Text>
              </View>
            </View>
          </View>
          <View style={styles.detail}>
            <Text style={[styles.txtMode, commonStyle.f12]}>{data.model}</Text>
            <View style={{ flexDirection: 'row' }}>
              <View style={[styles.tag]}>
                <Text style={styles.txtTag}>{data.usage}</Text>
              </View>
            </View>
            {data.delegateTo ? <Text style={styles.delegate} numberOfLines={2}>
              Delegated to: {data.delegateTo}
            </Text> : null}
            {data.delegateFrom ? <Text style={styles.delegate} numberOfLines={2}>
              Delegated From: {data.delegateFrom}
            </Text> : null}
          </View>
        </View>
      </View>
    </TouchableOpacity>)

  renderFooter = () => (this.props.needRenderFooter ? (<ListViewFooter />) : null)
  renderHeader =() => (this.props.withHeader ? (<View style={styles.header} />) : null)

  render() {
    return (
      <ListView
        onEndReachedThreshold={1}
        onEndReached={this.loadMoreContentAsync}
        refreshControl={this.props.disableRefresh ? null :
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this.onRefresh}
          />
        }
        style={commonStyle.bgDefault}
        dataSource={ds.cloneWithRows(this.props.data)}
        renderRow={this.renderRow}
        renderFooter={this.renderFooter}
        renderHeader={this.renderHeader}
      />
    )
  }
}

module.exports = VehicleListComponent
