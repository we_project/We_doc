import React, { Component } from 'react'
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Platform
} from 'react-native'
import CheckBox from '../../components/CheckBox'
import { getRem } from '../../styles/getRem'
import Localized from '../../utils/Localized'
import images from '../../images/index'


const strings = Localized.Strings
const styles = StyleSheet.create({
  root: {
    backgroundColor: '#ffffff',
    ...Platform.select({
      android: {
        paddingTop: getRem(2),
      },
    }),
  },
  searchContainer: {
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    flexDirection: 'row',
    paddingBottom: getRem(7),
    paddingTop: getRem(7),
    paddingRight: getRem(10),
    paddingLeft: getRem(10)
  },
  scan: {
    width: getRem(24),
    marginLeft: getRem(20),
    marginRight: getRem(20),
    marginTop: getRem(2),
  },
  scanIcon: {
    width: getRem(22),
    height: getRem(22),
  },
  wrapper: {
    flex: 1
  },
  inputRow: {
    flex: 1,
    height: getRem(28),
    paddingTop: getRem(5),
    paddingLeft: getRem(50),
    alignItems: 'flex-start',
    flexDirection: 'row',
    borderColor: '#ccc',
    borderRadius: getRem(4),
    backgroundColor: '#f4f4f4'
  },
  icon: {
    alignItems: 'center',
    marginRight: getRem(7),
    marginTop: getRem(2)
  },
  input: {
    flex: 1,
    paddingVertical: getRem(0),
    height: getRem(16),
    fontSize: getRem(12),
    color: '#999999',
    fontWeight: '300',
    fontFamily: 'HelveticaNeue'
  },
  filterContainer: {
    paddingHorizontal: getRem(5),
    justifyContent: 'space-around',
    flexDirection: 'row',
    flexWrap: 'wrap',
    borderTopWidth: getRem(1),
    borderBottomWidth: getRem(1),
    borderColor: '#f4f4f4',
  },
  rightVerticalLine: {
    borderRightWidth: getRem(1),
    borderColor: '#d8d8d8',
    marginTop: getRem(8),
    marginBottom: getRem(8),
  },
})

type Props = {
  navigation: any,
  onCountedChange: (Boolean) => any,
  onAssignedToMeChange: (Boolean) => any,
  onAllChange: () => any,
  countedChecked: Boolean,
  assignedToMeChecked: Boolean,
  allChecked: Boolean,
  onScanBthPress:() => any,
}

class SearchBox extends Component {
  props: Props
  toSearch() {
    this.props.navigation.navigate('SearchVehicle')
  }

  render() {
    return (
      <View style={styles.root}>
        <View style={styles.searchContainer} >
          <TouchableOpacity style={styles.scan} onPress={() => { this.props.onScanBthPress() }}>
            <Image
              source={images.scan}
              style={styles.scanIcon}
            />
          </TouchableOpacity>
          <TouchableWithoutFeedback
            style={styles.wrapper}
            onPress={() => { this.toSearch() }}
          >
            <View style={styles.inputRow}>
              <Image source={images.search} style={styles.icon} />
              <Text style={styles.input}>{strings.searchPlaceholder}</Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
        <View style={styles.filterContainer}>
          <CheckBox
            onChange={this.props.onCountedChange}
            checked={this.props.countedChecked}
          >
            {strings.countedFilter }
          </CheckBox>
          <View style={styles.rightVerticalLine} />
          <CheckBox
            onChange={this.props.onAssignedToMeChange}
            checked={this.props.assignedToMeChecked}
          >
            {strings.assignedToMeFilter}
          </CheckBox>
          <View style={styles.rightVerticalLine} />
          <CheckBox
            onChange={this.props.onAllChange}
            checked={this.props.allChecked}
          >
            {strings.allFilter}
          </CheckBox>
        </View>
      </View>)
  }
}


module.exports = SearchBox
