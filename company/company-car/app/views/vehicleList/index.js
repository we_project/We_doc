/**
 * @flow
 */
import React, { Component } from 'react'
import { StyleSheet, View, Image, Text } from 'react-native'

import CommonStyle from '../../styles/CommonStyle'
import Localized from '../../utils/Localized'
import AppNetworking from '../../utils/AppNetworking'
import VehicleListComponent from './VehicleListComponent'
import { getRem } from '../../styles/getRem'
import Reload from '../../components/Reload'
import images from '../../images'
import Logout from './Logout'

const strings = Localized.Strings
const commonStyle = CommonStyle.styles
const Networking = new AppNetworking()

const styles = StyleSheet.create({
  topBg: {
    width: CommonStyle.size.width,
    height: getRem(5),
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  loading: {
    alignSelf: 'center',
    width: getRem(20),
    height: getRem(20)
  }
})

type State = {
  vehicle: any,
  errString: any,
  isLoading: any,
  photoInfo: any,
  imageUri: any
}

type Props = {
  navigation: any,
}

class VehicleList extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: strings.titleCarList,
    headerLeft: <Text />,
    headerRight: <Logout navigation={navigation} routeName='Login' />,
  })

  constructor(props: Props) {
    super(props)
    this.state = {
      vehicle: [],
      errString: null,
      isLoading: false,
    }
  }

  state: State

  componentWillMount() {
    this.setState({
      isLoading: true
    })
    this.vehicleListRequest()
  }

  onRefresh = () => {
    this.vehicleListRequest()
  }

  onRefreshWithPromise =() => new Promise((resolve) => {
    this.onRefresh()
    resolve(true)
  })

  props: Props

  handleItemClick = (id: string) => {
    this.props.navigation.navigate('VehicleDetail', { id })
  }

  vehicleListRequest() {
    Networking.getVehicleList()
      .then((res) => {
        this.setState({
          vehicle: res,
          isLoading: false,
          errString: null
        })
      }, err => {
        this.setState({
          errString: err.message,
          isLoading: false
        })
      })
  }

  handleItemClick = (id: string) => {
    this.props.navigation.navigate('VehicleDetail', { id })
  }

  render() {
    return (
      <View style={[commonStyle.flex1]}>
        {this.state.isLoading === true ?
          <View style={styles.loadingContainer}>
            <Image source={images.loading} style={[commonStyle.mt20, styles.loading]} />
          </View>
          : this.state.errString != null ?
            <Reload onRefresh={this.onRefresh} text={this.state.errString} />
            : this.state.vehicle.length > 0 ?
              <VehicleListComponent
                onItemClick={this.handleItemClick}
                data={this.state.vehicle}
                onRefresh={this.onRefreshWithPromise}
              />
              : <Text style={[commonStyle.tc, commonStyle.mt20]}>{strings.noCarList}</Text> }
      </View>
    )
  }
}

module.exports = VehicleList
