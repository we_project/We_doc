/**
 * @flow
 */
import React, { Component } from 'react'
import { StyleSheet, View, Image, Text } from 'react-native'

import CommonStyle from '../../styles/CommonStyle'
import Localized from '../../utils/Localized'
import AppNetworking from '../../utils/AppNetworking'
import VehicleListComponent from './VehicleListComponent'
import { getRem } from '../../styles/getRem'
import Reload from '../../components/Reload'
import images from '../../images/index'
import Logout from './Logout'
import SearchBox from './SearchBox'
import Toast from '../../components/Toast'

const strings = Localized.Strings
const commonStyle = CommonStyle.styles
const Networking = new AppNetworking()
const initPageNumber = 1
const styles = StyleSheet.create({
  topBg: {
    width: CommonStyle.size.width,
    height: getRem(5),
  },
  loading: {
    alignSelf: 'center',
    width: getRem(20),
    height: getRem(20)
  },
  bgColor: {
    backgroundColor: '#f4f4f4'
  }
})

type State = {
  vehicle: any,
  errString: any,
  isLoading: any,
  photoInfo: any,
  imageUri: any,
  pageNumber: number,
  pageSize: number,
  dataCount:number
}

type Props = {
  navigation: any,
}

class AdminVehicleList extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: strings.titleCarList,
    headerLeft: <Text />,
    headerRight: <Logout navigation={navigation} routeName='AdminLogin' />,
  })

  constructor(props: Props) {
    super(props)
    this.state = {
      vehicle: [],
      errString: null,
      isLoading: false,
      pageSize: 8,
      pageNumber: 1,
      counted: global.stockDataService.vehicleFilter.counted,
      assignedToMe: global.stockDataService.vehicleFilter.assignedToMe,
      all: global.stockDataService.vehicleFilter.all,
    }
  }

  state: State

  componentWillMount() {
    this.loadVehicleList()
  }

  onRefreshWithPromise =() => new Promise((resolve) => {
    this.loadVehicleList(true, initPageNumber)
    resolve()
  })

  onEndReached = () => {
    if (this.hasMoreData()) {
      this.loadVehicleList(true, this.state.pageNumber + 1, this.state.vehicle)
    }
  }

  onScanBthPress = () => {
    this.refs.toast.show(strings.plateNumberComingSoon)
  }

  onRefresh = () => {
    this.loadVehicleList(false, initPageNumber)
  }

  props: Props

  handleItemClick = (id: string) => {
    this.props.navigation.navigate('VehicleDetail', { id })
  }

  hasMoreData = () => this.state.vehicle.length < this.dataCount

  loadVehicleList = (
    isPullDownFresh = false,
    pageNumber = initPageNumber,
    vehicle,
    counted = this.state.counted,
    assignedToMe = this.state.assignedToMe) => {
    if (!isPullDownFresh) {
      this.setState({ isLoading: true })
    }
    Networking.getAdminVehicleList(this.state.pageSize, pageNumber, counted, assignedToMe)
      .then((res) => {
        this.dataCount = res.totalRecordCount
        const newRes = vehicle ? vehicle.concat(res.data) : res.data
        this.setState({
          pageNumber,
          vehicle: newRes,
          isLoading: false,
          errString: null,
        })
      }, err => {
        this.setState({
          errString: err.message,
          isLoading: false
        })
      })
  }

  handleCountedChange = (counted) => {
    if (counted || this.state.assignedToMe) {
      this.setState({ all: false })
      global.stockDataService.changeVehicleFilter({ all: false })
    }
    this.setState({ counted }, () => {
      this.loadVehicleList()
      global.stockDataService.changeVehicleFilter({ counted })
    })
  }

  handleAssignedToMeChange = (assignedToMe) => {
    if (assignedToMe || this.state.counted) {
      this.setState({ all: false })
      global.stockDataService.changeVehicleFilter({ all: false })
    }
    this.setState({ assignedToMe }, () => {
      this.loadVehicleList()
      global.stockDataService.changeVehicleFilter({ assignedToMe })
    })
  }

  handleAllChange = (all) => {
    if (all) {
      this.setState({
        counted: false,
        assignedToMe: false,
      }, () => {
        this.loadVehicleList()
        global.stockDataService.changeVehicleFilter({
          counted: false,
          assignedToMe: false })
      })
    }
    this.setState({ all })
    global.stockDataService.changeVehicleFilter({ all })
  }

  render() {
    return (
      <View style={[commonStyle.flex1, styles.bgColor]}>
        <SearchBox
          onCountedChange={this.handleCountedChange}
          onAssignedToMeChange={this.handleAssignedToMeChange}
          onAllChange={this.handleAllChange}
          navigation={this.props.navigation}
          countedChecked={this.state.counted}
          assignedToMeChecked={this.state.assignedToMe}
          allChecked={this.state.all}
          onScanBthPress={this.onScanBthPress}
        />
        {this.state.isLoading === true ?
          <View style={styles.loadingContainer}>
            <Image source={images.loading} style={[commonStyle.mt20, styles.loading]} />
          </View> :
          this.state.errString != null ?
            <Reload onRefresh={this.onRefresh} text={this.state.errString} /> :
            this.state.vehicle.length > 0 ?
              <VehicleListComponent
                withHeader
                onItemClick={this.handleItemClick}
                data={this.state.vehicle}
                onEndReached={this.onEndReached}
                onRefresh={this.onRefreshWithPromise}
                needRenderFooter={this.hasMoreData()}
              /> : <Text style={[commonStyle.tc, commonStyle.mt20]}>{strings.noCarList}</Text> }
        <Toast style={styles.toast} ref='toast' />
      </View>
    )
  }
}

module.exports = AdminVehicleList
