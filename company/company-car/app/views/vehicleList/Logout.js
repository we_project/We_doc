/**
 * @flow
 */
import React from 'react'
import { Image, TouchableOpacity, Alert } from 'react-native'
import CookieManager from 'react-native-cookies'
import { NavigationActions } from 'react-navigation'

import Localized from '../../utils/Localized'
import { getRem } from '../../styles/getRem'
import images from '../../images'
import UserUtil from '../../utils/User'

const strings = Localized.Strings
const User = new UserUtil()

type Props = {
  navigation: any,
  routeName: String,
}

const logout = (navigation, routeName) => {
  Alert.alert(
    '',
    strings.confirmToLogOut,
    [
      { text: strings.cancel, onPress: () => {} },
      {
        text: strings.confirm,
        onPress: () => {
          User.deleteUser()
          if (routeName === 'Login') {
            CookieManager.clearAll()
              .then(() => {
                console.log('cookies cleared!')
              }).catch((err) => {
                console.log(err)
              })
          }
          navigation.dispatch(
            NavigationActions.reset({
              index: 0,
              actions: [
                NavigationActions.navigate({ routeName: 'RoleSelector' })
              ]
            }))
        },
      },
    ],
  )
}

export default function Logout(props: Props) {
  const { navigation, routeName } = props
  return (
    <TouchableOpacity onPress={() => logout(navigation, routeName)}>
      <Image
        source={images.logout}
        style={{ width: getRem(18), height: getRem(18), marginRight: getRem(10) }}
      />
    </TouchableOpacity>
  )
}
