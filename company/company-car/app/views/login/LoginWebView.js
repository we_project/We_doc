/**
 * @flow
 */
import React, { Component } from 'react'
import { NavigationActions } from 'react-navigation'
import {
  StyleSheet,
  View,
  WebView,
} from 'react-native'

import UserUtil from '../../utils/User'
import { getRem, width } from '../../styles/getRem'
import CookieManager from 'react-native-cookies'
import Localized from '../../utils/Localized'
import Reload from '../../components/Reload'

const User = new UserUtil()
const strings = Localized.Strings

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  btnLogin: {
    borderRadius: getRem(12),
    height: getRem(50),
    width: width * 0.5,
    backgroundColor: '#232142',
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: 'black',
    shadowOffset: { width: getRem(2), height: getRem(2) },
    shadowOpacity: 0.5
  },
  btnTxt: {
    color: '#fff'
  },
  webView: {
    borderRadius: getRem(20)
  }
})

type State = {
  showLogin: Boolean,
  loginUri: string,
  scalesPageToFit: any,
}

type Props = {
  navigation: any,
}
/* eslint-disable no-console */
class LoginWebView extends Component {
  static navigationOptions = {
    title: strings.titleLogin,
    header: null
  }

  constructor(props: Props) {
    super(props)
    this.state = {
      showLogin: true,
      loginUri: 'https://anywhere.i.daimler.com/any/anywhere/islogin'
    }
  }

  state: State

  componentWillMount() {
    User.getUser().then((user) => {
      const token = user.Authorization
      if (token && token.length > 0) {
        this.toListView()
      }
    }, err => {
      console.log(err)
    })
  }
  onNavigationStateChange = (navState: any) => {
    const url = navState.url
    if (url.search('token') >= 0) {
      this.setState({
        showLogin: false
      })
      const fromIndex = url.indexOf('stoken=') + 7
      const token = url.substring(fromIndex)
      this.getUserInfo(url, token)
    }
  }

  onError = () => {
    this.setState({
      showErr: true,
      showLogin: true
    })
  }


  onRefresh = () => {
    CookieManager.clearAll()
      .then(() => {
        console.log('cookies cleared!')
      }).catch((err) => {
        console.log(err)
      })
    User.deleteUser()
    this.setState({ showErr: false, showLogin: true })
  }

  getUserInfo = (url, token) => {
    global.fetch(url).then((response) => {
      response.json().then((resp) => {
        if (resp.status === 'success') {
          const userName = resp.userinfo.UName
          this.setUserTokenAndLogin(token, userName)
        } else {
          console.log('get userName error')
        }
      })
    }).catch((error) => {
      console.log(error)
    })
  }


  setUserTokenAndLogin = (token: String, userName: String) => {
    User.setUser({ Authorization: token, userName }).then(resUser => {
      this.toListView()
    }, (err) => {
      console.log(err)
    })
  }

  toListView = () => {
    this.props.navigation.dispatch(
      NavigationActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({ routeName: 'VehicleList' })
        ]
      }))
  }

  props: Props
  login = null

  render() {
    return (
      <View style={styles.container}>
        { this.state.showErr ?
          <Reload onRefresh={this.onRefresh} />
          :
          this.state.showLogin ? <WebView
            ref={ref => { this.login = ref }}
            automaticallyAdjustContentInsets={false}
            style={styles.webView}
            source={{ uri: this.state.loginUri }}
            javaScriptEnabled
            domStorageEnabled
            decelerationRate='normal'
            onNavigationStateChange={this.onNavigationStateChange}
            startInLoadingState
            scalesPageToFit={this.state.scalesPageToFit}
            onError={this.onError}
          /> : null }
      </View>
    )
  }
}

module.exports = LoginWebView
