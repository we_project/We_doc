/**
 * @flow
 */
import React, { Component } from 'react'
import { StyleSheet, View, Text } from 'react-native'
import CommonStyle from '../../styles/CommonStyle'
import Localized from '../../utils/Localized'
import { getRem } from '../../styles/getRem'

const strings = Localized.Strings
const commonStyle = CommonStyle.styles

const styles = StyleSheet.create({
  topBg: {
    width: CommonStyle.size.width,
    height: getRem(5),
  }
})

type Props = {
  navigation: any,
}

class LoginError extends Component {
  static navigationOptions = {
    title: strings.titleLoginError,
  }

  constructor(props: Props) {
    super(props)
    this.state = {
    }
  }

  state: State
  props: Props

  render() {
    return (
      <View style={[commonStyle.flex1]}>
        <Text>{this.props.navigation.state.params.msg}</Text>
      </View>
    )
  }
}

module.exports = LoginError
