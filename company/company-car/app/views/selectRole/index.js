/**
 * @flow
 */
import React, { Component } from 'react'
import {
  TouchableWithoutFeedback,
  Keyboard,
  StyleSheet,
  View,
  Dimensions,
} from 'react-native'
import { NavigationActions } from 'react-navigation'
import { getRem } from '../../styles/getRem'
import Localized from '../../utils/Localized'
import PanelButton from '../../components/PanelButton'
import UserUtil from '../../utils/User'

const strings = Localized.Strings
const User = new UserUtil()
const { height, width } = Dimensions.get('window')
const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: 'rgba(56,65,82,0.80)',
    paddingLeft: getRem(15),
    paddingRight: getRem(15)
  },
  buttonWrapper: {
    marginTop: getRem(400)
  },
  button: {
    padding: 0,
    height: getRem(48),
    width: getRem(345),
  }
})

type
State = {
  name: string,
  password: string,
  showLogin: Boolean
}

type
Props = {
  navigation: any,
}

class RoleSelector extends Component {
  static navigationOptions = () => ({
    title: strings.titleLogin,
    header: null
  })

  constructor(props: Props) {
    super(props)
    this.state = {
      tabIndex: 0,
      showLogin: false
    }
  }

  componentWillMount() {
    User.getUser().then((user) => {
      const token = user.Authorization
      if (token && token.length > 0) {
        this.setState({ showLogin: false })
        if (user.isAdmin) {
          this.toAdminView()
        } else {
          this.toCarUserView()
        }
      }
      this.setState({ showLogin: true })
    }, err => {
      this.setState({ showLogin: true })
      console.log(err)
    })
  }

  toAdminView = () => {
    this.directTo('AdminVehicleList')
  }

  toAdminLoginView = () => {
    this.directTo('AdminLogin')
  }

  toCarUserLoginView = () => {
    this.directTo('Login')
  }

  toCarUserView = () => {
    this.directTo('VehicleList')
  }

  directTo = (routeName) => {
    this.props.navigation.dispatch(
      NavigationActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({ routeName })
        ]
      }))
  }


  props: Props

  render() {
    const { showLogin } = this.state
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        {showLogin ?
          <View style={styles.buttonWrapper}>
            <PanelButton
              style={styles.button}
              rootStyle={styles.buttonRoot}
              text={strings.selectAdmin}
              onPress={() => this.toAdminLoginView()}
            />
            <PanelButton
              style={styles.button}
              rootStyle={styles.buttonRoot}
              text={strings.selectCarUser}
              onPress={() => this.toCarUserLoginView()}
            />
          </View> : <View />}
      </TouchableWithoutFeedback>
    )
  }
}

module.exports = RoleSelector
