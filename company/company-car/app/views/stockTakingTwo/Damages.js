import React, { Component } from 'react'
import { View, Text, TouchableWithoutFeedback } from 'react-native'
import { findIndex, fill, isEmpty, remove, filter } from 'lodash'

import Localized from '../../utils/Localized'
import Radio from '../../components/Radio'
import Card from '../../components/Card'
import images from '../../images'
import DamagesPhotoSquare from './DamagesPhotoSquare'
import styles from './DamagesSheetStyle'
import { YES_NO } from '../../utils/PlateNumber'
import { DAMAGES_TEXT } from './DamagesConstants'

const strings = Localized.Strings

type Props = {
  navigation: any,
  onDamagePhotoChange: () => void,
  onDamageNumberChange: () => void,
  onCarIsMobileAndInUse: () => void,
  value: any,
  carIsMobileAndInUse: any,
}

class Damages extends Component {
  constructor(props: Props) {
    super(props)
    this.state = {
      activeGrade: props.value.damageNumber,
      carIsMobileAndInUse: props.carIsMobileAndInUse,
      photoArray: props.value.damageImages,
    }
  }

  componentWillMount() {
    if (this.props.value.damageNumber !== null) {
      this.handleActiveGrade({
        number: this.props.value.damageNumber,
        level: this.props.value.damageLevel,
      })
      this.handleAddPhoto()
    }
  }

  props: Props

  handleDamagePhotoChange = (photos) => {
    this.props.onDamagePhotoChange(filter(photos, photoItem => !isEmpty(photoItem)))
  }

  handleActiveGrade = (v) => {
    let photoArray = this.state.photoArray
    if (this.state.activeGrade !== v.number) {
      photoArray = this.state.photoArray.slice(0, v.number)
      if (photoArray.length < v.number) {
        photoArray = fill(new Array(v.number), {}, 0, v.number)
          .map((item, index) => photoArray[index] || {})
      }
      this.setState({ activeGrade: v.number, photoArray }, () => {
        this.props.onDamageNumberChange(v)
        this.handleDamagePhotoChange(photoArray)
      })
    }
  }

  handleChangePhotoArray = (data, index) => {
    const photos = this.state.photoArray
    photos[index] = Object.assign({}, photos[index], data)
    this.setState({ photoArray: photos }, () => {
      this.handleAddPhoto()
      this.handleDamagePhotoChange(photos)
    })
  }

  handleAddPhoto = () => {
    if (this.state.activeGrade === 3) {
      const nullIndex = findIndex(this.state.photoArray, (v) => isEmpty(v))
      if (nullIndex < 0) {
        const newPhotoArray = this.state.photoArray
        if (newPhotoArray.length < 6) {
          newPhotoArray.push({})
        }
        this.setState({ photoArray: newPhotoArray })
      }
    }
  }

  handleDeletePhoto = (index) => {
    const photos = this.state.photoArray
    if (this.state.activeGrade === 3) {
      remove(photos, (v, itemIndex) => index === itemIndex)
    } else {
      photos[index] = {}
    }
    this.setState({ photoArray: photos }, () => {
      this.handleAddPhoto()
      this.handleDamagePhotoChange(photos)
    })
  }

  renderDamagesNumber = () => {
    const { activeGrade } = this.state
    return (
      <View style={styles.damageGradeContainer}>
        {DAMAGES_TEXT.map((v) => (
          <TouchableWithoutFeedback
            key={v.number}
            onPress={() => { this.handleActiveGrade(v) }}
          >
            <View style={styles.damageItem}>
              <Text
                style={[styles.gradeText, activeGrade === v.number ? styles.activeGradeText : null]}
              >
                {v.text}</Text>
              <View style={[styles.gradeNumber,
                activeGrade === v.number ? styles.activeGradeNumber : null]}
              >
                <Text style={[styles.gradeNumberText,
                  activeGrade === v.number ? styles.activeGradeNumberText : null]}
                >
                  {v.numberText}</Text>
              </View>
            </View>
          </TouchableWithoutFeedback>
        ))}
      </View>)
  }

  renderNumberTip = () => {
    switch (this.state.activeGrade) {
      case 1:
        return `${strings.PleaseAttach}1${strings.photosOfDamageParts}`
      case 2:
        return `${strings.PleaseAttach}2${strings.photosOfDamageParts}`
      case 3:
        return `${strings.PleaseAttach}3-6${strings.photosOfDamageParts}`
      default:
        return ''
    }
  }

  render() {
    const { photoArray } = this.state
    return (
      <View style={styles.root}>
        <Card
          title={strings.damages}
          icon={images.damages}
          subTitle={strings.pleaseChooseHowManyPanelsToRepairBelow}
        >
          {this.renderDamagesNumber()}
          {!this.state.activeGrade ?
            null : <View style={styles.damagesContainer}>
              <DamagesPhotoSquare
                photoArray={photoArray}
                navigation={this.props.navigation}
                onChange={this.handleChangePhotoArray}
                handleDeletePhoto={this.handleDeletePhoto}
              />
              <Text style={styles.maxPhotoTips}>{this.renderNumberTip()}</Text>
            </View>
          }
          <Radio
            noBorder
            showText={strings.carIsMobileAndInUse}
            value={this.state.carIsMobileAndInUse}
            data={YES_NO}
            onChange={(carIsMobileAndInUse) => {
              this.setState({ carIsMobileAndInUse })
              this.props.onCarIsMobileAndInUse(carIsMobileAndInUse)
            }}
          />
        </Card>
      </View>
    )
  }
}

export default Damages
