import Localized from '../../utils/Localized'

const strings = Localized.Strings

const POINT_POSITION = [{
  left: 169,
  top: 22,
  site: 'FRONT',
}, {
  left: 93,
  top: 75,
  site: 'FRONT_LEFT'
}, {
  left: 96,
  top: 180,
  site: 'LEFT_FRONT_DOOR',
}, {
  left: 100,
  top: 260,
  site: 'LEFT_BACK_DOOR',
}, {
  left: 105,
  top: 360,
  site: 'BACK_LEFT',
}, {
  left: 169,
  top: 376,
  site: 'BACK',
}, {
  left: 235,
  top: 360,
  site: 'BACK_RIGHT',
}, {
  left: 241,
  top: 260,
  site: 'RIGHT_FRONT_DOOR',
}, {
  left: 244,
  top: 180,
  site: 'RIGHT_BACK_DOOR',
}, {
  left: 245,
  top: 75,
  site: 'FRONT_RIGHT',
}]

const DAMAGES_TEXT = [
  {
    text: strings.perfect,
    number: 0,
    numberText: '0',
    level: 'PERFECT',
  },
  {
    text: strings.minor,
    number: 1,
    numberText: '1',
    level: 'MINOR',
  },
  {
    text: strings.medium,
    number: 2,
    numberText: '2',
    level: 'MEDIUM',
  },
  {
    text: strings.major,
    number: 3,
    numberText: '2+',
    level: 'MAJOR',
  }
]

export {
  POINT_POSITION,
  DAMAGES_TEXT,
}
