import { StyleSheet } from 'react-native'

import { getRem } from '../../styles/getRem'

const styles = StyleSheet.create({
  headerTitle: {
    fontSize: getRem(18),
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#000000'
  },
  headerSmallTitle: {
    fontSize: getRem(12),
    fontWeight: '300',
    lineHeight: getRem(16),
    color: '#9b9b9b',
    textAlign: 'center',
  },
  root: {
    flex: 1,
  },
  mileageView: {
    height: getRem(46),
    borderBottomWidth: getRem(1),
    borderBottomColor: 'rgba(51,51,51,0.1)',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  mileageTextContainer: {
    borderRightWidth: getRem(1),
    borderRightColor: '#eaebed',
    marginRight: getRem(20),
  },
  mileageText: {
    width: getRem(125),
    alignItems: 'center',
    color: '#333333',
  },
  mileageInput: {
    flex: 1,
    fontSize: getRem(14),
    fontWeight: '300',
    color: '#333333',
  },
  odometerPhotoTitle: {
    textAlign: 'left',
    marginTop: getRem(20),
    marginBottom: getRem(6),
    color: '#333333',
  },
  photoView: {
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
})

export default styles
