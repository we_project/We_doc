import { StyleSheet } from 'react-native'

import { getRem } from '../../styles/getRem'

const styles = StyleSheet.create({
  root: {
    flex: 1,
  },
  damageGradeContainer: {
    height: getRem(77),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    borderBottomWidth: getRem(1),
    borderBottomColor: 'rgba(216, 216, 216, 0.7)'
  },
  damageItem: {
    alignItems: 'center',
    flex: 1,
  },
  gradeText: {
    textAlign: 'center',
    fontSize: getRem(12),
    fontWeight: '300',
    color: '#373737',
    opacity: 0.36,
    marginBottom: getRem(8),
  },
  activeGradeText: {
    fontWeight: '500',
    opacity: 1,
  },
  gradeNumber: {
    width: getRem(26),
    height: getRem(26),
    borderRadius: getRem(13),
    borderWidth: 1,
    borderColor: '#d8d8d8',
    justifyContent: 'center',
  },
  activeGradeNumber: {
    backgroundColor: '#1f88d8',
    borderColor: '#1f88d8',
  },
  gradeNumberText: {
    fontSize: getRem(14),
    fontWeight: '500',
    color: '#9b9b9b',
    textAlign: 'center',
    backgroundColor: 'rgba(0,0,0,0)',
  },
  activeGradeNumberText: {
    color: '#ffffff',
  },
  damageIndication: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.7)',
  },
  damageIndicationHeader: {
    // flexDirection: 'row',
    borderBottomWidth: getRem(1),
    borderBottomColor: '#e7e7e7',
    // alignItems: 'center',
    height: getRem(51),
    position: 'relative',
  },
  damageIndicationTitle: {
    fontSize: 16,
    fontWeight: '500',
    textAlign: 'center',
    color: '#333333',
    flex: 1,
    lineHeight: getRem(51),
  },
  damageIndicationImage: {
    position: 'absolute',
    right: 0,
    top: 0,
    paddingHorizontal: getRem(16),
    paddingTop: getRem(18),
    height: getRem(51),
  },
  damageIndicationContainer: {
    width: '100%',
    height: getRem(520),
    backgroundColor: '#ffffff',
    position: 'absolute',
    bottom: 0,
  },
  tapTip: {
    fontSize: getRem(12),
    fontWeight: '300',
    lineHeight: getRem(16),
    color: '#999999',
    marginTop: getRem(12),
    textAlign: 'center',
  },
  imageContainer: {
    height: getRem(439),
    position: 'relative',
  },
  carTopView: {
    position: 'absolute',
    height: getRem(360),
    width: getRem(189),
    left: getRem(93),
    top: getRem(39),
  },
  bluePoint: {
    position: 'absolute',
  },
  photoContainer: {
    flexDirection: 'row',
    width: getRem(343),
    flexWrap: 'wrap',
    paddingLeft: getRem(13),
    paddingTop: getRem(10),
  },
  maxPhotoTips: {
    fontSize: getRem(10),
    fontWeight: '300',
    lineHeight: getRem(16),
    color: '#9b9b9b',
    marginBottom: getRem(10),
  },
  damagesContainer: {
    borderBottomWidth: getRem(1),
    borderBottomColor: '#e7e7e7',
  }
})

export default styles
