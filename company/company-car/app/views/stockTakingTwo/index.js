import React from 'react'
import { View, Text, ScrollView, TextInput } from 'react-native'

import PanelButton from '../../components/PanelButton'
import { getRem } from '../../styles/getRem'
import Localized from '../../utils/Localized'
import { YES_NO } from '../../utils/PlateNumber'
import styles from './StockStyleSheetTwo'
import Toast from '../../components/Toast'
import images from '../../images'
import Card from '../../components/Card'
import Radio from '../../components/Radio'
import PhotoSquare from '../../components/PhotoSquare/index'
import checkStockData from '../../utils/CheckStockData'
import Damages from './Damages'

const strings = Localized.Strings

type Props = {
  navigation: object,
}

class StockTakingTwo extends React.Component {
  static navigationOptions = () => ({
    headerStyle: {
      height: getRem(70),
      backgroundColor: '#ffffff',
    },
    headerTitle: (
      <View style={{ alignSelf: 'center' }}>
        <Text style={styles.headerTitle}>{strings.stockTaking}</Text>
        <Text style={styles.headerSmallTitle}>{strings.stockTakingSmallTitleTwo}</Text>
      </View>
    ),
  })

  constructor() {
    super()
    const {
      warningTriangleInCar,
      fireExtinguisher,
      floorMat,
      mileage,
      odometerImage,
      damages,
      carIsMobileAndInUse,
    } = global.stockDataService.getStockData
    this.state = {
      warningTriangleInCar,
      fireExtinguisher,
      floorMat,
      mileage,
      odometerImage,
      damages: {
        damageNumber: damages.damageNumber,
        damageLevel: damages.damageLevel,
        damageImages: damages.damageImages,
      },
      carIsMobileAndInUse,
    }
  }

  props: Props

  handleDamagePhotoChange = (damageImages) => {
    const damages = this.state.damages
    damages.damageImages = damageImages
    this.setState({ damages })
  }

  handleDamageNumberChange = (v) => {
    const damages = this.state.damages
    damages.damageLevel = v.level
    damages.damageNumber = v.number
    this.setState({ damages })
  }

  checkDamage = () => {
    if (this.state.damages) {
      const { damageImages, damageNumber } = this.state.damages
      if (damageNumber !== null && damageNumber < 3) {
        return damageImages.length === damageNumber
      }
      return damageImages.length > 2
    }
    return false
  }

  render() {
    const {
      warningTriangleInCar,
      fireExtinguisher,
      floorMat,
      mileage,
      odometerImage,
      carIsMobileAndInUse,
      damages,
    } = this.state
    global.stockDataService.addStockData({
      warningTriangleInCar,
      fireExtinguisher,
      floorMat,
      mileage,
      odometerImage,
      carIsMobileAndInUse,
      damages,
    })
    let isEnable = checkStockData(global.stockDataService.getStockData, [
      'warningTriangleInCar',
      'fireExtinguisher',
      'floorMat',
      'mileage',
      'odometerImage',
      'carIsMobileAndInUse',
    ])
    isEnable = isEnable && this.checkDamage()
    return (
      <View style={styles.root}>
        <ScrollView>
          <Card title={strings.vehicleSpecificInformation} icon={images.catalogue}>
            <Radio
              showText={strings.warningTriangleInCar}
              value={warningTriangleInCar}
              data={YES_NO}
              onChange={(v) => { this.setState({ warningTriangleInCar: v }) }}
            />
            <Radio
              showText={strings.fireExtinguisher}
              value={this.state.fireExtinguisher}
              data={YES_NO}
              onChange={(v) => { this.setState({ fireExtinguisher: v }) }}
            />
            <Radio
              showText={strings.floorMat}
              value={this.state.floorMat}
              data={YES_NO}
              noBorder
              onChange={(v) => { this.setState({ floorMat: v }) }}
            />
          </Card>
          <Card title={strings.mileage} icon={images.mileage}>
            <View style={styles.mileageView}>
              <View style={styles.mileageTextContainer}>
                <Text style={styles.mileageText}>{strings.mileageNumber}</Text>
              </View>
              <TextInput
                value={this.state.mileage}
                placeholder={strings.mileageRequired}
                maxLength={6}
                style={styles.mileageInput}
                onChangeText={(v) => { this.setState({ mileage: v }) }}
                keyboardType='numeric'
                multiline={false}
                returnKeyType='done'
                underlineColorAndroid={'rgba(0,0,0,0)'}
              />
            </View>
            <Text style={styles.odometerPhotoTitle}>{strings.odometerPhoto}</Text>
            <View style={styles.photoView}>
              <PhotoSquare
                value={odometerImage}
                navigation={this.props.navigation}
                onCancel={() => { this.setState({ odometerImage: null }) }}
                onChange={odometer => this.setState({
                  odometerImage: Object.assign({}, this.state.odometerImage, odometer) })}
              />
            </View>
          </Card>
          <Damages
            value={damages}
            carIsMobileAndInUse={carIsMobileAndInUse}
            navigation={this.props.navigation}
            onDamagePhotoChange={this.handleDamagePhotoChange}
            onDamageNumberChange={this.handleDamageNumberChange}
            onCarIsMobileAndInUse={(v) => {
              this.setState({ carIsMobileAndInUse: v })
            }}
          />
          <PanelButton
            status='enabled'
            text={strings.next}
            disabled={!isEnable}
            onPress={isEnable ?
              () => { this.props.navigation.navigate('StockTakingConfirm') } : null}
          />
          <Toast ref='toast' />
        </ScrollView>
      </View>
    )
  }
}

export default StockTakingTwo
