import React, { Component } from 'react'
import Resizer from 'react-native-image-resizer'
import { View, Image, TouchableWithoutFeedback, TouchableOpacity, Text, Modal } from 'react-native'
import { uniqueId } from 'lodash'

import AppNetworking from '../../utils/AppNetworking'
import { getRem } from '../../styles/getRem'
import images from '../../images'
import { normalStyles as photoStyles } from '../../components/PhotoSquare/PhotoSquareSheetStyle'
import styles from './DamagesSheetStyle'
import Localized from '../../utils/Localized'
import { POINT_POSITION } from './DamagesConstants'

const Networking = new AppNetworking()
const strings = Localized.Strings

type Props = {
  navigation: any,
  onChange: () => void,
  photoArray: Array,
  handleDeletePhoto: () => any,
}

class DamagesPhotoSquare extends Component {
  constructor() {
    super()
    this.state = {
      photo: null,
      modalVisible: false,
      currentPhotoIndex: null,
      currentPhotoSite: null,
    }
  }

  props: Props

  photoInfo = (data) => {
    Resizer.createResizedImage(data.path, 2000, 2000, 'JPEG', 80)
      .then(resizedImageUri => {
        this.props.onChange({ uri: resizedImageUri,
          site: this.state.currentPhotoSite }, this.state.currentPhotoIndex)
        this.uploadImage(resizedImageUri)
      }).catch((err) => {
        console.log(err)
      })
  }

  uploadImage = imageUri => {
    Networking.uploadImage(imageUri).then(resp => {
      const imageId = resp[0]
      this.props.onChange({ id: imageId.id }, this.state.currentPhotoIndex)
    }, (err) => {
      console.log(err)
    })
  }

  handleCancel = (index) => {
    this.props.handleDeletePhoto(index)
  }

  handlePhoto = (index) => {
    this.setState({ modalVisible: true, currentPhotoIndex: index })
  }

  handleTakePhoto = (v) => {
    this.setState({ modalVisible: false, currentPhotoSite: v.site })
    this.props.navigation.navigate('Camera', { photoInfo: this.photoInfo })
  }

  renderPhoto = (v, index) => {
    const photoUri = this.props.photoArray[index].uri || null
    const photo = photoUri && { uri: photoUri }
    const onTakePhoto = !photo ? () => { this.handlePhoto(index) } : null
    return (
      <View style={[photoStyles.container]} key={uniqueId()}>
        <TouchableWithoutFeedback onPress={onTakePhoto}>
          <View style={[photoStyles.imgContainer, photoStyles.imgContainerBorder]}>
            <Image
              source={photo || images.cameraIcon}
              style={photo && photoStyles.img}
            />
          </View>
        </TouchableWithoutFeedback>
        {photo ?
          <TouchableOpacity
            onPress={() => { this.handleCancel(index) }}
            style={photoStyles.cancel}
          >
            <Image
              style={photoStyles.imgCancel}
              source={images.close}
            />
          </TouchableOpacity> : null}
      </View>
    )
  }

  render() {
    const { photoArray } = this.props
    return (
      <View>
        { photoArray.length > 0 && <View style={styles.photoContainer}>
          {photoArray.map((v, index) => this.renderPhoto(v, index))}
        </View> }
        <Modal
          animationType={'slide'}
          transparent
          visible={this.state.modalVisible}
          onRequestClose={() => { this.setState({ modalVisible: false }) }}
        >
          <View style={styles.damageIndication}>
            <View style={styles.damageIndicationContainer}>
              <View style={styles.damageIndicationHeader}>
                <Text style={styles.damageIndicationTitle}>{strings.damageIndication}</Text>
                <TouchableOpacity
                  style={styles.damageIndicationImage}
                  onPress={() => { this.setState({ modalVisible: false }) }}
                >
                  <Image source={images.cancel} />
                </TouchableOpacity>
              </View>
              <View>
                <Text style={styles.tapTip}>{strings.tapToIndicateWhereYourVehicleIsDamaged}</Text>
                <View style={styles.imageContainer}>
                  <Image style={styles.carTopView} source={images.carTopView} />
                  {POINT_POSITION.map((v) => (
                    <TouchableOpacity
                      style={[styles.bluePoint, { left: getRem(v.left), top: getRem(v.top) }]}
                      key={uniqueId()}
                      onPress={() => { this.handleTakePhoto(v) }}
                    >
                      <Image
                        style={{ width: getRem(36), height: getRem(36) }}
                        source={images.bluePoint}
                      />
                    </TouchableOpacity>
                  ))}
                </View>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    )
  }
}

export default DamagesPhotoSquare
