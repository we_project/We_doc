/**
 * @flow
 */
import React, { Component } from 'react'
import {
  TouchableWithoutFeedback,
  Keyboard,
  StyleSheet,
  View,
  Image,
  Text,
  TextInput,
  Dimensions,
} from 'react-native'
import { NavigationActions } from 'react-navigation'
import { getRem } from '../../styles/getRem'
import Localized from '../../utils/Localized'
import AppNetworking from '../../utils/AppNetworking'
import PanelButton from '../../components/PanelButton'
import Toast from '../../components/Toast'
import images from '../../images/index'
import UserUtil from '../../utils/User'

const strings = Localized.Strings
const User = new UserUtil()
const Networking = new AppNetworking()
const { height, width } = Dimensions.get('window')
const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: 'rgba(56,65,82,0.80)',
    paddingLeft: getRem(15),
    paddingRight: getRem(15)
  },
  inputSection: {
    marginTop: getRem(158)
  },
  backgroundImage: {
    width,
    height
  },
  inputRow: {
    flexWrap: 'wrap',
    height: getRem(43),
    marginTop: getRem(10),
    alignItems: 'flex-start',
    flexDirection: 'row',
    borderColor: '#ccc',
    borderRadius: getRem(4),
    backgroundColor: 'rgba(216, 216, 216, 0.2)'
  },
  icon: {
    height: getRem(38),
    width: getRem(50),
    alignItems: 'center',
    paddingTop: getRem(12)
  },
  input: {
    borderLeftWidth: getRem(1),
    paddingLeft: getRem(15),
    paddingBottom: 0,
    paddingTop: 0,
    marginTop: getRem(10),
    height: getRem(24),
    width: getRem(294),
    borderColor: '#ccc',
    color: '#f4f4f4',
    fontSize: getRem(14),
  },
  tip: {
    marginTop: getRem(15),
    paddingLeft: getRem(2),
    paddingRight: getRem(2)
  },
  text: {
    fontSize: getRem(12),
    fontFamily: 'HelveticaNeue',
    color: '#d8d8d8'
  },
  submitSection: {
    marginTop: getRem(35)
  },
  button: {
    padding: 0,
    height: getRem(48),
    width: getRem(345),
  },
  buttonRoot: {
    width: getRem(345),
    margin: getRem(0),
  },
  toast: {
    position: 'absolute',
    left: (width - getRem(220)) / 2,
    top: (height - getRem(130)) / 2,
    width: getRem(220)
  }
})

type State = {
  name: string,
  password: string,
  showLogin: Boolean
}

type Props = {
  navigation: any,
}

class AdminLogin extends Component {
 static navigationOptions = () => ({
   title: strings.titleLogin,
   header: null
 })

 constructor(props: Props) {
   super(props)
   this.state = {
     tabIndex: 0,
     showLogin: false
   }
 }

 state: State

 componentWillMount() {
   User.getUser().then((user) => {
     const token = user.Authorization
     if (token && token.length > 0) {
       this.setState({ showLogin: false })
       this.toListView(token)
     }
     this.setState({ showLogin: true })
   }, err => {
     this.setState({ showLogin: true })
     console.log(err)
   })
 }

 setUserTokenAndLogin = (token: String, userName: String) => {
   User.setUser({ Authorization: token, isAdmin: true, userName }).then(resUser => {
     this.toListView()
   }, (err) => {
     console.log(err)
   })
 }

 toListView = () => {
   this.props.navigation.dispatch(
     NavigationActions.reset({
       index: 0,
       actions: [
         NavigationActions.navigate({ routeName: 'AdminVehicleList' })
       ]
     }))
 }

 handleLogin(name, password) {
   if (!name || !password) {
     this.refs.toast.show(strings.failedForMissInput)
     return
   }
   Networking.adminLogin({ loginId: name, password })
     .then((res) => {
       this.setUserTokenAndLogin(`JWT ${res.token}`, res.fullName)
     }, err => {
       if (err.toString().includes('Network')) {
         this.refs.toast.show(strings.failedForNetworkIssue)
       } else {
         this.refs.toast.show(strings.failedForIncorrectInput)
       }
     })
 }


  props: Props

  render() {
    const { name, password, showLogin } = this.state
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        {showLogin ?
          <Image style={styles.backgroundImage} source={images.adminLogin}>
            <View style={styles.root}>
              <View style={styles.inputSection}>
                <View style={styles.inputRow}>
                  <View style={styles.icon}><Image source={images.profile} /></View>
                  <TextInput
                    placeholderTextColor='#999999'
                    style={styles.input}
                    placeholder={strings.adminNameRequired}
                    value={name}
                    selectionColor={'white'}
                    onChangeText={(name) => this.setState({ name })}
                    underlineColorAndroid={'rgba(0,0,0,0)'}
                  />
                </View>
                <View style={styles.inputRow}>
                  <View style={styles.icon}><Image source={images.unlock} /></View>
                  <TextInput
                    secureTextEntry
                    placeholderTextColor='#999999'
                    style={styles.input}
                    placeholder={strings.adminPasswordRequired}
                    value={password}
                    selectionColor={'white'}
                    onChangeText={(password) => this.setState({ password })}
                    underlineColorAndroid={'rgba(0,0,0,0)'}
                  />
                </View>
                <View style={styles.tip}>
                  <Text style={styles.text}>* {strings.adminLoginTip}</Text>
                </View>
              </View>

              <View style={styles.submitSection}>
                <PanelButton
                  style={styles.button}
                  rootStyle={styles.buttonRoot}
                  text={strings.takeLogin}
                  onPress={() => this.handleLogin(name, password)}
                />
              </View>
              <Toast style={styles.toast} ref='toast' />
            </View>
          </Image> : <View />}
      </TouchableWithoutFeedback>
    )
  }
}

module.exports = AdminLogin
