import { StyleSheet } from 'react-native'

import { getRem, height, width } from '../../styles/getRem'

const styles = StyleSheet.create({
  root: {
    flex: 1,
  },
  vehicleInfoItem: {
    height: getRem(52),
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    borderBottomWidth: getRem(1),
    borderBottomColor: 'rgba(33, 33, 33, 0.1)',
    marginLeft: getRem(40),
    paddingRight: getRem(20),
  },
  vehicleInfoLeft: {
    flex: 1,
    fontSize: getRem(14),
    color: '#333333',
  },
  vehicleInfoRight: {
    fontSize: getRem(14),
    fontWeight: '300',
    lineHeight: getRem(16),
    textAlign: 'right',
    color: '#666',
    marginLeft: getRem(10),
  },
  signatureContainer: {
    marginTop: getRem(12),
  },
  signatureHeaderContainer: {
    paddingLeft: getRem(12),
    paddingTop: getRem(20),
    paddingBottom: getRem(13),
    paddingRight: getRem(20),
    borderBottomWidth: getRem(1),
    borderBottomColor: '#e7e7e7',
    backgroundColor: '#ffffff',
  },
  signatureHeader: {
    alignItems: 'flex-start',
    flexDirection: 'row',
  },
  signatureLeftIcon: {
    width: getRem(45),
    alignItems: 'center',
  },
  signatureTitle: {
    fontSize: getRem(16),
    fontWeight: '500',
    color: '#333333',
  },
  signatureAndroidContainer: {
    backgroundColor: '#ffffff',
  },
  signatureAndroidContent: {
    borderBottomWidth: getRem(1),
    marginLeft: getRem(8),
    marginRight: getRem(8),
    marginBottom: getRem(20),
    borderBottomColor: '#999999',
  },
  signature: {
    height: getRem(122),
  },
  deleteContainer: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  deleteButton: {
    fontSize: 14,
    textAlign: 'right',
    color: '#1f88d8',
  },
  existLocation: {
    color: '#666666',
  },
  imgContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    flexWrap: 'wrap',
    marginLeft: getRem(40),
  },
  imagesContainerStyle: {
    borderBottomWidth: getRem(1),
    borderBottomColor: 'rgba(33, 33, 33, 0.1)',
    paddingTop: getRem(20),
  },
  img: {
    height: getRem(90),
    width: getRem(90),
    borderRadius: getRem(4),
    marginBottom: getRem(20),
    marginRight: getRem(10),
  },
  licenseImgContainer: {
    margin: getRem(20),
    marginLeft: getRem(20),
  },
  license: {
    height: getRem(128),
    width: getRem(335),
    borderRadius: getRem(4),
    marginVertical: getRem(10),
    marginRight: getRem(8),
  },
  loadingImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    height,
    width,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  pleaseSignYourNameBelow: {
    paddingLeft: getRem(45),
    marginTop: getRem(5),
    fontSize: getRem(12),
    fontWeight: '300',
    lineHeight: getRem(16),
    color: '#9b9b9b',
  },
})

export default styles
