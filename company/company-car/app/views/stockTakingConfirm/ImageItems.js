/**
 * @flow
 */
import React from 'react'
import { View, Image } from 'react-native'

import styles from './StockStyleSheetConfirm'

type Props = {
  sources: Array,
  isLicense: boolean,
  containerStyle: any,
}

export default function ImageItems(props: Props) {
  const { sources, isLicense, containerStyle } = props
  return (
    <View style={[styles.imgContainer,
      containerStyle,
      isLicense && styles.licenseImgContainer]}
    >
      {sources.length > 0 && sources.map((v) => (
        v &&
        <Image
          key={v}
          source={{ uri: v }}
          style={isLicense ? styles.license : styles.img}
        />
      ))
      }
    </View>
  )
}
