/**
 * @flow
 */
import React from 'react'
import { View, Text } from 'react-native'

import styles from './StockStyleSheetConfirm'

type Props = {
  label: string,
  value: any,
  noBorder: boolean,
}

export default function StockItem(props: Props) {
  const { label, value, noBorder } = props
  return (
    <View style={[styles.vehicleInfoItem, noBorder ? { borderBottomWidth: 0 } : null]}>
      <Text style={styles.vehicleInfoLeft}>{label}</Text>
      <Text style={styles.vehicleInfoRight}>{value}</Text>
    </View>
  )
}
