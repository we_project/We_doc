import React, { Component } from 'react'
import { View, Image, Text, TouchableOpacity, Platform } from 'react-native'
import SignatureCapture from 'react-native-signature-capture'

import styles from './StockStyleSheetConfirm'
import images from '../../images'
import Localized from '../../utils/Localized'
import UserUtil from '../../utils/User'

const User = new UserUtil()

const strings = Localized.Strings

type Props = {
  onDeleteSignature: () => void,
  onSaveEvent: () => void,
  onDragEvent: () => void,
}

class Signature extends Component {
  constructor(props: Props) {
    super(props)
    this.state = {
      userName: '',
    }
  }
  componentWillMount() {
    User.getUser().then((user) => {
      const userName = user.userName
      this.setState({ userName })
    }, err => {
      console.log(err)
    })
  }

  props: Props

  render() {
    const { onDeleteSignature, onSaveEvent, onDragEvent } = this.props
    return (
      <View
        style={styles.signatureContainer}
      >
        <View style={styles.signatureHeaderContainer}>
          <View style={styles.signatureHeader}>
            <View style={styles.signatureLeftIcon}>
              <Image source={images.signature} />
            </View>
            <Text style={styles.signatureTitle}>
              {strings.digitalSignature}
            </Text>
            <TouchableOpacity
              style={styles.deleteContainer}
              onPress={onDeleteSignature}
            >
              <Text style={styles.deleteButton}>{strings.deleteSignature}</Text>
            </TouchableOpacity>
          </View>
          <Text style={styles.pleaseSignYourNameBelow}>
            {`${strings.pleaseSignYourNameBelow}${this.state.userName ? strings.comma : ''} ${this.state.userName || ''}`}
          </Text>
        </View>
        {Platform.OS === 'ios' ?
          <SignatureCapture
            ref='sign'
            style={styles.signature}
            onSaveEvent={onSaveEvent}
            onDragEvent={onDragEvent}
            saveImageFileInExtStorage={false}
            showNativeButtons={false}
            showTitleLabel={false}
            viewMode={'portrait'}
          /> :
          <View style={styles.signatureAndroidContainer}>
            <View style={styles.signatureAndroidContent}>
              <SignatureCapture
                ref='sign'
                style={styles.signature}
                onSaveEvent={onSaveEvent}
                onDragEvent={onDragEvent}
                saveImageFileInExtStorage={false}
                showNativeButtons={false}
                showTitleLabel={false}
                viewMode={'portrait'}
              />
            </View>
          </View>
        }
      </View>
    )
  }
}

export default Signature
