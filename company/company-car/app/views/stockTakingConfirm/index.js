/**
 * @flow
 */
import React from 'react'
import { View, Text, ScrollView, Image, Modal } from 'react-native'
import { uniqueId, isEmpty, filter } from 'lodash'
import { NavigationActions } from 'react-navigation'
import UserUtil from '../../utils/User'
import Localized from '../../utils/Localized'
import styles from './StockStyleSheetConfirm'
import PanelButton from '../../components/PanelButton'
import VehicleInfoCard from '../../components/VehicleInfoCard'
import AccordionPanel from '../../components/AccordionPanel'
import Toast from '../../components/Toast'
import DialogSuccess from '../../components/DialogSuccess'
import images from '../../images'
import StockItem from './StockItem'
import ImageItems from './ImageItems'
import AppNetworking from '../../utils/AppNetworking'
import Signature from './Signature'

const Networking = new AppNetworking()
const strings = Localized.Strings
const User = new UserUtil()

type Props = {
  navigation: object,
}

class StockTakingConfirm extends React.Component {
  static navigationOptions = {
    title: strings.stockTaking
  }

  constructor(props: Props) {
    super(props)
    this.state = {
      showSuccess: false,
      signResult: null,
      signPath: null,
      draggedSignature: false,
      isLoading: false,
      stockData: global.stockDataService.getStockData,
    }
  }

  handleSaveEvent = (result) => {
    if (this.state.draggedSignature) {
      this.setState({
        signResult: result.encoded,
        signPath: result.path,
        stockData: Object.assign(this.state.stockData, { signature: result.encoded }),
        isLoading: true,
      }, () => {
        this.handleSubmitData()
      })
    }
  }

  props: Props

  timer: null

  deleteTimer: null

  saveSignature = () => {
    this.timer = setTimeout(() => {
      if (!this.state.signResult && !this.state.draggedSignature) {
        this.refs.toast.show(strings.yourSignatureIsRequired)
      }
      this.timer = null
    }, 300)
    this.refs.signature.refs.sign.saveImage()
  }

  handleSubmitData = () => {
    const stockData = this.state.stockData
    const { stockImages, damageImages, hasUpload } = this.checkStockImage(stockData)
    if (hasUpload) {
      this.submitStockData(stockData)
    } else if (!isEmpty(stockImages)) {
      this.handleUploadStockImage(stockData, stockImages)
    } else if (!isEmpty(damageImages)) {
      this.handleUploadDamageImage(stockData)
    }
  }

  checkStockImage = (stockData) => {
    const stockDataImage = stockData
    const checkFiled = ['leftFrontImage', 'rightBackImage', 'licenseImage', 'odometerImage']
    const stockImages = filter(checkFiled, (v) => isEmpty(stockDataImage[v].id))
    const damageImages = filter(stockData.damages.damageImages, (v) => isEmpty(v.id))
    return { stockImages, damageImages, hasUpload: isEmpty(stockImages) && isEmpty(damageImages) }
  }

  handleUploadStockImage = (stockData, stockImages) => {
    const newStockData = stockData
    stockImages.forEach((v) => {
      if (!newStockData[v].id) {
        Networking.uploadImage(newStockData[v].uri).then((resp) => {
          newStockData[v].id = resp[0].id
          const { hasUpload } = this.checkStockImage(newStockData)
          if (hasUpload) {
            this.submitStockData(stockData)
          }
        }, () => {
          this.setState({ isLoading: false })
          this.handleDeleteSignature()
          this.refs.toast.show(strings.submitFailedPleaseTryAgainLater)
        })
      }
    })
  }

  handleUploadDamageImage = (stockData) => {
    const newStockData = stockData
    newStockData.damages.damageImages.forEach((v, index) => {
      if (!v.id) {
        Networking.uploadImage(v.uri).then((resp) => {
          newStockData.damages.damageImages[index].id = resp[0].id
          const { hasUpload } = this.checkStockImage(newStockData)
          if (hasUpload) {
            this.submitStockData(stockData)
          }
        }, () => {
          this.setState({ isLoading: false })
          this.handleDeleteSignature()
          this.refs.toast.show(strings.submitFailedPleaseTryAgainLater)
        })
      }
    })
  }

  submitStockData = () => {
    const stockData = this.state.stockData
    Networking.uploadStockData(stockData.id, stockData).then(() => {
      this.setState({ showSuccess: true })
      this.setState({ isLoading: false })
    }, () => {
      this.handleDeleteSignature()
      this.setState({ isLoading: false })
      this.refs.toast.show(strings.submitFailedPleaseTryAgainLater)
    })
  }

  handleDeleteSignature = () => {
    this.deleteTimer = setTimeout(() => {
      this.refs.signature.refs.sign.resetImage()
      this.setState({
        signResult: null,
        signPath: null,
        draggedSignature: false,
      })
      this.deleteTimer = null
    }, 350)
  }

  handleCloseSuccess = () => {
    this.setState({ showSuccess: false })
    User.getUser().then((user) => {
      const isAdmin = !!user.isAdmin
      this.props.navigation.dispatch(
        NavigationActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({
              routeName: isAdmin ? 'AdminVehicleList' : 'VehicleList',
            }),
          ],
        }),
      )
    }, err => {
      console.log(`get user info${err}`)
    })
  }

  handleDragEvent = (dragged) => {
    if (dragged.dragged) {
      this.setState({ draggedSignature: true })
    }
  }

  render() {
    const { plateNumber, model, vin, location, exteriorColour, interiorColour,
      warningTriangleInCar, fireExtinguisher, floorMat, mileage, provincialCapital,
      leftFrontImage, rightBackImage, licenseImage, odometerImage, damages, carIsMobileAndInUse }
      = this.state.stockData
    return (
      <View style={styles.root}>
        <ScrollView>
          <VehicleInfoCard data={{ plateNumber, provincialCapital, model, vin }} />
          <AccordionPanel title={strings.vehicleBasicInformation} icon={images.carIcon}>
            <View style={styles.vehicleInfoItem}>
              <Text style={styles.vehicleInfoLeft}>{strings.location}</Text>
              <View style={{ flexDirection: 'row' }}>
                <Image source={location ? images.blueLocation : images.grayLocation} />
                <Text style={[styles.vehicleInfoRight, styles.existLocation]} >
                  {strings.location}</Text>
              </View>
            </View>
            <StockItem label={strings.exteriorColour} value={strings.color[exteriorColour]} />
            <StockItem label={strings.interiorColour} value={strings.color[interiorColour]} />
            <View style={[styles.vehicleInfoItem, { borderBottomWidth: 0 }]}>
              <Text style={styles.vehicleInfoLeft}>{strings.exteriorPhoto}</Text>
            </View>
            <ImageItems
              sources={[leftFrontImage && leftFrontImage.uri, rightBackImage && rightBackImage.uri]}
            />
          </AccordionPanel>
          <AccordionPanel title={strings.vehicleLicense} icon={images.license}>
            {licenseImage && <ImageItems sources={[licenseImage.uri]} isLicense />}
          </AccordionPanel>
          <AccordionPanel title={strings.vehicleSpecificInformation} icon={images.catalogue}>
            <StockItem label={strings.warningTriangleInCar} value={strings[warningTriangleInCar]} />
            <StockItem label={strings.fireExtinguisher} value={strings[fireExtinguisher]} />
            <StockItem label={strings.floorMat} value={strings[floorMat]} noBorder />
          </AccordionPanel>
          <AccordionPanel title={strings.mileage} icon={images.mileage}>
            <StockItem label={strings.mileageNumber} value={mileage} />
            <View style={[styles.vehicleInfoItem, { borderBottomWidth: 0 }]}>
              <Text style={styles.vehicleInfoLeft}>{strings.odometerPhoto}</Text>
            </View>
            {odometerImage && <ImageItems sources={[odometerImage.uri]} />}
          </AccordionPanel>
          <AccordionPanel title={strings.damages} icon={images.damages}>
            <StockItem
              label={strings.damageLevel}
              value={strings[damages.damageLevel.toLowerCase()]}
            />
            {!isEmpty(damages.damageImages) ? <ImageItems
              key={uniqueId()}
              sources={damages.damageImages.map((v) => v.uri)}
              containerStyle={styles.imagesContainerStyle}
            /> : null}
            <StockItem
              label={strings.carIsMobileAndInUse}
              value={strings[carIsMobileAndInUse]}
              noBorder
            />
          </AccordionPanel>
          <Signature
            ref='signature'
            onDeleteSignature={this.handleDeleteSignature}
            onSaveEvent={this.handleSaveEvent}
            onDragEvent={this.handleDragEvent}
          />
          <PanelButton
            status='enabled'
            text={strings.btnTextSubmit}
            keyboardType='numeric'
            onPress={() => { this.saveSignature() }}
          />
        </ScrollView>
        <Toast ref='toast' />
        {this.state.showSuccess === true ?
          <DialogSuccess onClose={this.handleCloseSuccess} /> : null}
        <Modal
          transparent
          ref='modal'
          onRequestClose={() => { this.setState({ isLoading: false }) }}
          visible={this.state.isLoading}
          animationType='none'
        >
          <View style={styles.loadingImage}>
            <Image source={images.loading} />
          </View>
        </Modal>
        <Toast ref='toast' />
      </View>
    )
  }
}

export default StockTakingConfirm
