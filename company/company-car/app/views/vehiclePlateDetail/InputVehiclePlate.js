import React from 'react'
import { StyleSheet, View, Text, TextInput, TouchableOpacity, Image, Keyboard } from 'react-native'

import Localized from '../../utils/Localized'
import { getRem } from '../../styles/getRem'
import { PROVINCIAL_CAPITAL_ABBREVIATION } from '../../utils/PlateNumber'
import Toast from '../../components/Toast'
import down from '../../images/down.png'
import ModalPicker from '../../components/ModalPicker'

const strings = Localized.Strings
const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  verifyCarInfo: {
    textAlign: 'center',
    fontSize: getRem(16),
    color: '#333333',
    marginTop: getRem(47),
    marginBottom: getRem(28),
  },
  content: {
    height: getRem(60),
    borderStyle: 'solid',
    borderWidth: getRem(1),
    borderColor: '#d8d8d8',
    alignItems: 'center',
    flexDirection: 'row',
  },
  plateNumberText: {
    width: getRem(161),
    textAlign: 'center',
    fontSize: getRem(16),
  },
  plateNumberPicker: {
    width: getRem(72),
    borderLeftWidth: getRem(1),
    borderLeftColor: '#eaebed',
    height: getRem(26),
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  plateNumber: {
    fontSize: getRem(16),
    fontWeight: '300',
    textAlign: 'right',
    color: '#1f88d8'
  },
  pickerImage: {
    width: getRem(10),
    height: getRem(10),
    marginLeft: getRem(10),
  },
  plateNumberInput: {
    flex: 1,
    fontSize: getRem(16),
    fontWeight: '300',
    color: '#333333',
  },
  modal: {
    height: getRem(256),
  },
  btnContainer: {
    flexDirection: 'row',
    height: getRem(40),
    borderTopWidth: getRem(1),
    borderTopColor: '#1f88d8',
  },
  btn: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  next: {
    fontSize: getRem(14),
    fontWeight: '500',
    marginRight: getRem(16),
  },
})

type Props = {
  navigation: object,
}

export default class InputVehiclePlate extends React.Component {
  static navigationOptions = (props: Props) => {
    const { state } = props.navigation
    const isNextStatus = state.params && state.params.isNext
    return {
      title: strings.titleCarPlateDetail,
      headerRight: (
        <TouchableOpacity onPress={isNextStatus ? () => {
          Keyboard.dismiss()
          const { plateNumber, provincialCapital, toast } = state.params
          const stockData = global.stockDataService.getStockData
          const isCounted = stockData.plateNo.length > 6 ?
            plateNumber === stockData.plateNo.substring(1)
            : plateNumber === stockData.plateNo
          if (isCounted) {
            global.stockDataService.addStockData({
              provincialCapital,
              plateNumber,
            })
            props.navigation.navigate('StockTakingOne')
          } else {
            toast.show(strings.notCountableByUser)
          }
        } : null}
        >
          <Text style={[styles.next, isNextStatus ? { color: '#1f88d8' } : { color: '#999999' }]}>
            {strings.next}</Text>
        </TouchableOpacity>
      )
    }
  }

  constructor(props: Props) {
    super(props)
    const { plateNumber, provincialCapital } = global.stockDataService.getStockData
    this.state = {
      plateNumber,
      provincialCapital,
    }
  }

  componentWillMount() {
    this.checkNext(this.state.plateNumber)
  }

  handleTextChange = (text) => {
    const nText = text.toUpperCase()
    if (nText.indexOf('I') === -1 && nText.indexOf('O') === -1) {
      this.setState({ plateNumber: nText })
      global.stockDataService.addStockData({ plateNumber: nText })
      this.checkNext(nText)
    }
  }

  checkNext = (v) => {
    if (v.length === 6) {
      this.props.navigation.setParams({
        isNext: true,
        plateNumber: v,
        toast: this.refs.toast,
        provincialCapital: this.state.provincialCapital
      })
    } else {
      this.props.navigation.setParams({
        isNext: false,
        plateNumber: v,
        toast: this.refs.toast,
        provincialCapital: this.state.provincialCapital
      })
    }
  }

  render() {
    return (
      <View style={styles.root}>
        <Text style={styles.verifyCarInfo}>{strings.inputPlateNumber}</Text>
        <View style={styles.content}>
          <Text style={styles.plateNumberText}>{strings.carPlateNumber}</Text>
          <ModalPicker
            data={PROVINCIAL_CAPITAL_ABBREVIATION.map((v) => ({ key: v, label: v }))}
            initValue={this.state.provincialCapital}
            onChange={option => {
              this.setState({ provincialCapital: option.label })
              global.stockDataService.addStockData({ provincialCapital: option.label })
              this.props.navigation.setParams({ provincialCapital: option.label })
            }}
          >
            <View style={styles.plateNumberPicker}>
              <Text style={styles.plateNumber}>{this.state.provincialCapital}</Text>
              <Image source={down} style={styles.pickerImage} />
            </View>
          </ModalPicker>
          <TextInput
            style={styles.plateNumberInput}
            autoFocus
            autoCorrect={false}
            placeholder={strings.required}
            maxLength={6}
            onChangeText={this.handleTextChange}
            value={this.state.plateNumber}
            multiline={false}
            returnKeyType='done'
            underlineColorAndroid={'rgba(0,0,0,0)'}
          />
        </View>
        <Toast ref='toast' />
      </View>
    )
  }
}
