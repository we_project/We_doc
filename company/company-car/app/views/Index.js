/**
 * @flow
 */
import { StackNavigator } from 'react-navigation'
import React from 'react'
import { Text } from 'react-native'
import RoleSelector from './selectRole'
import LoginWebView from './login/LoginWebView'
import AdminLogin from './adminLogin'
import AdminVehicleList from './vehicleList/admin'
import SearchVehicle from './search'
import VehicleList from './vehicleList'
import LoginError from './login/LoginError'
import VehicleDetail from './vehicleDetail'
import InputVehiclePlate from './vehiclePlateDetail/InputVehiclePlate'
import StockTakingOne from './stockTakingOne'
import StockTakingTwo from './stockTakingTwo'
import StockTakingConfirm from './stockTakingConfirm'
import Camera from '../components/Camera'
import StockDataService from '../utils/StockDataService'

global.stockDataService = new StockDataService()

const App = StackNavigator({
  RoleSelector: { screen: RoleSelector },
  Login: { screen: LoginWebView },
  LoginError: { screen: LoginError },
  VehicleList: { screen: VehicleList, key: 'VehicleList' },
  AdminVehicleList: { screen: AdminVehicleList, key: 'AdminVehicleList' },
  VehicleDetail: { screen: VehicleDetail },
  InputVehiclePlate: { screen: InputVehiclePlate },
  StockTakingOne: { screen: StockTakingOne },
  StockTakingTwo: { screen: StockTakingTwo },
  StockTakingConfirm: { screen: StockTakingConfirm },
  Camera: { screen: Camera },
  AdminLogin: { screen: AdminLogin },
  SearchVehicle: { screen: SearchVehicle, key: 'SearchVehicle' }
}, {
  navigationOptions: {
    headerStyle: {
      backgroundColor: '#fff'
    },
    headerTitleStyle: {
      alignSelf: 'center',
      fontWeight: 'bold',
      color: '#0b0b0b'
    },
    headerBackTitle: null,
    headerTruncatedBackTitle: null,
    headerTintColor: '#0b0b0b',
    headerRight: <Text />
  }
})

module.exports = App
