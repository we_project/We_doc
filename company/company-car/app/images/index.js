import blueLocation from './blue-location/blue-location.png'
import carIcon from './car/car.png'
import grayLocation from './gray-location/gray-location.png'
import warning from './warning/warning.png'
import rightArrow from './right-arrow/right-arrow.png'
import leftArrow from './left-arrow/left-arrow.png'
import catalogue from './catalogue/catalogue.png'
import mileage from './mileage/mileage.png'
import signature from './signature/signature.png'
import downArrow from './down-arrow/down-arrow.png'
import correct from './correct/correct.png'
import rightBack from './rightBack/rightBack.png'
import close from './close/close.png'
import license from './license/lisence.png'
import cameraIcon from './cameraIcon/cameraIcon.png'
import loading from './loading/loading.gif'
import leftFront from './leftFront/leftFront.png'
import rightGrayArrow from './right-gray-arrow/right-gray-arrow.png'
import damages from './damages/damages.png'
import cancel from './cancel/cancel.png'
import carTopView from './carTopView/carTopView.png'
import bluePoint from './bluePoint/bluePoint.png'
import success from './success/success.png'
import profile from './profile/profile.png'
import unlock from './unlock/unlock.png'
import adminLogin from './admin-login/admin-login.png'
import search from './search/search.png'
import scan from './scan/scan.png'
import checked from './thick/thick.png'
import logout from './logout/logout.png'
import reload from './reload/reload.png'

const images = {
  blueLocation,
  carIcon,
  grayLocation,
  warning,
  catalogue,
  mileage,
  signature,
  rightArrow,
  downArrow,
  leftArrow,
  correct,
  rightBack,
  close,
  license,
  cameraIcon,
  loading,
  leftFront,
  rightGrayArrow,
  damages,
  cancel,
  carTopView,
  bluePoint,
  success,
  profile,
  unlock,
  adminLogin,
  search,
  scan,
  checked,
  logout,
  reload,
}

export default images
