/**
 * @flow
 */
/*  eslint-disable */
import React, { Component } from 'react'
import { AppRegistry } from 'react-native'
import Orientation from 'react-native-orientation'

import Index from './app/views/Index'

export default class companycar extends Component {
  componentDidMount() {
    Orientation.lockToPortrait()
  }

  render() {
    return (
      <Index />
    )
  }
}

AppRegistry.registerComponent('companycar', () => companycar)
