echo '==== create new key ===='
keytool -genkey -v \
-keystore app/company-car-key.keystore \
-alias company-car-key-alias \
-keyalg RSA \
-keysize 2048 \
-validity 10000 \
-storepass changeW0rld! \
-keypass changeW0rld! \
-dname CN=COMPANYCAR,OU=TW,O=ORG,L=Wuhan,ST=Hubei,C=CN
echo '==== end generate key end ===='