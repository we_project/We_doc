# Company Car Mobile

## Execution environment requirements
- Node.js
- npm/yarn latest stable version




## How to run employee version
### install and link
```
npm install
react-native link

```
### run
```
react-native run-ios
react-native run-android

```

## How to run admin version

### difference
```
npm run pre-admin

```
this step is to generate different index.ios.js & index.android.js as entry file for application, we will use this way until go live.

### ditto
```

npm install
react-native link
react-native run-ios
react-native run-android


```

### debug
cmd+d


### comman way to quick fix
rm -r ios/build
