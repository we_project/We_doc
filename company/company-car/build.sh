#!/usr/bin/env bash

ENV=`echo "$1" | tr "[:upper:]" "[:lower:]"`
JENKINS_BUILD_NUMBER=$2
PLATFORM=`echo "$3" | tr "[:upper:]" "[:lower:]"`
PROJECT_DIR="$( cd "$( dirname "$0"  )" && pwd  )"

echo ""
echo "JENKINS_BUILD_NUMBER=${JENKINS_BUILD_NUMBER}"
echo "ENV=${ENV}"
echo "PLATFORM=${PLATFORM}"
echo "PROJECT_DIR=${PROJECT_DIR}"
echo ""

function ios(){
    react-native bundle --platform ios  --dev false --entry-file index.ios.js --bundle-output ios/bundle/index.ios.jsbundle  --assets-dest ios/bundle
    cd $PROJECT_DIR/ios
    fastlane $ENV JENKINS_BUILD_NUMBER:$JENKINS_BUILD_NUMBER
    cd $PROJECT_DIR
}

function android(){
   react-native bundle --platform android --dev false --entry-file index.android.js --bundle-output android/app/src/main/assets/index.android.bundle --assets-dest android/app/src/main/res/
   cd $PROJECT_DIR/android
   fastlane $ENV JENKINS_BUILD_NUMBER:$JENKINS_BUILD_NUMBER
   cd $PROJECT_DIR
}




case ${PLATFORM} in
  ios)
    ios
    ;;
  android)
    android
    ;;
  "")
    ios
    android
    ;;
  *)
    echo "FAILED!"
    echo "Invalid Platform Argument: $PLATFORM"
    exit 1
    ;;
esac

