using System;
using System.Reflection;

namespace Daimler.MicroServices.HRWTAPI.Entry.Areas.HelpPage.ModelDescriptions
{
    public interface IModelDocumentationProvider
    {
        string GetDocumentation(MemberInfo member);

        string GetDocumentation(Type type);
    }
}