﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Daimler.MicroServices.HRWT.BizObject;
using log4net;
using Daimler.MicroServices.HRWTAPI.BLL;

namespace Daimler.MicroServices.HRWTAPI.Entry.Controllers {
    [RoutePrefix("api/Account")]
    public class AccountController : ApiController {
        private static readonly ILog log = LogManager.GetLogger(typeof(AccountController));
        [HttpPost]
        public ReturnBoolResult AccountExist(ClientAccount account) {
            ReturnBoolResult result = new ReturnBoolResult();
            try {
                if (account == null || string.IsNullOrWhiteSpace(account.clientId) || string.IsNullOrWhiteSpace(account.clientSecret)) {
                    result.msg = ReturnMsgHRWT.RequiredParameterIsNull;
                    return result;
                }
                result.boolResult = account.Exist();
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgHRWT.ExceptionOccurs;
            }
            return result;
        }
    }
}
