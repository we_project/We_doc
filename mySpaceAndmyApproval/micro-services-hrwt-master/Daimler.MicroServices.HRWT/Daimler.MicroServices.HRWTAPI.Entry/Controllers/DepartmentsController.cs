﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Daimler.MicroServices.HRWT.BizObject;
using Daimler.MicroServices.HRWTAPI.BLL;
using log4net;

namespace Daimler.MicroServices.HRWTAPI.Entry.Controllers {
    [RoutePrefix("api/Departments")]
    public class DepartmentsController : ApiController {
        private static readonly ILog log = LogManager.GetLogger(typeof(DepartmentsController));
        public ReturnDepartments GetDepts() {
            ReturnDepartments result = new ReturnDepartments();
            try {
                result.depts = DepartmentsService.GetDepts();
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgHRWT.ExceptionOccurs;
            }
            return result;
        }
    }
}
