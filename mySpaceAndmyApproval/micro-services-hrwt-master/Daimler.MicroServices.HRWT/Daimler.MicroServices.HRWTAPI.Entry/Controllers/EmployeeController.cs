﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Daimler.MicroServices.HRWT.BizObject;
using Daimler.MicroServices.HRWTAPI.BLL;
using log4net;

namespace Daimler.MicroServices.HRWTAPI.Entry.Controllers {
    [RoutePrefix("api/Employee")]
    public class EmployeeController : ApiController {
        private static readonly ILog log = LogManager.GetLogger(typeof(EmployeeController));
        [Route("{smUser}/Email")]
        public ReturnEmail GetEmail(string smUser) {
            ReturnEmail result = new ReturnEmail();
            try {
                if (string.IsNullOrWhiteSpace(smUser)) {
                    result.msg = ReturnMsgHRWT.RequiredParameterIsNull;
                    return result;
                }
                result.email = EmployeeService.GetEmail(smUser);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgHRWT.ExceptionOccurs;
            }
            return result;
        }
    }
}
