﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Daimler.MicroServices.HRWT.BizObject;
using Daimler.MicroServices.HRWTAPI.BLL;
using log4net;

namespace Daimler.MicroServices.HRWTAPI.Entry.Controllers {
    [RoutePrefix("api/Employees")]
    public class EmployeesController : ApiController {
        private static readonly ILog log = LogManager.GetLogger(typeof(EmployeesController));
        public ReturnEmployees GetByCondition([FromUri]EmployeesSearchCondition search) {
            ReturnEmployees result = new ReturnEmployees();
            try {
                if (search == null || (string.IsNullOrWhiteSpace(search.name) && string.IsNullOrWhiteSpace(search.deptId) && string.IsNullOrWhiteSpace(search.exectvelvl))) {
                    result.msg = ReturnMsgHRWT.RequiredParameterIsNull;
                    return result;
                }
                result.employees = EmployeesService.GetEmployees(search);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgHRWT.ExceptionOccurs;
            }
            return result;
        }
    }
}
