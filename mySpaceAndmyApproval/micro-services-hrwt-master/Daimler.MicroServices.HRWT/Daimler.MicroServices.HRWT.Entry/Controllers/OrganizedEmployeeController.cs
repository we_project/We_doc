﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Daimler.MicroServices.HRWT.BizObject;
using Daimler.MicroServices.HRWT.BLL;
using log4net;

namespace Daimler.MicroServices.HRWT.Entry.Controllers {
    [Authorize]
    [RoutePrefix("api/OrganizedEmployee")]
    public class OrganizedEmployeeController : ApiController {
        private static readonly ILog log = LogManager.GetLogger(typeof(OrganizedEmployeeController));
        public async Task<ReturnOrganizedEmployee> GetEmployees(string emplId) {
            ReturnOrganizedEmployee result = new ReturnOrganizedEmployee();
            try {
                if (string.IsNullOrWhiteSpace(emplId)) {
                    result.msg = ReturnMsgHRWT.RequiredParameterIsNull;
                    return result;
                }
                result.employee = await OrganizedEmployeeService.GetOrganizedEmployeeAsync(emplId);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgHRWT.ExceptionOccurs;
            }
            return result;
        }
    }
}
