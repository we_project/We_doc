﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Daimler.MicroServices.HRWT.BizObject;
using Daimler.MicroServices.HRWT.BLL;
using log4net;

namespace Daimler.MicroServices.HRWT.Entry.Controllers {
    [Authorize]
    [RoutePrefix("api/Employees")]
    public class EmployeesController : ApiController {
        private static readonly ILog log = LogManager.GetLogger(typeof(EmployeesController));
        public async Task<ReturnEmployees> GetByCondition([FromUri]EmployeesSearchCondition search) {
            ReturnEmployees result = new ReturnEmployees();
            try {
                if (search == null || (string.IsNullOrWhiteSpace(search.name) && string.IsNullOrWhiteSpace(search.deptId) && string.IsNullOrWhiteSpace(search.exectvelvl))) {
                    result.msg = ReturnMsgHRWT.RequiredParameterIsNull;
                    return result;
                }
                result.employees = await EmployeesService.GetEmployeesAsync(search);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgHRWT.ExceptionOccurs;
            }
            return result;
        }
    }
}
