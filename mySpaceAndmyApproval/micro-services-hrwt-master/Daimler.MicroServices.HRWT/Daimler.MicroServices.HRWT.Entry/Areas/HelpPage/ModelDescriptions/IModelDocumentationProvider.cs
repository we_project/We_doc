using System;
using System.Reflection;

namespace Daimler.MicroServices.HRWT.Entry.Areas.HelpPage.ModelDescriptions
{
    public interface IModelDocumentationProvider
    {
        string GetDocumentation(MemberInfo member);

        string GetDocumentation(Type type);
    }
}