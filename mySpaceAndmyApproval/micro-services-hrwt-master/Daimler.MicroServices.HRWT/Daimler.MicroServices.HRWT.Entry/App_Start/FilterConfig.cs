﻿using System.Web;
using System.Web.Mvc;

namespace Daimler.MicroServices.HRWT.Entry {
    public class FilterConfig {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters) {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
