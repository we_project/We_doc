﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;
using Daimler.MicroServices.Utils.WebApi.Filters;
using Daimler.MicroServices.Utils.WebApi.MessageHandlers;
using System.Configuration;
using System.Web.Http.Cors;

namespace Daimler.MicroServices.HRWT.Entry {
    public static class WebApiConfig {
        public static void Register(HttpConfiguration config) {
            string origins = ConfigurationManager.AppSettings["allow-origins"];
            if (!string.IsNullOrWhiteSpace(origins)) {
                var cors = new EnableCorsAttribute(origins, "*", "*");
                config.EnableCors(cors);
            }

            //var cors=
            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));
            config.Filters.Add(new CustomExceptionFilterAttribute());
            config.MessageHandlers.Add(new LogHandler());
            config.Formatters.XmlFormatter.SupportedMediaTypes.Clear();

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
