﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.HRWT.DataImport {
    class ScheduleConfig {
        public class TimerSchedule {
            static TimerSchedule _default = new TimerSchedule(0, DefaultPeriodMinute);
            public static TimerSchedule Deafult {
                get {
                    return _default;
                }
            }
            const int DefaultPeriodMinute = 24 * 60;
            public TimerSchedule(DateTime nextRunTime, int PeriodMinute = DefaultPeriodMinute)
                : this(PeriodMinute) {
                Due = Convert.ToInt32(nextRunTime.Subtract(DateTime.Now).TotalMilliseconds);
            }
            public TimerSchedule(int PeriodMinute) {
                Period = PeriodMinute * 60 * 1000;
            }
            public TimerSchedule(int Due, int PeriodMinute)
                : this(PeriodMinute) {
                this.Due = Due;
            }
            public int Due { get; set; }

            public int Period { get; set; }
        }
        private static string beginTimeOfDay = ConfigurationManager.AppSettings["BeginTimeOfDay"];
        private static string runMulitipleTimesOneDay = ConfigurationManager.AppSettings["RunMulitipleTimesOneDay"];
        private static string interval = ConfigurationManager.AppSettings["Interval"];
        public static bool RunMulitipleTimesOneDay {
            get {
                if ("true".Equals(runMulitipleTimesOneDay, StringComparison.CurrentCultureIgnoreCase)) {
                    return true;
                }
                return false;
            }
        }
        public static int IntervalMinute {
            get {
                if (!string.IsNullOrWhiteSpace(interval)) {
                    return Convert.ToInt32(interval);
                }
                return 24 * 60;
            }
        }
        public static class BeginTime {
            static BeginTime() {
                string[] beginTime = beginTimeOfDay.Split(new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries);
                Hour = Convert.ToInt32(beginTime[0]);
                Minute = Convert.ToInt32(beginTime[1]);
            }
            public static int Hour { get; set; }
            public static int Minute { get; set; }
        }
        public static TimerSchedule NewSchedule {
            get {
                if (string.IsNullOrWhiteSpace(beginTimeOfDay)) {
                    throw new Exception("Need to config BeginTimeOfDay in App config.");
                }
                DateTime now = DateTime.Now;
                DateTime todayBeginTime = new DateTime(now.Year, now.Month, now.Day, BeginTime.Hour, BeginTime.Minute, 0);
                DateTime tomorrowBeginTime = todayBeginTime.AddDays(1);
                if (!RunMulitipleTimesOneDay) {
                    if (now > todayBeginTime) {
                        return new TimerSchedule(tomorrowBeginTime);
                    } else {
                        return new TimerSchedule(todayBeginTime);
                    }
                } else {
                    if (now < todayBeginTime) {
                        if (todayBeginTime.Subtract(now.AddMinutes(IntervalMinute)).TotalMinutes < IntervalMinute) {
                            return new TimerSchedule(todayBeginTime, IntervalMinute);

                        }
                    } else {
                        if (tomorrowBeginTime.Subtract(now.AddMinutes(IntervalMinute)).TotalMinutes < IntervalMinute) {
                            return new TimerSchedule(tomorrowBeginTime, IntervalMinute);
                        }
                    }
                }
                return null;
            }
        }
    }
}
