﻿@echo off
set serviceName="MicroServicesHRWT"
set "servicePath=%~dp0Daimler.MicroServices.HRWT.DataImport.exe"
set serviceDesc=""
echo %servicePath%
sc create %serviceName% binPath= %servicePath% DisplayName= %serviceName% start= auto
sc description %serviceName% %serviceDesc%
sc start %serviceName%
pause