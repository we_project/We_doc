﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.HRWTAPI.BLL.DataImport;
using log4net;

namespace Daimler.MicroServices.HRWT.DataImport {
    partial class HRWTService : ServiceBase {
        private static readonly ILog log = LogManager.GetLogger(typeof(HRWTService));        
        public HRWTService() {
            InitializeComponent();
        }

        protected override void OnStart(string[] args) {
            try {
                log.Debug("Start service");
                ScheduleTask.Schedule(HRWTServices.LoadUsers);
            } catch (Exception ex) {
                log.Error(this,ex);
                throw;
            }
        }

        protected override void OnStop() {
            try {
                ScheduleTask.StopSchedule();
                log.Debug("Stop service");
            } catch (Exception ex) {
                log.Error(this, ex);
                throw;
            }
        }
    }
}
