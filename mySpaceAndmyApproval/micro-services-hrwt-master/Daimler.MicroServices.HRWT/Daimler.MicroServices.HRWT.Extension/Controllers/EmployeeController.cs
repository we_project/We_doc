﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Daimler.MicroServices.HRWT.BizObject;
using Daimler.MicroServices.HRWT.BLL;
using log4net;

namespace Daimler.MicroServices.HRWT.Extension.Controllers {
    [Authorize]
    [RoutePrefix("api/Employee")]
    public class EmployeeController : ApiController {
        private static readonly ILog log = LogManager.GetLogger(typeof(EmployeeController));

        [Route("{sm_user}/Email")]
        public async Task<ReturnEmail> GetEmail(string sm_user) {

            ReturnEmail result = new ReturnEmail();
            try {
                if (string.IsNullOrWhiteSpace(sm_user)) {
                    result.msg = ReturnMsgHRWT.RequiredParameterIsNull;
                    return result;
                }
                result.email = await EmployeeService.GetEmailAsync(sm_user);
                result.Concur_psw = ConfigurationManager.AppSettings["Concur_psw"];
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgHRWT.ExceptionOccurs;
            }
            return result;
        }
    }
}
