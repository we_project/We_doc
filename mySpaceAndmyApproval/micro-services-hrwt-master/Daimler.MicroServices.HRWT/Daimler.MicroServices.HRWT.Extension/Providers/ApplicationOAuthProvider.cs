﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Daimler.MicroServices.HRWT.Extension.Models;
using System.Security.Principal;
using Daimler.MicroServices.HRWT.BLL;
using Daimler.MicroServices.HRWT.BizObject;
using System.Configuration;
using log4net;

namespace Daimler.MicroServices.HRWT.Extension.Providers {
    public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider {
        private readonly string _publicClientId;
        private static readonly ILog log = LogManager.GetLogger(typeof(ApplicationOAuthProvider));
        public ApplicationOAuthProvider(string publicClientId) {
            if (publicClientId == null) {
                throw new ArgumentNullException("publicClientId");
            }

            _publicClientId = publicClientId;
        }



        public override Task TokenEndpoint(OAuthTokenEndpointContext context) {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary) {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }

        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context) {
            string clientId = null;
            string clientSecret = null;
            if (context.TryGetBasicCredentials(out clientId, out clientSecret) || context.TryGetFormCredentials(out clientId, out clientSecret)) {
                ClientAccount account = new ClientAccount {
                    clientId = clientId,
                    clientSecret = clientSecret
                };
                log.Debug(ConfigurationManager.AppSettings["clients"]);
                log.Debug(clientId);
                string[] clients = ConfigurationManager.AppSettings["clients"].Split(',');
                log.Debug(clients[0]);

                if (!clients.Contains(clientId)) {
                    return;
                }
                bool exist = await AccountService.ClientExist(account);
                log.Debug(exist);
                if (exist) {
                    context.Validated(context.ClientId);
                }
            }
            //return Task.FromResult<object>(null);
        }

        public override Task GrantClientCredentials(OAuthGrantClientCredentialsContext context) {
            var identity = new ClaimsIdentity(
                new GenericIdentity(context.ClientId, OAuthDefaults.AuthenticationType),
                context.Scope.Select(x => new Claim("urn:oauth:scope", x))
                );
            context.Validated(identity);
            return base.GrantClientCredentials(context);
        }
        public override Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context) {
            if (context.ClientId == _publicClientId) {
                Uri expectedRootUri = new Uri(context.Request.Uri, "/");

                if (expectedRootUri.AbsoluteUri == context.RedirectUri) {
                    context.Validated();
                }
            }

            return Task.FromResult<object>(null);
        }

        public static AuthenticationProperties CreateProperties(string userName) {
            IDictionary<string, string> data = new Dictionary<string, string>
            {
                { "userName", userName }
            };
            return new AuthenticationProperties(data);
        }
    }
}