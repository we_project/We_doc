﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.HRWTAPI.IDAL;
using Daimler.MicroServices.HRWTAPI.SqlServerDAL;
using log4net;

namespace Daimler.MicroServices.HRWTAPI.BLL.DataImport {
    public class UserFile {
        private static readonly ILog log = LogManager.GetLogger(typeof(UserFile));
        static DirectoryInfo dir = new DirectoryInfo(SourceConfig.UserDataPath);
        static string searchPattern = string.Format("{0}*.{1}", SourceConfig.FilePrefix, SourceConfig.FileExt);
        public string GetLatestFile() {
            string result = null;
            FileInfo file = (from f in dir.GetFiles(searchPattern)
                             orderby f.LastWriteTime descending
                             select f).First();
            if (file != null) {
                result = file.FullName;
            }
            return result;
        }

        public string[] GetAllLines() {
            string file = GetLatestFile();
            if (!string.IsNullOrWhiteSpace(file)) {
                log.DebugFormat("Loading from file: {0}", file);
                string[] allLines = File.ReadAllLines(file);
                if (allLines != null && allLines.Count() > 0) {
                    allLines = allLines.Where(line => !string.IsNullOrWhiteSpace(line)).ToArray();
                    return allLines;
                }
            }
            return null;
        }

        public string[] GetHeader() {
            string[] header = null;
            string[] allLines = GetAllLines();
            if (allLines != null && allLines.Count() > 0) {
                header = allLines.First().Split(new string[] { SourceConfig.SplitStr }, StringSplitOptions.None);
            }
            log.Debug(string.Join("\n", header));
            return header;
        }
        public DataTable GetAllData() {
            DataTable dt = null;
            string[] allLines = GetAllLines();
            if (allLines != null && allLines.Count() > 1) {

                #region filter last line which start with F
                string lastLineStr = allLines.Last();
                string[] lastLine = lastLineStr.Split(new string[] { SourceConfig.SplitStr }, StringSplitOptions.None);
                if (SourceConfig.LastLineH.Equals(lastLine.First())) {
                    Array.Resize(ref allLines, allLines.Length - 1);
                }
                #endregion

                string[] header = allLines.First().Split(new string[] { SourceConfig.SplitStr }, StringSplitOptions.None);
                DataColumn[] columns = header.Select(h => new DataColumn(h, typeof(string))).ToArray();
                dt = new DataTable();
                dt.Columns.AddRange(columns);
                foreach (var userStr in allLines.Skip(1)) {
                    DataRow row = dt.NewRow();
                    string[] user = userStr.Split(new string[] { SourceConfig.SplitStr }, StringSplitOptions.None);
                    dt.Rows.Add(user);
                }
                log.DebugFormat("Loaded {0} users", dt.Rows.Count);
            }
            return dt;
        }
        static IHRWT hrwt = DAL.DefaultHRWT;
        public bool SaveToDB() {
            bool result = false;
            DataTable dt = GetAllData();
            try {
                result = hrwt.SaveUsers(dt);
                log.DebugFormat("Save to DB: {0}", result);
            } catch (Exception ex) {
                log.Error(this, ex);
            }
            return result;
        }
    }
}
