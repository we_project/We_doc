﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.HRWTAPI.BLL.DataImport {
    class SourceConfig {
        private static string userDataPath;
        private static class Keys {
            public static string UserDataPath = "UserDataPath";
            public static string FilePrefix = "FilePrefix";
            public static string FileExt = "FileExt";
        }
        public static string UserDataPath {
            get {
                if (string.IsNullOrWhiteSpace(userDataPath)) {
                    userDataPath = ConfigurationManager.AppSettings[Keys.UserDataPath];
                }
                return userDataPath;
            }
        }

        private static string splitStr = "|";
        public static string SplitStr {
            get {
                return splitStr;
            }
        }

        private static string lastLineH = "F";
        public static string LastLineH {
            get {
                return lastLineH;
            }
        }
        private static string filePrefix;
        public static string FilePrefix {
            get {
                if (string.IsNullOrWhiteSpace(filePrefix)) {
                    filePrefix = ConfigurationManager.AppSettings[Keys.FilePrefix];
                }
                return filePrefix;
            }
        }
        private static string fileExt;
        public static string FileExt {
            get {
                if (string.IsNullOrWhiteSpace(fileExt)) {
                    fileExt = ConfigurationManager.AppSettings[Keys.FileExt];
                }
                return fileExt;
            }
        }
    }
}
