﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.HRWT.BizObject;
using Daimler.MicroServices.HRWTAPI.IDAL;
using Daimler.MicroServices.HRWTAPI.SqlServerDAL;

namespace Daimler.MicroServices.HRWTAPI.BLL {
    public class OrganizedEmployeeService {
        static IHRWT hrwt = DAL.DefaultHRWT;
        public static OrganizedEmployee GetOrganizedEmployee(string emplId) {
            return hrwt.GetOrganizedEmployee(emplId);
        }
    }
}
