﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.HRWTAPI.IDAL;
using Daimler.MicroServices.HRWTAPI.SqlServerDAL;

namespace Daimler.MicroServices.HRWTAPI.BLL {
    public class EmployeeService {
        static IHRWT hrwt = DAL.DefaultHRWT;
        public static string GetEmail(string smUser) {
            return hrwt.GetEmail(smUser);
        }
    }
}
