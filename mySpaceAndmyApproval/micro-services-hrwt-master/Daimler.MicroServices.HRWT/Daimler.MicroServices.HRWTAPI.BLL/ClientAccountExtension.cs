﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.HRWT.BizObject;

namespace Daimler.MicroServices.HRWTAPI.BLL {
    public static class ClientAccountExtension {
        public static bool Exist(this ClientAccount account) {
            return AccountService.ExistClient(account.clientId, account.clientSecret);
        }
    }
}
