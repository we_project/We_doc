﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.HRWT.BizObject {
    public class ReturnEmployees : BaseReturnHRWT {
        public List<Employee> employees { get; set; }
    }
}
