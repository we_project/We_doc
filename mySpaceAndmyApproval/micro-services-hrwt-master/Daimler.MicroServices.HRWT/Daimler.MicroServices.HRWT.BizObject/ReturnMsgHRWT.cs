﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.HRWT.BizObject {
    public enum ReturnMsgHRWT {
        ExceptionOccurs = 1,
        RequiredParameterIsNull=2
    }
}
