﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.HRWT.BizObject {
    public class EmployeesSearchCondition {
        /// <summary>
        /// search by displayname,oprid
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// search by departmentid, eg: BMBS/SN
        /// </summary>
        public string deptId { get; set; }
        /// <summary>
        /// search by position level
        /// </summary>
        public string exectvelvl { get; set; }
    }
}
