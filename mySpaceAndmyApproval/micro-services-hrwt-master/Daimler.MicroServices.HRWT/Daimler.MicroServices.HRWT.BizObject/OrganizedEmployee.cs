﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.HRWT.BizObject {
    public class OrganizedEmployee : Employee {
        public OrganizedEmployee() { }
        public OrganizedEmployee(Employee e)
            : base(e) {

        }

        public List<OrganizedEmployee> subordinates { get; set; }
    }
}
