﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.HRWT.BizObject {
    public class Company {
        /// <summary>
        /// DC_COMPANY_DL from HRWT
        /// </summary>
        public string company { get; set; }
        /// <summary>
        /// sub departments
        /// </summary>
        public List<Dept> items { get; set; }
    }
}
