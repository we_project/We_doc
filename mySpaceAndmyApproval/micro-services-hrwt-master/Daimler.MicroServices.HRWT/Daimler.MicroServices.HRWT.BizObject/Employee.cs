﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.HRWT.BizObject {
    public class Employee {
        public Employee() { }
        public Employee(Employee e) {
            this.NAME_DISPLAY = e.NAME_DISPLAY;
            this.DC_DEPTID_DS = e.DC_DEPTID_DS;
            this.DC_CELL_PHONE = e.DC_CELL_PHONE;
            this.DC_BUSN_PHONE = e.DC_BUSN_PHONE;
            this.EMAIL_ADDR = e.EMAIL_ADDR;
            this.DC_COMPANY_DL = e.DC_COMPANY_DL;
            this.EMPLID = e.EMPLID;
            this.NAME = e.NAME;
            this.SUPERVISOR_ID = e.SUPERVISOR_ID;
            this.DC_LOCATION_DL = e.DC_LOCATION_DL;
            this.OPRID = e.OPRID;
            this.DC_EMPLMNTTYPE_ID = e.DC_EMPLMNTTYPE_ID;
            this.DC_EMPLMNTTYPE_DL = e.DC_EMPLMNTTYPE_DL;
            this.DC_APPROVER1 = e.DC_APPROVER1;
            this.DC_APPROVER1_NAME = e.DC_APPROVER1_NAME;
            this.DC_EXECTVELVL_ID = e.DC_EXECTVELVL_ID;
            this.DC_COMPANY_DS = e.DC_COMPANY_DS;
            this.DC_COST_CENTR_ID = e.DC_COST_CENTR_ID;
        }
        /// <summary>
        /// display name
        /// </summary>
        public string NAME_DISPLAY { get; set; }
        /// <summary>
        /// department id
        /// </summary>
        public string DC_DEPTID_DS { get; set; }
        /// <summary>
        /// cell phone
        /// </summary>
        public string DC_CELL_PHONE { get; set; }
        /// <summary>
        /// business phone
        /// </summary>
        public string DC_BUSN_PHONE { get; set; }
        /// <summary>
        /// email
        /// </summary>
        public string EMAIL_ADDR { get; set; }
        /// <summary>
        /// company
        /// </summary>
        public string DC_COMPANY_DL { get; set; }
        /// <summary>
        /// employee id
        /// </summary>
        public string EMPLID { get; set; }
        /// <summary>
        /// supervisor name
        /// </summary>
        public string NAME { get; set; }
        /// <summary>
        /// supervisor emplid
        /// </summary>
        public string SUPERVISOR_ID { get; set; }
        /// <summary>
        /// location
        /// </summary>
        public string DC_LOCATION_DL { get; set; }
        /// <summary>
        /// user id
        /// </summary>
        public string OPRID { get; set; }
        /// <summary>
        /// employee type id
        /// </summary>
        public string DC_EMPLMNTTYPE_ID { get; set; }
        /// <summary>
        /// employee type
        /// </summary>
        public string DC_EMPLMNTTYPE_DL { get; set; }
        /// <summary>
        /// approver emplid
        /// </summary>
        public string DC_APPROVER1 { get; set; }
        /// <summary>
        /// approver name
        /// </summary>
        public string DC_APPROVER1_NAME { get; set; }
        /// <summary>
        /// position level
        /// </summary>
        public string DC_EXECTVELVL_ID { get; set; }
        /// <summary>
        /// company
        /// </summary>
        public string DC_COMPANY_DS { get; set; }
        /// <summary>
        /// cost center
        /// </summary>
        public string DC_COST_CENTR_ID { get; set; }

    }
}
