﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.HRWT.BizObject {
    public class ReturnBoolResult : BaseReturnHRWT {
        public bool boolResult { get; set; }
    }
}
