﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.HRWT.BizObject {
    public class ReturnEmail : BaseReturnHRWT {
        public string email { get; set; }

        public string Concur_psw { get; set; }
    }
}
