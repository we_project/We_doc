﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.HRWT.BizObject
{
    public class BaseReturnHRWT
    {
        public bool success { get; set; }
        public ReturnMsgHRWT? msg { get; set; }
        public static BaseReturnHRWT Exception {
            get {
                return new BaseReturnHRWT {
                    success = false,
                    msg = ReturnMsgHRWT.ExceptionOccurs
                };
            }
        }
    }
}
