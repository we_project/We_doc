﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.HRWT.BizObject {

    /// <summary>
    /// Department
    /// </summary>
    public class Dept {
        /// <summary>
        /// DC_DEPTID_DS from HRWT
        /// </summary>
        public string dept { get; set; }
    }
}
