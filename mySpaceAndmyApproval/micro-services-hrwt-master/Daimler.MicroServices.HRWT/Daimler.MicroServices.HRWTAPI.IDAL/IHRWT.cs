﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.HRWT.BizObject;

namespace Daimler.MicroServices.HRWTAPI.IDAL {
    public interface IHRWT {
        bool SaveUsers(DataTable data);
        string GetEmail(string smUser);
        List<Company> GetDepts();
        bool ExistClient(string clientId, string clientSecret);
        List<Employee> GetEmployees(EmployeesSearchCondition search);
        OrganizedEmployee GetOrganizedEmployee(string emplId);
    }
}
