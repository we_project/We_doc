﻿using System;
using Daimler.MicroServices.HRWTAPI.BLL;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daimler.MicroServices.HRWTAPI.Entry.Test {
    [TestClass]
    public class AccountUnitTest {
        [TestMethod]
        public void TestMethod1() {
            bool result = AccountService.ExistClient("yayayahei", "12334");
            Assert.AreEqual(false, result);

            bool mySpaceResult = AccountService.ExistClient("mySpace", "d9ds1+_$##");
            Assert.AreEqual(true, mySpaceResult);
        }
    }
}
