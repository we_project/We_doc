﻿using System;
using System.Collections.Generic;
using Daimler.MicroServices.HRWT.BizObject;
using Daimler.MicroServices.HRWTAPI.BLL;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daimler.MicroServices.HRWTAPI.Entry.Test {
    [TestClass]
    public class EmployeesUnitTest {
        [TestMethod]
        public void TestMethod1() {
            EmployeesSearchCondition search1 = new EmployeesSearchCondition {
                name = null,
                deptId = null
            };
            List<Employee> result1 = EmployeesService.GetEmployees(search1);

            EmployeesSearchCondition search2 = new EmployeesSearchCondition {
                name = "le",
                deptId = null
            };
            List<Employee> result2 = EmployeesService.GetEmployees(search2);

            EmployeesSearchCondition search3 = new EmployeesSearchCondition {
                name = "",
                deptId = null
            };
            List<Employee> result3 = EmployeesService.GetEmployees(search3);

            EmployeesSearchCondition search4 = new EmployeesSearchCondition {
                name = "",
                deptId = "BMBS/AR"
            };
            List<Employee> result4 = EmployeesService.GetEmployees(search4);

            EmployeesSearchCondition search5 = new EmployeesSearchCondition {
                name = null,
                deptId = "GO/CN"
            };
            List<Employee> result5 = EmployeesService.GetEmployees(search5);

            EmployeesSearchCondition search6 = new EmployeesSearchCondition {
                name = null,
                deptId = ""
            };
            List<Employee> result6 = EmployeesService.GetEmployees(search6);
        }
    }
}
