﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Daimler.MicroServices.HRWT.EntryClient.Startup))]
namespace Daimler.MicroServices.HRWT.EntryClient
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
