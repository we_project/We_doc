﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.HRWTAPI.SqlServerDAL {
    public class Config {
        public static string DefaultConnectionString {
            get {
                return ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            }
        }

        public class Procedures {
            public static class cp_hrwt_saveUser {
                public static readonly string name = "cp_hrwt_saveUser";
                public static readonly string hrwtUser = "@hrwtUser";
            }
            public static class cp_hrwt_getEmail {
                public static readonly string name = "cp_hrwt_getEmail";
                public static readonly string smuser = "@smUser";
            }

            public static class cp_hrwt_getEmployees {
                public static readonly string procName = "cp_hrwt_getEmployees";
                public static readonly string @name = "@name";
                public static readonly string @deptId = "@deptId";
                public static readonly string @exectvelvl = "@exectvelvl";
            }
            public static class cp_hrwt_getOrganizedEmployees {
                public static readonly string name = "cp_hrwt_getOrganizedEmployees";
                public static readonly string @supervisor = "@supervisor";
            }
        }
    }
}
