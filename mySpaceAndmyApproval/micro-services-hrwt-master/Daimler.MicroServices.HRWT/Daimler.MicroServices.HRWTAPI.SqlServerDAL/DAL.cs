﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.HRWTAPI.IDAL;

namespace Daimler.MicroServices.HRWTAPI.SqlServerDAL {
    public class DAL {
        private DAL() { }
        private static IHRWT hrwt = new HRWT();
        public static IHRWT DefaultHRWT {
            get {
                return hrwt;
            }
        }
    }
}
