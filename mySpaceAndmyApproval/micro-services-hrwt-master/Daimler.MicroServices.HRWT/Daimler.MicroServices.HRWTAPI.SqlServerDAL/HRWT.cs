﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.HRWT.BizObject;
using Daimler.MicroServices.HRWTAPI.IDAL;
using log4net;

namespace Daimler.MicroServices.HRWTAPI.SqlServerDAL {
    public class HRWT : IHRWT {
        private static readonly ILog log = LogManager.GetLogger(typeof(HRWT));
        DBHelper dbHelper = new DBHelper();
        bool IHRWT.SaveUsers(DataTable data) {
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_hrwt_saveUser.hrwtUser,data)
            };
            int result = dbHelper.Execute(Config.Procedures.cp_hrwt_saveUser.name, sqlParameters, CommandType.StoredProcedure);
            if (result == 0) {
                return false;
            }
            return true;
        }

        string IHRWT.GetEmail(string smUser) {
            SqlParameter[] param ={
                                     new SqlParameter(Config.Procedures.cp_hrwt_getEmail.smuser,smUser)
                                 };
            string result = dbHelper.ExcuteMyScalar(Config.Procedures.cp_hrwt_getEmail.name, param, CommandType.StoredProcedure) as string;
            return result;
        }


        List<Company> IHRWT.GetDepts() {
            List<Company> depts = null;
            string sql = @"select distinct DC_COMPANY_DL,DC_DEPTID_DS from HRWTUser
                            where DC_COMPANY_DL is not null and LTRIM(RTRIM(DC_COMPANY_DL))!=''
                            and DC_DEPTID_DS is not null and LTRIM(RTRIM(DC_DEPTID_DS))!=''";
            DataSet result = dbHelper.ExcuteQuery(sql, null, CommandType.Text);
            if (result != null && result.Tables != null && result.Tables[0].Rows != null && result.Tables[0].Rows.Count > 0) {
                depts = (from row in result.Tables[0].AsEnumerable()
                         group row by row.Field<string>("DC_COMPANY_DL") into g
                         select new Company {
                             company = g.Key,
                             items = (from dept in g
                                      select new Dept {
                                          dept = dept.Field<string>("DC_DEPTID_DS")
                                      }).ToList()
                         }).ToList();
            }
            return depts;
        }


        bool IHRWT.ExistClient(string clientId, string clientSecret) {
            string sql = @"select COUNT(*) from Client where clientId=@clientId and clientSecret=@clientSecret";
            SqlParameter[] sqlParams ={
                                         new SqlParameter("@clientId",clientId),
                                         new SqlParameter("@clientSecret",clientSecret)
                                     };
            object count = dbHelper.ExcuteMyScalar(sql, sqlParams, CommandType.Text);
            bool result = Convert.ToInt32(count) > 0;
            return result;
        }


        List<Employee> IHRWT.GetEmployees(EmployeesSearchCondition search) {
            List<Employee> result = null;
            SqlParameter[] sqlParams ={
                                         new SqlParameter(Config.Procedures.cp_hrwt_getEmployees.name,search.name),
                                         new SqlParameter(Config.Procedures.cp_hrwt_getEmployees.deptId,search.deptId),
                                         new SqlParameter(Config.Procedures.cp_hrwt_getEmployees.exectvelvl,search.exectvelvl)
                                     };

            DataSet data = dbHelper.ExcuteQuery(Config.Procedures.cp_hrwt_getEmployees.procName, sqlParams, CommandType.StoredProcedure);
            if (data != null && data.Tables != null && data.Tables.Count > 0 && data.Tables[0].Rows != null && data.Tables[0].Rows.Count > 0) {
                result = (from row in data.Tables[0].AsEnumerable()
                          select GetEmployeeFrom(row)
                    ).ToList();
            }
            return result;
        }

        private static Employee GetEmployeeFrom(DataRow row) {
            return new Employee {
                NAME_DISPLAY = row.Field<string>("NAME_DISPLAY"),
                DC_DEPTID_DS = row.Field<string>("DC_DEPTID_DS"),
                DC_CELL_PHONE = row.Field<string>("DC_CELL_PHONE"),
                DC_BUSN_PHONE = row.Field<string>("DC_BUSN_PHONE"),
                EMAIL_ADDR = row.Field<string>("EMAIL_ADDR"),
                DC_COMPANY_DL = row.Field<string>("DC_COMPANY_DL"),
                EMPLID = row.Field<string>("EMPLID"),
                NAME = row.Field<string>("NAME"),
                SUPERVISOR_ID = row.Field<string>("SUPERVISOR_ID"),
                DC_LOCATION_DL = row.Field<string>("DC_LOCATION_DL"),
                OPRID = row.Field<string>("OPRID"),
                DC_EMPLMNTTYPE_ID = row.Field<string>("DC_EMPLMNTTYPE_ID"),
                DC_EMPLMNTTYPE_DL = row.Field<string>("DC_EMPLMNTTYPE_DL"),
                DC_APPROVER1 = row.Field<string>("DC_APPROVER1"),
                DC_APPROVER1_NAME = row.Field<string>("DC_APPROVER1_NAME"),
                DC_EXECTVELVL_ID = row.Field<string>("DC_EXECTVELVL_ID"),
                DC_COMPANY_DS = row.Field<string>("DC_COMPANY_DS"),
                DC_COST_CENTR_ID = row.Field<string>("DC_COST_CENTR_ID"),
            };
        }
        private static OrganizedEmployee GetOrganizedEmployeeFrom(DataRow row) {
            OrganizedEmployee result = new OrganizedEmployee {
                NAME_DISPLAY = row.Field<string>("NAME_DISPLAY"),
                DC_DEPTID_DS = row.Field<string>("DC_DEPTID_DS"),
                DC_CELL_PHONE = row.Field<string>("DC_CELL_PHONE"),
                DC_BUSN_PHONE = row.Field<string>("DC_BUSN_PHONE"),
                EMAIL_ADDR = row.Field<string>("EMAIL_ADDR"),
                DC_COMPANY_DL = row.Field<string>("DC_COMPANY_DL"),
                EMPLID = row.Field<string>("EMPLID"),
                NAME = row.Field<string>("NAME"),
                SUPERVISOR_ID = row.Field<string>("SUPERVISOR_ID"),
                DC_LOCATION_DL = row.Field<string>("DC_LOCATION_DL"),
                OPRID = row.Field<string>("OPRID"),
                DC_EMPLMNTTYPE_ID = row.Field<string>("DC_EMPLMNTTYPE_ID"),
                DC_EMPLMNTTYPE_DL = row.Field<string>("DC_EMPLMNTTYPE_DL"),
                DC_APPROVER1 = row.Field<string>("DC_APPROVER1"),
                DC_APPROVER1_NAME = row.Field<string>("DC_APPROVER1_NAME"),
                DC_EXECTVELVL_ID = row.Field<string>("DC_EXECTVELVL_ID"),
                DC_COMPANY_DS = row.Field<string>("DC_COMPANY_DS"),
                DC_COST_CENTR_ID = row.Field<string>("DC_COST_CENTR_ID")
            };
            return result;

        }


        //private static OrganizedEmployee GetOrganizedEmployee(IEnumerable<DataRow> table, string supervisor) {
        //    DataRow row = table.FirstOrDefault(r => supervisor.Equals(r.Field<string>("EMPLID"), StringComparison.CurrentCultureIgnoreCase));
        //    OrganizedEmployee result = GetOrganizedEmployeeFrom(row);
        //    result.subordinates = (from r in table
        //                           where supervisor.Equals(r.Field<string>("SUPERVISOR_ID"), StringComparison.CurrentCultureIgnoreCase)
        //                           select GetOrganizedEmployee(table, r.Field<string>("EMPLID"))
        //                             ).ToList();
        //    return result;
        //}

        private static OrganizedEmployee GetOrganizedEmployee(IEnumerable<DataRow> table, string supervisor) {
            IList<OrganizedEmployee> flatted = table.Select(r => GetOrganizedEmployeeFrom(r)).ToList();
            IList<string> supervisors = flatted.Select(f => f.SUPERVISOR_ID).Distinct().ToList();
            IList<OrganizedEmployee> hasChildren = flatted.Where(foe => supervisors.Contains(foe.EMPLID)).ToList();
            foreach (OrganizedEmployee oe in flatted) {
                OrganizedEmployee parent = hasChildren.FirstOrDefault(foe => oe.SUPERVISOR_ID.Equals(foe.EMPLID));
                if (parent != null) {
                    parent.subordinates = parent.subordinates ?? new List<OrganizedEmployee>();
                    parent.subordinates.Add(oe);
                }
            };

            OrganizedEmployee root = flatted.FirstOrDefault(oe => oe.EMPLID.Equals(supervisor));
            return root;
        }

        OrganizedEmployee IHRWT.GetOrganizedEmployee(string emplId) {
            OrganizedEmployee result = null;
            SqlParameter[] sqlParams ={
                                         new SqlParameter(Config.Procedures.cp_hrwt_getOrganizedEmployees.supervisor,emplId)
                                     };
            log.Debug("GetOrganizedEmployee Sql Begin");
            DataSet data = dbHelper.ExcuteQuery(Config.Procedures.cp_hrwt_getOrganizedEmployees.name, sqlParams, CommandType.StoredProcedure);
            log.Debug("GetOrganizedEmployee Sql End");
            if (data != null && data.Tables != null && data.Tables.Count > 0 && data.Tables[0].Rows != null && data.Tables[0].Rows.Count > 0) {
                log.Debug("Recursive data Begin");
                result = GetOrganizedEmployee(data.Tables[0].AsEnumerable(), emplId);
                log.Debug("Recursive data End");
            }
            return result;
        }
    }
}
