﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.HRWT.BizObject;
using Daimler.MicroServices.Utils.Network;
using log4net;

namespace Daimler.MicroServices.HRWT.BLL {
    public class DepartmentsService {
        private static readonly ILog log = LogManager.GetLogger(typeof(DepartmentsService));
        public static async Task<List<Company>> GetDeptsAsync() {
            string url = string.Format("{0}/api/Departments", Config.ApiUrl);
            ReturnDepartments depts = await HttpHelper_Json.GetJsonAsync<ReturnDepartments>(url);
            if (depts.success) {
                return depts.depts;
            } else {
                log.ErrorFormat("Get Depratments Error: {0}", depts.msg);
            }
            return null;
        }
    }
}
