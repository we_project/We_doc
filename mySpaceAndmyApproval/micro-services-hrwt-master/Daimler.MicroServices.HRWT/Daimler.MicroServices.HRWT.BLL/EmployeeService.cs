﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.HRWT.BizObject;
using Daimler.MicroServices.Utils.Network;

namespace Daimler.MicroServices.HRWT.BLL {
    public class EmployeeService {
        public static async Task<string> GetEmailAsync(string smUser) {
            string url = string.Format("{0}/api/Employee/{1}/Email", Config.ApiUrl, smUser);
            ReturnEmail email = await HttpHelper_Json.GetJsonAsync<ReturnEmail>(url);
            if (email.success) {
                return email.email;
            }
            return null;
        }
    }
}
