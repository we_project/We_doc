﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.HRWT.BizObject;
using Daimler.MicroServices.Utils.Network;

namespace Daimler.MicroServices.HRWT.BLL {
    public class OrganizedEmployeeService {
        public static async Task<OrganizedEmployee> GetOrganizedEmployeeAsync(string emplId) {
            string url = string.Format("{0}/api/OrganizedEmployee?emplId={1}", Config.ApiUrl, emplId);
            ReturnOrganizedEmployee result = await HttpHelper_Json.GetJsonAsync<ReturnOrganizedEmployee>(url);
            if (result.success) {
                return result.employee;
            }
            return null;
        }
    }
}
