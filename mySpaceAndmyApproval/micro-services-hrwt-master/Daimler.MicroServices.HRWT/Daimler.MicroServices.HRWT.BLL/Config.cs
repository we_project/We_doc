﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace Daimler.MicroServices.HRWT.BLL {
    class Config {
        private static readonly string apiUrl = ConfigurationManager.AppSettings["dataApiBase"];
        public static string ApiUrl {
            get {
                return apiUrl;
            }
        }
    }
}
