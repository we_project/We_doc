﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.HRWT.BizObject;
using log4net;
using Daimler.MicroServices.Utils.Network;

namespace Daimler.MicroServices.HRWT.BLL {
    public class AccountService {
        private static readonly ILog log = LogManager.GetLogger(typeof(AccountService));
        public static async Task<bool> ClientExist(ClientAccount account) {
            string url = string.Format("{0}/api/Account", Config.ApiUrl);
            ReturnBoolResult apiResult = await HttpHelper_Json.PostJsonAsync<ReturnBoolResult, ClientAccount>(url, account);
            if (apiResult.success && apiResult.boolResult) {
                return true;
            }
            return false;
        }
    }
}
