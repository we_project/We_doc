﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.HRWT.BizObject;
using Daimler.MicroServices.Utils.Network;

namespace Daimler.MicroServices.HRWT.BLL {
    public class EmployeesService {
        public static async Task<List<Employee>> GetEmployeesAsync(EmployeesSearchCondition search) {
            string url = string.Format("{0}/api/Employees?name={1}&deptId={2}&exectvelvl={3}", Config.ApiUrl, search.name, search.deptId,search.exectvelvl);
            ReturnEmployees result = await HttpHelper_Json.GetJsonAsync<ReturnEmployees>(url);
            if (result.success) {
                return result.employees;
            }
            return null;
        }
    }
}
