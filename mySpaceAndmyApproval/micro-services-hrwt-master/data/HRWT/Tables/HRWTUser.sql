--drop table HRWTUser;
create table HRWTUser(
H nvarchar(100) null,
NAME_DISPLAY nvarchar(100) null,
DC_DEPTID_DS nvarchar(100) null,
DC_CELL_PHONE nvarchar(100) null,
DC_BUSN_PHONE nvarchar(100) null,
EMAIL_ADDR nvarchar(100) null,
DC_COMPANY_DL nvarchar(100) null,
EMPLID nvarchar(100) null,
NAME nvarchar(100) null,
SUPERVISOR_ID nvarchar(100) null,
DC_LOCATION_DL nvarchar(100) null,
OPRID nvarchar(100) null,
DC_EMPLMNTTYPE_ID nvarchar(100) null,
DC_EMPLMNTTYPE_DL nvarchar(100) null,
DC_APPROVER1 nvarchar(100) null,
DC_APPROVER1_NAME nvarchar(100) null,
DC_EXECTVELVL_ID nvarchar(100) null,
DC_COMPANY_DS nvarchar(100) null,
DC_COST_CENTR_ID nvarchar(100) null,
IsEnable bit null
)

create index Index_HRWTUser_Name on HRWTUser(NAME_DISPLAY,NAME,OPRID)
create index Index_HRWTUser_Dept on HRWTUser(DC_DEPTID_DS)
create index Index_HRWTUser_Empl on HRWTUser(EMPLID)
create index Index_HRWTUser_Supervisor on HRWTUser(SUPERVISOR_ID)
create index Index_HRWTUser_Oprid on HRWTUser(OPRID)
create index Index_HRWTUser_Isenabled on HRWTUser(IsEnable)

alter table HRWTUser add DC_EXECTVELVL_ID nvarchar(100) null;
alter table HRWTUser add DC_COMPANY_DS nvarchar(100) null;
alter table HRWTUser add DC_COST_CENTR_ID nvarchar(100) null;
create index Index_HRWTUser_Exectvelvl on HRWTUser(DC_EXECTVELVL_ID);
