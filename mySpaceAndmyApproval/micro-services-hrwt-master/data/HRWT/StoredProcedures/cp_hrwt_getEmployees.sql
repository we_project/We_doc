alter proc cp_hrwt_getEmployees(
@name nvarchar(100)=null,
@deptId nvarchar(100)=null,
@exectvelvl nvarchar(100)=null
)
as
begin
	select * from HRWTUser 
	where 
	IsEnable=1
	and (
		coalesce(@deptId,'') = ''
		or DC_DEPTID_DS=@deptId)
	and (
		coalesce(@name,'')=''
		or
		(NAME_DISPLAY like '%'+@name+'%' or OPRID like '%'+@name+'%'))
	and (
		coalesce(@exectvelvl,'')='' 
		or  DC_EXECTVELVL_ID like '%'+@exectvelvl+'%');
end