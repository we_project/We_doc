# Fiori token interface

## Post Token
Get oauth2 token.

#### Request Information
##### Header Parameters
| Name        | Description           | Type  |Additional information
| :------------ |:-------------:| -----:| ----------:|
| Content-Type      |   |   |application/x-www-form-urlencoded|
| Authorization     |   |   |Basic base64\(\{clientId:clientSecret\}\)|


##### Body Parameters
    grant_type=client_credentials

#### Response Information
##### Response Formats
  
    {
	    "access_token": "wo5ajiIXRBWNRbxMj6FFgL8lvEJmNDJMF-OHi3FmI3c2oM17qH9RcEw-qvL6XChxDQ5SC5AwUGwRKyZeH-rs3ngRVoo_AMDiMjJE3KCHe9gi2VjnRDiEzzO2Ga8H0xCeUy0ih0uL-65ruyOMKGUAbGmINQvzuHyKxYHtpVZ2v_HO3NVM1-RJ6Q51DhDryp2mX-i1wA5x80DImzOBpOkchMxnNqOf2Nsw07twRc62uY4",
	    "token_type": "bearer",
	    "expires_in": 1209599,
	    ".issued": "Mon, 13 Nov 2017 02:58:17 GMT",
	    ".expires": "Mon, 27 Nov 2017 02:58:17 GMT"
	}

#### Note

The response access_token need to be post as header on other api request.

```
GET /fiori/api/Fiori/UrlsForCount HTTP/1.1
Host: anywhere.i.daimler.com
Content-Type: application/json
Authorization: Bearer lLcnG9mstUeG8yezyfuLYpqvmcpC24OThIl-FUn0U3RhxJAGTY5131mgmXScYlbCnOylWeAgwoVYNjSeK2PmpHOHzkInBLc9oDv_nN3kGnSL8e6J8G0srQjnCqWZawUO3Xa7YfLO9V-xcatQ09oO7gL8_uYdah25UYE6qBaGSXXDnJFQhYsYijGEZp2HS1OP-uPSafvd1Snv_4eF0lAH5a7FvuzIdYwOTKZ38e6zrmAw_pWiwCLh0t3rMBNeLEngCujOHM1m7dHtuKMKhFaWs_Nd6ECmNR-UOZssu4LKnqSVoNeSvBYkyNP0VdR7fw95RTu5NURSp9ggRjYESJpXBz24laL1jNbadksPWvRWh7vrUmwy1_HtOxNYNEJpVRNMp2mv0fFrKZpzXze39cvTH4wGvPaIoPs3ZuGiyhrIzxBFxI88FF6EDh7oAPHeaZNowws8BDhcZeJLz7FswQQZnA
Cache-Control: no-cache
```