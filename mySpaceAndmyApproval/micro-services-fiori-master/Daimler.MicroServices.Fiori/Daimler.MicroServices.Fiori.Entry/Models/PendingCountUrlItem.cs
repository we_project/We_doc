﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Daimler.MicroServices.Fiori.Entry.Models {
    public class PendingCountUrlItem {
        public string Application { get; set; }
        public string URL { get; set; }
    }
}