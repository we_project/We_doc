﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Daimler.MicroServices.Fiori.Entry.Models {
    public class ClientAccount {
        public string clientId { get; set; }
        public string clientSecret { get; set; }
    }
}