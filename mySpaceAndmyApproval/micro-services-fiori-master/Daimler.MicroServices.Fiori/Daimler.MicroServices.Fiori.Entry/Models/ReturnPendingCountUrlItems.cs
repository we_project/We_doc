﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Daimler.MicroServices.Fiori.Entry.Models {
    public class ReturnPendingCountUrlItems :BaseReturn{
        public PendingCountUrlItem[] list { get; set; }
    }
}