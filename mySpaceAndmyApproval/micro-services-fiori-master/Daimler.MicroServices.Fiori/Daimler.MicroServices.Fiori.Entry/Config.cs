﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Daimler.MicroServices.Fiori.Entry {
    public class Config {

        private static readonly string clients = ConfigurationManager.AppSettings["clients"];
        private static Dictionary<string, string> _clients;
        public static Dictionary<string, string> Clients {
            get {
                if (_clients == null) {
                    _clients = clients.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                        .Select(part => part.Split(':'))
                        .Where(kv => kv != null && kv.Length == 2)
                        .ToDictionary(kv => kv[0], kv => kv[1]);
                }
                return _clients;
            }
        }
    }
}