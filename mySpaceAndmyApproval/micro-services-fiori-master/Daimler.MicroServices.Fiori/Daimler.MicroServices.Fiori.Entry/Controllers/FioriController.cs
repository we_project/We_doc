﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using Daimler.MicroServices.Fiori.Entry.Models;
using log4net;
using Newtonsoft.Json;

namespace Daimler.MicroServices.Fiori.Entry.Controllers {
    [Authorize]
    [RoutePrefix("api/Fiori")]
    public class FioriController : ApiController {
        private static readonly ILog log = LogManager.GetLogger(typeof(FioriController));

        [Route("UrlsForCount")]
        [HttpGet]
        public ReturnPendingCountUrlItems GetUrlsForCount() {
            ReturnPendingCountUrlItems result = new ReturnPendingCountUrlItems();
            try {
                var json = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "App_Data", "PendingCountUrlItems.json"));
                var list = JsonConvert.DeserializeObject<PendingCountUrlItem[]>(json);
                result.list = list;
                result.success = true;
            } catch (Exception ex) {
                log.Error(Request.RequestUri, ex);
                result.msg = ReturnMsg.ExceptionOccurs;
            }
            return result;
        }
    }
}
