﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Daimler.MicroServices.Fiori.Entry.Models;
using log4net;

namespace Daimler.MicroServices.Fiori.Entry.BLL {
    public class AccountService {
        private static readonly ILog log = LogManager.GetLogger(typeof(AccountService));
        public static bool ClientExist(ClientAccount account) {
            if (Config.Clients.Contains(new KeyValuePair<string, string>(account.clientId, account.clientSecret))) {
                return true;
            }
            return false;
        }
    }
}