﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace Daimler.MicroServices.Utils.Cache {
    public class CacheHelper {
        private static readonly ILog log = LogManager.GetLogger(typeof(CacheHelper));
        static ObjectCache cache = MemoryCache.Default;
        public static object Get(string key) {
            object result = null;
            if (cache.Contains(key)) {
                result = cache.Get(key);
            }
            return result;
        }
        [MethodImpl(MethodImplOptions.Synchronized)]
        public static bool Set(string key, object value, CacheItemPolicy policy = null) {
            bool result = false;
            if (policy == null) {
                policy = new CacheItemPolicy();
            }
            if (cache.Contains(key)) {
                if (value != null) {
                    cache.Set(key, value, policy);
                } else {
                    cache.Remove(key);
                }
            } else {
                if (value != null) {
                    cache.Add(key, value, policy);
                }
            }
            result = true;
            return result;
        }
        public static bool Contains(string key) {
            bool result = false;
            if (cache.Contains(key)) {
                result = true;
            }
            return result;
        }
        [MethodImpl(MethodImplOptions.Synchronized)]
        public static bool Remove(string key) {
            bool result = false;
            try {
                if (cache.Contains(key)) {
                    cache.Remove(key);
                }
                result = true;
            } catch (Exception ex) {
                log.Error(new {
                    action = "Remove",
                    key = key
                }, ex);
            }
            return result;
        }
    }
}
