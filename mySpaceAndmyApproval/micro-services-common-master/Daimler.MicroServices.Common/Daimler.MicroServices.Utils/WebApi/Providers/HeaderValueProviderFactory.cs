﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.ValueProviders;

namespace Daimler.MicroServices.Utils.WebApi.Filters {
    /// <summary>
    /// add attribute for webapi parameters:
    ///     eg. [ValueProvider(typeof(HeaderValueProviderFactory))]string SM_USER
    /// </summary>
    public class HeaderValueProviderFactory : ValueProviderFactory {
        public class HeaderValueProvider : IValueProvider {
            private readonly HttpRequestHeaders _headers;

            public HeaderValueProvider(HttpActionContext actionContext) {
                _headers = actionContext.ControllerContext.Request.Headers;
            }

            public bool ContainsPrefix(string prefix) {
                return _headers.Any(x => x.Key.Contains(prefix));
            }

            public ValueProviderResult GetValue(string key) {
                if (_headers == null) {
                    return null;
                }
                IEnumerable<string> values;
                if (_headers.TryGetValues(key, out values)) {
                    if (values != null && values.Count() > 0) {
                        var val = values.FirstOrDefault();
                        return new ValueProviderResult(val, val, CultureInfo.CurrentCulture);
                    }
                }
                return null;
            }
        }



        public override IValueProvider GetValueProvider(HttpActionContext actionContext) {
            return new HeaderValueProvider(actionContext);
        }
    }
}