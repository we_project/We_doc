﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using log4net;

namespace Daimler.MicroServices.Utils.WebApi.MessageHandlers {
    public class LogHandler : DelegatingHandler {
        private static readonly ILog log = LogManager.GetLogger("Request");
        protected async override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken) {
            log.DebugFormat("{0}\n{1}", request.RequestUri, request.Headers);
            HttpResponseMessage response = await base.SendAsync(request, cancellationToken);
            return response;
        }
    }
}
