﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Filters;
using log4net;

namespace Daimler.MicroServices.Utils.WebApi.Filters {
    public class LogActionFilter : ActionFilterAttribute {
        private static readonly ILog log = LogManager.GetLogger("Request");
        public override System.Threading.Tasks.Task OnActionExecutingAsync(System.Web.Http.Controllers.HttpActionContext actionContext, System.Threading.CancellationToken cancellationToken) {
            var id = actionContext.RequestContext.Principal.Identity;
            log.DebugFormat("{2}\n{0}\n{1}", actionContext.Request.RequestUri, actionContext.Request.Headers, id.IsAuthenticated ? id.Name : "");
            return base.OnActionExecutingAsync(actionContext, cancellationToken);
        }
    }
}
