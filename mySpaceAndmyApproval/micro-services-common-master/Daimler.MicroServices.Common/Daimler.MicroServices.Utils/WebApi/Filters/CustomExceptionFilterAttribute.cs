﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Filters;
using Daimler.MicroServices.Utils.Network;
using log4net;
using Newtonsoft.Json;

namespace Daimler.MicroServices.Utils.WebApi.Filters {
    public class CustomExceptionFilterAttribute : ExceptionFilterAttribute {
        private static readonly ILog log = LogManager.GetLogger("GlobalException");

        public override void OnException(HttpActionExecutedContext actionExecutedContext) {
            log.Error(actionExecutedContext, actionExecutedContext.Exception);
            actionExecutedContext.Response = new HttpResponseMessage(HttpStatusCode.OK);
            actionExecutedContext.Response.Content = new StringContent(JsonConvert.SerializeObject(BaseReturn.Exception), Encoding.UTF8, mediaType: MediaTypes.json);
        }
    }
}
