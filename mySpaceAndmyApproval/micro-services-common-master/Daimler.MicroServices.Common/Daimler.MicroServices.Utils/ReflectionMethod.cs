﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.Utils {
    public class ReflectionMethod {
        public static List<string> GetPropertyNames<T>() {
            return typeof(T).GetProperties().Select(p => p.Name).ToList();
        }
    }
}
