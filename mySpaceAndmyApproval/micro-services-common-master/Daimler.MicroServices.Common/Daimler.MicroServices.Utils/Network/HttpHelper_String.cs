﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace Daimler.MicroServices.Utils.Network {
    public class HttpHelper_String {
        private static readonly ILog log = LogManager.GetLogger(typeof(HttpHelper_String));
        public static string GetString(string address) {
            using (HttpClientHandler handler = new HttpClientHandler {
                UseProxy = false
            })
            using (HttpClient client = new HttpClient(handler))
            using (HttpResponseMessage response = client.GetAsync(address).Result) {
                string str = response.Content.ReadAsStringAsync().Result;
                log.Debug(str);
                if (response.IsSuccessStatusCode) {
                    return str;
                } else {
                    return null;
                }
            }
        }

        public static async Task<string> GetStringAsync(string address) {
            using (HttpClientHandler handler = new HttpClientHandler {
                UseProxy = false
            })
            using (HttpClient client = new HttpClient(handler))
            using (HttpResponseMessage response = await client.GetAsync(address)) {
                string str = await response.Content.ReadAsStringAsync();
                log.Debug(str);
                if (response.IsSuccessStatusCode) {
                    return str;
                } else {
                    return null;
                }
            }
        }
    }
}
