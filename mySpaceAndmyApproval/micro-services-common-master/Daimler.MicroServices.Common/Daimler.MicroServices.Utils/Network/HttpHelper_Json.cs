﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using log4net;
using Newtonsoft.Json;

namespace Daimler.MicroServices.Utils.Network {
    public class HttpHelper_Json {
        static HttpHelper_Json() {
            //System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3;
            //ServicePointManager.CheckCertificateRevocationList = false;
            //ServicePointManager.ServerCertificateValidationCallback = (s, certificate, chain, sslPolicyErrors) => {
            //    log.Debug("ServerCertificateValidationCallback at static HttpHelper_Json occurs");
            //    log.Debug(sslPolicyErrors);
            //    return true;
            //};
            //System.Net.ServicePointManager.CertificatePolicy = new MyPolicy();
        }



        public class MyPolicy : ICertificatePolicy {
            public bool CheckValidationResult(ServicePoint srvPoint, X509Certificate certificate, WebRequest request,
          int certificateProblem) {
                //Return True to force the certificate to be accepted.
                return true;
            }
        }
        private static readonly ILog log = LogManager.GetLogger(typeof(HttpHelper_Json));
        #region Json
        public static T PostJson<T, M>(string address, M post) {
            string postJson = JsonConvert.SerializeObject(post);
            using (HttpClientHandler handler = new HttpClientHandler {
                UseProxy = false,
            })
            using (HttpClient client = new HttpClient(handler))
            using (HttpContent content = new StringContent(postJson, Encoding.UTF8, MediaTypes.json))
            using (HttpResponseMessage response = client.PostAsync(address, content).Result) {
                string str = response.Content.ReadAsStringAsync().Result;
                log.Debug(str);
                T responseStr = JsonConvert.DeserializeObject<T>(str);
                return responseStr;
            }
        }
        public static async Task<T> PostJsonAsync<T, M>(string address, M post) {
            string postJson = JsonConvert.SerializeObject(post);
            using (HttpClientHandler handler = new HttpClientHandler {
                UseProxy = false
            })
            using (HttpClient client = new HttpClient(handler))
            using (HttpContent content = new StringContent(postJson, Encoding.UTF8, MediaTypes.json))
            using (HttpResponseMessage response = await client.PostAsync(address, content)) {
                string str = await response.Content.ReadAsStringAsync();
                log.Debug(str);
                T responseStr = JsonConvert.DeserializeObject<T>(str);
                return responseStr;
            }
        }

        public static T PostJson<T, M>(string address, M post, CredentialCache c, string FormDigest, bool useProxy = false, WebProxy proxy = null) {
            string postJson = JsonConvert.SerializeObject(post);
            log.Debug(postJson);
            using (WebRequestHandler handler = new WebRequestHandler {
            }) {

                handler.Credentials = c;
                handler.UseDefaultCredentials = false;
                handler.PreAuthenticate = true;
                handler.UseProxy = useProxy;
                handler.ClientCertificateOptions = ClientCertificateOption.Automatic;
                X509Certificate value = X509Certificate.CreateFromCertFile(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "sharepoint.cer"));

                handler.ClientCertificates.Add(value);
                if (useProxy) {
                    handler.Proxy = proxy;
                }
                using (HttpClient client = new HttpClient(handler)) {
                    client.DefaultRequestHeaders.Add("Accept", "application/json;odata=verbose");
                    client.DefaultRequestHeaders.Add("X-HTTP-Method", "MERGE");
                    client.DefaultRequestHeaders.Add("IF-MATCH", "*");
                    client.DefaultRequestHeaders.Add("X-RequestDigest", FormDigest);

                    using (HttpContent content = new StringContent(postJson, Encoding.UTF8)) {
                        {
                            content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json;odata=verbose");
                            using (HttpResponseMessage response = client.PostAsync(address, content).Result) {
                                string str = response.Content.ReadAsStringAsync().Result;
                                log.Debug(str);
                                if (response.IsSuccessStatusCode) {
                                    if (typeof(T).Equals(typeof(string))) {
                                        return (T)Convert.ChangeType(str, typeof(T));
                                    }
                                    T responseStr = JsonConvert.DeserializeObject<T>(str);
                                    return responseStr;
                                } else {
                                    throw new Exception(str);
                                }
                            }
                        }

                    }
                }


            }
        }
        public static T GetJson<T>(string address) {
            log.Debug(address);
            using (HttpClientHandler handler = new HttpClientHandler {
                UseProxy = false
            })
            using (HttpClient client = new HttpClient(handler))
            using (HttpResponseMessage response = client.GetAsync(address).Result) {
                if (response.IsSuccessStatusCode) {
                    string str = response.Content.ReadAsStringAsync().Result;
                    log.Debug(str);
                    T responseStr = JsonConvert.DeserializeObject<T>(str);
                    return responseStr;
                } else {
                    string responseStr = response.Content.ReadAsStringAsync().Result;
                    log.Debug(responseStr);
                    return default(T);
                }

            }
        }
        public static bool AcceptAllCertifications(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certification, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors) {
            return true;
        }
        public static T GetJson<T>(string address, CredentialCache c, bool useProxy = false, WebProxy proxy = null) {
            //ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);

            log.Debug(address);
            //ServicePointManager.ServerCertificateValidationCallback = delegate {
            //    log.Debug("ServerCertificateValidationCallback occurs");
            //    return true;
            //};    
            ServicePointManager.CheckCertificateRevocationList = false;


            using (WebRequestHandler handler = new WebRequestHandler {

            }) {
                //handler.ClientCertificateOption
                //handler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => { return true; };
                handler.Credentials = c;

                handler.UseDefaultCredentials = false;
                handler.PreAuthenticate = true;
                handler.AuthenticationLevel = System.Net.Security.AuthenticationLevel.None;

                handler.UseProxy = useProxy;
                //handler.ServerCertificateValidationCallback = (s, certificate, chain, sslPolicyErrors) => {
                //    log.Debug("ServerCertificateValidationCallback at static HttpHelper_Json occurs");
                //    log.Debug(sslPolicyErrors);
                //    return true;
                //};

                // to ignore SSL certificate errors
                //ServicePointManager.Expect100Continue = false;
                //string filename = AppDomain.CurrentDomain.BaseDirectory + "\\sharepoint.cer";
                //X509Certificate value = X509Certificate.CreateFromCertFile(filename);
                //handler.ClientCertificates.Add(value);
                if (useProxy) {
                    handler.Proxy = proxy;
                }

                using (HttpClient client = new HttpClient(handler)) {

                    client.BaseAddress = new Uri(address);
                    client.DefaultRequestHeaders.Add("Accept", "application/json;odata=verbose");
                    var requestMessage = new HttpRequestMessage {
                        Version = HttpVersion.Version10
                    };
                    using (HttpResponseMessage response = client.SendAsync(requestMessage).Result) {
                        if (response.IsSuccessStatusCode) {
                            string str = response.Content.ReadAsStringAsync().Result;
                            log.Debug(str);
                            T responseStr = JsonConvert.DeserializeObject<T>(str);
                            return responseStr;
                        } else {
                            string responseStr = response.Content.ReadAsStringAsync().Result;
                            log.Debug(responseStr);
                            return default(T);
                        }
                    }
                }

            }
        }
        //public static T GetJson<T>(string address, CredentialCache c, bool useProxy = false, WebProxy proxy = null) {
        //    log.Debug(address);
        //    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(address);

        //    request.Credentials = c;
        //    request.UseDefaultCredentials = false;
        //    request.PreAuthenticate = false;
        //    request.Proxy = useProxy ? proxy : null;
        //    request.Accept = "application/json;odata=verbose";
        //    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        //    // Get the stream associated with the response.

        //    // Pipes the stream to a higher level stream reader with the required encoding format. 
        //    using (Stream receiveStream = response.GetResponseStream()) {
        //        using (StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8)) {
        //            string sOutput = readStream.ReadToEnd();
        //            if (response.StatusCode == HttpStatusCode.OK) {
        //                log.Debug(sOutput);
        //                T responseStr = JsonConvert.DeserializeObject<T>(sOutput);
        //                return responseStr;
        //            } else {
        //                log.Debug(sOutput);
        //                return default(T);
        //            }
        //        };

        //    };

        //}
        public static async Task<T> GetJsonAsync<T>(string address) {
            using (HttpClientHandler handler = new HttpClientHandler {
                UseProxy = false
            })
            using (HttpClient client = new HttpClient(handler))
            using (HttpResponseMessage response = await client.GetAsync(address)) {
                if (response.IsSuccessStatusCode) {
                    string str = await response.Content.ReadAsStringAsync();
                    log.Debug(str);
                    T responseStr = JsonConvert.DeserializeObject<T>(str);
                    return responseStr;
                } else {
                    string responseStr = await response.Content.ReadAsStringAsync();
                    log.Debug(responseStr);
                    return default(T);
                }
            }
        }



        #endregion
    }
}
