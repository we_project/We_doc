﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.Utils.Network {
    public class RequestValueExtractor {
        public static string GetHead(HttpRequestMessage request, string key) {
            string result = null;
            IEnumerable<string> tmp;
            if (request.Headers.TryGetValues(key, out tmp) && tmp != null && tmp.Count() > 0) {
                result = tmp.FirstOrDefault();
            }
            return result;
        }

        public static string GetCookie(HttpRequestMessage request, string key) {
            CookieHeaderValue cookie = request.Headers.GetCookies(key).FirstOrDefault();
            if (cookie != null) {
                return cookie[key].Value;
            }
            return null;
        }
    }
}
