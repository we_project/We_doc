﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.Text;
using System.Threading.Tasks;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Daimler.MicroServices.Utils.Network.Sharepoint {
    public class SPHttpHelper_Json {
        private static readonly ILog log = LogManager.GetLogger(typeof(SPHttpHelper_Json));
        public static IEnumerable<T> GetJson<T>(string address, CredentialCache c) {
            log.Debug(new {
                action = "SPHttpHelper_Json.GetJson<T>",
                address = address
            });

            ServicePointManager.CheckCertificateRevocationList = false;
            using (WebRequestHandler handler = new WebRequestHandler {
                Credentials = c,
                UseDefaultCredentials = false,
                PreAuthenticate = true,
                AuthenticationLevel = AuthenticationLevel.None
            }) {
                using (HttpClient client = new HttpClient(handler)) {
                    client.BaseAddress = new Uri(address);
                    client.DefaultRequestHeaders.Add("Accept", "application/json;odata=verbose");
                    var requestMessage = new HttpRequestMessage {
                    };
                    using (HttpResponseMessage response = client.SendAsync(requestMessage).Result) {
                        string str = response.Content.ReadAsStringAsync().Result;
                        if (response.IsSuccessStatusCode) {
                            log.Debug(new {
                                raw = str
                            });
                            JObject jObj = JObject.Parse(str);
                            IList<JToken> results = jObj["d"]["results"].Children().ToList();
                            if (results != null && results.Count > 0) {
                                return results.Select(j => j.ToObject<T>());
                            }
                        } else {
                            log.Debug(new {
                                success = false,
                                code = response.StatusCode,
                                responseStr = str
                            });
                        }
                        return null;
                    }
                }

            }
        }
        public static async Task<IEnumerable<T>> GetJsonAsync<T>(string address, CredentialCache c) {
            log.Debug(new {
                action = "SPHttpHelper_Json.GetJson<T>",
                address = address
            });

            ServicePointManager.CheckCertificateRevocationList = false;
            using (WebRequestHandler handler = new WebRequestHandler {
                Credentials = c,
                UseDefaultCredentials = false,
                PreAuthenticate = true,
                AuthenticationLevel = AuthenticationLevel.None
            }) {
                using (HttpClient client = new HttpClient(handler)) {
                    client.BaseAddress = new Uri(address);
                    client.DefaultRequestHeaders.Add("Accept", "application/json;odata=verbose");
                    var requestMessage = new HttpRequestMessage {
                    };
                    using (HttpResponseMessage response = client.SendAsync(requestMessage).Result) {
                        string str = await response.Content.ReadAsStringAsync();
                        if (response.IsSuccessStatusCode) {
                            log.Debug(new {
                                raw = str
                            });
                            JObject jObj = JObject.Parse(str);
                            IList<JToken> results = jObj["d"]["results"].Children().ToList();
                            if (results != null && results.Count > 0) {
                                return results.Select(j => j.ToObject<T>());
                            }
                        } else {
                            log.Debug(new {
                                success = false,
                                code = response.StatusCode,
                                responseStr = str
                            });
                        }
                        return null;
                    }
                }

            }
        }

        public static async Task<IEnumerable<T>> GetAllItemsJsonAsync<T>(string address, CredentialCache c) {
            log.Debug(new {
                action = "SPHttpHelper_Json.GetJson<T>",
                address = address
            });

            ServicePointManager.CheckCertificateRevocationList = false;
            using (WebRequestHandler handler = new WebRequestHandler {
                Credentials = c,
                UseDefaultCredentials = false,
                PreAuthenticate = true,
                AuthenticationLevel = AuthenticationLevel.None
            }) {
                using (HttpClient client = new HttpClient(handler)) {
                    client.BaseAddress = new Uri(address);
                    client.DefaultRequestHeaders.Add("Accept", "application/json;odata=verbose");
                    var requestMessage = new HttpRequestMessage {
                    };
                    using (HttpResponseMessage response = client.SendAsync(requestMessage).Result) {
                        string str = await response.Content.ReadAsStringAsync();
                        if (response.IsSuccessStatusCode) {
                            log.Debug(new {
                                raw = str
                            });
                            JObject jObj = JObject.Parse(str);
                            List<T> r = new List<T>();
                            IList<JToken> results = jObj["d"]["results"].Children().ToList();
                            if (results != null && results.Count > 0) {
                                r.AddRange(results.Select(j => j.ToObject<T>()));
                            }

                            JToken __next = jObj["d"]["__next"];
                            string __next_ = null;
                            if (__next != null) {
                                __next_ = __next.ToString();
                                var next = await GetAllItemsJsonAsync<T>(__next_, c);
                                if (next == null || next.Count() == 0) {
                                    return null;
                                } else {
                                    r.AddRange(next);
                                }
                            }
                            return r;
                        } else {
                            log.Debug(new {
                                success = false,
                                code = response.StatusCode,
                                responseStr = str
                            });
                        }
                        return null;
                    }
                }

            }
        }

        #region T
        public static T GetJsonObj<T>(string address, CredentialCache c) {
            log.Debug(new {
                action = "SPHttpHelper_Json.GetJsonObj<T>",
                address = address
            });
            ServicePointManager.CheckCertificateRevocationList = false;
            using (WebRequestHandler handler = new WebRequestHandler {
                Credentials = c,
                UseDefaultCredentials = false,
                PreAuthenticate = true,
                AuthenticationLevel = AuthenticationLevel.None
            }) {
                using (HttpClient client = new HttpClient(handler)) {
                    client.BaseAddress = new Uri(address);
                    client.DefaultRequestHeaders.Add("Accept", "application/json;odata=verbose");
                    var requestMessage = new HttpRequestMessage {
                    };
                    using (HttpResponseMessage response = client.SendAsync(requestMessage).Result) {
                        string str = response.Content.ReadAsStringAsync().Result;
                        if (response.IsSuccessStatusCode) {
                            log.Debug(new {
                                raw = str
                            });
                            JObject jObj = JObject.Parse(str);
                            JToken result = jObj["d"];
                            return result.ToObject<T>();
                        } else {
                            log.Debug(new {
                                success = false,
                                code = response.StatusCode,
                                responseStr = str
                            });
                        }
                        return default(T);
                    }
                }
            }
        }
        public static async Task<T> GetJsonObjAsync<T>(string address, CredentialCache c) {
            log.Debug(new {
                action = "SPHttpHelper_Json.GetJsonObj<T>",
                address = address
            });
            ServicePointManager.CheckCertificateRevocationList = false;
            using (WebRequestHandler handler = new WebRequestHandler {
                Credentials = c,
                UseDefaultCredentials = false,
                PreAuthenticate = true,
                AuthenticationLevel = AuthenticationLevel.None
            }) {
                using (HttpClient client = new HttpClient(handler)) {
                    client.BaseAddress = new Uri(address);
                    client.DefaultRequestHeaders.Add("Accept", "application/json;odata=verbose");
                    var requestMessage = new HttpRequestMessage {
                    };
                    using (HttpResponseMessage response = client.SendAsync(requestMessage).Result) {
                        string str = await response.Content.ReadAsStringAsync();
                        if (response.IsSuccessStatusCode) {
                            log.Debug(new {
                                raw = str
                            });
                            JObject jObj = JObject.Parse(str);
                            JToken result = jObj["d"];
                            return result.ToObject<T>();
                        } else {
                            log.Debug(new {
                                success = false,
                                code = response.StatusCode,
                                responseStr = str
                            });
                        }
                        return default(T);
                    }
                }
            }
        }
        #endregion

        public static string GetJson(string address, CredentialCache c) {
            log.Debug(new {
                action = "SPHttpHelper_Json.GetJson",
                address = address
            });

            ServicePointManager.CheckCertificateRevocationList = false;
            using (WebRequestHandler handler = new WebRequestHandler {
                Credentials = c,
                UseDefaultCredentials = false,
                PreAuthenticate = true,
                AuthenticationLevel = AuthenticationLevel.None
            }) {
                using (HttpClient client = new HttpClient(handler)) {
                    client.BaseAddress = new Uri(address);
                    client.DefaultRequestHeaders.Add("Accept", "application/json;odata=verbose");
                    var requestMessage = new HttpRequestMessage {
                    };
                    using (HttpResponseMessage response = client.SendAsync(requestMessage).Result) {
                        string str = response.Content.ReadAsStringAsync().Result;
                        if (response.IsSuccessStatusCode) {
                            log.Debug(new {
                                raw = str
                            });
                        } else {
                            log.Debug(new {
                                success = false,
                                code = response.StatusCode,
                                responseStr = str
                            });
                        }
                        return str;
                    }
                }

            }
        }

        public static async Task<string> GetJsonAsync(string address, CredentialCache c) {
            log.Debug(new {
                action = "SPHttpHelper_Json.GetJson",
                address = address
            });

            ServicePointManager.CheckCertificateRevocationList = false;
            using (WebRequestHandler handler = new WebRequestHandler {
                Credentials = c,
                UseDefaultCredentials = false,
                PreAuthenticate = true,
                AuthenticationLevel = AuthenticationLevel.None
            }) {
                using (HttpClient client = new HttpClient(handler)) {
                    client.BaseAddress = new Uri(address);
                    client.DefaultRequestHeaders.Add("Accept", "application/json;odata=verbose");
                    var requestMessage = new HttpRequestMessage {
                    };
                    using (HttpResponseMessage response = client.SendAsync(requestMessage).Result) {
                        string str = await response.Content.ReadAsStringAsync();
                        if (response.IsSuccessStatusCode) {
                            log.Debug(new {
                                raw = str
                            });
                        } else {
                            log.Debug(new {
                                success = false,
                                code = response.StatusCode,
                                responseStr = str
                            });
                        }
                        return str;
                    }
                }

            }
        }
    }
}
