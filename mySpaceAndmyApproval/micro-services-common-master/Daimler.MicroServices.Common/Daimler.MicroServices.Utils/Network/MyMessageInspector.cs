﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel.Dispatcher;
using log4net;

namespace Daimler.MicroServices.Utils.Network {
    public class MyMessageInspector : IClientMessageInspector {
        private static readonly ILog log = LogManager.GetLogger(typeof(MyMessageInspector));
        public void AfterReceiveReply(ref System.ServiceModel.Channels.Message reply, object correlationState) {
            log.Debug(reply.ToString());
        }

        public object BeforeSendRequest(ref System.ServiceModel.Channels.Message request, System.ServiceModel.IClientChannel channel) {
            log.Debug(request.ToString());
            return null;
        }
    }
}
