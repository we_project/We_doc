﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Daimler.MicroServices.Utils {
    public class BaseReturn {
        public bool success { get; set; }
        public ReturnMsg? msg { get; set; }
        public static BaseReturn Exception {
            get {
                return new BaseReturn {
                    success = false,
                    msg = ReturnMsg.ExceptionOccurs
                };
            }
        }
    }
}