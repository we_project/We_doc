cd %~dp0
REM generate odata proxy class for sharepoint

REM load odata xml from sharepointurl/_api/$metadata
REM rename the odata xml file to *.edmx
REM run datasvcutil.exe from own pc
REM datasvcutil.exe https://www.microsoft.com/en-us/download/details.aspx?id=39373
"c:\Program Files (x86)\Microsoft WCF Data Services\5.6\bin\tools\datasvcutil.exe" /in:metadata.edmx /out:BadgeProxy.cs /version:3.0

