﻿@echo off
set serviceName="MicroServicesSharepoint"
set "servicePath=%~dp0Daimler.MicroServices.SP.DataImport.exe"
set serviceDesc=""
echo %servicePath%
sc create %serviceName% binPath= %servicePath% DisplayName= %serviceName% start= auto
sc description %serviceName% %serviceDesc%
sc start %serviceName%
pause