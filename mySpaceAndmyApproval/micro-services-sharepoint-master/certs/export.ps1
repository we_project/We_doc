$type = [System.Security.Cryptography.X509Certificates.X509ContentType]::Cert
# $certs = get-childitem -path cert:\LocalMachine\AuthRoot
$certs = get-childitem -path cert:\LocalMachine -Recurse | Where-Object {-not $_.PSIsContainer} | Select *
foreach($cert in $certs)
{
    $hash = $cert.GetCertHashString()
    $path = "c:\temp\" + $hash + ".der"
     [System.IO.File]::WriteAllBytes($path, $cert.export($type) ) 
}