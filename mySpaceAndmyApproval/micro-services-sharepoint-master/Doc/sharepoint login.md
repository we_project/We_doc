# Web interface for Mobile Device

## Post Token
* Get oauth2 token.
* Use this api as login method.
* For logout, just forget the token on mobile side.


#### Request Information
##### Header Parameters
| Name        | Description           | Type  |Additional information
| :------------ |:-------------:| -----:| ----------:|
| Content-Type      |  |  |application/x-www-form-urlencoded|


##### Body Parameters
grant_type=password is fixed, other 3 parameters need to input

    grant_type=password&domain=&username=&password=

#### Response Information
##### Response Formats
  
    {
    "access_token": "LuehRs6wD3A9DrCYpfOv9uMy_yRleLoUZLyYrTMNWwTFy0y9o3rkg1KSo309oaBZp8yNjTMBOk5DKoDCG14XnBeZjXsHtReAVzT6QD9w64IDg1pOQT2FppvvCMjLwlxgt0oWYFXo6c6bp9X3N6KeEbX1a1bpFPJFt175GkbGuSc8QJLuM6DwxPmcE1VzyArScICF28c2dwoRvGusubMoGYXsLDfVMaM-RS0OaXaHUGyr2w5KJbybXpG9UwCMi8KWCVdQkrPoMidt7XRqp-3KhL8dAVrBNnaroUdhABEqOtIXACm5v7_UPWNlCdYdJUGMfKEYJmH_rU69r6yTCjDZUNa2ljpRgxdRGi0byO3tobGVNLW98KqBlmBEc9Dwv3Hic1jIwBbh_CuRpTlP6C9T-IUzF_f-etYaINq5N-9GiYqqpqgwmFy2R_wT7EmjfOtk408baNfgp1Urj62riY5NOA",
    "token_type": "bearer",
    "expires_in": 1209599,
    "userName": "tonglli",
    ".issued": "Mon, 12 Jun 2017 09:25:21 GMT",
    ".expires": "Mon, 26 Jun 2017 09:25:21 GMT"
    }

#### Note

The response access_token need to be post as header on other api request.

```
POST /approver-home/uat/api/BusinessCard/Approval HTTP/1.1
Host: anywhere.i.daimler.com
SM_USER: tonglli
Content-Type: application/json
Authorization: Bearer lLcnG9mstUeG8yezyfuLYpqvmcpC24OThIl-FUn0U3RhxJAGTY5131mgmXScYlbCnOylWeAgwoVYNjSeK2PmpHOHzkInBLc9oDv_nN3kGnSL8e6J8G0srQjnCqWZawUO3Xa7YfLO9V-xcatQ09oO7gL8_uYdah25UYE6qBaGSXXDnJFQhYsYijGEZp2HS1OP-uPSafvd1Snv_4eF0lAH5a7FvuzIdYwOTKZ38e6zrmAw_pWiwCLh0t3rMBNeLEngCujOHM1m7dHtuKMKhFaWs_Nd6ECmNR-UOZssu4LKnqSVoNeSvBYkyNP0VdR7fw95RTu5NURSp9ggRjYESJpXBz24laL1jNbadksPWvRWh7vrUmwy1_HtOxNYNEJpVRNMp2mv0fFrKZpzXze39cvTH4wGvPaIoPs3ZuGiyhrIzxBFxI88FF6EDh7oAPHeaZNowws8BDhcZeJLz7FswQQZnA
Cache-Control: no-cache

{
                "ID" : 52,
                "approve" : true,
                "comment" : "testtesttest"
            }
```