create table FormA(
ID int not null,
        Title nvarchar(255) null,
        CaseID nvarchar(255) null,
        Status nvarchar(255) null,
        FlowPath nvarchar(max) null,
        Requestor int null,
        CostCenter int null,
        Tax int null,
        TaxManager int null,
        Controlling int null,
        ContactNumber nvarchar(255) null,
        IONo nvarchar(255) null,
        PRNo nvarchar(255) null,
        EventName nvarchar(255) null,
        TargetGroup nvarchar(255) null,
        SelectionCriteria nvarchar(255) null,
        ActivityDescription nvarchar(255) null,
        ActivityNature nvarchar(255) null,
        OtherSelectionCriteria nvarchar(255) null,
        BenefitTo nvarchar(255) null,
        BenefitAmount nvarchar(255) null,
        ExternalNumber nvarchar(255) null,
        EICost1 nvarchar(255) null,
        EICost2 nvarchar(255) null,
        EICost3 nvarchar(255) null,
        EECost1 nvarchar(255) null,
        EECost2 nvarchar(255) null,
        EECost3 nvarchar(255) null,
        InternalCost1 nvarchar(255) null,
        InternalCost2 nvarchar(255) null,
        InternalCost3 nvarchar(255) null,
        ExpenditureItem4 nvarchar(255) null,
        ExpenditureItem5 nvarchar(255) null,
        ExpenditureItem6 nvarchar(255) null,
        EICost4 nvarchar(255) null,
        EICost5 nvarchar(255) null,
        EICost6 nvarchar(255) null,
        EECost4 nvarchar(255) null,
        EECost5 nvarchar(255) null,
        EECost6 nvarchar(255) null,
        InternalCost4 nvarchar(255) null,
        InternalCost5 nvarchar(255) null,
        InternalCost6 nvarchar(255) null,
        EstimatedIITAmount nvarchar(255) null,
        EstimatedIITAmountOccasional nvarchar(255) null,
        EstimatedIITAmountLabor nvarchar(255) null,
        EstimatedOutputAmount nvarchar(255) null,
        EstimatedInputAmount nvarchar(255) null,
        Note nvarchar(max) null,
        StatusIndex nvarchar(255) null,
        _x0054_ax2 int null,
        Controlling2 int null,
        NewComment nvarchar(max) null,
        TeamGroup nvarchar(255) null,
        LegalEntity nvarchar(255) null,
        FormBCount nvarchar(255) null,
        IsStatusChange bit null,
        FormA nvarchar(255) null,
        CalculationMethod nvarchar(255) null,
        VATRate nvarchar(255) null,
        CurrentUser nvarchar(255) null,
        FormAComments nvarchar(255) null,
        Year nvarchar(255) null,
        IDIndex nvarchar(255) null,
        IsReject bit null,
        AddFormB nvarchar(255) null,
        Pending int null,
        Pending2 int null,
        CostCenterCode nvarchar(255) null,
        FormBAmount nvarchar(255) null,
        SurplusBudget decimal null,
        EICost1_Submit nvarchar(255) null,
        EICost2_Submit nvarchar(255) null,
        EICost3_Submit nvarchar(255) null,
        EICost4_Submit nvarchar(255) null,
        EICost5_Submit nvarchar(255) null,
        EICost6_Submit nvarchar(255) null,
        EECost1_Submit nvarchar(255) null,
        EECost2_Submit nvarchar(255) null,
        EECost3_Submit nvarchar(255) null,
        EECost4_Submit nvarchar(255) null,
        EECost5_Submit nvarchar(255) null,
        EECost6_Submit nvarchar(255) null,
        InternalCost1_Submit nvarchar(255) null,
        InternalCost2_Submit nvarchar(255) null,
        InternalCost3_Submit nvarchar(255) null,
        InternalCost4_Submit nvarchar(255) null,
        InternalCost5_Submit nvarchar(255) null,
        InternalCost6_Submit nvarchar(255) null,
        Modified DateTime null,
        Created DateTime null,
		IsEnable bit not null
)