--drop table CostCenterDeputyProfile
create table CostCenterDeputyProfile(
ID int not null,
Title nvarchar(255) null,
Cost_x0020_Center_x0020_Manager int null,
Cost_x0020_Center_x0020_Deputy int null,
Delegate_x0020_From_x0020_Date datetime null,
Delegate_x0020_To_x0020_Date datetime null,
Modified datetime null,
Created datetime null,
IsEnable bit not null
)