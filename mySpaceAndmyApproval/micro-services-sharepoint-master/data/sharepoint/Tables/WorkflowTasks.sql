--drop table OfficeStationaryRequests
create table WorkflowTasks(
ID int not null,
        Status nvarchar(255) null,
        AssignedTo int null,
        Title nvarchar(255) null,
        WorkflowOutcome nvarchar(255) null,
        WorkflowName nvarchar(255) null,
        WorkflowItemId int null,
        ApprovalOutcome int null,
        Decision int null,
        ApproverComments nvarchar(max) null,
        Created datetime null,
		IsEnable bit not null
)

alter table WorkflowTasks add IsApprovedFromOurSide bit null;