create table OSLocationAdminMapping(
 ID int not null,
        Title nvarchar(255) null,
        Address_CN nvarchar(255) null,
        Address_EN nvarchar(255) null,
        Entity nvarchar(255) null,
        AdminContact nvarchar(255) null,
        AdminContactBackup nvarchar(255) null,
		IsEnable bit not null
)