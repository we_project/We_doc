alter proc cp_sp_getFormAV2(
@userId int,
@pageIndex int=null,
@pageSize int=null
)
as
begin
if @pageIndex is not null
begin 
	declare @offset int;
	set @offset=@pageIndex*@pageSize;
	select 
	f.ID,
			--f.Title,
			CaseID,
			Status,
			--FlowPath,
			Requestor.Title Requestor,
		--  CostCenter.Title  CostCenter,
		--  Tax.Title  Tax,
		--	TaxManager.Title  TaxManager,
		--  Controlling.Title  Controlling,
		--	ContactNumber,
		--	IONo,
		--	PRNo,
		--	EventName,
		--	TargetGroup,
		--	SelectionCriteria,
		--	ActivityDescription,
		--	ActivityNature,
		--	OtherSelectionCriteria,
		--	BenefitTo,
		--	BenefitAmount,
		--	ExternalNumber,
		--	EICost1,
		--	EICost2,
		--	EICost3,
		--	EECost1,
		--	EECost2,
		--	EECost3,
		--	InternalCost1,
		--	InternalCost2,
		--	InternalCost3,
		--	ExpenditureItem4,
		--	ExpenditureItem5,
		--	ExpenditureItem6,
		--	EICost4,
		--	EICost5,
		--	EICost6,
		--	EECost4,
		--	EECost5,
		--	EECost6,
		--	InternalCost4,
		--	InternalCost5,
		--	InternalCost6,
		--	EstimatedIITAmount,
		--	EstimatedIITAmountOccasional,
		--	EstimatedIITAmountLabor,
		--	EstimatedOutputAmount,
		--	EstimatedInputAmount,
		--	Note,
		--	StatusIndex,
		--_x0054_ax2.Title    _x0054_ax2,
		-- Controlling2.Title   Controlling2,
		--	NewComment,
		--	TeamGroup,
		--	LegalEntity,
		--	FormBCount,
		--	IsStatusChange,
		--	FormA,
		--	CalculationMethod,
		--	VATRate,
		--	CurrentUser,
		--	FormAComments,
		--	Year,
		--	IDIndex,
		--	IsReject,
		--	AddFormB,
		--   Pending.Title Pending,
		--   Pending2.Title Pending2,
		--	CostCenterCode,
		--	FormBAmount,
		--	SurplusBudget,
		--	EICost1_Submit,
		--	EICost2_Submit,
		--	EICost3_Submit,
		--	EICost4_Submit,
		--	EICost5_Submit,
		--	EICost6_Submit,
		--	EECost1_Submit,
		--	EECost2_Submit,
		--	EECost3_Submit,
		--	EECost4_Submit,
		--	EECost5_Submit,
		--	EECost6_Submit,
		--	InternalCost1_Submit,
		--	InternalCost2_Submit,
		--	InternalCost3_Submit,
		--	InternalCost4_Submit,
		--	InternalCost5_Submit,
		--	InternalCost6_Submit,
			Modified,
			Created
	from FormA f
	left join FormABUser Requestor on f.Requestor=Requestor.ID
	left join FormABUser CostCenter on f.CostCenter=CostCenter.ID
	left join FormABUser Tax on f.Tax=Tax.ID
	left join FormABUser TaxManager on f.TaxManager=TaxManager.ID
	left join FormABUser Controlling on f.Controlling=Controlling.ID
	left join FormABUser _x0054_ax2 on f._x0054_ax2=_x0054_ax2.ID
	left join FormABUser Controlling2 on f.Controlling2=Controlling2.ID
	left join FormABUser Pending on f.Pending=Pending.ID
	left join FormABUser Pending2 on f.Pending2=Pending2.ID
	where 
	f.IsEnable=1
	and IsStatusChange=0
	and(
	(Status='Submitted' and CostCenter=@userId)
	--or (Status='Tax team confirmed' and TaxManager=@userId)
	--or (Status='Tax manager confirmed' and (Controlling=@userId or Controlling2=@userId))
	)
	order by Created offset @offset rows fetch next @pageSize rows only;
end
else
begin
	select 
	f.ID,
			--f.Title,
			CaseID,
			Status,
			--FlowPath,
			Requestor.Title Requestor,
		--  CostCenter.Title  CostCenter,
		--  Tax.Title  Tax,
		--	TaxManager.Title  TaxManager,
		--  Controlling.Title  Controlling,
		--	ContactNumber,
		--	IONo,
		--	PRNo,
		--	EventName,
		--	TargetGroup,
		--	SelectionCriteria,
		--	ActivityDescription,
		--	ActivityNature,
		--	OtherSelectionCriteria,
		--	BenefitTo,
		--	BenefitAmount,
		--	ExternalNumber,
		--	EICost1,
		--	EICost2,
		--	EICost3,
		--	EECost1,
		--	EECost2,
		--	EECost3,
		--	InternalCost1,
		--	InternalCost2,
		--	InternalCost3,
		--	ExpenditureItem4,
		--	ExpenditureItem5,
		--	ExpenditureItem6,
		--	EICost4,
		--	EICost5,
		--	EICost6,
		--	EECost4,
		--	EECost5,
		--	EECost6,
		--	InternalCost4,
		--	InternalCost5,
		--	InternalCost6,
		--	EstimatedIITAmount,
		--	EstimatedIITAmountOccasional,
		--	EstimatedIITAmountLabor,
		--	EstimatedOutputAmount,
		--	EstimatedInputAmount,
		--	Note,
		--	StatusIndex,
		--_x0054_ax2.Title    _x0054_ax2,
		-- Controlling2.Title   Controlling2,
		--	NewComment,
		--	TeamGroup,
		--	LegalEntity,
		--	FormBCount,
		--	IsStatusChange,
		--	FormA,
		--	CalculationMethod,
		--	VATRate,
		--	CurrentUser,
		--	FormAComments,
		--	Year,
		--	IDIndex,
		--	IsReject,
		--	AddFormB,
		--   Pending.Title Pending,
		--   Pending2.Title Pending2,
		--	CostCenterCode,
		--	FormBAmount,
		--	SurplusBudget,
		--	EICost1_Submit,
		--	EICost2_Submit,
		--	EICost3_Submit,
		--	EICost4_Submit,
		--	EICost5_Submit,
		--	EICost6_Submit,
		--	EECost1_Submit,
		--	EECost2_Submit,
		--	EECost3_Submit,
		--	EECost4_Submit,
		--	EECost5_Submit,
		--	EECost6_Submit,
		--	InternalCost1_Submit,
		--	InternalCost2_Submit,
		--	InternalCost3_Submit,
		--	InternalCost4_Submit,
		--	InternalCost5_Submit,
		--	InternalCost6_Submit,
			Modified,
			Created
	from FormA f
	left join FormABUser Requestor on f.Requestor=Requestor.ID
	left join FormABUser CostCenter on f.CostCenter=CostCenter.ID
	left join FormABUser Tax on f.Tax=Tax.ID
	left join FormABUser TaxManager on f.TaxManager=TaxManager.ID
	left join FormABUser Controlling on f.Controlling=Controlling.ID
	left join FormABUser _x0054_ax2 on f._x0054_ax2=_x0054_ax2.ID
	left join FormABUser Controlling2 on f.Controlling2=Controlling2.ID
	left join FormABUser Pending on f.Pending=Pending.ID
	left join FormABUser Pending2 on f.Pending2=Pending2.ID
	where 
	f.IsEnable=1
	and IsStatusChange=0
	and(
	(Status='Submitted' and CostCenter=@userId)
	--or (Status='Tax team confirmed' and TaxManager=@userId)
	--or (Status='Tax manager confirmed' and (Controlling=@userId or Controlling2=@userId))
	)
	order by Created
end
end