create proc cp_sp_saveOSLocationAdminMapping(
@infoes ut_OSLocationAdminMapping readonly
)
as
begin
merge into OSLocationAdminMapping t
using @infoes s
on t.ID=s.ID
when matched then
update set
        t.Title=s.Title,
        t.Address_CN=s.Address_CN,
        t.Address_EN=s.Address_EN,
        t.Entity=s.Entity,
        t.AdminContact=s.AdminContact,
        t.AdminContactBackup=s.AdminContactBackup,
		t.IsEnable=1
when not matched then
insert(
 ID,
        Title,
        Address_CN,
        Address_EN,
        Entity,
        AdminContact,
        AdminContactBackup,
		IsEnable
)
values(
 s.ID,
        s.Title,
        s.Address_CN,
        s.Address_EN,
        s.Entity,
        s.AdminContact,
        s.AdminContactBackup,
		1
)
when not matched by source then

update set
t.IsEnable=0
;

end