create proc cp_sp_saveSite(
@infoes ut_Common readonly
)
as
begin
merge into Site t
using @infoes s
on t.ID=s.ID
when matched then
update set
t.Title=s.Title,
t.IsEnable=1
when not matched then
insert(
ID,
Title,
IsEnable
)
values(
s.ID,
s.Title,
1
)
when not matched by source then
update set
t.IsEnable=0;

end