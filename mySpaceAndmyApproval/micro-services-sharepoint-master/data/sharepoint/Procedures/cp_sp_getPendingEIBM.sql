create proc cp_sp_getPendingEIBM(
@userId int
)
as
begin 

select 
badge.ID,
bu.Title Employee_x0020_Name,
badge.Application_x0020_Date,
badge.Application_x0020_Type,
badge.Status
from EIBM badge
left join BadgeUser bu on Employee_x0020_NameId=bu.ID
left join CostCenterDeputyProfile ccdp on CCApproverId=ccdp.Cost_x0020_Center_x0020_Manager and CCDeputyId=ccdp.Cost_x0020_Center_x0020_Deputy and ccdp.IsEnable=1 and ccdp.Delegate_x0020_From_x0020_Date<=GETDATE() and ccdp.Delegate_x0020_To_x0020_Date>=GETDATE()
where 
badge.IsEnable=1
and (
	(
		Status='Pending Cost Center Manager Approval'
		and ( CCApproverId=@userId or (
			CCDeputyId=@userId
			and ccdp.Cost_x0020_Center_x0020_Deputy=@userId
		) )
	)
	or
	(
		Status='Pending Special Area Approval'
		and (
			(
				SAStatus1='Pending Approval'
				and ( SAApprover1Id=@userId or SADeputy1Id=@userId )
			)
			or(
				SAStatus2='Pending Approval'
				and ( SAApprover2Id=@userId or SADeputy2Id=@userId )
			)
			or(
				SAStatus3='Pending Approval'
				and ( SAApprover3Id=@userId or SADeputy3Id=@userId )
			)
			or(
				SAStatus4='Pending Approval'
				and ( SAApprover4Id=@userId or SADeputy4Id=@userId )
			)
			or(
				SAStatus5='Pending Approval'
				and ( SAApprover5Id=@userId or SADeputy5Id=@userId )
			)
			or(
				SAStatus6='Pending Approval'
				and ( SAApprover6Id=@userId or SADeputy6Id=@userId )
			)
			or(
				SAStatus7='Pending Approval'
				and ( SAApprover7Id=@userId or SADeputy7Id=@userId )
			)
		)
	)
)
order by badge.Created;
end