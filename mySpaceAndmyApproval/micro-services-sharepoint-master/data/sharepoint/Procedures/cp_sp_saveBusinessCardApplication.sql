--use MicroServicesSharepoint
--drop proc cp_sp_saveBusinessCardApplication
create proc cp_sp_saveBusinessCardApplication(
@infoes ut_BusinessCardApplication readonly
)
as
begin
merge into BusinessCardApplication t
using @infoes s
on t.ID=s.ID
when matched then
update set
        t.Title=s.Title,
        t.UserName=s.UserName,
        t.EmployeeNumber=s.EmployeeNumber,
        t.CompanyName=s.CompanyName,
        t.CostCenter=s.CostCenter,
        t.ExtensionNumber=s.ExtensionNumber,
        t.FirstName=s.FirstName,
        t.LastName=s.LastName,
        t.DepartmentName=s.DepartmentName,
        t.PhoneNo=s.PhoneNo,
        t.Email=s.Email,
        t.Position=s.Position,
        t.FixNo=s.FixNo,
        t.Address=s.Address,
        t.Modified=s.Modified,
        t.Created=s.Created,
        t.Business=s.Business,
        t.BusinessCardType=s.BusinessCardType,
        t.FirstNameCN=s.FirstNameCN,
        t.SecondNameCN=s.SecondNameCN,
        t.DepartmentCN=s.DepartmentCN,
        t.PositionCN=s.PositionCN,
        t.AddressCN=s.AddressCN,
        t.RequestorLoginName=s.RequestorLoginName,
        t.Supervisor=s.Supervisor,
        t.State=s.State,
        t.Location=s.Location,
        t.EmployeeType=s.EmployeeType,
        t.HRKA=s.HRKA,
        t.cWorkflowHistory=s.cWorkflowHistory,
        t.AdminContact=s.AdminContact,
        t.Overprint=s.Overprint,
        t.Business0=s.Business0,
        --t.Remark=s.Remark,
		t.IsEnable=1
when not matched then
insert(
ID,
        Title,
        UserName,
        EmployeeNumber,
        CompanyName,
        CostCenter,
        ExtensionNumber,
        FirstName,
        LastName,
        DepartmentName,
        PhoneNo,
        Email,
        Position,
        FixNo,
        Address,
        Modified,
        Created,
        Business,
        BusinessCardType,
        FirstNameCN,
        SecondNameCN,
        DepartmentCN,
        PositionCN,
        AddressCN,
        RequestorLoginName,
        Supervisor,
        State,
        Location,
        EmployeeType,
        HRKA,
        cWorkflowHistory,
        AdminContact,
        Overprint,
        Business0,
        --Remark,
		IsEnable
)
values(
s.ID,
        s.Title,
        s.UserName,
        s.EmployeeNumber,
        s.CompanyName,
        s.CostCenter,
        s.ExtensionNumber,
        s.FirstName,
        s.LastName,
        s.DepartmentName,
        s.PhoneNo,
        s.Email,
        s.Position,
        s.FixNo,
        s.Address,
        s.Modified,
        s.Created,
        s.Business,
        s.BusinessCardType,
        s.FirstNameCN,
        s.SecondNameCN,
        s.DepartmentCN,
        s.PositionCN,
        s.AddressCN,
        s.RequestorLoginName,
        s.Supervisor,
        s.State,
        s.Location,
        s.EmployeeType,
        s.HRKA,
        s.cWorkflowHistory,
        s.AdminContact,
        s.Overprint,
        s.Business0,
        --s.Remark,
		1
)
when not matched by source then

update set
t.IsEnable=0
;

end