--drop proc cp_sp_getPetrolCard_CreditAdjustmentV2
alter proc cp_sp_getPetrolCard_CreditAdjustmentV2(
@supervisor nvarchar(255),
@pageIndex int=null,
@pageSize int=null
)
as
begin
if @pageIndex is not null
begin
	declare @offset int;
	set @offset=@pageIndex*@pageSize;
	select
	pcca.ID,
			--pcca.Title,
			u.Title UserName,
			--EmployeeNumber,
			--CompanyName,
			CostCenter,
			RequestAmount,
			--AdjustmentReason,
			--ExtensionNumber,
			--PlateNumber,
			--CurrentCredit,
			--Status,
			--Comments,
			--Supervisor,
			--CardNo,
			--DisplayName,
			--PetrolCa,
			Modified,
			Created
	from
	PetrolCard_CreditAdjustment pcca
	left join CommonUser u on u.AccountName=pcca.UserName
	where 
	pcca.IsEnable=1
	and Supervisor=@supervisor
	and Status='Submitted'
	order by Created offset @offset rows fetch next @pageSize rows only;
end
else
begin
	select
	pcca.ID,
			--pcca.Title,
			u.Title UserName,
			--EmployeeNumber,
			--CompanyName,
			CostCenter,
			RequestAmount,
			--AdjustmentReason,
			--ExtensionNumber,
			--PlateNumber,
			--CurrentCredit,
			--Status,
			--Comments,
			--Supervisor,
			--CardNo,
			--DisplayName,
			--PetrolCa,
			Modified,
			Created
	from
	PetrolCard_CreditAdjustment pcca
	left join CommonUser u on u.AccountName=pcca.UserName
	where 
	pcca.IsEnable=1
	and Supervisor=@supervisor
	and Status='Submitted'
	order by Created
end
end