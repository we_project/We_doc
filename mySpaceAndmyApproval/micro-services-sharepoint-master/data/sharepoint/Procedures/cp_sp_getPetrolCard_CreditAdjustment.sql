--drop proc cp_sp_getPetrolCard_CreditAdjustment
create proc cp_sp_getPetrolCard_CreditAdjustment(
@supervisor nvarchar(255),
@pageIndex int,
@pageSize int
)
as
begin

declare @offset int;
set @offset=@pageIndex*@pageSize;
select
pcca.ID,
        pcca.Title,
        UserName,
        EmployeeNumber,
        CompanyName,
        CostCenter,
        RequestAmount,
        AdjustmentReason,
        ExtensionNumber,
        PlateNumber,
        CurrentCredit,
        Status,
        Comments,
        Supervisor,
        CardNo,
        u.Title DisplayName,
        PetrolCa,
        Modified,
        Created
from
PetrolCard_CreditAdjustment pcca
left join CommonUser u on u.AccountName=pcca.UserName
where 
pcca.IsEnable=1
and 
Supervisor=@supervisor
and Status='Submitted'
order by Created offset @offset rows fetch next @pageSize rows only;

end