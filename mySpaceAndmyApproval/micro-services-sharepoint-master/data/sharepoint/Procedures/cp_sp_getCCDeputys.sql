create proc cp_sp_getCCDeputys(
@deputyId int
)
as
begin
SELECT [ID]
      ,[Title]
      ,[Cost_x0020_Center_x0020_Manager]
      ,[Cost_x0020_Center_x0020_Deputy]
      ,[Delegate_x0020_From_x0020_Date]
      ,[Delegate_x0020_To_x0020_Date]
      ,[Modified]
      ,[Created]
  FROM [dbo].[CostCenterDeputyProfile]
  where Cost_x0020_Center_x0020_Deputy=@deputyId
  and (
  (Delegate_x0020_From_x0020_Date is null and  Delegate_x0020_To_x0020_Date is null)
  or( Delegate_x0020_From_x0020_Date is null and Delegate_x0020_To_x0020_Date is not null and Delegate_x0020_To_x0020_Date>GETDATE() )
  or (Delegate_x0020_From_x0020_Date is not null and Delegate_x0020_To_x0020_Date is null and Delegate_x0020_From_x0020_Date <GETDATE())
  or (Delegate_x0020_From_x0020_Date is not null and Delegate_x0020_To_x0020_Date is not null and Delegate_x0020_From_x0020_Date<GETDATE() and Delegate_x0020_To_x0020_Date >GETDATE())
  )
  and IsEnable=1
  ;
end