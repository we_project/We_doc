create proc cp_sp_getStationaryDetail(
@ID int
)
as
begin
	select top 1
			request.ID,
			UserName,
			Status,
			Company,
			TotalAmount,
			Created,
			CostCenter,
			ExtensionNumber,
			location.Title LocationCode,
			Location,
			DetailAddress,
			FullAddress,
			Orders
	from OfficeStationaryRequests request
	join OSLocationAdminMapping location
	on request.LocationCode=location.ID
	where request.ID=@ID
end