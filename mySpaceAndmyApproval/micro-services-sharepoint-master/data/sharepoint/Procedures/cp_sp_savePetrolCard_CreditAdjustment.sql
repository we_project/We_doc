--use MicroServicesSharepoint
--drop proc cp_sp_savePetrolCard_CreditAdjustment
create proc cp_sp_savePetrolCard_CreditAdjustment(
@infoes ut_PetrolCard_CreditAdjustment readonly
)
as
begin
merge into PetrolCard_CreditAdjustment t
using @infoes s
on t.ID=s.ID
when matched then
update set
        t.Title=s.Title,
        t.UserName=s.UserName,
        t.EmployeeNumber=s.EmployeeNumber,
        t.CompanyName=s.CompanyName,
        t.CostCenter=s.CostCenter,
        t.RequestAmount=s.RequestAmount,
        t.AdjustmentReason=s.AdjustmentReason,
        t.ExtensionNumber=s.ExtensionNumber,
        t.PlateNumber=s.PlateNumber,
        t.CurrentCredit=s.CurrentCredit,
        t.Status=s.Status,
        t.Comments=s.Comments,
        t.Supervisor=s.Supervisor,
        t.CardNo=s.CardNo,
        t.DisplayName=s.DisplayName,
        t.PetrolCa=s.PetrolCa,
        t.Modified=s.Modified,
        t.Created=s.Created,
		t.IsEnable=1
when not matched then
insert(
 ID,
        Title,
        UserName,
        EmployeeNumber,
        CompanyName,
        CostCenter,
        RequestAmount,
        AdjustmentReason,
        ExtensionNumber,
        PlateNumber,
        CurrentCredit,
        Status,
        Comments,
        Supervisor,
        CardNo,
        DisplayName,
        PetrolCa,
        Modified,
        Created,
		IsEnable
)
values(
 s.ID,
        s.Title,
        s.UserName,
        s.EmployeeNumber,
        s.CompanyName,
        s.CostCenter,
        s.RequestAmount,
        s.AdjustmentReason,
        s.ExtensionNumber,
        s.PlateNumber,
        s.CurrentCredit,
        s.Status,
        s.Comments,
        s.Supervisor,
        s.CardNo,
        s.DisplayName,
        s.PetrolCa,
        s.Modified,
        s.Created,
		1
)
when not matched by source then

update set
t.IsEnable=0
;

end