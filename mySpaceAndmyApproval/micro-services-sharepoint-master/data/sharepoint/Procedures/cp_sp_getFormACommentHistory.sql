alter proc cp_sp_getFormACommentHistory(
@caseId int
)
as
begin
select 
 ID,
        Title,
        CaseID,
        Comment,
        UserStr,
        Modified,
        Created
 from FormACommentHistory
where
IsEnable=1
and
 CaseID=@caseId
 order by Created desc
end