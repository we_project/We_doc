create proc cp_sp_saveCCDeputy(
@infoes ut_CostCenterDeputyProfile readonly
)
as
begin
merge into CostCenterDeputyProfile t
using @infoes s
on t.ID=s.ID
when matched then
update set
        t.Title=s.Title,
		t.Cost_x0020_Center_x0020_Manager=s.Cost_x0020_Center_x0020_Manager,
		t.Cost_x0020_Center_x0020_Deputy=s.Cost_x0020_Center_x0020_Deputy,
		t.Delegate_x0020_From_x0020_Date=s.Delegate_x0020_From_x0020_Date,
		t.Delegate_x0020_To_x0020_Date=s.Delegate_x0020_To_x0020_Date,
        t.Modified=s.Modified,
        t.Created=s.Created,
		t.IsEnable=1
when not matched then
insert(
ID,
        Title,
        Cost_x0020_Center_x0020_Manager,
        Cost_x0020_Center_x0020_Deputy,
        Delegate_x0020_From_x0020_Date,
        Delegate_x0020_To_x0020_Date,
        Modified,
        Created,
		IsEnable
)
values(
s.ID,
        s.Title,
		s.Cost_x0020_Center_x0020_Manager,
        s.Cost_x0020_Center_x0020_Deputy,
        s.Delegate_x0020_From_x0020_Date,
        s.Delegate_x0020_To_x0020_Date,
        s.Modified,
        s.Created,
		1
)
when not matched by source then

update set
t.IsEnable=0
;

end