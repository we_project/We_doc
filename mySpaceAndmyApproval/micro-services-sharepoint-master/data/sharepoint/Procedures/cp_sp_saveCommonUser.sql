create proc cp_sp_saveCommonUser(
@infoes ut_SPUser readonly
)
as
begin
merge into CommonUser t
using @infoes s
on t.ID=s.ID
when matched then
update set
        t.Title=s.Title,
        t.AccountName=s.AccountName,
        t.EMail=s.EMail,
		t.IsEnable=1
when not matched then
insert(
        ID,
         Title,
        AccountName,
        EMail,
		IsEnable
)
values(
        s.ID,
        s.Title,
        s.AccountName,
        s.EMail,
		1
)
when not matched by source then

update set
t.IsEnable=0
;

end