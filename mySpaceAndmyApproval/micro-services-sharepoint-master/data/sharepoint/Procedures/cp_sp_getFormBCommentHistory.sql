create proc cp_sp_getFormBCommentHistory(
@caseId int
)
as
begin
select 
 ID,
        Title,
        CaseID,
        Comment,
        UserStr,
        Modified,
        Created
 from FormBCommentHistory
where
IsEnable=1
and
 CaseID=@caseId
 order by Created desc
end