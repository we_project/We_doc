alter proc cp_sp_getPendingStationarySnapshotV2(
@userId int,
@pageIndex int=null,
@pageSize int=null
)
as
begin

if @pageIndex is not null
begin

	declare @offset int;
	set @offset=@pageIndex*@pageSize;
	select ID ,
		--Company ,
		Modified,
		Status,
		TotalAmount,
		UserName from OfficeStationaryRequests
	where 
	IsEnable=1
	and
	(
	( Status='CostCenterManagerApproveInProcess' and CostCenterManagerPeople =@userId)
	or
	(Status='AdminLocalManagerApproveInProcess' and AdminLocalManager=@userId)
	or
	(Status='AdminSeniorManagerApproveInProcess' and AdminSeniorManager=@userId)
	)
	order by Created offset @offset rows fetch next @pageSize rows only;
end
else
begin
	select ID ,
		--Company ,
		Modified,
		Status,
		TotalAmount,
		UserName from OfficeStationaryRequests
	where 
	IsEnable=1
	and
	(
	( Status='CostCenterManagerApproveInProcess' and CostCenterManagerPeople =@userId)
	or
	(Status='AdminLocalManagerApproveInProcess' and AdminLocalManager=@userId)
	or
	(Status='AdminSeniorManagerApproveInProcess' and AdminSeniorManager=@userId)
	)
	order by Created;
end
end