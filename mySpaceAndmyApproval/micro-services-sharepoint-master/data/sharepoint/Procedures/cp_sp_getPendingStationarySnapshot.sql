alter proc cp_sp_getPendingStationarySnapshot(
@userId int,
@pageIndex int,
@pageSize int
)
as
begin

declare @offset int;
set @offset=@pageIndex*@pageSize;
select ID ,
    Company ,
    Modified,
    Status,
    TotalAmount,
    UserName from OfficeStationaryRequests
where 
IsEnable=1
and
(
( Status='CostCenterManagerApproveInProcess' and CostCenterManagerPeople =@userId)
or
(Status='AdminLocalManagerApproveInProcess' and AdminLocalManager=@userId)
or
(Status='AdminSeniorManagerApproveInProcess' and AdminSeniorManager=@userId)
)
order by Created offset @offset rows fetch next @pageSize rows only;

end