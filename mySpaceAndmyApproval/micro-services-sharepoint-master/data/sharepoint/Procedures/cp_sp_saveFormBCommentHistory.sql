--use MicroServicesSharepoint
--drop proc cp_sp_saveFormBCommentHistory
create proc cp_sp_saveFormBCommentHistory(
@infoes ut_FormBCommentHistory readonly
)
as
begin
merge into FormBCommentHistory t
using @infoes s
on t.ID=s.ID
when matched then
update set
        t.Title=s.Title,
        t.CaseID=s.CaseID,
        t.Comment=s.Comment,
        t.UserStr=s.UserStr,
        t.Modified=s.Modified,
        t.Created=s.Created,
		t.IsEnable=1
when not matched then
insert(
 ID,
        Title,
        CaseID,
        Comment,
        UserStr,
        Modified,
        Created,
		IsEnable
)
values(
 s.ID,
        s.Title,
        s.CaseID,
        s.Comment,
        s.UserStr,
        s.Modified,
        s.Created,
		1
)
when not matched by source then

update set
t.IsEnable=0
;

end