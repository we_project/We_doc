--drop proc cp_sp_getPetrolCard_CreditAdjustmentDetail
create proc cp_sp_getPetrolCard_CreditAdjustmentDetail(
@ID int
)
as
begin
select
pcca.ID,
        pcca.Title,
        UserName,
        EmployeeNumber,
        CompanyName,
        CostCenter,
        RequestAmount,
        AdjustmentReason,
        ExtensionNumber,
        PlateNumber,
        CurrentCredit,
        Status,
        Comments,
        Supervisor,
        CardNo,
        u.Title DisplayName,
        PetrolCa,
        Modified,
        Created
from
PetrolCard_CreditAdjustment pcca
left join CommonUser u on u.AccountName=pcca.UserName
where 
pcca.IsEnable=1
and pcca.ID=@ID
end