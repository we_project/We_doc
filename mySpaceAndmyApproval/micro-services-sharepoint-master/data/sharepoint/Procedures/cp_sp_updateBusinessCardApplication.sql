--use MicroServicesSharepoint
--drop proc cp_sp_updateBusinessCardApplication
create proc cp_sp_updateBusinessCardApplication(
@infoes ut_BusinessCardApplication readonly
)
as
begin
merge into BusinessCardApplication t
using @infoes s
on t.ID=s.ID
when matched then
update set
        t.Title=s.Title,
        t.UserName=s.UserName,
        t.EmployeeNumber=s.EmployeeNumber,
        t.CompanyName=s.CompanyName,
        t.CostCenter=s.CostCenter,
        t.ExtensionNumber=s.ExtensionNumber,
        t.FirstName=s.FirstName,
        t.LastName=s.LastName,
        t.DepartmentName=s.DepartmentName,
        t.PhoneNo=s.PhoneNo,
        t.Email=s.Email,
        t.Position=s.Position,
        t.FixNo=s.FixNo,
        t.Address=s.Address,
        t.Modified=s.Modified,
        t.Created=s.Created,
        t.Business=s.Business,
        t.BusinessCardType=s.BusinessCardType,
        t.FirstNameCN=s.FirstNameCN,
        t.SecondNameCN=s.SecondNameCN,
        t.DepartmentCN=s.DepartmentCN,
        t.PositionCN=s.PositionCN,
        t.AddressCN=s.AddressCN,
        t.RequestorLoginName=s.RequestorLoginName,
        t.Supervisor=s.Supervisor,
        t.State=s.State,
        t.Location=s.Location,
        t.EmployeeType=s.EmployeeType,
        t.HRKA=s.HRKA,
        t.cWorkflowHistory=s.cWorkflowHistory,
        t.AdminContact=s.AdminContact,
        t.Overprint=s.Overprint,
        t.Business0=s.Business0,
        --t.Remark=s.Remark,
		t.IsEnable=1
;

end