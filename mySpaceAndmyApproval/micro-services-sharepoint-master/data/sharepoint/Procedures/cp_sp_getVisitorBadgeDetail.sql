create proc cp_sp_getVisitorBadgeDetail(
@ID int
)
as
begin
        select top 1
                badge.ID,
badge.Status,
badge.CCApproverId,
cca.Title CCApproverTitle,
badge.CCDeputyId,
ccd.Title CCDeputyTitle,
badge.Application_x0020_Date,
badge.Application_x0020_Type,
badge.Employee_x0020_NameId,
en.Title Employee_x0020_NameTitle,
badge.Title,
badge.Employee_x0020_Number,
badge.Company,
badge.Department,
badge.Deskphone_x0020_Number,
badge.Cost_x0020_Center,
badge.Badge_x0020_Type,
badge.Arrive_x0020_Date,
badge.Leave_x0020_Date,
badge.Reason_x0020_for_x0020_Visit,
badge.Remarks,
badge.NumOfLocation,
badge.Site_x0020_NameId,
site.Title Site_x0020_NameTitle,

badge.Location_x0020_1Id,
loc1.Title Location_x0020_1Title,

badge.Area1Id,

badge.Location_x0020_2Id,
loc2.Title Location_x0020_2Title,

badge.Area2Id,

badge.Location_x0020_3Id,
loc3.Title Location_x0020_3Title,

badge.Area3Id,

badge.Location_x0020_4Id,
loc4.Title Location_x0020_4Title,

badge.Area4Id,

badge.Location_x0020_5Id,
loc5.Title Location_x0020_5Title,

badge.Area5Id,

badge.Location_x0020_6Id,
loc6.Title Location_x0020_6Title,

badge.Area6Id,

badge.Location_x0020_7Id,
loc7.Title Location_x0020_7Title,

badge.Area7Id,
badge.SAApprover1Id,

badge.SAApprover2Id,

badge.SAApprover3Id,

badge.SAApprover4Id,

badge.SAApprover5Id,

badge.SAApprover6Id,

badge.SAApprover7Id,
badge.SADeputy1Id,
badge.SADeputy2Id,

badge.SADeputy3Id,

badge.SADeputy4Id,

badge.SADeputy5Id,

badge.SADeputy6Id,

badge.SADeputy7Id,
badge.SAStatus1,
badge.SAStatus2,

badge.SAStatus3,

badge.SAStatus4,

badge.SAStatus5,

badge.SAStatus6,

badge.SAStatus7,
badge.Date_x0020_From_x0020_1,

badge.Date_x0020_From_x0020_2,

badge.Date_x0020_From_x0020_3,

badge.Date_x0020_From_x0020_4,

badge.Date_x0020_From_x0020_5,

badge.Date_x0020_From_x0020_6,

badge.Date_x0020_From_x0020_7,

badge.Date_x0020_To_x0020_1,

badge.Date_x0020_To_x0020_2,

badge.Date_x0020_To_x0020_3,

badge.Date_x0020_To_x0020_4,

badge.Date_x0020_To_x0020_5,

badge.Date_x0020_To_x0020_6,

badge.Date_x0020_To_x0020_7,
badge.NumOfVisitor,
badge.First_x0020_Name_x0020_1,
badge.First_x0020_Name_x0020_2,
badge.First_x0020_Name_x0020_3,
badge.First_x0020_Name_x0020_4,
badge.First_x0020_Name_x0020_5,
badge.Last_x0020_Name_x0020_1,
badge.Last_x0020_Name_x0020_2,
badge.Last_x0020_Name_x0020_3,
badge.Last_x0020_Name_x0020_4,
badge.Last_x0020_Name_x0020_5,
badge.Visitor_x0020_Company_x0020_1,
badge.Visitor_x0020_Mobile_x0020_1,
badge.Visitor_x0020_Company_x0020_2,
badge.Visitor_x0020_Mobile_x0020_2,
badge.Visitor_x0020_Company_x0020_3,
badge.Visitor_x0020_Mobile_x0020_3,
badge.Visitor_x0020_Company_x0020_4,
badge.Visitor_x0020_Mobile_x0020_4,
badge.Visitor_x0020_Company_x0020_5,
badge.Visitor_x0020_Mobile_x0020_5
from
VisitorBadge badge
left join BadgeUser cca on badge.CCApproverId=cca.ID and cca.IsEnable=1
left join BadgeUser ccd on badge.CCDeputyId=ccd.ID and ccd.IsEnable=1
left join BadgeUser en on badge.Employee_x0020_NameId=en.ID and en.IsEnable=1
left join Site site on badge.Site_x0020_NameId=site.ID and site.IsEnable=1
left join Location loc1 on badge.Location_x0020_1Id=loc1.ID and loc1.IsEnable=1
left join Location loc2 on badge.Location_x0020_2Id=loc2.ID and loc2.IsEnable=1
left join Location loc3 on badge.Location_x0020_3Id=loc3.ID and loc3.IsEnable=1
left join Location loc4 on badge.Location_x0020_4Id=loc4.ID and loc4.IsEnable=1
left join Location loc5 on badge.Location_x0020_5Id=loc5.ID and loc5.IsEnable=1
left join Location loc6 on badge.Location_x0020_6Id=loc6.ID and loc6.IsEnable=1
left join Location loc7 on badge.Location_x0020_7Id=loc7.ID and loc7.IsEnable=1
where badge.ID=@ID;
select ID,Title from Area where ID in (
        select distinct  split.xmlTable.value('.', 'int') as xmlValue
        from (
           select cast(('<w>' + replace(right(LEFT(Area1Id,LEN(Area1Id)-1),LEN(Area1Id)-2)+','+
        right(LEFT(Area2Id,LEN(Area2Id)-1),LEN(Area2Id)-2)+','+
        right(LEFT(Area3Id,LEN(Area3Id)-1),LEN(Area3Id)-2)+','+
        right(LEFT(Area4Id,LEN(Area4Id)-1),LEN(Area4Id)-2)+','+
        right(LEFT(Area5Id,LEN(Area5Id)-1),LEN(Area5Id)-2)+','+
        right(LEFT(Area6Id,LEN(Area6Id)-1),LEN(Area6Id)-2)+','+
        right(LEFT(Area7Id,LEN(Area7Id)-1),LEN(Area7Id)-2), ',', '</w><w>') + '</w>') as xml) as xmlValue
                 from VisitorBadge
                 where ID=@ID
        ) as xmlTable
        cross apply xmlValue.nodes ('/w') as split(xmlTable)
);
end