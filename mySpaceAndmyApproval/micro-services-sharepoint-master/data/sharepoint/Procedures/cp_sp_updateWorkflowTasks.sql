--drop proc cp_sp_updateWorkflowTasks;
create proc cp_sp_updateWorkflowTasks(
@infoes ut_WorkflowTasks readonly
)
as
begin
merge into WorkflowTasks t
using @infoes s
on t.ID=s.ID

when matched then
update set
        t.Status=s.Status,
        t.AssignedTo=s.AssignedTo,
        t.WorkflowOutcome=s.WorkflowOutcome,
        t.WorkflowName=s.WorkflowName,
        t.WorkflowItemId=s.WorkflowItemId,
        t.ApprovalOutcome=s.ApprovalOutcome,
        t.Decision=s.Decision,
        t.ApproverComments=s.ApproverComments,
        t.Created=s.Created,
		t.IsEnable=1
when not matched then
insert(
ID,
        Status,
        AssignedTo,
        WorkflowOutcome,
        WorkflowName,
        WorkflowItemId,
        ApprovalOutcome,
        Decision,
        ApproverComments,
        Created,
		IsEnable
)
values(
s.ID,
        s.Status,
        s.AssignedTo,
        s.WorkflowOutcome,
        s.WorkflowName,
        s.WorkflowItemId,
        s.ApprovalOutcome,
        s.Decision,
        s.ApproverComments,
        s.Created,
		1
)
;

end