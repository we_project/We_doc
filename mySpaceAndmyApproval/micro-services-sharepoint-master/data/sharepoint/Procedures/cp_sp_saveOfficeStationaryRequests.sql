--use MicroServicesSharepoint;
--drop proc cp_sp_saveOfficeStationaryRequests
create proc cp_sp_saveOfficeStationaryRequests(
@infoes ut_OfficeStationaryRequests readonly
)
as
begin
merge into OfficeStationaryRequests t
using @infoes s
on t.ID=s.ID
when matched then
update set
        t.Title=s.Title,
        t.UserName=s.UserName,
        t.CostCenter=s.CostCenter,
        t.Orders=s.Orders,
        t.Status=s.Status,
        t.ExtensionNumber=s.ExtensionNumber,
        t.Company=s.Company,
        t.LocationCode=s.LocationCode,
        t.Location=s.Location,
        t.FullAddress=s.FullAddress,
        t.Comments=s.Comments,
        t.ApproveOutCome=s.ApproveOutCome,
        t.ProcessFlow=s.ProcessFlow,
        t.LoginId=s.LoginId,
        t.AdminLocalManager=s.AdminLocalManager,
        t.AdminSeniorManager=s.AdminSeniorManager,
        t.AdminContact=s.AdminContact,
        t.CostCenterManager=s.CostCenterManager,
        t.CostCenterManagerName=s.CostCenterManagerName,
        t.DetailAddress=s.DetailAddress,
        t.CostCenterManagerPeople=s.CostCenterManagerPeople,
        t.TotalAmount=s.TotalAmount,
        t.Department=s.Department,
        t.AdminContactBackup=s.AdminContactBackup,
        t.Created=s.Created,
		t.Modified=s.Modified,
		t.IsEnable=1
when not matched then
insert(
ID,
        Title,
        UserName,
        CostCenter,
        Orders,
        Status,
        ExtensionNumber,
        Company,
        LocationCode,
        Location,
        FullAddress,
        Comments,
        ApproveOutCome,
        ProcessFlow,
        LoginId,
        AdminLocalManager,
        AdminSeniorManager,
        AdminContact,
        CostCenterManager,
        CostCenterManagerName,
        DetailAddress,
        CostCenterManagerPeople,
        TotalAmount,
        Department,
        AdminContactBackup,
        Created,
		Modified,
		IsEnable
)
values(
s.ID,
        s.Title,
        s.UserName,
        s.CostCenter,
        s.Orders,
        s.Status,
        s.ExtensionNumber,
        s.Company,
        s.LocationCode,
        s.Location,
        s.FullAddress,
        s.Comments,
        s.ApproveOutCome,
        s.ProcessFlow,
        s.LoginId,
        s.AdminLocalManager,
        s.AdminSeniorManager,
        s.AdminContact,
        s.CostCenterManager,
        s.CostCenterManagerName,
        s.DetailAddress,
        s.CostCenterManagerPeople,
        s.TotalAmount,
        s.Department,
        s.AdminContactBackup,
        s.Created,
		s.Modified,
		1
)
when not matched by source then

update set
t.IsEnable=0
;

end