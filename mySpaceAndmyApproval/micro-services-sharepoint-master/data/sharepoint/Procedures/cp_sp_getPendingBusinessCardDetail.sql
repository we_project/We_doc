--drop proc cp_sp_getPendingBusinessCardDetail
alter proc cp_sp_getPendingBusinessCardDetail(
@ID int
)
as
begin
	select top 1
bc.ID,
        bc.Title,
        bc.UserName,
        bc.EmployeeNumber,
        bc.CompanyName,
        bc.CostCenter,
        bc.ExtensionNumber,
        bc.FirstName,
        bc.LastName,
        bc.DepartmentName,
        bc.PhoneNo,
        bc.Email,
        bc.Position,
        bc.FixNo,
        bc.Address,
        bc.Modified,
        bc.Created,
        cast(bc.Business as nvarchar(max)) Business,
        bc.BusinessCardType,
        bc.FirstNameCN,
        bc.SecondNameCN,
        bc.DepartmentCN,
        bc.PositionCN,
        bc.AddressCN,
        bc.RequestorLoginName,
        bc.Supervisor,
        bc.State,
		location.Title Location,
        bc.EmployeeType,
        bc.HRKA,
        bc.cWorkflowHistory,
		u.Title AdminContact,
        bc.Overprint,
        cast(bc.Business0 as nvarchar(max)) Business0,
        bc.Remark
from
BusinessCardApplication bc
left join OSLocationAdminMapping location
on bc.Location=location.ID
left join CommonUser u
on bc.AdminContact=u.ID
	where bc.ID=@ID
end