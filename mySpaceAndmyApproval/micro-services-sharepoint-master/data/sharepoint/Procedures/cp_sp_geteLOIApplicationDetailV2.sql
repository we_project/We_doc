--drop proc cp_sp_geteLOIApplicationDetail
create proc cp_sp_geteLOIApplicationDetailV2(
@ID int
)
as
begin
	select top 1
il.ID,
        --il.Modified_x0020_By,
        --il.Created_x0020_By,
        --il._SourceUrl,
        --il._SharedFileIndex,
        --il.Title,
        --il.TemplateUrl,
        --il.xd_ProgID,
        --il.xd_Signature,
        --il.ShowRepairView,
        --il.ShowCombineView,
        il.User_x0020_Name,
        il.Employee_x0020_Number,
        il.Cost_x0020_Center,
        il.Extension_x0020_Number,
        il.Applicants_x0020_Company_x0020_Name,
        il.Applicants_x0020_Company_x0020_Address,
        il.JobTitle,
        il.Contact_x0020_in_x0020_Company,
        il.Entry_x0020_Port,
        --il.Cost_x0020_Center_x0020_Manager,
        --il.Form_x0020_Name,
        il.Last_x0020_Name,
        --il.Nationality,
        il.Birth_x0020_Date,
        --il.Passport,
        il.Entry_x0020_Date,
        il.Departure_x0020_Date,
        il.Purposeof_x0020_Visit,
        il.VISAType,
        il.Apply_x0020_Place,
        il.Contact_x0020_Phone,
        il.Relationship_x0020_With_x0020_Daimler,
        il.FirstName,
        il.Gender,
        --il.Stay_x0020_Days,
        il.Invitation_x0020_Status,
        --il.Form_x0020_Number,
        il.Company_x0020_Name,
        --il.Finished_x0020_Print,
         --cast(il.Invitati as nvarchar(255)) Invitati,
        --cast(il.CCManagerUser as nvarchar(255)) CCManagerUser,
		--u.Title CCManagerUser,
        --cast(il.DelegateUser as nvarchar(255)) DelegateUser,
		--u1.Title DelegateUser,
        il.Created,
        il.Modified
		--,
        --il.File_x0020_Size,
        --cast(il.CheckedOutUserId as nvarchar(255)) CheckedOutUserId,
        --il.IsCheckedoutToLocal,
        --u2.Title CheckoutUser,
        --il.VirusStatus,
        --il.CheckedOutTitle,
        --il._CheckinComment,
        --il.LinkCheckedOutTitle,
        --il.FileSizeDisplay,
        --il.SelectFilename,
        --il.ParentVersionString,
        --il.ParentLeafName,
        --il.DocConcurrencyNumber,
        --il.Combine,
        --il.RepairDocument
from
eLOIApplication il
left join CommonUser u on il.CCManagerUser=u.ID
left join CommonUser u1 on il.DelegateUser=u1.ID
left join CommonUser u2 on il.CheckoutUser=u2.ID

	where il.ID=@ID
end