create proc cp_sp_getPendingStationaryCount(
@userId int
)
as
begin 

select count(*) from OfficeStationaryRequests
where 
IsEnable=1
and (

( Status='CostCenterManagerApproveInProcess' and CostCenterManagerPeople =@userId)
or
(Status='AdminLocalManagerApproveInProcess' and AdminLocalManager=@userId)
or
(Status='AdminSeniorManagerApproveInProcess' and AdminSeniorManager=@userId)
)
end
