create proc cp_sp_getStationaryMenus(
@ids ut_intList readonly
)
as
begin
	select 
	m.ID,
	ItemName,
	ItemName_EN,
	Price_x0028_with_x0020_Tax_x0029_
	 from OfficeStationaryMenu m join @ids i on  m.ID=i.id
	 where IsEnable=1
end