create proc cp_sp_getPendingPCCACount(
@supervisor nvarchar(255)
)
as
begin 

select count(*) from PetrolCard_CreditAdjustment
where 
IsEnable=1
and 
Supervisor=@supervisor
and Status='Submitted'
end
