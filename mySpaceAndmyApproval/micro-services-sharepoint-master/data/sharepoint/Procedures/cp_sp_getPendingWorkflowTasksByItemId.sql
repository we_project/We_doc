alter proc cp_sp_getPendingWorkflowTasksByItemId(
@userId int,
@itemId int,
@workflowName nvarchar(255)
)
as 
begin
select ID from WorkflowTasks
where AssignedTo=@userId
and Status='Not Started'
and WorkflowName=@workflowName
and WorkflowItemId=@itemId
end