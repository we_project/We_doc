alter proc cp_sp_getEIBMDetail(
@ID int
)
as
begin
	select top 1
		eibm.ID,
eibm.Status,
eibm.CCApproverId,
cca.Title CCApproverTitle,
eibm.CCDeputyId,
ccd.Title CCDeputyTitle,
eibm.Application_x0020_Date,
eibm.Application_x0020_Type,
eibm.Employee_x0020_NameId,
en.Title Employee_x0020_NameTitle,
eibm.Title,
eibm.Employee_x0020_Number,
eibm.Company,
eibm.Department,
eibm.Deskphone_x0020_Number,
eibm.Cost_x0020_Center,
eibm.Badge_x0020_Type,
eibm.Remarks,
eibm.NumOfLocation,
eibm.Site_x0020_NameId,
site.Title Site_x0020_NameTitle,

eibm.Location_x0020_1Id,
loc1.Title Location_x0020_1Title,

eibm.Area1Id,

eibm.Location_x0020_2Id,
loc2.Title Location_x0020_2Title,

eibm.Area2Id,

eibm.Location_x0020_3Id,
loc3.Title Location_x0020_3Title,

eibm.Area3Id,

eibm.Location_x0020_4Id,
loc4.Title Location_x0020_4Title,

eibm.Area4Id,

eibm.Location_x0020_5Id,
loc5.Title Location_x0020_5Title,

eibm.Area5Id,

eibm.Location_x0020_6Id,
loc6.Title Location_x0020_6Title,

eibm.Area6Id,

eibm.Location_x0020_7Id,
loc7.Title Location_x0020_7Title,

eibm.Area7Id,
eibm.SAApprover1Id,

eibm.SAApprover2Id,

eibm.SAApprover3Id,

eibm.SAApprover4Id,

eibm.SAApprover5Id,

eibm.SAApprover6Id,

eibm.SAApprover7Id,
eibm.SADeputy1Id,
eibm.SADeputy2Id,

eibm.SADeputy3Id,

eibm.SADeputy4Id,

eibm.SADeputy5Id,

eibm.SADeputy6Id,

eibm.SADeputy7Id,
eibm.SAStatus1,
eibm.SAStatus2,

eibm.SAStatus3,

eibm.SAStatus4,

eibm.SAStatus5,

eibm.SAStatus6,

eibm.SAStatus7,

eibm.Always,

eibm.Always2,

eibm.Always3,

eibm.Always4,

eibm.Always5,

eibm.Always6,

eibm.Always7,
eibm.Date_x0020_From_x0020_1,

eibm.Date_x0020_From_x0020_2,

eibm.Date_x0020_From_x0020_3,

eibm.Date_x0020_From_x0020_4,

eibm.Date_x0020_From_x0020_5,

eibm.Date_x0020_From_x0020_6,

eibm.Date_x0020_From_x0020_7,

eibm.Date_x0020_To_x0020_1,

eibm.Date_x0020_To_x0020_2,

eibm.Date_x0020_To_x0020_3,

eibm.Date_x0020_To_x0020_4,

eibm.Date_x0020_To_x0020_5,

eibm.Date_x0020_To_x0020_6,

eibm.Date_x0020_To_x0020_7
from
EIBM eibm
left join BadgeUser cca on eibm.CCApproverId=cca.ID and cca.IsEnable=1
left join BadgeUser ccd on eibm.CCDeputyId=ccd.ID and ccd.IsEnable=1
left join BadgeUser en on eibm.Employee_x0020_NameId=en.ID and en.IsEnable=1
left join Site site on eibm.Site_x0020_NameId=site.ID and site.IsEnable=1
left join Location loc1 on eibm.Location_x0020_1Id=loc1.ID and loc1.IsEnable=1
left join Location loc2 on eibm.Location_x0020_2Id=loc2.ID and loc2.IsEnable=1
left join Location loc3 on eibm.Location_x0020_3Id=loc3.ID and loc3.IsEnable=1
left join Location loc4 on eibm.Location_x0020_4Id=loc4.ID and loc4.IsEnable=1
left join Location loc5 on eibm.Location_x0020_5Id=loc5.ID and loc5.IsEnable=1
left join Location loc6 on eibm.Location_x0020_6Id=loc6.ID and loc6.IsEnable=1
left join Location loc7 on eibm.Location_x0020_7Id=loc7.ID and loc7.IsEnable=1
where eibm.ID=@ID;
select ID,Title from Area where ID in (
	select distinct  split.xmlTable.value('.', 'int') as xmlValue
	from (
	   select cast(('<w>' + replace(right(LEFT(Area1Id,LEN(Area1Id)-1),LEN(Area1Id)-2)+','+
	right(LEFT(Area2Id,LEN(Area2Id)-1),LEN(Area2Id)-2)+','+
	right(LEFT(Area3Id,LEN(Area3Id)-1),LEN(Area3Id)-2)+','+
	right(LEFT(Area4Id,LEN(Area4Id)-1),LEN(Area4Id)-2)+','+
	right(LEFT(Area5Id,LEN(Area5Id)-1),LEN(Area5Id)-2)+','+
	right(LEFT(Area6Id,LEN(Area6Id)-1),LEN(Area6Id)-2)+','+
	right(LEFT(Area7Id,LEN(Area7Id)-1),LEN(Area7Id)-2), ',', '</w><w>') + '</w>') as xml) as xmlValue
		 from EIBM
		 where ID=@ID
	) as xmlTable
	cross apply xmlValue.nodes ('/w') as split(xmlTable)
);
end