--drop proc cp_sp_getPetrolCard_CreditAdjustmentDetailV2
alter proc cp_sp_getPetrolCard_CreditAdjustmentDetailV2(
@ID int
)
as
begin
select
pcca.ID,
        --pcca.Title,
        u.Title UserName,
        EmployeeNumber,
        CompanyName,
        CostCenter,
        RequestAmount,
        AdjustmentReason,
        ExtensionNumber,
        PlateNumber,
        CurrentCredit,
        Status,
        --Comments,
        --Supervisor,
        --CardNo,
        --u.Title DisplayName,
        --PetrolCa,
        Modified,
        Created
from
PetrolCard_CreditAdjustment pcca
left join CommonUser u on u.AccountName=pcca.UserName
where 
pcca.IsEnable=1
and pcca.ID=@ID
end