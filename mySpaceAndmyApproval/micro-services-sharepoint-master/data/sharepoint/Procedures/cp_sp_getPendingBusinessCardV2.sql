--drop proc cp_sp_getPendingBusinessCardV2
alter proc cp_sp_getPendingBusinessCardV2(
@userId int,
@pageIndex int=null,
@pageSize int=null
)
as
begin
if @pageIndex is not null
begin
	declare @offset int;
	set @offset=@pageIndex*@pageSize;
	select
	bc.ID,
			--bc.Title,
			bc.UserName,
			--bc.EmployeeNumber,
			--bc.CompanyName,
			--bc.CostCenter,
			--bc.ExtensionNumber,
			--bc.FirstName,
			--bc.LastName,
			bc.DepartmentName,
			--bc.PhoneNo,
			--bc.Email,
			--bc.Position,
			--bc.FixNo,
			--bc.Address,
			bc.Modified,
			bc.Created,
			--cast(bc.Business as nvarchar(max)) Business,
			--bc.BusinessCardType,
			--bc.FirstNameCN,
			--bc.SecondNameCN,
			--bc.DepartmentCN,
			--bc.PositionCN,
			--bc.AddressCN,
			--bc.RequestorLoginName,
			--bc.Supervisor,
			bc.State
			--,
			--location.Title Location,
			--bc.EmployeeType,
			--bc.HRKA,
			--bc.cWorkflowHistory,
			--u.Title AdminContact,
			--bc.Overprint,
			--cast(bc.Business0 as nvarchar(max)) Business0,
			--bc.Remark
	from
	BusinessCardApplication bc
	left join WorkflowTasks wt on wt.WorkflowItemId=bc.ID
	left join OSLocationAdminMapping location on bc.Location=location.ID
	left join CommonUser u on bc.AdminContact=u.ID
	where 
	wt.IsEnable=1
	and bc.IsEnable=1
	and wt.Status='Not Started'
	and wt.AssignedTo =@userId
	and wt.WorkflowName='BusinessCardApproveProcess'
	and (wt.IsApprovedFromOurSide is null or wt.IsApprovedFromOurSide!=1)
	order by bc.Created offset @offset rows fetch next @pageSize rows only;
end
else
begin
	select
	bc.ID,
			--bc.Title,
			bc.UserName,
			--bc.EmployeeNumber,
			--bc.CompanyName,
			--bc.CostCenter,
			--bc.ExtensionNumber,
			--bc.FirstName,
			--bc.LastName,
			bc.DepartmentName,
			--bc.PhoneNo,
			--bc.Email,
			--bc.Position,
			--bc.FixNo,
			--bc.Address,
			bc.Modified,
			bc.Created,
			--cast(bc.Business as nvarchar(max)) Business,
			--bc.BusinessCardType,
			--bc.FirstNameCN,
			--bc.SecondNameCN,
			--bc.DepartmentCN,
			--bc.PositionCN,
			--bc.AddressCN,
			--bc.RequestorLoginName,
			--bc.Supervisor,
			bc.State
			--,
			--location.Title Location,
			--bc.EmployeeType,
			--bc.HRKA,
			--bc.cWorkflowHistory,
			--u.Title AdminContact,
			--bc.Overprint,
			--cast(bc.Business0 as nvarchar(max)) Business0,
			--bc.Remark
	from
	BusinessCardApplication bc
	left join WorkflowTasks wt on wt.WorkflowItemId=bc.ID
	left join OSLocationAdminMapping location on bc.Location=location.ID
	left join CommonUser u on bc.AdminContact=u.ID
	where 
	wt.IsEnable=1
	and bc.IsEnable=1
	and wt.Status='Not Started'
	and wt.AssignedTo =@userId
	and wt.WorkflowName='BusinessCardApproveProcess'
	and (wt.IsApprovedFromOurSide is null or wt.IsApprovedFromOurSide!=1)
	order by bc.Created
end

end