alter proc cp_sp_getPendingBCCount(
@userId int
)
as
begin 

select count(*) from WorkflowTasks
where 
IsEnable=1
and 
Status='Not Started'
and AssignedTo =@userId
and WorkflowName='BusinessCardApproveProcess'
and (IsApprovedFromOurSide is null or IsApprovedFromOurSide!=1)
;
end
