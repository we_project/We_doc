create proc cp_sp_saveOfficeStationaryMenu(
@infoes ut_OfficeStationaryMenu readonly
)
as
begin
merge into OfficeStationaryMenu t
using @infoes s
on t.ID=s.ID
when matched then
update set
        t.ThumbnailExists=s.ThumbnailExists,
        t.ItemName=s.ItemName,
        t.Price_x0028_with_x0020_Tax_x0029_=s.Price_x0028_with_x0020_Tax_x0029_,
        t.ItemName_EN=s.ItemName_EN,
		t.EncodedAbsThumbnailUrl=s.EncodedAbsThumbnailUrl,
        t.EncodedAbsWebImgUrl=s.EncodedAbsWebImgUrl,
		t.IsEnable=1
when not matched then
insert(
         ThumbnailExists,
        ItemName,
        Price_x0028_with_x0020_Tax_x0029_,
        ItemName_EN,
        ID,
		EncodedAbsThumbnailUrl,
        EncodedAbsWebImgUrl,
		IsEnable
)
values(
         s.ThumbnailExists,
        s.ItemName,
        s.Price_x0028_with_x0020_Tax_x0029_,
        s.ItemName_EN,
        s.ID,
		s.EncodedAbsThumbnailUrl,
        s.EncodedAbsWebImgUrl,
		1
)
when not matched by source then

update set
t.IsEnable=0
;

end