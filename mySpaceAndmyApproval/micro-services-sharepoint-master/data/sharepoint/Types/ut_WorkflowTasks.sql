--drop type ut_WorkflowTasks
create type  ut_WorkflowTasks as table(
ID int not null,
        Status nvarchar(255) null,
        AssignedTo int null,
        WorkflowOutcome nvarchar(255) null,
        WorkflowName nvarchar(255) null,
        WorkflowItemId int null,
        ApprovalOutcome int null,
        Decision int null,
        ApproverComments nvarchar(max) null,
        Created datetime null
)