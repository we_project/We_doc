--drop type ut_FormBCommentHistory;
 create type ut_FormBCommentHistory as table(
  ID int not null,
        Title nvarchar(255) null,
        CaseID nvarchar(255) null,
        UserStr nvarchar(255) null,
        Comment nvarchar(max) null,
        Modified datetime null,
        Created datetime null
)