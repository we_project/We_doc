create type ut_FormACommentHistory as table(
  ID int not null,
        Title nvarchar(255) null,
        CaseID nvarchar(255) null,
        Comment nvarchar(max) null,
        UserStr nvarchar(255) null,
        Modified datetime null,
        Created datetime null
)