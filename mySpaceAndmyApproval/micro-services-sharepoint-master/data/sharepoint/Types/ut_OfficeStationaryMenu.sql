create type  ut_OfficeStationaryMenu as table(
        ID int not null,
        ThumbnailExists bit null,
        ItemName nvarchar(255) null,
        Price_x0028_with_x0020_Tax_x0029_ decimal null,
        ItemName_EN nvarchar(255) null,
 EncodedAbsThumbnailUrl nvarchar(max) null,
        EncodedAbsWebImgUrl nvarchar(max) null
)