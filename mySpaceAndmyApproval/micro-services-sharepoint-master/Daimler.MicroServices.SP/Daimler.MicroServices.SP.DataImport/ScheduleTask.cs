﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using log4net;

namespace Daimler.MicroServices.SP.DataImport {
    class ScheduleTask {
        static readonly ILog log = LogManager.GetLogger(typeof(ScheduleTask));
        static Action runTask;
        static Timer timer;
        static object timerLock = new object();
        public static void Schedule(Action task) {
            runTask = task;
            timer = new Timer(TimerMethod, null, 0, ScheduleConfig.TimerSchedule.Deafult.Period);
        }
        static void TimerMethod(object state) {
            if (runTask != null) {
                try {
                    lock (timerLock) {
                        log.Info("Begin Task");
                        try {
                            runTask();
                        } catch (Exception ex) {
                            log.Error("runTask exception", ex);
                        }
                        AdjustTime();
                        log.Info("End Task");
                    }
                } catch (Exception ex) {
                    log.Error("Timer Method Exception", ex);
                }
            }
        }
        static void AdjustTime() {

            ScheduleConfig.TimerSchedule newSchedule = ScheduleConfig.NewSchedule;
            if (newSchedule != null) {
                timer.Change(newSchedule.Due, newSchedule.Period);
                log.InfoFormat("next time run task:{0}\nnew period in minute:{1}",
                                 DateTime.Now.AddMilliseconds(newSchedule.Due),
                                 newSchedule.Period / 1000 / 60);
            }
        }
        public static void StopSchedule() {
            if (timer != null) {
                timer.Change(Timeout.Infinite, Timeout.Infinite);
                timer.Dispose();
                timer = null;
            }
        }
    }
}
