﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.DataImport.BLL;
using log4net;

namespace Daimler.MicroServices.SP.DataImport {
    partial class SharepointService : ServiceBase {
        private static readonly ILog log = LogManager.GetLogger(typeof(SharepointService));
        public SharepointService() {
            InitializeComponent();
        }

        protected override void OnStart(string[] args) {
            try {
                log.Info("Start service");
                ScheduleTask.Schedule(SPService.LoadData);
            } catch (Exception ex) {
                log.Error(this, ex);
                throw;
            }

        }

        protected override void OnStop() {
            try {
                ScheduleTask.StopSchedule();
                log.Info("Stop service");
            } catch (Exception ex) {
                log.Error(this, ex);
                throw;
            }
        }
    }
}
