﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.BusinessCard;
using Daimler.MicroServices.SPAPI.IDAL;
using Daimler.MicroServices.SPAPI.SqlServerDAL;

namespace Daimler.MicroServices.SPAPI.BLL {
    public class BusinessCardApplication {
        static IBusinessCardApplication osRequest = DAL.DefaultBusinessCardApplication;

        public static bool SaveRequests(List<BCApplicationInfo> infoes) {
            return osRequest.SaveInfoes(infoes);
        }

        public static bool UpdateRequests(List<BCApplicationInfo> infoes) {
            return osRequest.UpdateRequests(infoes);

        }
        public static List<BusinessCardApplicationItem> GetBCApplicatoinList(int userId, int? pageIndex, int? pageSize) {
            return osRequest.GetBCApplicatoinList(userId, pageIndex, pageSize);
        }
        public static BusinessCardApplicationDetail GetBCApplicatoinDetail(int ID) {

            return osRequest.GetBCApplicatoinDetail(ID);
        }
    }
}
