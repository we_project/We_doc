﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.InvitationLetter;
using Daimler.MicroServices.SPAPI.IDAL;
using Daimler.MicroServices.SPAPI.SqlServerDAL;

namespace Daimler.MicroServices.SPAPI.BLL {
  public  class eLOIApplication {
      static IeLOIApplication osRequest = DAL.DefaulteLOIApplication;

      public static bool SaveRequests(List<eLOIApplicationInfo> infoes) {
            return osRequest.SaveInfoes(infoes);
        }

      public static bool UpdateRequests(List<eLOIApplicationInfo> infoes) {
            return osRequest.UpdateRequests(infoes);

        }
      public static List<InvitationLetterApplicationItem> GetILApplicatoinList(int userId, int? pageIndex, int? pageSize) {
            return osRequest.GetILApplicatoinList(userId, pageIndex, pageSize);
        }
      public static InvitationLetterApplicationDetail GetILApplicatoinDetail(int ID) {

            return osRequest.GetILApplicatoinDetail(ID);
        }
    }
}
