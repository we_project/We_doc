﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.OfficeStationary;
using Daimler.MicroServices.SPAPI.IDAL;
using Daimler.MicroServices.SPAPI.SqlServerDAL;

namespace Daimler.MicroServices.SPAPI.BLL {
    public class OSLocationAdminMapping {
        static IOSLocationAdminMapping osLocation = DAL.DefaultOSLocationAdminMapping;

        public static bool SaveRequests(List<OSLocationInfo> infoes) {
            return osLocation.SaveInfoes(infoes);
        }
    }
}
