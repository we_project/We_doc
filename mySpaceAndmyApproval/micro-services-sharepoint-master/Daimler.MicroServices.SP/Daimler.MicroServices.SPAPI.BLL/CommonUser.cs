﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SPAPI.IDAL;
using Daimler.MicroServices.SPAPI.SqlServerDAL;

namespace Daimler.MicroServices.SPAPI.BLL {
    public class CommonUser {
        static ICommonUser dal = DAL.DefaultCommonUser;

        public static bool SaveUsers(List<SPUserInfo> infoes) {
            return dal.SaveInfoes(infoes);
        }

        public static int? GetUserId(string accountName) {
            return dal.GetUserId(accountName);
        }
    }
}
