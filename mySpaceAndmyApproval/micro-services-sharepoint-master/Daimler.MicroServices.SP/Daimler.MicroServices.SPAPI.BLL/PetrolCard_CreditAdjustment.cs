﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.PetrolCard;
using Daimler.MicroServices.SPAPI.IDAL;
using Daimler.MicroServices.SPAPI.SqlServerDAL;

namespace Daimler.MicroServices.SPAPI.BLL {
 public   class PetrolCard_CreditAdjustment {
        static IPetrolCard_CreditAdjustment osRequest = DAL.DefaultPetrolCard_CreditAdjustment;

        public static bool SaveInfoes(List<PCCreditAdjustmentInfo> infoes) {
            return osRequest.SaveInfoes(infoes);
        }

        public static bool Update(List<PCCreditAdjustmentInfo> infoes) {
            return osRequest.UpdateRequests(infoes);

        }

        public static int GetPendingCount(string supervisor) {
            return osRequest.GetPendingCount(supervisor);
        }
        public static List<PCCreditAdjustmentItem> GetPCCAApplicatoinList(string supervisor, int? pageIndex, int? pageSize) {
            return osRequest.GetPCCAApplicatoinList(supervisor, pageIndex, pageSize);
        }
        public static PCCreditAdjustmentDetail GetPCCAApplicatoinDetail(int ID) {

            return osRequest.GetPCCAApplicatoinDetail(ID);
        }
    }
}
