﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.OfficeStationary;
using Daimler.MicroServices.SPAPI.IDAL;
using Daimler.MicroServices.SPAPI.SqlServerDAL;

namespace Daimler.MicroServices.SPAPI.BLL {
   public class OfficeStationaryMenu {
        static IOfficeStationaryMenu osRequest = DAL.DefaultOfficeStationaryMenu;

        public static bool SaveMenus(List<StationaryMenuInfo> infoes) {
            return osRequest.SaveInfoes(infoes);
        }

        public static List<StationaryDetail> GetMenus(List<int> ids) {
            return osRequest.GetList(ids);
        }
    }
}
