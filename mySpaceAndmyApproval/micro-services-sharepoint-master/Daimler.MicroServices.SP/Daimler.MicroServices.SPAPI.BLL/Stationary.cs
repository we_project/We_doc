﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.OfficeStationary;
using Daimler.MicroServices.SPAPI.IDAL;
using Daimler.MicroServices.SPAPI.SqlServerDAL;

namespace Daimler.MicroServices.SPAPI.BLL {
    public class Stationary {
        static IOfficeStationaryRequests osRequest = DAL.DefaultOfficeStationaryRequests;

        public static int GetPendingCount(int userId) {
            return osRequest.GetPendingStationaryCount(userId);
        }
        public static List<StationaryRequestItem> GetPendingStationaryList(int userId, int? pageIndex, int? pageSize) {
            return osRequest.GetPendingStationaryList(userId, pageIndex, pageSize);
        }

        public static APIStationaryRequestDetail GetStationaryDetail(int ID) {
            return osRequest.GetStationaryDetail(ID);
        }
    }
}
