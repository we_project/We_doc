﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.AccessManagement;
using Daimler.MicroServices.SPAPI.IDAL;
using Daimler.MicroServices.SPAPI.SqlServerDAL;

namespace Daimler.MicroServices.SPAPI.BLL {
   public class VisitorBadge {
        static IVisitorBadge osRequest = DAL.DefaultVisitorBadge;

        public static bool SaveRequests(List<VistorBadgeApplicationInfo> infoes) {
            return osRequest.SaveInfoes(infoes);
        }

        public static int? GetPendingCount(int userId) {
            return osRequest.GetPendingCount(userId);
        }

        public static List<VisitorBadgeItem> GetPendingList(int userId) {
            return osRequest.GetPendingList(userId);
        }

        public static VisitorBadgeDetail1 GetDetail(int id) {
            return osRequest.GetDetail(id);
        }

        public static bool UpdateRequests(List<VistorBadgeApplicationInfo> infoes) {
            return osRequest.UpdateRequests(infoes);
        }
    }
}
