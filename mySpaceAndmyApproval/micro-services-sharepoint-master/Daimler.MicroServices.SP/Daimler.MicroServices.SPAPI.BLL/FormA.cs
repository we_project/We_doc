﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.FormAB;
using Daimler.MicroServices.SPAPI.IDAL;
using Daimler.MicroServices.SPAPI.SqlServerDAL;

namespace Daimler.MicroServices.SPAPI.BLL {
    public class FormA {
        static IFormA osRequest = DAL.DefaultFormA;
        static IFormACommentHistory formACommentHistory = DAL.DefaultFormACommentHistory;

        public static bool SaveRequests(List<FormAInfo> infoes) {
            return osRequest.SaveInfoes(infoes);
        }

        public static bool UpdateRequests(List<FormAInfo> infoes) {
            return osRequest.UpdateRequests(infoes);

        }
        public static bool SaveCommentHistory(List<FormACommentHistoryInfo> infoes) {
            return formACommentHistory.SaveInfoes(infoes);
        }
        public static int GetPendingCount(int userId) {
            return osRequest.GetPendingCount(userId);
        }
        public static List<FormAItem> GetList(int userId, int? pageIndex, int? pageSize) {
            return osRequest.GetList(userId, pageIndex, pageSize);
        }
        public static FormADetail GetDetail(int ID) {
            Task<FormADetail> t1 = Task.Factory.StartNew<FormADetail>(() => {
                return osRequest.GetDetail(ID);
            });
            Task<List<FormAComment>> t2 = Task.Factory.StartNew<List<FormAComment>>(() => {
                return formACommentHistory.GetCommentList(ID);
            });
            Task.WaitAll(t1, t2);

            FormADetail detail = t1.Result;
            detail.CommentHistory = t2.Result;
            return detail;
        }
    }
}
