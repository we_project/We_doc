﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.AccessManagement;
using Daimler.MicroServices.SPAPI.IDAL;
using Daimler.MicroServices.SPAPI.SqlServerDAL;

namespace Daimler.MicroServices.SPAPI.BLL {
   public class Location {
       static ILocation osRequest = DAL.DefaultLocation;

       public static bool SaveRequests(List<LocationInfo> infoes) {
           return osRequest.SaveInfoes(infoes);
       }
    }
}
