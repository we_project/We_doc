﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.Workflow;
using Daimler.MicroServices.SPAPI.IDAL;
using Daimler.MicroServices.SPAPI.SqlServerDAL;

namespace Daimler.MicroServices.SPAPI.BLL {
   public class WorkflowTasks {
       static IWorkflowTasks osRequest = DAL.DefaultWorkflowTasks;

       public static bool SaveRequests(List<WorkflowTaskInfo> infoes) {
            return osRequest.SaveInfoes(infoes);
        }

       public static bool UpdateRequests(List<WorkflowTaskInfo> infoes) {
            return osRequest.UpdateRequests(infoes);

        }

       public static int GetPendingBCCount(int userId) {
           return osRequest.GetPendingBCCount(userId);
       }
       public static int GetPendingILCount(int userId) {
           return osRequest.GetPendingILCount(userId);
       }
       public static int? GetPendingWorkflowTasksByItemId(int userId, int itemId, string workflowName) {
           return osRequest.GetPendingWorkflowTasksByItemId(userId, itemId, workflowName);
       }


       public static bool MarkApproved(int ID) {
           return osRequest.MarkApproved(ID);
       }
    }
}
