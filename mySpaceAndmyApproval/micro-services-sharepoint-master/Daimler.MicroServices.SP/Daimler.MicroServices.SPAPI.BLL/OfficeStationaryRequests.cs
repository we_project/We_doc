﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.OfficeStationary;
using Daimler.MicroServices.SPAPI.IDAL;
using Daimler.MicroServices.SPAPI.SqlServerDAL;

namespace Daimler.MicroServices.SPAPI.BLL {
   public class OfficeStationaryRequests {
        static IOfficeStationaryRequests osRequest = DAL.DefaultOfficeStationaryRequests;

        public static bool SaveRequests(List<StationaryRequestInfo> infoes) {
            return osRequest.SaveInfoes(infoes);
        }

        public static bool UpdateRequests(List<StationaryRequestInfo> infoes) {
            return osRequest.UpdateRequests(infoes);

        }

    }
}
