﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.FormAB;
using Daimler.MicroServices.SPAPI.IDAL;
using Daimler.MicroServices.SPAPI.SqlServerDAL;

namespace Daimler.MicroServices.SPAPI.BLL {
   public class FormB {
       static IFormB osRequest = DAL.DefaultFormB;
       static IFormBCommentHistory FormBCommentHistory = DAL.DefaultFormBCommentHistory;

       public static bool SaveRequests(List<FormBInfo> infoes) {
           return osRequest.SaveInfoes(infoes);
       }

       public static bool UpdateRequests(List<FormBInfo> infoes) {
           return osRequest.UpdateRequests(infoes);

       }
       public static bool SaveCommentHistory(List<FormBCommentHistoryInfo> infoes) {
           return FormBCommentHistory.SaveInfoes(infoes);
       }
       public static int GetPendingCount(int userId) {
           return osRequest.GetPendingCount(userId);
       }
       public static List<FormBItem> GetList(int userId, int? pageIndex, int? pageSize) {
           return osRequest.GetList(userId, pageIndex, pageSize);
       }
       public static FormBDetail GetDetail(int ID) {
           Task<FormBDetail> t1 = Task.Factory.StartNew<FormBDetail>(() => {
               return osRequest.GetDetail(ID);
           });
           Task<List<FormBComment>> t2 = Task.Factory.StartNew<List<FormBComment>>(() => {
               return FormBCommentHistory.GetCommentList(ID);
           });
           Task.WaitAll(t1,t2);

           FormBDetail detail = t1.Result;
           detail.CommentHistory = t2.Result;
           return detail;
       }
    }
}
