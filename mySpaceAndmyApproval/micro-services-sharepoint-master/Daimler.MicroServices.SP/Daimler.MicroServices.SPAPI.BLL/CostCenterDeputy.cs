﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.AccessManagement;
using Daimler.MicroServices.SPAPI.IDAL;
using Daimler.MicroServices.SPAPI.SqlServerDAL;

namespace Daimler.MicroServices.SPAPI.BLL {
    public class CostCenterDeputy {
        static ICostCenterDeputyProfile osRequest = DAL.DefaultCCDeputy;

        public static bool SaveRequests(List<CostCenterDeputyProfileInfo> infoes) {
            return osRequest.SaveInfoes(infoes);
        }

        public static List<CostCenterDeputyProfileInfo> GetCCDeputys(int deputyId) {
            return osRequest.GetCCDeputys(deputyId);
        }
    }
}
