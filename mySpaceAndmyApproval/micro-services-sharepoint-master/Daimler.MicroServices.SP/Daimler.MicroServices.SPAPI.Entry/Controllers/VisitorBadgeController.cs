﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.AccessManagement;
using Daimler.MicroServices.SPAPI.BLL;
using log4net;

namespace Daimler.MicroServices.SPAPI.Entry.Controllers
{
    [RoutePrefix("api/VisitorBadge")]
    public class VisitorBadgeController : ApiController
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(VisitorBadgeController));

        [Route("")]
        [HttpPost]
        public ReturnBoolResult Save([FromBody] List<VistorBadgeApplicationInfo> infoes) {
            ReturnBoolResult result = new ReturnBoolResult();
            try {
                if (infoes == null || infoes.Count == 0) {
                    result.msg = ReturnMsgSP.RequiredParameterIsNull;
                    return result;
                }
                result.boolResult = VisitorBadge.SaveRequests(infoes);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }
        [Route("Update")]
        [HttpPost]
        public ReturnBoolResult Update([FromBody] List<VistorBadgeApplicationInfo> infoes) {
            ReturnBoolResult result = new ReturnBoolResult();
            try {
                if (infoes == null || infoes.Count == 0) {
                    result.msg = ReturnMsgSP.RequiredParameterIsNull;
                    return result;
                }
                result.boolResult = VisitorBadge.UpdateRequests(infoes);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }
        [Route("{userId:int}/PendingCount")]
        [HttpGet]
        public ReturnIntResult GetPendingCount(int userId) {
            ReturnIntResult result = new ReturnIntResult();
            try {

                result.intResult = VisitorBadge.GetPendingCount(userId);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }
        [Route("{userId:int}/PendingList")]
        [HttpGet]
        public ReturnVisitorBadgePendingList GetPendingList(int userId) {
            ReturnVisitorBadgePendingList result = new ReturnVisitorBadgePendingList();
            try {
                result.list = VisitorBadge.GetPendingList(userId);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }

        [Route("{id:int}/Detail")]
        [HttpGet]
        public ReturnVisitorBadgeDetail GetILApplicatoinDetail(int id) {
            ReturnVisitorBadgeDetail result = new ReturnVisitorBadgeDetail();
            try {
                result.detail = VisitorBadge.GetDetail(id);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }
    }
}
