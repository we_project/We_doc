﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.Workflow;
using Daimler.MicroServices.SPAPI.BLL;
using log4net;

namespace Daimler.MicroServices.SPAPI.Entry.Controllers {
    [RoutePrefix("api/WorkflowTasks")]
    public class WorkflowTasksController : ApiController {
        private static readonly ILog log = LogManager.GetLogger(typeof(StationaryController));



        //[Route("Requests")]
        [HttpPost]
        public ReturnBoolResult Save([FromBody] List<WorkflowTaskInfo> infoes) {
            ReturnBoolResult result = new ReturnBoolResult();
            try {
                if (infoes == null || infoes.Count == 0) {
                    result.msg = ReturnMsgSP.RequiredParameterIsNull;
                    return result;
                }
                result.boolResult = WorkflowTasks.SaveRequests(infoes);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }
        [Route("Update")]
        [HttpPost]
        public ReturnBoolResult Update([FromBody] List<WorkflowTaskInfo> infoes) {
            ReturnBoolResult result = new ReturnBoolResult();
            try {
                if (infoes == null || infoes.Count == 0) {
                    result.msg = ReturnMsgSP.RequiredParameterIsNull;
                    return result;
                }
                result.boolResult = WorkflowTasks.UpdateRequests(infoes);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }
        [Route("{ID:int}/MarkApproved")]
        [HttpPost]
        public ReturnBoolResult MarkApproved(int ID) {
            ReturnBoolResult result = new ReturnBoolResult();
            try {
                result.boolResult = WorkflowTasks.MarkApproved(ID);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }

        [Route("{userId:int}/PendingBCCount")]
        [HttpGet]
        public ReturnIntResult GetPendingBCCount(int userId) {
            ReturnIntResult result = new ReturnIntResult();
            try {

                result.intResult = WorkflowTasks.GetPendingBCCount(userId);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }
        [Route("{userId:int}/PendingILCount")]
        [HttpGet]
        public ReturnIntResult GetPendingILCount(int userId) {
            ReturnIntResult result = new ReturnIntResult();
            try {

                result.intResult = WorkflowTasks.GetPendingILCount(userId);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }
        [Route("{userId:int}/{workflowName}/{itemId:int}/ID")]
        [HttpGet]
        public ReturnIntResult GetWTId(int userId, string workflowName, int itemId) {
            ReturnIntResult result = new ReturnIntResult();
            try {
                result.intResult = WorkflowTasks.GetPendingWorkflowTasksByItemId(userId, itemId, workflowName);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }
    }
}
