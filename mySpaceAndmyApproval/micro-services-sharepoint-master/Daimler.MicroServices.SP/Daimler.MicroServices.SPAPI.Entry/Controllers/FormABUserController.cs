﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SPAPI.BLL;
using log4net;

namespace Daimler.MicroServices.SPAPI.Entry.Controllers {
    [RoutePrefix("api/FormABUser")]
    public class FormABUserController : ApiController {
        private static readonly ILog log = LogManager.GetLogger(typeof(FormABUserController));
        [HttpPost]
        public ReturnBoolResult Save([FromBody] List<SPUserInfo> infoes) {
            ReturnBoolResult result = new ReturnBoolResult();
            try {
                if (infoes == null || infoes.Count == 0) {
                    result.msg = ReturnMsgSP.RequiredParameterIsNull;
                    return result;
                }
                result.boolResult = FormABUser.SaveUsers(infoes);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }
        [Route("id")]
        [HttpGet]
        public ReturnIntResult GetUserId(string AccountName) {
            ReturnIntResult result = new ReturnIntResult();
            try {
                if (string.IsNullOrWhiteSpace(AccountName)) {
                    result.msg = ReturnMsgSP.RequiredParameterIsNull;
                    return result;
                }
                result.intResult = FormABUser.GetUserId(AccountName);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }
        [Route("name")]
        [HttpGet]
        public ReturnStringResult GetUserName(string AccountName) {
            ReturnStringResult result = new ReturnStringResult();
            try {
                if (string.IsNullOrWhiteSpace(AccountName)) {
                    result.msg = ReturnMsgSP.RequiredParameterIsNull;
                    return result;
                }
                result.stringResult = FormABUser.GetUserName(AccountName);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }
    }
}
