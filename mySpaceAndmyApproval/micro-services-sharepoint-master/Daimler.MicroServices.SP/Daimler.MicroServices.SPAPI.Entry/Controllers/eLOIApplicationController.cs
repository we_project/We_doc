﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.InvitationLetter;
using Daimler.MicroServices.SPAPI.BLL;
using log4net;

namespace Daimler.MicroServices.SPAPI.Entry.Controllers
{
    [RoutePrefix("api/IL")]
    public class eLOIApplicationController : ApiController
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(eLOIApplicationController));

        [Route("")]
        [HttpPost]
        public ReturnBoolResult Save([FromBody] List<eLOIApplicationInfo> infoes) {
            ReturnBoolResult result = new ReturnBoolResult();
            try {
                if (infoes == null || infoes.Count == 0) {
                    result.msg = ReturnMsgSP.RequiredParameterIsNull;
                    return result;
                }
                result.boolResult = eLOIApplication.SaveRequests(infoes);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }
        [Route("Update")]
        [HttpPost]
        public ReturnBoolResult Update([FromBody] List<eLOIApplicationInfo> infoes) {
            ReturnBoolResult result = new ReturnBoolResult();
            try {
                if (infoes == null || infoes.Count == 0) {
                    result.msg = ReturnMsgSP.RequiredParameterIsNull;
                    return result;
                }
                result.boolResult = eLOIApplication.UpdateRequests(infoes);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }
        [Route("{userId:int}/PendingList")]
        [HttpGet]
        public ReturnInvitationLetterPendingList GetPendingList(int userId, int? pageIndex=null, int? pageSize=null) {
            ReturnInvitationLetterPendingList result = new ReturnInvitationLetterPendingList();
            try {
                result.list = eLOIApplication.GetILApplicatoinList(userId, pageIndex, pageSize);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }
        [Route("{id:int}/Detail")]
        [HttpGet]
        public ReturnInvitationLetterApplicationDetail GetILApplicatoinDetail(int id) {
            ReturnInvitationLetterApplicationDetail result = new ReturnInvitationLetterApplicationDetail();
            try {
                result.detail = eLOIApplication.GetILApplicatoinDetail(id);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }
    }
}
