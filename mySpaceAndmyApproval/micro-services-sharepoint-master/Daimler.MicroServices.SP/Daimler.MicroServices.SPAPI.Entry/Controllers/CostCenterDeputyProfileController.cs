﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.AccessManagement;
using Daimler.MicroServices.SPAPI.BLL;
using log4net;

namespace Daimler.MicroServices.SPAPI.Entry.Controllers
{
    [RoutePrefix("api/CCDeputy")]
    public class CostCenterDeputyProfileController : ApiController
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(CostCenterDeputyProfileController));
        [Route("")]
        [HttpPost]
        public ReturnBoolResult Save([FromBody] List<CostCenterDeputyProfileInfo> infoes) {
            ReturnBoolResult result = new ReturnBoolResult();
            try {
                if (infoes == null || infoes.Count == 0) {
                    result.msg = ReturnMsgSP.RequiredParameterIsNull;
                    return result;
                }
                result.boolResult = CostCenterDeputy.SaveRequests(infoes);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }

        [Route("{deputyId:int}")]
        [HttpGet]
        public ReturnCostCenterDeputyProfileResult GetCCDeputys(int deputyId) {
            ReturnCostCenterDeputyProfileResult result = new ReturnCostCenterDeputyProfileResult();
            try {
                result.list = CostCenterDeputy.GetCCDeputys(deputyId);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }
    }
}
