﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.AccessManagement;
using Daimler.MicroServices.SPAPI.BLL;
using log4net;

namespace Daimler.MicroServices.SPAPI.Entry.Controllers {
    [RoutePrefix("api/EIBM")]
    public class EIBMController : ApiController {
        private static readonly ILog log = LogManager.GetLogger(typeof(EIBMController));

        [Route("")]
        [HttpPost]
        public ReturnBoolResult Save([FromBody] List<EIBMInfo> infoes) {
            ReturnBoolResult result = new ReturnBoolResult();
            try {
                if (infoes == null || infoes.Count == 0) {
                    result.msg = ReturnMsgSP.RequiredParameterIsNull;
                    return result;
                }
                result.boolResult = EIBM.SaveRequests(infoes);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }
        [Route("Update")]
        [HttpPost]
        public ReturnBoolResult Update([FromBody] List<EIBMInfo> infoes) {
            ReturnBoolResult result = new ReturnBoolResult();
            try {
                if (infoes == null || infoes.Count == 0) {
                    result.msg = ReturnMsgSP.RequiredParameterIsNull;
                    return result;
                }
                result.boolResult = EIBM.UpdateRequests(infoes);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }
        [Route("{userId:int}/PendingCount")]
        [HttpGet]
        public ReturnIntResult GetPendingCount(int userId) {
            ReturnIntResult result = new ReturnIntResult();
            try {

                result.intResult = EIBM.GetPendingCount(userId);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }
        [Route("{userId:int}/PendingList")]
        [HttpGet]
        public ReturnEIBMPendingList GetPendingList(int userId) {
            ReturnEIBMPendingList result = new ReturnEIBMPendingList();
            try {
                result.list = EIBM.GetPendingList(userId);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }

        [Route("{id:int}/Detail")]
        [HttpGet]
        public ReturnEIBMDetail GetILApplicatoinDetail(int id) {
            ReturnEIBMDetail result = new ReturnEIBMDetail();
            try {
                result.detail = EIBM.GetDetail(id);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }
    }
}
