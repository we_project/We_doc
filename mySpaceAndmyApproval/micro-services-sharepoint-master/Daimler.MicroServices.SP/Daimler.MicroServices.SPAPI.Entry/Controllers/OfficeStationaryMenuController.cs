﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.OfficeStationary;
using Daimler.MicroServices.SPAPI.BLL;
using log4net;

namespace Daimler.MicroServices.SPAPI.Entry.Controllers {
    [RoutePrefix("api/Stationary")]
    public class OfficeStationaryMenuController : ApiController {
        private static readonly ILog log = LogManager.GetLogger(typeof(StationaryController));



        [Route("OSMenu")]
        [HttpPost]
        public ReturnBoolResult Save([FromBody] List<StationaryMenuInfo> infoes) {
            ReturnBoolResult result = new ReturnBoolResult();
            try {
                if (infoes == null || infoes.Count == 0) {
                    result.msg = ReturnMsgSP.RequiredParameterIsNull;
                    return result;
                }
                result.boolResult = OfficeStationaryMenu.SaveMenus(infoes);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }
        [Route("OSMenu/List")]
        [HttpGet]
        public ReturnStationaryDetailList GetList([FromUri] List<int> ids) {
            ReturnStationaryDetailList result = new ReturnStationaryDetailList();
            try {
                if (ids == null || ids.Count == 0) {
                    result.msg = ReturnMsgSP.RequiredParameterIsNull;
                    return result;
                }
                log.Debug("get menu list: " + string.Join(",", ids));
                result.details = OfficeStationaryMenu.GetMenus(ids);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }

    }
}
