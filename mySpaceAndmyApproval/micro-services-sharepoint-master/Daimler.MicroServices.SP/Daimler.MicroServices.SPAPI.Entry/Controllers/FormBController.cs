﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.FormAB;
using Daimler.MicroServices.SPAPI.BLL;
using log4net;

namespace Daimler.MicroServices.SPAPI.Entry.Controllers
{
    [RoutePrefix("api/FormB")]
    public class FormBController : ApiController
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(FormBController));

        [Route("")]
        [HttpPost]
        public ReturnBoolResult Save([FromBody] List<FormBInfo> infoes) {
            ReturnBoolResult result = new ReturnBoolResult();
            try {
                if (infoes == null || infoes.Count == 0) {
                    result.msg = ReturnMsgSP.RequiredParameterIsNull;
                    return result;
                }
                result.boolResult = FormB.SaveRequests(infoes);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }
        [Route("Update")]
        [HttpPost]
        public ReturnBoolResult Update([FromBody] List<FormBInfo> infoes) {
            ReturnBoolResult result = new ReturnBoolResult();
            try {
                if (infoes == null || infoes.Count == 0) {
                    result.msg = ReturnMsgSP.RequiredParameterIsNull;
                    return result;
                }
                result.boolResult = FormB.UpdateRequests(infoes);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }
        [Route("{userId:int}/Pending")]
        [HttpGet]
        public ReturnIntResult GetPending(int userId) {
            ReturnIntResult result = new ReturnIntResult();
            try {
                result.intResult = FormB.GetPendingCount(userId);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }
        [Route("CommentHistory")]
        [HttpPost]
        public ReturnBoolResult Save([FromBody] List<FormBCommentHistoryInfo> infoes) {
            ReturnBoolResult result = new ReturnBoolResult();
            try {
                if (infoes == null || infoes.Count == 0) {
                    result.msg = ReturnMsgSP.RequiredParameterIsNull;
                    return result;
                }
                result.boolResult = FormB.SaveCommentHistory(infoes);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }
        [Route("{userId:int}/PendingList")]
        [HttpGet]
        public ReturnFormBPendingList GetPendingList(int userId, int? pageIndex = null, int? pageSize = null) {
            ReturnFormBPendingList result = new ReturnFormBPendingList();
            try {
                result.list = FormB.GetList(userId, pageIndex, pageSize);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }
        [Route("{id:int}/Detail")]
        [HttpGet]
        public ReturnFormBDetail GetDetail(int id) {
            ReturnFormBDetail result = new ReturnFormBDetail();
            try {
                result.detail = FormB.GetDetail(id);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }
    }
}
