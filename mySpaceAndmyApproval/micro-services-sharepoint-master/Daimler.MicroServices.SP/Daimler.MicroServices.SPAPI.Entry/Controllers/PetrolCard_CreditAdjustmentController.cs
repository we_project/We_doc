﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.PetrolCard;
using Daimler.MicroServices.SPAPI.BLL;
using log4net;

namespace Daimler.MicroServices.SPAPI.Entry.Controllers
{
    [RoutePrefix("api/PCCA")]
    public class PetrolCard_CreditAdjustmentController : ApiController
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(PetrolCard_CreditAdjustmentController));

        [Route("")]
        [HttpPost]
        public ReturnBoolResult Save([FromBody] List<PCCreditAdjustmentInfo> infoes) {
            ReturnBoolResult result = new ReturnBoolResult();
            try {
                if (infoes == null || infoes.Count == 0) {
                    result.msg = ReturnMsgSP.RequiredParameterIsNull;
                    return result;
                }
                result.boolResult = PetrolCard_CreditAdjustment.SaveInfoes(infoes);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }
        [Route("Update")]
        [HttpPost]
        public ReturnBoolResult Update([FromBody] List<PCCreditAdjustmentInfo> infoes) {
            ReturnBoolResult result = new ReturnBoolResult();
            try {
                if (infoes == null || infoes.Count == 0) {
                    result.msg = ReturnMsgSP.RequiredParameterIsNull;
                    return result;
                }
                result.boolResult = PetrolCard_CreditAdjustment.Update(infoes);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }
        [Route("Pending")]
        [HttpGet]
        public ReturnIntResult GetPendingStationary(string supervisor) {
            ReturnIntResult result = new ReturnIntResult();
            try {
                result.intResult = PetrolCard_CreditAdjustment.GetPendingCount(supervisor);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }
        [Route("PendingList")]
        [HttpGet]
        public ReturnPetrolCardCreditAdjustmentPendingList GetPendingStationaryList(string supervisor, int? pageIndex = null, int? pageSize = null) {
            ReturnPetrolCardCreditAdjustmentPendingList result = new ReturnPetrolCardCreditAdjustmentPendingList();
            try {
                result.list = PetrolCard_CreditAdjustment.GetPCCAApplicatoinList(supervisor, pageIndex, pageSize);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }
        [Route("{id:int}/Detail")]
        [HttpGet]
        public ReturnPetrolCardCreditAdjustmentApplicationDetail GetBCApplicatoinDetail(int id) {
            ReturnPetrolCardCreditAdjustmentApplicationDetail result = new ReturnPetrolCardCreditAdjustmentApplicationDetail();
            try {
                result.detail = PetrolCard_CreditAdjustment.GetPCCAApplicatoinDetail(id);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }
    }
}
