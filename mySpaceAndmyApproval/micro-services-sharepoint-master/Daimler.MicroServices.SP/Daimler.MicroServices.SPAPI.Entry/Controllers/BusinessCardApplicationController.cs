﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.BusinessCard;
using Daimler.MicroServices.SPAPI.BLL;
using log4net;

namespace Daimler.MicroServices.SPAPI.Entry.Controllers {
    [RoutePrefix("api/BC")]
    public class BusinessCardApplicationController : ApiController {
        private static readonly ILog log = LogManager.GetLogger(typeof(BusinessCardApplicationController));

        [Route("")]
        [HttpPost]
        public ReturnBoolResult Save([FromBody] List<BCApplicationInfo> infoes) {
            ReturnBoolResult result = new ReturnBoolResult();
            try {
                if (infoes == null || infoes.Count == 0) {
                    result.msg = ReturnMsgSP.RequiredParameterIsNull;
                    return result;
                }
                result.boolResult = BusinessCardApplication.SaveRequests(infoes);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }
        [Route("Update")]
        [HttpPost]
        public ReturnBoolResult Update([FromBody] List<BCApplicationInfo> infoes) {
            ReturnBoolResult result = new ReturnBoolResult();
            try {
                if (infoes == null || infoes.Count == 0) {
                    result.msg = ReturnMsgSP.RequiredParameterIsNull;
                    return result;
                }
                result.boolResult = BusinessCardApplication.UpdateRequests(infoes);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }
        [Route("{userId:int}/PendingList")]
        [HttpGet]
        public ReturnBusinessCardPendingList GetPendingStationaryList(int userId, int? pageIndex = null, int? pageSize = null) {
            ReturnBusinessCardPendingList result = new ReturnBusinessCardPendingList();
            try {
                result.list = BusinessCardApplication.GetBCApplicatoinList(userId, pageIndex, pageSize);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }
        [Route("{id:int}/Detail")]
        [HttpGet]
        public ReturnBusinessCardApplicationDetail GetBCApplicatoinDetail(int id) {
            ReturnBusinessCardApplicationDetail result = new ReturnBusinessCardApplicationDetail();
            try {
                result.detail = BusinessCardApplication.GetBCApplicatoinDetail(id);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }
    }
}
