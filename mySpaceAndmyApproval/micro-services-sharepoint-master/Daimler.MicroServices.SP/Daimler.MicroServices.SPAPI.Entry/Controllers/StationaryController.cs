﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Web.Http;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.OfficeStationary;
using Daimler.MicroServices.SPAPI.BLL;
using log4net;

namespace Daimler.MicroServices.SPAPI.Entry.Controllers {
    [RoutePrefix("api/Stationary")]
    public class StationaryController : ApiController {
        private static readonly ILog log = LogManager.GetLogger(typeof(StationaryController));



        [Route("{id:int}/Pending")]
        [HttpGet]
        public ReturnIntResult GetPendingStationary(int id) {
            ReturnIntResult result = new ReturnIntResult();
            try {

                result.intResult = Stationary.GetPendingCount(id);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }

        [Route("{userId:int}/PendingList")]
        [HttpGet]
        public ReturnStationaryRequestList GetPendingStationaryList(int userId, int? pageIndex = null, int? pageSize = null) {
            ReturnStationaryRequestList result = new ReturnStationaryRequestList();
            try {
                result.list = Stationary.GetPendingStationaryList(userId, pageIndex, pageSize);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }



    }
}
