﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.OfficeStationary;
using Daimler.MicroServices.SPAPI.BLL;
using log4net;

namespace Daimler.MicroServices.SPAPI.Entry.Controllers {
    [RoutePrefix("api/Stationary")]
    public class OfficeStationaryRequestsController : ApiController {
        private static readonly ILog log = LogManager.GetLogger(typeof(StationaryController));



        [Route("Requests")]
        [HttpPost]
        public ReturnBoolResult Save([FromBody] List<StationaryRequestInfo> infoes) {
            ReturnBoolResult result = new ReturnBoolResult();
            try {
                if (infoes == null || infoes.Count == 0) {
                    result.msg = ReturnMsgSP.RequiredParameterIsNull;
                    return result;
                }
                result.boolResult = OfficeStationaryRequests.SaveRequests(infoes);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }
        [Route("Requests/Update")]
        [HttpPost]
        public ReturnBoolResult Update([FromBody] List<StationaryRequestInfo> infoes) {
            ReturnBoolResult result = new ReturnBoolResult();
            try {
                if (infoes == null || infoes.Count == 0) {
                    result.msg = ReturnMsgSP.RequiredParameterIsNull;
                    return result;
                }
                result.boolResult = OfficeStationaryRequests.UpdateRequests(infoes);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }
        [Route("{ID:int}/Detail")]
        [HttpGet]
        public APIReturnStationaryRequestDetail GetRequestDetail(int ID) {
            APIReturnStationaryRequestDetail result = new APIReturnStationaryRequestDetail();
            try {
                result.detail = Stationary.GetStationaryDetail(ID);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        } 
    }
}
