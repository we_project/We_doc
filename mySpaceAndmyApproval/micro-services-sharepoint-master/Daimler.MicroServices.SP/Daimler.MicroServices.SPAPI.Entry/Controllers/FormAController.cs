﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.FormAB;
using Daimler.MicroServices.SPAPI.BLL;
using log4net;

namespace Daimler.MicroServices.SPAPI.Entry.Controllers
{
    [RoutePrefix("api/FormA")]
    public class FormAController : ApiController
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(FormAController));

        [Route("")]
        [HttpPost]
        public ReturnBoolResult Save([FromBody] List<FormAInfo> infoes) {
            ReturnBoolResult result = new ReturnBoolResult();
            try {
                if (infoes == null || infoes.Count == 0) {
                    result.msg = ReturnMsgSP.RequiredParameterIsNull;
                    return result;
                }
                result.boolResult = FormA.SaveRequests(infoes);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }
        [Route("Update")]
        [HttpPost]
        public ReturnBoolResult Update([FromBody] List<FormAInfo> infoes) {
            ReturnBoolResult result = new ReturnBoolResult();
            try {
                if (infoes == null || infoes.Count == 0) {
                    result.msg = ReturnMsgSP.RequiredParameterIsNull;
                    return result;
                }
                result.boolResult = FormA.UpdateRequests(infoes);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }


        [Route("{userId:int}/Pending")]
        [HttpGet]
        public ReturnIntResult GetPendingStationary(int userId) {
            ReturnIntResult result = new ReturnIntResult();
            try {
                result.intResult = FormA.GetPendingCount(userId);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }
        [Route("CommentHistory")]
        [HttpPost]
        public ReturnBoolResult Save([FromBody] List<FormACommentHistoryInfo> infoes) {
            ReturnBoolResult result = new ReturnBoolResult();
            try {
                if (infoes == null || infoes.Count == 0) {
                    result.msg = ReturnMsgSP.RequiredParameterIsNull;
                    return result;
                }
                result.boolResult = FormA.SaveCommentHistory(infoes);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }
        [Route("{userId:int}/PendingList")]
        [HttpGet]
        public ReturnFormAPendingList GetPendingList(int userId, int? pageIndex = null, int? pageSize =null) {
            ReturnFormAPendingList result = new ReturnFormAPendingList();
            try {
                result.list = FormA.GetList(userId, pageIndex, pageSize);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }
        [Route("{id:int}/Detail")]
        [HttpGet]
        public ReturnFormADetail GetDetail(int id) {
            ReturnFormADetail result = new ReturnFormADetail();
            try {
                result.detail = FormA.GetDetail(id);
                result.success = true;
            } catch (Exception ex) {
                log.Error(this, ex);
                result.msg = ReturnMsgSP.ExceptionOccurs;
            }
            return result;
        }
    }
}
