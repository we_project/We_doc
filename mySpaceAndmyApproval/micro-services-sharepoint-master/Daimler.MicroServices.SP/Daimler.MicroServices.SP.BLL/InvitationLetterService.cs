﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.Approver;
using Daimler.MicroServices.SP.BizObject.InvitationLetter;
using Daimler.MicroServices.SP.BizObject.Workflow;
using Daimler.MicroServices.SP.Service.Common;
using Daimler.MicroServices.SP.Service.Workflow;
using Daimler.MicroServices.Utils.Network;
using log4net;

namespace Daimler.MicroServices.SP.BLL {
    public class InvitationLetterService {
        private static readonly ILog log = LogManager.GetLogger(typeof(InvitationLetterService));
        private UserService userService;
        //private readonly IOfficeStationary officeStationaryService;
        private readonly ICommon commonService;
        private readonly IWorkflowTasks workflowTasksService;
        UserCredential uc;
        public InvitationLetterService(UserCredential uc) {
            if (uc == null) {
                throw new ArgumentNullException();
            }
            this.uc = uc;
            this.userService = new UserService(uc);
            //this.officeStationaryService = new OfficeStationaryImpl(uc);
            this.commonService = new CommonImpl(uc);
            this.workflowTasksService = new WorkflowTasksImpl(uc);
        }
        public static async Task<bool> SaveILApplicatoinAsync(List<eLOIApplicationInfo> infoes) {
            string url = string.Format("{0}/api/IL", Config.ApiUrl);
            ReturnBoolResult result = await HttpHelper_Json.PostJsonAsync<ReturnBoolResult, List<eLOIApplicationInfo>>(url, infoes);
            if (result.success && result.boolResult) {
                return true;
            }
            log.DebugFormat("post eLOIApplicationInfo to dataApi result: handled {0}, handled result: {1}", result.success, result.msg);
            return false;
        }
        public static async Task<bool> UpdateILApplicatoinAsync(List<eLOIApplicationInfo> infoes) {
            string url = string.Format("{0}/api/IL/Update", Config.ApiUrl);
            ReturnBoolResult result = await HttpHelper_Json.PostJsonAsync<ReturnBoolResult, List<eLOIApplicationInfo>>(url, infoes);
            if (result.success && result.boolResult) {
                return true;
            }
            log.DebugFormat("post eLOIApplicationInfo to dataApi result: handled {0}, handled result: {1}", result.success, result.msg);
            return false;
        }
        public async Task<List<InvitationLetterApplicationItem>> GetILApplicatoinList(int? pageIndex, int? pageSize ) {
            List<InvitationLetterApplicationItem> result = new List<InvitationLetterApplicationItem>();
            int? commonUserid = await userService.GetCommonUserIdAsync();
            if (commonUserid.HasValue) {
                string url = string.Format("{0}/api/IL/{1}/PendingList?pageIndex={2}&pageSize={3}", Config.ApiUrl, commonUserid.Value, pageIndex, pageSize);
                ReturnInvitationLetterPendingList returnList = await HttpHelper_Json.GetJsonAsync<ReturnInvitationLetterPendingList>(url);
                if (returnList.success) {
                    return returnList.list;
                }
            }
            return null;
        }
        public async Task<InvitationLetterApplicationDetail> GetILApplicatoinDetail(int id) {
            string url = string.Format("{0}/api/IL/{1}/Detail", Config.ApiUrl, id);
            ReturnInvitationLetterApplicationDetail returnList = await HttpHelper_Json.GetJsonAsync<ReturnInvitationLetterApplicationDetail>(url);
            if (returnList.success) {
                return returnList.detail;
            }
            return null;
        }

        public async Task<ApproveResult> Approval(bool approve, int ID, string comment) {
            ApproveResult result = new ApproveResult();
            // if the newest task is not equal to current task, return status changed, update current task
            // if equal, then approve
            int? commonUserid = await userService.GetCommonUserIdAsync();
            if (commonUserid.HasValue) {
                Task<int?> t1 = workflowTasksService.GetPendingWorkflowTasksByItemIdAsync(commonUserid.Value, ID, WorkflowNames.Invitation);
                Task<int?> t2 = WorkflowTasksService.GetPendingWorkflowTasksByItemId(commonUserid.Value, ID, WorkflowNames.Invitation);
                int? wfId = await t1;
                int? currentWfId = await t2;
                log.DebugFormat("approve IL: {0}, newest workflowTask id: {1},current wf id: {2}", ID, wfId.HasValue ? wfId.Value.ToString() : "null", currentWfId.HasValue ? currentWfId.Value.ToString() : "null");
                if (wfId.HasValue && currentWfId.HasValue && wfId.Value.Equals(currentWfId.Value)) {
                    try {
                        result = workflowTasksService.Approve(wfId.Value, approve, comment);
                        // if success, mark this item as complete.
                        if (result != null && result.Success) {
                            bool markApproved = await WorkflowTasksService.MarkApprovedWorkflowTasksAsync(currentWfId.Value);
                            log.Info(new {
                                action = "mark approved for workflow task",
                                wfId = currentWfId.Value,
                                result = markApproved
                            });
                        }
                    } catch (Exception ex) {
                        result.Success = false;
                        result.Msg = AllErrors.Others;
                        log.ErrorFormat("user {0} approve IL:{1} failed wf id: {3}, {2}", commonUserid.Value, ID, ex, wfId.Value);
                    }
                }
                if (currentWfId.HasValue) {
                    // todo: test this method, why it failed the first time
                    // get newest request data
                    WorkflowTaskInfo info = commonService.GetListItemById<WorkflowTaskInfo>(currentWfId.Value);
                    bool save = await WorkflowTasksService.UpdateWorkflowTasksAsync(new List<WorkflowTaskInfo>() { info });
                    log.DebugFormat("save WorkflowTaskInfo for ID:{0},{1}", ID, save);
                }
            }
            return result;
        }

    }
}
