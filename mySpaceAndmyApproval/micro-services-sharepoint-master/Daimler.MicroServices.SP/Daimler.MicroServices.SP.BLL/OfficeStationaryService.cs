﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.Approver;
using Daimler.MicroServices.SP.BizObject.OfficeStationary;
using Daimler.MicroServices.SP.Service.Common;
using Daimler.MicroServices.SP.Service.OfficeStationary;
using Daimler.MicroServices.Utils.Network;
using log4net;

namespace Daimler.MicroServices.SP.BLL {
    public class OfficeStationaryService {
        private static readonly ILog log = LogManager.GetLogger(typeof(OfficeStationaryService));
        private UserService userService;
        private readonly IOfficeStationary officeStationaryService;
        private readonly ICommon commonService;
        UserCredential uc;
        public OfficeStationaryService(UserCredential uc) {
            if (uc == null) {
                throw new ArgumentNullException();
            }
            this.uc = uc;
            this.userService = new UserService(uc);
            this.officeStationaryService = new OfficeStationaryImpl(uc);
            this.commonService = new CommonImpl(uc);
        }
        public static async Task<bool> SaveRequestsAsync(List<StationaryRequestInfo> infoes) {
            string url = string.Format("{0}/api/Stationary/Requests", Config.ApiUrl);
            ReturnBoolResult result = await HttpHelper_Json.PostJsonAsync<ReturnBoolResult, List<StationaryRequestInfo>>(url, infoes);
            if (result.success && result.boolResult) {
                return true;
            }
            log.DebugFormat("post stationary requests to dataApi result: handled {0}, handled result: {1}", result.success, result.msg);
            return false;
        }
        public static async Task<bool> UpdateRequestsAsync(List<StationaryRequestInfo> infoes) {
            string url = string.Format("{0}/api/Stationary/Requests/Update", Config.ApiUrl);
            ReturnBoolResult result = await HttpHelper_Json.PostJsonAsync<ReturnBoolResult, List<StationaryRequestInfo>>(url, infoes);
            if (result.success && result.boolResult) {
                return true;
            }
            log.DebugFormat("post stationary requests to dataApi result: handled {0}, handled result: {1}", result.success, result.msg);
            return false;
        }
        public static async Task<bool> SaveOSLocationAsync(List<OSLocationInfo> infoes) {
            string url = string.Format("{0}/api/Stationary/OSLocation", Config.ApiUrl);
            ReturnBoolResult result = await HttpHelper_Json.PostJsonAsync<ReturnBoolResult, List<OSLocationInfo>>(url, infoes);
            if (result.success && result.boolResult) {
                return true;
            }
            log.DebugFormat("post OSLocationInfo to dataApi result: handled {0}, handled result: {1}", result.success, result.msg);
            return false;
        }


        public static async Task<bool> SaveMenusAsync(List<StationaryMenuInfo> infoes) {
            string url = string.Format("{0}/api/Stationary/OSMenu", Config.ApiUrl);
            ReturnBoolResult result = await HttpHelper_Json.PostJsonAsync<ReturnBoolResult, List<StationaryMenuInfo>>(url, infoes);
            if (result.success && result.boolResult) {
                return true;
            }
            log.DebugFormat("post StationaryMenuInfo to dataApi result: handled {0}, handled result: {1}", result.success, result.msg);
            return false;
        }

        public static async Task<int> GetPendingCountAsync(int userId) {
            string url = string.Format("{0}/api/Stationary/{1}/Pending", Config.ApiUrl, userId);
            ReturnIntResult result = await HttpHelper_Json.GetJsonAsync<ReturnIntResult>(url);
            if (result.success) {
                return result.intResult.HasValue ? result.intResult.Value : 0;
            }
            log.DebugFormat("GetPendingCountAsync result: handled {0}, handled result: {1}", result.success, result.msg);
            return 0;
        }

        public async Task<List<StationaryRequestItem>> GetStationaryList(int? pageIndex, int? pageSize) {
            List<StationaryRequestItem> result = new List<StationaryRequestItem>();
            int? commonUserid = await userService.GetCommonUserIdAsync();
            if (commonUserid.HasValue) {
                string url = string.Format("{0}/api/Stationary/{1}/PendingList?pageIndex={2}&pageSize={3}", Config.ApiUrl, commonUserid.Value, pageIndex, pageSize);
                ReturnStationaryRequestList returnList = await HttpHelper_Json.GetJsonAsync<ReturnStationaryRequestList>(url);
                if (returnList.success) {
                    return returnList.list;
                }
            }
            return null;
        }

        public async Task<StationaryRequestDetail> GetStationaryRequestDetail(int ID) {

            StationaryRequestDetail result = new StationaryRequestDetail();
            string url = string.Format("{0}/api/Stationary/{1}/Detail", Config.ApiUrl, ID);
            APIReturnStationaryRequestDetail returnList = await HttpHelper_Json.GetJsonAsync<APIReturnStationaryRequestDetail>(url);
            if (returnList.success) {
                return await GetFrom(returnList.detail);
            }
            return null;
        }

        async Task<StationaryRequestDetail> GetFrom(APIStationaryRequestDetail info) {
            StationaryRequestDetail result = new StationaryRequestDetail {
                Company = info.Company,
                CostCenter = info.CostCenter,
                CreatedTime = info.CreatedTime,
                DetailAddress = info.DetailAddress,
                ExtensionNumber = info.ExtensionNumber,
                FullAddress = string.Join(" ", info.Location, info.DetailAddress),
                ID = info.ID,
                LocationCode = info.LocationCode,
                Location = info.Location,
                Status = info.Status,
                TotalAmount = info.TotalAmount,
                UserName = info.UserName
            };
            result.Orders = await GetFrom(info.orders);

            return result;
        }

        public async Task<List<Order>> GetFrom(List<RequestOrder> orders) {
            List<Order> data = null;
            if (orders != null && orders.Count > 0) {
                List<StationaryDetail> details = await GetDetails(orders.Select(o => o.id).ToList());
                if (details != null && details.Count > 0) {
                    data = details.Join(
                                    orders,
                                    detail => detail.ID,
                                    order => order.id,
                                    (detail, order) => GetFrom(order, detail)
                                    ).ToList();
                }
            }
            return data;
        }
        public async Task<List<StationaryDetail>> GetDetails(List<int> ids) {
            string url = string.Format("{0}/api/Stationary/OSMenu/List?{1}", Config.ApiUrl, string.Join("&", ids.Select(id => "ids=" + id)));
            log.Debug(url);
            ReturnStationaryDetailList returnList = await HttpHelper_Json.GetJsonAsync<ReturnStationaryDetailList>(url);
            if (returnList.success) {
                return returnList.details;
            };
            return null;
        }
        public Order GetFrom(RequestOrder order, StationaryDetail detail) {
            Order tmpOrder = new Order();
            tmpOrder.ID = order.id;
            tmpOrder.Name = detail.ItemName;
            tmpOrder.ItemName_EN = detail.ItemName_EN;
            tmpOrder.Price = detail.Price_x0028_with_x0020_Tax_x0029_;
            tmpOrder.Quantity = order.quantity;
            tmpOrder.Remark = order.reason;

            // if file exist return url
            string fName = order.id + ".jpg";
            string filename = Path.Combine(HostingEnvironment.MapPath("~/" + Config.StationaryImagesDirName), fName);
            log.DebugFormat("order path: {0}", filename);
            if (File.Exists(filename)) {
                tmpOrder.ImageUrl = Config.StationaryImagesUrl + "/" + fName;
            }
            return tmpOrder;
        }

        public async Task<ApproveResult> Approval(bool approve, int ID, string comment) {
            ApproveResult result = new ApproveResult();
            StationaryRequestInfo info = null;
            int? commonUserid = await userService.GetCommonUserIdAsync();
            if (commonUserid.HasValue) {
                result = officeStationaryService.Approval(commonUserid.Value, approve, ID, comment, out info);
            }
            if (!result.Success) {
                result.Msg = AllErrors.InvalidUser;
            }
            // get newest request data
            if (info != null) {
                bool save = await UpdateRequestsAsync(new List<StationaryRequestInfo>() { info });
                log.DebugFormat("save request for ID:{0},{1}", ID, save);
            }

            return result;
        }

    }
}
