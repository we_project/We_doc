﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.Approver;
using Daimler.MicroServices.SP.BizObject.PetrolCard;
using Daimler.MicroServices.SP.Service.Common;
using Daimler.MicroServices.SP.Service.PetrolCard;
using Daimler.MicroServices.Utils.Network;
using log4net;

namespace Daimler.MicroServices.SP.BLL {
    public class PetrolCardService {
        private static readonly ILog log = LogManager.GetLogger(typeof(PetrolCardService));
        private UserService userService;
        private readonly IPetrolCardCreditAdjustment pccaService;
        private readonly ICommon commonService;
        UserCredential uc;
        public PetrolCardService(UserCredential uc) {
            if (uc == null) {
                throw new ArgumentNullException();
            }
            this.uc = uc;
            this.userService = new UserService(uc);
            this.commonService = new CommonImpl(uc);
            this.pccaService = new PetrolCardCreditAdjustmentImpl(uc);
        }

        public static async Task<bool> SavePCCAAsync(List<PCCreditAdjustmentInfo> infoes) {
            string url = string.Format("{0}/api/PCCA", Config.ApiUrl);
            ReturnBoolResult result = await HttpHelper_Json.PostJsonAsync<ReturnBoolResult, List<PCCreditAdjustmentInfo>>(url, infoes);
            if (result.success && result.boolResult) {
                return true;
            }
            log.DebugFormat("post PCCreditAdjustmentInfo to dataApi result: handled {0}, handled result: {1}", result.success, result.msg);
            return false;
        }
        public static async Task<bool> UpdatePCCAAsync(List<PCCreditAdjustmentInfo> infoes) {
            string url = string.Format("{0}/api/PCCA/Update", Config.ApiUrl);
            ReturnBoolResult result = await HttpHelper_Json.PostJsonAsync<ReturnBoolResult, List<PCCreditAdjustmentInfo>>(url, infoes);
            if (result.success && result.boolResult) {
                return true;
            }
            log.DebugFormat("post PCCreditAdjustmentInfo to dataApi result: handled {0}, handled result: {1}", result.success, result.msg);
            return false;
        }
        public static async Task<int> GetPendingCountAsync(string supervisor) {
            string url = string.Format("{0}/api/PCCA/Pending?supervisor={1}", Config.ApiUrl,supervisor);
            ReturnIntResult result = await HttpHelper_Json.GetJsonAsync<ReturnIntResult>(url);
            if (result.success) {
                return result.intResult.HasValue ? result.intResult.Value : 0;
            }
            log.DebugFormat("GetPendingCountAsync result: handled {0}, handled result: {1}", result.success, result.msg);
            return 0;
        }

        public async Task<List<PCCreditAdjustmentItem>> GetPCCAList(int? pageIndex, int? pageSize ) {
            List<PCCreditAdjustmentItem> result = new List<PCCreditAdjustmentItem>();
            string url = string.Format("{0}/api/PCCA/PendingList?supervisor={1}&pageIndex={2}&pageSize={3}", Config.ApiUrl, uc, pageIndex, pageSize);
            ReturnPetrolCardCreditAdjustmentPendingList returnList = await HttpHelper_Json.GetJsonAsync<ReturnPetrolCardCreditAdjustmentPendingList>(url);
            if (returnList.success) {
                return returnList.list;
            }
            return null;
        }
        public async Task<PCCreditAdjustmentDetail> GetPCCADetail(int id) {
            string url = string.Format("{0}/api/PCCA/{1}/Detail", Config.ApiUrl, id);
            ReturnPetrolCardCreditAdjustmentApplicationDetail returnList = await HttpHelper_Json.GetJsonAsync<ReturnPetrolCardCreditAdjustmentApplicationDetail>(url);
            if (returnList.success) {
                return returnList.detail;
            }
            return null;
        }

        public async Task<ApproveResult> Approval(bool approve, int ID, string comment) {
            ApproveResult result = new ApproveResult();
            try {
                result = pccaService.Approval(uc.ToString(), approve, ID, comment);
                if (!result.Success) {
                    result.Msg = AllErrors.InvalidUser;
                }
            } catch (Exception ex) {
                result.Success = false;
                result.Msg = AllErrors.Others;
                log.ErrorFormat("user {0} approve PetrolCardCreditAdjustment:{1} failed {2}", uc, ID, ex);
            }
            // get newest request data
            PCCreditAdjustmentInfo info = commonService.GetListItemById<PCCreditAdjustmentInfo>(ID);
            bool save = await UpdatePCCAAsync(new List<PCCreditAdjustmentInfo>() { info });
            log.DebugFormat("save request for ID:{0},{1}", ID, save);
            return result;
        }
    }
}
