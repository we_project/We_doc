﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.AccessManagement;
using Daimler.MicroServices.SP.Service.AccessManagement;
using Daimler.MicroServices.Utils.Network;
using log4net;
using SP.Data;

namespace Daimler.MicroServices.SP.BLL {
    public class EIBMService {
        private static readonly ILog log = LogManager.GetLogger(typeof(EIBMService));
        private UserService userService;
        //private EIBMNew spEIBM;
        private EIBMRestful spEIBMRestful;
        UserCredential uc;
        public EIBMService(UserCredential uc) {
            if (uc == null) {
                throw new ArgumentNullException();
            }
            this.uc = uc;
            this.userService = new UserService(uc);
            //this.spEIBM = new EIBMNew(uc);
            this.spEIBMRestful = new EIBMRestful(uc);
        }


        public async Task<int> GetPendingCountAsync(int userId) {
            spEIBMRestful.CurrentUserId = userId;
            spEIBMRestful.ccdeputys = await BadgeService.GetCCDeputys(userId);
            return await Task.Factory.StartNew<int>(() => {
                return spEIBMRestful.GetPendingCount();
            });
        }

        /// <summary>
        /// get from sharepoint
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<EIBMItem>> GetPendingList() {
            int? badgeUserid = await userService.GetBadgeUserIdAsync();
            if (badgeUserid.HasValue) {
                spEIBMRestful.CurrentUserId = badgeUserid.Value;
                spEIBMRestful.ccdeputys = await BadgeService.GetCCDeputys(badgeUserid.Value);
                return spEIBMRestful.GetPendingList();
            }
            return null;
        }


        //public async Task<EIBMApproveResult[]> BatchApproval(EIBMBadgeApprovalPost post) {
        //    if (post != null && post.infoes != null && post.infoes.Count() > 0) {
        //        int? badgeUserid = await userService.GetBadgeUserIdAsync();
        //        if (badgeUserid.HasValue) {
        //            spEIBM.CurrentUserId = badgeUserid.Value;
        //            spEIBM.ccdeputys = await BadgeService.GetCCDeputys(badgeUserid.Value);
        //            // get item new data
        //            Intern_x0020_badge_x0020_ApplicationListItem detail = await spEIBM.GetItem(post.ID);
        //            List<Task<EIBMApproveResult>> tasks = new List<Task<EIBMApproveResult>>();
        //            foreach (var item in post.infoes) {
        //                tasks.Add(spEIBM.ApprovalAsync(detail, item));
        //            }
        //            EIBMApproveResult[] result = await Task.WhenAll(tasks.ToArray());
        //            return result;
        //        }
        //    }
        //    return null;
        //}
        public async Task<EIBMApproveResult[]> BatchApprovalUseRestful(EIBMBadgeApprovalPost post) {
            if (post != null && post.infoes != null && post.infoes.Count() > 0) {
                int? badgeUserid = await userService.GetBadgeUserIdAsync();
                if (badgeUserid.HasValue) {
                    spEIBMRestful.CurrentUserId = badgeUserid.Value;
                    spEIBMRestful.ccdeputys = await BadgeService.GetCCDeputys(badgeUserid.Value);
                    // get item new data
                    EIBMInfoForApproval detail = await spEIBMRestful.GetItemForApproval(post.ID);
                    List<Task<EIBMApproveResult>> tasks = new List<Task<EIBMApproveResult>>();
                    foreach (var item in post.infoes) {
                        tasks.Add(spEIBMRestful.ApprovalAsync(detail, item));
                    }
                    EIBMApproveResult[] result = await Task.WhenAll(tasks.ToArray());
                    log.InfoFormat("BatchApprovalUseRestful complete for EIBM {0}", post.ID);

                    // when approved, then load the newest item, and upload it to db
                    EIBMInfo info = await spEIBMRestful.GetItemByIdAsync<EIBMInfo>(post.ID);
                    bool save = await UpdateEIBMsAsync(new List<EIBMInfo>() { info });
                    log.InfoFormat("save EIBMInfo for ID:{0},{1}", post.ID, save);

                    return result;
                }
            }
            return null;
        }


        public static async Task<bool> SaveEIBMsAsync(List<EIBMInfo> infoes) {
            string url = string.Format("{0}/api/EIBM", Config.ApiUrl);
            ReturnBoolResult result = await HttpHelper_Json.PostJsonAsync<ReturnBoolResult, List<EIBMInfo>>(url, infoes);
            if (result.success && result.boolResult) {
                return true;
            }
            log.DebugFormat("post EIBMInfo to dataApi result: handled {0}, handled result: {1}", result.success, result.msg);
            return false;
        }
        public static async Task<bool> UpdateEIBMsAsync(List<EIBMInfo> infoes) {
            string url = string.Format("{0}/api/EIBM/Update", Config.ApiUrl);
            ReturnBoolResult result = await HttpHelper_Json.PostJsonAsync<ReturnBoolResult, List<EIBMInfo>>(url, infoes);
            if (result.success && result.boolResult) {
                return true;
            }
            log.DebugFormat("post EIBM to dataApi result: handled {0}, handled result: {1}", result.success, result.msg);
            return false;
        }
        /// <summary>
        /// get from db
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static async Task<int> GetPendingEIBMCountAsync(int userId) {
            string url = string.Format("{0}/api/EIBM/{1}/PendingCount", Config.ApiUrl, userId);
            ReturnIntResult result = await HttpHelper_Json.GetJsonAsync<ReturnIntResult>(url);
            if (result.success) {
                return result.intResult.HasValue ? result.intResult.Value : 0;
            }
            log.DebugFormat("GetPendingEIBMCountAsync result: handled {0}, handled result: {1}", result.success, result.msg);
            return 0;
        }

        /// <summary>
        /// get from db
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<EIBMItem>> GetPendingEIBMList() {
            List<EIBMItem> result = new List<EIBMItem>();
            int? badgeUserid = await userService.GetBadgeUserIdAsync();
            if (badgeUserid.HasValue) {
                string url = string.Format("{0}/api/EIBM/{1}/PendingList", Config.ApiUrl, badgeUserid.Value);
                ReturnEIBMPendingList returnList = await HttpHelper_Json.GetJsonAsync<ReturnEIBMPendingList>(url);
                if (returnList.success) {
                    return returnList.list;
                }
            }
            return null;
        }


        /// <summary>
        /// get from db
        /// </summary>
        /// <returns></returns>
        public async Task<EIBMDetail1> GeEIBMDetail(int id) {
            string url = string.Format("{0}/api/EIBM/{1}/Detail", Config.ApiUrl, id);
            ReturnEIBMDetail returnList = await HttpHelper_Json.GetJsonAsync<ReturnEIBMDetail>(url);
            if (returnList.success) {
                return returnList.detail;
            }
            return null;
        }
    }
}
