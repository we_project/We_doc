﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BLL {
    public class Config {
        private static readonly string apiUrl = ConfigurationManager.AppSettings["dataApiBase"];
        public static string ApiUrl {
            get {
                return apiUrl;
            }
        }

        public static string StationaryImagesDirName = "StationaryImages";
        public static string StationaryImagesLocalPath {
            get {

                string dir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, StationaryImagesDirName);
                //if (!Directory.Exists(dir)) {
                //    Directory.CreateDirectory(dir);
                //}
                return dir;
            }
        }
        public static string ThisSiteUrlWithDomain {
            get {
                return ConfigurationManager.AppSettings["ThisSiteUrlWithDomain"];
            }
        }

        public static string StationaryImagesUrl {
            get {
                return ThisSiteUrlWithDomain.TrimEnd('/') + "/" + StationaryImagesDirName;
            }
        }

        public static bool UpdateToSP {
            get {
                string _UpdateToSP = ConfigurationManager.AppSettings["UpdateToSP"];
                if (!string.IsNullOrWhiteSpace(_UpdateToSP)) {
                    if ("true".Equals(_UpdateToSP, StringComparison.CurrentCultureIgnoreCase)) {
                        return true;
                    }
                }
                return false;
            }
        }
    }
}
