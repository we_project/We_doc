﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.Approver;
using Daimler.MicroServices.SP.BizObject.FormAB;
using Daimler.MicroServices.SP.Service.Common;
using Daimler.MicroServices.SP.Service.FormAB;
using Daimler.MicroServices.Utils.Network;
using log4net;

namespace Daimler.MicroServices.SP.BLL {
   public class FormBService {
         private static readonly ILog log = LogManager.GetLogger(typeof(FormBService));
         private readonly IFormAB formABservice;
         private UserService userService;
        private readonly ICommon commonService;
        UserCredential uc;
        public FormBService(UserCredential uc) {
            if (uc == null) {
                throw new ArgumentNullException();
            }
            this.uc = uc;
            this.userService = new UserService(uc);
            this.commonService = new CommonImpl(uc);
            this.formABservice = new FormABImpl(uc);
        }

        public static async Task<bool> SaveFormBInfoAsync(List<FormBInfo> infoes) {
            string url = string.Format("{0}/api/FormB", Config.ApiUrl);
            ReturnBoolResult result = await HttpHelper_Json.PostJsonAsync<ReturnBoolResult, List<FormBInfo>>(url, infoes);
            if (result.success && result.boolResult) {
                return true;
            }
            log.DebugFormat("post FormBInfo to dataApi result: handled {0}, handled result: {1}", result.success, result.msg);
            return false;
        }
        public static async Task<bool> UpdateFormBAsync(List<FormBInfo> infoes) {
            string url = string.Format("{0}/api/FormB/Update", Config.ApiUrl);
            ReturnBoolResult result = await HttpHelper_Json.PostJsonAsync<ReturnBoolResult, List<FormBInfo>>(url, infoes);
            if (result.success && result.boolResult) {
                return true;
            }
            log.DebugFormat("post FormBInfo to dataApi result: handled {0}, handled result: {1}", result.success, result.msg);
            return false;
        }

        public static async Task<bool> SaveFormBCommentHistoryInfoAsync(List<FormBCommentHistoryInfo> infoes) {
            string url = string.Format("{0}/api/FormB/CommentHistory", Config.ApiUrl);
            ReturnBoolResult result = await HttpHelper_Json.PostJsonAsync<ReturnBoolResult, List<FormBCommentHistoryInfo>>(url, infoes);
            if (result.success && result.boolResult) {
                return true;
            }
            log.DebugFormat("post FormBCommentHistoryInfo to dataApi result: handled {0}, handled result: {1}", result.success, result.msg);
            return false;
        }
        public static async Task<int> GetPendingCountAsync(int userId) {
            string url = string.Format("{0}/api/FormB/{1}/Pending", Config.ApiUrl, userId);
            ReturnIntResult result = await HttpHelper_Json.GetJsonAsync<ReturnIntResult>(url);
            if (result.success) {
                return result.intResult.HasValue ? result.intResult.Value : 0;
            }
            log.DebugFormat("GetPendingCountAsync result: handled {0}, handled result: {1}", result.success, result.msg);
            return 0;
        }
        public async Task<List<FormBItem>> GetFormBList(int? pageIndex, int? pageSize) {
            List<FormBItem> result = new List<FormBItem>();
            int? commonUserid = await userService.GetFormABUserIdAsync();
            if (commonUserid.HasValue) {
                string url = string.Format("{0}/api/FormB/{1}/PendingList?pageIndex={2}&pageSize={3}", Config.ApiUrl, commonUserid.Value, pageIndex, pageSize);
                ReturnFormBPendingList returnList = await HttpHelper_Json.GetJsonAsync<ReturnFormBPendingList>(url);
                if (returnList.success) {
                    return returnList.list;
                }
            }
            return null;
        }
        public async Task<FormBDetail> GetDetail(int id) {
            string url = string.Format("{0}/api/FormB/{1}/Detail", Config.ApiUrl, id);
            ReturnFormBDetail returnList = await HttpHelper_Json.GetJsonAsync<ReturnFormBDetail>(url);
            if (returnList.success) {
                return returnList.detail;
            }
            return null;
        }

        public async Task<ApproveResult> Approval(bool approve, int ID, string comment, string StatusIndex, FormBTaxTeamFields taxTeamField) {
            ApproveResult result = new ApproveResult();

            string userName = await userService.GetFormABUserNameAsync();
            result = formABservice.ApprovalFormB(userName, approve, ID, comment, StatusIndex, taxTeamField);
            if (!result.Success) {
                result.Msg = AllErrors.InvalidUser;
            }
            // get newest form data
            FormBInfo info = commonService.GetListItemById<FormBInfo>(ID);
            bool save = await UpdateFormBAsync(new List<FormBInfo>() { info });
            log.DebugFormat("save FormBInfo for ID:{0},{1}", ID, save);
            return result;
        }
    }
}
