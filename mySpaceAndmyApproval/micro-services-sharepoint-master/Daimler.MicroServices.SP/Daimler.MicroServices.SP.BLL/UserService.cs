﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.Utils.Network;
using log4net;

namespace Daimler.MicroServices.SP.BLL {
    public class UserService {
        UserCredential uc;
        public UserService(UserCredential uc) {
            if (uc == null) {
                throw new ArgumentNullException();
            }
            this.uc = uc;
        }
        private static readonly ILog log = LogManager.GetLogger(typeof(OfficeStationaryService));
        public static async Task<bool> SaveCommonUserAsync(List<SPUserInfo> infoes) {
            string url = string.Format("{0}/api/CommonUser", Config.ApiUrl);
            ReturnBoolResult result = await HttpHelper_Json.PostJsonAsync<ReturnBoolResult, List<SPUserInfo>>(url, infoes);
            if (result.success && result.boolResult) {
                return true;
            }
            log.DebugFormat("post CommonUser to dataApi result: handled {0}, handled result: {1}", result.success, result.msg);
            return false;
        }
        public static async Task<bool> SaveFormABUserAsync(List<SPUserInfo> infoes) {
            string url = string.Format("{0}/api/FormABUser", Config.ApiUrl);
            ReturnBoolResult result = await HttpHelper_Json.PostJsonAsync<ReturnBoolResult, List<SPUserInfo>>(url, infoes);
            if (result.success && result.boolResult) {
                return true;
            }
            log.DebugFormat("post FormABUser to dataApi result: handled {0}, handled result: {1}", result.success, result.msg);
            return false;
        }
        public static async Task<bool> SaveBadgeUserAsync(List<SPUserInfo> infoes) {
            string url = string.Format("{0}/api/BadgeUser", Config.ApiUrl);
            ReturnBoolResult result = await HttpHelper_Json.PostJsonAsync<ReturnBoolResult, List<SPUserInfo>>(url, infoes);
            if (result.success && result.boolResult) {
                return true;
            }
            log.DebugFormat("post BadgeUser to dataApi result: handled {0}, handled result: {1}", result.success, result.msg);
            return false;
        }
        public async Task<int?> GetCommonUserIdAsync() {
            string url = string.Format("{0}/api/CommonUser/id?AccountName={1}", Config.ApiUrl, uc);
            ReturnIntResult result = await HttpHelper_Json.GetJsonAsync<ReturnIntResult>(url);
            if (result != null && result.success) {
                return result.intResult;
            }
            log.DebugFormat("GetCommonUserIdAsync: handled {0}, handled result: {1}", result.success, result.msg);
            return null;
        }

        public async Task<int?> GetFormABUserIdAsync() {
            string url = string.Format("{0}/api/FormABUser/id?AccountName={1}", Config.ApiUrl, uc);
            ReturnIntResult result = await HttpHelper_Json.GetJsonAsync<ReturnIntResult>(url);
            if (result.success) {
                return result.intResult;
            }
            log.DebugFormat("GetFormABUserIdAsync: handled {0}, handled result: {1}", result.success, result.msg);
            return null;
        }
        public async Task<string> GetFormABUserNameAsync() {
            string url = string.Format("{0}/api/FormABUser/name?AccountName={1}", Config.ApiUrl, uc);
            ReturnStringResult result = await HttpHelper_Json.GetJsonAsync<ReturnStringResult>(url);
            if (result.success) {
                return result.stringResult;
            }
            log.DebugFormat("GetFormABUserIdAsync: handled {0}, handled result: {1}", result.success, result.msg);
            return null;
        }
        public async Task<int?> GetBadgeUserIdAsync() {
            string url = string.Format("{0}/api/BadgeUser/id?AccountName={1}", Config.ApiUrl, uc);
            ReturnIntResult result = await HttpHelper_Json.GetJsonAsync<ReturnIntResult>(url);
            if (result != null && result.success) {
                return result.intResult;
            }
            log.DebugFormat("GetBadgeUserIdAsync: handled {0}, handled result: {1}", result.success, result.msg);
            return null;
        }
    }
}
