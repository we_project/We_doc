﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.Approver;
using Daimler.MicroServices.SP.BizObject.SPUser;
using Daimler.MicroServices.SP.Service.AccessManagement;
using log4net;

namespace Daimler.MicroServices.SP.BLL {
    public class ApproverService {

        private static readonly ILog log = LogManager.GetLogger(typeof(ApproverService));
        private UserService userService;
        private EIBMService eibmService;
        private VisitorBadgeService vbService;
        UserCredential uc;
        public ApproverService(UserCredential uc) {
            if (uc == null) {
                throw new ArgumentNullException();
            }
            this.uc = uc;
            this.userService = new UserService(uc);
            this.eibmService = new EIBMService(uc);
            this.vbService = new VisitorBadgeService(uc);
        }
        public async Task<PendingCount> GetPendingCountSnapshot() {
            log.Info(new {
                uc = uc,
                action = "GetPendingCountSnapshot"
            });
            PendingCount result = new PendingCount();

            Task<int> t1 = null;
            Task<int> t2 = null;
            Task<int> t3 = null;
            Task<int> t4 = null;
            Task<int> t5 = null;
            Task<int> t6 = null;
            //Task<int> t7 = null;
            //Task<int> t8 = null;

            Task<int?> commonUserTask = userService.GetCommonUserIdAsync();
            Task<int?> formABUserTask = userService.GetFormABUserIdAsync();
            //Task<int?> badgeUserTask = userService.GetBadgeUserIdAsync();

            //int? badgeUserId = await badgeUserTask;
            //if (badgeUserId.HasValue) {
            //    t7 = GetPendingEIBMCountAsync(badgeUserId.Value);
            //    t8 = GetPendingVisitorBadgeCountAsync(badgeUserId.Value);
            //}

            int? commonUserid = await commonUserTask;
            if (commonUserid.HasValue) {
                t1 = GetPendingStaionaryCountAsync(commonUserid.Value);
                t2 = GetPendingBCCountAsync(commonUserid.Value);
                t3 = GetPendingILCountAsync(commonUserid.Value);
                t4 = GetPendingPCCACountAsync(uc.ToString());
            }
            int? formABUserid = await formABUserTask;
            if (formABUserid.HasValue) {
                t5 = GetPendingFormACountAsync(formABUserid.Value);
                t6 = GetPendingFormBCountAsync(formABUserid.Value);
            }


            IEnumerable<Task> tasks = new List<Task>(){
                                 t1,t2,t3,t4,t5,t6,
                                 //t7,t8
            }.Where(t => t != null);
            await Task.WhenAll(tasks);
            result.Stationary = t1 != null ? t1.Result : 0;
            result.BussinessCard = t2 != null ? t2.Result : 0;
            result.InvitationLetter = t3 != null ? t3.Result : 0;
            result.PetrolCardCreditAdjustment = t4 != null ? t4.Result : 0;
            result.FormA = t5 != null ? t5.Result : 0;
            result.FormB = t6 != null ? t6.Result : 0;
            //result.EIBM = t7 != null ? t7.Result : 0;
            //result.VisitorBadgeApplication = t8 != null ? t8.Result : 0;
            log.Info(new {
                uc = uc,
                action = "GetPendingCountSnapshot",
                result = result
            });
            return result;
        }


        public async Task<BadgePendingCount> GetBadgePendingCountSnapshot() {
            log.Info(new {
                uc = uc,
                action = "GetBadgePendingCountSnapshot"
            });
            BadgePendingCount result = new BadgePendingCount();

            Task<int> t7 = null;
            Task<int> t8 = null;

            Task<int?> badgeUserTask = userService.GetBadgeUserIdAsync();

            int? badgeUserId = await badgeUserTask;
            if (badgeUserId.HasValue) {
                t7 = GetPendingEIBMCountAsync(badgeUserId.Value);
                t8 = GetPendingVisitorBadgeCountAsync(badgeUserId.Value);
            }

            IEnumerable<Task> tasks = new List<Task>(){
                                 t7,t8
            }.Where(t => t != null);
            await Task.WhenAll(tasks);
            result.EIBM = t7 != null ? t7.Result : 0;
            result.VisitorBadgeApplication = t8 != null ? t8.Result : 0;
            log.Info(new {
                uc = uc,
                action = "GetPendingCountSnapshot",
                result = result
            });
            return result;
        }
        public async Task<int> GetPendingStaionaryCountAsync(int userId) {
            try {
                return await OfficeStationaryService.GetPendingCountAsync(userId);
            } catch (Exception ex) {
                log.Error(this, ex);
                return 0;
            }
        }

        public async Task<int> GetPendingBCCountAsync(int userId) {
            try {
                return await WorkflowTasksService.GetPendingBCCountAsync(userId);
            } catch (Exception ex) {
                log.Error(this, ex);
                return 0;
            }
        }

        public async Task<int> GetPendingILCountAsync(int userId) {
            try {
                return await WorkflowTasksService.GetPendingILCountAsync(userId);
            } catch (Exception ex) {
                log.Error(this, ex);
                return 0;
            }
        }
        public async Task<int> GetPendingPCCACountAsync(string supervisor) {
            try {
                return await PetrolCardService.GetPendingCountAsync(supervisor);
            } catch (Exception ex) {
                log.Error(this, ex);
                return 0;
            }
        }
        public async Task<int> GetPendingFormACountAsync(int userId) {
            try {
                return await FormAService.GetPendingCountAsync(userId);
            } catch (Exception ex) {
                log.Error(this, ex);
                return 0;
            }
        }
        public async Task<int> GetPendingFormBCountAsync(int userId) {
            try {
                return await FormBService.GetPendingCountAsync(userId);
            } catch (Exception ex) {
                log.Error(this, ex);
                return 0;
            }
        }
        public async Task<int> GetPendingEIBMCountAsync(int userId) {
            try {
                //return await eibmService.GetPendingCountAsync(userId);
                return await EIBMService.GetPendingEIBMCountAsync(userId);
            } catch (Exception ex) {
                log.Error(this, ex);
                return 0;
            }
        }
        private async Task<int> GetPendingVisitorBadgeCountAsync(int userId) {
            try {
                //return await vbService.GetPendingCountAsync(userId);
                return await VisitorBadgeService.GetPendingVisitorBadgeCountAsync(userId);
            } catch (Exception ex) {
                log.Error(this, ex);
                return 0;
            }
        }
    }
}
