﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.Approver;
using Daimler.MicroServices.SP.BizObject.FormAB;
using Daimler.MicroServices.SP.Service.Common;
using Daimler.MicroServices.SP.Service.FormAB;
using Daimler.MicroServices.Utils.Network;
using log4net;

namespace Daimler.MicroServices.SP.BLL {
    public class FormAService {
        private static readonly ILog log = LogManager.GetLogger(typeof(FormAService));
        private UserService userService;
        private readonly IFormAB formABservice;
        private readonly ICommon commonService;
        UserCredential uc;
        public FormAService(UserCredential uc) {
            if (uc == null) {
                throw new ArgumentNullException();
            }
            this.uc = uc;
            this.userService = new UserService(uc);
            this.commonService = new CommonImpl(uc);
            this.formABservice = new FormABImpl(uc);
        }

        public static async Task<bool> SaveFormAInfoAsync(List<FormAInfo> infoes) {
            string url = string.Format("{0}/api/FormA", Config.ApiUrl);
            ReturnBoolResult result = await HttpHelper_Json.PostJsonAsync<ReturnBoolResult, List<FormAInfo>>(url, infoes);
            if (result.success && result.boolResult) {
                return true;
            }
            log.DebugFormat("post FormAInfo to dataApi result: handled {0}, handled result: {1}", result.success, result.msg);
            return false;
        }
        public static async Task<bool> UpdateFormAAsync(List<FormAInfo> infoes) {
            string url = string.Format("{0}/api/FormA/Update", Config.ApiUrl);
            ReturnBoolResult result = await HttpHelper_Json.PostJsonAsync<ReturnBoolResult, List<FormAInfo>>(url, infoes);
            if (result.success && result.boolResult) {
                return true;
            }
            log.DebugFormat("post FormAInfo to dataApi result: handled {0}, handled result: {1}", result.success, result.msg);
            return false;
        }

        public static async Task<bool> SaveFormACommentHistoryInfoAsync(List<FormACommentHistoryInfo> infoes) {
            string url = string.Format("{0}/api/FormA/CommentHistory", Config.ApiUrl);
            ReturnBoolResult result = await HttpHelper_Json.PostJsonAsync<ReturnBoolResult, List<FormACommentHistoryInfo>>(url, infoes);
            if (result.success && result.boolResult) {
                return true;
            }
            log.DebugFormat("post FormACommentHistoryInfo to dataApi result: handled {0}, handled result: {1}", result.success, result.msg);
            return false;
        }
        public static async Task<int> GetPendingCountAsync(int userId) {
            string url = string.Format("{0}/api/FormA/{1}/Pending", Config.ApiUrl, userId);
            ReturnIntResult result = await HttpHelper_Json.GetJsonAsync<ReturnIntResult>(url);
            if (result.success) {
                return result.intResult.HasValue ? result.intResult.Value : 0;
            }
            log.DebugFormat("GetPendingCountAsync result: handled {0}, handled result: {1}", result.success, result.msg);
            return 0;
        }
        public async Task<List<FormAItem>> GetFormAList(int? pageIndex, int? pageSize) {
            List<FormAItem> result = new List<FormAItem>();
            int? commonUserid = await userService.GetFormABUserIdAsync();
            if (commonUserid.HasValue) {
                string url = string.Format("{0}/api/FormA/{1}/PendingList?pageIndex={2}&pageSize={3}", Config.ApiUrl, commonUserid.Value, pageIndex, pageSize);
                ReturnFormAPendingList returnList = await HttpHelper_Json.GetJsonAsync<ReturnFormAPendingList>(url);
                if (returnList.success) {
                    return returnList.list;
                }
            }
            return null;
        }
        public async Task<FormADetail> GetFormADetail(int id) {
            string url = string.Format("{0}/api/FormA/{1}/Detail", Config.ApiUrl, id);
            ReturnFormADetail returnList = await HttpHelper_Json.GetJsonAsync<ReturnFormADetail>(url);
            if (returnList.success) {
                return returnList.detail;
            }
            return null;
        }
        public async Task<ApproveResult> Approval(bool approve, int ID, string comment, string StatusIndex, FormATaxTeamFields taxTeamField) {
            ApproveResult result = new ApproveResult();

            string userName = await userService.GetFormABUserNameAsync();
            result = formABservice.ApprovalFormA(userName, approve, ID, comment, StatusIndex, taxTeamField);
            if (!result.Success) {
                result.Msg = AllErrors.InvalidUser;
            }
            // get newest form data
            FormAInfo info = commonService.GetListItemById<FormAInfo>(ID);
            bool save = await UpdateFormAAsync(new List<FormAInfo>() { info });
            log.DebugFormat("save FormAInfo for ID:{0},{1}", ID, save);
            return result;
        }
    }
}
