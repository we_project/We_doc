﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.Workflow;
using Daimler.MicroServices.SP.Service.Common;
using Daimler.MicroServices.Utils.Network;
using log4net;

namespace Daimler.MicroServices.SP.BLL {
    public class WorkflowTasksService {
        private static readonly ILog log = LogManager.GetLogger(typeof(WorkflowTasksService));
        private UserService userService;
        //private readonly IOfficeStationary officeStationaryService;
        private readonly ICommon commonService;
        UserCredential uc;
        public WorkflowTasksService(UserCredential uc) {
            if (uc == null) {
                throw new ArgumentNullException();
            }
            this.uc = uc;
            this.userService = new UserService(uc);
            //this.officeStationaryService = new OfficeStationaryImpl(uc);
            this.commonService = new CommonImpl(uc);
        }
        public static async Task<bool> SaveWorkflowTasksAsync(List<WorkflowTaskInfo> infoes) {
            string url = string.Format("{0}/api/WorkflowTasks", Config.ApiUrl);
            ReturnBoolResult result = await HttpHelper_Json.PostJsonAsync<ReturnBoolResult, List<WorkflowTaskInfo>>(url, infoes);
            if (result.success && result.boolResult) {
                return true;
            }
            log.DebugFormat("post WorkflowTasks to dataApi result: handled {0}, handled result: {1}", result.success, result.msg);
            return false;
        }
        public static async Task<bool> UpdateWorkflowTasksAsync(List<WorkflowTaskInfo> infoes) {
            string url = string.Format("{0}/api/WorkflowTasks/Update", Config.ApiUrl);
            ReturnBoolResult result = await HttpHelper_Json.PostJsonAsync<ReturnBoolResult, List<WorkflowTaskInfo>>(url, infoes);
            if (result.success && result.boolResult) {
                return true;
            }
            log.DebugFormat("post WorkflowTasks to dataApi result: handled {0}, handled result: {1}", result.success, result.msg);
            return false;
        }
        public static async Task<bool> MarkApprovedWorkflowTasksAsync(int ID) {
            string url = string.Format("{0}/api/WorkflowTasks/{1}/MarkApproved", Config.ApiUrl, ID);
            ReturnBoolResult result = await HttpHelper_Json.PostJsonAsync<ReturnBoolResult, string>(url, null);
            if (result.success && result.boolResult) {
                return true;
            }
            log.DebugFormat("post WorkflowTasks to dataApi result: handled {0}, handled result: {1}", result.success, result.msg);
            return false;
        }
        public static async Task<int> GetPendingBCCountAsync(int userId) {
            string url = string.Format("{0}/api/WorkflowTasks/{1}/PendingBCCount", Config.ApiUrl, userId);
            ReturnIntResult result = await HttpHelper_Json.GetJsonAsync<ReturnIntResult>(url);
            if (result.success) {
                return result.intResult.HasValue ? result.intResult.Value : 0;
            }
            log.DebugFormat("GetPendingBCCountAsync result: handled {0}, handled result: {1}", result.success, result.msg);
            return 0;
        }
        public static async Task<int> GetPendingILCountAsync(int userId) {
            string url = string.Format("{0}/api/WorkflowTasks/{1}/PendingILCount", Config.ApiUrl, userId);
            ReturnIntResult result = await HttpHelper_Json.GetJsonAsync<ReturnIntResult>(url);
            if (result.success) {
                return result.intResult.HasValue ? result.intResult.Value : 0;
            }
            log.DebugFormat("GetPendingILCountAsync result: handled {0}, handled result: {1}", result.success, result.msg);
            return 0;
        }
        public static async Task<int?> GetPendingWorkflowTasksByItemId(int userId, int itemId, string workflowName) {
            string url = string.Format("{0}/api/WorkflowTasks/{1}/{2}/{3}/ID", Config.ApiUrl, userId, workflowName, itemId);
            ReturnIntResult result = await HttpHelper_Json.GetJsonAsync<ReturnIntResult>(url);
            if (result.success) {
                return result.intResult;
            }
            log.DebugFormat("GetPendingWorkflowTasksByItemId result: handled {0}, handled result: {1}", result.success, result.msg);
            return null;
        }



    }
}
