﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.Approver;
using Daimler.MicroServices.SP.BizObject.BusinessCard;
using Daimler.MicroServices.SP.BizObject.Workflow;
using Daimler.MicroServices.SP.Service.Common;
using Daimler.MicroServices.SP.Service.Workflow;
using Daimler.MicroServices.Utils.Network;
using log4net;

namespace Daimler.MicroServices.SP.BLL {
    public class BusinessCardService {
        private static readonly ILog log = LogManager.GetLogger(typeof(BusinessCardService));
        private UserService userService;
        private readonly ICommon commonService;
        private readonly IWorkflowTasks workflowTasksService;
        UserCredential uc;
        public BusinessCardService(UserCredential uc) {
            if (uc == null) {
                throw new ArgumentNullException();
            }
            this.uc = uc;
            this.userService = new UserService(uc);
            this.commonService = new CommonImpl(uc);
            this.workflowTasksService = new WorkflowTasksImpl(uc);
        }

        public static async Task<bool> SaveBCApplicatoinAsync(List<BCApplicationInfo> infoes) {
            string url = string.Format("{0}/api/BC", Config.ApiUrl);
            ReturnBoolResult result = await HttpHelper_Json.PostJsonAsync<ReturnBoolResult, List<BCApplicationInfo>>(url, infoes);
            if (result.success && result.boolResult) {
                return true;
            }
            log.DebugFormat("post BCApplicationInfo to dataApi result: handled {0}, handled result: {1}", result.success, result.msg);
            return false;
        }
        public static async Task<bool> UpdateBCApplicatoinAsync(List<BCApplicationInfo> infoes) {
            string url = string.Format("{0}/api/BC/Update", Config.ApiUrl);
            ReturnBoolResult result = await HttpHelper_Json.PostJsonAsync<ReturnBoolResult, List<BCApplicationInfo>>(url, infoes);
            if (result.success && result.boolResult) {
                return true;
            }
            log.DebugFormat("post BCApplicationInfo to dataApi result: handled {0}, handled result: {1}", result.success, result.msg);
            return false;
        }
        public async Task<List<BusinessCardApplicationItem>> GetBCApplicatoinList(int? pageIndex, int? pageSize) {
            List<BusinessCardApplicationItem> result = new List<BusinessCardApplicationItem>();
            int? commonUserid = await userService.GetCommonUserIdAsync();
            if (commonUserid.HasValue) {
                string url = string.Format("{0}/api/BC/{1}/PendingList?pageIndex={2}&pageSize={3}", Config.ApiUrl, commonUserid.Value, pageIndex, pageSize);
                ReturnBusinessCardPendingList returnList = await HttpHelper_Json.GetJsonAsync<ReturnBusinessCardPendingList>(url);
                if (returnList.success) {
                    return returnList.list;
                }
            }
            return null;
        }
        public async Task<BusinessCardApplicationDetail> GetBCApplicatoinDetail(int id) {
            string url = string.Format("{0}/api/BC/{1}/Detail", Config.ApiUrl, id);
            ReturnBusinessCardApplicationDetail returnList = await HttpHelper_Json.GetJsonAsync<ReturnBusinessCardApplicationDetail>(url);
            if (returnList.success) {
                return returnList.detail;
            }
            return null;
        }

        public async Task<ApproveResult> Approval(bool approve, int ID, string comment) {
            ApproveResult result = new ApproveResult();
            // if the newest task is not equal to current task, return status changed, update current task
            // if equal, then approve
            int? commonUserid = await userService.GetCommonUserIdAsync();
            if (commonUserid.HasValue) {
                Task<int?> t1 = workflowTasksService.GetPendingWorkflowTasksByItemIdAsync(commonUserid.Value, ID, WorkflowNames.BusinessCardApproveProcess);
                Task<int?> t2 = WorkflowTasksService.GetPendingWorkflowTasksByItemId(commonUserid.Value, ID, WorkflowNames.BusinessCardApproveProcess);
                int? wfId = await t1;
                int? currentWfId = await t2;
                log.DebugFormat("approve bc: {0}, newest workflowTask id: {1},current wf id: {2}", ID, wfId.HasValue ? wfId.Value.ToString() : "null", currentWfId.HasValue ? currentWfId.Value.ToString() : "null");
                if (wfId.HasValue && currentWfId.HasValue && wfId.Value.Equals(currentWfId.Value)) {
                    try {
                        result = workflowTasksService.Approve(wfId.Value, approve, comment);
                        // if success, mark this item as complete.
                        if (result != null && result.Success) {
                            bool markApproved = await WorkflowTasksService.MarkApprovedWorkflowTasksAsync(currentWfId.Value);
                            log.Info(new {
                                action = "mark approved for workflow task",
                                wfId = currentWfId.Value,
                                result = markApproved
                            });
                        }
                    } catch (Exception ex) {
                        result.Success = false;
                        result.Msg = AllErrors.Others;
                        log.ErrorFormat("user {0} approve BC:{1} failed wf id: {3}, {2}", commonUserid.Value, ID, ex, wfId.Value);
                    }
                }
                if (currentWfId.HasValue) {
                    // todo: test this method, why it failed the first time
                    // get newest request data
                    WorkflowTaskInfo info = commonService.GetListItemById<WorkflowTaskInfo>(currentWfId.Value);
                    bool save = await WorkflowTasksService.UpdateWorkflowTasksAsync(new List<WorkflowTaskInfo>() { info });
                    log.DebugFormat("save WorkflowTaskInfo for ID:{0},{1}", ID, save);
                }
            }
            return result;
        }

    }
}
