﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.AccessManagement;
using Daimler.MicroServices.SP.Service.Common;
using Daimler.MicroServices.Utils.Network;
using log4net;

namespace Daimler.MicroServices.SP.BLL {
    public class BadgeService {
        private static readonly ILog log = LogManager.GetLogger(typeof(BadgeService));
        private UserService userService;
        private readonly ICommon commonService;
        UserCredential uc;
        public BadgeService(UserCredential uc) {
            if (uc == null) {
                throw new ArgumentNullException();
            }
            this.uc = uc;
            this.userService = new UserService(uc);
            this.commonService = new CommonImpl(uc);
        }

        public static async Task<bool> SaveCCDeputyAsync(List<CostCenterDeputyProfileInfo> infoes) {
            string url = string.Format("{0}/api/CCDeputy", Config.ApiUrl);
            ReturnBoolResult result = await HttpHelper_Json.PostJsonAsync<ReturnBoolResult, List<CostCenterDeputyProfileInfo>>(url, infoes);
            if (result.success && result.boolResult) {
                return true;
            }
            log.DebugFormat("post CostCenterDeputyProfileInfo to dataApi result: handled {0}, handled result: {1}", result.success, result.msg);
            return false;
        }


        public async Task<List<CostCenterDeputyProfileInfo>> GetCCDeputys() {
            List<CostCenterDeputyProfileInfo> result = new List<CostCenterDeputyProfileInfo>();
            int? badgeUserId = await userService.GetBadgeUserIdAsync();
            if (badgeUserId.HasValue) {
                string url = string.Format("{0}/api/CCDeputy/{1}", Config.ApiUrl, badgeUserId.Value);
                ReturnCostCenterDeputyProfileResult returnList = await HttpHelper_Json.GetJsonAsync<ReturnCostCenterDeputyProfileResult>(url);
                if (returnList.success) {
                    return returnList.list;
                }
            }
            return null;
        }
        public static async Task<List<CostCenterDeputyProfileInfo>> GetCCDeputys(int deputyId) {
            List<CostCenterDeputyProfileInfo> result = new List<CostCenterDeputyProfileInfo>();
            string url = string.Format("{0}/api/CCDeputy/{1}", Config.ApiUrl, deputyId);
            ReturnCostCenterDeputyProfileResult returnList = await HttpHelper_Json.GetJsonAsync<ReturnCostCenterDeputyProfileResult>(url);
            if (returnList.success) {
                return returnList.list;
            }
            return null;
        }

        public static async Task<bool> SaveSitesAsync(List<SiteInfo> infoes) {
            string url = string.Format("{0}/api/Site", Config.ApiUrl);
            ReturnBoolResult result = await HttpHelper_Json.PostJsonAsync<ReturnBoolResult, List<SiteInfo>>(url, infoes);
            if (result.success && result.boolResult) {
                return true;
            }
            log.DebugFormat("post SiteInfo to dataApi result: handled {0}, handled result: {1}", result.success, result.msg);
            return false;
        }

        public static async Task<bool> SaveLocationsAsync(List<LocationInfo> infoes) {
            string url = string.Format("{0}/api/Location", Config.ApiUrl);
            ReturnBoolResult result = await HttpHelper_Json.PostJsonAsync<ReturnBoolResult, List<LocationInfo>>(url, infoes);
            if (result.success && result.boolResult) {
                return true;
            }
            log.DebugFormat("post LocationInfo to dataApi result: handled {0}, handled result: {1}", result.success, result.msg);
            return false;
        }

        public static async Task<bool> SaveAreasAsync(List<AreaInfo> infoes) {
            string url = string.Format("{0}/api/Area", Config.ApiUrl);
            ReturnBoolResult result = await HttpHelper_Json.PostJsonAsync<ReturnBoolResult, List<AreaInfo>>(url, infoes);
            if (result.success && result.boolResult) {
                return true;
            }
            log.DebugFormat("post AreaInfo to dataApi result: handled {0}, handled result: {1}", result.success, result.msg);
            return false;
        }
    }
}
