﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.AccessManagement;
using Daimler.MicroServices.SP.Service.AccessManagement;
using Daimler.MicroServices.Utils.Network;
using log4net;
using SP.Data;

namespace Daimler.MicroServices.SP.BLL {
    public class VisitorBadgeService {
        private static readonly ILog log = LogManager.GetLogger(typeof(VisitorBadgeService));
        private UserService userService;
        private VisitorBadgeRestful spVisitorBadgeRestful;
        UserCredential uc;
        public VisitorBadgeService(UserCredential uc) {
            if (uc == null) {
                throw new ArgumentNullException();
            }
            this.uc = uc;
            this.userService = new UserService(uc);
            this.spVisitorBadgeRestful = new VisitorBadgeRestful(uc);
        }


        public async Task<int> GetPendingCountAsync(int userId) {
            spVisitorBadgeRestful.CurrentUserId = userId;
            spVisitorBadgeRestful.ccdeputys = await BadgeService.GetCCDeputys(userId);
            return await Task.Factory.StartNew<int>(() => {
                return spVisitorBadgeRestful.GetPendingCount();
            });
        }


        public async Task<IEnumerable<VisitorBadgeItem>> GetPendingList() {
            int? badgeUserid = await userService.GetBadgeUserIdAsync();
            if (badgeUserid.HasValue) {
                spVisitorBadgeRestful.CurrentUserId = badgeUserid.Value;
                spVisitorBadgeRestful.ccdeputys = await BadgeService.GetCCDeputys(badgeUserid.Value);
                return spVisitorBadgeRestful.GetPendingList();
            }
            return null;
        }

        public async Task<VisitorBadgeApproveResult[]> BatchApproval(VisitorBadgeApprovalPost post) {
            if (post != null && post.infoes != null && post.infoes.Count() > 0) {
                int? badgeUserid = await userService.GetBadgeUserIdAsync();
                if (badgeUserid.HasValue) {
                    spVisitorBadgeRestful.CurrentUserId = badgeUserid.Value;
                    spVisitorBadgeRestful.ccdeputys = await BadgeService.GetCCDeputys(badgeUserid.Value);
                    // get item new data
                    EIBMInfoForApproval detail = await spVisitorBadgeRestful.GetItemForApproval(post.ID);
                    List<Task<VisitorBadgeApproveResult>> tasks = new List<Task<VisitorBadgeApproveResult>>();
                    foreach (var item in post.infoes) {
                        tasks.Add(spVisitorBadgeRestful.ApprovalAsync(detail, item));
                    }
                    VisitorBadgeApproveResult[] result = await Task.WhenAll(tasks.ToArray());
                    log.InfoFormat("BatchApprovalUseRestful complete for VisitorBadge {0}", post.ID);

                    // when approved, then load the newest item, and upload it to db
                    VistorBadgeApplicationInfo info = await spVisitorBadgeRestful.GetItemByIdAsync<VistorBadgeApplicationInfo>(post.ID);
                    bool save = await UpdateVisitorBadgesAsync(new List<VistorBadgeApplicationInfo>() { info });
                    log.InfoFormat("save VistorBadgeApplicationInfo for ID:{0},{1}", post.ID, save);

                    return result;
                }
            }
            return null;

        }




        public static async Task<bool> SaveVisitorBadgesAsync(List<VistorBadgeApplicationInfo> infoes) {
            string url = string.Format("{0}/api/VisitorBadge", Config.ApiUrl);
            ReturnBoolResult result = await HttpHelper_Json.PostJsonAsync<ReturnBoolResult, List<VistorBadgeApplicationInfo>>(url, infoes);
            if (result.success && result.boolResult) {
                return true;
            }
            log.DebugFormat("post VistorBadgeApplicationInfo to dataApi result: handled {0}, handled result: {1}", result.success, result.msg);
            return false;
        }

        public static async Task<bool> UpdateVisitorBadgesAsync(List<VistorBadgeApplicationInfo> infoes) {
            string url = string.Format("{0}/api/VisitorBadge/Update", Config.ApiUrl);
            ReturnBoolResult result = await HttpHelper_Json.PostJsonAsync<ReturnBoolResult, List<VistorBadgeApplicationInfo>>(url, infoes);
            if (result.success && result.boolResult) {
                return true;
            }
            log.DebugFormat("post VisitorBadge to dataApi result: handled {0}, handled result: {1}", result.success, result.msg);
            return false;
        }
        /// <summary>
        /// get from db
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static async Task<int> GetPendingVisitorBadgeCountAsync(int userId) {
            string url = string.Format("{0}/api/VisitorBadge/{1}/PendingCount", Config.ApiUrl, userId);
            ReturnIntResult result = await HttpHelper_Json.GetJsonAsync<ReturnIntResult>(url);
            if (result.success) {
                return result.intResult.HasValue ? result.intResult.Value : 0;
            }
            log.DebugFormat("GetPendingVisitorBadgeCountAsync result: handled {0}, handled result: {1}", result.success, result.msg);
            return 0;
        }

        /// <summary>
        /// get from db
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<VisitorBadgeItem>> GetPendingVisitorBadgeList() {
            List<VisitorBadgeItem> result = new List<VisitorBadgeItem>();
            int? badgeUserid = await userService.GetBadgeUserIdAsync();
            if (badgeUserid.HasValue) {
                string url = string.Format("{0}/api/VisitorBadge/{1}/PendingList", Config.ApiUrl, badgeUserid.Value);
                ReturnVisitorBadgePendingList returnList = await HttpHelper_Json.GetJsonAsync<ReturnVisitorBadgePendingList>(url);
                if (returnList.success) {
                    return returnList.list;
                }
            }
            return null;
        }


        /// <summary>
        /// get from db
        /// </summary>
        /// <returns></returns>
        public async Task<VisitorBadgeDetail1> GeVisitorBadgeDetail(int id) {
            string url = string.Format("{0}/api/VisitorBadge/{1}/Detail", Config.ApiUrl, id);
            ReturnVisitorBadgeDetail returnList = await HttpHelper_Json.GetJsonAsync<ReturnVisitorBadgeDetail>(url);
            if (returnList.success) {
                return returnList.detail;
            }
            return null;
        }
    }
}
