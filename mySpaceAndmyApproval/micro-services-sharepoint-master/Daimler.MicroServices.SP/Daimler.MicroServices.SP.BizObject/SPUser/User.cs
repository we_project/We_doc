﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.SPUser {
    public class User {
        public int Id { get; set; }
        public string LoginName { get; set; }
        public string Title { get; set; }

        public string DomainName {
            get {
                string[] splited=LoginName.Split(new char[] { '|' });
                string domainName = splited.Last();
                //Logs.DefaultFileLog.Write(string.Format("get user domain name: {0}({1})", LoginName, domainName));
                return domainName;
            }
        }
    }
}
