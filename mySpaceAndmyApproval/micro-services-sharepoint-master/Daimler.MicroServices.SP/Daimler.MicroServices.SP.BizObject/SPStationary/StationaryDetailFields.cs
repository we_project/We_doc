﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.SPStationary {
    public class StationaryDetailFields {
        public static List<string> FieldsToLoad = new List<string>() {
        "ThumbnailExists",
        "ItemName",
        "Price_x0028_with_x0020_Tax_x0029_",
        "ItemName_EN",
        "ID",
        "EncodedAbsThumbnailUrl",
        "EncodedAbsWebImgUrl"
      };
    }
}
