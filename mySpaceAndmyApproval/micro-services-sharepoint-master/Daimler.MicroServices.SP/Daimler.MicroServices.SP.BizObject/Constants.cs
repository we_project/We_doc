﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject {
    public class Constants {

        public static string LogFilePrefix = "Sharepoint";


        public static class CSharpTypes {

            public static string _string = "string";
            public static string _dateTime = "DateTime";
            public static string _int = "int";
            public static string _double = "double";
            public static string _bool = "bool";
        }

    }
}
