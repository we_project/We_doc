﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject {
    public enum ReturnMsgSP {
        SPLoginFailed,
        ExceptionOccurs = 1,
        RequiredParameterIsNull = 2,
        
    }
}
