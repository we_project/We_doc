﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.BusinessCard {
    public class State {
        public static string Draft = "Draft";
        public static string Submitted = "Submitted";
        public static string SupervisorApproval = "Supervisor Approval";
        public static string HRContactApproval = "HR Contact Approval";
        public static string HRHeadApproval = "HR Head Approval";
        public static string RejectedToEdit = "Rejected To Edit";
        public static string WaitingForTheCardSmapleFromAdminContact = "Waiting For The Card Smaple From Admin Contact";
        public static string ConfirmingCardSampleWithRequestor = "Confirming Card Sample With Requestor";
        public static string PreparingCard = "Preparing Card";
        public static string CardisReady = "Card is Ready";
        public static string AskforOverprint = "Ask for Overprint";
        public static string Overprint_PreparingCard = "Overprint_Preparing Card";
    }
}
