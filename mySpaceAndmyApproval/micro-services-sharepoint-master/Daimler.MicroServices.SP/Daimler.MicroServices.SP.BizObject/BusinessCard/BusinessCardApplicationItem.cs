﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.BusinessCard {
    public class BusinessCardApplicationItem {
        //public string Title { get; set; }
        public string UserName { get; set; }
        //public string EmployeeNumber { get; set; }
        //public string CompanyName { get; set; }
        //public string CostCenter { get; set; }
        //public string ExtensionNumber { get; set; }
        //public string FirstName { get; set; }
        //public string LastName { get; set; }
        public string DepartmentName { get; set; }
        //public string PhoneNo { get; set; }
        //public string Email { get; set; }
        //public string Position { get; set; }
        //public string FixNo { get; set; }
        //public string Address { get; set; }
        public DateTime Modified { get; set; }
        public DateTime Created { get; set; }
        //public string Business { get; set; }
        //public string BusinessCardType { get; set; }
        //public string FirstNameCN { get; set; }
        //public string SecondNameCN { get; set; }
        //public string DepartmentCN { get; set; }
        //public string PositionCN { get; set; }
        //public string AddressCN { get; set; }
        //public string RequestorLoginName { get; set; }
        //public string Supervisor { get; set; }
        public string State { get; set; }
        //public string Location { get; set; }
        //public string EmployeeType { get; set; }
        //public string HRKA { get; set; }
        //public string cWorkflowHistory { get; set; }
        //public string AdminContact { get; set; }
        //public string Overprint { get; set; }
        //public string Business0 { get; set; }
        public int ID { get; set; }

    }
}
