﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.BusinessCard {
    public class BCApplicationInfoFields {
        public static List<string> FieldsToLoad = new List<string>() {
           "Title",
        "UserName",
        "EmployeeNumber",
        "CompanyName",
        "CostCenter",
        "ExtensionNumber",
        "FirstName",
        "LastName",
        "DepartmentName",
        "PhoneNo",
        "Email",
        "Position",
        "FixNo",
        "Address",
        "Modified",
        "Created",
        "Business",
        "BusinessCardType",
        "FirstNameCN",
        "SecondNameCN",
        "DepartmentCN",
        "PositionCN",
        "AddressCN",
        "RequestorLoginName",
        "Supervisor",
        "State",
        "Location",
        "EmployeeType",
        "HRKA",
        "cWorkflowHistory",
        "AdminContact",
        "Overprint",
        "Business0",
        //"Remark",
        "ID"
       };
    }
}
