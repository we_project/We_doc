﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Daimler.MicroServices.SP.BizObject.BusinessCard;

namespace Daimler.MicroServices.SP.BizObject.BusinessCard {
    public class ReturnBusinessCardApplicationDetail:BaseReturnSP {
        public BusinessCardApplicationDetail detail { get; set; }
    }
}