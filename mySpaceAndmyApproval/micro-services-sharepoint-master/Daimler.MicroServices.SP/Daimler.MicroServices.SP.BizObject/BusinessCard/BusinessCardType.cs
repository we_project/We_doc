﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.BusinessCard {
    public class BusinessCardType {
        public static string EnglishAndChinese = "English & Chinese";
        public static string English = "English";

        public static string Chinese = "Chinese";
    }
}
