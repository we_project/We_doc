﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.BusinessCard {
    public class BCHRKAInfo {
        public string Title { get; set; }
        public string HRKA { get; set; }
        public string ResponsibleType { get; set; }
        public int ID { get; set; }
        public DateTime Modified { get; set; }
        public DateTime Created { get; set; }
    }
}
