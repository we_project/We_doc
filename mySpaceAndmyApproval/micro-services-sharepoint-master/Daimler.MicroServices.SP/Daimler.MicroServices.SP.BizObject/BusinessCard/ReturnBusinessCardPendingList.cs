﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Daimler.MicroServices.SP.BizObject;

namespace Daimler.MicroServices.SP.BizObject.BusinessCard {
    public class ReturnBusinessCardPendingList : BaseReturnSP {
        public List<BusinessCardApplicationItem> list { get; set; }

    }
}