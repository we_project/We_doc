﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.BusinessCard {
    public class BCHRKAInfoFields {
        public static List<string> FieldsToLoad = new List<string>() {
            "Title",
"HRKA",
"ResponsibleType",
"ID",
"Modified",
"Created"
        };
    }
}
