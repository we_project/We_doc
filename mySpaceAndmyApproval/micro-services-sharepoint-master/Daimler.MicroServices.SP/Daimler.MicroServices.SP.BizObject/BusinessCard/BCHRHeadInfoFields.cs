﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.BusinessCard {
    public class BCHRHeadInfoFields {
        public static List<string> FieldsToLoad = new List<string>() {
            
"Title",
"HRAdminHead",
"ID",
"Modified",
"Created"
        };

    }
}
