﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject {
    public class SPUserInfo {
        public int ID { get; set; }
        /// <summary>
        /// name
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// apac\tonglli
        /// </summary>
        public string AccountName { get; set; }
        public string EMail { get; set; }
    }
}
