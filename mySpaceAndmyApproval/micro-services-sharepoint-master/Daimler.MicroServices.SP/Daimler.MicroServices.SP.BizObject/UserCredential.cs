﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject {
    /// <summary>
    /// user account used to login sharepoint site
    /// </summary>
    public class UserCredential {
        public string domain { get; set; }
        public string userName { get; set; }
        public string psw { get; set; }
        public override string ToString() {
            return string.Format("{0}\\{1}", domain, userName);
        }
        public static class Fields {
            public static readonly string domain = "domain";
            public static readonly string userName = "userName";
            public static readonly string psw = "psw";
        }
    }
}
