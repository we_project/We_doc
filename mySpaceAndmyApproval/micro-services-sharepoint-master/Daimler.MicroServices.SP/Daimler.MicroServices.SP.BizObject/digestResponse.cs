﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject {
    public class digestResponse {
        public digestResponse_http d { get; set; }
    }

    public class digestResponse_http {
        public formDigest GetContextWebInformation { get; set; }
    }
    public class formDigest {
        public string FormDigestValue { get; set; }
    }
}
