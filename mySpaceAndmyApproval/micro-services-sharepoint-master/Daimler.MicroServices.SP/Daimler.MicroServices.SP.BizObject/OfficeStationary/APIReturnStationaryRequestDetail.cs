﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.OfficeStationary {
    public class APIReturnStationaryRequestDetail : BaseReturnSP {
        public APIStationaryRequestDetail detail { get; set; }
    }
}
