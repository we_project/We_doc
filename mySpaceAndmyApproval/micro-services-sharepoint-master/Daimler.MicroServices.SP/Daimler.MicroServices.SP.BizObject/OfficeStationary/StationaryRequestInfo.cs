﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.OfficeStationary {
    /// <summary>
    /// data from sharepoint?
    /// </summary>
    public class StationaryRequestInfo {
        public int ID { get; set; }
        public string Title { get; set; }
        public string UserName { get; set; }
        public string CostCenter { get; set; }
        public string Orders { get; set; }
        public string Status { get; set; }
        public string ExtensionNumber { get; set; }
        public string Company { get; set; }
        public int? LocationCode { get; set; }
        public string Location { get; set; }
        public string FullAddress { get; set; }
        public string Comments { get; set; }
        public string ApproveOutCome { get; set; }
        public string ProcessFlow { get; set; }
        public string LoginId { get; set; }
        public int? AdminLocalManager { get; set; }
        public int? AdminSeniorManager { get; set; }
        public int? AdminContact { get; set; }
        public string CostCenterManager { get; set; }
        public string CostCenterManagerName { get; set; }
        public string DetailAddress { get; set; }
        public int? CostCenterManagerPeople { get; set; }
        public double TotalAmount { get; set; }
        public string Department { get; set; }
        public string AdminContactBackup { get; set; }
        public DateTime? Created { get; set; }
        public DateTime? Modified { get; set; }
        public override string ToString() {
            StringBuilder sb = new StringBuilder();
            foreach (var prop in this.GetType().GetProperties()) {
                sb.AppendFormat("{0}: {1}\n", prop.Name, prop.GetValue(this));
            }
            return sb.ToString();
        }
    }
}
