﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.OfficeStationary {
    public class ApprovalInfo : updateBase {
        public ApprovalInfo(string listName) {
            __metadata = new metadata {
                type = metadata.GetItemTypeForListName(listName)
            };
        }
        public string Status { get; set; }
        public string ApproveOutCome { get; set; }
        public string Comments { get; set; }


    }
}
