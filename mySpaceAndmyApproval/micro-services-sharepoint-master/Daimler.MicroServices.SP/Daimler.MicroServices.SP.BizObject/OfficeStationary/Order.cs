﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.OfficeStationary {
    public class Order {
        public int ID { get; set; }
        public string ImageUrl { get; set; }
        public string Name { get; set; }
        public string ItemName_EN { get; set; }
        public double Price { get; set; }
        public int Quantity { get; set; }
        public string Remark { get; set; }
    }
}
