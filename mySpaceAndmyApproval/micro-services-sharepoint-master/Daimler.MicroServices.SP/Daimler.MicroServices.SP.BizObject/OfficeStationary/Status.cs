﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.OfficeStationary {
    public class Status {
        public static string Saved = "Saved";
        public static string Submitted = "Submitted";
        public static string CostCenterManagerApproveInProcess = "CostCenterManagerApproveInProcess";
        public static string CostCenterManagerApproveCompleted = "CostCenterManagerApproveCompleted";
        public static string AdminLocalManagerApproveInProcess = "AdminLocalManagerApproveInProcess";
        public static string AdminLocalManagerApproveCompleted = "AdminLocalManagerApproveCompleted";
        public static string AdminSeniorManagerApproveInProcess = "AdminSeniorManagerApproveInProcess";
        public static string AdminSeniorManagerApproveCompleted = "AdminSeniorManagerApproveCompleted";
        public static string Purchasing = "Purchasing";
        public static string Rejected = "Rejected";
        public static string Settled = "Settled";
    }
}
