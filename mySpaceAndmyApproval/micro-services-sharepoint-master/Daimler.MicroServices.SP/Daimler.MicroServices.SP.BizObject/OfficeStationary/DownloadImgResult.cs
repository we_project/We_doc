﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.OfficeStationary {
    public class DownloadImgResult {
        public StationaryMenuInfo menuInfo { get; set; }

        public bool success { get; set; }
    }
}
