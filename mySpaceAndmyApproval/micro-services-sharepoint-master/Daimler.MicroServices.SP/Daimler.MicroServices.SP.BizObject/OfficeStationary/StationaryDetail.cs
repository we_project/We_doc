﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.OfficeStationary {
    /// <summary>
    /// Stationary Menu or Item detail
    /// </summary>
    public class StationaryDetail {

        //public string Modified_x0020_By { get; set; }
        //public string Created_x0020_By { get; set; }
        //public string _SourceUrl { get; set; }
        //public string _SharedFileIndex { get; set; }
        //public string PreviewOnForm { get; set; }
        //public string FileType { get; set; }
        //public string ImageSize { get; set; }
        //public string Title { get; set; }
        //public int ImageWidth { get; set; }
        //public int ImageHeight { get; set; }
        //public DateTime ImageCreateDate { get; set; }
        //public string Description { get; set; }
        public bool ThumbnailExists { get; set; }
        //public bool PreviewExists { get; set; }
        //public string AlternateThumbnailUrl { get; set; }
        //public string Keywords { get; set; }
        public string ItemName { get; set; }
        //public string Model { get; set; }
        //public string Unit { get; set; }
        public double Price_x0028_with_x0020_Tax_x0029_ { get; set; }
        //public string B9_x0020_No_x002e_ { get; set; }
        //public string Comments { get; set; }
        //public string Category_x0020_Level_x0020_1 { get; set; }
        //public string Category_x0020_Level_x0020_2 { get; set; }
        //public string Category_x0020_Level_x0020_3 { get; set; }
        //public string CommodityNo { get; set; }
        public string ItemName_EN { get; set; }
        //public string Unit_EN { get; set; }
        public int ID { get; set; }
        //public string File_x0020_Size { get; set; }
        //public string CheckedOutUserId { get; set; }
        //public string IsCheckedoutToLocal { get; set; }
        //public string CheckoutUser { get; set; }
        //public string VirusStatus { get; set; }
        //public string CheckedOutTitle { get; set; }
        //public string _CheckinComment { get; set; }
        //public string LinkCheckedOutTitle { get; set; }
        //public string FileSizeDisplay { get; set; }
        //public string SelectFilename { get; set; }
        //public string ParentVersionString { get; set; }
        //public string ParentLeafName { get; set; }
        //public string DocConcurrencyNumber { get; set; }
        public string EncodedAbsThumbnailUrl { get; set; }
        public string EncodedAbsWebImgUrl { get; set; }
        //public string SelectedFlag { get; set; }
        //public string NameOrTitle { get; set; }
        //public string RequiredField { get; set; }
        //public string Thumbnail { get; set; }
        //public string Preview { get; set; }

    }
}
