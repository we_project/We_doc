﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.OfficeStationary {
    /// <summary>
    /// stationaryRequestInfo's Orders field, json object
    /// </summary>
    public class RequestOrder {
        //"id":"298","quantity":"8","reason":""

        public int id { get; set; }
        public int quantity { get; set; }
        public string reason { get; set; }
    }
}
