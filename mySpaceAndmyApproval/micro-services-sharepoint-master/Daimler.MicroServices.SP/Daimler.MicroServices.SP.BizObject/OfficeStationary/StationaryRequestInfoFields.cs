﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.OfficeStationary {
    public class StationaryRequestInfoFields {
        public static List<string> FieldsToLoad = new List<string>() {
        "UserName",
       "CostCenter",
       "Orders",
       "Status",
       "ExtensionNumber",
       "Company",
       "LocationCode",
       "Location",
       "FullAddress",
       "DetailAddress",
       "TotalAmount",
       "ID",
       "Created"
        };
        public static string ID = "ID";
        public static string CostCenterManagerPeople = "CostCenterManagerPeople";
        public static string AdminLocalManager = "AdminLocalManager";
        public static string AdminSeniorManager = "AdminSeniorManager";
        public static string Status = "Status";
        public static string ApproveOutCome = "ApproveOutCome";
        public static string Comments = "Comments";



        public static readonly string AdminLocalManagerId = "AdminLocalManagerId";
        public static readonly string CostCenterManagerPeopleId = "CostCenterManagerPeopleId";
        public static readonly string AdminSeniorManagerId = "AdminSeniorManagerId";
    }
}
