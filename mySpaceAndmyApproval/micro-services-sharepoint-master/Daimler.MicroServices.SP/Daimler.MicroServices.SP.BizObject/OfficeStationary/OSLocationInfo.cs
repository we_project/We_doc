﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.OfficeStationary {
    public class OSLocationInfo {
        public int ID { get; set; }
        /// <summary>
        /// Location
        /// </summary>
        public string Title { get; set; }
        public string Address_CN { get; set; }
        public string Address_EN { get; set; }
        public string Entity { get; set; }
        public string AdminContact { get; set; }
        public string AdminContactBackup { get; set; }

        public override string ToString() {
            StringBuilder sb = new StringBuilder();
            foreach (var prop in this.GetType().GetProperties()) {
                sb.AppendFormat("{0}: {1}\n", prop.Name, prop.GetValue(this));
            }
            return sb.ToString();
        }
    }
}
