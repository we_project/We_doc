﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Daimler.MicroServices.SP.BizObject.OfficeStationary {
    public class ReturnStationaryRequestList :BaseReturnSP{
        public List<StationaryRequestItem> list { get; set; }
    }
}