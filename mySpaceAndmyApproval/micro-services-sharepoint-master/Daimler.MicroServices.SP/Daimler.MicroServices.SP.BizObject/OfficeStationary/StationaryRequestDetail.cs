﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.OfficeStationary {
    /// <summary>
    /// request detail response to mobile
    /// </summary>
    public class StationaryRequestDetail : StationaryRequestItem {
        public string Company { get; set; }
        public string CostCenter { get; set; }
        public string ExtensionNumber { get; set; }
        public string LocationCode { get; set; }
        public string Location { get; set; }
        public string DetailAddress { get; set; }
        public string FullAddress { get; set; }
        public List<Order> Orders { get; set; }

    }
}
