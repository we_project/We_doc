﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Daimler.MicroServices.SP.BizObject.OfficeStationary {
    /// <summary>
    /// list item response to mobile
    /// </summary>
    public class StationaryRequestItem {
        public int ID { get; set; }
        public string UserName { get; set; }
        public string Status { get; set; }
        //public string Company{ get; set; }
        public double TotalAmount { get; set; }
        public DateTime CreatedTime { get; set; }
    }
}