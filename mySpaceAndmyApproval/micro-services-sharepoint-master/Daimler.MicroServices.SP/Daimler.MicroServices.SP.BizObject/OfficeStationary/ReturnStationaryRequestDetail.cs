﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Daimler.MicroServices.SP.BizObject.OfficeStationary {
    public class ReturnStationaryRequestDetail : BaseReturnSP {
        public StationaryRequestDetail detail { get; set; }
    }
}