﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Daimler.MicroServices.SP.BizObject {
    public class ApprovalPost {
        public int ID { get; set; }
        public bool approve { get; set; }
        public string comment { get; set; }
    }
}