﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Daimler.MicroServices.SP.BizObject.InvitationLetter;

namespace Daimler.MicroServices.SP.BizObject.InvitationLetter {
    public class ReturnInvitationLetterApplicationDetail:BaseReturnSP {
        public InvitationLetterApplicationDetail detail { get; set; }
    }
}