﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.InvitationLetter {
   public class InvitationLetterApplicationItem {
       //public string Modified_x0020_By { get; set; }
       //public string Created_x0020_By { get; set; }
       //public string _SourceUrl { get; set; }
       //public string _SharedFileIndex { get; set; }
       //public string Title { get; set; }
       //public string TemplateUrl { get; set; }
       //public string xd_ProgID { get; set; }
       //public bool? xd_Signature { get; set; }
       //public string ShowRepairView { get; set; }
       //public string ShowCombineView { get; set; }
       public string User_x0020_Name { get; set; }
       //public string Employee_x0020_Number { get; set; }
       //public string Cost_x0020_Center { get; set; }
       //public string Extension_x0020_Number { get; set; }
       public string Applicants_x0020_Company_x0020_Name { get; set; }
       //public string Applicants_x0020_Company_x0020_Address { get; set; }
       //public string JobTitle { get; set; }
       //public string Contact_x0020_in_x0020_Company { get; set; }
       //public string Entry_x0020_Port { get; set; }
       //public string Cost_x0020_Center_x0020_Manager { get; set; }
       //public string Form_x0020_Name { get; set; }
       //public string Last_x0020_Name { get; set; }
       //public string Nationality { get; set; }
       //public DateTime? Birth_x0020_Date { get; set; }
       //public string Passport { get; set; }
       //public DateTime? Entry_x0020_Date { get; set; }
       //public DateTime? Departure_x0020_Date { get; set; }
       //public string Purposeof_x0020_Visit { get; set; }
       //public string VISAType { get; set; }
       //public string Apply_x0020_Place { get; set; }
       //public string Contact_x0020_Phone { get; set; }
       //public string Relationship_x0020_With_x0020_Daimler { get; set; }
       //public string FirstName { get; set; }
       //public string Gender { get; set; }
       //public string Invitati { get; set; }
       //public string Stay_x0020_Days { get; set; }
       public string Invitation_x0020_Status { get; set; }
       //public string Form_x0020_Number { get; set; }
       //public string Company_x0020_Name { get; set; }
       //public string Finished_x0020_Print { get; set; }
       //public string Chinese_x0020_Visa_x0020_Type { get; set; }
       //public string CCManagerUser { get; set; }
       //public string DelegateUser { get; set; }
       public int ID { get; set; }
       public DateTime? Created { get; set; }
       public DateTime? Modified { get; set; }
       //public string File_x0020_Size { get; set; }
       //public string CheckedOutUserId { get; set; }
       //public string IsCheckedoutToLocal { get; set; }
       //public string CheckoutUser { get; set; }
       //public string VirusStatus { get; set; }
       //public string CheckedOutTitle { get; set; }
       //public string _CheckinComment { get; set; }
       //public string LinkCheckedOutTitle { get; set; }
       //public string FileSizeDisplay { get; set; }
       //public string SelectFilename { get; set; }
       //public string WorkflowInstanceID { get; set; }
       //public string ParentVersionString { get; set; }
       //public string ParentLeafName { get; set; }
       //public string DocConcurrencyNumber { get; set; }
       //public string Combine { get; set; }
       //public string RepairDocument { get; set; }
    }
}
