﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.PetrolCard {
    public class PCCreditAdjustmentInfoFields {
        public static List<string> FieldsToLoad = new List<string>() { 
            "Title",
"UserName",
"EmployeeNumber",
"CompanyName",
"CostCenter",
"RequestAmount",
"AdjustmentReason",
"ExtensionNumber",
"PlateNumber",
"CurrentCredit",
"Modified",
"Created",
"Status",
"PetrolCa",
"Comments",
"Supervisor",
"CardNo",
"DisplayName",
"ID",
"Attachments",
"WorkflowInstanceID"
        };
        public static string ID = "ID";
        public static string Status = "Status";
        public static string Comments = "Comments";
    }
}
