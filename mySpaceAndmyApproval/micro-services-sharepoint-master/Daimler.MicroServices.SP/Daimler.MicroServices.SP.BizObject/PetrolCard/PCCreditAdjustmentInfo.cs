﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.PetrolCard {
    public class PCCreditAdjustmentInfo {
        public int ID { get; set; }
        public string Title { get; set; }
        public string UserName { get; set; }
        public string EmployeeNumber { get; set; }
        public string CompanyName { get; set; }
        public string CostCenter { get; set; }
        public double? RequestAmount { get; set; }
        public string AdjustmentReason { get; set; }
        public string ExtensionNumber { get; set; }
        public string PlateNumber { get; set; }
        public double? CurrentCredit { get; set; }
        public string Status { get; set; }
        public string Comments { get; set; }
        public string Supervisor { get; set; }
        public string CardNo { get; set; }
        public string DisplayName { get; set; }
        public int? PetrolCa { get; set; }
        public DateTime Modified { get; set; }
        public DateTime Created { get; set; }

    }
}
