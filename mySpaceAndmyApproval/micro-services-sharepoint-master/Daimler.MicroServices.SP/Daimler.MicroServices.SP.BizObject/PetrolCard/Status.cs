﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.PetrolCard {
    public class Status {
        public static string Submitted = "Submitted";
        public static string Approved = "Approved";
        public static string Rejected = "Rejected";
    }
}
