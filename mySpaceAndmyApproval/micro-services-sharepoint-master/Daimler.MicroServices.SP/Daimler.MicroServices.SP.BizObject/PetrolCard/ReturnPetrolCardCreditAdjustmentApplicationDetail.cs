﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Daimler.MicroServices.SP.BizObject.PetrolCard;

namespace Daimler.MicroServices.SP.BizObject.PetrolCard {
    public class ReturnPetrolCardCreditAdjustmentApplicationDetail : BaseReturnSP {
        public PCCreditAdjustmentDetail detail { get; set; }

    }
}