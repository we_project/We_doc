﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Daimler.MicroServices.SP.BizObject.PetrolCard;

namespace Daimler.MicroServices.SP.BizObject.PetrolCard {
    public class ReturnPetrolCardCreditAdjustmentPendingList :BaseReturnSP{
        public List<PCCreditAdjustmentItem> list { get; set; }

    }
}