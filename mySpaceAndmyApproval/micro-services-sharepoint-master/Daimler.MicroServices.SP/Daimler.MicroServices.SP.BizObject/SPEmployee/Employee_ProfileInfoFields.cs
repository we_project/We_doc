﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.SPEmployee {
    public class Employee_ProfileInfoFields {
        public static List<string> FieldsToLoad = new List<string>(){
"Title",
"AD_x0020_Account",
"Employee_x0020_Number",
"Company_x0020_Name",
"Department",
"Cost_x0020_Center",
"Phone_x0020_No",
"Cost_x0020_Center_x0020_Manager",
"Cost_x0020_Center_x0020_Manager_",
"DomainAccount",
"Supervisor",
"SupervisorName",
"ID",
"Modified",
"Created"
         };
    }
}
