﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.SPEmployee {
    public class Employee_ProfileInfo {
        public int ID { get; set; }
        public string Title { get; set; }
        public string AD_x0020_Account { get; set; }
        public string Employee_x0020_Number { get; set; }
        public string Company_x0020_Name { get; set; }
        public string Department { get; set; }
        public string Cost_x0020_Center { get; set; }
        public string Phone_x0020_No { get; set; }
        public string Cost_x0020_Center_x0020_Manager { get; set; }
        public string Cost_x0020_Center_x0020_Manager_ { get; set; }
        public string DomainAccount { get; set; }
        public string Supervisor { get; set; }
        public string SupervisorName { get; set; }
        public DateTime Modified { get; set; }
        public DateTime Created { get; set; }
    }
}
