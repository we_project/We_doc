﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.AccessManagement {
    public enum ApproveFailedReason {
        NotApproverOrApprovedYet=0,
        NotSupportedWorkflow=1,
        Other=2,
    }
}
