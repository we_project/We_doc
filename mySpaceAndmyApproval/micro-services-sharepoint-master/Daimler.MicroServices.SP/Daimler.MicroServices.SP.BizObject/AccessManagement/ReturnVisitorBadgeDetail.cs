﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.AccessManagement;

namespace Daimler.MicroServices.SP.BizObject.AccessManagement {

    public class ReturnVisitorBadgeDetail : BaseReturnSP {
        public VisitorBadgeDetail1 detail { get; set; }
    }
}
