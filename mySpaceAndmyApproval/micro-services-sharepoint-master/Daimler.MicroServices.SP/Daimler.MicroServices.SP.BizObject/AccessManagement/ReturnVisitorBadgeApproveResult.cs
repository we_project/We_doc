﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.AccessManagement {
    public class ReturnVisitorBadgeApproveResult : BaseReturnSP {
        public VisitorBadgeApproveResult[] approveResults { get; set; }
    }
}
