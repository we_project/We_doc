﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.AccessManagement {
    public class EIBMInfoForList {
        public int ID { get; set; }
        public string Status { get; set; }
        public UserInfo CCApprover { get; set; }
        public UserInfo CCDeputy { get; set; }
        public DateTime? Application_x0020_Date { get; set; }
        public string Application_x0020_Type { get; set; }
        public UserInfo Employee_x0020_Name { get; set; }
    }
}
