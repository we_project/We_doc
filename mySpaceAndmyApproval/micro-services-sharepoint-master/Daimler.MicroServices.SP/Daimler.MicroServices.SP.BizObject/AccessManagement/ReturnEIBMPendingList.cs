﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.AccessManagement {
    public class ReturnEIBMPendingList :BaseReturnSP{
        public List<EIBMItem> list { get; set; }
    }
}
