﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.AccessManagement {
  public  class VisitorBadgeDetail1 {
      public int ID { get; set; }
      public string Status { get; set; }
      public UserInfo CCApprover { get; set; }
      public UserInfo CCDeputy { get; set; }
      public DateTime? Application_x0020_Date { get; set; }
      public string Application_x0020_Type { get; set; }
      public UserInfo Employee_x0020_Name { get; set; }
      public string Title { get; set; }
      public string Employee_x0020_Number { get; set; }
      public string Company { get; set; }
      public string Department { get; set; }
      public string Deskphone_x0020_Number { get; set; }
      public string Cost_x0020_Center { get; set; }
      public string Badge_x0020_Type { get; set; }
      public DateTime? Arrive_x0020_Date { get; set; }

      public DateTime? Leave_x0020_Date { get; set; }

      public string Reason_x0020_for_x0020_Visit { get; set; }
      public string Remarks { get; set; }
      public string NumOfLocation { get; set; }
      public SiteInfo Site_x0020_Name { get; set; }
      public LocationInfo Location_x0020_1 { get; set; }

      public AreaInfo[] Area_x0020_1 { get; set; }

      public LocationInfo Location_x0020_2 { get; set; }

      public AreaInfo[] Area_x0020_2 { get; set; }

      public LocationInfo Location_x0020_3 { get; set; }

      public AreaInfo[] Area_x0020_3 { get; set; }

      public LocationInfo Location_x0020_4 { get; set; }

      public AreaInfo[] Area_x0020_4 { get; set; }

      public LocationInfo Location_x0020_5 { get; set; }

      public AreaInfo[] Area_x0020_5 { get; set; }

      public LocationInfo Location_x0020_6 { get; set; }

      public AreaInfo[] Area_x0020_6 { get; set; }

      public LocationInfo Location_x0020_7 { get; set; }

      public AreaInfo[] Area_x0020_7 { get; set; }
      public int? SAApprover1Id { get; set; }

      public int? SAApprover2Id { get; set; }

      public int? SAApprover3Id { get; set; }

      public int? SAApprover4Id { get; set; }

      public int? SAApprover5Id { get; set; }

      public int? SAApprover6Id { get; set; }

      public int? SAApprover7Id { get; set; }
      public int? SADeputy1Id { get; set; }
      public int? SADeputy2Id { get; set; }

      public int? SADeputy3Id { get; set; }

      public int? SADeputy4Id { get; set; }

      public int? SADeputy5Id { get; set; }

      public int? SADeputy6Id { get; set; }

      public int? SADeputy7Id { get; set; }
      public string SAStatus1 { get; set; }
      public string SAStatus2 { get; set; }

      public string SAStatus3 { get; set; }

      public string SAStatus4 { get; set; }

      public string SAStatus5 { get; set; }

      public string SAStatus6 { get; set; }

      public string SAStatus7 { get; set; }

      public DateTime? Date_x0020_From_x0020_1 { get; set; }

      public DateTime? Date_x0020_From_x0020_2 { get; set; }

      public DateTime? Date_x0020_From_x0020_3 { get; set; }

      public DateTime? Date_x0020_From_x0020_4 { get; set; }

      public DateTime? Date_x0020_From_x0020_5 { get; set; }

      public DateTime? Date_x0020_From_x0020_6 { get; set; }

      public DateTime? Date_x0020_From_x0020_7 { get; set; }

      public DateTime? Date_x0020_To_x0020_1 { get; set; }

      public DateTime? Date_x0020_To_x0020_2 { get; set; }

      public DateTime? Date_x0020_To_x0020_3 { get; set; }

      public DateTime? Date_x0020_To_x0020_4 { get; set; }

      public DateTime? Date_x0020_To_x0020_5 { get; set; }

      public DateTime? Date_x0020_To_x0020_6 { get; set; }

      public DateTime? Date_x0020_To_x0020_7 { get; set; }
      public string NumOfVisitor { get; set; }
      public string First_x0020_Name_x0020_1 { get; set; }

      public string First_x0020_Name_x0020_2 { get; set; }

      public string First_x0020_Name_x0020_3 { get; set; }

      public string First_x0020_Name_x0020_4 { get; set; }

      public string First_x0020_Name_x0020_5 { get; set; }

      public string Last_x0020_Name_x0020_1 { get; set; }

      public string Last_x0020_Name_x0020_2 { get; set; }

      public string Last_x0020_Name_x0020_3 { get; set; }

      public string Last_x0020_Name_x0020_4 { get; set; }

      public string Last_x0020_Name_x0020_5 { get; set; }
      public string Visitor_x0020_Company_x0020_1 { get; set; }

      public string Visitor_x0020_Mobile_x0020_1 { get; set; }


      public string Visitor_x0020_Company_x0020_2 { get; set; }

      public string Visitor_x0020_Mobile_x0020_2 { get; set; }


      public string Visitor_x0020_Company_x0020_3 { get; set; }

      public string Visitor_x0020_Mobile_x0020_3 { get; set; }


      public string Visitor_x0020_Company_x0020_4 { get; set; }

      public string Visitor_x0020_Mobile_x0020_4 { get; set; }


      public string Visitor_x0020_Company_x0020_5 { get; set; }

      public string Visitor_x0020_Mobile_x0020_5 { get; set; }

  }
}
