﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.AccessManagement {

    public class VisitorBadgeApprovalPost {
        public int ID { get; set; }

        public VisitorBadgeApprovalInfo[] infoes { get; set; }

        public override string ToString() {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("ID:" + ID);
            if (infoes != null && infoes.Count() > 0) {
                foreach (var item in infoes) {
                    sb.AppendFormat("wf:{0}; comment:{1}", item.wf, item.comment);
                    sb.AppendLine();
                }
            }
            return sb.ToString();
        }
    }
}
