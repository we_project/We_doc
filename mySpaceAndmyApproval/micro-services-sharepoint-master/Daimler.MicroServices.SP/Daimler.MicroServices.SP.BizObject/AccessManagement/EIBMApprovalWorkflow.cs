﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.AccessManagement {
    /// <summary>
    /// please select an item to start approval action
    /// if you select *Approval, do not need to commit comment parameter,
    /// if you select *Rejection, must commit comment parameter
    /// </summary>
    public enum EIBMApprovalWorkflow {
        EIBMCCManagerApproval,
        EIBMCCManagerRejection,
        EIBMSpecialArea1Approval,
        EIBMSpecialArea1Rejection,
        EIBMSpecialArea2Approval,
        EIBMSpecialArea2Rejection,
        EIBMSpecialArea3Approval,
        EIBMSpecialArea3Rejection,
        EIBMSpecialArea4Approval,
        EIBMSpecialArea4Rejection,
        EIBMSpecialArea5Approval,
        EIBMSpecialArea5Rejection,
        EIBMSpecialArea6Approval,
        EIBMSpecialArea6Rejection,
        EIBMSpecialArea7Approval,
        EIBMSpecialArea7Rejection
    }
}
