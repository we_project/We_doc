﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.AccessManagement {

    /// <summary>
    /// please select an item to start approval action
    /// if you select *Approval, do not need to commit comment parameter,
    /// if you select *Rejection, must commit comment parameter
    /// </summary>
    public enum VisitorBadgeApprovalWorkflow {
        CCManagerApproval,
        CCManagerRejection,
        SpecialArea1Approval,
        SpecialArea1Rejection,
        SpecialArea2Approval,
        SpecialArea2Rejection,
        SpecialArea3Approval,
        SpecialArea3Rejection,
        SpecialArea4Approval,
        SpecialArea4Rejection,
        SpecialArea5Approval,
        SpecialArea5Rejection,
        SpecialArea6Approval,
        SpecialArea6Rejection,
        SpecialArea7Approval,
        SpecialArea7Rejection
    }
}
