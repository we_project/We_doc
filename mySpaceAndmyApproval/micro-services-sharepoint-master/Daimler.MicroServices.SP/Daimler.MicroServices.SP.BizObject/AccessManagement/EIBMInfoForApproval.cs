﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.AccessManagement {
    public class EIBMInfoForApproval {
        public int ID { get; set; }
        public string Status { get; set; }
        public int? CCApproverId { get; set; }
        public int? CCDeputyId { get; set; }
        public int? SAApprover1Id { get; set; }

        public int? SAApprover2Id { get; set; }

        public int? SAApprover3Id { get; set; }

        public int? SAApprover4Id { get; set; }

        public int? SAApprover5Id { get; set; }

        public int? SAApprover6Id { get; set; }

        public int? SAApprover7Id { get; set; }
        public int? SADeputy1Id { get; set; }
        public int? SADeputy2Id { get; set; }

        public int? SADeputy3Id { get; set; }

        public int? SADeputy4Id { get; set; }

        public int? SADeputy5Id { get; set; }

        public int? SADeputy6Id { get; set; }

        public int? SADeputy7Id { get; set; }
        public string SAStatus1 { get; set; }
        public string SAStatus2 { get; set; }

        public string SAStatus3 { get; set; }

        public string SAStatus4 { get; set; }

        public string SAStatus5 { get; set; }

        public string SAStatus6 { get; set; }

        public string SAStatus7 { get; set; }

    }
}
