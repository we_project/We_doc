﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.AccessManagement {
    public class EIBMBadgeApprovalInfo {
        public EIBMApprovalWorkflow wf { get; set; }
        public string comment { get; set; }
    }
}
