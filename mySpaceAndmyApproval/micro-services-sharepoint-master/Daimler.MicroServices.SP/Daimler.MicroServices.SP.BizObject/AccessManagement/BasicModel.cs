﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.AccessManagement {
    /// <summary>
    /// only contain ID and Title field
    /// </summary>
    public class BasicModel {
        public int? ID { get; set; }
        public string Title { get; set; }
    }
}
