﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Daimler.MicroServices.SP.BizObject.AccessManagement {
    /// <summary>
    /// Load eibm info to db
    /// </summary>
    public class EIBMInfo {

        public int ID { get; set; }
        public string Status { get; set; }
        public DateTime? Application_x0020_Date { get; set; }
        public string Application_x0020_Type { get; set; }
        public int? Employee_x0020_NameId { get; set; }
        public string Title { get; set; }
        public string Employee_x0020_Number { get; set; }
        public string Company { get; set; }
        public string Department { get; set; }
        public string Deskphone_x0020_Number { get; set; }
        public string Cost_x0020_Center { get; set; }
        public string Badge_x0020_Type { get; set; }
        public string Remarks { get; set; }
        public string NumOfLocation { get; set; }
        public int? Site_x0020_NameId { get; set; }
        public int? Location_x0020_1Id { get; set; }

        public string Area1Id { get; set; }

        public int? Location_x0020_2Id { get; set; }

        public string Area2Id { get; set; }

        public int? Location_x0020_3Id { get; set; }

        public string Area3Id { get; set; }

        public int? Location_x0020_4Id { get; set; }

        public string Area4Id { get; set; }

        public int? Location_x0020_5Id { get; set; }

        public string Area5Id { get; set; }

        public int? Location_x0020_6Id { get; set; }

        public string Area6Id { get; set; }

        public int? Location_x0020_7Id { get; set; }

        public string Area7Id { get; set; }
        public int? CCApproverId { get; set; }
        public int? CCDeputyId { get; set; }
        public int? SAApprover1Id { get; set; }

        public int? SAApprover2Id { get; set; }

        public int? SAApprover3Id { get; set; }

        public int? SAApprover4Id { get; set; }

        public int? SAApprover5Id { get; set; }

        public int? SAApprover6Id { get; set; }

        public int? SAApprover7Id { get; set; }
        public int? SADeputy1Id { get; set; }
        public int? SADeputy2Id { get; set; }

        public int? SADeputy3Id { get; set; }

        public int? SADeputy4Id { get; set; }

        public int? SADeputy5Id { get; set; }

        public int? SADeputy6Id { get; set; }

        public int? SADeputy7Id { get; set; }
        public string SAStatus1 { get; set; }
        public string SAStatus2 { get; set; }

        public string SAStatus3 { get; set; }

        public string SAStatus4 { get; set; }

        public string SAStatus5 { get; set; }

        public string SAStatus6 { get; set; }

        public string SAStatus7 { get; set; }

        public string Always { get; set; }

        public string Always2 { get; set; }

        public string Always3 { get; set; }

        public string Always4 { get; set; }

        public string Always5 { get; set; }

        public string Always6 { get; set; }

        public string Always7 { get; set; }
        public DateTime? Date_x0020_From_x0020_1 { get; set; }

        public DateTime? Date_x0020_From_x0020_2 { get; set; }

        public DateTime? Date_x0020_From_x0020_3 { get; set; }

        public DateTime? Date_x0020_From_x0020_4 { get; set; }

        public DateTime? Date_x0020_From_x0020_5 { get; set; }

        public DateTime? Date_x0020_From_x0020_6 { get; set; }

        public DateTime? Date_x0020_From_x0020_7 { get; set; }

        public DateTime? Date_x0020_To_x0020_1 { get; set; }

        public DateTime? Date_x0020_To_x0020_2 { get; set; }

        public DateTime? Date_x0020_To_x0020_3 { get; set; }

        public DateTime? Date_x0020_To_x0020_4 { get; set; }

        public DateTime? Date_x0020_To_x0020_5 { get; set; }

        public DateTime? Date_x0020_To_x0020_6 { get; set; }

        public DateTime? Date_x0020_To_x0020_7 { get; set; }

        public DateTime Modified { get; set; }

        public DateTime Created { get; set; }

        [JsonExtensionData]
        private IDictionary<string, JToken> _additionalData;

        [OnDeserialized]
        private void OnDeserialized(StreamingContext context) {
            // Area_x0020_1Id... is not deserialized to any property
            // and so it is added to the extension data dictionary
            if (_additionalData != null) {
                Area1Id = JsonConvert.SerializeObject(_additionalData["Area_x0020_1Id"]["results"]);
                Area2Id = JsonConvert.SerializeObject(_additionalData["Area_x0020_2Id"]["results"]);
                Area3Id = JsonConvert.SerializeObject(_additionalData["Area_x0020_3Id"]["results"]);
                Area4Id = JsonConvert.SerializeObject(_additionalData["Area_x0020_4Id"]["results"]);
                Area5Id = JsonConvert.SerializeObject(_additionalData["Area_x0020_5Id"]["results"]);
                Area6Id = JsonConvert.SerializeObject(_additionalData["Area_x0020_6Id"]["results"]);
                Area7Id = JsonConvert.SerializeObject(_additionalData["Area_x0020_7Id"]["results"]);
            }
        }
    }
}
