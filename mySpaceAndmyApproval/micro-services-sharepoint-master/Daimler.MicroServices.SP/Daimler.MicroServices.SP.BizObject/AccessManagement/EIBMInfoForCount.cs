﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.AccessManagement {
    /// <summary>
    /// simple model for EIBM get count
    /// </summary>
    public class EIBMInfoForCount {
        public string ID { get; set; }
        public string Status { get; set; }
        public UserInfo CCApprover { get; set; }
        public UserInfo CCDeputy { get; set; }
    }
}
