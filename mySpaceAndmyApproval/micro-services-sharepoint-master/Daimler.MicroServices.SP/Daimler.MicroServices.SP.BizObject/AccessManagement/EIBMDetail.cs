﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.AccessManagement {
    public class EIBMDetail {
        public int ID { get; set; }
        public string Status { get; set; }
        public UserInfo CCApprover { get; set; }
        public UserInfo CCDeputy { get; set; }
        public DateTime? Application_x0020_Date { get; set; }
        public string Application_x0020_Type { get; set; }
        public UserInfo Employee_x0020_Name { get; set; }
        public string Title { get; set; }
        public string Employee_x0020_Number { get; set; }
        public string Company { get; set; }
        public string Department { get; set; }
        public string Deskphone_x0020_Number { get; set; }
        public string Cost_x0020_Center { get; set; }
        public string Badge_x0020_Type { get; set; }
        public string Remarks { get; set; }
        public string NumOfLocation { get; set; }
        public SiteInfo Site_x0020_Name { get; set; }
        public LocationInfo Location_x0020_1 { get; set; }

        public AreaInfo[] Area_x0020_1 { get; set; }

        public LocationInfo Location_x0020_2 { get; set; }

        public AreaInfo[] Area_x0020_2 { get; set; }

        public LocationInfo Location_x0020_3 { get; set; }

        public AreaInfo[] Area_x0020_3 { get; set; }

        public LocationInfo Location_x0020_4 { get; set; }

        public AreaInfo[] Area_x0020_4 { get; set; }

        public LocationInfo Location_x0020_5 { get; set; }

        public AreaInfo[] Area_x0020_5 { get; set; }

        public LocationInfo Location_x0020_6 { get; set; }

        public AreaInfo[] Area_x0020_6 { get; set; }

        public LocationInfo Location_x0020_7 { get; set; }

        public AreaInfo[] Area_x0020_7 { get; set; }
        public UserInfo SAApprover1 { get; set; }

        public UserInfo SAApprover2 { get; set; }

        public UserInfo SAApprover3 { get; set; }

        public UserInfo SAApprover4 { get; set; }

        public UserInfo SAApprover5 { get; set; }

        public UserInfo SAApprover6 { get; set; }

        public UserInfo SAApprover7 { get; set; }
        public UserInfo SADeputy1 { get; set; }
        public UserInfo SADeputy2 { get; set; }

        public UserInfo SADeputy3 { get; set; }

        public UserInfo SADeputy4 { get; set; }

        public UserInfo SADeputy5 { get; set; }

        public UserInfo SADeputy6 { get; set; }

        public UserInfo SADeputy7 { get; set; }
        public string SAStatus1 { get; set; }
        public string SAStatus2 { get; set; }

        public string SAStatus3 { get; set; }

        public string SAStatus4 { get; set; }

        public string SAStatus5 { get; set; }

        public string SAStatus6 { get; set; }

        public string SAStatus7 { get; set; }

        public string Always { get; set; }

        public string Always2 { get; set; }

        public string Always3 { get; set; }

        public string Always4 { get; set; }

        public string Always5 { get; set; }

        public string Always6 { get; set; }

        public string Always7 { get; set; }
        public DateTime? Date_x0020_From_x0020_1 { get; set; }

        public DateTime? Date_x0020_From_x0020_2 { get; set; }

        public DateTime? Date_x0020_From_x0020_3 { get; set; }

        public DateTime? Date_x0020_From_x0020_4 { get; set; }

        public DateTime? Date_x0020_From_x0020_5 { get; set; }

        public DateTime? Date_x0020_From_x0020_6 { get; set; }

        public DateTime? Date_x0020_From_x0020_7 { get; set; }

        public DateTime? Date_x0020_To_x0020_1 { get; set; }

        public DateTime? Date_x0020_To_x0020_2 { get; set; }

        public DateTime? Date_x0020_To_x0020_3 { get; set; }

        public DateTime? Date_x0020_To_x0020_4 { get; set; }

        public DateTime? Date_x0020_To_x0020_5 { get; set; }

        public DateTime? Date_x0020_To_x0020_6 { get; set; }

        public DateTime? Date_x0020_To_x0020_7 { get; set; }


    }
}
