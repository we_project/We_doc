﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.AccessManagement {
    public class CostCenterDeputyProfileInfo {
        public int ID { get; set; }
        public string Title { get; set; }

        public int? Cost_x0020_Center_x0020_Manager { get; set; }

        public int? Cost_x0020_Center_x0020_Deputy { get; set; }

        public DateTime? Delegate_x0020_From_x0020_Date { get; set; }

        public DateTime? Delegate_x0020_To_x0020_Date { get; set; }

        public DateTime? Modified { get; set; }

        public DateTime? Created { get; set; }

    }
}
