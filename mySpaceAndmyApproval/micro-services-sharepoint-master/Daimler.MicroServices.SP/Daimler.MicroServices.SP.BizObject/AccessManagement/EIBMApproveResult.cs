﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.AccessManagement {
    public class EIBMApproveResult {
        public EIBMBadgeApprovalInfo info { get; set; }
        public bool success { get; set; }
        public ApproveFailedReason? msg { get; set; }
    }

}
