﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.AccessManagement {
    public class EIBMItem {
        public int Id { get; set; }

        public string EmployeeName { get; set; }
        public DateTime? ApplicationDate { get; set; }
        public string ApplicationType { get; set; }
        public string Status { get; set; }
    }
}
