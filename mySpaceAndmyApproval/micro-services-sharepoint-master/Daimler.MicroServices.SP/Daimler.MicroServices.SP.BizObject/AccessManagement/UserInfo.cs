﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.AccessManagement {
    public class UserInfo {
        public string Title { get; set; }
        public int? ID { get; set; }
    }
}
