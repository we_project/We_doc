﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject {
    public enum AllErrors {
        /// <summary>
        /// Can't get SM_USER from HTTP Header
        /// </summary>
        SM_USERIsNull,
        /// <summary>
        /// "Value cannot be null"
        /// </summary>
        InputCannotBeNull,
        /// <summary>
        /// "This is a fake action"
        /// </summary>
        FakeAction,

        #region Sharepoint
        /// <summary>
        /// sharepoint login failed
        /// </summary>
        SPLoginFailed,
        /// <summary>
        /// Have been approved
        /// </summary>
        HaveBeenApproved,
        /// <summary>
        /// You are not the approver
        /// </summary>
        NotApprover,


        #region NintexWorkflowError
        CannotObtainLock,
        InvalidUser,

        /// <summary>
        /// Get task id by workflowname-itemId-List-status failed
        /// </summary>
        GetTaskIDByItemFailed,
        #endregion

        #region FormAB
        /// <summary>
        /// Tax team need to input values
        /// </summary>
        TaxTeamInputIsNull,
        /// <summary>
        /// Status has changed
        /// </summary>
        StatusChanged,
        #endregion
        #endregion

        Others
    }
}
