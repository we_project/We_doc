﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject {
    public class SPLoginError {
        public static bool IsSPLoginError(Exception ex) {
            if (ex != null && ex.ToString().Contains("(401) Unauthorized")) {
                return true;
            }
            return false;
        }
    }
}
