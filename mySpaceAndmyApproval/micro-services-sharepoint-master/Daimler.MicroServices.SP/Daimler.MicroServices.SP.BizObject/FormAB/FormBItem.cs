﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.FormAB {
    public class FormBItem {
        public int ID { get; set; }
        public string Requestor { get; set; }
        public string Status { get; set; }
        public DateTime Modified { get; set; }
        public DateTime Created { get; set; }
        public string CaseID { get; set; }
    }
}
