﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.FormAB {
    public class FormADetail : FormAItem {
        //public string Title { get; set; }
        //public string FlowPath { get; set; }
        public string CostCenter { get; set; }
        //public string Tax { get; set; }
        //public string TaxManager { get; set; }
        //public string Controlling { get; set; }
        //public string ContactNumber { get; set; }
        //public string IONo { get; set; }
        //public string PRNo { get; set; }
        public string EventName { get; set; }
        public string TargetGroup { get; set; }
        public string SelectionCriteria { get; set; }
        public string ActivityDescription { get; set; }
        public string ActivityNature { get; set; }
        //public string OtherSelectionCriteria { get; set; }
        public string BenefitTo { get; set; }
        //public string BenefitAmount { get; set; }
        public string ExternalNumber { get; set; }
        public string EICost1 { get; set; }
        public string EICost2 { get; set; }
        public string EICost3 { get; set; }
        public string EECost1 { get; set; }
        public string EECost2 { get; set; }
        public string EECost3 { get; set; }
        public string InternalCost1 { get; set; }
        public string InternalCost2 { get; set; }
        public string InternalCost3 { get; set; }
        public string ExpenditureItem4 { get; set; }
        public string ExpenditureItem5 { get; set; }
        public string ExpenditureItem6 { get; set; }
        public string EICost4 { get; set; }
        public string EICost5 { get; set; }
        public string EICost6 { get; set; }
        public string EECost4 { get; set; }
        public string EECost5 { get; set; }
        public string EECost6 { get; set; }
        public string InternalCost4 { get; set; }
        public string InternalCost5 { get; set; }
        public string InternalCost6 { get; set; }
        //public string EstimatedIITAmount { get; set; }
        //public string EstimatedIITAmountOccasional { get; set; }
        //public string EstimatedIITAmountLabor { get; set; }
        //public string EstimatedOutputAmount { get; set; }
        //public string EstimatedInputAmount { get; set; }
        //public string Note { get; set; }
        public string StatusIndex { get; set; }
        //public string _x0054_ax2 { get; set; }
        //public string Controlling2 { get; set; }
        //public string NewComment { get; set; }
        //public string TeamGroup { get; set; }
        //public string LegalEntity { get; set; }
        //public string FormBCount { get; set; }
        //public bool IsStatusChange { get; set; }
        //public string FormA { get; set; }
        //public string CalculationMethod { get; set; }
        //public string VATRate { get; set; }
        //public string CurrentUser { get; set; }
        //public string FormAComments { get; set; }
        //public string Year { get; set; }
        //public string IDIndex { get; set; }
        //public bool IsReject { get; set; }
        //public string AddFormB { get; set; }
        //public string Pending { get; set; }
        //public string Pending2 { get; set; }
        //public string CostCenterCode { get; set; }
        //public string FormBAmount { get; set; }
        //public string SurplusBudget { get; set; }
        //public string EICost1_Submit { get; set; }
        //public string EICost2_Submit { get; set; }
        //public string EICost3_Submit { get; set; }
        //public string EICost4_Submit { get; set; }
        //public string EICost5_Submit { get; set; }
        //public string EICost6_Submit { get; set; }
        //public string EECost1_Submit { get; set; }
        //public string EECost2_Submit { get; set; }
        //public string EECost3_Submit { get; set; }
        //public string EECost4_Submit { get; set; }
        //public string EECost5_Submit { get; set; }
        //public string EECost6_Submit { get; set; }
        //public string InternalCost1_Submit { get; set; }
        //public string InternalCost2_Submit { get; set; }
        //public string InternalCost3_Submit { get; set; }
        //public string InternalCost4_Submit { get; set; }
        //public string InternalCost5_Submit { get; set; }
        //public string InternalCost6_Submit { get; set; }
        public List<FormAComment> CommentHistory { get; set; }
    }
}
