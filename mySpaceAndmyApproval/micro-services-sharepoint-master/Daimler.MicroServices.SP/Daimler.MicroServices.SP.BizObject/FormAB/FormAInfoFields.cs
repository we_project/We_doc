﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.FormAB {
    public class FormAInfoFields {
        public static List<string> FieldsToLoad = new List<string>() { 
            "Title",
"CaseID",
"Status",
"FlowPath",
"Requestor",
"CostCenter",
"Tax",
"TaxManager",
"Controlling",
"ContactNumber",
"IONo",
"PRNo",
"EventName",
"TargetGroup",
"SelectionCriteria",
"ActivityDescription",
"ActivityNature",
"OtherSelectionCriteria",
"BenefitTo",
"BenefitAmount",
"ExternalNumber",
"EICost1",
"EICost2",
"EICost3",
"EECost1",
"EECost2",
"EECost3",
"InternalCost1",
"InternalCost2",
"InternalCost3",
"ExpenditureItem4",
"ExpenditureItem5",
"ExpenditureItem6",
"EICost4",
"EICost5",
"EICost6",
"EECost4",
"EECost5",
"EECost6",
"InternalCost4",
"InternalCost5",
"InternalCost6",
"EstimatedIITAmount",
"EstimatedIITAmountOccasional",
"EstimatedIITAmountLabor",
"EstimatedOutputAmount",
"EstimatedInputAmount",
"Note",
"StatusIndex",
"_x0054_ax2",
"Controlling2",
"NewComment",
"TeamGroup",
"LegalEntity",
"FormBCount",
"IsStatusChange",
"FormA",
"CalculationMethod",
"VATRate",
"CurrentUser",
"FormAComments",
"Year",
"IDIndex",
"IsReject",
"AddFormB",
"Pending",
"Pending2",
"CostCenterCode",
"FormBAmount",
"SurplusBudget",
"EICost1_Submit",
"EICost2_Submit",
"EICost3_Submit",
"EICost4_Submit",
"EICost5_Submit",
"EICost6_Submit",
"EECost1_Submit",
"EECost2_Submit",
"EECost3_Submit",
"EECost4_Submit",
"EECost5_Submit",
"EECost6_Submit",
"InternalCost1_Submit",
"InternalCost2_Submit",
"InternalCost3_Submit",
"InternalCost4_Submit",
"InternalCost5_Submit",
"InternalCost6_Submit",
"ID",
"Modified",
"Created"
        };

        public static string StatusIndex = "StatusIndex";
        public static string ID = "ID";
        public static string IsStatusChange = "IsStatusChange";
        public static string Pending = "Pending";
        public static string Pending2 = "Pending2";
        public static string Tax = "Tax";
        public static string _x0054_ax2 = "_x0054_ax2";
        public static string TaxManager = "TaxManager";
        public static string Controlling = "Controlling";
        public static string Controlling2 = "Controlling2";
        public static string Status = "Status";
        public static string FlowPath = "FlowPath";
        public static string IsReject = "IsReject";
        public static string CurrentUser = "CurrentUser";
        public static string NewComment = "NewComment";

        public static string IONo = "IONo";

        #region TaxTeam
        public class TaxTeamFields {
            public static string CalculationMethod = "CalculationMethod";
            public static string VATRate = "VATRate";
            public static string EstimatedIITAmountOccasional = "EstimatedIITAmountOccasional";
            public static string EstimatedIITAmountLabor = "EstimatedIITAmountLabor";
            public static string EstimatedOutputAmount = "EstimatedOutputAmount";
            public static string EstimatedInputAmount = "EstimatedInputAmount";
            public static string Note = "Note";
        }
        #endregion
    }
}
