﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.FormAB {
    public class FormBTaxTeamFields {
        public string CalculationMethod { get; set; }
        public string VATRate { get; set; }
        public string ActualIITAmountOccasional { get; set; }
        public string ActualIITAmountLabor { get; set; }
        public string ActualOutputAmount { get; set; }
        public string ActualInputAmount { get; set; }
        public string Note { get; set; }
    }
}
