﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.FormAB {
    public  class FormBWorkflowCollection {
        static List<FormABWorkflow> workflows = new List<FormABWorkflow>{
                                           new FormABWorkflow(0, "Draft", 1, 0),
                                           new FormABWorkflow(1, "Submitted", 2, 0),
                                           new FormABWorkflow(2, "Cost center manager signed", 3, 0),
                                           new FormABWorkflow(3, "Tax team confirmed", 4, 2),
                                           new FormABWorkflow(4, "Complete",4, 4),
                                       };
        static FormBWorkflowCollection _default = new FormBWorkflowCollection();
        private FormBWorkflowCollection() {

        }
        public static FormBWorkflowCollection Default {
            get {
                return _default;
            }
        }
        public FormABWorkflow this[int index] {
            get {
                if (index < workflows.Count) {
                    return workflows[index];
                }
                throw new IndexOutOfRangeException("can not find this status");
            }
        }
    }
}
