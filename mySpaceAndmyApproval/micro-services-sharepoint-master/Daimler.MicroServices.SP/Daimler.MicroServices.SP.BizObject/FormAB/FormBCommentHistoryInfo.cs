﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.FormAB {
    public class FormBCommentHistoryInfo {
        public int ID { get; set; }
        public string Title { get; set; }

        public string CaseID { get; set; }

        public string User { get; set; }

        public string Comment { get; set; }


        public DateTime? Modified { get; set; }

        public DateTime? Created { get; set; }
    }

    public class FormBCommentHistoryInfoForDB {
        public FormBCommentHistoryInfoForDB(FormBCommentHistoryInfo info) {
            this.ID = info.ID;
            this.Title = info.Title;
            this.CaseID = info.CaseID;
            this.Comment = info.Comment;
            this.UserStr = info.User;
            this.Modified = info.Modified;
            this.Created = info.Created;
        }
        public int ID { get; set; }
        public string Title { get; set; }
        public string CaseID { get; set; }
        public string UserStr { get; set; }
        public string Comment { get; set; }
        public DateTime? Modified { get; set; }
        public DateTime? Created { get; set; }

    }
}
