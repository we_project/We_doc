﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.FormAB {
    public class FormACommentHistoryInfoFields {
        public static List<string> FieldsToLoad = new List<string>() { 
            "Title",
            "CaseID",
            "Comment",
            "User",
            "ID",
            "Modified",
            "Created"
        };
    }
}
