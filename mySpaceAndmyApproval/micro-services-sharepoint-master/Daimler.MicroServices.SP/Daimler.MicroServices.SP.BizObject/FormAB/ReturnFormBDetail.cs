﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Daimler.MicroServices.SP.BizObject.FormAB;

namespace Daimler.MicroServices.SP.BizObject.FormAB {
    public class ReturnFormBDetail : BaseReturnSP {
        public FormBDetail detail { get; set; }
    }
}