﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.FormAB {
    public class FormATaxTeamFields {
        public string CalculationMethod { get; set; }
        public string VATRate { get; set; }
        public string EstimatedIITAmountOccasional { get; set; }
        public string EstimatedIITAmountLabor { get; set; }
        public string EstimatedOutputAmount { get; set; }
        public string EstimatedInputAmount { get; set; }
        public string Note { get; set; }
    }
}
