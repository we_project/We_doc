﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.FormAB {
   public  class FormBInfoFields {
        public static List<string> FieldsToLoad = new List<string>() {
            "Title",
"CaseID",
"Status",
"FlowPath",
"Requestor",
"CostCenter",
"Tax",
"_x0054_ax2",
"TaxManager",
"Controlling",
"Controlling2",
"BenefitTo",
"ExternalNumber",
"IsEntitled",
"RecipientsNumber",
"TotalCost",
"EIUnitCost1",
"EIUnitCost2",
"EIUnitCost3",
"EIUnitCost4",
"EIUnitCost5",
"EIUnitCost6",
"EIVolume1",
"EIVolume2",
"EIVolume3",
"EIVolume4",
"EIVolume5",
"EIVolume6",
"EITotalCost1",
"EITotalCost2",
"EITotalCost3",
"EITotalCost4",
"EITotalCost5",
"EITotalCost6",
"EEUnitCost1",
"EEUnitCost2",
"EEUnitCost3",
"EEUnitCost4",
"EEUnitCost5",
"EEUnitCost6",
"EEVolume1",
"EEVolume2",
"EEVolume3",
"EEVolume4",
"EEVolume5",
"EEVolume6",
"EETotalCost1",
"EETotalCost2",
"EETotalCost3",
"EETotalCost4",
"EETotalCost5",
"EETotalCost6",
"InternalUnitCost1",
"InternalUnitCost2",
"InternalUnitCost3",
"InternalUnitCost4",
"InternalUnitCost5",
"InternalUnitCost6",
"InternalVolume1",
"InternalVolume2",
"InternalVolume3",
"InternalVolume4",
"InternalVolume5",
"InternalVolume6",
"InternalTotalCost1",
"InternalTotalCost2",
"InternalTotalCost3",
"InternalTotalCost4",
"InternalTotalCost5",
"InternalTotalCost6",
"ExpenditureItem4",
"ExpenditureItem5",
"ExpenditureItem6",
"CalculationMethod",
"ActualIITAmount",
"ActualIITAmountOccasional",
"ActualIITAmountLabor",
"ActualOutputAmount",
"ActualInputAmount",
"NewComment",
"CurrentUser",
"IsStatusChange",
"StatusIndex",
"VATRate",
"FormACaseID",
"FormB",
"IsReject",
"FormBComments",
"FormBComments_x0028_1_x0029_",
"Note",
"Pending",
"Pending2",
"CostCenterCode",
"FormASysID",
"IIT",
"CompletionDate",
"ID",
"Modified",
"Created"
        };
        public static string StatusIndex = "StatusIndex";
        public static string ID = "ID";
        public static string IsStatusChange = "IsStatusChange";
        public static string Pending = "Pending";
        public static string Pending2 = "Pending2";
        public static string Tax = "Tax";
        public static string _x0054_ax2 = "_x0054_ax2";
        public static string TaxManager = "TaxManager";
        public static string Controlling = "Controlling";
        public static string Controlling2 = "Controlling2";
        public static string Status = "Status";
        public static string FlowPath = "FlowPath";
        public static string IsReject = "IsReject";
        public static string CurrentUser = "CurrentUser";
        public static string NewComment = "NewComment";

        public static string IONo = "IONo";

        #region TaxTeam
        public class TaxTeamFields {
            public static string CalculationMethod = "CalculationMethod";
            public static string VATRate = "VATRate";
            public static string ActualIITAmountOccasional = "ActualIITAmountOccasional";
            public static string ActualIITAmountLabor = "ActualIITAmountLabor";
            public static string ActualOutputAmount = "ActualOutputAmount";
            public static string ActualInputAmount = "ActualInputAmount";
            public static string Note = "Note";
        }
        #endregion
    }
}
