﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Daimler.MicroServices.SP.BizObject.FormAB;

namespace Daimler.MicroServices.SP.BizObject.FormAB {
    public class FormBApprovalPost : ApprovalPost {
        /// <summary>
        /// post as the same value which returned from detail interface
        /// </summary>
        public string StatusIndex { get; set; }

        /// <summary>
        /// Reserved, useless now
        /// </summary>
        public FormBTaxTeamFields taxTeamFields { get; set; }
    }
}