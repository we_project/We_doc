﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.FormAB {
    public class FormABWorkflow {
        public FormABWorkflow(int index, string displayName, int approvedTo, int rejectTo) {
            this.index = index;
            this.displayName = displayName;
            this.approvedTo = approvedTo;
            this.rejectTo = rejectTo;
        }
        public int index { get; set; }
        public string displayName { get; set; }
        public int approvedTo { get; set; }
        public int rejectTo { get; set; }
        public override string ToString() {

            return "FormABWorkflow:" + string.Join("|", index, displayName, approvedTo, rejectTo);
        }

        public string NewFlowPath(string oldFlowPath) {
            return oldFlowPath + DateTime.Now.ToString("(yyyy-MM-dd hh:mm)") + " >> " + displayName;
        }
    }
}
