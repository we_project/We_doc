﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.FormAB {
    public  class FormBDetail :FormBItem{
        //public string Title { get; set; }
        //public string FlowPath { get; set; }
        public string CostCenter { get; set; }
        //public string Tax { get; set; }
        //public string _x0054_ax2 { get; set; }
        //public string TaxManager { get; set; }
        //public string Controlling { get; set; }
        //public string Controlling2 { get; set; }
        public string BenefitTo { get; set; }
        //public string ExternalNumber { get; set; }
        //public bool IsEntitled { get; set; }
        //public string RecipientsNumber { get; set; }
        //public string TotalCost { get; set; }
        public string EIUnitCost1 { get; set; }
        public string EIUnitCost2 { get; set; }
        public string EIUnitCost3 { get; set; }
        public string EIUnitCost4 { get; set; }
        public string EIUnitCost5 { get; set; }
        public string EIUnitCost6 { get; set; }
        public string EIVolume1 { get; set; }
        public string EIVolume2 { get; set; }
        public string EIVolume3 { get; set; }
        public string EIVolume4 { get; set; }
        public string EIVolume5 { get; set; }
        public string EIVolume6 { get; set; }
        public string EITotalCost1 { get; set; }
        public string EITotalCost2 { get; set; }
        public string EITotalCost3 { get; set; }
        public string EITotalCost4 { get; set; }
        public string EITotalCost5 { get; set; }
        public string EITotalCost6 { get; set; }
        public string EEUnitCost1 { get; set; }
        public string EEUnitCost2 { get; set; }
        public string EEUnitCost3 { get; set; }
        public string EEUnitCost4 { get; set; }
        public string EEUnitCost5 { get; set; }
        public string EEUnitCost6 { get; set; }
        public string EEVolume1 { get; set; }
        public string EEVolume2 { get; set; }
        public string EEVolume3 { get; set; }
        public string EEVolume4 { get; set; }
        public string EEVolume5 { get; set; }
        public string EEVolume6 { get; set; }
        public string EETotalCost1 { get; set; }
        public string EETotalCost2 { get; set; }
        public string EETotalCost3 { get; set; }
        public string EETotalCost4 { get; set; }
        public string EETotalCost5 { get; set; }
        public string EETotalCost6 { get; set; }
        public string InternalUnitCost1 { get; set; }
        public string InternalUnitCost2 { get; set; }
        public string InternalUnitCost3 { get; set; }
        public string InternalUnitCost4 { get; set; }
        public string InternalUnitCost5 { get; set; }
        public string InternalUnitCost6 { get; set; }
        public string InternalVolume1 { get; set; }
        public string InternalVolume2 { get; set; }
        public string InternalVolume3 { get; set; }
        public string InternalVolume4 { get; set; }
        public string InternalVolume5 { get; set; }
        public string InternalVolume6 { get; set; }
        public string InternalTotalCost1 { get; set; }
        public string InternalTotalCost2 { get; set; }
        public string InternalTotalCost3 { get; set; }
        public string InternalTotalCost4 { get; set; }
        public string InternalTotalCost5 { get; set; }
        public string InternalTotalCost6 { get; set; }
        public string ExpenditureItem4 { get; set; }
        public string ExpenditureItem5 { get; set; }
        public string ExpenditureItem6 { get; set; }
        //public string CalculationMethod { get; set; }
        //public string ActualIITAmount { get; set; }
        //public string ActualIITAmountOccasional { get; set; }
        //public string ActualIITAmountLabor { get; set; }
        //public string ActualOutputAmount { get; set; }
        //public string ActualInputAmount { get; set; }
        //public string NewComment { get; set; }
        //public string CurrentUser { get; set; }
        //public bool IsStatusChange { get; set; }
        public string StatusIndex { get; set; }
        //public string VATRate { get; set; }
        //public string FormACaseID { get; set; }
        //public string FormB { get; set; }
        //public bool IsReject { get; set; }
        //public string FormBComments { get; set; }
        //public string FormBComments_x0028_1_x0029_ { get; set; }
        //public string Note { get; set; }
        //public string Pending { get; set; }
        //public string Pending2 { get; set; }
        //public string CostCenterCode { get; set; }
        //public string FormASysID { get; set; }
        //public string IIT { get; set; }
        //public DateTime CompletionDate { get; set; }
        
        public List<FormBComment> CommentHistory { get; set; }
      
    }
}
