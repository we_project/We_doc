﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject {
    public class updateBase {
        public metadata __metadata { get; set; }
    }

    public class metadata {
        public string type { get; set; }
        public static string GetItemTypeForListName(string name) {
            return "SP.Data." + char.ToUpper(name[0]) + name.Substring(1) + "ListItem";
        }
    }


}
