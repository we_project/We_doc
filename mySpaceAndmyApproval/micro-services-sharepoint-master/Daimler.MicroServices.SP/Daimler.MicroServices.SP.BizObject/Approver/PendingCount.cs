﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.Approver {
    public class PendingCount {
        public int Stationary { get; set; }
        public int BussinessCard { get; set; }
        public int InvitationLetter { get; set; }
        public int PetrolCardCreditAdjustment{ get; set; }
        public int FormA { get; set; }
        public int FormB { get; set; }
        ///// <summary>
        ///// Employee/Intern Badge Management
        ///// </summary>
        //public int EIBM { get; set; }
        //public int VisitorBadgeApplication { get; set; }
    }
}
