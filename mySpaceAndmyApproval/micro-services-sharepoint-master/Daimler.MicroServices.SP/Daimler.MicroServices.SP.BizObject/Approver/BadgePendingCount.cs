﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.Approver {
    public class BadgePendingCount {
        /// <summary>
        /// Employee/Intern Badge Management
        /// </summary>
        public int EIBM { get; set; }
        public int VisitorBadgeApplication { get; set; }
    }
}
