﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.Approver {
    public class ApproveResult {
        public bool Success { get; set; }
        public AllErrors Msg { get; set; }
    }
}
