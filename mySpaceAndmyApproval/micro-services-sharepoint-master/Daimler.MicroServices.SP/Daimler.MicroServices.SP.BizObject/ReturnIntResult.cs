﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject {
    
    public class ReturnIntResult : BaseReturnSP {
        public int? intResult { get; set; }
    }
}
