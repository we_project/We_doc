﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.Workflow {
    public class WorkflowTaskInfo {
        public int ID { get; set; }
        //public string Title { get; set; }
        //public string Predecessors { get; set; }
        //public string Priority { get; set; }
        public string Status { get; set; }
        //public double PercentComplete { get; set; }
        public int AssignedTo { get; set; }
        //public string TaskGroup { get; set; }
        //public string Body { get; set; }
        //public DateTime StartDate { get; set; }
        //public DateTime DueDate { get; set; }
        //public string RelatedItems { get; set; }
        //public string WorkflowLink { get; set; }
        //public string OffsiteParticipant { get; set; }
        //public string OffsiteParticipantReason { get; set; }
        public string WorkflowOutcome { get; set; }
        public string WorkflowName { get; set; }
        //public int TaskType { get; set; }
        //public string FormURN { get; set; }
        //public string FormData { get; set; }
        //public string EmailBody { get; set; }
        //public bool HasCustomEmailBody { get; set; }
        //public bool SendEmailNotification { get; set; }
        //public DateTime PendingModTime { get; set; }
        //public bool Completed { get; set; }
        //public string WorkflowListId { get; set; }
        public int WorkflowItemId { get; set; }
        //public string ExtendedProperties { get; set; }
        public int ApprovalOutcome { get; set; }
        public int Decision { get; set; }
        public string ApproverComments { get; set; }
        //public int HumanWorkflowID { get; set; }
        //public int ApproverTaskID { get; set; }
        //public string MultiOutcomeTaskInfo { get; set; }
        //public string NWFormUrl { get; set; }
        //public int DatabaseID { get; set; }
        //public DateTime Modified { get; set; }
        public DateTime Created { get; set; }
    }
}
