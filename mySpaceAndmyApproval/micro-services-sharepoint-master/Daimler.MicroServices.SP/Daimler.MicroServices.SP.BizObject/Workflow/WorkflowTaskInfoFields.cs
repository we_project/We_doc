﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.Workflow {
    /// <summary>
    /// todo: remove useless fields
    /// </summary>
    public class WorkflowTaskInfoFields {
        public static List<string> FieldsToLoad = new List<string>() {
            
//"Title",
//"Predecessors",
//"Priority",
"Status",
"PercentComplete",
"AssignedTo",
//"TaskGroup",
//"Body",
"StartDate",
"DueDate",
//"RelatedItems",
//"WorkflowLink",
//"OffsiteParticipant",
//"OffsiteParticipantReason",
"WorkflowOutcome",
"WorkflowName",
"TaskType",
//"FormURN",
//"FormData",
//"EmailBody",
//"HasCustomEmailBody",
//"SendEmailNotification",
//"PendingModTime",
"Completed",
"WorkflowListId",
"WorkflowItemId",
//"ExtendedProperties",
"ApprovalOutcome",
"Decision",
"ApproverComments",
//"HumanWorkflowID",
//"ApproverTaskID",
//"MultiOutcomeTaskInfo",
//"NWFormUrl",
//"DatabaseID",
"ID",
//"Modified",
//"Created"
        };

        public static string ID = "ID";
        public static string WorkflowItemId = "WorkflowItemId";
        public static string WorkflowName = "WorkflowName";
        public static List<string> SnapshotFields = new List<string> {
            ID,
            WorkflowItemId,
            WorkflowName
        };
    }
}
