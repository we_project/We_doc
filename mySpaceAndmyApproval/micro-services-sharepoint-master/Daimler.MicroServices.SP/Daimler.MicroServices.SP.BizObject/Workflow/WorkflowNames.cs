﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.Workflow {
    public class WorkflowNames {
        public static string BusinessCardApproveProcess = "BusinessCardApproveProcess";
        public static string Invitation = "Invitation";
    }
}
