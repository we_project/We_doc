﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.Workflow {
    public class Status {
        public static string NotStarted = "Not Started";
        public static string InProgress = "In Progress";
        public static string Completed = "Completed";
        public static string Deferred = "Deferred";
        public static string Waitingonsomeoneelse = "Waiting on someone else";
    }
}
