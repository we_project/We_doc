﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject.Workflow {
    public class WorkflowTaskSnapshot {
        public string WorkflowName { get; set; }
        public int ID { get; set; }
        public int WorkflowItemId { get; set; }

    }
}
