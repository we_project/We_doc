﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.BizObject {
   public class BaseReturnSP {
       public bool success { get; set; }
       public ReturnMsgSP? msg { get; set; }
       public static BaseReturnSP Exception {
           get {
               return new BaseReturnSP {
                   success = false,
                   msg = ReturnMsgSP.ExceptionOccurs
               };
           }
       }
       public static BaseReturnSP SPLoginFailed {
           get {
               return new BaseReturnSP {
                   success = false,
                   msg = ReturnMsgSP.SPLoginFailed
               };
           }
       }
    }
}
