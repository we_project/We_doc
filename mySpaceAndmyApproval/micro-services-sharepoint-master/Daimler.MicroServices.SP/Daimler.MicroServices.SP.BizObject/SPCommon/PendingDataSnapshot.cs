﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.Approver;
using Daimler.MicroServices.SP.BizObject.SPUser;
using Daimler.MicroServices.SP.BizObject.Workflow;

namespace Daimler.MicroServices.SP.BizObject.SPCommon {
    public class PendingDataSnapshot {
        public PendingDataSnapshot(string smUser, User spCommonUser, User spFormABUser) {
            this.smUser = smUser;
            this.spCommonUser = spCommonUser;
            this.spFormABUser = spFormABUser;
        }
        public string smUser { get; set; }
        public User spCommonUser { get; set; }
        public User spFormABUser { get; set; }
        public List<int> stationary { get; set; }
        public List<WorkflowTaskSnapshot> bcIl { get; set; }
        public List<WorkflowTaskSnapshot> bc { get; set; }
        public List<WorkflowTaskSnapshot> il { get; set; }
        public List<int> pertolcard { get; set; }
        public List<int> formA { get; set; }
        public List<int> formB { get; set; }


        public PendingCount GetCount() {
            PendingCount count = new PendingCount();
            count.Stationary = stationary != null ? stationary.Count : 0;
            count.BussinessCard = bc != null ? bc.Count : 0;
            count.InvitationLetter = il != null ? il.Count : 0;
            count.PetrolCardCreditAdjustment = pertolcard != null ? pertolcard.Count : 0;
            count.FormA = formA != null ? formA.Count : 0;
            count.FormB = formB != null ? formB.Count : 0;
            return count;
        }
    }
}
