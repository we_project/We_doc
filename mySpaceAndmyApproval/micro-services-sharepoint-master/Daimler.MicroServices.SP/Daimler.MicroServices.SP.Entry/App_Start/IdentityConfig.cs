﻿using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Daimler.MicroServices.SP.Entry.Models;
using Daimler.MicroServices.SP.BizObject;
using System;

namespace Daimler.MicroServices.SP.Entry {
    // Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.


    public class ApplicationUserManager : UserManager<ApplicationUser> {
        public ApplicationUserManager(IUserStore<ApplicationUser> store)
            : base(store) {
        }

        public ApplicationUser Get(UserCredential user) {
            ApplicationUser result = null;
            if (user.Login()) {
                result = new ApplicationUser() {
                    UserName = user.userName,
                    SecurityStamp = Guid.NewGuid().ToString()
                };
                result.UserCredential = user;
            }
            return result;
        }

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context) {
            var manager = new ApplicationUserManager(new CustomUserStore());
            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null) {
                manager.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser>(dataProtectionProvider.Create("ASP.NET Identity"));
            }
            return manager;
        }

    }
    public class CustomUserStore : IUserStore<ApplicationUser> {



        public Task CreateAsync(ApplicationUser user) {
            throw new System.NotImplementedException();
        }

        public Task DeleteAsync(ApplicationUser user) {
            throw new System.NotImplementedException();
        }

        public Task<ApplicationUser> FindByIdAsync(string userId) {
            throw new System.NotImplementedException();
        }

        public Task<ApplicationUser> FindByNameAsync(string userName) {
            throw new System.NotImplementedException();
        }

        public Task UpdateAsync(ApplicationUser user) {
            throw new System.NotImplementedException();
        }

        public void Dispose() {
            //throw new System.NotImplementedException();
        }
    }
}
