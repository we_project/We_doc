using System;
using System.Reflection;

namespace Daimler.MicroServices.SP.Entry.Areas.HelpPage.ModelDescriptions
{
    public interface IModelDocumentationProvider
    {
        string GetDocumentation(MemberInfo member);

        string GetDocumentation(Type type);
    }
}