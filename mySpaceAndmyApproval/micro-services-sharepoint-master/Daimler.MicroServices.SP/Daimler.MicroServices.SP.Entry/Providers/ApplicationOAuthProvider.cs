﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Daimler.MicroServices.SP.Entry.Models;
using Daimler.MicroServices.SP.BizObject;
using log4net;

namespace Daimler.MicroServices.SP.Entry.Providers {
    public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider {
        private static readonly ILog log = LogManager.GetLogger(typeof(ApplicationOAuthProvider));
        private readonly string _publicClientId;

        public ApplicationOAuthProvider(string publicClientId) {
            if (publicClientId == null) {
                throw new ArgumentNullException("publicClientId");
            }

            _publicClientId = publicClientId;
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context) {
            string domain = context.OwinContext.Get<string>(DomainKey);
            string username = context.UserName;
            string psw = context.Password;
            var c = Daimler.MicroServices.SP.Service.SPConfig.Instance;
            UserCredential userCredential = new UserCredential {
                domain = domain,
                userName = username,
                psw = psw
            };
            ApplicationUserManager userManager = context.OwinContext.GetUserManager<ApplicationUserManager>();
            ApplicationUser user = null;
            log.DebugFormat("{0}\\{1} trys to login", domain, username);
            try {
                user = userManager.Get(userCredential);
                log.DebugFormat("{0}\\{1} login successfully", domain, username);
            } catch (Exception ex) {
                string msg = "The user name or password is incorrect.";
                log.Error("LoginFailed", ex);
                AllErrors error = ExceptionClassifier.GetErrors(ex);
                if (!error.Equals(AllErrors.SPLoginFailed)) {
                    msg = "other error";
                }
                context.SetError("invalid_grant", msg);
                return;
            }
            ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(userManager,
               OAuthDefaults.AuthenticationType);
            //ClaimsIdentity cookiesIdentity = await user.GenerateUserIdentityAsync(userManager,
            //    CookieAuthenticationDefaults.AuthenticationType);

            AuthenticationProperties properties = CreateProperties(user.UserName);
            AuthenticationTicket ticket = new AuthenticationTicket(oAuthIdentity, properties);
            context.Validated(ticket);
            //context.Request.Context.Authentication.SignIn(cookiesIdentity);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context) {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary) {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }
        static string DomainKey = "domain";
        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context) {
            string clientId = null;
            string secret = null;
            if (!context.TryGetBasicCredentials(out clientId, out secret)) {
                context.TryGetFormCredentials(out clientId, out secret);
            }
            string domain = context.Parameters.Where(p => p.Key.Equals(DomainKey)).Select(f => f.Value).SingleOrDefault()[0];
            context.OwinContext.Set<string>(DomainKey, domain);
            // Resource owner password credentials does not provide a client ID.
            if (context.ClientId == null) {
                context.Validated();
            }

            return Task.FromResult<object>(null);
        }

        public override Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context) {
            if (context.ClientId == _publicClientId) {
                Uri expectedRootUri = new Uri(context.Request.Uri, "/");

                if (expectedRootUri.AbsoluteUri == context.RedirectUri) {
                    context.Validated();
                }
            }

            return Task.FromResult<object>(null);
        }

        public static AuthenticationProperties CreateProperties(string userName) {
            IDictionary<string, string> data = new Dictionary<string, string>
            {
                { "userName", userName }
            };
            return new AuthenticationProperties(data);
        }
    }
}