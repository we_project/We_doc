﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ValueProviders;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.Approver;
using Daimler.MicroServices.SP.Entry.Models;
using Daimler.MicroServices.SP.Entry.Providers;
using Daimler.MicroServices.SP.BizObject.FormAB;
using Daimler.MicroServices.SP.BLL;
using System.Threading.Tasks;
using log4net;

namespace Daimler.MicroServices.SP.Entry.Controllers {
    [Authorize]
    [RoutePrefix("api/FormB")]
    public class FormBController : BaseApiController {
        private static readonly ILog log = LogManager.GetLogger(typeof(FormBController));
        [HttpGet]
        [Route("GetPendingList")]
        public async Task<ReturnFormBPendingList> GetApplicationList(int? pageIndex = null, int? pageSize = null) {
            ReturnFormBPendingList result = new ReturnFormBPendingList();
            try {
                FormBService service = new FormBService(UserCredential);
                result.list = await service.GetFormBList(pageIndex, pageSize);
                result.success = true;
            } catch (Exception ex) {
                log.Error("" + Request.RequestUri + UserCredential, ex);
                result.msg = ExceptionClassifier.GetFrom(ex);
            }
            return result;
        }

        [HttpGet]
        [Route("GetFormBDetail")]
        public async Task<ReturnFormBDetail> GetRequestDetail(int ID) {
            ReturnFormBDetail data = new ReturnFormBDetail();

            try {
                FormBService service = new FormBService(UserCredential);
                data.detail = await service.GetDetail(ID);
                data.success = true;
            } catch (Exception ex) {
                log.Error("" + Request.RequestUri + UserCredential + ",ID:" + ID, ex);
                data.msg = ExceptionClassifier.GetFrom(ex);
            }
            return data;
        }


        /// <summary>
        /// approve or reject requets with comment
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Approval")]
        public async Task<BaseReturnSP> Approval(FormBApprovalPost post) {
            BaseReturnSP result = new BaseReturnSP();

            try {
                if (post == null) {
                    result.success = false;
                    result.msg = ExceptionClassifier.GetFrom(AllErrors.InputCannotBeNull);
                    return result;
                }
                if (Config.UpdateToSP) {
                    FormBService service = new FormBService(UserCredential);
                    ApproveResult approveResult = await service.Approval(post.approve, post.ID, post.comment, post.StatusIndex, post.taxTeamFields);
                    result.success = approveResult.Success;
                    result.msg = ExceptionClassifier.GetFrom(approveResult.Msg);
                } else {
                    result.success = true;
                    result.msg = ExceptionClassifier.GetFrom(AllErrors.FakeAction);
                }
            } catch (Exception ex) {
                log.Error("" + Request.RequestUri + UserCredential + string.Join("_", post.ID, post.approve, post.comment), ex);
                result.msg = ExceptionClassifier.GetFrom(ex);
            }
            return result;
        }
    }
}
