﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ValueProviders;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.Approver;
using Daimler.MicroServices.SP.Entry.Models;
using Daimler.MicroServices.SP.Entry.Providers;
using Daimler.MicroServices.SP.BizObject.BusinessCard;
using Daimler.MicroServices.SP.BLL;
using log4net;
using System.Threading.Tasks;
namespace Daimler.MicroServices.SP.Entry.Controllers {
    [Authorize]
    [RoutePrefix("api/BusinessCard")]
    public class BusinessCardController : BaseApiController {

        private static readonly ILog log = LogManager.GetLogger(typeof(BusinessCardController));
        [HttpGet]
        [Route("GetApplicationList")]
        public async Task<ReturnBusinessCardPendingList> GetApplicationList(int? pageIndex = null, int? pageSize = null) {
            ReturnBusinessCardPendingList result = new ReturnBusinessCardPendingList();
            try {
                BusinessCardService service = new BusinessCardService(UserCredential);
                result.list = await service.GetBCApplicatoinList(pageIndex, pageSize);
                result.success = true;
            } catch (Exception ex) {
                log.Error("" + Request.RequestUri + UserCredential, ex);
                result.msg = ExceptionClassifier.GetFrom(ex);
            }
            return result;
        }

        [HttpGet]
        [Route("GetApplicationDetail")]
        public async Task<ReturnBusinessCardApplicationDetail> GetRequestDetail(int ID) {
            ReturnBusinessCardApplicationDetail data = new ReturnBusinessCardApplicationDetail();
            try {
                BusinessCardService service = new BusinessCardService(UserCredential);
                data.detail = await service.GetBCApplicatoinDetail(ID);
                data.success = true;
            } catch (Exception ex) {
                log.Error("" + Request.RequestUri + UserCredential + ",ID:" + ID, ex);
                data.msg = ExceptionClassifier.GetFrom(ex);
            }
            return data;
        }


        /// <summary>
        /// approve or reject requets with comment
        /// </summary>
        /// <param name="post"></param>
        /// <param name="SM_USER"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Approval")]
        public async Task<BaseReturnSP> Approval(ApprovalPost post) {
            BaseReturnSP result = new BaseReturnSP();

            try {
                if (post == null) {
                    result.success = false;
                    result.msg = ExceptionClassifier.GetFrom(AllErrors.InputCannotBeNull);
                    return result;
                }
                if (Config.UpdateToSP) {
                    BusinessCardService service = new BusinessCardService(UserCredential);
                    ApproveResult approveResult = await service.Approval(post.approve, post.ID, post.comment);
                    result.success = approveResult.Success;
                    if (!result.success) {
                        result.msg = ExceptionClassifier.GetFrom(approveResult.Msg);
                    }
                } else {
                    result.success = true;
                    result.msg = ExceptionClassifier.GetFrom(AllErrors.FakeAction);
                }
            } catch (Exception ex) {
                log.Error("" + Request.RequestUri + UserCredential + string.Join("_", post.ID, post.approve, post.comment), ex);
                result.msg = ExceptionClassifier.GetFrom(ex);
            }
            return result;
        }
    }
}
