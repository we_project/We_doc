﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.Entry.Models;
using log4net;

namespace Daimler.MicroServices.SP.Entry.Controllers {
    public class BaseApiController : ApiController {
        private static readonly ILog log = LogManager.GetLogger(typeof(BaseApiController));
        UserCredential uc = null;
        public UserCredential UserCredential {
            get {
                try {
                    if (uc == null) {
                        var identity = User.Identity as ClaimsIdentity;

                        Claim domainClaim = null;
                        Claim userNameClaim = null;
                        Claim pswClaim = null;
                        foreach (var claim in identity.Claims) {
                            if (UserCredential.Fields.domain.Equals(claim.Type)) {
                                domainClaim = claim;
                            } else if (UserCredential.Fields.userName.Equals(claim.Type)) {
                                userNameClaim = claim;
                            } else if (UserCredential.Fields.psw.Equals(claim.Type)) {
                                pswClaim = claim;
                            }
                        }

                        if (domainClaim != null && userNameClaim != null && pswClaim != null) {
                            uc = new UserCredential {
                                domain = domainClaim.Value,
                                psw = pswClaim.Value,
                                userName = userNameClaim.Value
                            };
                        }
                    }
                } catch (Exception ex) {
                    log.Error("UserCredential", ex);
                }
                return uc;
            }
        }


    }
}
