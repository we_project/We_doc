﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ValueProviders;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.Approver;
using Daimler.MicroServices.SP.BizObject.PetrolCard;
using Daimler.MicroServices.SP.BLL;
using Daimler.MicroServices.SP.Entry.Models;
using Daimler.MicroServices.SP.Entry.Providers;
using log4net;

namespace Daimler.MicroServices.SP.Entry.Controllers {
    [Authorize]
    [RoutePrefix("api/PetrolCardCreditAdjustment")]
    public class PetrolCardCreditAdjustmentController : BaseApiController {

        private static readonly ILog log = LogManager.GetLogger(typeof(PetrolCardCreditAdjustmentController));
        [HttpGet]
        [Route("GetApplicationList")]
        public async Task<ReturnPetrolCardCreditAdjustmentPendingList> GetApplicationList(int? pageIndex = null, int? pageSize = null) {
            ReturnPetrolCardCreditAdjustmentPendingList result = new ReturnPetrolCardCreditAdjustmentPendingList();
            try {
                PetrolCardService service = new PetrolCardService(UserCredential);
                result.list = await service.GetPCCAList(pageIndex, pageSize);
                result.success = true;
            } catch (Exception ex) {
                log.Error("" + Request.RequestUri + UserCredential, ex);
                result.msg = ExceptionClassifier.GetFrom(ex);
            }
            return result;
        }

        [HttpGet]
        [Route("GetApplicationDetail")]
        public async Task<ReturnPetrolCardCreditAdjustmentApplicationDetail> GetRequestDetail(int ID) {
            ReturnPetrolCardCreditAdjustmentApplicationDetail data = new ReturnPetrolCardCreditAdjustmentApplicationDetail();

            try {
                PetrolCardService service = new PetrolCardService(UserCredential);
                data.detail = await service.GetPCCADetail(ID);
                data.success = true;
            } catch (Exception ex) {
                log.Error("" + Request.RequestUri + UserCredential + ",ID:" + ID, ex);
                data.msg = ExceptionClassifier.GetFrom(ex);
            }
            return data;
        }


        /// <summary>
        /// approve or reject requets with comment
        /// </summary>
        /// <param name="post"></param>
        /// <param name="SM_USER"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Approval")]
        public async Task<BaseReturnSP> Approval(ApprovalPost post) {
            BaseReturnSP result = new BaseReturnSP();

            try {
                if (post == null) {
                    result.success = false;
                    result.msg = ExceptionClassifier.GetFrom(AllErrors.InputCannotBeNull);
                    return result;
                }
                if (Config.UpdateToSP) {
                    PetrolCardService service = new PetrolCardService(UserCredential);
                    ApproveResult approveResult = await service.Approval(post.approve, post.ID, post.comment);
                    result.success = approveResult.Success;
                    result.msg = ExceptionClassifier.GetFrom(approveResult.Msg); ;
                } else {
                    result.success = true;
                    result.msg = ExceptionClassifier.GetFrom(AllErrors.FakeAction);
                }
            } catch (Exception ex) {
                log.Error("" + Request.RequestUri + UserCredential + string.Join("_", post.ID, post.approve, post.comment), ex);
                result.msg = ExceptionClassifier.GetFrom(ex);
            }
            return result;
        }
    }
}
