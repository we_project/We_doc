﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BLL;
using Daimler.MicroServices.SP.Entry.Models;
using Daimler.MicroServices.SP.Service.AccessManagement;
using log4net;

namespace Daimler.MicroServices.SP.Entry.Controllers {
    [Authorize]
    [RoutePrefix("api/Badge")]
    public class BadgeController : BaseApiController {
        private static readonly ILog log = LogManager.GetLogger(typeof(BadgeController));

        /// <summary>
        /// get user id for Access Management System
        /// this id is used to display Badge approval button, for more detail, please refer to api/EIBMBadge/Approval
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("CurrentUserId")]
        public async Task<ReturnIntResult> GetCurrentUserId() {
            ReturnIntResult data = new ReturnIntResult();
            try {
                UserService service = new UserService(UserCredential);
                data.intResult = await service.GetBadgeUserIdAsync();
                data.success = true;
            } catch (Exception ex) {
                log.Error(Request.RequestUri, ex);
                data.msg = ExceptionClassifier.GetFrom(ex);
            }
            return data;
        }


    }
}
