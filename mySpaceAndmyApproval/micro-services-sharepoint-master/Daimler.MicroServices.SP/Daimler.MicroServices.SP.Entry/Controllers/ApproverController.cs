﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ValueProviders;
using Daimler.ApproverHome.WebApi.Entry.Models.Approver;
using Daimler.MicroServices.SP.BLL;
using Daimler.MicroServices.SP.Entry.Models;
using Daimler.MicroServices.SP.Entry.Models.Approver;
using Daimler.MicroServices.SP.Entry.Providers;
using log4net;

namespace Daimler.MicroServices.SP.Entry.Controllers {
    /// <summary>
    /// general interface 
    /// </summary>
    [Authorize]
    [RoutePrefix("api/Approver")]
    public class ApproverController : BaseApiController {
        private static readonly ILog log = LogManager.GetLogger(typeof(ApproverController));

        /// <summary>
        /// get pending count by user
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetPendingCount")]
        public async Task<ReturnPendingCount> GetPendingCount() {
            ReturnPendingCount data = new ReturnPendingCount();
            try {
                ApproverService service = new ApproverService(UserCredential);
                data.pendingCount = await service.GetPendingCountSnapshot();
                data.success = true;
            } catch (Exception ex) {
                log.Error(Request.RequestUri, ex);
                data.msg = ExceptionClassifier.GetFrom(ex);
            }
            return data;
        }


        /// <summary>
        /// get pending count by user
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetBadgePendingCount")]
        public async Task<ReturnBadgePendingCount> GetBadgePendingCount() {
            ReturnBadgePendingCount data = new ReturnBadgePendingCount();
            try {
                ApproverService service = new ApproverService(UserCredential);
                data.pendingCount = await service.GetBadgePendingCountSnapshot();
                data.success = true;
            } catch (Exception ex) {
                log.Error(Request.RequestUri, ex);
                data.msg = ExceptionClassifier.GetFrom(ex);
            }
            return data;
        }

    }
}
