﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.AccessManagement;
using Daimler.MicroServices.SP.BLL;
using Daimler.MicroServices.SP.Entry.Models;
using Daimler.MicroServices.SP.Service.AccessManagement;
using log4net;
using SP.Data;

namespace Daimler.MicroServices.SP.Entry.Controllers {
    /// <summary>
    /// 'Employee/Intern Badge Management' badge service
    /// </summary>
    [Authorize]
    [RoutePrefix("api/EIBMBadge")]
    public class EIBMBadgeController : BaseApiController {
        private static readonly ILog log = LogManager.GetLogger(typeof(EIBMBadgeController));

        /// <summary>
        /// get pending approval list
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("PendingList")]
        public async Task<ReturnEIBMPendingList> GetPendingList() {
            ReturnEIBMPendingList result = new ReturnEIBMPendingList();
            try {
                EIBMService service = new EIBMService(UserCredential);
                var r = await service.GetPendingEIBMList();
                result.list = r != null ? r.ToList() : null;
                result.success = true;
            } catch (Exception ex) {
                log.Error("" + Request.RequestUri + UserCredential, ex);
                result.msg = ExceptionClassifier.GetFrom(ex);
            }
            return result;
        }

        /// <summary>
        /// get pending count by user
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("{Id:int}/Detail")]
        public async Task<ReturnEIBMDetail> GetDetail(int Id) {
            ReturnEIBMDetail data = new ReturnEIBMDetail();
            try {
                //EIBMRestful spEIBM = new EIBMRestful(UserCredential);
                //data.detail = await spEIBM.GetItem(Id);
                EIBMService spEIBM = new EIBMService(UserCredential);
                data.detail = await spEIBM.GeEIBMDetail(Id);
                data.success = true;
            } catch (Exception ex) {
                log.Error(Request.RequestUri, ex);
                data.msg = ExceptionClassifier.GetFrom(ex);
            }
            return data;
        }

        /// <summary>
        /// approve or reject requets with comment
        /// 
        ///  cc manager pending approval
        ///       the item CCStatus is 'Pending Approval' (after this the workflow send email )
        ///       the item Status is 'Pending Cost Center Manager Approval'
        ///       current user is cc manager or (current user is cc deputy and is in delegate date, for frontend you don't need to check delegate date, it's filtered by backend)
        /// 
        ///  special area pending approval
        ///       the item Status is 'Pending Special Area Approval'
        ///       current user is SAApprover1|SADeputy1 and SAStatus1 is 'Pending Approval'
        ///       current user is SAApprover2|SADeputy2 and SAStatus2 is 'Pending Approval'
        ///       current user is SAApprover3|SADeputy3 and SAStatus3 is 'Pending Approval'
        ///       current user is SAApprover4|SADeputy4 and SAStatus4 is 'Pending Approval'
        ///       current user is SAApprover5|SADeputy5 and SAStatus5 is 'Pending Approval'
        ///       current user is SAApprover6|SADeputy6 and SAStatus6 is 'Pending Approval'
        ///       current user is SAApprover7|SADeputy7 and SAStatus7 is 'Pending Approval'
        ///       
        /// to get current user id please refer to interface 
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Approval")]
        public async Task<ReturnEIBMApproveResult> Approval(EIBMBadgeApprovalPost post) {
            ReturnEIBMApproveResult result = new ReturnEIBMApproveResult();

            try {
                if (post == null) {
                    result.success = false;
                    result.msg = ExceptionClassifier.GetFrom(AllErrors.InputCannotBeNull);
                    return result;
                }
                if (Config.UpdateToSP) {
                    EIBMService spEIBM = new EIBMService(UserCredential);
                    EIBMApproveResult[] r = await spEIBM.BatchApprovalUseRestful(post);
                    result.approveResults = r;
                    if (r != null && r.Count() > 0 && r.Any(s => s.success == false)) {
                        result.success = false;
                    } else {
                        result.success = true;
                    }
                } else {
                    result.success = true;
                    result.msg = ExceptionClassifier.GetFrom(AllErrors.FakeAction);
                }
            } catch (Exception ex) {
                log.Error("" + Request.RequestUri + UserCredential + (post != null ? post.ToString() : ""), ex);
                result.msg = ExceptionClassifier.GetFrom(ex);
            }
            return result;
        }

    }
}
