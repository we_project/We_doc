﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ValueProviders;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.Approver;
using Daimler.MicroServices.SP.BizObject.OfficeStationary;
using Daimler.MicroServices.SP.BLL;
using Daimler.MicroServices.SP.Entry.Models;
using Daimler.MicroServices.SP.Entry.Providers;
using log4net;

namespace Daimler.MicroServices.SP.Entry.Controllers {
    /// <summary>
    /// all interfaces for stationary
    /// </summary>
    [Authorize]
    [RoutePrefix("api/Stationary")]
    public class StationaryController : BaseApiController {
        private static readonly ILog log = LogManager.GetLogger(typeof(StationaryController));
        /// <summary>
        /// get stationary request list
        /// </summary>
        /// <param name="pageIndex">page index</param>
        /// <param name="pageSize">page size</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetRequestList")]
        public async Task<ReturnStationaryRequestList> GetRequestList(int? pageIndex = null, int? pageSize = null) {
            ReturnStationaryRequestList data = new ReturnStationaryRequestList();
            try {
                OfficeStationaryService StationaryService = new OfficeStationaryService(UserCredential);
                data.list = await StationaryService.GetStationaryList(pageIndex, pageSize);
                data.success = true;
            } catch (Exception ex) {
                log.Error(Request.RequestUri, ex);
                data.msg = ExceptionClassifier.GetFrom(ex);
            }
            return data;
        }

        /// <summary>
        /// get stationary request detail by ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetRequestDetail")]
        public async Task<ReturnStationaryRequestDetail> GetRequestDetail(int ID) {
            ReturnStationaryRequestDetail data = new ReturnStationaryRequestDetail();

            try {
                OfficeStationaryService StationaryService = new OfficeStationaryService(UserCredential);
                data.detail = await StationaryService.GetStationaryRequestDetail(ID);
                data.success = true;
            } catch (Exception ex) {
                log.Error(Request.RequestUri, ex);
                data.msg = ExceptionClassifier.GetFrom(ex);
            }
            return data;
        }

        /// <summary>
        /// approve or reject requets with comment
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Approval")]
        public async Task<BaseReturnSP> Approval(ApprovalPost post) {
            BaseReturnSP result = new BaseReturnSP();
            try {
                if (post == null) {
                    result.success = false;
                    result.msg = ExceptionClassifier.GetFrom(AllErrors.InputCannotBeNull);
                    return result;
                }
                if (Config.UpdateToSP) {
                    OfficeStationaryService StationaryService = new OfficeStationaryService(UserCredential);
                    ApproveResult approveResult = await StationaryService.Approval(post.approve, post.ID, post.comment);
                    result.success = approveResult.Success;
                    if (!result.success) {
                        result.msg = ExceptionClassifier.GetFrom(approveResult.Msg);
                    }
                } else {
                    result.success = true;
                    result.msg = ExceptionClassifier.GetFrom(AllErrors.FakeAction);
                }
            } catch (Exception ex) {
                log.Error(Request.RequestUri + string.Join("_", post.ID, post.approve, post.comment), ex);
                result.msg = ExceptionClassifier.GetFrom(ex);
            }
            return result;
        }
    }
}
