﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ValueProviders;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.Approver;
using Daimler.MicroServices.SP.Entry.Models;
using Daimler.MicroServices.SP.Entry.Providers;
using Daimler.MicroServices.SP.BizObject.FormAB;
using Daimler.MicroServices.SP.BLL;
using log4net;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.Entry.Controllers {
    [Authorize]
    [RoutePrefix("api/FormA")]
    public class FormAController : BaseApiController {
        private static readonly ILog log = LogManager.GetLogger(typeof(FormAController));
        [HttpGet]
        [Route("GetPendingList")]
        public async Task<ReturnFormAPendingList> GetApplicationList(int? pageIndex=null, int? pageSize = null) {
            ReturnFormAPendingList result = new ReturnFormAPendingList();
            try {
                FormAService service = new FormAService(UserCredential);
                result.list = await service.GetFormAList(pageIndex, pageSize);
                result.success = true;
            } catch (Exception ex) {
                log.Error(""+Request.RequestUri+UserCredential, ex);
                result.msg = ExceptionClassifier.GetFrom(ex);
            }
            return result;
        }

        [HttpGet]
        [Route("GetFormADetail")]
        public async Task<ReturnFormADetail> GetRequestDetail(int ID) {
            ReturnFormADetail data = new ReturnFormADetail();

            try {
                FormAService service = new FormAService(UserCredential);
                data.detail = await service.GetFormADetail(ID);
                data.success = true;
            } catch (Exception ex) {
                log.Error("" + Request.RequestUri + UserCredential + ",ID:" + ID, ex);
                data.msg = ExceptionClassifier.GetFrom(ex);
            }
            return data;
        }


        /// <summary>
        /// approve or reject requets with comment
        /// </summary>
        /// <param name="post"></param>
        /// <param name="SM_USER"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Approval")]
        public async Task<BaseReturnSP> Approval(FormAApprovalPost post) {
            BaseReturnSP result = new BaseReturnSP();

            try {
                if (post == null) {
                    result.success = false;
                    result.msg = ExceptionClassifier.GetFrom(AllErrors.InputCannotBeNull);
                    return result;
                }
                if (Config.UpdateToSP) {
                    FormAService service = new FormAService(UserCredential);
                    ApproveResult approveResult = await service.Approval(post.approve, post.ID, post.comment, post.StatusIndex, post.taxTeamFields);
                    result.success = approveResult.Success;
                    result.msg = ExceptionClassifier.GetFrom(approveResult.Msg);
                } else {
                    result.success = true;
                    result.msg = ExceptionClassifier.GetFrom(AllErrors.FakeAction);
                }
            } catch (Exception ex) {
                log.Error("" + Request.RequestUri + UserCredential + string.Join("_", post.ID, post.approve, post.comment), ex);
                result.msg = ExceptionClassifier.GetFrom(ex);
            }
            return result;
        }
    }
}
