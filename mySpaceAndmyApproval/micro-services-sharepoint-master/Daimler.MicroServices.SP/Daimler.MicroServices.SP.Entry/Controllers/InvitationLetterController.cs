﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ValueProviders;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.Approver;
using Daimler.MicroServices.SP.BizObject.InvitationLetter;
using Daimler.MicroServices.SP.BLL;
using Daimler.MicroServices.SP.Entry.Models;
using Daimler.MicroServices.SP.Entry.Providers;
using log4net;

namespace Daimler.MicroServices.SP.Entry.Controllers {
    [Authorize]
    [RoutePrefix("api/InvitationLetter")]
    public class InvitationLetterController : BaseApiController {
        private static readonly ILog log = LogManager.GetLogger(typeof(InvitationLetterController));

        [HttpGet]
        [Route("GetApplicationList")]
        public async Task<ReturnInvitationLetterPendingList> GetApplicationList(int? pageIndex = null, int? pageSize = null) {
            ReturnInvitationLetterPendingList result = new ReturnInvitationLetterPendingList();
            try {
                InvitationLetterService service = new InvitationLetterService(UserCredential);
                result.list = await
                service.GetILApplicatoinList(pageIndex, pageSize);
                result.success = true;
            } catch (Exception ex) {
                log.Error("" + Request.RequestUri + UserCredential, ex);
                result.msg = ExceptionClassifier.GetFrom(ex);
            }
            return result;
        }

        [HttpGet]
        [Route("GetApplicationDetail")]
        public async Task<ReturnInvitationLetterApplicationDetail> GetRequestDetail(int ID) {
            ReturnInvitationLetterApplicationDetail data = new ReturnInvitationLetterApplicationDetail();

            try {
                InvitationLetterService service = new InvitationLetterService(UserCredential);
                data.detail = await service.GetILApplicatoinDetail(ID);
                data.success = true;
            } catch (Exception ex) {
                log.Error("" + Request.RequestUri + UserCredential + ",ID:" + ID, ex);
                data.msg = ExceptionClassifier.GetFrom(ex);
            }
            return data;
        }


        /// <summary>
        /// approve or reject requets with comment
        /// </summary>
        /// <param name="post"></param>
        /// <param name="SM_USER"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Approval")]
        public async Task<BaseReturnSP> Approval(ApprovalPost post) {
            BaseReturnSP result = new BaseReturnSP();

            try {
                if (post == null) {
                    result.success = false;
                    result.msg = ExceptionClassifier.GetFrom(AllErrors.InputCannotBeNull);
                    return result;
                }
                if (Config.UpdateToSP) {
                    //result.success = service.Approval(SM_USER, post.approve, post.ID, post.comment);
                    InvitationLetterService service = new InvitationLetterService(UserCredential);
                    ApproveResult approveResult = await service.Approval(post.approve, post.ID, post.comment);
                    result.success = approveResult.Success;
                    result.msg = ExceptionClassifier.GetFrom(approveResult.Msg);
                } else {
                    result.success = true;
                    result.msg = ExceptionClassifier.GetFrom(AllErrors.FakeAction);
                }
            } catch (Exception ex) {
                log.Error("" + Request.RequestUri + UserCredential + string.Join("_", post.ID, post.approve, post.comment), ex);
                result.msg = ExceptionClassifier.GetFrom(ex);
            }
            return result;
        }
    }
}
