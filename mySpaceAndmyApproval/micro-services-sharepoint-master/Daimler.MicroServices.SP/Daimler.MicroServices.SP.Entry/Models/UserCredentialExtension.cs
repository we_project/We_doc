﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.Service.Common;

namespace Daimler.MicroServices.SP.Entry.Models {
    public static class UserCredentialExtension {
        public static bool Login(this UserCredential user) {
            bool result = false;
            ICommon service = new CommonImpl(user);
            result = service.Login();
            return result;
        }
    }
}