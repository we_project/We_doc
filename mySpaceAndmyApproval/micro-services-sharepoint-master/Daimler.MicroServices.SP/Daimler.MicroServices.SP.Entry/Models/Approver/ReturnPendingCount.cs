﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.Approver;

namespace Daimler.ApproverHome.WebApi.Entry.Models.Approver {
    public class ReturnPendingCount :BaseReturnSP{
        public PendingCount pendingCount { get; set; }
    }
}