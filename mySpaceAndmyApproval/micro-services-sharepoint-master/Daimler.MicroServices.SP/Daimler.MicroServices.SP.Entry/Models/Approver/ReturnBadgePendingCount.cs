﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.Approver;

namespace Daimler.MicroServices.SP.Entry.Models.Approver {
    public class ReturnBadgePendingCount : BaseReturnSP {
        public BadgePendingCount pendingCount { get; set; }
    }
}