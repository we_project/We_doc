﻿using System.Security.Claims;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;

namespace Daimler.MicroServices.SP.Entry.Models {
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType) {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            //var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            ClaimsIdentity userIdentity = null;
            try {
                userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
                userIdentity.AddClaim(new Claim(UserCredential.Fields.domain, UserCredential.domain));
                userIdentity.AddClaim(new Claim(UserCredential.Fields.userName, UserCredential.userName));
                userIdentity.AddClaim(new Claim(UserCredential.Fields.psw, UserCredential.psw));
            } catch (System.Exception ex) {

                throw;
            }
            // Add custom user claims here
            return userIdentity;
        }
        public UserCredential UserCredential { get; set; }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser> {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false) {
        }

        public static ApplicationDbContext Create() {
            return new ApplicationDbContext();
        }
    }
}