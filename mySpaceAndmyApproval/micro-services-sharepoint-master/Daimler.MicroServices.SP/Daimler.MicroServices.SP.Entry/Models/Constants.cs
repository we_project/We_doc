﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Daimler.MicroServices.SP.Entry.Models {
    public class Constants {
        public static string SMUserHead = "SM_USER";

        public class Split {
            public static string KeyValueSplit = ": ";
            public static string KeyValueSplitWithNewLine = ":\n";
        }

        public const int PageSize = 10;
    }
}