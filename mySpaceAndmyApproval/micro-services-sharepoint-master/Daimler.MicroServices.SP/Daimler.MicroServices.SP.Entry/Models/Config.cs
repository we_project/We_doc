﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Daimler.ApproverHome.WebApi.Entry.Models {
    public class Config {
        public static string SMLogoutUrl {
            get {
                return ConfigurationManager.AppSettings["SMLogoutUrl"];
            }
        }
        
    }
}