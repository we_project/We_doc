﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Daimler.MicroServices.SP.BizObject;

namespace Daimler.MicroServices.SP.Entry.Models {
    public static class ExceptionClassifier {
        public static ReturnMsgSP GetFrom(Exception ex) {
            return GetFrom(GetErrors(ex));
        }

        public static ReturnMsgSP GetFrom(AllErrors error) {
            switch (error) {
                case AllErrors.SPLoginFailed:
                    return ReturnMsgSP.SPLoginFailed;
                default:
                    return ReturnMsgSP.ExceptionOccurs;
            }
        }
        public static AllErrors GetErrors(Exception ex) {
            if (SPLoginError.IsSPLoginError(ex)) {
                return AllErrors.SPLoginFailed;
            } else {
                return AllErrors.Others;
            }
        }
    }
}