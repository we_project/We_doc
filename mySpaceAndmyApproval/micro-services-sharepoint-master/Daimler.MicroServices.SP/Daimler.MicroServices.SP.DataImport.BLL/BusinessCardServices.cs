﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.BusinessCard;
using Daimler.MicroServices.SP.BLL;
using Daimler.MicroServices.SP.Service.Common;
using log4net;

namespace Daimler.MicroServices.SP.DataImport.BLL {
    class BusinessCardServices {
        private static readonly ILog log = LogManager.GetLogger(typeof(BusinessCardServices));
        
        private static readonly ICommon service = new CommonImpl(SPService.uc);
        //private static readonly IOfficeStationary officeStationaryService = new OfficeStationaryImpl(SPService.uc);
        public static async Task LoadData() {
            log.Info("BusinessCardServices Begin Load ");
            Task<bool> t1 = LoadBCApplicationAsync();
            Task[] tasks ={
                            t1
                         };
            await Task.WhenAll(tasks);

            log.Info("BusinessCardServices End Load");
        }

        private static async Task<bool> LoadBCApplicationAsync() {
            try {
                log.Info("Begin get BCApplicationInfo:");
                List<BCApplicationInfo> infoes = await service.GetListItemsAsync<BCApplicationInfo>();
                int count = infoes != null ? infoes.Count : 0;
                log.InfoFormat("BCApplicationInfo count: {0}", count);
                if (count > 0) {
                    bool saveRequestsResult = await BusinessCardService.SaveBCApplicatoinAsync(infoes);
                    log.InfoFormat("save BCApplicationInfo: {0}", saveRequestsResult);
                    return saveRequestsResult;
                }
                return true;
            } catch (Exception ex) {
                log.Error("LoadBCApplicationAsync", ex);
                return false;
            }
        }
    }
}
