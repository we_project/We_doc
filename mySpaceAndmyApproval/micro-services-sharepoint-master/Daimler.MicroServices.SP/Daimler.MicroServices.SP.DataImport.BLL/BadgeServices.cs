﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.AccessManagement;
using Daimler.MicroServices.SP.BLL;
using Daimler.MicroServices.SP.Service.AccessManagement;
using Daimler.MicroServices.SP.Service.Common;
using log4net;

namespace Daimler.MicroServices.SP.DataImport.BLL {
    /// <summary>
    /// Load
    ///     CostCenterDeputyInfo
    ///     Site
    ///     Location
    ///     Area
    /// </summary>
    class BadgeServices {
        private static readonly ILog log = LogManager.GetLogger(typeof(BadgeServices));

        private static readonly ICommon service = new CommonImpl(SPService.uc);
        private static readonly Site site = new Site(SPService.uc);
        private static readonly Location location = new Location(SPService.uc);
        private static readonly Area area = new Area(SPService.uc);
        public static async Task LoadData() {
            log.Info("BadgeServices Begin Load ");
            Task<bool> t1 = LoadCCDeputyAsync();
            Task<bool> t2 = LoadSiteAsync();
            Task<bool> t3 = LoadLocationAsync();
            Task<bool> t4 = LoadAreaAsync();
            Task[] tasks ={
                            t1,t2,t3,t4
                         };
            await Task.WhenAll(tasks);

            log.Info("BadgeServices End Load");
        }

        private static async Task<bool> LoadCCDeputyAsync() {
            try {
                log.Info("Begin get CostCenterDeputyProfileInfo:");
                List<CostCenterDeputyProfileInfo> infoes = await service.GetListItemsAsync<CostCenterDeputyProfileInfo>();
                int count = infoes != null ? infoes.Count : 0;
                log.InfoFormat("CostCenterDeputyProfileInfo count: {0}", count);
                if (count > 0) {
                    bool saveRequestsResult = await BadgeService.SaveCCDeputyAsync(infoes);
                    log.InfoFormat("save CostCenterDeputyProfileInfo: {0}", saveRequestsResult);
                    return saveRequestsResult;
                }
                return true;
            } catch (Exception ex) {
                log.Error("LoadCCDeputyAsync", ex);
                return false;
            }
        }

        private static async Task<bool> LoadSiteAsync() {
            try {
                log.Info("Begin get SiteInfo:");
                List<SiteInfo> infoes = await site.GetAllItemsAsyncV2<SiteInfo>();
                int count = infoes != null ? infoes.Count : 0;
                log.InfoFormat("SiteInfo count: {0}", count);
                if (count > 0) {
                    bool saveRequestsResult = await BadgeService.SaveSitesAsync(infoes);
                    log.InfoFormat("save SiteInfo: {0}", saveRequestsResult);
                    return saveRequestsResult;
                }
                return true;
            } catch (Exception ex) {
                log.Error("LoadSiteAsync", ex);
                return false;
            }
        }
        private static async Task<bool> LoadLocationAsync() {
            try {
                log.Info("Begin get LocationInfo:");
                List<LocationInfo> infoes = await location.GetAllItemsAsyncV2<LocationInfo>();
                int count = infoes != null ? infoes.Count : 0;
                log.InfoFormat("LocationInfo count: {0}", count);
                if (count > 0) {
                    bool saveRequestsResult = await BadgeService.SaveLocationsAsync(infoes);
                    log.InfoFormat("save LocationInfo: {0}", saveRequestsResult);
                    return saveRequestsResult;
                }
                return true;
            } catch (Exception ex) {
                log.Error("LoadLocationAsync", ex);
                return false;
            }
        }
        private static async Task<bool> LoadAreaAsync() {
            try {
                log.Info("Begin get AreaInfo:");
                List<AreaInfo> infoes = await area.GetAllItemsAsyncV2<AreaInfo>();
                int count = infoes != null ? infoes.Count : 0;
                log.InfoFormat("AreaInfo count: {0}", count);
                if (count > 0) {
                    bool saveRequestsResult = await BadgeService.SaveAreasAsync(infoes);
                    log.InfoFormat("save AreaInfo: {0}", saveRequestsResult);
                    return saveRequestsResult;
                }
                return true;
            } catch (Exception ex) {
                log.Error("LoadAreaAsync", ex);
                return false;
            }
        }
    }
}
