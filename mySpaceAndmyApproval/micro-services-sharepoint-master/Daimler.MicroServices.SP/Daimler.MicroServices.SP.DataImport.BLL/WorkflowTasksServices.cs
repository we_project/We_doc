﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.Workflow;
using Daimler.MicroServices.SP.BLL;
using Daimler.MicroServices.SP.Service.Common;
using log4net;

namespace Daimler.MicroServices.SP.DataImport.BLL {
    public class WorkflowTasksServices {
        private static readonly ILog log = LogManager.GetLogger(typeof(WorkflowTasksServices));

        private static readonly ICommon service = new CommonImpl(SPService.uc);
        //private static readonly IOfficeStationary officeStationaryService = new OfficeStationaryImpl(SPService.uc);
        public static async Task LoadData() {
            log.Info("WorkflowTasksServices Begin Load ");
            Task<bool> t1 = LoadWorkflowTasksAsync();
            Task[] tasks ={
                            t1
                         };
            await Task.WhenAll(tasks);

            log.Info("WorkflowTasksServices End Load");
        }

        private static async Task<bool> LoadWorkflowTasksAsync() {
            try {
                log.Info("Begin get WorkflowTaskInfo:");
                List<WorkflowTaskInfo> infoes = await service.GetListItemsAsync<WorkflowTaskInfo>();
                int count = infoes != null ? infoes.Count : 0;
                log.InfoFormat("WorkflowTaskInfo count: {0}", count);
                if (count > 0) {
                    bool saveRequestsResult = await WorkflowTasksService.SaveWorkflowTasksAsync(infoes);
                    log.InfoFormat("save WorkflowTaskInfo: {0}", saveRequestsResult);
                    return saveRequestsResult;
                }
                return true;
            } catch (Exception ex) {
                log.Error("LoadWorkflowTasksAsync", ex);
                return false;
            }
        }
    }
}
