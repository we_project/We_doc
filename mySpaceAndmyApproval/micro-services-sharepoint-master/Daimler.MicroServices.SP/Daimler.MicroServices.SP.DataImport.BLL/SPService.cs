﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.Service;
using log4net;

namespace Daimler.MicroServices.SP.DataImport.BLL {
    public class SPService {
        private static readonly ILog log = LogManager.GetLogger(typeof(SPService));
        public static readonly UserCredential uc = new UserCredential {
            domain = SPConfig.Instance.Domain,
            userName = SPConfig.Instance.UserName,
            psw = SPConfig.Instance.Password
        };

        public static void LoadData() {
            log.Info("Begin");
            Task t10 = EIBMServices.LoadData();
            Task t11 = VisitorBadgeServices.LoadData();
            Task t9 = BadgeServices.LoadData();

            Task t1 = OfficeStationaryServices.LoadData();
            Task t2 = UserServices.LoadData();
            Task t3 = WorkflowTasksServices.LoadData();
            Task t4 = BusinessCardServices.LoadData();
            Task t5 = eLOIApplicationServices.LoadData();
            Task t6 = PetrolCardServices.LoadData();
            Task t7 = FormAServices.LoadData();
            Task t8 = FormBServices.LoadData();


            Task[] tasks ={
                             t1,
                             t2,
                             t3,
                             t4,
                             t5,
                             t6,
                             t7,
                             t8,
                             t9,
                             t10,
                             t11
                         };
            Task.WaitAll(tasks);
            System.Console.WriteLine("End");
        }
    }
}
