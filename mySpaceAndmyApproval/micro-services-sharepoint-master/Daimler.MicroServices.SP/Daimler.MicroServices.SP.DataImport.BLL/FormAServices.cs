﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.FormAB;
using Daimler.MicroServices.SP.BLL;
using Daimler.MicroServices.SP.Service.Common;
using log4net;

namespace Daimler.MicroServices.SP.DataImport.BLL {
    class FormAServices {
        private static readonly ILog log = LogManager.GetLogger(typeof(FormAServices));

        private static readonly ICommon service = new CommonImpl(SPService.uc);
        //private static readonly IOfficeStationary officeStationaryService = new OfficeStationaryImpl(SPService.uc);
        public static async Task LoadData() {
            log.Info("FormAServices Begin Load ");
            Task<bool> t1 = LoadFormAAsync();
            Task<bool> t2 = LoadFormACommentHistoryInfoAsync();
            Task[] tasks ={
                            t1,
                            t2,
                         };
            await Task.WhenAll(tasks);

            log.Info("FormAServices End Load");
        }

        private static async Task<bool> LoadFormAAsync() {
            try {
                log.Info("Begin get FormAInfo:");
                List<FormAInfo> infoes = await service.GetListItemsAsync<FormAInfo>();
                int count = infoes != null ? infoes.Count : 0;
                log.InfoFormat("FormAInfo count: {0}", count);
                if (count > 0) {
                    bool saveRequestsResult = await FormAService.SaveFormAInfoAsync(infoes);
                    log.InfoFormat("save FormAInfo: {0}", saveRequestsResult);
                    return saveRequestsResult;
                }
                return true;
            } catch (Exception ex) {
                log.Error("LoadFormAAsync", ex);
                return false;
            }
        }

        private static async Task<bool> LoadFormACommentHistoryInfoAsync() {
            try {
                log.Info("Begin get FormACommentHistoryInfo:");
                List<FormACommentHistoryInfo> infoes = await service.GetListItemsAsync<FormACommentHistoryInfo>();
                int count = infoes != null ? infoes.Count : 0;
                log.InfoFormat("FormACommentHistoryInfo count: {0}", count);
                if (count > 0) {
                    bool saveRequestsResult = await FormAService.SaveFormACommentHistoryInfoAsync(infoes);
                    log.InfoFormat("save FormACommentHistoryInfo: {0}", saveRequestsResult);
                    return saveRequestsResult;
                }
                return true;
            } catch (Exception ex) {
                log.Error("LoadFormACommentHistoryInfoAsync", ex);
                return false;
            }
        }
    }
}
