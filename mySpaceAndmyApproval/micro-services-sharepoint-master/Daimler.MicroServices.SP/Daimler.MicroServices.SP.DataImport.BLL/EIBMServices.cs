﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.AccessManagement;
using Daimler.MicroServices.SP.BLL;
using Daimler.MicroServices.SP.Service.AccessManagement;
using Daimler.MicroServices.SP.Service.Common;
using log4net;

namespace Daimler.MicroServices.SP.DataImport.BLL {
    public class EIBMServices {
        private static readonly ILog log = LogManager.GetLogger(typeof(EIBMServices));

        private static readonly EIBMRestful eibm = new EIBMRestful(SPService.uc);
        public static async Task LoadData() {
            log.Info("EIBMServices Begin Load ");
            Task<bool> t1 = LoadEIBMAsync();
            Task[] tasks ={
                            t1
                         };
            await Task.WhenAll(tasks);

            log.Info("EIBMServices End Load");
        }

        private static async Task<bool> LoadEIBMAsync() {
            try {
                log.Info("Begin get EIBMInfo:");
                List<EIBMInfo> infoes = await eibm.GetAllItemsAsyncV2<EIBMInfo>(RestfulUrlHelper.ConditionPart(EIBMFields.Status, RestfulUrlHelper.Eq, EIBMStatus.PendingCCMApproval) + " or " + RestfulUrlHelper.ConditionPart(EIBMFields.Status, RestfulUrlHelper.Eq, EIBMStatus.PendingSpecialAreaApproval));
                int count = infoes != null ? infoes.Count : 0;
                log.InfoFormat("EIBMInfo count: {0}", count);
                if (count > 0) {
                    bool saveRequestsResult = await EIBMService.SaveEIBMsAsync(infoes);
                    log.InfoFormat("save EIBMInfo: {0}", saveRequestsResult);
                    return saveRequestsResult;
                } else {
                    // if no pending, then get all items to keep db data clean
                    log.Info("Begin get EIBMInfo:");
                    List<EIBMInfo> infoes1 = await eibm.GetAllItemsAsyncV2<EIBMInfo>();
                    int count1 = infoes1 != null ? infoes1.Count : 0;
                    log.InfoFormat("EIBMInfo count: {0}", count1);
                    if (count1 > 0) {
                        bool saveRequestsResult = await EIBMService.SaveEIBMsAsync(infoes1);
                        log.InfoFormat("save EIBMInfo: {0}", saveRequestsResult);
                        return saveRequestsResult;
                    }
                }
                return true;
            } catch (Exception ex) {
                log.Error("LoadEIBMAsync", ex);
                return false;
            }
        }
    }
}
