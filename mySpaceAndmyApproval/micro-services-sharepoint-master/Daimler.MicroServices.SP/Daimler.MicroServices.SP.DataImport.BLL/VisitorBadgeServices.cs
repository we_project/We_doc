﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.AccessManagement;
using Daimler.MicroServices.SP.BLL;
using Daimler.MicroServices.SP.Service.AccessManagement;
using log4net;

namespace Daimler.MicroServices.SP.DataImport.BLL {
    public class VisitorBadgeServices {
        private static readonly ILog log = LogManager.GetLogger(typeof(VisitorBadgeServices));

        private static readonly VisitorBadgeRestful vb = new VisitorBadgeRestful(SPService.uc);
        public static async Task LoadData() {
            log.Info("VisitorBadgeServices Begin Load ");
            Task<bool> t1 = LoadVisitorBadgeAsync();
            Task[] tasks ={
                            t1
                         };
            await Task.WhenAll(tasks);

            log.Info("VisitorBadgeServices End Load");
        }

        private static async Task<bool> LoadVisitorBadgeAsync() {
            try {
                log.Info("Begin get VisitorBadgeInfo:");
                List<VistorBadgeApplicationInfo> infoes = await vb.GetAllItemsAsyncV2<VistorBadgeApplicationInfo>(RestfulUrlHelper.ConditionPart(EIBMFields.Status, RestfulUrlHelper.Eq, EIBMStatus.PendingCCMApproval) + " or " + RestfulUrlHelper.ConditionPart(EIBMFields.Status, RestfulUrlHelper.Eq, EIBMStatus.PendingSpecialAreaApproval));
                int count = infoes != null ? infoes.Count : 0;
                log.InfoFormat("VisitorBadgeInfo count: {0}", count);
                if (count > 0) {
                    bool saveRequestsResult = await VisitorBadgeService.SaveVisitorBadgesAsync(infoes);
                    log.InfoFormat("save VisitorBadgeInfo: {0}", saveRequestsResult);
                    return saveRequestsResult;
                } else {
                    // if no pending, then get all items to keep db data clean
                    log.Info("Begin get VisitorBadgeInfo:");
                    List<VistorBadgeApplicationInfo> infoes1 = await vb.GetAllItemsAsyncV2<VistorBadgeApplicationInfo>();
                    int count1 = infoes1 != null ? infoes1.Count : 0;
                    log.InfoFormat("VisitorBadgeInfo count: {0}", count1);
                    if (count1 > 0) {
                        bool saveRequestsResult = await VisitorBadgeService.SaveVisitorBadgesAsync(infoes1);
                        log.InfoFormat("save VisitorBadgeInfo: {0}", saveRequestsResult);
                        return saveRequestsResult;
                    }
                }
                return true;
            } catch (Exception ex) {
                log.Error("LoadVisitorBadgeAsync", ex);
                return false;
            }
        }
    }
}
