﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BLL;
using Daimler.MicroServices.SP.Service.Common;
using log4net;

namespace Daimler.MicroServices.SP.DataImport.BLL {
    public class UserServices {
        private static readonly ILog log = LogManager.GetLogger(typeof(UserServices));

        private static readonly ICommon service = new CommonImpl(SPService.uc);
        public static async Task LoadData() {
            log.Info("UserServices Begin Load ");
            Task<bool> t1 = LoadCommonUserAsync();
            Task<bool> t2 = LoadFormABUserAsync();
            Task<bool> t3 = LoadBadgeUserAsync();
            Task[] tasks ={
                            t1,t2,t3
                         };
            await Task.WhenAll(tasks);

            log.Info("UserServices End Load");
        }

        private static async Task<bool> LoadCommonUserAsync() {
            try {
                log.Info("Begin get CommonUser:");
                List<SPUserInfo> infoes = await service.GetAllUsersAsync();
                int count = infoes != null ? infoes.Count : 0;
                log.InfoFormat("CommonUser count: {0}", count);
                if (count > 0) {
                    bool saveRequestsResult = await UserService.SaveCommonUserAsync(infoes);
                    log.InfoFormat("save CommonUser: {0}", saveRequestsResult);
                    return saveRequestsResult;
                }
                return true;
            } catch (Exception ex) {
                log.Error("LoadCommonUserAsync", ex);
                return false;
            }
        }

        private static async Task<bool> LoadFormABUserAsync() {
            try {
                log.Info("Begin get FormABUser:");
                List<SPUserInfo> infoes = await service.GetAllFormABUsersAsync();
                int count = infoes != null ? infoes.Count : 0;
                log.InfoFormat("FormABUser count: {0}", count);
                if (count > 0) {
                    bool saveRequestsResult = await UserService.SaveFormABUserAsync(infoes);
                    log.InfoFormat("save FormABUser: {0}", saveRequestsResult);
                    return saveRequestsResult;
                }
                return true;
            } catch (Exception ex) {
                log.Error("LoadFormABUserAsync", ex);
                return false;
            }
        }
        private static async Task<bool> LoadBadgeUserAsync() {
            try {
                log.Info("Begin LoadBadgeUserAsync:");
                List<SPUserInfo> infoes = await service.GetAllBadgeUsersAsync();
                int count = infoes != null ? infoes.Count : 0;
                log.InfoFormat("BadgeUser count: {0}", count);
                if (count > 0) {
                    bool saveRequestsResult = await UserService.SaveBadgeUserAsync(infoes);
                    log.InfoFormat("save BadgeUser: {0}", saveRequestsResult);
                    return saveRequestsResult;
                }
                return true;
            } catch (Exception ex) {
                log.Error("LoadBadgeUserAsync", ex);
                return false;
            }
        }
    }
}
