﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.OfficeStationary;
using Daimler.MicroServices.SP.BLL;
using Daimler.MicroServices.SP.Service.Common;
using Daimler.MicroServices.SP.Service.OfficeStationary;
using log4net;

namespace Daimler.MicroServices.SP.DataImport.BLL {
    public class OfficeStationaryServices {
        private static readonly ILog log = LogManager.GetLogger(typeof(OfficeStationaryServices));

        private static readonly ICommon service = new CommonImpl(SPService.uc);
        private static readonly IOfficeStationary officeStationaryService = new OfficeStationaryImpl(SPService.uc);
        public static async Task LoadData() {
            log.Info("OfficeStationaryServices Begin Load ");
            Task<bool> t1 = LoadRequestsAsync();
            Task<bool> t2 = LoadOSLocationAsync();
            Task<bool> t3 = LoadMenusAsync();
            Task[] tasks ={
                            t1,t2,t3
                         };
            await Task.WhenAll(tasks);

            log.Info("OfficeStationaryServices End Load");
        }

        private static async Task<bool> LoadRequestsAsync() {
            try {
                log.Info("Begin get StationaryRequestInfo:");
                List<StationaryRequestInfo> infoes = await service.GetListItemsAsync<StationaryRequestInfo>();
                int count = infoes != null ? infoes.Count : 0;
                log.InfoFormat("StationaryRequestInfo count: {0}", count);
                if (count > 0) {
                    bool saveRequestsResult = await OfficeStationaryService.SaveRequestsAsync(infoes);
                    log.InfoFormat("save requests: {0}", saveRequestsResult);
                    return saveRequestsResult;
                }
                return true;
            } catch (Exception ex) {
                log.Error("LoadRequests", ex);
                return false;
            }
        }

        private static async Task<bool> LoadOSLocationAsync() {
            try {
                log.Info("Begin get OSLocationInfo :");
                List<OSLocationInfo> infoes = await service.GetListItemsAsync<OSLocationInfo>();
                int count = infoes != null ? infoes.Count : 0;
                log.InfoFormat("OSLocationInfo count: {0}", count);
                if (count > 0) {
                    bool saveRequestsResult = await OfficeStationaryService.SaveOSLocationAsync(infoes);
                    log.InfoFormat("save OSLocationInfo: {0}", saveRequestsResult);
                    return saveRequestsResult;
                }
                return true;
            } catch (Exception ex) {
                log.Error("LoadOSLocation", ex);
                return false;
            }
        }
        private static async Task<bool> LoadMenusAsync() {
            try {
                log.Info("Begin get StationaryMenuInfo :");
                List<StationaryMenuInfo> infoes = await service.GetListItemsAsync<StationaryMenuInfo>();
                int count = infoes != null ? infoes.Count : 0;
                log.InfoFormat("StationaryMenuInfo count: {0}", count);
                if (count > 0) {
                    bool saveRequestsResult = await OfficeStationaryService.SaveMenusAsync(infoes);
                    log.InfoFormat("save StationaryMenuInfo: {0}", saveRequestsResult);
                    await officeStationaryService.LoadStationaryImgsAsync(infoes);
                    log.Info("LoadStationaryImgsAsync: success");
                    return saveRequestsResult;
                }
                return true;
            } catch (Exception ex) {
                log.Error("LoadMenus", ex);
                return false;
            }
        }
    }
}
