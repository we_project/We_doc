﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.InvitationLetter;
using Daimler.MicroServices.SP.BLL;
using Daimler.MicroServices.SP.Service.Common;
using log4net;

namespace Daimler.MicroServices.SP.DataImport.BLL {
    class eLOIApplicationServices {
        private static readonly ILog log = LogManager.GetLogger(typeof(eLOIApplicationServices));

        private static readonly ICommon service = new CommonImpl(SPService.uc);
        //private static readonly IOfficeStationary officeStationaryService = new OfficeStationaryImpl(SPService.uc);
        public static async Task LoadData() {
            log.Info("eLOIApplicationServices Begin Load ");
            Task<bool> t1 = LoadILApplicationAsync();
            Task[] tasks ={
                            t1
                         };
            await Task.WhenAll(tasks);

            log.Info("eLOIApplicationServices End Load");
        }

        private static async Task<bool> LoadILApplicationAsync() {
            try {
                log.Info("Begin get eLOIApplicationInfo:");
                List<eLOIApplicationInfo> infoes = await service.GetListItemsAsync<eLOIApplicationInfo>();
                int count = infoes != null ? infoes.Count : 0;
                log.InfoFormat("eLOIApplicationInfo count: {0}", count);
                if (count > 0) {
                    bool saveRequestsResult = await InvitationLetterService.SaveILApplicatoinAsync(infoes);
                    log.InfoFormat("save eLOIApplicationInfo: {0}", saveRequestsResult);
                    return saveRequestsResult;
                }
                return true;
            } catch (Exception ex) {
                log.Error("LoadILApplicationAsync", ex);
                return false;
            }
        }
    }
}
