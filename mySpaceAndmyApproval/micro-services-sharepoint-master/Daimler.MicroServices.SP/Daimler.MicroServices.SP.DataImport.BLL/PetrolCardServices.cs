﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.PetrolCard;
using Daimler.MicroServices.SP.BLL;
using Daimler.MicroServices.SP.Service.Common;
using log4net;

namespace Daimler.MicroServices.SP.DataImport.BLL {
    class PetrolCardServices {
        private static readonly ILog log = LogManager.GetLogger(typeof(PetrolCardServices));

        private static readonly ICommon service = new CommonImpl(SPService.uc);
        //private static readonly IOfficeStationary officeStationaryService = new OfficeStationaryImpl(SPService.uc);
        public static async Task LoadData() {
            log.Info("PetrolCardServices Begin Load ");
            Task<bool> t1 = LoadPCCAAsync();
            Task[] tasks ={
                            t1
                         };
            await Task.WhenAll(tasks);

            log.Info("PetrolCardServices End Load");
        }

        private static async Task<bool> LoadPCCAAsync() {
            try {
                log.Info("Begin get PCCreditAdjustmentInfo:");
                List<PCCreditAdjustmentInfo> infoes = await service.GetListItemsAsync<PCCreditAdjustmentInfo>();
                int count = infoes != null ? infoes.Count : 0;
                log.InfoFormat("PCCreditAdjustmentInfo count: {0}", count);
                if (count > 0) {
                    bool saveRequestsResult = await PetrolCardService.SavePCCAAsync(infoes);
                    log.InfoFormat("save PCCreditAdjustmentInfo: {0}", saveRequestsResult);
                    return saveRequestsResult;
                }
                return true;
            } catch (Exception ex) {
                log.Error("LoadPCCAAsync", ex);
                return false;
            }
        }
    }
}
