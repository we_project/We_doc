﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.FormAB;
using Daimler.MicroServices.SP.BLL;
using Daimler.MicroServices.SP.Service.Common;
using log4net;

namespace Daimler.MicroServices.SP.DataImport.BLL {
    class FormBServices {
        private static readonly ILog log = LogManager.GetLogger(typeof(FormBServices));

        private static readonly ICommon service = new CommonImpl(SPService.uc);
        public static async Task LoadData() {
            log.Info("FormBServices Begin Load ");
            Task<bool> t1 = LoadFormBAsync();
            Task<bool> t2 = LoadFormBCommentHistoryInfoAsync();
            Task[] tasks ={
                            t1,
                            t2,
                         };
            await Task.WhenAll(tasks);

            log.Info("FormBServices End Load");
        }

        private static async Task<bool> LoadFormBAsync() {
            try {
                log.Info("Begin get FormBInfo:");
                List<FormBInfo> infoes = await service.GetListItemsAsync<FormBInfo>();
                int count = infoes != null ? infoes.Count : 0;
                log.InfoFormat("FormBInfo count: {0}", count);
                if (count > 0) {
                    bool saveRequestsResult = await FormBService.SaveFormBInfoAsync(infoes);
                    log.InfoFormat("save FormBInfo: {0}", saveRequestsResult);
                    return saveRequestsResult;
                }
                return true;
            } catch (Exception ex) {
                log.Error("LoadFormBAsync", ex);
                return false;
            }
        }

        private static async Task<bool> LoadFormBCommentHistoryInfoAsync() {
            try {
                log.Info("Begin get FormBCommentHistoryInfo:");
                List<FormBCommentHistoryInfo> infoes = await service.GetListItemsAsync<FormBCommentHistoryInfo>();
                int count = infoes != null ? infoes.Count : 0;
                log.InfoFormat("FormBCommentHistoryInfo count: {0}", count);
                if (count > 0) {
                    bool saveRequestsResult = await FormBService.SaveFormBCommentHistoryInfoAsync(infoes);
                    log.InfoFormat("save FormBCommentHistoryInfo: {0}", saveRequestsResult);
                    return saveRequestsResult;
                }
                return true;
            } catch (Exception ex) {
                log.Error("LoadFormBCommentHistoryInfoAsync", ex);
                return false;
            }
        }
    }
}
