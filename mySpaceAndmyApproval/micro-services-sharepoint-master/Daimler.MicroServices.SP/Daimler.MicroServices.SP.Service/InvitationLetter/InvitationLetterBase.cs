﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;

namespace Daimler.MicroServices.SP.Service.InvitationLetter {
    public abstract class InvitationLetterBase : AbstractSharepoint {
        public InvitationLetterBase(UserCredential uc) : base(uc) { }
    }
}
