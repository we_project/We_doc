﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.Approver;
using Daimler.MicroServices.SP.BizObject.Workflow;
using Daimler.MicroServices.SP.Service.CamlQueryXmls;
using Daimler.MicroServices.SP.Service.Common;
using log4net;
using Microsoft.SharePoint.Client;
using Proxys;

namespace Daimler.MicroServices.SP.Service.Workflow {
    public class WorkflowTasksImpl : IWorkflowTasks, IDisposable {
        private static readonly ILog log = LogManager.GetLogger(typeof(WorkflowTasksImpl));
        public UserCredential uc { get; set; }
        private readonly ICommon commonService;
        public AbstractSharepoint sharepoint { get; set; }
        NintexWorkflowWSSoapClient client { get; set; }
        public WorkflowTasksImpl(UserCredential uc) {
            this.uc = uc;
            this.commonService = new CommonImpl(uc);
            sharepoint = new WorkflowTasks(uc);
            client = new NintexWorkflowWSSoapClient("NintexWorkflowWSSoap12");
            ServicePointManager.ServerCertificateValidationCallback = (s, certificate, chain, sslPolicyErrors) => true;
            client.ClientCredentials.Windows.ClientCredential = AbstractSharepoint.GetCredential(uc);
        }
        ApproveResult IWorkflowTasks.Approve(int id, bool approve, string comment) {
            ApproveResult result = new ApproveResult();
            #region webservice
            string outcome = approve ? OutCome.Approve : OutCome.Reject;
            ProcessTaskResponseResult clientResult = client.ProcessFlexiTaskResponse2(comment, outcome, id, WorkflowTasks.ListName);
            switch (clientResult) {
                case ProcessTaskResponseResult.Success:
                    result.Success = true;
                    break;
                case ProcessTaskResponseResult.CannotObtainLock:
                    result.Msg = AllErrors.CannotObtainLock;
                    break;
                case ProcessTaskResponseResult.InvalidUser:
                    result.Msg = AllErrors.InvalidUser;
                    break;
                default:
                    break;
            }
            #endregion
            return result;
        }
        async Task<int?> IWorkflowTasks.GetPendingWorkflowTasksByItemIdAsync(int userId, int itemId, string workflowName) {
            Task<int?> task = Task.Factory.StartNew<int?>(() => {
                return GetPendingWorkflowTasksByItemId(userId, itemId, workflowName);
            });
            return await task;
        }
        public int? GetPendingWorkflowTasksByItemId(int userId, int itemId, string workflowName) {
            int? id = null;
            CamlQuery query = getQuery(userId, itemId, workflowName);
            if (query != null) {
                ListItemCollection items = sharepoint.GetListItems(query, new List<string>() { 
                    WorkflowTaskInfoFields.ID
                    });
                if (items != null && items.Count > 0) {
                    ListItem item = items[0];
                    id = int.Parse(item[WorkflowTaskInfoFields.ID].ToString());
                }
            }
            return id;
        }
        private static CamlQuery getQuery(int userId, int itemId, string workflowName) {
            CamlQuery query = null;
            string xml = null;
            if (WorkflowNames.BusinessCardApproveProcess.Equals(workflowName)) {
                xml = CamlQueryHelper.Workflow.GetPendingBusinessCardWorkflowTasksByItemId(userId, itemId);
            } else if (WorkflowNames.Invitation.Equals(workflowName)) {
                xml = CamlQueryHelper.Workflow.GetPendingInvitationWorkflowTasksByItemId(userId, itemId);
            }
            if (!string.IsNullOrWhiteSpace(xml)) {
                query = new CamlQuery();
                query.ViewXml = xml;
            } else {
                string msg = "get xml failed in Approval: userId:" + userId.ToString() + "_ID:" + itemId.ToString();
                log.Info(msg);
            }
            return query;
        }

        void IDisposable.Dispose() {
            if (sharepoint != null) {
                sharepoint.Dispose();
            }
            if (client != null) {
                try {
                    (client as IDisposable).Dispose();
                } catch (Exception ex) {
                }
            }
        }

    }
}
