﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.Approver;

namespace Daimler.MicroServices.SP.Service.Workflow {
    public interface IWorkflowTasks {
        ApproveResult Approve(int id, bool approve, string comment);

        int? GetPendingWorkflowTasksByItemId(int userId, int itemId, string workflowName);
        Task<int?> GetPendingWorkflowTasksByItemIdAsync(int userId, int itemId, string workflowName);
    }
}
