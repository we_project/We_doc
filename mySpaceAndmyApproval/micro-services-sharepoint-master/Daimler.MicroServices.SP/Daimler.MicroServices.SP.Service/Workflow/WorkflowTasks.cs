﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.Workflow;
using Daimler.MicroServices.Utils;

namespace Daimler.MicroServices.SP.Service.Workflow {
    public class WorkflowTasks : AbstractSharepoint {
        public WorkflowTasks(UserCredential uc)
            : base(uc) {
        }
        public static string ListName = "Workflow Tasks";
        public override string listName {
            get { return ListName; }
        }

        public override List<string> FieldsToLoad {
            get { return _FieldsToLoad; }
        }
        public static List<string> _FieldsToLoad = ReflectionMethod.GetPropertyNames<WorkflowTaskInfo>();
    }
}
