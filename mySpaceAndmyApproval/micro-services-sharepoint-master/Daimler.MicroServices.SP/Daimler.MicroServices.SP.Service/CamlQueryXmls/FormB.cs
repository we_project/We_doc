﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.Service.CamlQueryXmls {
    public partial class CamlQueryHelper {
        public class FormB {

            public static string relativePath = Path.Combine(CamlQueryHelper.relativePath, "FormB");
            static FormB() {
                try {
                    xmlGetPendingApprove = CamlQueryHelper.ReadFile(CamlXmls.FormB.GetPendingApprove, relativePath);
                    xmlGetCommentHistory = CamlQueryHelper.ReadFile(CamlXmls.FormB.GetCommentHistory, relativePath);
                } catch (Exception ex) {
                    log.Debug("CamlQueryHelper.FormB"+CamlQueryHelper.Steps.ReadCamlXml, ex);
                }
            }
            public static string xmlGetPendingApprove { get; private set; }
            public static string xmlGetCommentHistory { get; private set; }
          
            public static string GetPendingApprove(int userId) {
                string xml = xmlGetPendingApprove;
                if (!string.IsNullOrWhiteSpace(xml)) {
                    xml = xml.Replace("@userId", userId.ToString());
                }
                log.Debug(xml);
                return xml;
            }
            public static string GetCommentHistory(int caseID) {
                string xml = xmlGetCommentHistory;
                if (!string.IsNullOrWhiteSpace(xml)) {
                    xml = xml.Replace("@caseID", caseID.ToString());
                }
                log.Debug(xml);
                return xml;
            }
        }
    }
}
