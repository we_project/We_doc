﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Daimler.MicroServices.SP.BizObject;
using log4net;

namespace Daimler.MicroServices.SP.Service.CamlQueryXmls {
    public partial class CamlQueryHelper {
        private static readonly ILog log = LogManager.GetLogger(typeof(CamlQueryHelper));
        public class Steps {
            public static string ReadCamlXml = "ReadCamlXml";
        }

        public static string folder {
            get {
                if (System.Web.HttpContext.Current == null) return AppDomain.CurrentDomain.BaseDirectory;
                else return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "bin");
            }
        }
        public static string relativePath = "CamlQueryXmls";
        public static string ReadFile(string fileName, string relaPath) {
            string s = null;
            try {
                s = File.ReadAllText(Path.Combine(folder, relaPath, fileName));
            } catch (Exception ex) {
                log.Error(Steps.ReadCamlXml + Constants.LogFilePrefix, ex);
            }
            return s;
        }
        public static string GetListItemByTitle(string title) {
            string xml = xmlGetListItemByTitle;
            if (!string.IsNullOrWhiteSpace(xml)) {
                xml = xml.Replace("@title", title);
            }
            log.Debug(xml);
            return xml;
        }
        public static string xmlGetListItemsByIds { get; private set; }
        public static string xmlGetListItemByTitle { get; private set; }


        static CamlQueryHelper() {
            xmlGetListItemsByIds = ReadFile(CamlXmls.GetListItemsByIds, relativePath);
            xmlGetListItemByTitle = ReadFile(CamlXmls.GetListItemByTitle, relativePath);
        }
        public static string GetListItemsByIds(List<int> ids) {
            string xml = xmlGetListItemsByIds;
            if (!string.IsNullOrWhiteSpace(xml)) {
                XmlDocument xdoc = new XmlDocument();
                xdoc.LoadXml(xml);
                XmlNode inNode = xdoc.GetElementsByTagName("In")[0];
                XmlNode valuesNode = null;
                foreach (XmlNode item in inNode.ChildNodes) {
                    if (item.Name == "Values") {
                        valuesNode = item;
                        break;
                    }
                }
                if (valuesNode != null) {
                    //<Value Type="Counter">68</Value>
                    foreach (int id in ids) {
                        XmlDocumentFragment xdf = xdoc.CreateDocumentFragment();
                        string content = string.Format("<Value Type=\"Counter\">{0}</Value>", id);
                        xdf.InnerXml = content;
                        if (valuesNode.HasChildNodes) {
                            valuesNode.InsertAfter(xdf, valuesNode.LastChild);
                        } else {
                            valuesNode.InnerXml = content;
                        }
                    }

                    xml = GetFromXml(xdoc);
                }
            }
            log.Debug(xml);
            return xml;
        }

        public static string GetFromXml(XmlNode xNode) {
            StringBuilder sb = new StringBuilder();
            using (XmlWriter xw = XmlWriter.Create(sb)) {
                xNode.WriteTo(xw);
                xw.Flush();
                return sb.ToString();
            }
        }

        public static string GetFromXml(XmlDocument xDoc) {
            StringBuilder sb = new StringBuilder();
            //using (StringWriter sw=new StringWriter())
            using (XmlWriter xw = XmlWriter.Create(sb)) {
                xDoc.WriteTo(xw);
                xw.Flush();
                return sb.ToString();
            }
        }

    }
}
