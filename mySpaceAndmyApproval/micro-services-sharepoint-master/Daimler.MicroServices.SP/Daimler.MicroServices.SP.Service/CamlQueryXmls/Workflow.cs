﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.Service.CamlQueryXmls {
    public partial class CamlQueryHelper {
        
        public class Workflow {
            public static string xmlGetPendingBusinessCardWorkflowTasks { get; private set; }
            public static string xmlGetPendingInvitationWorkflowTasks { get; private set; }
            public static string xmlGetPendingBCAndILWorkflowTasks { get; private set; }
            public static string xmlGetPendingInvitationWorkflowTasksByItemId { get; private set; }
            public static string xmlGetPendingBusinessCardWorkflowTasksByItemId { get; private set; }
            static Workflow() {
                try {
                    xmlGetPendingBusinessCardWorkflowTasks = CamlQueryHelper.ReadFile(CamlXmls.Workflow.GetPendingBusinessCardWorkflowTasks, relativePath);
                    xmlGetPendingInvitationWorkflowTasks = CamlQueryHelper.ReadFile(CamlXmls.Workflow.GetPendingInvitationWorkflowTasks, relativePath);
                    xmlGetPendingBCAndILWorkflowTasks = CamlQueryHelper.ReadFile(CamlXmls.Workflow.GetPendingBCAndILWorkflowTasks, relativePath);
                    xmlGetPendingInvitationWorkflowTasksByItemId = CamlQueryHelper.ReadFile(CamlXmls.Workflow.GetPendingInvitationWorkflowTasksByItemId, relativePath);
                    xmlGetPendingBusinessCardWorkflowTasksByItemId = CamlQueryHelper.ReadFile(CamlXmls.Workflow.GetPendingBusinessCardWorkflowTasksByItemId, relativePath);
                } catch (Exception ex) {
                    log.Error( "CamlQueryHelper.Workflow"+ CamlQueryHelper.Steps.ReadCamlXml,ex);
                }
            }
            public static string relativePath = Path.Combine(CamlQueryHelper.relativePath, "Workflow");
            public static string GetPendingBusinessCardWorkflowTasks(int userId) {
                string xml = xmlGetPendingBusinessCardWorkflowTasks;
                if (!string.IsNullOrWhiteSpace(xml)) {
                    xml = xml.Replace("@userId", userId.ToString());
                }
                log.Debug(xml);
                return xml;
            }

            public static string GetPendingInvitationWorkflowTasks(int userId) {
                string xml = xmlGetPendingInvitationWorkflowTasks;
                if (!string.IsNullOrWhiteSpace(xml)) {
                    xml = xml.Replace("@userId", userId.ToString());
                }
                log.Debug(xml);
                return xml;
            }

            public static string GetPendingBCAndILWorkflowTasks(int userId) {
                string xml = xmlGetPendingBCAndILWorkflowTasks;
                if (!string.IsNullOrWhiteSpace(xml)) {
                    xml = xml.Replace("@userId", userId.ToString());
                }
                log.Debug(xml);
                return xml;
            }

            public static string GetPendingInvitationWorkflowTasksByItemId(int userId, int itemId) {
                string xml = xmlGetPendingInvitationWorkflowTasksByItemId;
                if (!string.IsNullOrWhiteSpace(xml)) {
                    xml = xml.Replace("@userId", userId.ToString())
                             .Replace("@itemId", itemId.ToString());
                }
                log.Debug(xml);
                return xml;
            }
            public static string GetPendingBusinessCardWorkflowTasksByItemId(int userId, int itemId) {
                string xml = xmlGetPendingBusinessCardWorkflowTasksByItemId;
                if (!string.IsNullOrWhiteSpace(xml)) {
                    xml = xml.Replace("@userId", userId.ToString())
                             .Replace("@itemId", itemId.ToString());
                }
                log.Debug(xml);
                return xml;
            }
        }
    }
}
