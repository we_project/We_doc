﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.Service.CamlQueryXmls {
    public partial class CamlQueryHelper {

        public class PetrolCard {
            public static string xmlGetListItemsBySupervisor { get; private set; }
            static PetrolCard() {
                try {
                    xmlGetListItemsBySupervisor = CamlQueryHelper.ReadFile(CamlXmls.PetrolCard.GetListItemsBySupervisor, relativePath);
                } catch (Exception ex) {
                    log.Debug("CamlQueryHelper.PetrolCard"+ CamlQueryHelper.Steps.ReadCamlXml,ex);
                }
            }
            public static string relativePath = Path.Combine(CamlQueryHelper.relativePath, "PetrolCard");

            public static string GetListItemsBySupervisor(string supervisor) {
                string xml = xmlGetListItemsBySupervisor;
                if (!string.IsNullOrWhiteSpace(xml)) {
                    xml = xml.Replace("@supervisor", supervisor);
                }
                log.Debug(xml);
                return xml;
            }

        }
    }

}
