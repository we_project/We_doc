﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.OfficeStationary;

namespace Daimler.MicroServices.SP.Service.CamlQueryXmls {
    public partial class CamlQueryHelper {
        public class Stationary {
            public static string xmlGetListItemsByCostCenterManager { get; private set; }
            public static string xmlGetPendingApprove { get; private set; }
            static Stationary() {
                try {
                    xmlGetListItemsByCostCenterManager = CamlQueryHelper.ReadFile(CamlXmls.Stationary.GetListItemsByCostCenterManager, relativePath);
                    xmlGetPendingApprove = CamlQueryHelper.ReadFile(CamlXmls.Stationary.GetPendingApprove, relativePath);
                } catch (Exception ex) {
                    log.Debug("CamlQueryHelper.Stationary"+ CamlQueryHelper.Steps.ReadCamlXml,ex);
                }
            }
            public static string relativePath = Path.Combine(CamlQueryHelper.relativePath, "Staionary");
            public static string GetListItemsByCostCenterManager(string CostCenterManager) {
                string xml = xmlGetListItemsByCostCenterManager;
                if (!string.IsNullOrWhiteSpace(xml)) {
                    xml = xml.Replace("@costCenterManager", CostCenterManager)
                             .Replace("@status", Status.CostCenterManagerApproveInProcess);
                }
                log.Debug(xml);
                return xml;
            }

            public static string GetPendingApprove(int userId) {
                string xml = xmlGetPendingApprove;
                if (!string.IsNullOrWhiteSpace(xml)) {
                    xml = xml.Replace("@userId", userId.ToString());
                }
                log.Debug(xml);
                return xml;
            }
        }
    }
}
