﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.Service.CamlQueryXmls {
    public class CamlXmls {
        public static string GetListItemByTitle = "GetListItemByTitle.xml";
        public static string GetListItemsByIds = "GetListItemsByIds.xml";
        public class Stationary {
            public static string GetListItemsByCostCenterManager = "GetListItemsByCostCenterManager.xml";
            public static string GetPendingApprove = "GetPendingApprove.xml";
        }
        public class BusinessCard {
            public static string GetListItemsBySupervisor = "GetListItemsBySupervisor.xml";
        }

        public class Workflow {
            public static string GetWorkflowTasksByAssignedTo = "GetWorkflowTasksByAssignedTo.xml";
            public static string GetPendingBusinessCardWorkflowTasks = "GetPendingBusinessCardWorkflowTasks.xml";
            public static string GetPendingInvitationWorkflowTasks = "GetPendingInvitationWorkflowTasks.xml";
            public static string GetPendingBCAndILWorkflowTasks = "GetPendingBCAndILWorkflowTasks.xml";

            public static string GetPendingInvitationWorkflowTasksByItemId = "GetPendingInvitationWorkflowTasksByItemId.xml";

            public static string GetPendingBusinessCardWorkflowTasksByItemId = "GetPendingBusinessCardWorkflowTasksByItemId.xml";
        }

        public class PetrolCard {
            public static string GetListItemsBySupervisor = "GetListItemsBySupervisor.xml";
        }

        public class FormA {
            public static string GetPendingApprove = "GetPendingApprove.xml";
            public static string GetCommentHistory = "GetCommentHistory.xml";
        }

        public class FormB {
            public static string GetPendingApprove = "GetPendingApprove.xml";
            public static string GetCommentHistory = "GetCommentHistory.xml";
        }
    }
}
