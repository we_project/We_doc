﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.Approver;
using Daimler.MicroServices.SP.BizObject.FormAB;

namespace Daimler.MicroServices.SP.Service.FormAB {
    public interface IFormAB {
        ApproveResult ApprovalFormA(string user, bool approve, int ID, string comment, string StatusIndex, FormATaxTeamFields taxTeamField);
        ApproveResult ApprovalFormB(string user, bool approve, int ID, string comment, string StatusIndex, FormBTaxTeamFields taxTeamField);
    }
}
