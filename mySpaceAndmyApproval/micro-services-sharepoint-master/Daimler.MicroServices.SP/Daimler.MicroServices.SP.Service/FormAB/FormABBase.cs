﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;

namespace Daimler.MicroServices.SP.Service.FormAB {
    public abstract class FormABBase : AbstractSharepoint {
        public FormABBase(UserCredential uc) : base(uc) { }
        public override string sharepointUrl {
            get {
                return SPConfig.Instance.FormAFormBUrl;
            }
        }
    }
}
