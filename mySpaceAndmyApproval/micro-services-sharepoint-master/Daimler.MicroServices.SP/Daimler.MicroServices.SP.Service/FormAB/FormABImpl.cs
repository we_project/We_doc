﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.Approver;
using Daimler.MicroServices.SP.BizObject.FormAB;
using log4net;
using Microsoft.SharePoint.Client;

namespace Daimler.MicroServices.SP.Service.FormAB {
   public class FormABImpl : IFormAB {
        private static readonly ILog log = LogManager.GetLogger(typeof(FormABImpl));
        public UserCredential uc { get; set; }

        public FormABImpl(UserCredential uc) {
            this.uc = uc;
        }
        ApproveResult IFormAB.ApprovalFormA(string user,bool approve, int ID, string comment, string StatusIndex, FormATaxTeamFields taxTeamField) {
            ApproveResult result = new ApproveResult();
            using (AbstractSharepoint sharepoint = new FormA(uc)) {
                ListItem item = sharepoint.GetListItemById(ID, new List<string> { 
                    FormAInfoFields.ID,
                    FormAInfoFields.StatusIndex,
                    FormAInfoFields.FlowPath,
                    FormAInfoFields.Tax,
                    FormAInfoFields._x0054_ax2,
                    FormAInfoFields.TaxManager,
                    FormAInfoFields.Controlling,
                    FormAInfoFields.Controlling2
                });

                if (item[FormAInfoFields.StatusIndex].ToString().Equals(StatusIndex)) {
                    int statusIndex = Convert.ToInt32(StatusIndex);
                    FormABWorkflow currentWorkflow = FormAWorkflowCollection.Default[statusIndex];
                    if (approve) {
                        #region update pending
                        switch (statusIndex) {
                            case 1:
                                item[FormAInfoFields.Pending] = item[FormAInfoFields.Tax] as FieldUserValue;
                                item[FormAInfoFields.Pending2] = item[FormAInfoFields._x0054_ax2] as FieldUserValue;
                                break;
                            #region ignore tax team
                            //case 2:
                            //    item[FormAInfoFields.Pending] = item[FormAInfoFields.TaxManager] as FieldUserValue;
                            //    item[FormAInfoFields.Pending2] = dj;

                            //    #region tax team
                            //    if (taxTeamField != null) {
                            //        item[FormAInfoFields.TaxTeamFields.CalculationMethod] = taxTeamField.CalculationMethod;
                            //        item[FormAInfoFields.TaxTeamFields.EstimatedIITAmountLabor] = taxTeamField.EstimatedIITAmountLabor;
                            //        item[FormAInfoFields.TaxTeamFields.EstimatedIITAmountOccasional] = taxTeamField.EstimatedIITAmountOccasional;
                            //        item[FormAInfoFields.TaxTeamFields.EstimatedInputAmount] = taxTeamField.EstimatedInputAmount;
                            //        item[FormAInfoFields.TaxTeamFields.EstimatedOutputAmount] = taxTeamField.EstimatedOutputAmount;
                            //        item[FormAInfoFields.TaxTeamFields.Note] = taxTeamField.Note;
                            //        item[FormAInfoFields.TaxTeamFields.VATRate] = taxTeamField.VATRate;
                            //    } else {
                            //        result.Success = false;
                            //        result.Msg = "Tax team need to input values";
                            //        return result;
                            //    }
                            //    #endregion
                            //    break; 
                            #endregion
                            case 3:
                                item[FormAInfoFields.Pending] = item[FormAInfoFields.Controlling] as FieldUserValue;
                                item[FormAInfoFields.Pending2] = item[FormAInfoFields.Controlling2] as FieldUserValue;
                                break;
                            case 4:
                                item[FormAInfoFields.Pending] = Pending.dj;
                                item[FormAInfoFields.Pending2] = Pending.dj;
                                break;
                            default:
                                break;
                        }
                        #endregion
                    } else {
                        item[FormAInfoFields.IsReject] = "1";
                    }
                    item[FormAInfoFields.IsStatusChange] = "1";
                    item[FormAInfoFields.NewComment] = comment;
                    FormABWorkflow nextWorkflow = approve ? FormAWorkflowCollection.Default[currentWorkflow.approvedTo] : FormAWorkflowCollection.Default[currentWorkflow.rejectTo];
                    item[FormAInfoFields.Status] = nextWorkflow.displayName;
                    item[FormAInfoFields.StatusIndex] = nextWorkflow.index.ToString();
                    item[FormAInfoFields.FlowPath] = nextWorkflow.NewFlowPath(item[FormAInfoFields.FlowPath].ToString());
                    item[FormAInfoFields.CurrentUser] = user;
                    item.Update();
                    sharepoint.context.ExecuteQuery();
                    result.Success = true;
                } else {
                    result.Success = false;
                    result.Msg = AllErrors.StatusChanged;
                }
            }
            return result;
        }


        ApproveResult IFormAB.ApprovalFormB(string user, bool approve, int ID, string comment, string StatusIndex, FormBTaxTeamFields taxTeamField) {
            ApproveResult result = new ApproveResult();
            using (AbstractSharepoint sharepoint = new FormB(uc)) {
                ListItem item = sharepoint.GetListItemById(ID, new List<string> { 
                    FormBInfoFields.ID,
                    FormBInfoFields.StatusIndex,
                    FormBInfoFields.FlowPath,
                    FormBInfoFields.Tax,
                    FormBInfoFields._x0054_ax2,
                    FormBInfoFields.TaxManager,
                });
                if (item[FormBInfoFields.StatusIndex].ToString().Equals(StatusIndex)) {
                    int statusIndex = Convert.ToInt32(StatusIndex);
                    FormABWorkflow currentWorkflow = FormBWorkflowCollection.Default[statusIndex];
                    if (approve) {
                        #region update pending
                        switch (statusIndex) {
                            case 1:
                                item[FormBInfoFields.Pending] = item[FormBInfoFields.Tax] as FieldUserValue;
                                item[FormBInfoFields.Pending2] = item[FormBInfoFields._x0054_ax2] as FieldUserValue;
                                break;
                            #region ignore tax team
                            //case 2:
                            //    item[FormBInfoFields.Pending] = item[FormBInfoFields.TaxManager] as FieldUserValue;
                            //    item[FormBInfoFields.Pending2] = dj;

                            //    #region tax team
                            //    if (taxTeamField != null) {
                            //        item[FormBInfoFields.TaxTeamFields.CalculationMethod] = taxTeamField.CalculationMethod;
                            //        item[FormBInfoFields.TaxTeamFields.ActualIITAmountLabor] = taxTeamField.ActualIITAmountLabor;
                            //        item[FormBInfoFields.TaxTeamFields.ActualIITAmountOccasional] = taxTeamField.ActualIITAmountOccasional;
                            //        item[FormBInfoFields.TaxTeamFields.ActualInputAmount] = taxTeamField.ActualInputAmount;
                            //        item[FormBInfoFields.TaxTeamFields.ActualOutputAmount] = taxTeamField.ActualOutputAmount;
                            //        item[FormBInfoFields.TaxTeamFields.Note] = taxTeamField.Note;
                            //        item[FormBInfoFields.TaxTeamFields.VATRate] = taxTeamField.VATRate;
                            //    } else {
                            //        result.Success = false;
                            //        result.Msg = AllErrors.TaxTeamInputIsNull;
                            //        return result;
                            //    }
                            //    #endregion
                            //    break; 
                            #endregion
                            case 3:
                                item[FormBInfoFields.Pending] = Pending.dj;
                                item[FormBInfoFields.Pending2] = Pending.dj;
                                break;
                            default:
                                break;
                        }
                        #endregion
                    } else {
                        item[FormBInfoFields.IsReject] = "1";
                    }
                    item[FormBInfoFields.IsStatusChange] = "1";
                    item[FormBInfoFields.NewComment] = comment;
                    FormABWorkflow nextWorkflow = approve ? FormBWorkflowCollection.Default[currentWorkflow.approvedTo] : FormBWorkflowCollection.Default[currentWorkflow.rejectTo];
                    log.Debug("NextWorkflow" + nextWorkflow.ToString());
                    item[FormBInfoFields.Status] = nextWorkflow.displayName;
                    item[FormBInfoFields.StatusIndex] = nextWorkflow.index.ToString();
                    item[FormBInfoFields.FlowPath] = nextWorkflow.NewFlowPath(item[FormAInfoFields.FlowPath].ToString());
                    item[FormBInfoFields.CurrentUser] = user;
                    item.Update();
                    sharepoint.context.ExecuteQuery();
                    result.Success = true;
                } else {
                    result.Success = false;
                    result.Msg = AllErrors.StatusChanged;
                }
            }
            return result;
        }
   }
}
