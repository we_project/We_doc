﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint.Client;

namespace Daimler.MicroServices.SP.Service.FormAB {
    class Pending {
        private Pending() { }
        private static FieldUserValue _dj = new FieldUserValue { LookupId = 31 };
        public static FieldUserValue dj {
            get {
                return _dj;
            }
        }
    }
}
