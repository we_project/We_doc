﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.Utils.Network;
using Proxys;

namespace Daimler.MicroServices.SP.Service.AccessManagement {
    public abstract class AMBase : AbstractSharepoint {
        public AMBase(UserCredential uc)
            : base(uc) {

        }
        public override string sharepointUrl {
            get {
                return SPConfig.Instance.AccessManagementUrl;
            }
        }


        private NintexWorkflowWSSoapClient _client;
        private static object lockClient = new object();
        protected NintexWorkflowWSSoapClient client {
            get {
                lock (lockClient) {
                    if (_client == null) {
                        _client = new NintexWorkflowWSSoapClient("BadgeNintexWorkflowWSSoap12");
                        _client.ClientCredentials.Windows.ClientCredential = GetCredential(uc);
                        _client.Endpoint.EndpointBehaviors.Add(new InspectorBehavior());
                    }
                }
                return _client;
            }
        }
        private static readonly string AssociateDataTemplateForReject = "<dfs:myFields xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:dfs=\"http://schemas.microsoft.com/office/infopath/2003/dataFormSolution\" xmlns:d=\"http://schemas.microsoft.com/office/infopath/2009/WSSList/dataFields\" xmlns:pc=\"http://schemas.microsoft.com/office/infopath/2007/PartnerControls\" xmlns:ma=\"http://schemas.microsoft.com/office/2009/metadata/properties/metaAttributes\" xmlns:q=\"http://schemas.microsoft.com/office/infopath/2009/WSSList/queryFields\" xmlns:dms=\"http://schemas.microsoft.com/office/2009/documentManagement/types\" xmlns:xd=\"http://schemas.microsoft.com/office/infopath/2003\"><dfs:queryFields /><dfs:dataFields><d:SharePointListItem_RW><d:Reject_x0020_Comments>{0}</d:Reject_x0020_Comments></d:SharePointListItem_RW></dfs:dataFields></dfs:myFields>";

        protected string GetRejectAssociationData(string comment) {
            if (string.IsNullOrWhiteSpace(comment)) {
                return null;
            }
            string xml = string.Format(AssociateDataTemplateForReject, comment);
            return xml;
        }

    }
}
