﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.AccessManagement;
using log4net;
using SP.Data;

namespace Daimler.MicroServices.SP.Service.AccessManagement {
    /// <summary>
    /// Use proxy class
    /// </summary>
    public class VisitorBadge : AMBaseNew {
        public VisitorBadge(UserCredential uc)
            : base(uc) {

        }
        private static readonly ILog log = LogManager.GetLogger(typeof(VisitorBadge));
        public static string ListName = "Visitor Badge Application";

        ///// <summary>
        ///// use restful api
        ///// </summary>
        ///// <returns></returns>
        //public int GetPendingCountNew() {
        //    StringBuilder sb = new StringBuilder();
        //    sb.AppendFormat("{0}/_api/web/Lists/getbytitle('{1}')/items?$filter=", Config.ConfigManager.SharepointSecion.AccessManagementUrl.TrimEnd('/'), ListName);
        //    #region pending CCM approval
        //    var Parameters = new {
        //        @PendingApproval = "@PendingApproval",
        //        @currentUserId = "@currentUserId",
        //        @PendingCCMApproval = "@PendingCCMApproval",
        //        @PendingSpecialAreaApproval = "@PendingSpecialAreaApproval"
        //    };
        //    sb.AppendFormat("(Status eq {0} and (CCApproverId eq {1} or CCDeputyId eq {1}))", Parameters.PendingCCMApproval, Parameters.currentUserId);
        //    sb.AppendFormat("or (Status eq {0} and ((SAStatus1 eq {1} and (SAApprover1Id eq {2} or SADeputy1Id eq {2})");
        //    sb.AppendFormat("");
        //    sb.AppendFormat(" and ((");
        //    sb.AppendFormat("", Parameters.PendingApproval, Parameters.currentUserId);
        //    sb.AppendFormat(") or (");
        //    sb.AppendFormat("SAStatus2 eq {0} and (SAApprover2Id eq {1} or SADeputy2Id eq {1})", Parameters.PendingApproval, Parameters.currentUserId);
        //    sb.AppendFormat(") or (");
        //    sb.AppendFormat("SAStatus3 eq {0} and (SAApprover3Id eq {1} or SADeputy3Id eq {1})", Parameters.PendingApproval, Parameters.currentUserId);
        //    sb.AppendFormat(") or (");
        //    sb.AppendFormat("SAStatus4 eq {0} and (SAApprover4Id eq {1} or SADeputy4Id eq {1})", Parameters.PendingApproval, Parameters.currentUserId);
        //    sb.AppendFormat(") or (");
        //    sb.AppendFormat("SAStatus5 eq {0} and (SAApprover5Id eq {1} or SADeputy5Id eq {1})", Parameters.PendingApproval, Parameters.currentUserId);
        //    sb.AppendFormat(") or (");
        //    sb.AppendFormat("SAStatus6 eq {0} and (SAApprover6Id eq {1} or SADeputy6Id eq {1})", Parameters.PendingApproval, Parameters.currentUserId);
        //    sb.AppendFormat(") or (");
        //    sb.AppendFormat("SAStatus7 eq {0} and (SAApprover7Id eq {1} or SADeputy7Id eq {1})", Parameters.PendingApproval, Parameters.currentUserId);
        //    sb.AppendFormat(")))");


        //    sb.AppendFormat("");
        //    #endregion
        //    sb.AppendFormat("&@PendingCCMApproval='{0}'", EIBMStatus.PendingCCMApproval);
        //    sb.AppendFormat("&@PendingSpecialAreaApproval='{0}'", EIBMStatus.PendingSpecialAreaApproval);
        //    sb.AppendFormat("&@PendingApproval='{0}'", SAStatus.PendingApproval);
        //    sb.AppendFormat("&{0}='{1}'", Parameters.currentUserId, CurrentUserId);
        //    sb.Append("&$select=Id,Status");

        //    string url = sb.ToString();
        //    log.Info(url);
        //    Uri uri = new Uri(url);

        //    Visitor_x0020_Badge_x0020_Application2ListItem[] idArray = apiData.Execute<Visitor_x0020_Badge_x0020_Application2ListItem>(uri, "GET", true).ToArray();
        //    log.Info(idArray);
        //    return idArray != null ? idArray.Length : 0;
        //}
        public int GetPendingCount() {
            log.Debug(new {
                listName = ListName,
                action = "GetPendingCount",
                currentUser = CurrentUserId
            });
            DateTime now = DateTime.Now;
            Func<Visitor_x0020_Badge_x0020_Application2ListItem, bool> isPending = item => {
                bool pending = false;

                // cc manager pending approval
                //      the item CCStatus is 'Pending Approval' (after this the workflow send email )
                //      the item Status is 'Pending Cost Center Manager Approval'
                //      current user is cc manager or (current user is cc deputy and is in delegate date)
                //bool pendingCCApproval = EIBMStatus.PendingCCMApproval.Equals(item.Status) && (
                //    CurrentUserId.Equals(item.CCApproverId)
                //    || (ccdupty != null && item.CCApproverId.Equals(ccdupty.Cost_x0020_Center_x0020_ManagerId) && CurrentUserId.Equals(item.CCDeputyId) && ccdupty.Delegate_x0020_From_x0020_Date.HasValue && ccdupty.Delegate_x0020_From_x0020_Date.Value < now && ccdupty.Delegate_x0020_To_x0020_Date.HasValue && ccdupty.Delegate_x0020_To_x0020_Date.Value > now));
                bool pendingCCApproval = EIBMStatus.PendingCCMApproval.Equals(item.Status) && (CurrentUserId.Equals(item.CCApproverId) || CurrentUserId.Equals(item.CCDeputyId));

                // special area pending approval
                //      the item Status is 'Pending Special Area Approval'
                //      current user is SAApprover1|SADeputy1 and SAStatus1 is 'Pending Approval'
                //      current user is SAApprover2|SADeputy2 and SAStatus2 is 'Pending Approval'
                //      current user is SAApprover3|SADeputy3 and SAStatus3 is 'Pending Approval'
                //      current user is SAApprover4|SADeputy4 and SAStatus4 is 'Pending Approval'
                //      current user is SAApprover5|SADeputy5 and SAStatus5 is 'Pending Approval'
                //      current user is SAApprover6|SADeputy6 and SAStatus6 is 'Pending Approval'
                //      current user is SAApprover7|SADeputy7 and SAStatus7 is 'Pending Approval'
                bool pendingSpecialAreaApproval =
                    EIBMStatus.PendingSpecialAreaApproval.Equals(item.Status) && (
                   SAStatus.PendingApproval.Equals(item.SAStatus1) && (CurrentUserId.Equals(item.SAApprover1Id) || CurrentUserId.Equals(item.SADeputy1Id))
                    || (SAStatus.PendingApproval.Equals(item.SAStatus2) && (CurrentUserId.Equals(item.SAApprover2Id) || CurrentUserId.Equals(item.SADeputy2Id)))
                    || (SAStatus.PendingApproval.Equals(item.SAStatus3) && (CurrentUserId.Equals(item.SAApprover3Id) || CurrentUserId.Equals(item.SADeputy3Id)))
                    || (SAStatus.PendingApproval.Equals(item.SAStatus4) && (CurrentUserId.Equals(item.SAApprover4Id) || CurrentUserId.Equals(item.SADeputy4Id)))
                    || (SAStatus.PendingApproval.Equals(item.SAStatus5) && (CurrentUserId.Equals(item.SAApprover5Id) || CurrentUserId.Equals(item.SADeputy5Id)))
                    || (SAStatus.PendingApproval.Equals(item.SAStatus6) && (CurrentUserId.Equals(item.SAApprover6Id) || CurrentUserId.Equals(item.SADeputy6Id)))
                    || (SAStatus.PendingApproval.Equals(item.SAStatus7) && (CurrentUserId.Equals(item.SAApprover7Id) || CurrentUserId.Equals(item.SADeputy7Id)))
                    );

                return pending || pendingCCApproval || pendingSpecialAreaApproval;
            };


            var simpleResult =
               listData.Visitor_x0020_Badge_x0020_Application2ListItems
               .AddQueryOption("$select", "Id,Application_x0020_Date,Application_x0020_Type,CCApproverId,CCDeputyId,Status,Employee_x0020_Name/Title,SAStatus1,SAApprover1Id,SADeputy1Id,SAStatus2,SAApprover2Id,SADeputy2Id,SAStatus3,SAApprover3Id,SADeputy3Id,SAStatus4,SAApprover4Id,SADeputy4Id,SAStatus5,SAApprover5Id,SADeputy5Id,SAStatus6,SAApprover6Id,SADeputy6Id,SAStatus7,SAApprover7Id,SADeputy7Id")
               .Expand("Employee_x0020_Name")
               .Where(isPending)
               .Select(item => new {
                   Id = item.Id,
                   EmployeeName = item.Employee_x0020_Name.Title,
                   ApplicationDate = item.Application_x0020_Date,
                   ApplicationType = item.Application_x0020_Type,
                   Status = item.Status,
                   CCApproverId = item.CCApproverId,
                   CCDeputyId = item.CCDeputyId
               });
            //simpleResult = simpleResult.Except(
            //    from item in simpleResult
            //    join d in ccduptys on new { p = item.CCApproverId, q = item.CCDeputyId } equals new { p = (int?)d.Cost_x0020_Center_x0020_Manager.Id, q = (int?)d.Cost_x0020_Center_x0020_Deputy.Id }
            //    where EIBMStatus.PendingCCMApproval.Equals(item.Status) && (d.Delegate_x0020_From_x0020_Date > now || d.Delegate_x0020_To_x0020_Date < now)
            //    select item
            //    );
            if (ccdeputys != null && ccdeputys.Count > 0) {
                // simpleResult = simpleResult.Except(
                //from item in simpleResult
                //join d in ccduptys on new { p = item.CCApproverId, q = item.CCDeputyId } equals new { p = d.Cost_x0020_Center_x0020_Manager, q = d.Cost_x0020_Center_x0020_Deputy }
                //where EIBMStatus.PendingCCMApproval.Equals(item.Status) && (d.Delegate_x0020_From_x0020_Date > now || d.Delegate_x0020_To_x0020_Date < now)
                //select item
                //);

                // select all deputy record
                // select valid deputy
                // get invalid deputy
                // except invalid deputy
                var allDeputyRecords = simpleResult.Where(b => EIBMStatus.PendingCCMApproval.Equals(b.Status) && CurrentUserId.Equals(b.CCDeputyId));
                var approversOfDeputy = ccdeputys.Select(c => c.Cost_x0020_Center_x0020_Manager);
                var validRecords = allDeputyRecords.Where(b => approversOfDeputy.Contains(b.CCApproverId));
                var invalidRecords = allDeputyRecords.Except(validRecords);
                simpleResult = simpleResult.Except(invalidRecords);
            }
            IEnumerable<VisitorBadgeItem> result =
               simpleResult
                .Select(item => new VisitorBadgeItem {
                    Id = item.Id,
                    EmployeeName = item.EmployeeName,
                    ApplicationDate = item.ApplicationDate,
                    ApplicationType = item.ApplicationType,
                    Status = item.Status
                });
            int count = result.Count();
            log.Debug(new {
                listName = ListName,
                action = "GetPendingCount",
                currentUser = CurrentUserId,
                result = count
            });
            return count;
        }
        public IEnumerable<VisitorBadgeItem> GetPendingList() {
            log.Debug(new {
                listName = ListName,
                action = "GetPendingList",
                currentUser = CurrentUserId
            });
            DateTime now = DateTime.Now;
            Func<Visitor_x0020_Badge_x0020_Application2ListItem, bool> isPending = item => {
                bool pending = false;

                // cc manager pending approval
                //      the item CCStatus is 'Pending Approval' (after this the workflow send email )
                //      the item Status is 'Pending Cost Center Manager Approval'
                //      current user is cc manager or (current user is cc deputy and is in delegate date)
                bool pendingCCApproval = EIBMStatus.PendingCCMApproval.Equals(item.Status) && (CurrentUserId.Equals(item.CCApproverId) || CurrentUserId.Equals(item.CCDeputyId));


                // special area pending approval
                //      the item Status is 'Pending Special Area Approval'
                //      current user is SAApprover1|SADeputy1 and SAStatus1 is 'Pending Approval'
                //      current user is SAApprover2|SADeputy2 and SAStatus2 is 'Pending Approval'
                //      current user is SAApprover3|SADeputy3 and SAStatus3 is 'Pending Approval'
                //      current user is SAApprover4|SADeputy4 and SAStatus4 is 'Pending Approval'
                //      current user is SAApprover5|SADeputy5 and SAStatus5 is 'Pending Approval'
                //      current user is SAApprover6|SADeputy6 and SAStatus6 is 'Pending Approval'
                //      current user is SAApprover7|SADeputy7 and SAStatus7 is 'Pending Approval'
                bool pendingSpecialAreaApproval =
                    EIBMStatus.PendingSpecialAreaApproval.Equals(item.Status) && (
                   SAStatus.PendingApproval.Equals(item.SAStatus1) && (CurrentUserId.Equals(item.SAApprover1Id) || CurrentUserId.Equals(item.SADeputy1Id))
                    || (SAStatus.PendingApproval.Equals(item.SAStatus2) && (CurrentUserId.Equals(item.SAApprover2Id) || CurrentUserId.Equals(item.SADeputy2Id)))
                    || (SAStatus.PendingApproval.Equals(item.SAStatus3) && (CurrentUserId.Equals(item.SAApprover3Id) || CurrentUserId.Equals(item.SADeputy3Id)))
                    || (SAStatus.PendingApproval.Equals(item.SAStatus4) && (CurrentUserId.Equals(item.SAApprover4Id) || CurrentUserId.Equals(item.SADeputy4Id)))
                    || (SAStatus.PendingApproval.Equals(item.SAStatus5) && (CurrentUserId.Equals(item.SAApprover5Id) || CurrentUserId.Equals(item.SADeputy5Id)))
                    || (SAStatus.PendingApproval.Equals(item.SAStatus6) && (CurrentUserId.Equals(item.SAApprover6Id) || CurrentUserId.Equals(item.SADeputy6Id)))
                    || (SAStatus.PendingApproval.Equals(item.SAStatus7) && (CurrentUserId.Equals(item.SAApprover7Id) || CurrentUserId.Equals(item.SADeputy7Id)))
                    );

                return pending || pendingCCApproval || pendingSpecialAreaApproval;
            };
            //IEnumerable<VisitorBadgeItem> simpleResult =
            //    listData.Visitor_x0020_Badge_x0020_Application2ListItems
            //    .AddQueryOption("$select", "*,Employee_x0020_Name/Title")
            //    .Expand("Employee_x0020_Name")
            //    .Where(isPending)
            //    .Select(item => new VisitorBadgeItem {
            //        Id = item.Id,
            //        EmployeeName = item.Employee_x0020_Name.Title,
            //        ApplicationDate = item.Application_x0020_Date,
            //        ApplicationType = item.Application_x0020_Type,
            //        Status = item.Status
            //    });
            var simpleResult =
               listData.Visitor_x0020_Badge_x0020_Application2ListItems
               .AddQueryOption("$select", "Id,Application_x0020_Date,Application_x0020_Type,CCApproverId,CCDeputyId,Status,Employee_x0020_Name/Title,SAStatus1,SAApprover1Id,SADeputy1Id,SAStatus2,SAApprover2Id,SADeputy2Id,SAStatus3,SAApprover3Id,SADeputy3Id,SAStatus4,SAApprover4Id,SADeputy4Id,SAStatus5,SAApprover5Id,SADeputy5Id,SAStatus6,SAApprover6Id,SADeputy6Id,SAStatus7,SAApprover7Id,SADeputy7Id")
               .Expand("Employee_x0020_Name")
               .Where(isPending)
               .Select(item => new {
                   Id = item.Id,
                   EmployeeName = item.Employee_x0020_Name.Title,
                   ApplicationDate = item.Application_x0020_Date,
                   ApplicationType = item.Application_x0020_Type,
                   Status = item.Status,
                   CCApproverId = item.CCApproverId,
                   CCDeputyId = item.CCDeputyId
               });
            //simpleResult = simpleResult.Except(
            //    from item in simpleResult
            //    join d in ccduptys on new { p = item.CCApproverId, q = item.CCDeputyId } equals new { p = (int?)d.Cost_x0020_Center_x0020_Manager.Id, q = (int?)d.Cost_x0020_Center_x0020_Deputy.Id }
            //    where EIBMStatus.PendingCCMApproval.Equals(item.Status) && (d.Delegate_x0020_From_x0020_Date > now || d.Delegate_x0020_To_x0020_Date < now)
            //    select item
            //    );
            if (ccdeputys != null && ccdeputys.Count > 0) {
                // simpleResult = simpleResult.Except(
                //from item in simpleResult
                //join d in ccduptys on new { p = item.CCApproverId, q = item.CCDeputyId } equals new { p = d.Cost_x0020_Center_x0020_Manager, q = d.Cost_x0020_Center_x0020_Deputy }
                //where EIBMStatus.PendingCCMApproval.Equals(item.Status) && (d.Delegate_x0020_From_x0020_Date > now || d.Delegate_x0020_To_x0020_Date < now)
                //select item
                //);

                // select all deputy record
                // select valid deputy
                // get invalid deputy
                // except invalid deputy
                var allDeputyRecords = simpleResult.Where(b => EIBMStatus.PendingCCMApproval.Equals(b.Status) && CurrentUserId.Equals(b.CCDeputyId));
                var approversOfDeputy = ccdeputys.Select(c => c.Cost_x0020_Center_x0020_Manager);
                var validRecords = allDeputyRecords.Where(b => approversOfDeputy.Contains(b.CCApproverId));
                var invalidRecords = allDeputyRecords.Except(validRecords);
                simpleResult = simpleResult.Except(invalidRecords);
            }
            IEnumerable<VisitorBadgeItem> result =
               simpleResult
                .Select(item => new VisitorBadgeItem {
                    Id = item.Id,
                    EmployeeName = item.EmployeeName,
                    ApplicationDate = item.ApplicationDate,
                    ApplicationType = item.ApplicationType,
                    Status = item.Status
                });
            log.Debug(new {
                listName = ListName,
                action = "GetPendingList",
                currentUser = CurrentUserId,
                simpleResult = result
            });
            return result;
        }


        public void GetListItems() {
            foreach (var item in listData.Intern_x0020_badge_x0020_ApplicationListItems) {
                log.Debug(item.ID);
            }
        }


        public async Task<Visitor_x0020_Badge_x0020_Application2ListItem> GetItem(int ID) {
            Visitor_x0020_Badge_x0020_Application2ListItem result = null;
            Visitor_x0020_Badge_x0020_Application2ListItem lookup = null;
            Visitor_x0020_Badge_x0020_Application2ListItem lookup1 = null;
            Visitor_x0020_Badge_x0020_Application2ListItem lookup2 = null;
            Visitor_x0020_Badge_x0020_Application2ListItem lookup3 = null;

            Task t1 = Task.Factory.StartNew(() => {
                result = listData.Visitor_x0020_Badge_x0020_Application2ListItems.Where(item => ID.Equals(item.Id)).SingleOrDefault();
            });
            Task t2 = Task.Factory.StartNew(() => {
                lookup = CreateListData().Visitor_x0020_Badge_x0020_Application2ListItems
                 .AddQueryOption("$select", string.Join(",",
                     EIBMFields.Id,
                     string.Join("/", EIBMFields.Employee_x0020_Name, EIBMFields.Title),
                     string.Join("/", EIBMFields.Site_x0020_Name, EIBMFields.Title),
                     string.Join("/", EIBMFields.Location_x0020_1, EIBMFields.Title),
                     string.Join("/", EIBMFields.Location_x0020_2, EIBMFields.Title),
                     string.Join("/", EIBMFields.Location_x0020_3, EIBMFields.Title),
                     string.Join("/", EIBMFields.Location_x0020_4, EIBMFields.Title),
                     string.Join("/", EIBMFields.Location_x0020_5, EIBMFields.Title),
                     string.Join("/", EIBMFields.Location_x0020_6, EIBMFields.Title),
                     string.Join("/", EIBMFields.Location_x0020_7, EIBMFields.Title)
                     ))
                 .Expand(string.Join(",",
                    EIBMFields.Employee_x0020_Name,
                    EIBMFields.Site_x0020_Name,
                    EIBMFields.Location_x0020_1,
                    EIBMFields.Location_x0020_2,
                    EIBMFields.Location_x0020_3,
                    EIBMFields.Location_x0020_4,
                    EIBMFields.Location_x0020_5,
                    EIBMFields.Location_x0020_6,
                    EIBMFields.Location_x0020_7))
                 .Where(item => ID.Equals(item.Id)).SingleOrDefault();
            });
            Task t3 = Task.Factory.StartNew(() => {
                lookup1 = CreateListData().Visitor_x0020_Badge_x0020_Application2ListItems
                 .AddQueryOption("$select", string.Join(",",
                         EIBMFields.Id,
                     string.Join("/", EIBMFields.Area_x0020_1, EIBMFields.Id),
                     string.Join("/", EIBMFields.Area_x0020_1, EIBMFields.Title),
                     string.Join("/", EIBMFields.Area_x0020_2, EIBMFields.Id),
                     string.Join("/", EIBMFields.Area_x0020_2, EIBMFields.Title),
                     string.Join("/", EIBMFields.Area_x0020_3, EIBMFields.Id),
                     string.Join("/", EIBMFields.Area_x0020_3, EIBMFields.Title),
                     string.Join("/", EIBMFields.Area_x0020_4, EIBMFields.Id),
                     string.Join("/", EIBMFields.Area_x0020_4, EIBMFields.Title),
                     string.Join("/", EIBMFields.Area_x0020_5, EIBMFields.Id),
                     string.Join("/", EIBMFields.Area_x0020_5, EIBMFields.Title),
                     string.Join("/", EIBMFields.Area_x0020_6, EIBMFields.Id),
                     string.Join("/", EIBMFields.Area_x0020_6, EIBMFields.Title),
                     string.Join("/", EIBMFields.Area_x0020_7, EIBMFields.Id),
                     string.Join("/", EIBMFields.Area_x0020_7, EIBMFields.Title)))
                 .Expand(string.Join(",",
                         EIBMFields.Area_x0020_1,
                         EIBMFields.Area_x0020_2,
                         EIBMFields.Area_x0020_3,
                         EIBMFields.Area_x0020_4,
                         EIBMFields.Area_x0020_5,
                         EIBMFields.Area_x0020_6,
                         EIBMFields.Area_x0020_7))
                 .Where(item => ID.Equals(item.Id)).SingleOrDefault();
            });
            Task t4 = Task.Factory.StartNew(() => {
                lookup2 = CreateListData().Visitor_x0020_Badge_x0020_Application2ListItems
                 .AddQueryOption("$select", string.Join(",",
                         EIBMFields.Id,
                     string.Join("/", EIBMFields.CCApprover, EIBMFields.Id),
                     string.Join("/", EIBMFields.CCApprover, EIBMFields.Title),
                     string.Join("/", EIBMFields.SAApprover1, EIBMFields.Id),
                     string.Join("/", EIBMFields.SAApprover1, EIBMFields.Title),
                     string.Join("/", EIBMFields.SAApprover2, EIBMFields.Id),
                     string.Join("/", EIBMFields.SAApprover2, EIBMFields.Title),
                     string.Join("/", EIBMFields.SAApprover3, EIBMFields.Id),
                     string.Join("/", EIBMFields.SAApprover3, EIBMFields.Title),
                     string.Join("/", EIBMFields.SAApprover4, EIBMFields.Id),
                     string.Join("/", EIBMFields.SAApprover4, EIBMFields.Title),
                     string.Join("/", EIBMFields.SAApprover5, EIBMFields.Id),
                     string.Join("/", EIBMFields.SAApprover5, EIBMFields.Title),
                     string.Join("/", EIBMFields.SAApprover6, EIBMFields.Id),
                     string.Join("/", EIBMFields.SAApprover6, EIBMFields.Title),
                     string.Join("/", EIBMFields.SAApprover7, EIBMFields.Id),
                     string.Join("/", EIBMFields.SAApprover7, EIBMFields.Title)))
                 .Expand(string.Join(",",
                         EIBMFields.CCApprover,
                         EIBMFields.SAApprover1,
                         EIBMFields.SAApprover2,
                         EIBMFields.SAApprover3,
                         EIBMFields.SAApprover4,
                         EIBMFields.SAApprover5,
                         EIBMFields.SAApprover6,
                         EIBMFields.SAApprover7))
                 .Where(item => ID.Equals(item.Id)).SingleOrDefault();
            });
            Task t5 = Task.Factory.StartNew(() => {
                lookup3 = CreateListData().Visitor_x0020_Badge_x0020_Application2ListItems
                 .AddQueryOption("$select", string.Join(",",
                         EIBMFields.Id,
                     string.Join("/", EIBMFields.CCDeputy, EIBMFields.Id),
                     string.Join("/", EIBMFields.CCDeputy, EIBMFields.Title),
                     string.Join("/", EIBMFields.SADeputy1, EIBMFields.Id),
                     string.Join("/", EIBMFields.SADeputy1, EIBMFields.Title),
                     string.Join("/", EIBMFields.SADeputy2, EIBMFields.Id),
                     string.Join("/", EIBMFields.SADeputy2, EIBMFields.Title),
                     string.Join("/", EIBMFields.SADeputy3, EIBMFields.Id),
                     string.Join("/", EIBMFields.SADeputy3, EIBMFields.Title),
                     string.Join("/", EIBMFields.SADeputy4, EIBMFields.Id),
                     string.Join("/", EIBMFields.SADeputy4, EIBMFields.Title),
                     string.Join("/", EIBMFields.SADeputy5, EIBMFields.Id),
                     string.Join("/", EIBMFields.SADeputy5, EIBMFields.Title),
                     string.Join("/", EIBMFields.SADeputy6, EIBMFields.Id),
                     string.Join("/", EIBMFields.SADeputy6, EIBMFields.Title),
                     string.Join("/", EIBMFields.SADeputy7, EIBMFields.Id),
                     string.Join("/", EIBMFields.SADeputy7, EIBMFields.Title)))
                 .Expand(string.Join(",",
                         EIBMFields.CCDeputy,
                         EIBMFields.SADeputy1,
                         EIBMFields.SADeputy2,
                         EIBMFields.SADeputy3,
                         EIBMFields.SADeputy4,
                         EIBMFields.SADeputy5,
                         EIBMFields.SADeputy6,
                         EIBMFields.SADeputy7))
                 .Where(item => ID.Equals(item.Id)).SingleOrDefault();
            });
            await Task.WhenAll(t1, t2, t3);
            result.Employee_x0020_Name = lookup != null ? lookup.Employee_x0020_Name : null;
            result.Site_x0020_Name = lookup != null ? lookup.Site_x0020_Name : null;
            result.Location_x0020_1 = lookup != null ? lookup.Location_x0020_1 : null;
            result.Location_x0020_2 = lookup != null ? lookup.Location_x0020_2 : null;
            result.Location_x0020_3 = lookup != null ? lookup.Location_x0020_3 : null;
            result.Location_x0020_4 = lookup != null ? lookup.Location_x0020_4 : null;
            result.Location_x0020_5 = lookup != null ? lookup.Location_x0020_5 : null;
            result.Location_x0020_6 = lookup != null ? lookup.Location_x0020_6 : null;
            result.Location_x0020_7 = lookup != null ? lookup.Location_x0020_7 : null;
            result.Area_x0020_1 = lookup1 != null ? lookup1.Area_x0020_1 : null;
            result.Area_x0020_2 = lookup1 != null ? lookup1.Area_x0020_2 : null;
            result.Area_x0020_3 = lookup1 != null ? lookup1.Area_x0020_3 : null;
            result.Area_x0020_4 = lookup1 != null ? lookup1.Area_x0020_4 : null;
            result.Area_x0020_5 = lookup1 != null ? lookup1.Area_x0020_5 : null;
            result.Area_x0020_6 = lookup1 != null ? lookup1.Area_x0020_6 : null;
            result.Area_x0020_7 = lookup1 != null ? lookup1.Area_x0020_7 : null;
            result.CCApprover = lookup2 != null ? lookup2.CCApprover : null;
            result.SAApprover1 = lookup2 != null ? lookup2.SAApprover1 : null;
            result.SAApprover2 = lookup2 != null ? lookup2.SAApprover2 : null;
            result.SAApprover3 = lookup2 != null ? lookup2.SAApprover3 : null;
            result.SAApprover4 = lookup2 != null ? lookup2.SAApprover4 : null;
            result.SAApprover5 = lookup2 != null ? lookup2.SAApprover5 : null;
            result.SAApprover6 = lookup2 != null ? lookup2.SAApprover6 : null;
            result.SAApprover7 = lookup2 != null ? lookup2.SAApprover7 : null;
            result.CCDeputy = lookup3 != null ? lookup3.CCDeputy : null;
            result.SADeputy1 = lookup3 != null ? lookup3.SADeputy1 : null;
            result.SADeputy2 = lookup3 != null ? lookup3.SADeputy2 : null;
            result.SADeputy3 = lookup3 != null ? lookup3.SADeputy3 : null;
            result.SADeputy4 = lookup3 != null ? lookup3.SADeputy4 : null;
            result.SADeputy5 = lookup3 != null ? lookup3.SADeputy5 : null;
            result.SADeputy6 = lookup3 != null ? lookup3.SADeputy6 : null;
            result.SADeputy7 = lookup3 != null ? lookup3.SADeputy7 : null;
            return result;
        }


        public void Approval(int itemId, VisitorBadgeApprovalWorkflow wf, string comment) {
            string wfName = SPConfig.BadgeWorkflows.GetFrom(wf);
            log.Debug(new {
                uc = uc,
                approvalItem = itemId,
                workflow = wf,
                wfName = wfName,
                comment = comment
            });
            Guid approvalResult = client.StartWorkflowOnListItem(itemId, ListName, wfName, GetRejectAssociationData(comment));
            log.Debug(new {
                uc = uc,
                approvalItem = itemId,
                workflow = wf,
                wfName = wfName,
                comment = comment,
                approvalResult = approvalResult
            });
        }

        // cost center manager reject
        public void CostCenterManagerReject(int itemId, string comment) {
            //int itemId, string listName, string workflowName, string associationData
            Guid rejectResult = client.StartWorkflowOnListItem(itemId, ListName, SPConfig.Instance.BadgeWorkflowNames.EIBMCCManagerRejection, GetRejectAssociationData(comment));
            log.Debug(rejectResult);
        }

        public async Task<VisitorBadgeApproveResult> ApprovalAsync(Visitor_x0020_Badge_x0020_Application2ListItem item, VisitorBadgeApprovalInfo info) {
            Task<VisitorBadgeApproveResult> t = Task.Factory.StartNew<VisitorBadgeApproveResult>(() => {
                return Approval(item, info);
            });
            return await t;
        }
        public VisitorBadgeApproveResult Approval(Visitor_x0020_Badge_x0020_Application2ListItem item, VisitorBadgeApprovalInfo info) {
            VisitorBadgeApproveResult result = new VisitorBadgeApproveResult();
            result.info = info;
            try {
                string wfName = SPConfig.BadgeWorkflows.GetFrom(info.wf);
                if (string.IsNullOrWhiteSpace(wfName)) {
                    result.msg = ApproveFailedReason.NotSupportedWorkflow;
                }
                string associationData = GetRejectAssociationData(info.comment);
                log.Debug(new {
                    uc = uc,
                    approvalItem = item.Id,
                    workflow = info.wf,
                    wfName = wfName,
                    comment = info.comment,
                    associationData = associationData
                });
                if (canApproval(item, info.wf)) {
                    Guid approvalResult = client.StartWorkflowOnListItem(item.Id, ListName, wfName, associationData);
                    result.success = true;
                    log.Debug(new {
                        uc = uc,
                        approvalItem = item.Id,
                        workflow = info.wf,
                        wfName = wfName,
                        comment = info.comment,
                        associationData = associationData,
                        approvalResult = approvalResult
                    });

                } else {
                    log.Debug(new {
                        uc = uc,
                        approvalItem = item.Id,
                        workflow = info.wf,
                        wfName = wfName,
                        comment = info.comment,
                        associationData = associationData,
                        approvalResult = "can't approval"
                    });
                    result.msg = ApproveFailedReason.NotApproverOrApprovedYet;
                }
            } catch (Exception ex) {
                result.msg = ApproveFailedReason.Other;
                log.Error(this, ex);
            }

            return result;
        }
        #region can approve
        public bool canApproval(Visitor_x0020_Badge_x0020_Application2ListItem item, VisitorBadgeApprovalWorkflow wf) {
            switch (wf) {

                case VisitorBadgeApprovalWorkflow.CCManagerApproval:
                case VisitorBadgeApprovalWorkflow.CCManagerRejection:
                    return PendingCCApproval(item);
                case VisitorBadgeApprovalWorkflow.SpecialArea1Approval:
                case VisitorBadgeApprovalWorkflow.SpecialArea1Rejection:
                    return PendingSA1Approval(item);
                case VisitorBadgeApprovalWorkflow.SpecialArea2Approval:
                case VisitorBadgeApprovalWorkflow.SpecialArea2Rejection:
                    return PendingSA2Approval(item);
                case VisitorBadgeApprovalWorkflow.SpecialArea3Approval:
                case VisitorBadgeApprovalWorkflow.SpecialArea3Rejection:
                    return PendingSA3Approval(item);
                case VisitorBadgeApprovalWorkflow.SpecialArea4Approval:
                case VisitorBadgeApprovalWorkflow.SpecialArea4Rejection:
                    return PendingSA4Approval(item);
                case VisitorBadgeApprovalWorkflow.SpecialArea5Approval:
                case VisitorBadgeApprovalWorkflow.SpecialArea5Rejection:
                    return PendingSA5Approval(item);
                case VisitorBadgeApprovalWorkflow.SpecialArea6Approval:
                case VisitorBadgeApprovalWorkflow.SpecialArea6Rejection:
                    return PendingSA6Approval(item);
                case VisitorBadgeApprovalWorkflow.SpecialArea7Approval:
                case VisitorBadgeApprovalWorkflow.SpecialArea7Rejection:
                    return PendingSA7Approval(item);
                default:
                    return false;
            }
        }

        private bool PendingSA7Approval(Visitor_x0020_Badge_x0020_Application2ListItem item) {
            bool pendingSpecialAreaApproval =
                   EIBMStatus.PendingSpecialAreaApproval.Equals(item.Status) && (
                   (SAStatus.PendingApproval.Equals(item.SAStatus7) && (CurrentUserId.Equals(item.SAApprover7Id) || CurrentUserId.Equals(item.SADeputy7Id)))
                   );
            return pendingSpecialAreaApproval;
        }

        private bool PendingSA6Approval(Visitor_x0020_Badge_x0020_Application2ListItem item) {
            bool pendingSpecialAreaApproval =
                    EIBMStatus.PendingSpecialAreaApproval.Equals(item.Status) && (
                     (SAStatus.PendingApproval.Equals(item.SAStatus6) && (CurrentUserId.Equals(item.SAApprover6Id) || CurrentUserId.Equals(item.SADeputy6Id)))
                    );

            return pendingSpecialAreaApproval;
        }

        private bool PendingSA5Approval(Visitor_x0020_Badge_x0020_Application2ListItem item) {
            bool pendingSpecialAreaApproval =
                    EIBMStatus.PendingSpecialAreaApproval.Equals(item.Status) && (
                    (SAStatus.PendingApproval.Equals(item.SAStatus5) && (CurrentUserId.Equals(item.SAApprover5Id) || CurrentUserId.Equals(item.SADeputy5Id)))
                    );

            return pendingSpecialAreaApproval;
        }

        private bool PendingSA4Approval(Visitor_x0020_Badge_x0020_Application2ListItem item) {
            bool pendingSpecialAreaApproval =
                   EIBMStatus.PendingSpecialAreaApproval.Equals(item.Status) && (
                    (SAStatus.PendingApproval.Equals(item.SAStatus4) && (CurrentUserId.Equals(item.SAApprover4Id) || CurrentUserId.Equals(item.SADeputy4Id)))
                   );

            return pendingSpecialAreaApproval;
        }

        private bool PendingSA3Approval(Visitor_x0020_Badge_x0020_Application2ListItem item) {
            bool pendingSpecialAreaApproval =
                   EIBMStatus.PendingSpecialAreaApproval.Equals(item.Status) && (
                    (SAStatus.PendingApproval.Equals(item.SAStatus3) && (CurrentUserId.Equals(item.SAApprover3Id) || CurrentUserId.Equals(item.SADeputy3Id)))
                   );

            return pendingSpecialAreaApproval;
        }

        private bool PendingSA2Approval(Visitor_x0020_Badge_x0020_Application2ListItem item) {
            bool pendingSpecialAreaApproval =
                   EIBMStatus.PendingSpecialAreaApproval.Equals(item.Status) && (
                    (SAStatus.PendingApproval.Equals(item.SAStatus2) && (CurrentUserId.Equals(item.SAApprover2Id) || CurrentUserId.Equals(item.SADeputy2Id)))
                   );

            return pendingSpecialAreaApproval;
        }

        private bool PendingSA1Approval(Visitor_x0020_Badge_x0020_Application2ListItem item) {
            bool pendingSpecialAreaApproval =
                   EIBMStatus.PendingSpecialAreaApproval.Equals(item.Status) && (
                  SAStatus.PendingApproval.Equals(item.SAStatus1) && (CurrentUserId.Equals(item.SAApprover1Id) || CurrentUserId.Equals(item.SADeputy1Id))
                   );

            return pendingSpecialAreaApproval;
        }

        private bool PendingCCApproval(Visitor_x0020_Badge_x0020_Application2ListItem item) {
            //DateTime now = DateTime.Now;
            //bool pendingCCApproval = EIBMStatus.PendingCCMApproval.Equals(item.Status) && (
            //      CurrentUserId.Equals(item.CCApproverId)
            //      || (ccdupty != null && item.CCApproverId.Equals(ccdupty.Cost_x0020_Center_x0020_ManagerId) && CurrentUserId.Equals(item.CCDeputyId) && ccdupty.Delegate_x0020_From_x0020_Date.HasValue && ccdupty.Delegate_x0020_From_x0020_Date.Value < now && ccdupty.Delegate_x0020_To_x0020_Date.HasValue && ccdupty.Delegate_x0020_To_x0020_Date.Value > now));
            //return pendingCCApproval;

            bool pendingCCApproval = EIBMStatus.PendingCCMApproval.Equals(item.Status) && (CurrentUserId.Equals(item.CCApproverId) || CurrentUserId.Equals(item.CCDeputyId));
            if (CurrentUserId.Equals(item.CCDeputyId) && ccdeputys != null && ccdeputys.Count > 0) {
                var approversOfDeputy = ccdeputys.Select(c => c.Cost_x0020_Center_x0020_Manager);
                pendingCCApproval = approversOfDeputy.Contains(item.CCApproverId);
            }
            return pendingCCApproval;
        }
        #endregion
    }
}
