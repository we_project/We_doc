﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.AccessManagement;
using Daimler.MicroServices.Utils;
using Daimler.MicroServices.Utils.Network.Sharepoint;
using log4net;
using Newtonsoft.Json.Linq;
using Proxys;

namespace Daimler.MicroServices.SP.Service.AccessManagement {
    /// <summary>
    /// Use restfull http only
    /// </summary>
    public class EIBMRestful : AMBase {

        static EIBMRestful() {
            _FieldsToLoad = ReflectionMethod.GetPropertyNames<EIBMInfo>();
            _FieldsToLoad.Remove("Area1Id");
            _FieldsToLoad.Remove("Area2Id");
            _FieldsToLoad.Remove("Area3Id");
            _FieldsToLoad.Remove("Area4Id");
            _FieldsToLoad.Remove("Area5Id");
            _FieldsToLoad.Remove("Area6Id");
            _FieldsToLoad.Remove("Area7Id");
            _FieldsToLoad.AddRange(new string[]{
            "Area_x0020_1Id",
            "Area_x0020_2Id",
            "Area_x0020_3Id",
            "Area_x0020_4Id",
            "Area_x0020_5Id",
            "Area_x0020_6Id",
            "Area_x0020_7Id"
            });
        }
        public EIBMRestful(UserCredential uc)
            : base(uc) {

        }
        private static readonly ILog log = LogManager.GetLogger(typeof(EIBMRestful));

        public static string ListName = "Employee/Intern Badge Management";

        public override string listName {
            get { return ListName; }
        }

        public override List<string> FieldsToLoad {
            get {
                return _FieldsToLoad;
            }
        }
        public static List<string> _FieldsToLoad { get; private set; }

        public int CurrentUserId { get; set; }
        public List<CostCenterDeputyProfileInfo> ccdeputys { get; set; }
        // cc manager pending approval
        //      the item CCStatus is 'Pending Approval' (after this the workflow send email )
        //      the item Status is 'Pending Cost Center Manager Approval'
        //      current user is cc manager or (current user is cc deputy and is in delegate date)
        //
        // special area pending approval
        //      the item Status is 'Pending Special Area Approval'
        //      current user is SAApprover1|SADeputy1 and SAStatus1 is 'Pending Approval'
        //      current user is SAApprover2|SADeputy2 and SAStatus2 is 'Pending Approval'
        //      current user is SAApprover3|SADeputy3 and SAStatus3 is 'Pending Approval'
        //      current user is SAApprover4|SADeputy4 and SAStatus4 is 'Pending Approval'
        //      current user is SAApprover5|SADeputy5 and SAStatus5 is 'Pending Approval'
        //      current user is SAApprover6|SADeputy6 and SAStatus6 is 'Pending Approval'
        //      current user is SAApprover7|SADeputy7 and SAStatus7 is 'Pending Approval'
        private string GetPendingFilterStr() {
            string filter = "$filter=" +
                "(" +
                RestfulUrlHelper.ConditionPart(EIBMFields.Status, RestfulUrlHelper.Eq, EIBMStatus.PendingCCMApproval) +
                    " and " + "(" +
                        RestfulUrlHelper.ConditionPart(EIBMFields.CCApprover + "/" + EIBMFields.ID, RestfulUrlHelper.Eq, CurrentUserId) +
                        " or " +
                        RestfulUrlHelper.ConditionPart(EIBMFields.CCDeputy + "/" + EIBMFields.ID, RestfulUrlHelper.Eq, CurrentUserId) +
                    ")" +
                ")"
                + " or " + "(" +
                RestfulUrlHelper.ConditionPart(EIBMFields.Status, RestfulUrlHelper.Eq, EIBMStatus.PendingSpecialAreaApproval) +
                    " and " + "(" +
                        RestfulUrlHelper.ConditionPart(EIBMFields.SAStatus1, RestfulUrlHelper.Eq, SAStatus.PendingApproval) +
                            " and " + "(" +
                                RestfulUrlHelper.ConditionPart(EIBMFields.SAApprover1 + "/" + EIBMFields.ID, RestfulUrlHelper.Eq, CurrentUserId) +
                                " or " +
                                RestfulUrlHelper.ConditionPart(EIBMFields.SADeputy1 + "/" + EIBMFields.ID, RestfulUrlHelper.Eq, CurrentUserId) +
                            ")" +
                        " or " + "(" +
                        RestfulUrlHelper.ConditionPart(EIBMFields.SAStatus2, RestfulUrlHelper.Eq, SAStatus.PendingApproval) +
                            " and " + "(" +
                                RestfulUrlHelper.ConditionPart(EIBMFields.SAApprover2 + "/" + EIBMFields.ID, RestfulUrlHelper.Eq, CurrentUserId) +
                                " or " +
                                RestfulUrlHelper.ConditionPart(EIBMFields.SADeputy2 + "/" + EIBMFields.ID, RestfulUrlHelper.Eq, CurrentUserId) +
                            ")" +
                        ")" +
                        " or " + "(" +
                        RestfulUrlHelper.ConditionPart(EIBMFields.SAStatus3, RestfulUrlHelper.Eq, SAStatus.PendingApproval) +
                            " and " + "(" +
                                RestfulUrlHelper.ConditionPart(EIBMFields.SAApprover3 + "/" + EIBMFields.ID, RestfulUrlHelper.Eq, CurrentUserId) +
                                " or " +
                                RestfulUrlHelper.ConditionPart(EIBMFields.SADeputy3 + "/" + EIBMFields.ID, RestfulUrlHelper.Eq, CurrentUserId) +
                            ")" +
                        ")" +
                        " or " + "(" +
                        RestfulUrlHelper.ConditionPart(EIBMFields.SAStatus4, RestfulUrlHelper.Eq, SAStatus.PendingApproval) +
                            " and " + "(" +
                                RestfulUrlHelper.ConditionPart(EIBMFields.SAApprover4 + "/" + EIBMFields.ID, RestfulUrlHelper.Eq, CurrentUserId) +
                                " or " +
                                RestfulUrlHelper.ConditionPart(EIBMFields.SADeputy4 + "/" + EIBMFields.ID, RestfulUrlHelper.Eq, CurrentUserId) +
                            ")" +
                        ")" +
                        " or " + "(" +
                        RestfulUrlHelper.ConditionPart(EIBMFields.SAStatus5, RestfulUrlHelper.Eq, SAStatus.PendingApproval) +
                            " and " + "(" +
                                RestfulUrlHelper.ConditionPart(EIBMFields.SAApprover5 + "/" + EIBMFields.ID, RestfulUrlHelper.Eq, CurrentUserId) +
                                " or " +
                                RestfulUrlHelper.ConditionPart(EIBMFields.SADeputy5 + "/" + EIBMFields.ID, RestfulUrlHelper.Eq, CurrentUserId) +
                            ")" +
                        ")" +
                        " or " + "(" +
                        RestfulUrlHelper.ConditionPart(EIBMFields.SAStatus6, RestfulUrlHelper.Eq, SAStatus.PendingApproval) +
                            " and " + "(" +
                                RestfulUrlHelper.ConditionPart(EIBMFields.SAApprover6 + "/" + EIBMFields.ID, RestfulUrlHelper.Eq, CurrentUserId) +
                                " or " +
                                RestfulUrlHelper.ConditionPart(EIBMFields.SADeputy6 + "/" + EIBMFields.ID, RestfulUrlHelper.Eq, CurrentUserId) +
                            ")" +
                        ")" +
                        " or " + "(" +
                        RestfulUrlHelper.ConditionPart(EIBMFields.SAStatus7, RestfulUrlHelper.Eq, SAStatus.PendingApproval) +
                            " and " + "(" +
                                RestfulUrlHelper.ConditionPart(EIBMFields.SAApprover7 + "/" + EIBMFields.ID, RestfulUrlHelper.Eq, CurrentUserId) +
                                " or " +
                                RestfulUrlHelper.ConditionPart(EIBMFields.SADeputy7 + "/" + EIBMFields.ID, RestfulUrlHelper.Eq, CurrentUserId) +
                            ")" +
                        ")" +
                    ")" +
                ")";
            return filter;
        }
        public int GetPendingCount() {
            log.Info(new {
                listName = ListName,
                action = "GetPendingCount",
                currentUser = CurrentUserId
            });

            string url = RestfulUrlHelper.GetListItemsUrl(sharepointUrl, listName);
            string select = RestfulUrlHelper.GetSelectPart(new List<string> {
                EIBMFields.ID,
                EIBMFields.Status,
                EIBMFields.CCApprover+"/"+EIBMFields.ID,
                EIBMFields.CCDeputy+"/"+EIBMFields.ID
            });
            string expand = RestfulUrlHelper.GetExpandPart(new List<string> {
                EIBMFields.CCApprover+"/"+EIBMFields.ID,
                EIBMFields.CCDeputy+"/"+EIBMFields.ID
            });

            url += "?" + select;
            url += "&" + expand;
            url += "&" + GetPendingFilterStr();
            log.Info(new { url = url });
            IEnumerable<EIBMInfoForCount> simpleResult = SPHttpHelper_Json.GetJson<EIBMInfoForCount>(url, GetNetworkCredential(url));
            log.Info(new { result = simpleResult });

            if (simpleResult != null && simpleResult.Count() > 0 && ccdeputys != null && ccdeputys.Count > 0) {
                // select all deputy record
                // select valid deputy
                // get invalid deputy
                // except invalid deputy
                var allDeputyRecords = simpleResult.Where(b => EIBMStatus.PendingCCMApproval.Equals(b.Status) && b.CCDeputy != null && CurrentUserId.Equals(b.CCDeputy.ID));
                var approversOfDeputy = ccdeputys.Select(c => c.Cost_x0020_Center_x0020_Manager);
                var validRecords = allDeputyRecords.Where(b => b.CCApprover != null && approversOfDeputy.Contains(b.CCApprover.ID));
                var invalidRecords = allDeputyRecords.Except(validRecords);
                simpleResult = simpleResult.Except(invalidRecords);
            }

            int count = simpleResult != null ? simpleResult.Count() : 0;
            log.Info(new {
                listName = ListName,
                action = "GetPendingCount",
                currentUser = CurrentUserId,
                result = count
            });
            return count;
        }

        public IEnumerable<EIBMItem> GetPendingList() {
            log.Info(new {
                listName = ListName,
                action = "GetPendingList",
                currentUser = CurrentUserId
            });
            string url = RestfulUrlHelper.GetListItemsUrl(sharepointUrl, listName);
            string select = RestfulUrlHelper.GetSelectPart(new List<string> {
                EIBMFields.ID,
                EIBMFields.Status,
                EIBMFields.Application_x0020_Date,
                EIBMFields.Application_x0020_Type,
                EIBMFields.Employee_x0020_Name+"/"+EIBMFields.Title,
                EIBMFields.CCApprover+"/"+EIBMFields.ID,
                EIBMFields.CCDeputy+"/"+EIBMFields.ID
            });
            string expand = RestfulUrlHelper.GetExpandPart(new List<string> {
                EIBMFields.CCApprover+"/"+EIBMFields.ID,
                EIBMFields.CCDeputy+"/"+EIBMFields.ID,
                EIBMFields.Employee_x0020_Name+"/"+EIBMFields.Title,
            });

            url += "?" + select;
            url += "&" + expand;
            url += "&" + GetPendingFilterStr();
            log.Info(new { url = url });
            IEnumerable<EIBMInfoForList> simpleResult = SPHttpHelper_Json.GetJson<EIBMInfoForList>(url, GetNetworkCredential(url));
            log.Info(new { result = simpleResult });

            if (simpleResult != null && simpleResult.Count() > 0 && ccdeputys != null && ccdeputys.Count > 0) {
                // select all deputy record
                // select valid deputy
                // get invalid deputy
                // except invalid deputy
                var allDeputyRecords = simpleResult.Where(b => EIBMStatus.PendingCCMApproval.Equals(b.Status) && b.CCDeputy != null && CurrentUserId.Equals(b.CCDeputy.ID));
                var approversOfDeputy = ccdeputys.Select(c => c.Cost_x0020_Center_x0020_Manager);
                var validRecords = allDeputyRecords.Where(b => b.CCApprover != null && approversOfDeputy.Contains(b.CCApprover.ID));
                var invalidRecords = allDeputyRecords.Except(validRecords);
                simpleResult = simpleResult.Except(invalidRecords);
            }
            IEnumerable<EIBMItem> result = simpleResult != null ?
               simpleResult
                .Select(item => new EIBMItem {
                    Id = item.ID,
                    EmployeeName = item.Employee_x0020_Name != null ? item.Employee_x0020_Name.Title : null,
                    ApplicationDate = item.Application_x0020_Date,
                    ApplicationType = item.Application_x0020_Type,
                    Status = item.Status
                }) : null;
            log.Info(new {
                listName = ListName,
                action = "GetPendingList",
                currentUser = CurrentUserId,
                simpleResult = result
            });
            return result;
        }

        public async Task<EIBMDetail1> GetItem(int ID) {
            string url = RestfulUrlHelper.GetListItemsUrl(sharepointUrl, listName) + string.Format("({0})", ID);

            #region basic info
            string select_basic = RestfulUrlHelper.GetSelectPart(new List<string> {
                EIBMFields.ID,
                EIBMFields.Status,
                EIBMFields.CCApprover+"/"+EIBMFields.ID,
                EIBMFields.CCDeputy+"/"+EIBMFields.ID,
                EIBMFields.Application_x0020_Date,
                EIBMFields.Application_x0020_Type,
                EIBMFields.Employee_x0020_Name+"/"+EIBMFields.ID,
                EIBMFields.Employee_x0020_Name+"/"+EIBMFields.Title,
                EIBMFields.Title,
                EIBMFields.Employee_x0020_Number,
                EIBMFields.Company,
                EIBMFields.Department,
                EIBMFields.Deskphone_x0020_Number,
                EIBMFields.Cost_x0020_Center,
                EIBMFields.Badge_x0020_Type,
                EIBMFields.Remarks,
                EIBMFields.NumOfLocation,
                EIBMFields.SAStatus1,
                EIBMFields.SAStatus2,
                EIBMFields.SAStatus3,
                EIBMFields.SAStatus4,
                EIBMFields.SAStatus5,
                EIBMFields.SAStatus6,
                EIBMFields.SAStatus7,
                EIBMFields.Always,
                EIBMFields.Always2,
                EIBMFields.Always3,
                EIBMFields.Always4,
                EIBMFields.Always5,
                EIBMFields.Always6,
                EIBMFields.Always7,
                EIBMFields.Date_x0020_From_x0020_1,
                EIBMFields.Date_x0020_From_x0020_2,
                EIBMFields.Date_x0020_From_x0020_3,
                EIBMFields.Date_x0020_From_x0020_4,
                EIBMFields.Date_x0020_From_x0020_5,
                EIBMFields.Date_x0020_From_x0020_6,
                EIBMFields.Date_x0020_From_x0020_7,
                EIBMFields.Date_x0020_To_x0020_1,
                EIBMFields.Date_x0020_To_x0020_2,
                EIBMFields.Date_x0020_To_x0020_3,
                EIBMFields.Date_x0020_To_x0020_4,
                EIBMFields.Date_x0020_To_x0020_5,
                EIBMFields.Date_x0020_To_x0020_6,
                EIBMFields.Date_x0020_To_x0020_7,
            });
            string expand_basic = RestfulUrlHelper.GetExpandPart(new List<string> {
                EIBMFields.CCApprover+"/"+EIBMFields.ID,
                EIBMFields.CCDeputy+"/"+EIBMFields.ID,
                EIBMFields.Employee_x0020_Name+"/"+EIBMFields.ID,
                EIBMFields.Employee_x0020_Name+"/"+EIBMFields.Title,
            });

            string url_basic = url + "?" + select_basic;
            url_basic += "&" + expand_basic;
            Task<EIBMDetail1> t1 = SPHttpHelper_Json.GetJsonObjAsync<EIBMDetail1>(url_basic, GetNetworkCredential(url_basic));
            #endregion

            #region Site & Location lookup info
            string select_location = RestfulUrlHelper.GetSelectPart(new List<string> {
                EIBMFields.ID,
                string.Join("/", EIBMFields.Site_x0020_Name, EIBMFields.Title),
                string.Join("/", EIBMFields.Location_x0020_1, EIBMFields.Title),
                string.Join("/", EIBMFields.Location_x0020_2, EIBMFields.Title),
                string.Join("/", EIBMFields.Location_x0020_3, EIBMFields.Title),
                string.Join("/", EIBMFields.Location_x0020_4, EIBMFields.Title),
                string.Join("/", EIBMFields.Location_x0020_5, EIBMFields.Title),
                string.Join("/", EIBMFields.Location_x0020_6, EIBMFields.Title),
                string.Join("/", EIBMFields.Location_x0020_7, EIBMFields.Title)
            });
            string expand_location = RestfulUrlHelper.GetExpandPart(new List<string> {
                string.Join("/", EIBMFields.Site_x0020_Name, EIBMFields.Title),
                string.Join("/", EIBMFields.Location_x0020_1, EIBMFields.Title),
                string.Join("/", EIBMFields.Location_x0020_2, EIBMFields.Title),
                string.Join("/", EIBMFields.Location_x0020_3, EIBMFields.Title),
                string.Join("/", EIBMFields.Location_x0020_4, EIBMFields.Title),
                string.Join("/", EIBMFields.Location_x0020_5, EIBMFields.Title),
                string.Join("/", EIBMFields.Location_x0020_6, EIBMFields.Title),
                string.Join("/", EIBMFields.Location_x0020_7, EIBMFields.Title)
            });

            string url_location = url + "?" + select_location;
            url_location += "&" + expand_location;
            Task<EIBMDetail1> t2 = SPHttpHelper_Json.GetJsonObjAsync<EIBMDetail1>(url_location, GetNetworkCredential(url_location));
            #endregion

            #region Area lookup info
            string select_area = RestfulUrlHelper.GetSelectPart(new List<string> {
                EIBMFields.ID,
                string.Join("/", EIBMFields.Area_x0020_1, EIBMFields.Title),
                string.Join("/", EIBMFields.Area_x0020_2, EIBMFields.Title),
                string.Join("/", EIBMFields.Area_x0020_3, EIBMFields.Title),
                string.Join("/", EIBMFields.Area_x0020_4, EIBMFields.Title),
                string.Join("/", EIBMFields.Area_x0020_5, EIBMFields.Title),
                string.Join("/", EIBMFields.Area_x0020_6, EIBMFields.Title),
                string.Join("/", EIBMFields.Area_x0020_7, EIBMFields.Title)
            });
            string expand_area = RestfulUrlHelper.GetExpandPart(new List<string> {
                string.Join("/", EIBMFields.Area_x0020_1, EIBMFields.Title),
                string.Join("/", EIBMFields.Area_x0020_2, EIBMFields.Title),
                string.Join("/", EIBMFields.Area_x0020_3, EIBMFields.Title),
                string.Join("/", EIBMFields.Area_x0020_4, EIBMFields.Title),
                string.Join("/", EIBMFields.Area_x0020_5, EIBMFields.Title),
                string.Join("/", EIBMFields.Area_x0020_6, EIBMFields.Title),
                string.Join("/", EIBMFields.Area_x0020_7, EIBMFields.Title)
            });

            string url_area = url + "?" + select_area;
            url_area += "&" + expand_area;
            //Task<EIBMDetail> t3 = SPHttpHelper_Json.GetJsonObjAsync<EIBMDetail>(url_area, GetNetworkCredential(url_area));
            Task<string> t3 = SPHttpHelper_Json.GetJsonAsync(url_area, GetNetworkCredential(url_area));
            #endregion

            #region Approver lookup info
            string select_approver = RestfulUrlHelper.GetSelectPart(new List<string> {
                EIBMFields.ID,
                string.Join("/", EIBMFields.SAApprover1, EIBMFields.ID),
                string.Join("/", EIBMFields.SAApprover1, EIBMFields.Title),
                string.Join("/", EIBMFields.SAApprover2, EIBMFields.ID),
                string.Join("/", EIBMFields.SAApprover2, EIBMFields.Title),
                string.Join("/", EIBMFields.SAApprover3, EIBMFields.ID),
                string.Join("/", EIBMFields.SAApprover3, EIBMFields.Title),
                string.Join("/", EIBMFields.SAApprover4, EIBMFields.ID),
                string.Join("/", EIBMFields.SAApprover4, EIBMFields.Title),
                string.Join("/", EIBMFields.SAApprover5, EIBMFields.ID),
                string.Join("/", EIBMFields.SAApprover5, EIBMFields.Title),
                string.Join("/", EIBMFields.SAApprover6, EIBMFields.ID),
                string.Join("/", EIBMFields.SAApprover6, EIBMFields.Title),
                string.Join("/", EIBMFields.SAApprover7, EIBMFields.ID),
                string.Join("/", EIBMFields.SAApprover7, EIBMFields.Title)
            });
            string expand_approver = RestfulUrlHelper.GetExpandPart(new List<string> {
                string.Join("/", EIBMFields.SAApprover1, EIBMFields.ID),
                string.Join("/", EIBMFields.SAApprover1, EIBMFields.Title),
                string.Join("/", EIBMFields.SAApprover2, EIBMFields.ID),
                string.Join("/", EIBMFields.SAApprover2, EIBMFields.Title),
                string.Join("/", EIBMFields.SAApprover3, EIBMFields.ID),
                string.Join("/", EIBMFields.SAApprover3, EIBMFields.Title),
                string.Join("/", EIBMFields.SAApprover4, EIBMFields.ID),
                string.Join("/", EIBMFields.SAApprover4, EIBMFields.Title),
                string.Join("/", EIBMFields.SAApprover5, EIBMFields.ID),
                string.Join("/", EIBMFields.SAApprover5, EIBMFields.Title),
                string.Join("/", EIBMFields.SAApprover6, EIBMFields.ID),
                string.Join("/", EIBMFields.SAApprover6, EIBMFields.Title),
                string.Join("/", EIBMFields.SAApprover7, EIBMFields.ID),
                string.Join("/", EIBMFields.SAApprover7, EIBMFields.Title)
            });

            string url_approver = url + "?" + select_approver;
            url_approver += "&" + expand_approver;
            Task<EIBMDetail> t4 = SPHttpHelper_Json.GetJsonObjAsync<EIBMDetail>(url_approver, GetNetworkCredential(url_approver));
            #endregion

            #region Deputy lookup info
            string select_deputy = RestfulUrlHelper.GetSelectPart(new List<string> {
                EIBMFields.ID,
                string.Join("/", EIBMFields.SADeputy1, EIBMFields.ID),
                string.Join("/", EIBMFields.SADeputy1, EIBMFields.Title),
                string.Join("/", EIBMFields.SADeputy2, EIBMFields.ID),
                string.Join("/", EIBMFields.SADeputy2, EIBMFields.Title),
                string.Join("/", EIBMFields.SADeputy3, EIBMFields.ID),
                string.Join("/", EIBMFields.SADeputy3, EIBMFields.Title),
                string.Join("/", EIBMFields.SADeputy4, EIBMFields.ID),
                string.Join("/", EIBMFields.SADeputy4, EIBMFields.Title),
                string.Join("/", EIBMFields.SADeputy5, EIBMFields.ID),
                string.Join("/", EIBMFields.SADeputy5, EIBMFields.Title),
                string.Join("/", EIBMFields.SADeputy6, EIBMFields.ID),
                string.Join("/", EIBMFields.SADeputy6, EIBMFields.Title),
                string.Join("/", EIBMFields.SADeputy7, EIBMFields.ID),
                string.Join("/", EIBMFields.SADeputy7, EIBMFields.Title)
            });
            string expand_deputy = RestfulUrlHelper.GetExpandPart(new List<string> {
                string.Join("/", EIBMFields.SADeputy1, EIBMFields.ID),
                string.Join("/", EIBMFields.SADeputy1, EIBMFields.Title),
                string.Join("/", EIBMFields.SADeputy2, EIBMFields.ID),
                string.Join("/", EIBMFields.SADeputy2, EIBMFields.Title),
                string.Join("/", EIBMFields.SADeputy3, EIBMFields.ID),
                string.Join("/", EIBMFields.SADeputy3, EIBMFields.Title),
                string.Join("/", EIBMFields.SADeputy4, EIBMFields.ID),
                string.Join("/", EIBMFields.SADeputy4, EIBMFields.Title),
                string.Join("/", EIBMFields.SADeputy5, EIBMFields.ID),
                string.Join("/", EIBMFields.SADeputy5, EIBMFields.Title),
                string.Join("/", EIBMFields.SADeputy6, EIBMFields.ID),
                string.Join("/", EIBMFields.SADeputy6, EIBMFields.Title),
                string.Join("/", EIBMFields.SADeputy7, EIBMFields.ID),
                string.Join("/", EIBMFields.SADeputy7, EIBMFields.Title)
            });

            string url_deputy = url + "?" + select_deputy;
            url_deputy += "&" + expand_deputy;
            Task<EIBMDetail> t5 = SPHttpHelper_Json.GetJsonObjAsync<EIBMDetail>(url_deputy, GetNetworkCredential(url_deputy));
            #endregion


            EIBMDetail1 result = null;
            EIBMDetail1 lookup = null;
            EIBMDetail lookup2 = null;
            EIBMDetail lookup3 = null;
            result = await t1;
            lookup = await t2;
            string areaJson = await t3;
            JObject areaObj = JObject.Parse(areaJson);
            lookup2 = await t4;
            lookup3 = await t5;
            result.Site_x0020_Name = lookup != null ? lookup.Site_x0020_Name : null;
            result.Location_x0020_1 = lookup != null ? lookup.Location_x0020_1 : null;
            result.Location_x0020_2 = lookup != null ? lookup.Location_x0020_2 : null;
            result.Location_x0020_3 = lookup != null ? lookup.Location_x0020_3 : null;
            result.Location_x0020_4 = lookup != null ? lookup.Location_x0020_4 : null;
            result.Location_x0020_5 = lookup != null ? lookup.Location_x0020_5 : null;
            result.Location_x0020_6 = lookup != null ? lookup.Location_x0020_6 : null;
            result.Location_x0020_7 = lookup != null ? lookup.Location_x0020_7 : null;
            result.Area_x0020_1 = GetArea(1, areaObj);
            result.Area_x0020_2 = GetArea(2, areaObj);
            result.Area_x0020_3 = GetArea(3, areaObj);
            result.Area_x0020_4 = GetArea(4, areaObj);
            result.Area_x0020_5 = GetArea(5, areaObj);
            result.Area_x0020_6 = GetArea(6, areaObj);
            result.Area_x0020_7 = GetArea(7, areaObj);


            result.SAApprover1Id = lookup2 != null && lookup2.SAApprover1 != null ? lookup2.SAApprover1.ID : null;
            result.SAApprover2Id = lookup2 != null && lookup2.SAApprover2 != null ? lookup2.SAApprover2.ID : null;
            result.SAApprover3Id = lookup2 != null && lookup2.SAApprover3 != null ? lookup2.SAApprover3.ID : null;
            result.SAApprover4Id = lookup2 != null && lookup2.SAApprover4 != null ? lookup2.SAApprover4.ID : null;
            result.SAApprover5Id = lookup2 != null && lookup2.SAApprover5 != null ? lookup2.SAApprover5.ID : null;
            result.SAApprover6Id = lookup2 != null && lookup2.SAApprover6 != null ? lookup2.SAApprover6.ID : null;
            result.SAApprover7Id = lookup2 != null && lookup2.SAApprover7 != null ? lookup2.SAApprover7.ID : null;
            result.SADeputy1Id = lookup3 != null && lookup3.SADeputy1 != null ? lookup3.SADeputy1.ID : null;
            result.SADeputy2Id = lookup3 != null && lookup3.SADeputy2 != null ? lookup3.SADeputy2.ID : null;
            result.SADeputy3Id = lookup3 != null && lookup3.SADeputy3 != null ? lookup3.SADeputy3.ID : null;
            result.SADeputy4Id = lookup3 != null && lookup3.SADeputy4 != null ? lookup3.SADeputy4.ID : null;
            result.SADeputy5Id = lookup3 != null && lookup3.SADeputy5 != null ? lookup3.SADeputy5.ID : null;
            result.SADeputy6Id = lookup3 != null && lookup3.SADeputy6 != null ? lookup3.SADeputy6.ID : null;
            result.SADeputy7Id = lookup3 != null && lookup3.SADeputy7 != null ? lookup3.SADeputy7.ID : null;
            return result;
        }
        public async Task<EIBMInfoForApproval> GetItemForApproval(int ID) {
            string url = RestfulUrlHelper.GetListItemsUrl(sharepointUrl, listName) + string.Format("({0})", ID);

            #region basic info
            string select_basic = RestfulUrlHelper.GetSelectPart(new List<string> {
                EIBMFields.ID,
                EIBMFields.Status,
                EIBMFields.CCApprover+EIBMFields.Id,
                EIBMFields.CCDeputy+EIBMFields.Id,
                EIBMFields.SAStatus1,
                EIBMFields.SAStatus2,
                EIBMFields.SAStatus3,
                EIBMFields.SAStatus4,
                EIBMFields.SAStatus5,
                EIBMFields.SAStatus6,
                EIBMFields.SAStatus7,
            });

            string url_basic = url + "?" + select_basic;
            Task<EIBMInfoForApproval> t1 = SPHttpHelper_Json.GetJsonObjAsync<EIBMInfoForApproval>(url_basic, GetNetworkCredential(url_basic));
            #endregion

            #region Approver lookup info
            string select_approver = RestfulUrlHelper.GetSelectPart(new List<string> {
                EIBMFields.ID,
                string.Join("", EIBMFields.SAApprover1, EIBMFields.Id),
                string.Join("", EIBMFields.SAApprover2, EIBMFields.Id),
                string.Join("", EIBMFields.SAApprover3, EIBMFields.Id),
                string.Join("", EIBMFields.SAApprover4, EIBMFields.Id),
                string.Join("", EIBMFields.SAApprover5, EIBMFields.Id),
                string.Join("", EIBMFields.SAApprover6, EIBMFields.Id),
                string.Join("", EIBMFields.SAApprover7, EIBMFields.Id),
            });
            string url_approver = url + "?" + select_approver;
            Task<EIBMInfoForApproval> t4 = SPHttpHelper_Json.GetJsonObjAsync<EIBMInfoForApproval>(url_approver, GetNetworkCredential(url_approver));
            #endregion

            #region Deputy lookup info
            string select_deputy = RestfulUrlHelper.GetSelectPart(new List<string> {
                EIBMFields.ID,
                string.Join("", EIBMFields.SADeputy1, EIBMFields.Id),
                string.Join("", EIBMFields.SADeputy2, EIBMFields.Id),
                string.Join("", EIBMFields.SADeputy3, EIBMFields.Id),
                string.Join("", EIBMFields.SADeputy4, EIBMFields.Id),
                string.Join("", EIBMFields.SADeputy5, EIBMFields.Id),
                string.Join("", EIBMFields.SADeputy6, EIBMFields.Id),
                string.Join("", EIBMFields.SADeputy7, EIBMFields.Id),
            });
            string url_deputy = url + "?" + select_deputy;
            Task<EIBMInfoForApproval> t5 = SPHttpHelper_Json.GetJsonObjAsync<EIBMInfoForApproval>(url_deputy, GetNetworkCredential(url_deputy));
            #endregion


            EIBMInfoForApproval result = null;
            EIBMInfoForApproval lookup2 = null;
            EIBMInfoForApproval lookup3 = null;
            result = await t1;
            lookup2 = await t4;
            lookup3 = await t5;
            result.SAApprover1Id = lookup2 != null ? lookup2.SAApprover1Id : null;
            result.SAApprover2Id = lookup2 != null ? lookup2.SAApprover2Id : null;
            result.SAApprover3Id = lookup2 != null ? lookup2.SAApprover3Id : null;
            result.SAApprover4Id = lookup2 != null ? lookup2.SAApprover4Id : null;
            result.SAApprover5Id = lookup2 != null ? lookup2.SAApprover5Id : null;
            result.SAApprover6Id = lookup2 != null ? lookup2.SAApprover6Id : null;
            result.SAApprover7Id = lookup2 != null ? lookup2.SAApprover7Id : null;
            result.SADeputy1Id = lookup3 != null ? lookup3.SADeputy1Id : null;
            result.SADeputy2Id = lookup3 != null ? lookup3.SADeputy2Id : null;
            result.SADeputy3Id = lookup3 != null ? lookup3.SADeputy3Id : null;
            result.SADeputy4Id = lookup3 != null ? lookup3.SADeputy4Id : null;
            result.SADeputy5Id = lookup3 != null ? lookup3.SADeputy5Id : null;
            result.SADeputy6Id = lookup3 != null ? lookup3.SADeputy6Id : null;
            result.SADeputy7Id = lookup3 != null ? lookup3.SADeputy7Id : null;
            return result;
        }
        private AreaInfo[] GetArea(int index, JObject areaObj) {
            IList<JToken> area = areaObj["d"][string.Format("Area_x0020_{0}", index)]["results"].Children().ToList();
            if (area != null && area.Count > 0) {
                return area.Select(j => j.ToObject<AreaInfo>()).ToArray();
            }
            return null;
        }

        public EIBMApproveResult Approval(EIBMInfoForApproval item, EIBMBadgeApprovalInfo info) {
            EIBMApproveResult result = new EIBMApproveResult();
            result.info = info;
            try {
                string wfName = SPConfig.BadgeWorkflows.GetFrom(info.wf);
                if (string.IsNullOrWhiteSpace(wfName)) {
                    result.msg = ApproveFailedReason.NotSupportedWorkflow;
                }
                string associationData = GetRejectAssociationData(info.comment);
                log.Info(new {
                    uc = uc,
                    approvalItem = item.ID,
                    workflow = info.wf,
                    wfName = wfName,
                    comment = info.comment,
                    associationData = associationData
                });
                if (canApproval(item, info.wf)) {
                    Guid approvalResult = client.StartWorkflowOnListItem(item.ID, ListName, wfName, associationData);
                    result.success = true;
                    log.Info(new {
                        uc = uc,
                        approvalItem = item.ID,
                        workflow = info.wf,
                        wfName = wfName,
                        comment = info.comment,
                        associationData = associationData,
                        approvalResult = approvalResult
                    });

                } else {
                    log.Info(new {
                        uc = uc,
                        approvalItem = item.ID,
                        workflow = info.wf,
                        wfName = wfName,
                        comment = info.comment,
                        associationData = associationData,
                        approvalResult = "can't approval"
                    });
                    result.msg = ApproveFailedReason.NotApproverOrApprovedYet;
                }
            } catch (Exception ex) {
                result.msg = ApproveFailedReason.Other;
                log.Error(this, ex);
            }

            return result;
        }

        public async Task<EIBMApproveResult> ApprovalAsync(EIBMInfoForApproval item, EIBMBadgeApprovalInfo info) {
            Task<EIBMApproveResult> t = Task.Factory.StartNew<EIBMApproveResult>(() => {
                return Approval(item, info);
            });
            return await t;
        }


        public bool canApproval(EIBMInfoForApproval item, EIBMApprovalWorkflow wf) {
            switch (wf) {
                case EIBMApprovalWorkflow.EIBMCCManagerApproval:
                case EIBMApprovalWorkflow.EIBMCCManagerRejection:
                    return PendingCCApproval(item);
                case EIBMApprovalWorkflow.EIBMSpecialArea1Approval:
                case EIBMApprovalWorkflow.EIBMSpecialArea1Rejection:
                    return PendingSA1Approval(item);
                case EIBMApprovalWorkflow.EIBMSpecialArea2Approval:
                case EIBMApprovalWorkflow.EIBMSpecialArea2Rejection:
                    return PendingSA2Approval(item);
                case EIBMApprovalWorkflow.EIBMSpecialArea3Approval:
                case EIBMApprovalWorkflow.EIBMSpecialArea3Rejection:
                    return PendingSA3Approval(item);
                case EIBMApprovalWorkflow.EIBMSpecialArea4Approval:
                case EIBMApprovalWorkflow.EIBMSpecialArea4Rejection:
                    return PendingSA4Approval(item);
                case EIBMApprovalWorkflow.EIBMSpecialArea5Approval:
                case EIBMApprovalWorkflow.EIBMSpecialArea5Rejection:
                    return PendingSA5Approval(item);
                case EIBMApprovalWorkflow.EIBMSpecialArea6Approval:
                case EIBMApprovalWorkflow.EIBMSpecialArea6Rejection:
                    return PendingSA6Approval(item);
                case EIBMApprovalWorkflow.EIBMSpecialArea7Approval:
                case EIBMApprovalWorkflow.EIBMSpecialArea7Rejection:
                    return PendingSA7Approval(item);
                default:
                    return false;
            }
        }

        private bool PendingCCApproval(EIBMInfoForApproval item) {
            bool pendingCCApproval = EIBMStatus.PendingCCMApproval.Equals(item.Status) && (CurrentUserId.Equals(item.CCApproverId) || CurrentUserId.Equals(item.CCDeputyId));
            if (CurrentUserId.Equals(item.CCDeputyId) && ccdeputys != null && ccdeputys.Count > 0) {
                var approversOfDeputy = ccdeputys.Select(c => c.Cost_x0020_Center_x0020_Manager);
                pendingCCApproval = approversOfDeputy.Contains(item.CCApproverId);
            }
            return pendingCCApproval;
        }

        private bool PendingSA1Approval(EIBMInfoForApproval item) {
            bool pendingSpecialAreaApproval =
                   EIBMStatus.PendingSpecialAreaApproval.Equals(item.Status) && (
                  SAStatus.PendingApproval.Equals(item.SAStatus1) && (CurrentUserId.Equals(item.SAApprover1Id) || CurrentUserId.Equals(item.SADeputy1Id))
                   );
            return pendingSpecialAreaApproval;
        }
        private bool PendingSA2Approval(EIBMInfoForApproval item) {
            bool pendingSpecialAreaApproval =
                   EIBMStatus.PendingSpecialAreaApproval.Equals(item.Status) && (
                    (SAStatus.PendingApproval.Equals(item.SAStatus2) && (CurrentUserId.Equals(item.SAApprover2Id) || CurrentUserId.Equals(item.SADeputy2Id)))
                   );
            return pendingSpecialAreaApproval;
        }
        private bool PendingSA3Approval(EIBMInfoForApproval item) {
            bool pendingSpecialAreaApproval =
                   EIBMStatus.PendingSpecialAreaApproval.Equals(item.Status) && (
                   (SAStatus.PendingApproval.Equals(item.SAStatus3) && (CurrentUserId.Equals(item.SAApprover3Id) || CurrentUserId.Equals(item.SADeputy3Id)))
                   );
            return pendingSpecialAreaApproval;
        }
        private bool PendingSA4Approval(EIBMInfoForApproval item) {
            bool pendingSpecialAreaApproval =
                   EIBMStatus.PendingSpecialAreaApproval.Equals(item.Status) && (
                   (SAStatus.PendingApproval.Equals(item.SAStatus4) && (CurrentUserId.Equals(item.SAApprover4Id) || CurrentUserId.Equals(item.SADeputy4Id)))
                   );
            return pendingSpecialAreaApproval;
        }
        private bool PendingSA5Approval(EIBMInfoForApproval item) {
            bool pendingSpecialAreaApproval =
                   EIBMStatus.PendingSpecialAreaApproval.Equals(item.Status) && (
                   (SAStatus.PendingApproval.Equals(item.SAStatus5) && (CurrentUserId.Equals(item.SAApprover5Id) || CurrentUserId.Equals(item.SADeputy5Id)))
                   );
            return pendingSpecialAreaApproval;
        }
        private bool PendingSA6Approval(EIBMInfoForApproval item) {
            bool pendingSpecialAreaApproval =
                   EIBMStatus.PendingSpecialAreaApproval.Equals(item.Status) && (
                    (SAStatus.PendingApproval.Equals(item.SAStatus6) && (CurrentUserId.Equals(item.SAApprover6Id) || CurrentUserId.Equals(item.SADeputy6Id)))
                   );
            return pendingSpecialAreaApproval;
        }
        private bool PendingSA7Approval(EIBMInfoForApproval item) {
            bool pendingSpecialAreaApproval =
                   EIBMStatus.PendingSpecialAreaApproval.Equals(item.Status) && (
                   (SAStatus.PendingApproval.Equals(item.SAStatus7) && (CurrentUserId.Equals(item.SAApprover7Id) || CurrentUserId.Equals(item.SADeputy7Id)))
                   );
            return pendingSpecialAreaApproval;
        }

    }
}
