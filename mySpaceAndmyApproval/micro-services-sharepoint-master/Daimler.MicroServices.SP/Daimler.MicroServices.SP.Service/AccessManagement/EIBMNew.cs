﻿using System;
using System.Collections.Generic;
using System.Data.Services.Client;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.AccessManagement;
using log4net;
using SP.Data;

namespace Daimler.MicroServices.SP.Service.AccessManagement {
    /// <summary>
    /// Use proxy class
    /// </summary>
    public class EIBMNew : AMBaseNew {
        public EIBMNew(UserCredential uc)
            : base(uc) {

        }
        private static readonly ILog log = LogManager.GetLogger(typeof(EIBMNew));
        public static string ListName = "Employee/Intern Badge Management";


        // cc manager pending approval
        //      the item CCStatus is 'Pending Approval' (after this the workflow send email )
        //      the item Status is 'Pending Cost Center Manager Approval'
        //      current user is cc manager or (current user is cc deputy and is in delegate date)
        //
        // special area pending approval
        //      the item Status is 'Pending Special Area Approval'
        //      current user is SAApprover1|SADeputy1 and SAStatus1 is 'Pending Approval'
        //      current user is SAApprover2|SADeputy2 and SAStatus2 is 'Pending Approval'
        //      current user is SAApprover3|SADeputy3 and SAStatus3 is 'Pending Approval'
        //      current user is SAApprover4|SADeputy4 and SAStatus4 is 'Pending Approval'
        //      current user is SAApprover5|SADeputy5 and SAStatus5 is 'Pending Approval'
        //      current user is SAApprover6|SADeputy6 and SAStatus6 is 'Pending Approval'
        //      current user is SAApprover7|SADeputy7 and SAStatus7 is 'Pending Approval'
        public int GetPendingCount() {
            log.Debug(new {
                listName = ListName,
                action = "GetPendingCount",
                currentUser = CurrentUserId
            });
            Func<Intern_x0020_badge_x0020_ApplicationListItem, bool> isPending = item => {
                bool pendingCCApproval = EIBMStatus.PendingCCMApproval.Equals(item.Status) && (CurrentUserId.Equals(item.CCApproverId) || CurrentUserId.Equals(item.CCDeputyId));
                bool pendingSpecialAreaApproval =
                    EIBMStatus.PendingSpecialAreaApproval.Equals(item.Status) && (
                   SAStatus.PendingApproval.Equals(item.SAStatus1) && (CurrentUserId.Equals(item.SAApprover1Id) || CurrentUserId.Equals(item.SADeputy1Id))
                    || (SAStatus.PendingApproval.Equals(item.SAStatus2) && (CurrentUserId.Equals(item.SAApprover2Id) || CurrentUserId.Equals(item.SADeputy2Id)))
                    || (SAStatus.PendingApproval.Equals(item.SAStatus3) && (CurrentUserId.Equals(item.SAApprover3Id) || CurrentUserId.Equals(item.SADeputy3Id)))
                    || (SAStatus.PendingApproval.Equals(item.SAStatus4) && (CurrentUserId.Equals(item.SAApprover4Id) || CurrentUserId.Equals(item.SADeputy4Id)))
                    || (SAStatus.PendingApproval.Equals(item.SAStatus5) && (CurrentUserId.Equals(item.SAApprover5Id) || CurrentUserId.Equals(item.SADeputy5Id)))
                    || (SAStatus.PendingApproval.Equals(item.SAStatus6) && (CurrentUserId.Equals(item.SAApprover6Id) || CurrentUserId.Equals(item.SADeputy6Id)))
                    || (SAStatus.PendingApproval.Equals(item.SAStatus7) && (CurrentUserId.Equals(item.SAApprover7Id) || CurrentUserId.Equals(item.SADeputy7Id)))
                    );
                bool pending = pendingCCApproval || pendingSpecialAreaApproval;
                return pending;
            };
            var simpleResult =
               listData.Intern_x0020_badge_x0020_ApplicationListItems
               .AddQueryOption("$select", "Id,Application_x0020_Date,Application_x0020_Type,CCApproverId,CCDeputyId,Status,Employee_x0020_Name/Title,SAStatus1,SAApprover1Id,SADeputy1Id,SAStatus2,SAApprover2Id,SADeputy2Id,SAStatus3,SAApprover3Id,SADeputy3Id,SAStatus4,SAApprover4Id,SADeputy4Id,SAStatus5,SAApprover5Id,SADeputy5Id,SAStatus6,SAApprover6Id,SADeputy6Id,SAStatus7,SAApprover7Id,SADeputy7Id")
               .Expand("Employee_x0020_Name")
               .Where(isPending)
               .Select(item => new {
                   Id = item.Id,
                   EmployeeName = item.Employee_x0020_Name.Title,
                   ApplicationDate = item.Application_x0020_Date,
                   ApplicationType = item.Application_x0020_Type,
                   Status = item.Status,
                   CCApproverId = item.CCApproverId,
                   CCDeputyId = item.CCDeputyId
               });

            if (ccdeputys != null && ccdeputys.Count > 0) {
                // select all deputy record
                // select valid deputy
                // get invalid deputy
                // except invalid deputy
                var allDeputyRecords = simpleResult.Where(b => EIBMStatus.PendingCCMApproval.Equals(b.Status) && CurrentUserId.Equals(b.CCDeputyId));
                var approversOfDeputy = ccdeputys.Select(c => c.Cost_x0020_Center_x0020_Manager);
                var validRecords = allDeputyRecords.Where(b => approversOfDeputy.Contains(b.CCApproverId));
                var invalidRecords = allDeputyRecords.Except(validRecords);
                simpleResult = simpleResult.Except(invalidRecords);
            }

            IEnumerable<EIBMItem> result =
               simpleResult
                .Select(item => new EIBMItem {
                    Id = item.Id,
                    EmployeeName = item.EmployeeName,
                    ApplicationDate = item.ApplicationDate,
                    ApplicationType = item.ApplicationType,
                    Status = item.Status
                });
            int count = result.Count();
            log.Debug(new {
                listName = ListName,
                action = "GetPendingCount",
                currentUser = CurrentUserId,
                result = count
            });
            return count;
        }


        public IEnumerable<EIBMItem> GetPendingList() {
            log.Debug(new {
                listName = ListName,
                action = "GetPendingList",
                currentUser = CurrentUserId
            });
            Func<Intern_x0020_badge_x0020_ApplicationListItem, bool> isPending = item => {
                bool pendingCCApproval = EIBMStatus.PendingCCMApproval.Equals(item.Status) && (CurrentUserId.Equals(item.CCApproverId) || CurrentUserId.Equals(item.CCDeputyId));
                bool pendingSpecialAreaApproval =
                    EIBMStatus.PendingSpecialAreaApproval.Equals(item.Status) && (
                   SAStatus.PendingApproval.Equals(item.SAStatus1) && (CurrentUserId.Equals(item.SAApprover1Id) || CurrentUserId.Equals(item.SADeputy1Id))
                    || (SAStatus.PendingApproval.Equals(item.SAStatus2) && (CurrentUserId.Equals(item.SAApprover2Id) || CurrentUserId.Equals(item.SADeputy2Id)))
                    || (SAStatus.PendingApproval.Equals(item.SAStatus3) && (CurrentUserId.Equals(item.SAApprover3Id) || CurrentUserId.Equals(item.SADeputy3Id)))
                    || (SAStatus.PendingApproval.Equals(item.SAStatus4) && (CurrentUserId.Equals(item.SAApprover4Id) || CurrentUserId.Equals(item.SADeputy4Id)))
                    || (SAStatus.PendingApproval.Equals(item.SAStatus5) && (CurrentUserId.Equals(item.SAApprover5Id) || CurrentUserId.Equals(item.SADeputy5Id)))
                    || (SAStatus.PendingApproval.Equals(item.SAStatus6) && (CurrentUserId.Equals(item.SAApprover6Id) || CurrentUserId.Equals(item.SADeputy6Id)))
                    || (SAStatus.PendingApproval.Equals(item.SAStatus7) && (CurrentUserId.Equals(item.SAApprover7Id) || CurrentUserId.Equals(item.SADeputy7Id)))
                    );
                bool pending = pendingCCApproval || pendingSpecialAreaApproval;
                return pending;
            };
            var simpleResult =
               listData.Intern_x0020_badge_x0020_ApplicationListItems
               .AddQueryOption("$select", "Id,Application_x0020_Date,Application_x0020_Type,CCApproverId,CCDeputyId,Status,Employee_x0020_Name/Title,SAStatus1,SAApprover1Id,SADeputy1Id,SAStatus2,SAApprover2Id,SADeputy2Id,SAStatus3,SAApprover3Id,SADeputy3Id,SAStatus4,SAApprover4Id,SADeputy4Id,SAStatus5,SAApprover5Id,SADeputy5Id,SAStatus6,SAApprover6Id,SADeputy6Id,SAStatus7,SAApprover7Id,SADeputy7Id")
               .Expand("Employee_x0020_Name")
               .Where(isPending)
               .Select(item => new {
                   Id = item.Id,
                   EmployeeName = item.Employee_x0020_Name.Title,
                   ApplicationDate = item.Application_x0020_Date,
                   ApplicationType = item.Application_x0020_Type,
                   Status = item.Status,
                   CCApproverId = item.CCApproverId,
                   CCDeputyId = item.CCDeputyId
               });

            if (ccdeputys != null && ccdeputys.Count > 0) {
                // select all deputy record
                // select valid deputy
                // get invalid deputy
                // except invalid deputy
                var allDeputyRecords = simpleResult.Where(b => EIBMStatus.PendingCCMApproval.Equals(b.Status) && CurrentUserId.Equals(b.CCDeputyId));
                var approversOfDeputy = ccdeputys.Select(c => c.Cost_x0020_Center_x0020_Manager);
                var validRecords = allDeputyRecords.Where(b => approversOfDeputy.Contains(b.CCApproverId));
                var invalidRecords = allDeputyRecords.Except(validRecords);
                simpleResult = simpleResult.Except(invalidRecords);
            }
            IEnumerable<EIBMItem> result =
               simpleResult
                .Select(item => new EIBMItem {
                    Id = item.Id,
                    EmployeeName = item.EmployeeName,
                    ApplicationDate = item.ApplicationDate,
                    ApplicationType = item.ApplicationType,
                    Status = item.Status
                });
            log.Debug(new {
                listName = ListName,
                action = "GetPendingList",
                currentUser = CurrentUserId,
                simpleResult = result
            });
            return result;
        }


        public async Task<Intern_x0020_badge_x0020_ApplicationListItem> GetItem(int ID) {

            Intern_x0020_badge_x0020_ApplicationListItem result = null;
            Intern_x0020_badge_x0020_ApplicationListItem lookup = null;
            Intern_x0020_badge_x0020_ApplicationListItem lookup1 = null;
            Intern_x0020_badge_x0020_ApplicationListItem lookup2 = null;
            Intern_x0020_badge_x0020_ApplicationListItem lookup3 = null;

            Task t1 = Task.Factory.StartNew(() => {
                result = listData.Intern_x0020_badge_x0020_ApplicationListItems.Where(item => ID.Equals(item.Id)).SingleOrDefault();
            });
            Task t2 = Task.Factory.StartNew(() => {
                lookup = CreateListData().Intern_x0020_badge_x0020_ApplicationListItems
                 .AddQueryOption("$select", string.Join(",",
                     EIBMFields.Id,
                     string.Join("/", EIBMFields.Employee_x0020_Name, EIBMFields.Title),
                     string.Join("/", EIBMFields.Site_x0020_Name, EIBMFields.Title),
                     string.Join("/", EIBMFields.Location_x0020_1, EIBMFields.Title),
                     string.Join("/", EIBMFields.Location_x0020_2, EIBMFields.Title),
                     string.Join("/", EIBMFields.Location_x0020_3, EIBMFields.Title),
                     string.Join("/", EIBMFields.Location_x0020_4, EIBMFields.Title),
                     string.Join("/", EIBMFields.Location_x0020_5, EIBMFields.Title),
                     string.Join("/", EIBMFields.Location_x0020_6, EIBMFields.Title),
                     string.Join("/", EIBMFields.Location_x0020_7, EIBMFields.Title)
                     ))
                 .Expand(string.Join(",",
                    EIBMFields.Employee_x0020_Name,
                    EIBMFields.Site_x0020_Name,
                    EIBMFields.Location_x0020_1,
                    EIBMFields.Location_x0020_2,
                    EIBMFields.Location_x0020_3,
                    EIBMFields.Location_x0020_4,
                    EIBMFields.Location_x0020_5,
                    EIBMFields.Location_x0020_6,
                    EIBMFields.Location_x0020_7))
                 .Where(item => ID.Equals(item.Id)).SingleOrDefault();
            });
            Task t3 = Task.Factory.StartNew(() => {
                lookup1 = CreateListData().Intern_x0020_badge_x0020_ApplicationListItems
                 .AddQueryOption("$select", string.Join(",",
                         EIBMFields.Id,
                     string.Join("/", EIBMFields.Area_x0020_1, EIBMFields.Id),
                     string.Join("/", EIBMFields.Area_x0020_1, EIBMFields.Title),
                     string.Join("/", EIBMFields.Area_x0020_2, EIBMFields.Id),
                     string.Join("/", EIBMFields.Area_x0020_2, EIBMFields.Title),
                     string.Join("/", EIBMFields.Area_x0020_3, EIBMFields.Id),
                     string.Join("/", EIBMFields.Area_x0020_3, EIBMFields.Title),
                     string.Join("/", EIBMFields.Area_x0020_4, EIBMFields.Id),
                     string.Join("/", EIBMFields.Area_x0020_4, EIBMFields.Title),
                     string.Join("/", EIBMFields.Area_x0020_5, EIBMFields.Id),
                     string.Join("/", EIBMFields.Area_x0020_5, EIBMFields.Title),
                     string.Join("/", EIBMFields.Area_x0020_6, EIBMFields.Id),
                     string.Join("/", EIBMFields.Area_x0020_6, EIBMFields.Title),
                     string.Join("/", EIBMFields.Area_x0020_7, EIBMFields.Id),
                     string.Join("/", EIBMFields.Area_x0020_7, EIBMFields.Title)))
                 .Expand(string.Join(",",
                         EIBMFields.Area_x0020_1,
                         EIBMFields.Area_x0020_2,
                         EIBMFields.Area_x0020_3,
                         EIBMFields.Area_x0020_4,
                         EIBMFields.Area_x0020_5,
                         EIBMFields.Area_x0020_6,
                         EIBMFields.Area_x0020_7))
                 .Where(item => ID.Equals(item.Id)).SingleOrDefault();
            });
            Task t4 = Task.Factory.StartNew(() => {
                lookup2 = CreateListData().Intern_x0020_badge_x0020_ApplicationListItems
                 .AddQueryOption("$select", string.Join(",",
                         EIBMFields.Id,
                     string.Join("/", EIBMFields.CCApprover, EIBMFields.Id),
                     string.Join("/", EIBMFields.CCApprover, EIBMFields.Title),
                     string.Join("/", EIBMFields.SAApprover1, EIBMFields.Id),
                     string.Join("/", EIBMFields.SAApprover1, EIBMFields.Title),
                     string.Join("/", EIBMFields.SAApprover2, EIBMFields.Id),
                     string.Join("/", EIBMFields.SAApprover2, EIBMFields.Title),
                     string.Join("/", EIBMFields.SAApprover3, EIBMFields.Id),
                     string.Join("/", EIBMFields.SAApprover3, EIBMFields.Title),
                     string.Join("/", EIBMFields.SAApprover4, EIBMFields.Id),
                     string.Join("/", EIBMFields.SAApprover4, EIBMFields.Title),
                     string.Join("/", EIBMFields.SAApprover5, EIBMFields.Id),
                     string.Join("/", EIBMFields.SAApprover5, EIBMFields.Title),
                     string.Join("/", EIBMFields.SAApprover6, EIBMFields.Id),
                     string.Join("/", EIBMFields.SAApprover6, EIBMFields.Title),
                     string.Join("/", EIBMFields.SAApprover7, EIBMFields.Id),
                     string.Join("/", EIBMFields.SAApprover7, EIBMFields.Title)))
                 .Expand(string.Join(",",
                         EIBMFields.CCApprover,
                         EIBMFields.SAApprover1,
                         EIBMFields.SAApprover2,
                         EIBMFields.SAApprover3,
                         EIBMFields.SAApprover4,
                         EIBMFields.SAApprover5,
                         EIBMFields.SAApprover6,
                         EIBMFields.SAApprover7))
                 .Where(item => ID.Equals(item.Id)).SingleOrDefault();
            });
            Task t5 = Task.Factory.StartNew(() => {
                lookup3 = CreateListData().Intern_x0020_badge_x0020_ApplicationListItems
                 .AddQueryOption("$select", string.Join(",",
                         EIBMFields.Id,
                     string.Join("/", EIBMFields.CCDeputy, EIBMFields.Id),
                     string.Join("/", EIBMFields.CCDeputy, EIBMFields.Title),
                     string.Join("/", EIBMFields.SADeputy1, EIBMFields.Id),
                     string.Join("/", EIBMFields.SADeputy1, EIBMFields.Title),
                     string.Join("/", EIBMFields.SADeputy2, EIBMFields.Id),
                     string.Join("/", EIBMFields.SADeputy2, EIBMFields.Title),
                     string.Join("/", EIBMFields.SADeputy3, EIBMFields.Id),
                     string.Join("/", EIBMFields.SADeputy3, EIBMFields.Title),
                     string.Join("/", EIBMFields.SADeputy4, EIBMFields.Id),
                     string.Join("/", EIBMFields.SADeputy4, EIBMFields.Title),
                     string.Join("/", EIBMFields.SADeputy5, EIBMFields.Id),
                     string.Join("/", EIBMFields.SADeputy5, EIBMFields.Title),
                     string.Join("/", EIBMFields.SADeputy6, EIBMFields.Id),
                     string.Join("/", EIBMFields.SADeputy6, EIBMFields.Title),
                     string.Join("/", EIBMFields.SADeputy7, EIBMFields.Id),
                     string.Join("/", EIBMFields.SADeputy7, EIBMFields.Title)))
                 .Expand(string.Join(",",
                         EIBMFields.CCDeputy,
                         EIBMFields.SADeputy1,
                         EIBMFields.SADeputy2,
                         EIBMFields.SADeputy3,
                         EIBMFields.SADeputy4,
                         EIBMFields.SADeputy5,
                         EIBMFields.SADeputy6,
                         EIBMFields.SADeputy7))
                 .Where(item => ID.Equals(item.Id)).SingleOrDefault();
            });
            await Task.WhenAll(t1, t2, t3,t4,t5);
            result.Employee_x0020_Name = lookup != null ? lookup.Employee_x0020_Name : null;
            result.Site_x0020_Name = lookup != null ? lookup.Site_x0020_Name : null;
            result.Location_x0020_1 = lookup != null ? lookup.Location_x0020_1 : null;
            result.Location_x0020_2 = lookup != null ? lookup.Location_x0020_2 : null;
            result.Location_x0020_3 = lookup != null ? lookup.Location_x0020_3 : null;
            result.Location_x0020_4 = lookup != null ? lookup.Location_x0020_4 : null;
            result.Location_x0020_5 = lookup != null ? lookup.Location_x0020_5 : null;
            result.Location_x0020_6 = lookup != null ? lookup.Location_x0020_6 : null;
            result.Location_x0020_7 = lookup != null ? lookup.Location_x0020_7 : null;
            result.Area_x0020_1 = lookup1 != null ? lookup1.Area_x0020_1 : null;
            result.Area_x0020_2 = lookup1 != null ? lookup1.Area_x0020_2 : null;
            result.Area_x0020_3 = lookup1 != null ? lookup1.Area_x0020_3 : null;
            result.Area_x0020_4 = lookup1 != null ? lookup1.Area_x0020_4 : null;
            result.Area_x0020_5 = lookup1 != null ? lookup1.Area_x0020_5 : null;
            result.Area_x0020_6 = lookup1 != null ? lookup1.Area_x0020_6 : null;
            result.Area_x0020_7 = lookup1 != null ? lookup1.Area_x0020_7 : null;
            result.CCApprover = lookup2 != null ? lookup2.CCApprover : null;
            result.SAApprover1 = lookup2 != null ? lookup2.SAApprover1 : null;
            result.SAApprover2 = lookup2 != null ? lookup2.SAApprover2 : null;
            result.SAApprover3 = lookup2 != null ? lookup2.SAApprover3 : null;
            result.SAApprover4 = lookup2 != null ? lookup2.SAApprover4 : null;
            result.SAApprover5 = lookup2 != null ? lookup2.SAApprover5 : null;
            result.SAApprover6 = lookup2 != null ? lookup2.SAApprover6 : null;
            result.SAApprover7 = lookup2 != null ? lookup2.SAApprover7 : null;
            result.CCDeputy = lookup3 != null ? lookup3.CCDeputy : null;
            result.SADeputy1 = lookup3 != null ? lookup3.SADeputy1 : null;
            result.SADeputy2 = lookup3 != null ? lookup3.SADeputy2 : null;
            result.SADeputy3 = lookup3 != null ? lookup3.SADeputy3 : null;
            result.SADeputy4 = lookup3 != null ? lookup3.SADeputy4 : null;
            result.SADeputy5 = lookup3 != null ? lookup3.SADeputy5 : null;
            result.SADeputy6 = lookup3 != null ? lookup3.SADeputy6 : null;
            result.SADeputy7 = lookup3 != null ? lookup3.SADeputy7 : null;
            return result;
        }

        public bool canApproval(Intern_x0020_badge_x0020_ApplicationListItem item, EIBMApprovalWorkflow wf) {
            switch (wf) {
                case EIBMApprovalWorkflow.EIBMCCManagerApproval:
                case EIBMApprovalWorkflow.EIBMCCManagerRejection:
                    return PendingCCApproval(item);
                case EIBMApprovalWorkflow.EIBMSpecialArea1Approval:
                case EIBMApprovalWorkflow.EIBMSpecialArea1Rejection:
                    return PendingSA1Approval(item);
                case EIBMApprovalWorkflow.EIBMSpecialArea2Approval:
                case EIBMApprovalWorkflow.EIBMSpecialArea2Rejection:
                    return PendingSA2Approval(item);
                case EIBMApprovalWorkflow.EIBMSpecialArea3Approval:
                case EIBMApprovalWorkflow.EIBMSpecialArea3Rejection:
                    return PendingSA3Approval(item);
                case EIBMApprovalWorkflow.EIBMSpecialArea4Approval:
                case EIBMApprovalWorkflow.EIBMSpecialArea4Rejection:
                    return PendingSA4Approval(item);
                case EIBMApprovalWorkflow.EIBMSpecialArea5Approval:
                case EIBMApprovalWorkflow.EIBMSpecialArea5Rejection:
                    return PendingSA5Approval(item);
                case EIBMApprovalWorkflow.EIBMSpecialArea6Approval:
                case EIBMApprovalWorkflow.EIBMSpecialArea6Rejection:
                    return PendingSA6Approval(item);
                case EIBMApprovalWorkflow.EIBMSpecialArea7Approval:
                case EIBMApprovalWorkflow.EIBMSpecialArea7Rejection:
                    return PendingSA7Approval(item);
                default:
                    return false;
            }
        }

        private bool PendingCCApproval(Intern_x0020_badge_x0020_ApplicationListItem item) {
            DateTime now = DateTime.Now;
            // bool pendingCCApproval = EIBMStatus.PendingCCMApproval.Equals(item.Status) && (
            //CurrentUserId.Equals(item.CCApproverId)
            //|| (ccdupty != null && item.CCApproverId.Equals(ccdupty.Cost_x0020_Center_x0020_ManagerId) && CurrentUserId.Equals(item.CCDeputyId) && ccdupty.Delegate_x0020_From_x0020_Date.HasValue && ccdupty.Delegate_x0020_From_x0020_Date.Value < now && ccdupty.Delegate_x0020_To_x0020_Date.HasValue && ccdupty.Delegate_x0020_To_x0020_Date.Value > now));
            // return pendingCCApproval;


            bool pendingCCApproval = EIBMStatus.PendingCCMApproval.Equals(item.Status) && (CurrentUserId.Equals(item.CCApproverId) || CurrentUserId.Equals(item.CCDeputyId));
            if (CurrentUserId.Equals(item.CCDeputyId) && ccdeputys != null && ccdeputys.Count > 0) {
                var approversOfDeputy = ccdeputys.Select(c => c.Cost_x0020_Center_x0020_Manager);
                pendingCCApproval = approversOfDeputy.Contains(item.CCApproverId);
            }
            return pendingCCApproval;
        }

        private bool PendingSA1Approval(Intern_x0020_badge_x0020_ApplicationListItem item) {
            bool pendingSpecialAreaApproval =
                   EIBMStatus.PendingSpecialAreaApproval.Equals(item.Status) && (
                  SAStatus.PendingApproval.Equals(item.SAStatus1) && (CurrentUserId.Equals(item.SAApprover1Id) || CurrentUserId.Equals(item.SADeputy1Id))
                   );
            return pendingSpecialAreaApproval;
        }
        private bool PendingSA2Approval(Intern_x0020_badge_x0020_ApplicationListItem item) {
            bool pendingSpecialAreaApproval =
                   EIBMStatus.PendingSpecialAreaApproval.Equals(item.Status) && (
                    (SAStatus.PendingApproval.Equals(item.SAStatus2) && (CurrentUserId.Equals(item.SAApprover2Id) || CurrentUserId.Equals(item.SADeputy2Id)))
                   );
            return pendingSpecialAreaApproval;
        }
        private bool PendingSA3Approval(Intern_x0020_badge_x0020_ApplicationListItem item) {
            bool pendingSpecialAreaApproval =
                   EIBMStatus.PendingSpecialAreaApproval.Equals(item.Status) && (
                   (SAStatus.PendingApproval.Equals(item.SAStatus3) && (CurrentUserId.Equals(item.SAApprover3Id) || CurrentUserId.Equals(item.SADeputy3Id)))
                   );
            return pendingSpecialAreaApproval;
        }
        private bool PendingSA4Approval(Intern_x0020_badge_x0020_ApplicationListItem item) {
            bool pendingSpecialAreaApproval =
                   EIBMStatus.PendingSpecialAreaApproval.Equals(item.Status) && (
                   (SAStatus.PendingApproval.Equals(item.SAStatus4) && (CurrentUserId.Equals(item.SAApprover4Id) || CurrentUserId.Equals(item.SADeputy4Id)))
                   );
            return pendingSpecialAreaApproval;
        }
        private bool PendingSA5Approval(Intern_x0020_badge_x0020_ApplicationListItem item) {
            bool pendingSpecialAreaApproval =
                   EIBMStatus.PendingSpecialAreaApproval.Equals(item.Status) && (
                   (SAStatus.PendingApproval.Equals(item.SAStatus5) && (CurrentUserId.Equals(item.SAApprover5Id) || CurrentUserId.Equals(item.SADeputy5Id)))
                   );
            return pendingSpecialAreaApproval;
        }
        private bool PendingSA6Approval(Intern_x0020_badge_x0020_ApplicationListItem item) {
            bool pendingSpecialAreaApproval =
                   EIBMStatus.PendingSpecialAreaApproval.Equals(item.Status) && (
                    (SAStatus.PendingApproval.Equals(item.SAStatus6) && (CurrentUserId.Equals(item.SAApprover6Id) || CurrentUserId.Equals(item.SADeputy6Id)))
                   );
            return pendingSpecialAreaApproval;
        }
        private bool PendingSA7Approval(Intern_x0020_badge_x0020_ApplicationListItem item) {
            bool pendingSpecialAreaApproval =
                   EIBMStatus.PendingSpecialAreaApproval.Equals(item.Status) && (
                   (SAStatus.PendingApproval.Equals(item.SAStatus7) && (CurrentUserId.Equals(item.SAApprover7Id) || CurrentUserId.Equals(item.SADeputy7Id)))
                   );
            return pendingSpecialAreaApproval;
        }

        public EIBMApproveResult Approval(Intern_x0020_badge_x0020_ApplicationListItem item, EIBMBadgeApprovalInfo info) {
            EIBMApproveResult result = new EIBMApproveResult();
            result.info = info;
            try {
                string wfName = SPConfig.BadgeWorkflows.GetFrom(info.wf);
                if (string.IsNullOrWhiteSpace(wfName)) {
                    result.msg = ApproveFailedReason.NotSupportedWorkflow;
                }
                string associationData = GetRejectAssociationData(info.comment);
                log.Debug(new {
                    uc = uc,
                    approvalItem = item.Id,
                    workflow = info.wf,
                    wfName = wfName,
                    comment = info.comment,
                    associationData = associationData
                });
                if (canApproval(item, info.wf)) {
                    Guid approvalResult = client.StartWorkflowOnListItem(item.Id, ListName, wfName, associationData);
                    result.success = true;
                    log.Debug(new {
                        uc = uc,
                        approvalItem = item.Id,
                        workflow = info.wf,
                        wfName = wfName,
                        comment = info.comment,
                        associationData = associationData,
                        approvalResult = approvalResult
                    });

                } else {
                    log.Debug(new {
                        uc = uc,
                        approvalItem = item.Id,
                        workflow = info.wf,
                        wfName = wfName,
                        comment = info.comment,
                        associationData = associationData,
                        approvalResult = "can't approval"
                    });
                    result.msg = ApproveFailedReason.NotApproverOrApprovedYet;
                }
            } catch (Exception ex) {
                result.msg = ApproveFailedReason.Other;
                log.Error(this, ex);
            }

            return result;
        }

        public async Task<EIBMApproveResult> ApprovalAsync(Intern_x0020_badge_x0020_ApplicationListItem item, EIBMBadgeApprovalInfo info) {
            Task<EIBMApproveResult> t = Task.Factory.StartNew<EIBMApproveResult>(() => {
                return Approval(item, info);
            });
            return await t;
        }



        // cost center manager reject
        public void CostCenterManagerReject(int itemId, string comment) {
            //int itemId, string listName, string workflowName, string associationData
            Guid rejectResult = client.StartWorkflowOnListItem(itemId, ListName, SPConfig.Instance.BadgeWorkflowNames.EIBMCCManagerRejection, GetRejectAssociationData(comment));
            log.Debug(rejectResult);
        }
    }
}
