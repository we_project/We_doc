﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.Service.AccessManagement {
    public static class EIBMFields {
        public static string Title = "Title";

        public static string Status = "Status";

        public static string Application_x0020_Date = "Application_x0020_Date";

        public static string Badge_x0020_Type = "Badge_x0020_Type";

        public static string Application_x0020_Type = "Application_x0020_Type";

        public static string Employee_x0020_Number = "Employee_x0020_Number";

        public static string Company = "Company";

        public static string Employee_x0020_Name = "Employee_x0020_Name";

        public static string Deskphone_x0020_Number = "Deskphone_x0020_Number";

        public static string Cost_x0020_Center = "Cost_x0020_Center";

        public static string Arrive_x0020_Date = "Arrive_x0020_Date";

        public static string Leave_x0020_Date = "Leave_x0020_Date";

        public static string Reason_x0020_for_x0020_Visit = "Reason_x0020_for_x0020_Visit";

        public static string Remarks = "Remarks";

        public static string Group = "Group";

        public static string Location = "Location";

        public static string Area = "Area";

        public static string CCApprover = "CCApprover";

        public static string CCDeputy = "CCDeputy";

        public static string CCStatus = "CCStatus";

        public static string CCApprovedBy = "CCApprovedBy";

        public static string CCApprovedDate = "CCApprovedDate";

        public static string CCComments = "CCComments";

        public static string SAApprover = "SAApprover";

        public static string SADeputy = "SADeputy";

        public static string SAStatus = "SAStatus";

        public static string SAApprovedBy = "SAApprovedBy";

        public static string SAApprovedDate = "SAApprovedDate";

        public static string SAComments = "SAComments";

        public static string Department = "Department";

        public static string NumOfVisitor = "NumOfVisitor";

        public static string NumOfLocation = "NumOfLocation";

        public static string Date_x0020_From_x0020_1 = "Date_x0020_From_x0020_1";

        public static string Date_x0020_From_x0020_2 = "Date_x0020_From_x0020_2";

        public static string Date_x0020_From_x0020_3 = "Date_x0020_From_x0020_3";

        public static string Date_x0020_From_x0020_4 = "Date_x0020_From_x0020_4";

        public static string Date_x0020_From_x0020_5 = "Date_x0020_From_x0020_5";

        public static string Date_x0020_From_x0020_6 = "Date_x0020_From_x0020_6";

        public static string Date_x0020_From_x0020_7 = "Date_x0020_From_x0020_7";

        public static string Date_x0020_To_x0020_1 = "Date_x0020_To_x0020_1";

        public static string Date_x0020_To_x0020_2 = "Date_x0020_To_x0020_2";

        public static string Date_x0020_To_x0020_3 = "Date_x0020_To_x0020_3";

        public static string Date_x0020_To_x0020_4 = "Date_x0020_To_x0020_4";

        public static string Date_x0020_To_x0020_5 = "Date_x0020_To_x0020_5";

        public static string Date_x0020_To_x0020_6 = "Date_x0020_To_x0020_6";

        public static string Date_x0020_To_x0020_7 = "Date_x0020_To_x0020_7";

        public static string SAApprover1 = "SAApprover1";

        public static string SADeputy1 = "SADeputy1";

        public static string SAStatus1 = "SAStatus1";

        public static string SAApprovedBy1 = "SAApprovedBy1";

        public static string SAApprovedDate1 = "SAApprovedDate1";

        public static string SAApprover2 = "SAApprover2";

        public static string SAApprover3 = "SAApprover3";

        public static string SAApprover4 = "SAApprover4";

        public static string SAApprover5 = "SAApprover5";

        public static string SAApprover6 = "SAApprover6";

        public static string SAApprover7 = "SAApprover7";

        public static string SADeputy2 = "SADeputy2";

        public static string SADeputy3 = "SADeputy3";

        public static string SADeputy4 = "SADeputy4";

        public static string SADeputy5 = "SADeputy5";

        public static string SADeputy6 = "SADeputy6";

        public static string SADeputy7 = "SADeputy7";

        public static string SAStatus2 = "SAStatus2";

        public static string SAStatus3 = "SAStatus3";

        public static string SAStatus4 = "SAStatus4";

        public static string SAStatus5 = "SAStatus5";

        public static string SAStatus6 = "SAStatus6";

        public static string SAStatus7 = "SAStatus7";

        public static string SAApprovedBy2 = "SAApprovedBy2";

        public static string SAApprovedBy3 = "SAApprovedBy3";

        public static string SAApprovedBy4 = "SAApprovedBy4";

        public static string SAApprovedBy5 = "SAApprovedBy5";

        public static string SAApprovedBy6 = "SAApprovedBy6";

        public static string SAApprovedBy7 = "SAApprovedBy7";

        public static string SAApprovedDate2 = "SAApprovedDate2";

        public static string SAApprovedDate3 = "SAApprovedDate3";

        public static string SAApprovedDate4 = "SAApprovedDate4";

        public static string SAApprovedDate5 = "SAApprovedDate5";

        public static string SAApprovedDate6 = "SAApprovedDate6";

        public static string SAApprovedDate7 = "SAApprovedDate7";

        public static string SAComments1 = "SAComments1";

        public static string SAComments2 = "SAComments2";

        public static string SAComments3 = "SAComments3";

        public static string SAComments4 = "SAComments4";

        public static string SAComments5 = "SAComments5";

        public static string SAComments6 = "SAComments6";

        public static string SAComments7 = "SAComments7";

        public static string Group_x0020_1 = "Group_x0020_1";

        public static string Group_x0020_2 = "Group_x0020_2";

        public static string Group_x0020_3 = "Group_x0020_3";

        public static string Group_x0020_4 = "Group_x0020_4";

        public static string Group_x0020_5 = "Group_x0020_5";

        public static string Group_x0020_6 = "Group_x0020_6";

        public static string Group_x0020_7 = "Group_x0020_7";

        public static string CreateApplicationNumber = "CreateApplicationNumber";

        public static string Year = "Year";

        public static string SubmitFlag = "SubmitFlag";

        public static string Set_x0020_SubmitFlag_x0020_to_x0 = "Set_x0020_SubmitFlag_x0020_to_x0";

        public static string BPSDesc = "BPSDesc";

        public static string DPDesc = "DPDesc";

        public static string PSCDesc = "PSCDesc";

        public static string MaxLeaveDate = "MaxLeaveDate";

        public static string Visitor_x0020_Name_x0020_1 = "Visitor_x0020_Name_x0020_1";

        public static string ReminderFlag = "ReminderFlag";

        public static string Badge_x0020_Issued_x0020_Date = "Badge_x0020_Issued_x0020_Date";

        public static string Badge_x0020_Returned_x0020_Date = "Badge_x0020_Returned_x0020_Date";

        public static string Badge_x0020_Issued_x0020_By = "Badge_x0020_Issued_x0020_By";

        public static string Badge_x0020_Returned_x0020_By = "Badge_x0020_Returned_x0020_By";

        public static string ReminderTest = "ReminderTest";

        public static string Query1 = "Query1";

        public static string Query2 = "Query2";

        public static string Query3 = "Query3";

        public static string Query4 = "Query4";

        public static string Query5 = "Query5";

        public static string TestCreateAppNumber = "TestCreateAppNumber";

        public static string AdminIssuanceStatus = "AdminIssuanceStatus";

        public static string AdminRejectedDate = "AdminRejectedDate";

        public static string AdminRejectedBy = "AdminRejectedBy";

        public static string AdminIssuedDate = "AdminIssuedDate";

        public static string AdminIssuedBy = "AdminIssuedBy";

        public static string AdminReturnedDate = "AdminReturnedDate";

        public static string AdminReturnedBy = "AdminReturnedBy";

        public static string AdminComments = "AdminComments";

        public static string Site_x0020_Name = "Site_x0020_Name";

        public static string Set_x0020_Stat_x0020_to_x0020_Ap = "Set_x0020_Stat_x0020_to_x0020_Ap";

        public static string Location_x0020_1 = "Location_x0020_1";

        public static string Area_x0020_1 = "Area_x0020_1";

        public static string Location_x0020_2 = "Location_x0020_2";

        public static string Area_x0020_2 = "Area_x0020_2";

        public static string Location_x0020_3 = "Location_x0020_3";

        public static string Area_x0020_3 = "Area_x0020_3";

        public static string Location_x0020_4 = "Location_x0020_4";

        public static string Area_x0020_4 = "Area_x0020_4";

        public static string Location_x0020_5 = "Location_x0020_5";

        public static string Area_x0020_5 = "Area_x0020_5";

        public static string Location_x0020_6 = "Location_x0020_6";

        public static string Area_x0020_6 = "Area_x0020_6";

        public static string Location_x0020_7 = "Location_x0020_7";

        public static string Area_x0020_7 = "Area_x0020_7";

        public static string Badge_x0020_Number = "Badge_x0020_Number";

        public static string Always = "Always";

        public static string Always2 = "Always2";

        public static string Always3 = "Always3";

        public static string Always4 = "Always4";

        public static string Always5 = "Always5";

        public static string Always6 = "Always6";

        public static string Always7 = "Always7";

        public static string Perry_x0020_Change_x0020_Status = "Perry_x0020_Change_x0020_Status";

        public static string UATEIBMC = "UATEIBMC";

        public static string UATEIBMC0 = "UATEIBMC0";

        public static string ID = "ID";
        public static string Id = "Id";

        public static string Modified = "Modified";

        public static string Created = "Created";
    }
}
