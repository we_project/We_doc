﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.Service.AccessManagement {
   public class EIBMStatus {
        public static readonly string PendingCCMApproval = "Pending Cost Center Manager Approval";
        public static readonly string PendingSpecialAreaApproval = "Pending Special Area Approval";
    }
}
