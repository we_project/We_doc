﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.Service.AccessManagement {
    class SAStatus {
        /// <summary>
        /// Pending Approval
        /// </summary>
        public static readonly string PendingApproval = "Pending Approval";
    }
}
