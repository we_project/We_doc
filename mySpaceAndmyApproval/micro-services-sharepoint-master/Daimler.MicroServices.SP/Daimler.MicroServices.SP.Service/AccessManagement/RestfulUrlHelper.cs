﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SP.Service.AccessManagement {
    public class RestfulUrlHelper {
        public static string GetListItemsUrl(string spSiteUrl, string listName) {
            string url = string.Format("{0}/_api/web/Lists/getByTitle('{1}')/items", spSiteUrl, listName);
            return url;

        }
        public static string GetListItemsCountUrl(string spSiteUrl, string listName) {
            string url = string.Format("{0}/_api/web/Lists/getByTitle('{1}')/itemcount", spSiteUrl, listName);
            return url;

        }

        public static string GetItemUrl(string spSiteUrl, string listName, int Id) {
            string url = string.Format("{0}/_api/web/Lists/getByTitle('{1}')/items({2})", spSiteUrl, listName, Id);
            return url;
        }

        public static string GetSelectPart(List<string> fields) {
            string result = null;
            if (fields != null && fields.Count > 0) {
                result = string.Format("$select={0}", string.Join(",", fields));
            }
            return result;
        }
        public static string GetTopPart(int top) {
            string result = null;
            if (top > 0) {
                result = string.Format("$top={0}", top);
            }
            return result;
        }
        public static string GetExpandPart(List<string> fields) {
            string result = null;
            if (fields != null && fields.Count > 0) {
                result = string.Format("$expand={0}", string.Join(",", fields));
            }
            return result;
        }
        public static string GetFilterPart(string filter) {
            string result = null;
            if (!string.IsNullOrWhiteSpace(filter)) {
                result = string.Format("$filter={0}", filter);
            }
            return result;
        }

        public static string ConditionPart(string a, string condition, object b) {
            if (b is string) {
                return string.Format("({0} {1} {2})", a, condition, string.Format("'{0}'", b));

            }
            return string.Format("({0} {1} {2})", a, condition, b);
        }

        /// <summary>
        /// why it only support lower case???
        /// </summary>
        public static string Eq = "eq";
    }
}
