﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.Utils.Cache;
using Daimler.MicroServices.Utils.Network;
using Daimler.MicroServices.Utils.XML;
using log4net;
using Proxys;
using SP;
using SP.Data;
using Newtonsoft.Json;
using Daimler.MicroServices.SP.BizObject.AccessManagement;

namespace Daimler.MicroServices.SP.Service.AccessManagement {
    public class AMBaseNew {
        private static readonly ILog log = LogManager.GetLogger(typeof(AMBaseNew));
        protected UserCredential uc;
        NetworkCredential credentials;
        public AMBaseNew(UserCredential uc) {
            this.uc = uc;
            credentials = AbstractSharepoint.GetCredential(uc);
            apiData = new ApiData(serviceRoot);
            apiData.Credentials = credentials;
            listData = new ListData(serviceRoot);
            listData.Credentials = credentials;
            client = new NintexWorkflowWSSoapClient("BadgeNintexWorkflowWSSoap12");
            client.ClientCredentials.Windows.ClientCredential = credentials;
            client.Endpoint.EndpointBehaviors.Add(new InspectorBehavior());
        }
        public ListData CreateListData() {
            var ld = new ListData(serviceRoot);
            ld.Credentials = credentials;
            return ld;
        }

        private static readonly Uri serviceRoot = new Uri(SPConfig.Instance.AccessManagementUrl.TrimEnd('/') + "/_api");
        private static readonly Uri currentUserResourceUrl = new Uri(SPConfig.Instance.AccessManagementUrl.TrimEnd('/') + "/_api" + "/web/currentuser");
        protected ListData listData { get; set; }
        protected ApiData apiData { get; set; }
        protected Web web { get; set; }
        //private Principal _currentUser;
        //private static object _lock = new object();
        //protected Principal CurrentUser {
        //    get {

        //        if (_currentUser != null) {
        //            return _currentUser;
        //        }
        //        //log.Debug(new {
        //        //    currentUserResourceUrl = currentUserResourceUrl
        //        //});
        //        lock (_lock) {
        //            string _cacheKey = "badge_user_for" + uc;
        //            object cacheObj = CacheHelper.Get(_cacheKey);
        //            if (cacheObj == null) {
        //                log.Info(new {
        //                    action = "GetCurrentUser",
        //                    uc = uc
        //                });
        //                _currentUser = apiData.Execute<Principal>(currentUserResourceUrl, "GET", true).SingleOrDefault();
        //                log.Info(new {
        //                    action = "GetCurrentUser",
        //                    uc = uc,
        //                    success = true
        //                });
        //                CacheHelper.Set(_cacheKey, _currentUser);
        //            } else {
        //                _currentUser = cacheObj as Principal;
        //            }
        //        }

        //        return _currentUser;
        //    }
        //}

        public int CurrentUserId { get; set; }
        protected NintexWorkflowWSSoapClient client { get; set; }
        //private Cost_x0020_Center_x0020_Deputy_x0020_ProfileListItem _ccdupty;
        //private static object _lockCCDupty = new object();
        //class CCDuptyWrapper {
        //    public Cost_x0020_Center_x0020_Deputy_x0020_ProfileListItem Value { get; set; }
        //}

        //protected Cost_x0020_Center_x0020_Deputy_x0020_ProfileListItem ccdupty {
        //    get {
        //        // get cc manager deputy
        //        lock (_lockCCDupty) {
        //            string _cacheKey = "badge_ccdupty_for" + uc;
        //            object cacheObj = CacheHelper.Get(_cacheKey);
        //            if (cacheObj == null) {
        //                log.Info(new {
        //                    action = "Getccdupty",
        //                    uc = uc
        //                });
        //                _ccdupty = listData.Cost_x0020_Center_x0020_Deputy_x0020_ProfileListItems
        //                   .AddQueryOption("$select",
        //                       string.Join(",",
        //                       "Cost_x0020_Center_x0020_Manager/Id",
        //                       "Cost_x0020_Center_x0020_Deputy/Id",
        //                       "Delegate_x0020_From_x0020_Date",
        //                       "Delegate_x0020_To_x0020_Date"))
        //                       .Expand(string.Join(",",
        //                       "Cost_x0020_Center_x0020_Manager",
        //                       "Cost_x0020_Center_x0020_Deputy"
        //                       ))
        //                   .Where(ccd => CurrentUserId.Equals(ccd.Cost_x0020_Center_x0020_Deputy.Id)).SingleOrDefault();
        //                log.Info(new {
        //                    action = "Getccdupty",
        //                    uc = uc,
        //                    success = true
        //                });
        //                CacheHelper.Set(_cacheKey, new CCDuptyWrapper { Value = _ccdupty }, new CacheItemPolicy { AbsoluteExpiration = DateTimeOffset.Now.AddHours(2) });
        //            } else {
        //                _ccdupty = (cacheObj as CCDuptyWrapper).Value;
        //            }
        //        }

        //        return _ccdupty;
        //    }
        //}


        //private IEnumerable<Cost_x0020_Center_x0020_Deputy_x0020_ProfileListItem> _ccduptys;

        //class CCDuptysWrapper {
        //    public IEnumerable<Cost_x0020_Center_x0020_Deputy_x0020_ProfileListItem> Value { get; set; }
        //}
        //public class CCDeputy {
        //    public int Id { get; set; }
        //    public int? CCManagerId { get; set; }
        //    public int? CCDeputyId { get; set; }
        //    public DateTime? FromDate { get; set; }
        //    public DateTime? ToDate { get; set; }
        //}
        //private static object _lockCCDuptys = new object();
        //protected IEnumerable<Cost_x0020_Center_x0020_Deputy_x0020_ProfileListItem> ccduptys {
        //    get {
        //        // get cc manager deputy
        //        lock (_lockCCDuptys) {
        //            DateTime now = DateTime.Now;
        //            string _cacheKey = "badge_ccduptys_for" + uc;
        //            object cacheObj = CacheHelper.Get(_cacheKey);
        //            if (cacheObj == null) {
        //                log.Info(new {
        //                    action = "Getccduptys",
        //                    uc = uc
        //                });
        //                _ccduptys = CreateListData().Cost_x0020_Center_x0020_Deputy_x0020_ProfileListItems
        //                   .AddQueryOption("$select",
        //                       string.Join(",",
        //                       "Cost_x0020_Center_x0020_Manager/Id",
        //                       "Cost_x0020_Center_x0020_Deputy/Id",
        //                       "Delegate_x0020_From_x0020_Date",
        //                       "Delegate_x0020_To_x0020_Date"))
        //                   .Expand(string.Join(",",
        //                       "Cost_x0020_Center_x0020_Manager",
        //                       "Cost_x0020_Center_x0020_Deputy"
        //                       ))
        //                   .Where(ccd => CurrentUserId.Equals(ccd.Cost_x0020_Center_x0020_Deputy.Id)).AsEnumerable();
        //                log.Info(new {
        //                    action = "Getccduptys",
        //                    uc = uc,
        //                    success = true
        //                });
        //                log.Debug(JsonConvert.SerializeObject(_ccduptys));
        //                CacheHelper.Set(_cacheKey, new CCDuptysWrapper { Value = _ccduptys }, new CacheItemPolicy { AbsoluteExpiration = DateTimeOffset.Now.AddHours(2) });
        //            } else {
        //                _ccduptys = (cacheObj as CCDuptysWrapper).Value;
        //            }
        //        }
        //        return _ccduptys;
        //    }
        //}


        public List<CostCenterDeputyProfileInfo> ccdeputys { get; set; }

        private static readonly string AssociateDataTemplateForReject = "<dfs:myFields xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:dfs=\"http://schemas.microsoft.com/office/infopath/2003/dataFormSolution\" xmlns:d=\"http://schemas.microsoft.com/office/infopath/2009/WSSList/dataFields\" xmlns:pc=\"http://schemas.microsoft.com/office/infopath/2007/PartnerControls\" xmlns:ma=\"http://schemas.microsoft.com/office/2009/metadata/properties/metaAttributes\" xmlns:q=\"http://schemas.microsoft.com/office/infopath/2009/WSSList/queryFields\" xmlns:dms=\"http://schemas.microsoft.com/office/2009/documentManagement/types\" xmlns:xd=\"http://schemas.microsoft.com/office/infopath/2003\"><dfs:queryFields /><dfs:dataFields><d:SharePointListItem_RW><d:Reject_x0020_Comments>{0}</d:Reject_x0020_Comments></d:SharePointListItem_RW></dfs:dataFields></dfs:myFields>";

        protected string GetRejectAssociationData(string comment) {
            if (string.IsNullOrWhiteSpace(comment)) {
                return null;
            }
            string xml = string.Format(AssociateDataTemplateForReject, comment);
            return xml;
        }



    }
}
