﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.AccessManagement;
using Daimler.MicroServices.Utils;
using log4net;

namespace Daimler.MicroServices.SP.Service.AccessManagement {
    public class Location : AMBase {
        public Location(UserCredential uc)
            : base(uc) {

        }
        private static readonly ILog log = LogManager.GetLogger(typeof(Location));

        public static string ListName = "Location";

        public override string listName {
            get { return ListName; }
        }

        public override List<string> FieldsToLoad {
            get {
                return _FieldsToLoad;
            }
        }
        public static List<string> _FieldsToLoad = ReflectionMethod.GetPropertyNames<LocationInfo>();
        
    }
}
