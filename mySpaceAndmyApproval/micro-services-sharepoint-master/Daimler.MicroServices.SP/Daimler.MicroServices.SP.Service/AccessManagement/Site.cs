﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.AccessManagement;
using Daimler.MicroServices.Utils;
using Daimler.MicroServices.Utils.Network.Sharepoint;
using log4net;

namespace Daimler.MicroServices.SP.Service.AccessManagement {
    public class Site : AMBase {
        public Site(UserCredential uc)
            : base(uc) {

        }
        private static readonly ILog log = LogManager.GetLogger(typeof(Site));

        public static string ListName = "Site";

        public override string listName {
            get { return ListName; }
        }

        public override List<string> FieldsToLoad {
            get {
                return _FieldsToLoad;
            }
        }
        public static List<string> _FieldsToLoad = ReflectionMethod.GetPropertyNames<SiteInfo>();
        
    }
}
