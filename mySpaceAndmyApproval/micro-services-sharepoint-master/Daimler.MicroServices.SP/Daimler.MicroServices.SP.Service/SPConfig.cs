﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.AccessManagement;
using Newtonsoft.Json;

namespace Daimler.MicroServices.SP.Service {
    public class SPConfig {
        private SPConfig() { }
        private static volatile SPConfig instance;
        private static object syncRoot = new object();
        private static string SPConfigFileName = ConfigurationManager.AppSettings["SPConfigFile"];
        public static SPConfig Instance {
            get {
                if (instance == null) {
                    lock (syncRoot) {
                        if (instance == null) {
                            string json = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SPConfigFileName));
                            instance = JsonConvert.DeserializeObject<SPConfig>(json);
                        }
                    }
                }
                return instance;
            }
        }
        public string Domain { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool UseProxy { get; set; }
        public string Proxy { get; set; }
        public string CommonUrl { get; set; }
        public string FleetManagementUrl { get; set; }
        public string FormAFormBUrl { get; set; }
        public string AccessManagementUrl { get; set; }

        public BadgeWorkflows BadgeWorkflowNames { get; set; }


        public static string StationaryImagesDirName = "StationaryImages";
        public static string StationaryImagesLocalPath {
            get {
                string dir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, StationaryImagesDirName);
                if (!Directory.Exists(dir)) {
                    Directory.CreateDirectory(dir);
                }
                return dir;
            }
        }

        public class BadgeWorkflows {
            public string EIBMCCManagerApproval { get; set; }
            public string EIBMCCManagerRejection { get; set; }
            public string EIBMSpecialArea1Approval { get; set; }
            public string EIBMSpecialArea1Rejection { get; set; }
            public string EIBMSpecialArea2Approval { get; set; }
            public string EIBMSpecialArea2Rejection { get; set; }
            public string EIBMSpecialArea3Approval { get; set; }
            public string EIBMSpecialArea3Rejection { get; set; }
            public string EIBMSpecialArea4Approval { get; set; }
            public string EIBMSpecialArea4Rejection { get; set; }
            public string EIBMSpecialArea5Approval { get; set; }
            public string EIBMSpecialArea5Rejection { get; set; }
            public string EIBMSpecialArea6Approval { get; set; }
            public string EIBMSpecialArea6Rejection { get; set; }
            public string EIBMSpecialArea7Approval { get; set; }
            public string EIBMSpecialArea7Rejection { get; set; }

            public string CCManagerApproval { get; set; }
            public string CCManagerRejection { get; set; }
            public string SpecialArea1Approval { get; set; }
            public string SpecialArea1Rejection { get; set; }
            public string SpecialArea2Approval { get; set; }
            public string SpecialArea2Rejection { get; set; }
            public string SpecialArea3Approval { get; set; }
            public string SpecialArea3Rejection { get; set; }
            public string SpecialArea4Approval { get; set; }
            public string SpecialArea4Rejection { get; set; }
            public string SpecialArea5Approval { get; set; }
            public string SpecialArea5Rejection { get; set; }
            public string SpecialArea6Approval { get; set; }
            public string SpecialArea6Rejection { get; set; }
            public string SpecialArea7Approval { get; set; }
            public string SpecialArea7Rejection { get; set; }

            public static string GetFrom(EIBMApprovalWorkflow wf) {
                switch (wf) {
                    case EIBMApprovalWorkflow.EIBMCCManagerApproval:
                        return SPConfig.Instance.BadgeWorkflowNames.EIBMCCManagerApproval;
                    case EIBMApprovalWorkflow.EIBMCCManagerRejection:
                        return SPConfig.Instance.BadgeWorkflowNames.EIBMCCManagerRejection;
                    case EIBMApprovalWorkflow.EIBMSpecialArea1Approval:
                        return SPConfig.Instance.BadgeWorkflowNames.EIBMSpecialArea1Approval;
                    case EIBMApprovalWorkflow.EIBMSpecialArea1Rejection:
                        return SPConfig.Instance.BadgeWorkflowNames.EIBMSpecialArea1Rejection;
                    case EIBMApprovalWorkflow.EIBMSpecialArea2Approval:
                        return SPConfig.Instance.BadgeWorkflowNames.EIBMSpecialArea2Approval;
                    case EIBMApprovalWorkflow.EIBMSpecialArea2Rejection:
                        return SPConfig.Instance.BadgeWorkflowNames.EIBMSpecialArea2Rejection;
                    case EIBMApprovalWorkflow.EIBMSpecialArea3Approval:
                        return SPConfig.Instance.BadgeWorkflowNames.EIBMSpecialArea3Approval;
                    case EIBMApprovalWorkflow.EIBMSpecialArea3Rejection:
                        return SPConfig.Instance.BadgeWorkflowNames.EIBMSpecialArea3Rejection;
                    case EIBMApprovalWorkflow.EIBMSpecialArea4Approval:
                        return SPConfig.Instance.BadgeWorkflowNames.EIBMSpecialArea4Approval;
                    case EIBMApprovalWorkflow.EIBMSpecialArea4Rejection:
                        return SPConfig.Instance.BadgeWorkflowNames.EIBMSpecialArea4Rejection;
                    case EIBMApprovalWorkflow.EIBMSpecialArea5Approval:
                        return SPConfig.Instance.BadgeWorkflowNames.EIBMSpecialArea5Approval;
                    case EIBMApprovalWorkflow.EIBMSpecialArea5Rejection:
                        return SPConfig.Instance.BadgeWorkflowNames.EIBMSpecialArea5Rejection;
                    case EIBMApprovalWorkflow.EIBMSpecialArea6Approval:
                        return SPConfig.Instance.BadgeWorkflowNames.EIBMSpecialArea6Approval;
                    case EIBMApprovalWorkflow.EIBMSpecialArea6Rejection:
                        return SPConfig.Instance.BadgeWorkflowNames.EIBMSpecialArea6Rejection;
                    case EIBMApprovalWorkflow.EIBMSpecialArea7Approval:
                        return SPConfig.Instance.BadgeWorkflowNames.EIBMSpecialArea7Approval;
                    case EIBMApprovalWorkflow.EIBMSpecialArea7Rejection:
                        return SPConfig.Instance.BadgeWorkflowNames.EIBMSpecialArea7Rejection;
                    default:
                        return null;
                }
            }
            public static string GetFrom(VisitorBadgeApprovalWorkflow wf) {
                switch (wf) {
                    case VisitorBadgeApprovalWorkflow.CCManagerApproval:
                        return SPConfig.Instance.BadgeWorkflowNames.CCManagerApproval;
                    case VisitorBadgeApprovalWorkflow.CCManagerRejection:
                        return SPConfig.Instance.BadgeWorkflowNames.CCManagerRejection;
                    case VisitorBadgeApprovalWorkflow.SpecialArea1Approval:
                        return SPConfig.Instance.BadgeWorkflowNames.SpecialArea1Approval;
                    case VisitorBadgeApprovalWorkflow.SpecialArea1Rejection:
                        return SPConfig.Instance.BadgeWorkflowNames.SpecialArea1Rejection;
                    case VisitorBadgeApprovalWorkflow.SpecialArea2Approval:
                        return SPConfig.Instance.BadgeWorkflowNames.SpecialArea2Approval;
                    case VisitorBadgeApprovalWorkflow.SpecialArea2Rejection:
                        return SPConfig.Instance.BadgeWorkflowNames.SpecialArea2Rejection;
                    case VisitorBadgeApprovalWorkflow.SpecialArea3Approval:
                        return SPConfig.Instance.BadgeWorkflowNames.SpecialArea3Approval;
                    case VisitorBadgeApprovalWorkflow.SpecialArea3Rejection:
                        return SPConfig.Instance.BadgeWorkflowNames.SpecialArea3Rejection;
                    case VisitorBadgeApprovalWorkflow.SpecialArea4Approval:
                        return SPConfig.Instance.BadgeWorkflowNames.SpecialArea4Approval;
                    case VisitorBadgeApprovalWorkflow.SpecialArea4Rejection:
                        return SPConfig.Instance.BadgeWorkflowNames.SpecialArea4Rejection;
                    case VisitorBadgeApprovalWorkflow.SpecialArea5Approval:
                        return SPConfig.Instance.BadgeWorkflowNames.SpecialArea5Approval;
                    case VisitorBadgeApprovalWorkflow.SpecialArea5Rejection:
                        return SPConfig.Instance.BadgeWorkflowNames.SpecialArea5Rejection;
                    case VisitorBadgeApprovalWorkflow.SpecialArea6Approval:
                        return SPConfig.Instance.BadgeWorkflowNames.SpecialArea6Approval;
                    case VisitorBadgeApprovalWorkflow.SpecialArea6Rejection:
                        return SPConfig.Instance.BadgeWorkflowNames.SpecialArea6Rejection;
                    case VisitorBadgeApprovalWorkflow.SpecialArea7Approval:
                        return SPConfig.Instance.BadgeWorkflowNames.SpecialArea7Approval;
                    case VisitorBadgeApprovalWorkflow.SpecialArea7Rejection:
                        return SPConfig.Instance.BadgeWorkflowNames.SpecialArea7Rejection;
                    default:
                        return null;
                }
            }
        }

    }

}
