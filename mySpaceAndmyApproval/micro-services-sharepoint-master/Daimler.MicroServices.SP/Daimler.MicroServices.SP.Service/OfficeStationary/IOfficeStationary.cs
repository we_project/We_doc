﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.Approver;
using Daimler.MicroServices.SP.BizObject.OfficeStationary;

namespace Daimler.MicroServices.SP.Service.OfficeStationary {
    public interface IOfficeStationary {

        Task LoadStationaryImgsAsync(List<StationaryMenuInfo> infoes);

        ApproveResult Approval(int userId, bool approve, int ID, string comment,out StationaryRequestInfo info);

        
    }
}
