﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.OfficeStationary;
using Daimler.MicroServices.SP.BizObject;
using Microsoft.SharePoint.Client;
using Daimler.MicroServices.SP.Service.Utils;
using log4net;
using System.IO;
using Daimler.MicroServices.SP.BizObject.Approver;
using System.Net;

namespace Daimler.MicroServices.SP.Service.OfficeStationary {
    public class OfficeStationaryImpl : IOfficeStationary, IDisposable {
        private static readonly ILog log = LogManager.GetLogger(typeof(OfficeStationaryImpl));
        public UserCredential uc { get; set; }
        AbstractSharepoint sharepoint { get; set; }

        public OfficeStationaryImpl(UserCredential uc) {
            this.uc = uc;
            sharepoint = new OfficeStationaryRequests(uc);
        }

        async Task IOfficeStationary.LoadStationaryImgsAsync(List<StationaryMenuInfo> infoes) {
            if (infoes != null && infoes.Count > 0) {
                Task<DownloadImgResult>[] tasks = infoes.Select(info => ImageHelpers.DownloadAndSaveAsync(info, AbstractSharepoint.GetCredential(uc))).ToArray();

                DownloadImgResult[] result = await Task.WhenAll<DownloadImgResult>(tasks);
                IList<DownloadImgResult> failed = result != null ? result.Where(r => !r.success).ToList() : null;
                if (failed != null && failed.Count > 0) {
                    log.ErrorFormat("some imgs load failed: {0}", string.Join(",", failed.Select(r => r.menuInfo.ID)));
                }
            }

        }
        private StationaryRequestInfo GetById(int id, AbstractSharepoint sp) {
            ListItem item = sp.GetListItemById(id);
            ModelConvertor<StationaryRequestInfo> convertor = new ModelConvertor<StationaryRequestInfo>();
            StationaryRequestInfo result = convertor.Get(item);
            return result;
        }

        //ApproveResult IOfficeStationary.Approval(int userId, bool approve, int ID, string comment, out StationaryRequestInfo info) {
        //    ApproveResult result = new ApproveResult();
        //    log.DebugFormat("user {0} is approving for {1}", userId, ID);
        //    StationaryRequestInfo_httpResponse item = sharepoint.GetListItemById<StationaryRequestInfo_httpResponse>(ID, new List<string>() { 
        //                                                        StationaryRequestInfoFields.CostCenterManagerPeopleId,
        //                                                        StationaryRequestInfoFields.AdminLocalManagerId,
        //                                                        StationaryRequestInfoFields.AdminSeniorManagerId,
        //                                                        StationaryRequestInfoFields.Status});
        //    bool isCCManager = item != null && item.d.CostCenterManagerPeopleId.HasValue ? item.d.CostCenterManagerPeopleId.Value.Equals(userId) : false;
        //    bool isALManager = item != null && item.d.AdminLocalManagerId.HasValue ? item.d.AdminLocalManagerId.Value.Equals(userId) : false;
        //    bool isASManager = item != null && item.d.AdminSeniorManagerId.HasValue ? item.d.AdminSeniorManagerId.Value.Equals(userId) : false;
        //    if (!isCCManager && !isALManager && !isASManager) {
        //        result.Msg = AllErrors.NotApprover;
        //        log.ErrorFormat("user {0} is not approver for {1}", userId, ID);
        //        info = sharepoint.GetListItemById<StationaryRequestInfo_httpResponse>(ID, StationaryRequestInfo_http.GetSPselect()).d.GetRequestInfo();
        //        return result;
        //    }

        //    string status = item.d.Status;
        //    string newStatus = null;
        //    if (status.Equals(Status.CostCenterManagerApproveInProcess) && isCCManager) {
        //        newStatus = Status.CostCenterManagerApproveCompleted;
        //    } else if (status.Equals(Status.AdminLocalManagerApproveInProcess) && isALManager) {
        //        newStatus = Status.AdminLocalManagerApproveCompleted;
        //    } else if (status.Equals(Status.AdminSeniorManagerApproveInProcess) && isASManager) {
        //        newStatus = Status.AdminSeniorManagerApproveCompleted;
        //    }

        //    if (!string.IsNullOrWhiteSpace(newStatus)) {
        //        string approveOutCome = approve ? ApproveOutCome.Approve : ApproveOutCome.Reject;
        //        ApprovalInfo approveInfo = new ApprovalInfo(OfficeStationaryRequests.ListName) {
        //            Status = newStatus,
        //            ApproveOutCome = approveOutCome,
        //            Comments = comment
        //        };
        //        string str = sharepoint.UpdateListItemById<ApprovalInfo>(ID, approveInfo);

        //        log.DebugFormat("update result: {0}", str);
        //        if (string.IsNullOrWhiteSpace(str)) {
        //            result.Success = true;
        //            log.DebugFormat("user {0} approve for {1} success", userId, ID);
        //        }
        //    }

        //    if (!result.Success) {
        //        result.Msg = AllErrors.HaveBeenApproved;
        //        log.DebugFormat("office request {0} has been approved", ID);
        //    }
        //    info = sharepoint.GetListItemById<StationaryRequestInfo_httpResponse>(ID, StationaryRequestInfo_http.GetSPselect()).d.GetRequestInfo();
        //    return result;
        //}
        #region approval old
        ApproveResult IOfficeStationary.Approval(int userId, bool approve, int ID, string comment, out StationaryRequestInfo info) {
            ApproveResult result = new ApproveResult();
            ListItem item = sharepoint.GetListItemById(ID, new List<string>() { 
                                                                StationaryRequestInfoFields.CostCenterManagerPeople,
                                                                StationaryRequestInfoFields.AdminLocalManager,
                                                                StationaryRequestInfoFields.AdminSeniorManager,
                                                                StationaryRequestInfoFields.Status});
            FieldUserValue CostCenterManagerPeople = item[StationaryRequestInfoFields.CostCenterManagerPeople] as FieldUserValue;
            FieldUserValue AdminLocalManager = item[StationaryRequestInfoFields.AdminLocalManager] as FieldUserValue;
            FieldUserValue AdminSeniorManager = item[StationaryRequestInfoFields.AdminSeniorManager] as FieldUserValue;
            bool isCCManager = CostCenterManagerPeople != null ? CostCenterManagerPeople.LookupId.Equals(userId) : false;
            bool isALManager = AdminLocalManager != null ? AdminLocalManager.LookupId.Equals(userId) : false;
            bool isASManager = AdminSeniorManager != null ? AdminSeniorManager.LookupId.Equals(userId) : false;
            if (!isCCManager && !isALManager && !isASManager) {
                result.Msg = AllErrors.NotApprover;
                log.ErrorFormat("user {0} is not approver for {1}", userId, ID);
                info = GetById(ID, sharepoint);
                return result;
            }

            string status = item[StationaryRequestInfoFields.Status] as string;
            string newStatus = null;
            if (status.Equals(Status.CostCenterManagerApproveInProcess) && isCCManager) {
                newStatus = Status.CostCenterManagerApproveCompleted;
            } else if (status.Equals(Status.AdminLocalManagerApproveInProcess) && isALManager) {
                newStatus = Status.AdminLocalManagerApproveCompleted;
            } else if (status.Equals(Status.AdminSeniorManagerApproveInProcess) && isASManager) {
                newStatus = Status.AdminSeniorManagerApproveCompleted;
            }

            if (!string.IsNullOrWhiteSpace(newStatus)) {
                string approveOutCome = approve ? ApproveOutCome.Approve : ApproveOutCome.Reject;
                item[StationaryRequestInfoFields.Status] = newStatus;
                item[StationaryRequestInfoFields.ApproveOutCome] = approveOutCome;
                item[StationaryRequestInfoFields.Comments] = comment;
                item.Update();
                sharepoint.context.ExecuteQuery();
                result.Success = true;
                log.DebugFormat("user {0} approve for {1} success", userId, ID);
            }

            if (!result.Success) {
                result.Msg = AllErrors.HaveBeenApproved;
                log.DebugFormat("office request {0} has been approved", ID);
            }
            info = GetById(ID, sharepoint);
            return result;
        } 
        #endregion

        void IDisposable.Dispose() {
            if (sharepoint != null) {
                sharepoint.Dispose();
            }
        }
    }
}
