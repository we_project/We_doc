﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.OfficeStationary;
using Daimler.MicroServices.Utils;

namespace Daimler.MicroServices.SP.Service.OfficeStationary {
    public class OfficeStationaryRequests : StationaryBase {

        public static string ListName = "OfficeStationaryRequests";
        public OfficeStationaryRequests(UserCredential uc) : base(uc) { }
        public override string listName {
            get { return ListName; }
        }
        public override List<string> FieldsToLoad {
            get {
                return _FieldsToLoad;
            }
        }

        public static List<string> _FieldsToLoad = ReflectionMethod.GetPropertyNames<StationaryRequestInfo>();
        public static string ID = "ID";
        public static string CostCenterManagerPeople = "CostCenterManagerPeople";
        public static string AdminLocalManager = "AdminLocalManager";
        public static string AdminSeniorManager = "AdminSeniorManager";
        public static string Status = "Status";
        public static string ApproveOutCome = "ApproveOutCome";
        public static string Comments = "Comments";
    }
}
