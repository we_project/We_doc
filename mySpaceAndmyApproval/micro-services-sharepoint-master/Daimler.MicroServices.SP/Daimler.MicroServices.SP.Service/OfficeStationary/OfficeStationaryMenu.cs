﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.OfficeStationary;
using Daimler.MicroServices.Utils;

namespace Daimler.MicroServices.SP.Service.OfficeStationary {
    public class OfficeStationaryMenu : StationaryBase {

        public static string ListName = "OfficeStationaryMenu";
        public OfficeStationaryMenu(UserCredential uc) : base(uc) { }
        public override string listName {
            get { return ListName; }
        }

        public override List<string> FieldsToLoad {
            get {
                return _FieldsToLoad;
            }
        }
        public static List<string> _FieldsToLoad = ReflectionMethod.GetPropertyNames<StationaryMenuInfo>();
    }
}
