﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;

namespace Daimler.MicroServices.SP.Service.OfficeStationary {
    public abstract class StationaryBase : AbstractSharepoint {
        public StationaryBase(UserCredential uc) : base(uc) { }
    }
}
