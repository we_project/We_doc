﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;

namespace Daimler.MicroServices.SP.Service.BusinessCard {
    public abstract class BusinessCardBase :AbstractSharepoint{
        public BusinessCardBase(UserCredential uc) : base(uc) { }
    }
}
