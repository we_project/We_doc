﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;

namespace Daimler.MicroServices.SP.Service.PetrolCard {
    public abstract class PetrolCardBase : AbstractSharepoint {
        public PetrolCardBase(UserCredential uc) : base(uc) { }
        public override string sharepointUrl {
            get {
                return SPConfig.Instance.FleetManagementUrl;
            }
        }
    }
}
