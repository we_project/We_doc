﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.Approver;

namespace Daimler.MicroServices.SP.Service.PetrolCard {
   public interface IPetrolCardCreditAdjustment {
       ApproveResult Approval(string supervisor, bool approve, int ID, string comment);
    }
}
