﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.Approver;
using Daimler.MicroServices.SP.BizObject.PetrolCard;
using log4net;
using Microsoft.SharePoint.Client;

namespace Daimler.MicroServices.SP.Service.PetrolCard {

    public class PetrolCardCreditAdjustmentImpl : IPetrolCardCreditAdjustment {
         private static readonly ILog log = LogManager.GetLogger(typeof(PetrolCardCreditAdjustmentImpl));
        public UserCredential uc { get; set; }

        public PetrolCardCreditAdjustmentImpl(UserCredential uc) {
            this.uc = uc;
        }

        ApproveResult IPetrolCardCreditAdjustment.Approval(string supervisor, bool approve, int ID, string comment) {
            ApproveResult result = new ApproveResult();
            using (AbstractSharepoint sharepoint = new PetrolCardCreditAdjustment(uc)) {
                ListItem item = sharepoint.GetListItemById(ID, new List<string> { PCCreditAdjustmentInfoFields.Status });
                if (item != null) {
                    string currentStatus = item[PCCreditAdjustmentInfoFields.Status] as string;
                    if (currentStatus.Equals(Status.Submitted)) {
                        if (approve) {
                            item[PCCreditAdjustmentInfoFields.Status] = Status.Approved;
                        } else {
                            item[PCCreditAdjustmentInfoFields.Status] = Status.Rejected;
                        }
                        item[PCCreditAdjustmentInfoFields.Comments] = comment;
                        item.Update();
                        sharepoint.context.ExecuteQuery();
                        result.Success = true;
                    }
                }
            }
            if (!result.Success) {
                result.Msg = AllErrors.HaveBeenApproved;
            }
            return result;
        }
    }
}
