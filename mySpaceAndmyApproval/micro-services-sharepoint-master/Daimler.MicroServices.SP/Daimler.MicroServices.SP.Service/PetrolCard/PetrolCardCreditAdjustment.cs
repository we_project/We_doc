﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.PetrolCard;
using Daimler.MicroServices.Utils;

namespace Daimler.MicroServices.SP.Service.PetrolCard {
    public class PetrolCardCreditAdjustment : PetrolCardBase {
        public static string ListName = "PetrolCard_CreditAdjustment";
        public PetrolCardCreditAdjustment(UserCredential uc) : base(uc) { }
        public override string listName {
            get { return ListName; }
        }
        public override List<string> FieldsToLoad {
            get {
                return _FieldsToLoad;
            }
        }

        public static List<string> _FieldsToLoad = ReflectionMethod.GetPropertyNames<PCCreditAdjustmentInfo>();
    }
}
