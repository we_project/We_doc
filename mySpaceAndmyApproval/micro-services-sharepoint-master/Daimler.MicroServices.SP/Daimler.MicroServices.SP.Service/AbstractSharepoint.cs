﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Security;
using System.Runtime.CompilerServices;
using System.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.Service.AccessManagement;
using Daimler.MicroServices.Utils.Network;
using Daimler.MicroServices.Utils.Network.Sharepoint;
using log4net;
using Microsoft.SharePoint.Client;

namespace Daimler.MicroServices.SP.Service {
    public abstract class AbstractSharepoint : IDisposable {
        private static readonly ILog log = LogManager.GetLogger(typeof(AbstractSharepoint));

        public AbstractSharepoint(UserCredential uc) {
            this.uc = uc;
        }
        //private static bool customXertificateValidation(object sender, X509Certificate cert, X509Chain chain, SslPolicyErrors error) {
        //    log.Debug("customXertificateValidation occurs");
        //    //log.Debug(s);
        //    //log.Debug(certificate);
        //    //log.Debug(chain);
        //    log.Debug(error);
        //    return true;
        //}
        static AbstractSharepoint() {
            //ServicePointManager.ServerCertificateValidationCallback = ((object sender1, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) => {
            //    log.Debug("ServicePointManager.ServerCertificateValidationCallback occurs");
            //    //log.Debug(s);
            //    //log.Debug(certificate);
            //    //log.Debug(chain);
            //    log.Debug(sslPolicyErrors);
            //    return true;
            //});
            //ServicePointManager.ServerCertificateValidationCallback = (RemoteCertificateValidationCallback)Delegate.Combine(ServicePointManager.ServerCertificateValidationCallback, new RemoteCertificateValidationCallback(customXertificateValidation));
        }
        public virtual string sharepointUrl {
            get {
                return SPConfig.Instance.CommonUrl;
            }
        }
        protected UserCredential uc;

        public static NetworkCredential GetCredential(UserCredential uc) {
            SecureString password = new SecureString();
            foreach (char c in uc.psw) {
                password.AppendChar(c);
            }
            NetworkCredential credential = new NetworkCredential(uc.userName, password, uc.domain);
            return credential;
        }
        public abstract string listName { get; }

        private ClientContext _context;
        public ClientContext context {
            get {
                if (_context == null) {
                    _context = new ClientContext(sharepointUrl);
                    _context.Credentials = GetCredential(uc);
                    _context.ExecutingWebRequest += _context_ExecutingWebRequest;
                }
                return _context;
            }
        }

        void _context_ExecutingWebRequest(object sender, WebRequestEventArgs e) {
            HttpWebRequest request = e.WebRequestExecutor.WebRequest;
            request.ServerCertificateValidationCallback = (s, certificate, chain, sslPolicyErrors) => {
                if (!sslPolicyErrors.Equals(SslPolicyErrors.None)) {
                    string chainStatus = null;
                    if (chain != null && chain.ChainStatus != null && chain.ChainStatus.Count() > 0) {
                        foreach (var item in chain.ChainStatus) {
                            chainStatus += item.Status + "\n" + item.StatusInformation + "\n";
                        }
                    }
                    log.Warn(new {
                        label = "ServerCertificateValidationCallback",
                        s = s,
                        certificate = certificate,
                        chain = chainStatus,
                        sslPolicyErrors = sslPolicyErrors
                    });
                }
                return true;
            };


            //X509Certificate value = X509Certificate.CreateFromCertFile(ConfigManager.spCerFileFullPath);
            //request.ClientCertificates.Add(value);
            //ServicePointManager.CheckCertificateRevocationList
            //request.UnsafeAuthenticatedConnectionSharing = true;
            //request.ServicePoint.Expect100Continue = false;
            //request.AuthenticationLevel = AuthenticationLevel.None;
            //request.Credentials = GetCredential(uc);
            if (SPConfig.Instance.UseProxy) {
                log.Info(new {
                    listName = listName,
                    proxy = true
                });
                request.Proxy = new System.Net.WebProxy(SPConfig.Instance.Proxy, true);
            } else {
                log.Info(new {
                    listName = listName,
                    proxy = false
                });
            }
        }
        public ListCollection Lists {
            get {
                return context.Web.Lists;
            }
        }
        public List List {
            get {
                return Lists.GetByTitle(listName);
            }
        }
        public FieldCollection Fields {
            get {
                return List.Fields;
            }
        }
        public abstract List<string> FieldsToLoad {
            get;
        }



        /// <summary>
        /// get list items by query
        /// </summary>
        /// <param name="query"></param>
        public ListItemCollection GetListItems(CamlQuery query, List<string> fields = null) {
            log.Info(new {
                sharepointUrl = sharepointUrl,
                listName = listName,
                action = "GetListItems"
            });
            ListItemCollection ListItems = List.GetItems(query);
            fields = (fields != null && fields.Count > 0) ? fields : FieldsToLoad;
            var expression = ManualFields(fields);
            context.Load(ListItems,
                        items => items.Include(expression));
            context.ExecuteQuery();
            log.Info(
                new {
                    sharepointUrl = sharepointUrl,
                    listName = listName,
                    action = "GetListItems",
                    result = "load successfully",
                    fields = fields
                });
            log.Debug(PrintListItems(ListItems, fields));
            return ListItems;
        }
        public ListItemCollection GetListItems(List list, CamlQuery query, List<string> fields = null) {
            log.Info(listName + ": " + sharepointUrl);
            ListItemCollection ListItems = list.GetItems(query);
            fields = (fields != null && fields.Count > 0) ? fields : FieldsToLoad;
            var expression = ManualFields(fields);
            context.Load(ListItems,
                        items => items.Include(expression));
            context.ExecuteQuery();
            log.Debug(PrintListItems(ListItems, fields));
            return ListItems;
        }

        public ListItemCollection GetListItems(List<string> fields = null) {
            return GetListItems(CamlQuery.CreateAllItemsQuery(), fields);
        }

        public ListItem GetListItemById(int id, List<string> fields = null) {
            log.Info(listName + ": " + sharepointUrl);
            log.Info("GetListItemById: " + id);
            ListItem item = List.GetItemById(id);
            fields = (fields != null && fields.Count > 0) ? fields : FieldsToLoad;
            var expression = ManualFields(fields);
            context.Load(item, expression);
            context.ExecuteQuery();
            log.Info("GetListItemById: " + id + " successfully");
            log.Debug(PrintListItem(item));
            return item;
        }

        //public T GetListItemsWithAllFields<T>() {
        //    log.Info(new {
        //        action = "GetListItemsWithAllFields",
        //        listName = listName,
        //        sharepointUrl = sharepointUrl,
        //    });
        //    string url = string.Format(@"{0}/_api/web/lists/getbytitle('{1}')/items", sharepointUrl, listName);
        //    url += "?$select=*";
        //    T result = SPHttpHelper_Json.GetJson<T>(url, GetNetworkCredential(url));
        //    log.Info(new {
        //        action = "GetListItems",
        //        listName = listName,
        //        sharepointUrl = sharepointUrl,
        //        successfully = true
        //    });
        //    return result;
        //}
        //public T GetListItems<T>(List<string> fields = null) {
        //    log.Info(new {
        //        action = "GetListItems",
        //        listName = listName,
        //        sharepointUrl = sharepointUrl,
        //    });
        //    string url = string.Format(@"{0}/_api/web/lists/getbytitle('{1}')/items", sharepointUrl, listName);
        //    if (fields != null) {
        //        url += "?$select=" + string.Join(",", fields);
        //    } else {
        //        url += "?$select=" + string.Join(",", FieldsToLoad);
        //    }
        //    T result = SPHttpHelper_Json.GetJson<T>(url, GetNetworkCredential(url));
        //    log.Info(new {
        //        action = "GetListItems",
        //        listName = listName,
        //        sharepointUrl = sharepointUrl,
        //        successfully = true
        //    });
        //    return result;
        //}
        public T GetListItemById<T>(int id, List<string> fields = null) {
            log.Info(listName + ": " + sharepointUrl);
            log.Info("GetListItemById: " + id);
            string url = string.Format(@"{0}/_api/web/lists/getbytitle('{1}')/items({2})", sharepointUrl, listName, id);
            if (fields != null) {
                url += "?$select=" + string.Join(",", fields);
            } else {
                url += "?$select=" + string.Join(",", FieldsToLoad);
            }
            T result = HttpHelper_Json.GetJson<T>(url, GetNetworkCredential(url), SPConfig.Instance.UseProxy, SPConfig.Instance.UseProxy ? new System.Net.WebProxy(SPConfig.Instance.Proxy, true) : null);
            log.Info("GetListItemById: " + id + " successfully");

            return result;
        }
        public T GetListItemById<T>(int id, string select) {
            log.Info(listName + ": " + sharepointUrl);
            log.Info("GetListItemById: " + id);
            string url = string.Format(@"{0}/_api/web/lists/getbytitle('{1}')/items({2})", sharepointUrl, listName, id);
            url += select;
            T result = HttpHelper_Json.GetJson<T>(url, GetNetworkCredential(url), SPConfig.Instance.UseProxy, SPConfig.Instance.UseProxy ? new System.Net.WebProxy(SPConfig.Instance.Proxy, true) : null);
            log.Info("GetListItemById: " + id + " successfully");
            return result;
        }
        private CredentialCache _cc;
        public CredentialCache cc {
            get {
                if (_cc == null) {
                    _cc = new CredentialCache();
                }
                return _cc;
            }
        }

        public CredentialCache GetNetworkCredential(string address) {
            NetworkCredential c = cc.GetCredential(new Uri(address), "NTLM");
            if (c == null) {
                log.Debug("NetworkCredential is null");
                cc.Add(new Uri(address), "NTLM", GetCredential(uc));
            }
            return cc;
        }
        public string UpdateListItemById<T>(int id, T post) {
            log.Info(listName + ": " + sharepointUrl);
            log.Info("UpdateListItemById: " + id);
            string url = string.Format(@"{0}/_api/web/lists/getbytitle('{1}')/items({2})", sharepointUrl, listName, id);
            string result = HttpHelper_Json.PostJson<string, T>(url, post, GetNetworkCredential(url), GetFormDigest(), SPConfig.Instance.UseProxy, SPConfig.Instance.UseProxy ? new System.Net.WebProxy(SPConfig.Instance.Proxy, true) : null);
            log.Info("UpdateListItemById: " + id + " successfully");
            return result;
        }

        public string GetFormDigest() {
            string formDigest = null;

            string resourceUrl = sharepointUrl + "/_api/contextinfo";
            digestResponse response = HttpHelper_Json.PostJson<digestResponse, string>(resourceUrl, null, GetNetworkCredential(resourceUrl), null, SPConfig.Instance.UseProxy, SPConfig.Instance.UseProxy ? new System.Net.WebProxy(SPConfig.Instance.Proxy, true) : null);
            return response.d.GetContextWebInformation.FormDigestValue;

        }
        public User GetCurrentUser() {
            var user = context.Web.CurrentUser;
            context.Load(user);
            context.ExecuteQuery();
            return user;
        }

        public bool IsLoginViaHttp() {
            log.InfoFormat("{0} is trying to login to sharepoint.", uc);
            string url = string.Format(@"{0}/_api/web", sharepointUrl);
            var user = context.Web.CurrentUser;
            Expression<Func<User, object>> expression = u => u.Id;

            context.Load(user, expression);
            context.ExecuteQuery();
            log.InfoFormat("{0} login to sharepoint successfully.", uc);
            return true;
        }
        public bool IsLogin() {
            log.InfoFormat("{0} is trying to login to sharepoint.", uc);
            var user = context.Web.CurrentUser;
            Expression<Func<User, object>> expression = u => u.Id;

            context.Load(user, expression);
            context.ExecuteQuery();
            log.InfoFormat("{0} login to sharepoint successfully.", uc);
            return true;
        }
        public ListItemCollection GetAllUsers() {
            return GetListItems(context.Web.SiteUserInfoList, CamlQuery.CreateAllItemsQuery());
        }
        public void GetUserUsefulFields() {
            //var users = context.Web.SiteUserInfoList.GetItems(CamlQuery.CreateAllItemsQuery());
            GetUsefulFields(context.Web.SiteUserInfoList.Fields);
        }


        #region RestfulMethods
        /// <summary>
        /// Restful Method for get all items of list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public async Task<List<T>> GetAllItemsAsync<T>() {
            string url = RestfulUrlHelper.GetListItemsUrl(sharepointUrl, listName);
            string select = RestfulUrlHelper.GetSelectPart(FieldsToLoad);
            url += "?" + select;
            log.Info(new { url = url });
            IEnumerable<T> result = await SPHttpHelper_Json.GetAllItemsJsonAsync<T>(url, GetNetworkCredential(url));
            return result != null ? result.ToList() : null;
        }
        public async Task<List<T>> GetAllItemsAsyncV2<T>(string filter = null) {
            int itemcount = await GetItemsCount();
            if (itemcount > 0) {
                log.Info(new {
                    sharepointUrl = sharepointUrl,
                    listName = listName,
                    itemCount = itemcount
                });
                return await GetItemsAsync<T>(itemcount, filter);
            }
            return null;
        }
        public async Task<List<T>> GetItemsAsync<T>(int top, string filter = null) {
            string url = RestfulUrlHelper.GetListItemsUrl(sharepointUrl, listName);
            string select = RestfulUrlHelper.GetSelectPart(FieldsToLoad);
            string top_ = RestfulUrlHelper.GetTopPart(top);
            string filter_ = RestfulUrlHelper.GetFilterPart(filter);
            url += "?" + select;
            url += "&" + top_;
            url += "&" + filter_;
            log.Info(new { url = url });
            IEnumerable<T> result = await SPHttpHelper_Json.GetJsonAsync<T>(url, GetNetworkCredential(url));
            return result != null ? result.ToList() : null;
        }
        public async Task<int> GetItemsCount() {
            string url = RestfulUrlHelper.GetListItemsCountUrl(sharepointUrl, listName);
            log.Info(new { url = url });
            ItemCountResult result = await SPHttpHelper_Json.GetJsonObjAsync<ItemCountResult>(url, GetNetworkCredential(url));
            return result != null ? result.ItemCount : 0;
        }
        /// <summary>
        /// Restful Method for get item by Id
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<T> GetItemByIdAsync<T>(int Id) {
            string url = RestfulUrlHelper.GetItemUrl(sharepointUrl, listName, Id);
            string select = RestfulUrlHelper.GetSelectPart(FieldsToLoad);
            url += "?" + select;
            log.Info(new { url = url });
            T result = await SPHttpHelper_Json.GetJsonObjAsync<T>(url, GetNetworkCredential(url));
            return result;
        }
        #endregion

        #region Helpers

        public static Expression<Func<ListItem, object>>[] ManualFields(List<string> fields) {
            Expression<Func<ListItem, object>>[] result = fields.Select(field => {
                Expression<Func<ListItem, object>> expression = item => item[field];
                return expression;
            }).ToArray();
            return result;
        }
        public void PrintAllLists() {
            context.Load(Lists);
            context.ExecuteQuery();

            foreach (List list in Lists) {
                log.Debug(list.Title);
            }
        }
        private string PrintListItems(ListItemCollection list, List<string> fields) {
            StringBuilder sb = new StringBuilder();
            foreach (ListItem item in list) {
                sb.AppendLine(PrintListItem(item));
            }
            return sb.ToString();
        }

        public string PrintListItem(ListItem item) {
            StringBuilder sb = new StringBuilder();
            Dictionary<string, object> values = item.FieldValues;
            foreach (var i in item.FieldValues) {
                object v = i.Value;
                if (v is FieldLookupValue) {
                    FieldLookupValue flv = v as FieldLookupValue;
                    sb.AppendFormat("{0}: {1}_{2}\n", i.Key, flv.LookupId, flv.LookupValue);
                } else if (v is FieldUserValue) {
                    FieldUserValue fuv = v as FieldUserValue;
                    sb.AppendFormat("{0}: {1}_{2}\n", i.Key, fuv.LookupId, fuv.LookupValue);
                } else if (v is FieldUserValue[]) {
                    FieldUserValue[] fuvs = v as FieldUserValue[];
                    sb.AppendFormat("{0}: {1}\n", i.Key, string.Join(",", fuvs.Select(fuv => string.Format("{0}_{1}", fuv.LookupId, fuv.LookupValue))));
                } else if (v is FieldUrlValue) {
                    FieldUrlValue fuv = v as FieldUrlValue;
                    sb.AppendFormat("{0}: {1}\n", i.Key, string.Join(",", fuv.Url, fuv.Description));
                } else {
                    sb.AppendFormat("{0}: {1}\n", i.Key, i.Value);
                }
            }
            return sb.ToString();
        }
        #endregion

        public void Dispose() {
            if (_context != null) {
                _context.Dispose();
            }
        }

        #region Experimental method
        /// <summary>
        /// load all fields
        /// </summary>
        public void PrintField(Field f) {
            log.Debug(new {
                DefaultValue = f.DefaultValue,
                Description = f.Description,
                EntityPropertyName = f.EntityPropertyName,
                InternalName = f.InternalName,
                Title = f.Title,
                TypeAsString = f.TypeAsString,
                TypeDisplayName = f.TypeDisplayName,
                AdditionalInfo = GetAdditionalInfoByType(f),
            });
        }

        /// <summary>
        /// get first item
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        public ListItemCollection GetListItems(int count) {
            context.Load(Fields);
            context.ExecuteQuery();

            List<string> UsefulFields = new List<string>();
            var expressions = ExcludeSystemFieldsExpressions(Fields, out UsefulFields);
            ListItemCollection items = List.GetItems(CamlQuery.CreateAllItemsQuery(count));
            context.Load(items,
                liteitems => liteitems.Include(expressions));
            context.ExecuteQuery();
            if (items != null && items.Count > 0) {
                log.Info(items.Count + " data loaded");
                foreach (ListItem item in items) {
                    log.Debug(PrintListItem(item));
                }
            } else {
                log.Info("no data loaded");
            }
            return items;
        }



        /// <summary>
        /// get useful fields for data
        /// </summary>
        public void GetUsefulFields() {
            context.Load(Fields,
                        fields => fields.Include(
                                                f => f.DefaultValue,
                                                f => f.Description,
                                                f => f.EntityPropertyName,
                                                f => f.InternalName,
                                                f => f.Title,
                                                f => f.TypeAsString,
                                                f => f.TypeDisplayName,
                                                f => f.FieldTypeKind));
            context.ExecuteQuery();
            foreach (var item in Fields) {
                if (!SharepointSystemFields.Default.Contains(item.InternalName)) {
                    PrintField(item);
                }
            }

            StringBuilder sb = new StringBuilder();
            foreach (var item in Fields) {
                if (!SharepointSystemFields.Default.Contains(item.InternalName)) {
                    sb.AppendLine(PrintFieldToCSharpFormat(item));
                }
            }
            log.Debug(sb.ToString());

        }

        /// <summary>
        /// get useful fields for data
        /// </summary>
        public void GetUsefulFields(FieldCollection Fields) {
            context.Load(Fields,
                        fields => fields.Include(
                                                f => f.DefaultValue,
                                                f => f.Description,
                                                f => f.EntityPropertyName,
                                                f => f.InternalName,
                                                f => f.Title,
                                                f => f.TypeAsString,
                                                f => f.TypeDisplayName,
                                                f => f.FieldTypeKind));
            context.ExecuteQuery();
            foreach (var item in Fields) {
                if (!SharepointSystemFields.Default.Contains(item.InternalName)) {
                    PrintField(item);
                }
            }

            StringBuilder sb = new StringBuilder();
            foreach (var item in Fields) {
                if (!SharepointSystemFields.Default.Contains(item.InternalName)) {
                    sb.AppendLine(PrintFieldToCSharpFormat(item));
                }
            }
            log.Debug(sb.ToString());

        }
        #region Helpers
        public static Expression<Func<ListItem, object>>[] ExcludeSystemFieldsExpressions(FieldCollection fields, out List<string> UsefulFields) {
            IEnumerable<string> fieldsInternalNames = fields.AsEnumerable().Select(item => item.InternalName);
            UsefulFields = fieldsInternalNames.Except(SharepointSystemFields.Default).ToList();
            Expression<Func<ListItem, object>>[] result = UsefulFields.Select(field => {
                Expression<Func<ListItem, object>> expression = item => item[field];
                return expression;
            }).ToArray();
            return result;
        }
        public string PrintFieldToCSharpFormat(Field f) {
            StringBuilder sb = new StringBuilder();
            string type = "string";

            switch (f.FieldTypeKind) {
                case FieldType.AllDayEvent:
                    break;
                case FieldType.Attachments:
                    break;
                case FieldType.Boolean:
                    type = Constants.CSharpTypes._bool;
                    break;
                case FieldType.Calculated:
                    type = Constants.CSharpTypes._double;
                    break;
                case FieldType.Choice:
                    break;
                case FieldType.Computed:
                    break;
                case FieldType.ContentTypeId:
                    break;
                case FieldType.Counter:
                    type = Constants.CSharpTypes._int;
                    break;
                case FieldType.CrossProjectLink:
                    break;
                case FieldType.Currency:
                    break;
                case FieldType.DateTime:
                    type = Constants.CSharpTypes._dateTime;
                    break;
                case FieldType.Error:
                    break;
                case FieldType.File:
                    break;
                case FieldType.Geolocation:
                    break;
                case FieldType.GridChoice:
                    break;
                case FieldType.Guid:
                    break;
                case FieldType.Integer:
                    type = Constants.CSharpTypes._int;
                    break;
                case FieldType.Invalid:
                    break;
                case FieldType.Lookup:
                    type = Constants.CSharpTypes._int;
                    break;
                case FieldType.MaxItems:
                    break;
                case FieldType.ModStat:
                    break;
                case FieldType.MultiChoice:
                    break;
                case FieldType.Note:
                    break;
                case FieldType.Number:
                    type = Constants.CSharpTypes._double;
                    break;
                case FieldType.OutcomeChoice:
                    break;
                case FieldType.PageSeparator:
                    break;
                case FieldType.Recurrence:
                    break;
                case FieldType.Text:
                    break;
                case FieldType.ThreadIndex:
                    break;
                case FieldType.Threading:
                    break;
                case FieldType.URL:
                    break;
                case FieldType.User:
                    type = Constants.CSharpTypes._int;
                    break;
                case FieldType.WorkflowEventType:
                    break;
                case FieldType.WorkflowStatus:
                    type = Constants.CSharpTypes._int;
                    break;
                default:
                    break;
            }
            if (!string.IsNullOrWhiteSpace(type)) {
                sb.AppendFormat("public {0} {1}{2}\n", type, f.InternalName, "{get;set;}");
            }
            return sb.ToString();
        }


        public string GetAdditionalInfoByType(Field f) {
            StringBuilder sb = new StringBuilder();
            switch (f.FieldTypeKind) {
                case FieldType.AllDayEvent:
                    break;
                case FieldType.Attachments:
                    break;
                case FieldType.Boolean:
                    break;
                case FieldType.Calculated:
                    break;
                case FieldType.Choice:
                    FieldChoice choice = context.CastTo<FieldChoice>(f);
                    context.Load(choice);
                    context.ExecuteQuery();
                    sb.AppendLine(string.Join("\n", choice.Choices));
                    break;
                case FieldType.Computed:
                    break;
                case FieldType.ContentTypeId:
                    break;
                case FieldType.Counter:
                    break;
                case FieldType.CrossProjectLink:
                    break;
                case FieldType.Currency:
                    break;
                case FieldType.DateTime:
                    break;
                case FieldType.Error:
                    break;
                case FieldType.File:
                    break;
                case FieldType.Geolocation:
                    break;
                case FieldType.GridChoice:
                    break;
                case FieldType.Guid:
                    break;
                case FieldType.Integer:
                    break;
                case FieldType.Invalid:
                    break;
                case FieldType.Lookup:
                    break;
                case FieldType.MaxItems:
                    break;
                case FieldType.ModStat:
                    break;
                case FieldType.MultiChoice:
                    break;
                case FieldType.Note:
                    break;
                case FieldType.Number:
                    break;
                case FieldType.OutcomeChoice:
                    break;
                case FieldType.PageSeparator:
                    break;
                case FieldType.Recurrence:
                    break;
                case FieldType.Text:
                    FieldText textField = context.CastTo<FieldText>(f);
                    context.Load(textField);
                    context.ExecuteQuery();
                    sb.AppendFormat("MaxLength: {0}\n", textField.MaxLength);
                    break;
                case FieldType.ThreadIndex:
                    break;
                case FieldType.Threading:
                    break;
                case FieldType.URL:
                    break;
                case FieldType.User:
                    break;
                case FieldType.WorkflowEventType:
                    break;
                case FieldType.WorkflowStatus:
                    break;
                default:
                    break;
            }
            return sb.ToString();
        }
        #endregion

        #endregion

    }
}
