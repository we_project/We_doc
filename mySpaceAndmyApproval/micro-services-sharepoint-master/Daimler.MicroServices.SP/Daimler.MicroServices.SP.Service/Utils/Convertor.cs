﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint.Client;

namespace Daimler.MicroServices.SP.Service.Utils {
    public class Convertor<T> {
        ModelConvertor<T> convertor = new ModelConvertor<T>();
        public List<T> GetInfoes(ListItemCollection items) {
            List<T> result = null;
            if (items != null && items.Count > 0) {
                result = new List<T>();
                foreach (ListItem item in items) {
                    T t = convertor.Get(item);
                    result.Add(t);
                }
            }
            return result;
        }


    }
}
