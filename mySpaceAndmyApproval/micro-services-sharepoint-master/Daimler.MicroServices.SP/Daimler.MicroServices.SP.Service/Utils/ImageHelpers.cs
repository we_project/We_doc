﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.OfficeStationary;
using log4net;

namespace Daimler.MicroServices.SP.Service.Utils {
    public class ImageHelpers {
        private static readonly ILog log = LogManager.GetLogger(typeof(ImageHelpers));
        public static bool DownloadAndSave(string url, string savepath, NetworkCredential credential) {
            using (WebClient client = new WebClient()) {
                if (SPConfig.Instance.UseProxy) {
                    client.Proxy = new WebProxy(SPConfig.Instance.Proxy);
                }
                ServicePointManager.ServerCertificateValidationCallback = (s, certificate, chain, sslPolicyErrors) => true;
                client.Credentials = credential;
                client.DownloadFile(url, savepath);
            }
            return true;
        }
        public static async Task<DownloadImgResult> DownloadAndSaveAsync(StationaryMenuInfo info, NetworkCredential credential) {
            DownloadImgResult result = new DownloadImgResult();
            try {
                result.menuInfo = info;
                using (WebClient client = new WebClient()) {
                    log.Debug("begin load img of id: " + info.ID);
                    if (SPConfig.Instance.UseProxy) {
                        client.Proxy = new WebProxy(SPConfig.Instance.Proxy);
                    }
                    ServicePointManager.ServerCertificateValidationCallback = (s, certificate, chain, sslPolicyErrors) => true;
                    client.Credentials = credential;
                    string fName = info.ID + ".jpg";
                    string filename = Path.Combine(SPConfig.StationaryImagesLocalPath, fName);
                    await client.DownloadFileTaskAsync(info.ThumbnailExists ? info.EncodedAbsThumbnailUrl : info.EncodedAbsWebImgUrl, filename);
                    result.success = true;
                    log.Debug("end load img of id: " + info.ID);
                }
            } catch (Exception ex) {
                log.Error("load img of id: " + info.ID, ex);
            }
            return result;
        }
    }
}
