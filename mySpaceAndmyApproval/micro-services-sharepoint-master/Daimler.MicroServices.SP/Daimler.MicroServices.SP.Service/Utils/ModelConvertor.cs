﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint.Client;
using log4net;
using Daimler.MicroServices.SP.BizObject.PetrolCard;

namespace Daimler.MicroServices.SP.Service.Utils {
    public class ModelConvertor<T> {
        private static readonly ILog log = LogManager.GetLogger(typeof(ModelConvertor<T>));
        public T Get(Dictionary<string, object> values) {
            Type type = typeof(T);
            T info = (T)Activator.CreateInstance(type);
            foreach (PropertyInfo property in type.GetProperties()) {
                try {
                    object v = values[property.Name];
                    if (v != null) {
                        if (v is FieldLookupValue) {
                            FieldLookupValue flv = v as FieldLookupValue;
                            property.SetValue(info, flv.LookupId);
                        } else if (v is FieldUserValue) {
                            FieldUserValue fuv = v as FieldUserValue;
                            property.SetValue(info, fuv.LookupId);
                        } else if (v is FieldUserValue[]) {
                            FieldUserValue[] fuvs = v as FieldUserValue[];
                            property.SetValue(info, string.Join(",", fuvs.Select(fuv => fuv.LookupId)));
                        } else if (v is FieldUrlValue) {
                            FieldUrlValue fuv = v as FieldUrlValue;
                            property.SetValue(info, fuv.Url);
                        } else {
                            property.SetValue(info, v);
                        }
                    }
                } catch (Exception ex) {
                    log.Error(property.Name, ex);
                    continue;
                }
            }
            //log.Debug("Convert complete.");
            //log.Debug(info.ToString());
            return info;
        }

        public T Get(ListItem item) {
            Dictionary<string, object> values = item.FieldValues;
            Type type = typeof(T);
            T info = (T)Activator.CreateInstance(type);
            foreach (PropertyInfo property in type.GetProperties()) {
                try {

                    //if (property.Name.Equals("DisplayName") && typeof(T).Equals(typeof(PCCreditAdjustmentInfo))) {
                    //    property.SetValue(info, item["User Name"]);
                    //    continue;
                    //}

                    if (typeof(T).Equals(typeof(PCCreditAdjustmentInfo))) {
                        if (property.Name.Equals("DisplayName")) {
                            property.SetValue(info, item.DisplayName);
                            continue;
                        } else if (property.Name.Equals("PlateNumber")) {
                            object PlateNumber = values[property.Name];
                            if (PlateNumber != null && PlateNumber is FieldLookupValue) {
                                FieldLookupValue flv = PlateNumber as FieldLookupValue;
                                //log.DebugFormat("PCCreditAdjustmentInfo PlateNumber: {0}", flv.LookupValue);
                                property.SetValue(info, flv.LookupValue);
                            }
                            continue;
                        }
                    }

                    object v = values[property.Name];
                    if (v != null) {
                        if (v is FieldLookupValue) {
                            FieldLookupValue flv = v as FieldLookupValue;
                            property.SetValue(info, flv.LookupId);
                        } else if (v is FieldUserValue) {
                            FieldUserValue fuv = v as FieldUserValue;
                            property.SetValue(info, fuv.LookupId);
                        } else if (v is FieldUserValue[]) {
                            FieldUserValue[] fuvs = v as FieldUserValue[];
                            property.SetValue(info, string.Join(",", fuvs.Select(fuv => fuv.LookupId)));
                        } else if (v is FieldUrlValue) {
                            FieldUrlValue fuv = v as FieldUrlValue;
                            property.SetValue(info, fuv.Url);
                        } else if (v is FieldCalculatedErrorValue) {
                            FieldCalculatedErrorValue fuv = v as FieldCalculatedErrorValue;
                            property.SetValue(info, null);
                        } else {
                            property.SetValue(info, v);
                        }
                    }
                } catch (Exception ex) {
                    log.Error(property.Name, ex);
                    continue;
                }
            }
            //log.Debug("Convert complete.");
            //log.Debug(info.ToString());
            return info;
        }
    }
}
