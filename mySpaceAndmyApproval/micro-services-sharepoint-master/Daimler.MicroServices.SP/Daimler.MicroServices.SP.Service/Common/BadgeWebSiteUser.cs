﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.Utils;

namespace Daimler.MicroServices.SP.Service.Common {
    public class BadgeWebSiteUser : AbstractSharepoint {
        public BadgeWebSiteUser(UserCredential uc)
            : base(uc) {

        }
        public override string sharepointUrl {
            get {
                return SPConfig.Instance.AccessManagementUrl;
            }
        }
        public override string listName {
            get { return "BadgeWebSiteUser"; }
        }

        public override List<string> FieldsToLoad {
            get {
                return _FieldsToLoad;
            }
        }
        public static List<string> _FieldsToLoad = ReflectionMethod.GetPropertyNames<SPUserInfo>();
    }
}
