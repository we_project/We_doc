﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.Utils;

namespace Daimler.MicroServices.SP.Service.Common {
    public class FormABWebSiteUser : AbstractSharepoint {
        public FormABWebSiteUser(UserCredential uc)
            : base(uc) {

        }
        public override string sharepointUrl {
            get {
                return SPConfig.Instance.FormAFormBUrl;
            }
        }
        public override string listName {
            get { return "FormABWebSiteUser"; }
        }

        public override List<string> FieldsToLoad {
            get {
                return _FieldsToLoad;
            }
        }
        public static List<string> _FieldsToLoad = ReflectionMethod.GetPropertyNames<SPUserInfo>();
    }
}
