﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.Service.OfficeStationary;
using Microsoft.SharePoint.Client;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.OfficeStationary;
using log4net;
using Daimler.MicroServices.SP.Service.Utils;
using Daimler.MicroServices.SP.BizObject.FormAB;

namespace Daimler.MicroServices.SP.Service.Common {
    public class CommonImpl : ICommon {
        private static readonly ILog log = LogManager.GetLogger(typeof(CommonImpl));
        public UserCredential uc { get; set; }

        public CommonImpl(UserCredential uc) {
            this.uc = uc;
        }
        public List<T> GetListItems<T>(List<string> fields = null) {
            using (AbstractSharepoint sp = SPRepository.Create<T>(uc)) {
                if (sp == null) {
                    return null;
                }
                Convertor<T> convertor = new Convertor<T>();
                ListItemCollection items = sp.GetListItems(fields);
                List<T> result = convertor.GetInfoes(items);
                log.InfoFormat("GetListItems of type({0}) count: {1}", typeof(T), (result != null ? result.Count : 0));
                return result;
            }
        }



        public void GetUsefulFields<T>() {
            AbstractSharepoint sp = SPRepository.Create<T>(uc);
            if (sp == null) {
                return;
            }
            sp.GetUsefulFields();
        }


        public async Task<List<T>> GetListItemsAsync<T>() {
            Task<List<T>> task = Task.Factory.StartNew<List<T>>(() => {
                return GetListItems<T>();
            });
            return await task;
        }


        public bool Login() {
            bool result = false;
            using (AbstractSharepoint sharepoint = SPRepository.Create<FormAInfo>(uc)) {
                //sharepoint.GetCurrentUser();
                //result = true;
                result = sharepoint.IsLogin();
            }
            return result;
        }


        public void GetUserUsefulFields() {
            using (AbstractSharepoint sharepoint = SPRepository.Create<StationaryRequestInfo>(uc)) {
                sharepoint.GetUserUsefulFields();
            }
        }
        public List<SPUserInfo> GetAllUsers() {
            using (AbstractSharepoint sp = new WebSiteUser(uc)) {
                Convertor<SPUserInfo> convertor = new Convertor<SPUserInfo>();
                ListItemCollection items = sp.GetAllUsers();
                List<SPUserInfo> result = convertor.GetInfoes(items);
                log.InfoFormat("GetAllUsers count: {0}", (result != null ? result.Count : 0));
                return result;
            }
        }

        public async Task<List<SPUserInfo>> GetAllUsersAsync() {
            Task<List<SPUserInfo>> task = Task.Factory.StartNew<List<SPUserInfo>>(() => {
                return GetAllUsers();
            });
            return await task;
        }
        public List<SPUserInfo> GetAllFormABUsers() {
            using (AbstractSharepoint sp = new FormABWebSiteUser(uc)) {
                Convertor<SPUserInfo> convertor = new Convertor<SPUserInfo>();
                ListItemCollection items = sp.GetAllUsers();
                List<SPUserInfo> result = convertor.GetInfoes(items);
                log.InfoFormat("GetAllFormABUsers count: {0}", (result != null ? result.Count : 0));
                return result;
            }
        }

        public async Task<List<SPUserInfo>> GetAllFormABUsersAsync() {
            Task<List<SPUserInfo>> task = Task.Factory.StartNew<List<SPUserInfo>>(() => {
                return GetAllFormABUsers();
            });
            return await task;
        }


        public List<SPUserInfo> GetAllBadgeUsers() {
            using (AbstractSharepoint sp = new BadgeWebSiteUser(uc)) {
                Convertor<SPUserInfo> convertor = new Convertor<SPUserInfo>();
                ListItemCollection items = sp.GetAllUsers();
                List<SPUserInfo> result = convertor.GetInfoes(items);
                log.InfoFormat("GetAllBadgeUsers count: {0}", (result != null ? result.Count : 0));
                return result;
            }
        }

        public async Task<List<SPUserInfo>> GetAllBadgeUsersAsync() {
            Task<List<SPUserInfo>> task = Task.Factory.StartNew<List<SPUserInfo>>(() => {
                return GetAllBadgeUsers();
            });
            return await task;
        }

        public T GetListItemById<T>(int id) {
            using (AbstractSharepoint sp = SPRepository.Create<T>(uc)) {
                if (sp == null) {
                    return default(T);
                }
                //sp.context.Credentials = AbstractSharepoint.GetCredential(uc);
                ListItem item = sp.GetListItemById(id);
                ModelConvertor<T> convertor = new ModelConvertor<T>();
                T result = convertor.Get(item);
                log.InfoFormat("GetListItemById of type({0}) :{1}", typeof(T), result != null ? result.ToString() : null);
                return result;
            }
        }


    }
}
