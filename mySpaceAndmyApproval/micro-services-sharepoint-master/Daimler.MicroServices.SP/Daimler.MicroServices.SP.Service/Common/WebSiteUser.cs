﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.Utils;

namespace Daimler.MicroServices.SP.Service.Common {
    public class WebSiteUser : AbstractSharepoint {
        public WebSiteUser(UserCredential uc)
            : base(uc) {

        }
        public override string listName {
            get { return "WebSiteUser"; }
        }

        public override List<string> FieldsToLoad {
            get {
                return _FieldsToLoad;
            }
        }
        public static List<string> _FieldsToLoad = ReflectionMethod.GetPropertyNames<SPUserInfo>();
    }
}
