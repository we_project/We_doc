﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;

namespace Daimler.MicroServices.SP.Service.Common {
    public interface ICommon {
        List<T> GetListItems<T>(List<string> fields = null);
        T GetListItemById<T>(int id);
        Task<List<T>> GetListItemsAsync<T>();
        void GetUsefulFields<T>();
        void GetUserUsefulFields();
        List<SPUserInfo> GetAllUsers();
        Task<List<SPUserInfo>> GetAllUsersAsync();
        List<SPUserInfo> GetAllFormABUsers();
        Task<List<SPUserInfo>> GetAllFormABUsersAsync();
        List<SPUserInfo> GetAllBadgeUsers();
        Task<List<SPUserInfo>> GetAllBadgeUsersAsync();
        bool Login();
    }
}
