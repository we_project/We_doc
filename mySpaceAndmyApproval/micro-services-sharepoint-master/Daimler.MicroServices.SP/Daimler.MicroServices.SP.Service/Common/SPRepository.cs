﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.BusinessCard;
using Daimler.MicroServices.SP.BizObject.InvitationLetter;
using Daimler.MicroServices.SP.BizObject.OfficeStationary;
using Daimler.MicroServices.SP.BizObject.PetrolCard;
using Daimler.MicroServices.SP.BizObject.Workflow;
using Daimler.MicroServices.SP.BizObject.FormAB;
using Daimler.MicroServices.SP.Service.BusinessCard;
using Daimler.MicroServices.SP.Service.InvitationLetter;
using Daimler.MicroServices.SP.Service.OfficeStationary;
using Daimler.MicroServices.SP.Service.PetrolCard;
using Daimler.MicroServices.SP.Service.Workflow;
using Daimler.MicroServices.SP.Service.FormAB;
using Daimler.MicroServices.SP.BizObject.AccessManagement;
using Daimler.MicroServices.SP.Service.AccessManagement;

namespace Daimler.MicroServices.SP.Service.Common {
    public class SPRepository {
        /// <summary>
        /// todo: supply AbstractSharepoint repository
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="uc"></param>
        /// <returns></returns>
        public static AbstractSharepoint Create<T>(UserCredential uc) {

            AbstractSharepoint sp = null;
            if (typeof(T).Equals(typeof(StationaryRequestInfo))) {
                sp = new OfficeStationaryRequests(uc);
            } else if (typeof(T).Equals(typeof(OSLocationInfo))) {
                sp = new OSLocationAdminMapping(uc);
            } else if (typeof(T).Equals(typeof(StationaryMenuInfo))) {
                sp = new OfficeStationaryMenu(uc);
            } else if (typeof(T).Equals(typeof(WorkflowTaskInfo))) {
                sp = new WorkflowTasks(uc);
            } else if (typeof(T).Equals(typeof(BCApplicationInfo))) {
                sp = new BusinessCardApplication(uc);
            } else if (typeof(T).Equals(typeof(eLOIApplicationInfo))) {
                sp = new eLOIApplication(uc);
            } else if (typeof(T).Equals(typeof(PCCreditAdjustmentInfo))) {
                sp = new PetrolCardCreditAdjustment(uc);
            } else if (typeof(T).Equals(typeof(FormAInfo))) {
                sp = new FormA(uc);
            } else if (typeof(T).Equals(typeof(FormACommentHistoryInfo))) {
                sp = new FormACommentHistory(uc);
            } else if (typeof(T).Equals(typeof(FormBInfo))) {
                sp = new FormB(uc);
            } else if (typeof(T).Equals(typeof(FormBCommentHistoryInfo))) {
                sp = new FormBCommentHistory(uc);
            } else if (typeof(T).Equals(typeof(CostCenterDeputyProfileInfo))) {
                sp = new CostCenterDeputyProfile(uc);
            }
            return sp;
        }
    }
}
