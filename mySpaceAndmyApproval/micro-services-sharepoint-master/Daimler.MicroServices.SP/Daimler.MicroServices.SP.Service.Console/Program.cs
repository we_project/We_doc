﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.AccessManagement;
using Daimler.MicroServices.SP.BizObject.BusinessCard;
using Daimler.MicroServices.SP.BizObject.FormAB;
using Daimler.MicroServices.SP.BizObject.InvitationLetter;
using Daimler.MicroServices.SP.BizObject.OfficeStationary;
using Daimler.MicroServices.SP.BizObject.PetrolCard;
using Daimler.MicroServices.SP.BizObject.Workflow;
using Daimler.MicroServices.SP.Service.AccessManagement;
using Daimler.MicroServices.SP.Service.Common;
using Daimler.MicroServices.SP.Service.OfficeStationary;
using Daimler.MicroServices.Utils;
using log4net;
using Microsoft.SharePoint.Client;

namespace Daimler.MicroServices.SP.Service.Console {
    class Program {
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));
        private static AbstractSharepoint officeStationarySharepoint { get; set; }

        private static UserCredential uc = new UserCredential {
            domain = SPConfig.Instance.Domain,
            userName = SPConfig.Instance.UserName,
            psw = SPConfig.Instance.Password
        };
        static void Main(string[] args) {

            try {
                System.Console.WriteLine("Begin");


                ICommon service = new CommonImpl(uc);

                #region office stationary
                //IOfficeStationary officeStationaryService = new OfficeStationaryImpl(uc);
                //officeStationarySharepoint = new OfficeStationaryRequests(uc);
                //AbstractSharepoint sharepoint = new OfficeStationaryRequests(uc_wl);
                //var fields = new List<string>() { 
                //                                                StationaryRequestInfoFields.CostCenterManagerPeople,
                //                                                StationaryRequestInfoFields.AdminLocalManager,
                //                                                StationaryRequestInfoFields.AdminSeniorManager,
                //                                                StationaryRequestInfoFields.Status};
                //StationaryRequestInfo_httpResponse item = sharepoint.GetListItemById<StationaryRequestInfo_httpResponse>(183, new List<string>() { 
                //                                                StationaryRequestInfoFields.CostCenterManagerPeopleId,
                //                                                StationaryRequestInfoFields.AdminLocalManagerId,
                //                                                StationaryRequestInfoFields.AdminSeniorManagerId,
                //                                                StationaryRequestInfoFields.Status});
                //string select = "?$select=CostCenterManagerPeople/ID,AdminLocalManager/ID,AdminSeniorManager/ID,Status&$expand=CostCenterManagerPeople,AdminLocalManager,AdminSeniorManager";
                //StationaryRequestInfo_httpResponse item = sharepoint.GetListItemById<StationaryRequestInfo_httpResponse>(183, select);
                //log.DebugFormat("item.Status:{0}", item.d.Status);
                //log.DebugFormat("item.CostCenterManagerPeople:{0}", item.d.CostCenterManagerPeople.ID);
                //log.DebugFormat("item.AdminLocalManager:{0}", item.d.AdminLocalManager.ID);
                //log.DebugFormat("item.AdminSeniorManager:{0}", item.d.AdminSeniorManager.ID);

                //StationaryRequestInfo_httpResponse item = sharepoint.GetListItemById<StationaryRequestInfo_httpResponse>(183);

                //string select = "?$select=CostCenterManagerPeopleId,AdminLocalManagerId,AdminSeniorManagerId,Status";
                //StationaryRequestInfo_httpResponse item = sharepoint.GetListItemById<StationaryRequestInfo_httpResponse>(183, select);
                //log.DebugFormat("item.Status:{0}", item.d.Status);
                //log.DebugFormat("item.CostCenterManagerPeople:{0}", item.d.CostCenterManagerPeopleId);
                //log.DebugFormat("item.AdminLocalManager:{0}", item.d.AdminLocalManagerId);
                //log.DebugFormat("item.AdminSeniorManager:{0}", item.d.AdminSeniorManagerId);



                //StationaryRequestInfo_httpResponse result = sharepoint.GetListItemById<StationaryRequestInfo_httpResponse>(183, StationaryRequestInfo_http.GetSPselect());

                //System.Console.WriteLine("Begin get StationaryRequestInfo:");
                //List<StationaryRequestInfo> infoes = service.GetListItems<StationaryRequestInfo>();
                //System.Console.WriteLine("StationaryRequestInfo count: {0}", infoes != null ? infoes.Count : 0);

                //System.Console.WriteLine("Begin get OSLocationInfo :");
                //List<OSLocationInfo> osLocationInfoes = service.GetListItems<OSLocationInfo>();
                //System.Console.WriteLine("OSLocationInfo count: {0}", osLocationInfoes != null ? osLocationInfoes.Count : 0);

                //System.Console.WriteLine("Begin get OSLocationInfo useful fields:");
                //service.GetUsefulFields<OSLocationInfo>();

                //System.Console.WriteLine("Begin get StationaryMenuInfo useful fields:");
                //service.GetUsefulFields<StationaryMenuInfo>();

                // System.Console.WriteLine("Begin get StationaryMenuInfo :");
                // List<StationaryMenuInfo> stationaryMenuInfoes = service.GetListItems<StationaryMenuInfo>();
                // System.Console.WriteLine("StationaryMenuInfo count: {0}", stationaryMenuInfoes != null ? stationaryMenuInfoes.Count : 0);

                //Task t= officeStationaryService.LoadStationaryImgsAsync(stationaryMenuInfoes);
                //t.Wait();


                #endregion

                #region User
                //service.GetUserUsefulFields();

                //List<SPUserInfo> users = service.GetAllUsers();
                //System.Console.WriteLine("GetAllUsers count: {0}", users != null ? users.Count : 0); 
                #endregion

                #region Workflow tasks
                //System.Console.WriteLine("Begin get WorkflowTaskInfo useful fields:");
                //service.GetUsefulFields<WorkflowTaskInfo>();

                //System.Console.WriteLine("Begin get WorkflowTaskInfo :");
                //List<WorkflowTaskInfo> workflowInfoes = service.GetListItems<WorkflowTaskInfo>();
                //System.Console.WriteLine("WorkflowTaskInfo count: {0}", workflowInfoes != null ? workflowInfoes.Count : 0); 
                #endregion

                #region BC
                //System.Console.WriteLine("Begin get BCApplicationInfo :");
                //List<BCApplicationInfo> bcInfoes = service.GetListItems<BCApplicationInfo>();
                //System.Console.WriteLine("BCApplicationInfo count: {0}", bcInfoes != null ? bcInfoes.Count : 0);
                #endregion

                #region IL
                //System.Console.WriteLine("Begin get eLOIApplicationInfo useful fields:");
                //service.GetUsefulFields<eLOIApplicationInfo>();

                ////System.Console.WriteLine("Begin get eLOIApplicationInfo :");
                ////List<eLOIApplicationInfo> ilInfoes = service.GetListItems<eLOIApplicationInfo>();
                ////System.Console.WriteLine("eLOIApplicationInfo count: {0}", ilInfoes != null ? ilInfoes.Count : 0);
                #endregion

                #region PC
                //System.Console.WriteLine("Begin get PCCreditAdjustmentInfo useful fields:");
                //service.GetUsefulFields<PCCreditAdjustmentInfo>();

                //System.Console.WriteLine("Begin get PCCreditAdjustmentInfo :");
                //List<PCCreditAdjustmentInfo> pcInfoes = service.GetListItems<PCCreditAdjustmentInfo>();
                //System.Console.WriteLine("PCCreditAdjustmentInfo count: {0}", pcInfoes != null ? pcInfoes.Count : 0);
                #endregion

                #region FormAB
                //System.Console.WriteLine("Begin get FormAInfo useful fields:");
                //service.GetUsefulFields<FormAInfo>();

                //System.Console.WriteLine("Begin get FormAInfo :");
                //List<FormAInfo> formaInfoes = service.GetListItems<FormAInfo>();
                //System.Console.WriteLine("FormAInfo count: {0}", formaInfoes != null ? formaInfoes.Count : 0);

                //System.Console.WriteLine("Begin get FormACommentHistoryInfo useful fields:");
                //service.GetUsefulFields<FormACommentHistoryInfo>();

                //System.Console.WriteLine("Begin get FormACommentHistoryInfo :");
                //List<FormACommentHistoryInfo> formaCommentInfoes = service.GetListItems<FormACommentHistoryInfo>();
                //System.Console.WriteLine("FormACommentHistoryInfo count: {0}", formaCommentInfoes != null ? formaCommentInfoes.Count : 0);

                //System.Console.WriteLine("Begin get FormBInfo useful fields:");
                //service.GetUsefulFields<FormBInfo>();

                //System.Console.WriteLine("Begin get FormBInfo :");
                //List<FormBInfo> FormBInfo = service.GetListItems<FormBInfo>();
                //System.Console.WriteLine("FormBInfo count: {0}", FormBInfo != null ? FormBInfo.Count : 0);


                //System.Console.WriteLine("Begin get FormBCommentHistoryInfo useful fields:");
                //service.GetUsefulFields<FormBCommentHistoryInfo>();

                //System.Console.WriteLine("Begin get FormBCommentHistoryInfo :");
                //List<FormBCommentHistoryInfo> FormBCommentHistoryInfo = service.GetListItems<FormBCommentHistoryInfo>();
                //System.Console.WriteLine("FormBCommentHistoryInfo count: {0}", FormBCommentHistoryInfo != null ? FormBCommentHistoryInfo.Count : 0);
                #endregion

                #region Test
                //bool loged = service_tl.Login();
                //bool wlLogged = service_wl.Login();
                //System.Console.WriteLine(DateTime.Now);
                //ListItem i1 = officeStationarySharepoint.GetListItemById(161);
                //System.Console.WriteLine(DateTime.Now);
                //ListItem i2 = officeStationarySharepoint.GetListItemById(165);
                //System.Console.WriteLine(DateTime.Now);
                //ListItem i3 = officeStationarySharepoint.GetListItemById(164);
                //System.Console.WriteLine(DateTime.Now);
                //ListItem i4 = officeStationarySharepoint.GetListItemById(163);
                //System.Console.WriteLine(DateTime.Now);
                //ListItem i5 = officeStationarySharepoint.GetListItemById(162);
                //System.Console.WriteLine(DateTime.Now);
                //ListItem i6 = officeStationarySharepoint.GetListItemById(166);
                //System.Console.WriteLine(DateTime.Now);
                #endregion

                #region Login

                //bool isLogin = service_tl.Login();

                //bool wlIsLogin = service_wl.Login();
                #endregion

                #region EIBM
                //service.GetUsefulFields<EIBMInfo>();
                //AbstractSharepoint EIBMsp = SPRepository.Create<EIBMInfo>(uc);
                //List<string> fields = ReflectionMethod.GetPropertyNames<EIBMInfo_http>();
                //try {
                //    //EIBMInfoes_http_response result = EIBMsp.GetListItemsWithAllFields<EIBMInfoes_http_response>();
                //    EIBMInfoes_http_response result = EIBMsp.GetListItems<EIBMInfoes_http_response>(fields);
                //    log.Debug(result);
                //} catch (Exception ex) {
                //    log.Error("error", ex);
                //}


                //log.Debug("one by one");
                //for (int i = 0; i < fields.Count; i++) {
                //    var currentField = fields[i];
                //    try {
                //        log.Debug(new {
                //            currentField = currentField,
                //        });
                //        //EIBMsp.GetListItems(new List<string>() { currentField });
                //        EIBMInfoes_http_response result = EIBMsp.GetListItems<EIBMInfoes_http_response>(new List<string>() { currentField });
                //        log.Debug(result);
                //    } catch (Exception ex) {
                //        log.Error(new {
                //            currentField = currentField
                //        }, ex);
                //    }
                //}

                //System.Console.WriteLine("Begin get EIBMInfo :");
                //List<EIBMInfo> infoes = service_tl.GetListItems<EIBMInfo>();
                //System.Console.WriteLine("EIBMInfo count: {0}", infoes != null ? infoes.Count : 0);
                #endregion

                #region EIBMNew
                EIBMNew spEIBM = new EIBMNew(uc);
                //spEIBM.GetListItems();

                //int currentUserId = spEIBM.GetCurrentUserId();
                //log.Debug(new { currentUserId = currentUserId });
                #region get pending list
                //var pendingList = spEIBM.GetPendingList();
                //log.Debug(pendingList.Count());
                #endregion

                #region reject
                //spEIBM.CostCenterManagerReject(8, "reject from webservice");
                #endregion

                #endregion

                #region EIBM Restful
                int badgeUserId = 1548;
                EIBMRestful spEIBMRestful = new EIBMRestful(uc);
                //spEIBMRestful.CurrentUserId = badgeUserId;
                //int badgePendingCount = spEIBMRestful.GetPendingCount();
                //log.Debug(new { count = badgePendingCount });

                //var list = spEIBMRestful.GetPendingList();
                //log.Debug(list);

                //var item = spEIBMRestful.GetItem(12);
                //log.Debug(item);
                //var allItems = spEIBMRestful.GetAllItemsAsync().Result;
                //System.Console.WriteLine(allItems != null ? allItems.Count : 0);
                #endregion

                #region Vistor Badge Application
                //AbstractSharepoint VistorBadgeApplicationsp = SPRepository.Create<VistorBadgeApplicationInfo>(uc);
                ////VistorBadgeApplicationsp.PrintAllLists();

                ////service.GetUsefulFields<VistorBadgeApplicationInfo>();

                //for (int i = 0; i < VistorBadgeApplicationsp.FieldsToLoad.Count; i++) {
                //    var currentField = VistorBadgeApplicationsp.FieldsToLoad[i];
                //    try {
                //        //VistorBadgeApplicationsp.GetListItems(new List<string>() { currentField });
                //        VistorBadgeApplicationsp.GetListItems<VistorBadgeApplicationInfo[]>(new List<string>() { currentField });

                //    } catch (Exception ex) {
                //        log.Error(new {
                //            currentField = currentField
                //        }, ex);
                //    }
                //}

                //System.Console.WriteLine("Begin get EIBMInfo :");
                //List<EIBMInfo> infoes = service_tl.GetListItems<EIBMInfo>();
                //System.Console.WriteLine("EIBMInfo count: {0}", infoes != null ? infoes.Count : 0);

                //var vb = new VisitorBadge(uc);
                //int count = vb.GetPendingCountNew();
                //log.Debug(count);
                //System.Console.WriteLine(count);
                VisitorBadgeRestful vb = new VisitorBadgeRestful(uc);
                int result = vb.GetItemsCount().Result;
                System.Console.WriteLine(result);
                #endregion

                #region Cost Center Deputy
                //service.GetUsefulFields<CostCenterDeputyProfileInfo>();

                #endregion

                #region Config
                SPConfig config = SPConfig.Instance;
                #endregion
                System.Console.WriteLine("End");
            } catch (Exception ex) {
                log.Error(ex);
                System.Console.WriteLine(ex);
            }
            System.Console.ReadKey();
        }
    }
}
