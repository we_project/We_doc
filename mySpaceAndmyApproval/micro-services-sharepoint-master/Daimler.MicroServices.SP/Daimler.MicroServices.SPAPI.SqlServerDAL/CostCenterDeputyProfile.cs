﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.AccessManagement;
using Daimler.MicroServices.SPAPI.IDAL;
using Daimler.MicroServices.Utils.Data;

namespace Daimler.MicroServices.SPAPI.SqlServerDAL {
    public class CostCenterDeputyProfile : ICostCenterDeputyProfile {
        DBHelper dbHelper = new DBHelper();
        bool ICostCenterDeputyProfile.SaveInfoes(List<CostCenterDeputyProfileInfo> infoes) {
            DataTable data = DataHelper.CreateDataTable<CostCenterDeputyProfileInfo>(infoes);
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_saveCCDeputy.infoes,data)
            };
            int result = dbHelper.Execute(Config.Procedures.cp_sp_saveCCDeputy.name, sqlParameters, CommandType.StoredProcedure);
            if (result == 0) {
                return false;
            }
            return true;
        }


        List<CostCenterDeputyProfileInfo> ICostCenterDeputyProfile.GetCCDeputys(int deputyId) {
            List<CostCenterDeputyProfileInfo> result = null;
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_getCCDeputys.deputyId,deputyId),
            };
            DataSet requests = dbHelper.ExcuteQuery(Config.Procedures.cp_sp_getCCDeputys.name, sqlParameters, CommandType.StoredProcedure);
            if (requests != null && requests.Tables != null && requests.Tables.Count > 0) {
                IEnumerable<DataRow> rows = requests.Tables[0].AsEnumerable();
                if (rows != null && rows.Count() > 0) {
                    result = rows.Select(row => new CostCenterDeputyProfileInfo {
                        ID = row.Field<int>("ID"),
                        Title = row.Field<string>("Title"),
                        Cost_x0020_Center_x0020_Manager = row.Field<int?>("Cost_x0020_Center_x0020_Manager"),
                        Cost_x0020_Center_x0020_Deputy = row.Field<int?>("Cost_x0020_Center_x0020_Deputy"),
                        Delegate_x0020_From_x0020_Date = row.Field<DateTime?>("Delegate_x0020_From_x0020_Date"),
                        Delegate_x0020_To_x0020_Date = row.Field<DateTime?>("Delegate_x0020_To_x0020_Date"),
                        Modified = row.Field<DateTime?>("Modified"),
                        Created = row.Field<DateTime?>("Created"),
                    }).ToList();
                }
            }
            return result;
        }
    }
}
