﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.InvitationLetter;
using Daimler.MicroServices.SPAPI.IDAL;
using Daimler.MicroServices.Utils.Data;

namespace Daimler.MicroServices.SPAPI.SqlServerDAL {
    public class eLOIApplication : IeLOIApplication {
        DBHelper dbHelper = new DBHelper();
        bool IeLOIApplication.SaveInfoes(List<SP.BizObject.InvitationLetter.eLOIApplicationInfo> infoes) {
            DataTable data = DataHelper.CreateDataTable<eLOIApplicationInfo>(infoes);
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_saveeLOIApplication.infoes,data)
            };
            int result = dbHelper.Execute(Config.Procedures.cp_sp_saveeLOIApplication.name, sqlParameters, CommandType.StoredProcedure);
            if (result == 0) {
                return false;
            }
            return true;
        }

        bool IeLOIApplication.UpdateRequests(List<SP.BizObject.InvitationLetter.eLOIApplicationInfo> infoes) {
            DataTable data = DataHelper.CreateDataTable<eLOIApplicationInfo>(infoes);
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_updateeLOIApplication.infoes,data)
            };
            int result = dbHelper.Execute(Config.Procedures.cp_sp_updateeLOIApplication.name, sqlParameters, CommandType.StoredProcedure);
            if (result == 0) {
                return false;
            }
            return true;
        }

        List<InvitationLetterApplicationItem> IeLOIApplication.GetILApplicatoinList(int userId, int? pageIndex, int? pageSize) {
            List<InvitationLetterApplicationItem> result = null;
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_geteLOIApplicationV2.userId,userId),
                new SqlParameter(Config.Procedures.cp_sp_geteLOIApplicationV2.pageIndex,pageIndex),
                new SqlParameter(Config.Procedures.cp_sp_geteLOIApplicationV2.pageSize,pageSize),
            };
            DataSet requests = dbHelper.ExcuteQuery(Config.Procedures.cp_sp_geteLOIApplicationV2.name, sqlParameters, CommandType.StoredProcedure);
            if (requests != null && requests.Tables != null && requests.Tables.Count > 0) {
                IEnumerable<DataRow> rows = requests.Tables[0].AsEnumerable();
                if (rows != null && rows.Count() > 0) {
                    result = rows.Select(row => new InvitationLetterApplicationItem {
                        ID = row.Field<int>("ID"),
                        //Modified_x0020_By = row.Field<string>("Modified_x0020_By"),
                        //Created_x0020_By = row.Field<string>("Created_x0020_By"),
                        //_SourceUrl = row.Field<string>("_SourceUrl"),
                        //_SharedFileIndex = row.Field<string>("_SharedFileIndex"),
                        //Title = row.Field<string>("Title"),
                        //TemplateUrl = row.Field<string>("TemplateUrl"),
                        //xd_ProgID = row.Field<string>("xd_ProgID"),
                        //xd_Signature = row.Field<bool?>("xd_Signature"),
                        //ShowRepairView = row.Field<string>("ShowRepairView"),
                        //ShowCombineView = row.Field<string>("ShowCombineView"),
                        User_x0020_Name = row.Field<string>("User_x0020_Name"),
                        //Employee_x0020_Number = row.Field<string>("Employee_x0020_Number"),
                        //Cost_x0020_Center = row.Field<string>("Cost_x0020_Center"),
                        //Extension_x0020_Number = row.Field<string>("Extension_x0020_Number"),
                        Applicants_x0020_Company_x0020_Name = row.Field<string>("Applicants_x0020_Company_x0020_Name"),
                        //Applicants_x0020_Company_x0020_Address = row.Field<string>("Applicants_x0020_Company_x0020_Address"),
                        //JobTitle = row.Field<string>("JobTitle"),
                        //Contact_x0020_in_x0020_Company = row.Field<string>("Contact_x0020_in_x0020_Company"),
                        //Entry_x0020_Port = row.Field<string>("Entry_x0020_Port"),
                        //Cost_x0020_Center_x0020_Manager = row.Field<string>("Cost_x0020_Center_x0020_Manager"),
                        //Form_x0020_Name = row.Field<string>("Form_x0020_Name"),
                        //Last_x0020_Name = row.Field<string>("Last_x0020_Name"),
                        //Nationality = row.Field<string>("Nationality"),
                        //Birth_x0020_Date = row.Field<DateTime?>("Birth_x0020_Date"),
                        //Passport = row.Field<string>("Passport"),
                        //Entry_x0020_Date = row.Field<DateTime?>("Entry_x0020_Date"),
                        //Departure_x0020_Date = row.Field<DateTime?>("Departure_x0020_Date"),
                        //Purposeof_x0020_Visit = row.Field<string>("Purposeof_x0020_Visit"),
                        //VISAType = row.Field<string>("VISAType"),
                        //Apply_x0020_Place = row.Field<string>("Apply_x0020_Place"),
                        //Contact_x0020_Phone = row.Field<string>("Contact_x0020_Phone"),
                        //Relationship_x0020_With_x0020_Daimler = row.Field<string>("Relationship_x0020_With_x0020_Daimler"),
                        //FirstName = row.Field<string>("FirstName"),
                        //Gender = row.Field<string>("Gender"),
                        //Stay_x0020_Days = row.Field<string>("Stay_x0020_Days"),
                        Invitation_x0020_Status = row.Field<string>("Invitation_x0020_Status"),
                        //Form_x0020_Number = row.Field<string>("Form_x0020_Number"),
                        //Company_x0020_Name = row.Field<string>("Company_x0020_Name"),
                        //Finished_x0020_Print = row.Field<string>("Finished_x0020_Print"),
                        //Invitati = row.Field<string>("Invitati"),
                        //CCManagerUser = row.Field<string>("CCManagerUser"),
                        //DelegateUser = row.Field<string>("DelegateUser"),
                        Created = row.Field<DateTime?>("Created"),
                        Modified = row.Field<DateTime?>("Modified"),
                        //File_x0020_Size = row.Field<string>("File_x0020_Size"),
                        //CheckedOutUserId = row.Field<string>("CheckedOutUserId"),
                        //IsCheckedoutToLocal = row.Field<string>("IsCheckedoutToLocal"),
                        //CheckoutUser = row.Field<string>("CheckoutUser"),
                        //VirusStatus = row.Field<int?>("VirusStatus")+"",
                        //CheckedOutTitle = row.Field<int?>("CheckedOutTitle") + "",
                        //_CheckinComment = row.Field<string>("_CheckinComment"),
                        //LinkCheckedOutTitle = row.Field<string>("LinkCheckedOutTitle"),
                        //FileSizeDisplay = row.Field<string>("FileSizeDisplay"),
                        //SelectFilename = row.Field<string>("SelectFilename"),
                        //ParentVersionString = row.Field<int?>("ParentVersionString")+"",
                        //ParentLeafName = row.Field<int?>("ParentLeafName")+"",
                        //DocConcurrencyNumber = row.Field<string>("DocConcurrencyNumber"),
                        //Combine = row.Field<string>("Combine"),
                        //RepairDocument = row.Field<string>("RepairDocument"),
                    }).ToList();
                }
            }
            return result;
        }

        SP.BizObject.InvitationLetter.InvitationLetterApplicationDetail IeLOIApplication.GetILApplicatoinDetail(int ID) {
            InvitationLetterApplicationDetail result = null;
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_geteLOIApplicationDetailV2.ID,ID),
            };
            DataSet requests = dbHelper.ExcuteQuery(Config.Procedures.cp_sp_geteLOIApplicationDetailV2.name, sqlParameters, CommandType.StoredProcedure);
            if (requests != null && requests.Tables != null && requests.Tables.Count > 0) {
                IEnumerable<DataRow> rows = requests.Tables[0].AsEnumerable();
                if (rows != null && rows.Count() > 0) {
                    DataRow row = rows.FirstOrDefault();
                    if (row != null) {
                        result = new InvitationLetterApplicationDetail {
                            ID = row.Field<int>("ID"),
                            //Modified_x0020_By = row.Field<string>("Modified_x0020_By"),
                            //Created_x0020_By = row.Field<string>("Created_x0020_By"),
                            //_SourceUrl = row.Field<string>("_SourceUrl"),
                            //_SharedFileIndex = row.Field<string>("_SharedFileIndex"),
                            //Title = row.Field<string>("Title"),
                            //TemplateUrl = row.Field<string>("TemplateUrl"),
                            //xd_ProgID = row.Field<string>("xd_ProgID"),
                            //xd_Signature = row.Field<bool?>("xd_Signature"),
                            //ShowRepairView = row.Field<string>("ShowRepairView"),
                            //ShowCombineView = row.Field<string>("ShowCombineView"),
                            User_x0020_Name = row.Field<string>("User_x0020_Name"),
                            Employee_x0020_Number = row.Field<string>("Employee_x0020_Number"),
                            Cost_x0020_Center = row.Field<string>("Cost_x0020_Center"),
                            Extension_x0020_Number = row.Field<string>("Extension_x0020_Number"),
                            Applicants_x0020_Company_x0020_Name = row.Field<string>("Applicants_x0020_Company_x0020_Name"),
                            Applicants_x0020_Company_x0020_Address = row.Field<string>("Applicants_x0020_Company_x0020_Address"),
                            JobTitle = row.Field<string>("JobTitle"),
                            Contact_x0020_in_x0020_Company = row.Field<string>("Contact_x0020_in_x0020_Company"),
                            Entry_x0020_Port = row.Field<string>("Entry_x0020_Port"),
                            //Cost_x0020_Center_x0020_Manager = row.Field<string>("Cost_x0020_Center_x0020_Manager"),
                            //Form_x0020_Name = row.Field<string>("Form_x0020_Name"),
                            Last_x0020_Name = row.Field<string>("Last_x0020_Name"),
                            //Nationality = row.Field<string>("Nationality"),
                            Birth_x0020_Date = row.Field<DateTime?>("Birth_x0020_Date"),
                            //Passport = row.Field<string>("Passport"),
                            Entry_x0020_Date = row.Field<DateTime?>("Entry_x0020_Date"),
                            Departure_x0020_Date = row.Field<DateTime?>("Departure_x0020_Date"),
                            Purposeof_x0020_Visit = row.Field<string>("Purposeof_x0020_Visit"),
                            VISAType = row.Field<string>("VISAType"),
                            Apply_x0020_Place = row.Field<string>("Apply_x0020_Place"),
                            Contact_x0020_Phone = row.Field<string>("Contact_x0020_Phone"),
                            Relationship_x0020_With_x0020_Daimler = row.Field<string>("Relationship_x0020_With_x0020_Daimler"),
                            FirstName = row.Field<string>("FirstName"),
                            Gender = row.Field<string>("Gender"),
                            //Stay_x0020_Days = row.Field<string>("Stay_x0020_Days"),
                            Invitation_x0020_Status = row.Field<string>("Invitation_x0020_Status"),
                            //Form_x0020_Number = row.Field<string>("Form_x0020_Number"),
                            Company_x0020_Name = row.Field<string>("Company_x0020_Name"),
                            //Finished_x0020_Print = row.Field<string>("Finished_x0020_Print"),
                            //Invitati = row.Field<string>("Invitati"),
                            //CCManagerUser = row.Field<string>("CCManagerUser"),
                            //DelegateUser = row.Field<string>("DelegateUser"),
                            Created = row.Field<DateTime?>("Created"),
                            Modified = row.Field<DateTime?>("Modified"),
                            //File_x0020_Size = row.Field<string>("File_x0020_Size"),
                            //CheckedOutUserId = row.Field<string>("CheckedOutUserId"),
                            //IsCheckedoutToLocal = row.Field<string>("IsCheckedoutToLocal"),
                            //CheckoutUser = row.Field<string>("CheckoutUser"),
                            //VirusStatus = row.Field<int?>("VirusStatus") + "",
                            //CheckedOutTitle = row.Field<int?>("CheckedOutTitle") + "",
                            //_CheckinComment = row.Field<string>("_CheckinComment"),
                            //LinkCheckedOutTitle = row.Field<string>("LinkCheckedOutTitle"),
                            //FileSizeDisplay = row.Field<string>("FileSizeDisplay"),
                            //SelectFilename = row.Field<string>("SelectFilename"),
                            //ParentVersionString = row.Field<int?>("ParentVersionString") + "",
                            //ParentLeafName = row.Field<int?>("ParentLeafName") + "",
                            //DocConcurrencyNumber = row.Field<string>("DocConcurrencyNumber"),
                            //Combine = row.Field<string>("Combine"),
                            //RepairDocument = row.Field<string>("RepairDocument"),
                        };
                    }
                }
            }
            return result;
        }
    }
}
