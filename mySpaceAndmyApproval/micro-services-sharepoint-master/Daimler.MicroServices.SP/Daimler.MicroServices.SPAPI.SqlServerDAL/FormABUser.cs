﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SPAPI.IDAL;
using Daimler.MicroServices.Utils.Data;

namespace Daimler.MicroServices.SPAPI.SqlServerDAL {
    public class FormABUser : IFormABUser {
        DBHelper dbHelper = new DBHelper();
        bool IFormABUser.SaveInfoes(List<SP.BizObject.SPUserInfo> infoes) {
            DataTable data = DataHelper.CreateDataTable<SPUserInfo>(infoes);
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_saveFormABUser.infoes,data)
            };
            int result = dbHelper.Execute(Config.Procedures.cp_sp_saveFormABUser.name, sqlParameters, CommandType.StoredProcedure);
            if (result == 0) {
                return false;
            }
            return true;
        }


        int? IFormABUser.GetUserId(string accountName) {
            string sql = @"
                select ID from FormABUser
                  where AccountName=@accountName
            ";
            SqlParameter[] sqlParameters ={
                new SqlParameter("@accountName",accountName)
            };
            object result = dbHelper.ExcuteMyScalar(sql, sqlParameters, CommandType.Text);
            if (result != null) {
                return Convert.ToInt32(result);
            }
            return null;
        }


        string IFormABUser.GetUserName(string accountName) {
            string sql = @"
                select Title from FormABUser
                  where AccountName=@accountName
            ";
            SqlParameter[] sqlParameters ={
                new SqlParameter("@accountName",accountName)
            };
            object result = dbHelper.ExcuteMyScalar(sql, sqlParameters, CommandType.Text);
            if (result != null) {
                return result.ToString();
            }
            return null;
        }
    }
}
