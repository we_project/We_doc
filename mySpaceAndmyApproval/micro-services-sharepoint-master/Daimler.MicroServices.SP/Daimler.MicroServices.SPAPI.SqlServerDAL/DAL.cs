﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SPAPI.IDAL;
using Daimler.MicroServices.SPAPI.SqlServerDAL;

namespace Daimler.MicroServices.SPAPI.SqlServerDAL {
    public class DAL {
        private DAL() { }
        private static IOfficeStationaryRequests officeStatonaryRequests = new OfficeStationaryRequests();
        public static IOfficeStationaryRequests DefaultOfficeStationaryRequests {
            get {
                return officeStatonaryRequests;
            }
        }

        private static IOSLocationAdminMapping osLocationAdminMapping = new OSLocationAdminMapping();
        public static IOSLocationAdminMapping DefaultOSLocationAdminMapping {
            get {
                return osLocationAdminMapping;
            }
        }

        private static IOfficeStationaryMenu officeStationaryMenu = new OfficeStationaryMenu();

        public static IOfficeStationaryMenu DefaultOfficeStationaryMenu {
            get { return officeStationaryMenu; }
        }

        private static ICommonUser commonUser = new CommonUser();

        public static ICommonUser DefaultCommonUser {
            get { return commonUser; }
        }

        private static IFormABUser formABUser = new FormABUser();

        public static IFormABUser DefaultFormABUser {
            get { return formABUser; }
        }

        private static IWorkflowTasks workflowTasks = new WorkflowTasks();

        public static IWorkflowTasks DefaultWorkflowTasks {
            get { return workflowTasks; }
        }
        private static IBusinessCardApplication businessCardApplication = new BusinessCardApplication();

        public static IBusinessCardApplication DefaultBusinessCardApplication {
            get { return businessCardApplication; }
        }
        private static IeLOIApplication eLOIApplication = new eLOIApplication();

        public static IeLOIApplication DefaulteLOIApplication {
            get { return eLOIApplication; }
        }

        private static IPetrolCard_CreditAdjustment petrolCard_CreditAdjustment = new PetrolCard_CreditAdjustment();

        public static IPetrolCard_CreditAdjustment DefaultPetrolCard_CreditAdjustment {
            get { return petrolCard_CreditAdjustment; }
        }

        private static IFormA formA = new FormA();

        public static IFormA DefaultFormA {
            get { return formA; }
        }
        private static IFormACommentHistory formACommentHistory = new FormACommentHistory();

        public static IFormACommentHistory DefaultFormACommentHistory {
            get { return formACommentHistory; }
        }
        private static IFormB formB = new FormB();

        public static IFormB DefaultFormB {
            get { return formB; }
        }
        private static IFormBCommentHistory formBCommentHistory = new FormBCommentHistory();

        public static IFormBCommentHistory DefaultFormBCommentHistory {
            get { return formBCommentHistory; }
        }
        #region Badge
        private static IBadgeUser badgeUser = new BadgeUser();

        public static IBadgeUser DefaultBadgeUser {
            get { return badgeUser; }
        }
        private static ICostCenterDeputyProfile ccDeputy = new CostCenterDeputyProfile();
        public static ICostCenterDeputyProfile DefaultCCDeputy {
            get { return ccDeputy; }
        }


        private static IEIBM eibm = new EIBM();

        public static IEIBM DefaultEIBM {
            get { return eibm; }
        }
        private static ISite site = new Site();

        public static ISite DefaultSite {
            get { return site; }
        }
        private static ILocation location = new Location();

        public static ILocation DefaultLocation {
            get { return location; }
        }
        private static IArea area = new Area();

        public static IArea DefaultArea {
            get { return area; }
        }
        private static IVisitorBadge visitorBadge = new VisitorBadge();

        public static IVisitorBadge DefaultVisitorBadge {
            get { return visitorBadge; }
        }
        #endregion
    }
}
