﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.Workflow;
using Daimler.MicroServices.SPAPI.IDAL;
using Daimler.MicroServices.Utils.Data;

namespace Daimler.MicroServices.SPAPI.SqlServerDAL {
    public class WorkflowTasks : IWorkflowTasks {
        DBHelper dbHelper = new DBHelper();
        bool IWorkflowTasks.SaveInfoes(List<SP.BizObject.Workflow.WorkflowTaskInfo> infoes) {
            DataTable data = DataHelper.CreateDataTable<WorkflowTaskInfo>(infoes);
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_saveWorkflowTasks.infoes,data)
            };
            int result = dbHelper.Execute(Config.Procedures.cp_sp_saveWorkflowTasks.name, sqlParameters, CommandType.StoredProcedure);
            if (result == 0) {
                return false;
            }
            return true;
        }

        bool IWorkflowTasks.UpdateRequests(List<SP.BizObject.Workflow.WorkflowTaskInfo> infoes) {
            DataTable data = DataHelper.CreateDataTable<WorkflowTaskInfo>(infoes);
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_updateWorkflowTasks.infoes,data)
            };
            int result = dbHelper.Execute(Config.Procedures.cp_sp_updateWorkflowTasks.name, sqlParameters, CommandType.StoredProcedure);
            if (result == 0) {
                return false;
            }
            return true;
        }


        int IWorkflowTasks.GetPendingBCCount(int userId) {
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_getPendingBCCount.userId,userId)
            };
            object result = dbHelper.ExcuteMyScalar(Config.Procedures.cp_sp_getPendingBCCount.name, sqlParameters, CommandType.StoredProcedure);
            if (result != null) {
                return Convert.ToInt32(result);
            }
            return 0;
        }

        int IWorkflowTasks.GetPendingILCount(int userId) {
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_getPendingILCount.userId,userId)
            };
            object result = dbHelper.ExcuteMyScalar(Config.Procedures.cp_sp_getPendingILCount.name, sqlParameters, CommandType.StoredProcedure);
            if (result != null) {
                return Convert.ToInt32(result);
            }
            return 0;
        }


        int? IWorkflowTasks.GetPendingWorkflowTasksByItemId(int userId, int itemId, string workflowName) {
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_getPendingWorkflowTasksByItemId.userId,userId),
                new SqlParameter(Config.Procedures.cp_sp_getPendingWorkflowTasksByItemId.itemId,itemId),
                new SqlParameter(Config.Procedures.cp_sp_getPendingWorkflowTasksByItemId.workflowName,workflowName)
            };
            object result = dbHelper.ExcuteMyScalar(Config.Procedures.cp_sp_getPendingWorkflowTasksByItemId.name, sqlParameters, CommandType.StoredProcedure);
            if (result != null) {
                return Convert.ToInt32(result);
            }
            return null;
        }


        bool IWorkflowTasks.MarkApproved(int ID) {
            string sql = @"
                Update WorkflowTasks set IsApprovedFromOurSide=1 where ID=@id
            ";
            SqlParameter[] sqlParameters ={
                                             new SqlParameter("@id",ID)
                                         };
            int result = dbHelper.Execute(sql, sqlParameters, CommandType.Text);
            if (result == 0) {
                return false;
            }
            return true;
        }
    }
}
