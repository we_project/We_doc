﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SP.BizObject.OfficeStationary;
using Daimler.MicroServices.SPAPI.IDAL;
using Daimler.MicroServices.Utils.Data;

namespace Daimler.MicroServices.SPAPI.SqlServerDAL {
    public class OfficeStationaryMenu : IOfficeStationaryMenu {
        DBHelper dbHelper = new DBHelper();
        bool IOfficeStationaryMenu.SaveInfoes(List<StationaryMenuInfo> infoes) {
            DataTable data = DataHelper.CreateDataTable<StationaryMenuInfo>(infoes);
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_saveOfficeStationaryMenu.infoes,data)
            };
            int result = dbHelper.Execute(Config.Procedures.cp_sp_saveOfficeStationaryMenu.name, sqlParameters, CommandType.StoredProcedure);
            if (result == 0) {
                return false;
            }
            return true;
        }



        List<StationaryDetail> IOfficeStationaryMenu.GetList(List<int> ids) {
            List<StationaryDetail> result = null;
            if (ids == null || ids.Count == 0) {
                return result;
            }
            DataTable data = DataHelper.CreateDataTable<ID>(ids.Select(id => new ID { id = id }));
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_getStationaryMenus.ids,data)
            };
            DataSet details = dbHelper.ExcuteQuery(Config.Procedures.cp_sp_getStationaryMenus.name, sqlParameters, CommandType.StoredProcedure);
            if (details != null && details.Tables != null && details.Tables.Count > 0) {
                IEnumerable<DataRow> rows = details.Tables[0].AsEnumerable();
                if (rows != null && rows.Count() > 0) {
                    result = rows.Select(row => new StationaryDetail {
                        ID = row.Field<int>("ID"),
                        ItemName = row.Field<string>("ItemName"),
                        ItemName_EN = row.Field<string>("ItemName_EN"),
                        Price_x0028_with_x0020_Tax_x0029_ = Convert.ToDouble(row.Field<decimal>("Price_x0028_with_x0020_Tax_x0029_"))
                    }).ToList();
                }
            }
            return result;
        }



    }
}
