﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.AccessManagement;
using Daimler.MicroServices.SPAPI.IDAL;
using Daimler.MicroServices.Utils.Data;

namespace Daimler.MicroServices.SPAPI.SqlServerDAL {
    public class Area : IArea {
        DBHelper dbHelper = new DBHelper();
        bool IArea.SaveInfoes(List<AreaInfo> infoes) {
            DataTable data = DataHelper.CreateDataTable<AreaInfo>(infoes);
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_saveArea.infoes,data)
            };
            int result = dbHelper.Execute(Config.Procedures.cp_sp_saveArea.name, sqlParameters, CommandType.StoredProcedure);
            if (result == 0) {
                return false;
            }
            return true;
        }
    }
}
