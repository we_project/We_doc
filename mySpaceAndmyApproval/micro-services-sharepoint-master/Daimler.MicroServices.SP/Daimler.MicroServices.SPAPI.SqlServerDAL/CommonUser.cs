﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;
using Daimler.MicroServices.SPAPI.IDAL;
using Daimler.MicroServices.Utils.Data;

namespace Daimler.MicroServices.SPAPI.SqlServerDAL {
    public class CommonUser : ICommonUser {
        DBHelper dbHelper = new DBHelper();
        bool ICommonUser.SaveInfoes(List<SPUserInfo> infoes) {
            DataTable data = DataHelper.CreateDataTable<SPUserInfo>(infoes);
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_saveCommonUser.infoes,data)
            };
            int result = dbHelper.Execute(Config.Procedures.cp_sp_saveCommonUser.name, sqlParameters, CommandType.StoredProcedure);
            if (result == 0) {
                return false;
            }
            return true;
        }


        int? ICommonUser.GetUserId(string accountName) {
            string sql = @"
                select ID from CommonUser
                  where AccountName=@accountName
            ";
            SqlParameter[] sqlParameters ={
                new SqlParameter("@accountName",accountName)
            };
            object result = dbHelper.ExcuteMyScalar(sql, sqlParameters, CommandType.Text);
            if (result != null) {
                return Convert.ToInt32(result);
            }
            return null;
        }
    }
}
