﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.OfficeStationary;
using Daimler.MicroServices.SPAPI.IDAL;
using Daimler.MicroServices.Utils.Data;

namespace Daimler.MicroServices.SPAPI.SqlServerDAL {
    public class OSLocationAdminMapping : IOSLocationAdminMapping {
        DBHelper dbHelper = new DBHelper();
        bool IOSLocationAdminMapping.SaveInfoes(List<OSLocationInfo> infoes) {
            DataTable data = DataHelper.CreateDataTable<OSLocationInfo>(infoes);
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_saveOSLocationAdminMapping.infoes,data)
            };
            int result = dbHelper.Execute(Config.Procedures.cp_sp_saveOSLocationAdminMapping.name, sqlParameters, CommandType.StoredProcedure);
            if (result == 0) {
                return false;
            }
            return true;
        }
    }
}
