﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SPAPI.SqlServerDAL;
using Daimler.MicroServices.SP.BizObject.OfficeStationary;
using Daimler.MicroServices.SPAPI.IDAL;
using System.Data;
using Daimler.MicroServices.Utils.Data;
using Newtonsoft.Json;

namespace Daimler.MicroServices.SPAPI.SqlServerDAL {
    public class OfficeStationaryRequests : IOfficeStationaryRequests {
        DBHelper dbHelper = new DBHelper();
        bool IOfficeStationaryRequests.SaveInfoes(List<StationaryRequestInfo> infoes) {

            DataTable data = DataHelper.CreateDataTable<StationaryRequestInfo>(infoes);
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_saveOfficeStationaryRequests.infoes,data)
            };
            int result = dbHelper.Execute(Config.Procedures.cp_sp_saveOfficeStationaryRequests.name, sqlParameters, CommandType.StoredProcedure);
            if (result == 0) {
                return false;
            }
            return true;
        }



        int IOfficeStationaryRequests.GetPendingStationaryCount(int userId) {
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_getPendingStationaryCount.userId,userId)
            };
            object result = dbHelper.ExcuteMyScalar(Config.Procedures.cp_sp_getPendingStationaryCount.name, sqlParameters, CommandType.StoredProcedure);
            if (result != null) {
                return Convert.ToInt32(result);
            }
            return 0;
        }


        List<StationaryRequestItem> IOfficeStationaryRequests.GetPendingStationaryList(int userId, int? pageIndex, int? pageSize) {
            List<StationaryRequestItem> result = null;
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_getPendingStationarySnapshotV2.userId,userId),
                new SqlParameter(Config.Procedures.cp_sp_getPendingStationarySnapshotV2.pageIndex,pageIndex),
                new SqlParameter(Config.Procedures.cp_sp_getPendingStationarySnapshotV2.pageSize,pageSize),
            };
            DataSet requests = dbHelper.ExcuteQuery(Config.Procedures.cp_sp_getPendingStationarySnapshotV2.name, sqlParameters, CommandType.StoredProcedure);
            if (requests != null && requests.Tables != null && requests.Tables.Count > 0) {
                IEnumerable<DataRow> rows = requests.Tables[0].AsEnumerable();
                if (rows != null && rows.Count() > 0) {
                    result = rows.Select(row => new StationaryRequestItem {
                        ID = row.Field<int>("ID"),
                        //Company = row.Field<string>("Company"),
                        CreatedTime = Convert.ToDateTime(row.Field<DateTime?>("Modified")),
                        Status = row.Field<string>("Status"),
                        TotalAmount = Convert.ToDouble(row.Field<decimal>("TotalAmount")),
                        UserName = row.Field<string>("UserName"),
                    }).ToList();
                }
            }
            return result;
        }




        APIStationaryRequestDetail IOfficeStationaryRequests.GetStationaryDetail(int ID) {
            APIStationaryRequestDetail result = null;
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_getStationaryDetail.ID,ID),
            };
            DataSet requests = dbHelper.ExcuteQuery(Config.Procedures.cp_sp_getStationaryDetail.name, sqlParameters, CommandType.StoredProcedure);
            if (requests != null && requests.Tables != null && requests.Tables.Count > 0) {
                IEnumerable<DataRow> rows = requests.Tables[0].AsEnumerable();
                if (rows != null && rows.Count() > 0) {
                    DataRow row = rows.FirstOrDefault();
                    if (row != null) {
                        result = new APIStationaryRequestDetail {
                            ID = row.Field<int>("ID"),
                            Company = row.Field<string>("Company"),
                            CreatedTime = row.Field<DateTime>("Created"),
                            Status = row.Field<string>("Status"),
                            TotalAmount = Convert.ToDouble(row.Field<decimal>("TotalAmount")),
                            UserName = row.Field<string>("UserName"),
                            CostCenter = row.Field<string>("CostCenter"),
                            ExtensionNumber = row.Field<string>("ExtensionNumber"),
                            LocationCode = row.Field<string>("LocationCode"),
                            Location = row.Field<string>("Location"),
                            DetailAddress = row.Field<string>("DetailAddress"),
                            FullAddress = row.Field<string>("FullAddress"),
                        };
                        result.orders = JsonConvert.DeserializeObject<List<RequestOrder>>(row.Field<string>("Orders"));
                    }
                }
            }
            return result;
        }


        bool IOfficeStationaryRequests.UpdateRequests(List<StationaryRequestInfo> infoes) {
            DataTable data = DataHelper.CreateDataTable<StationaryRequestInfo>(infoes);
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_updateOfficeStationaryRequests.infoes,data)
            };
            int result = dbHelper.Execute(Config.Procedures.cp_sp_updateOfficeStationaryRequests.name, sqlParameters, CommandType.StoredProcedure);
            if (result == 0) {
                return false;
            }
            return true;
        }
    }
}
