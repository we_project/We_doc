﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.FormAB;
using Daimler.MicroServices.SPAPI.IDAL;
using Daimler.MicroServices.Utils.Data;

namespace Daimler.MicroServices.SPAPI.SqlServerDAL {
    public class FormACommentHistory : IFormACommentHistory {
        DBHelper dbHelper = new DBHelper();
        bool IFormACommentHistory.SaveInfoes(List<FormACommentHistoryInfo> infoes) {
            if (infoes == null || infoes.Count == 0) {
                throw new ArgumentNullException();
            }
            List<FormACommentHistoryInfoForDB> infoesForDB = infoes.Select(info => new FormACommentHistoryInfoForDB(info)).ToList();

            DataTable data = DataHelper.CreateDataTable<FormACommentHistoryInfoForDB>(infoesForDB);
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_saveFormACommentHistory.infoes,data)
            };
            int result = dbHelper.Execute(Config.Procedures.cp_sp_saveFormACommentHistory.name, sqlParameters, CommandType.StoredProcedure);
            if (result == 0) {
                return false;
            }
            return true;
        }


        List<FormAComment> IFormACommentHistory.GetCommentList(int caseId) {
            List<FormAComment> result = null;
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_getFormACommentHistory.caseId,caseId),
            };
            DataSet requests = dbHelper.ExcuteQuery(Config.Procedures.cp_sp_getFormACommentHistory.name, sqlParameters, CommandType.StoredProcedure);
            if (requests != null && requests.Tables != null && requests.Tables.Count > 0) {
                IEnumerable<DataRow> rows = requests.Tables[0].AsEnumerable();
                if (rows != null && rows.Count() > 0) {
                    result = rows.Select(row => new FormAComment {
                        ID = row.Field<int>("ID"),
                        Title = row.Field<string>("Title"),
                        CaseID = row.Field<string>("CaseID"),
                        Comment = row.Field<string>("Comment"),
                        User = row.Field<string>("UserStr"),
                        Modified = row.Field<DateTime>("Modified"),
                        Created = row.Field<DateTime>("Created"),
                    }).ToList();
                }
            }
            return result;
        }
    }
}
