﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.AccessManagement;
using Daimler.MicroServices.SPAPI.IDAL;
using Daimler.MicroServices.Utils.Data;
using Newtonsoft.Json;

namespace Daimler.MicroServices.SPAPI.SqlServerDAL {
    public class VisitorBadge : IVisitorBadge {
        DBHelper dbHelper = new DBHelper();
        bool IVisitorBadge.SaveInfoes(List<VistorBadgeApplicationInfo> infoes) {
            DataTable data = DataHelper.CreateDataTable<VistorBadgeApplicationInfo>(infoes);
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_saveVisitorBadge.infoes,data)
            };
            int result = dbHelper.Execute(Config.Procedures.cp_sp_saveVisitorBadge.name, sqlParameters, CommandType.StoredProcedure);
            if (result == 0) {
                return false;
            }
            return true;
        }

        int IVisitorBadge.GetPendingCount(int userId) {
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_getPendingVisitorBadgeCount.userId,userId)
            };
            object result = dbHelper.ExcuteMyScalar(Config.Procedures.cp_sp_getPendingVisitorBadgeCount.name, sqlParameters, CommandType.StoredProcedure);
            if (result != null) {
                return Convert.ToInt32(result);
            }
            return 0;
        }

        List<VisitorBadgeItem> IVisitorBadge.GetPendingList(int userId) {
            List<VisitorBadgeItem> result = null;
            SqlParameter[] sqlParameters = {
                new SqlParameter(Config.Procedures.cp_sp_getPendingVisitorBadge.userId,userId),
            };
            DataSet requests = dbHelper.ExcuteQuery(Config.Procedures.cp_sp_getPendingVisitorBadge.name, sqlParameters, CommandType.StoredProcedure);
            if (requests != null && requests.Tables != null && requests.Tables.Count > 0) {
                IEnumerable<DataRow> rows = requests.Tables[0].AsEnumerable();
                if (rows != null && rows.Count() > 0) {
                    result = rows.Select(row => new VisitorBadgeItem {
                        Id = row.Field<int>("ID"),
                        ApplicationDate = row.Field<DateTime?>("Application_x0020_Date"),
                        ApplicationType = row.Field<string>("Application_x0020_Type"),
                        EmployeeName = row.Field<string>("Employee_x0020_Name"),
                        Status = row.Field<string>("Status"),
                    }).ToList();
                }
            }
            return result;
        }

        VisitorBadgeDetail1 IVisitorBadge.GetDetail(int id) {
            VisitorBadgeDetail1 result = null;
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_getVisitorBadgeDetail.ID,id),
            };
            DataSet requests = dbHelper.ExcuteQuery(Config.Procedures.cp_sp_getVisitorBadgeDetail.name, sqlParameters, CommandType.StoredProcedure);
            if (requests != null && requests.Tables != null && requests.Tables.Count > 1) {
                // VisitorBadge
                IEnumerable<DataRow> rows = requests.Tables[0].AsEnumerable();
                IEnumerable<DataRow> areas = requests.Tables[1].AsEnumerable();
                if (rows != null && rows.Count() > 0) {
                    DataRow row = rows.FirstOrDefault();
                    if (row != null) {
                        result = new VisitorBadgeDetail1 {
                            ID = row.Field<int>("ID"),
                            Status = row.Field<string>("Status"),
                            CCApprover = new UserInfo {
                                ID = row.Field<int?>("CCApproverId"),
                                Title = row.Field<string>("CCApproverTitle")
                            },
                            CCDeputy = new UserInfo {
                                ID = row.Field<int?>("CCDeputyId"),
                                Title = row.Field<string>("CCDeputyTitle")
                            },
                            Application_x0020_Date = row.Field<DateTime?>("Application_x0020_Date"),
                            Application_x0020_Type = row.Field<string>("Application_x0020_Type"),
                            Employee_x0020_Name = new UserInfo {
                                ID = row.Field<int?>("Employee_x0020_NameId"),
                                Title = row.Field<string>("Employee_x0020_NameTitle")
                            },
                            Title = row.Field<string>("Title"),
                            Employee_x0020_Number = row.Field<string>("Employee_x0020_Number"),
                            Company = row.Field<string>("Company"),
                            Department = row.Field<string>("Department"),
                            Deskphone_x0020_Number = row.Field<string>("Deskphone_x0020_Number"),
                            Cost_x0020_Center = row.Field<string>("Cost_x0020_Center"),
                            Badge_x0020_Type = row.Field<string>("Badge_x0020_Type"),
                            Arrive_x0020_Date = row.Field<DateTime?>("Arrive_x0020_Date"),

                            Leave_x0020_Date = row.Field<DateTime?>("Leave_x0020_Date"),

                            Reason_x0020_for_x0020_Visit = row.Field<string>("Reason_x0020_for_x0020_Visit"),
                            Remarks = row.Field<string>("Remarks"),
                            NumOfLocation = row.Field<string>("NumOfLocation"),
                            Site_x0020_Name = new SiteInfo {
                                ID = row.Field<int?>("Site_x0020_NameId"),
                                Title = row.Field<string>("Site_x0020_NameTitle")
                            },
                            Location_x0020_1 = new LocationInfo {
                                ID = row.Field<int?>("Location_x0020_1Id"),
                                Title = row.Field<string>("Location_x0020_1Title")
                            },
                            Location_x0020_2 = new LocationInfo {
                                ID = row.Field<int?>("Location_x0020_2Id"),
                                Title = row.Field<string>("Location_x0020_2Title")
                            },
                            Location_x0020_3 = new LocationInfo {
                                ID = row.Field<int?>("Location_x0020_3Id"),
                                Title = row.Field<string>("Location_x0020_3Title")
                            },


                            Location_x0020_4 = new LocationInfo {
                                ID = row.Field<int?>("Location_x0020_4Id"),
                                Title = row.Field<string>("Location_x0020_4Title")
                            },

                            Location_x0020_5 = new LocationInfo {
                                ID = row.Field<int?>("Location_x0020_5Id"),
                                Title = row.Field<string>("Location_x0020_5Title")
                            },

                            Location_x0020_6 = new LocationInfo {
                                ID = row.Field<int?>("Location_x0020_6Id"),
                                Title = row.Field<string>("Location_x0020_6Title")
                            },


                            Location_x0020_7 = new LocationInfo {
                                ID = row.Field<int?>("Location_x0020_7Id"),
                                Title = row.Field<string>("Location_x0020_7Title")
                            },

                            SAApprover1Id = row.Field<int?>("SAApprover1Id"),

                            SAApprover2Id = row.Field<int?>("SAApprover2Id"),

                            SAApprover3Id = row.Field<int?>("SAApprover3Id"),

                            SAApprover4Id = row.Field<int?>("SAApprover4Id"),

                            SAApprover5Id = row.Field<int?>("SAApprover5Id"),

                            SAApprover6Id = row.Field<int?>("SAApprover6Id"),

                            SAApprover7Id = row.Field<int?>("SAApprover7Id"),
                            SADeputy1Id = row.Field<int?>("SADeputy1Id"),
                            SADeputy2Id = row.Field<int?>("SADeputy2Id"),

                            SADeputy3Id = row.Field<int?>("SADeputy3Id"),

                            SADeputy4Id = row.Field<int?>("SADeputy4Id"),

                            SADeputy5Id = row.Field<int?>("SADeputy5Id"),

                            SADeputy6Id = row.Field<int?>("SADeputy6Id"),

                            SADeputy7Id = row.Field<int?>("SADeputy7Id"),
                            SAStatus1 = row.Field<string>("SAStatus1"),
                            SAStatus2 = row.Field<string>("SAStatus2"),

                            SAStatus3 = row.Field<string>("SAStatus3"),

                            SAStatus4 = row.Field<string>("SAStatus4"),

                            SAStatus5 = row.Field<string>("SAStatus5"),

                            SAStatus6 = row.Field<string>("SAStatus6"),

                            SAStatus7 = row.Field<string>("SAStatus7"),
                            Date_x0020_From_x0020_1 = row.Field<DateTime?>("Date_x0020_From_x0020_1"),

                            Date_x0020_From_x0020_2 = row.Field<DateTime?>("Date_x0020_From_x0020_2"),

                            Date_x0020_From_x0020_3 = row.Field<DateTime?>("Date_x0020_From_x0020_3"),

                            Date_x0020_From_x0020_4 = row.Field<DateTime?>("Date_x0020_From_x0020_4"),

                            Date_x0020_From_x0020_5 = row.Field<DateTime?>("Date_x0020_From_x0020_5"),

                            Date_x0020_From_x0020_6 = row.Field<DateTime?>("Date_x0020_From_x0020_6"),

                            Date_x0020_From_x0020_7 = row.Field<DateTime?>("Date_x0020_From_x0020_7"),

                            Date_x0020_To_x0020_1 = row.Field<DateTime?>("Date_x0020_To_x0020_1"),

                            Date_x0020_To_x0020_2 = row.Field<DateTime?>("Date_x0020_To_x0020_2"),

                            Date_x0020_To_x0020_3 = row.Field<DateTime?>("Date_x0020_To_x0020_3"),

                            Date_x0020_To_x0020_4 = row.Field<DateTime?>("Date_x0020_To_x0020_4"),

                            Date_x0020_To_x0020_5 = row.Field<DateTime?>("Date_x0020_To_x0020_5"),

                            Date_x0020_To_x0020_6 = row.Field<DateTime?>("Date_x0020_To_x0020_6"),

                            Date_x0020_To_x0020_7 = row.Field<DateTime?>("Date_x0020_To_x0020_7"),
                            NumOfVisitor = row.Field<string>("NumOfVisitor"),
                            First_x0020_Name_x0020_1 = row.Field<string>("First_x0020_Name_x0020_1"),

                            First_x0020_Name_x0020_2 = row.Field<string>("First_x0020_Name_x0020_2"),

                            First_x0020_Name_x0020_3 = row.Field<string>("First_x0020_Name_x0020_3"),

                            First_x0020_Name_x0020_4 = row.Field<string>("First_x0020_Name_x0020_4"),

                            First_x0020_Name_x0020_5 = row.Field<string>("First_x0020_Name_x0020_5"),

                            Last_x0020_Name_x0020_1 = row.Field<string>("Last_x0020_Name_x0020_1"),

                            Last_x0020_Name_x0020_2 = row.Field<string>("Last_x0020_Name_x0020_2"),

                            Last_x0020_Name_x0020_3 = row.Field<string>("Last_x0020_Name_x0020_3"),

                            Last_x0020_Name_x0020_4 = row.Field<string>("Last_x0020_Name_x0020_4"),

                            Last_x0020_Name_x0020_5 = row.Field<string>("Last_x0020_Name_x0020_5"),
                            Visitor_x0020_Company_x0020_1 = row.Field<string>("Visitor_x0020_Company_x0020_1"),

                            Visitor_x0020_Mobile_x0020_1 = row.Field<string>("Visitor_x0020_Mobile_x0020_1"),


                            Visitor_x0020_Company_x0020_2 = row.Field<string>("Visitor_x0020_Company_x0020_2"),

                            Visitor_x0020_Mobile_x0020_2 = row.Field<string>("Visitor_x0020_Mobile_x0020_2"),


                            Visitor_x0020_Company_x0020_3 = row.Field<string>("Visitor_x0020_Company_x0020_3"),

                            Visitor_x0020_Mobile_x0020_3 = row.Field<string>("Visitor_x0020_Mobile_x0020_3"),


                            Visitor_x0020_Company_x0020_4 = row.Field<string>("Visitor_x0020_Company_x0020_4"),

                            Visitor_x0020_Mobile_x0020_4 = row.Field<string>("Visitor_x0020_Mobile_x0020_4"),


                            Visitor_x0020_Company_x0020_5 = row.Field<string>("Visitor_x0020_Company_x0020_5"),

                            Visitor_x0020_Mobile_x0020_5 = row.Field<string>("Visitor_x0020_Mobile_x0020_5"),
                        };

                        Func<string, int[]> GetAreaIds = (field) => {
                            return JsonConvert.DeserializeObject<int[]>(row.Field<string>(field));
                        };
                        int[] area1Ids = GetAreaIds("Area1Id");
                        int[] area2Ids = GetAreaIds("Area2Id");
                        int[] area3Ids = GetAreaIds("Area3Id");
                        int[] area4Ids = GetAreaIds("Area4Id");
                        int[] area5Ids = GetAreaIds("Area5Id");
                        int[] area6Ids = GetAreaIds("Area6Id");
                        int[] area7Ids = GetAreaIds("Area7Id");

                        Func<int[], AreaInfo[]> GetAreaInfoes = (areaIds) => {
                            return areaIds != null && areaIds.Count() > 0 ? areaIds.Join(areas, (areaId) => areaId, (area) => area.Field<int>("ID"), (areaId, area) => new AreaInfo {
                                ID = areaId,
                                Title = area.Field<string>("Title")
                            }).ToArray() : null;
                        };

                        result.Area_x0020_1 = GetAreaInfoes(area1Ids);
                        result.Area_x0020_2 = GetAreaInfoes(area2Ids);
                        result.Area_x0020_3 = GetAreaInfoes(area3Ids);
                        result.Area_x0020_4 = GetAreaInfoes(area4Ids);
                        result.Area_x0020_5 = GetAreaInfoes(area5Ids);
                        result.Area_x0020_6 = GetAreaInfoes(area6Ids);
                        result.Area_x0020_7 = GetAreaInfoes(area7Ids);
                    }
                }
            }
            return result;
        }

        bool IVisitorBadge.UpdateRequests(List<VistorBadgeApplicationInfo> infoes) {
            DataTable data = DataHelper.CreateDataTable<VistorBadgeApplicationInfo>(infoes);
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_updateVisitorBadges.infoes,data)
            };
            int result = dbHelper.Execute(Config.Procedures.cp_sp_updateVisitorBadges.name, sqlParameters, CommandType.StoredProcedure);
            if (result == 0) {
                return false;
            }
            return true;
        }
    }
}
