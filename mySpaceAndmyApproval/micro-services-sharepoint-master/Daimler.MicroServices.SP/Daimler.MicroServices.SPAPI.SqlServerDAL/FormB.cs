﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.FormAB;
using Daimler.MicroServices.SPAPI.IDAL;
using Daimler.MicroServices.Utils.Data;

namespace Daimler.MicroServices.SPAPI.SqlServerDAL {
    public class FormB : IFormB {
        DBHelper dbHelper = new DBHelper();
        bool IFormB.SaveInfoes(List<FormBInfo> infoes) {
            DataTable data = DataHelper.CreateDataTable<FormBInfo>(infoes);
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_saveFormB.infoes,data)
            };
            int result = dbHelper.Execute(Config.Procedures.cp_sp_saveFormB.name, sqlParameters, CommandType.StoredProcedure);
            if (result == 0) {
                return false;
            }
            return true;
        }

        bool IFormB.UpdateRequests(List<FormBInfo> infoes) {
            DataTable data = DataHelper.CreateDataTable<FormBInfo>(infoes);
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_updateFormB.infoes,data)
            };
            int result = dbHelper.Execute(Config.Procedures.cp_sp_updateFormB.name, sqlParameters, CommandType.StoredProcedure);
            if (result == 0) {
                return false;
            }
            return true;
        }

        int IFormB.GetPendingCount(int userId) {
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_getPendingFormBCount.userId,userId)
            };
            object result = dbHelper.ExcuteMyScalar(Config.Procedures.cp_sp_getPendingFormBCount.name, sqlParameters, CommandType.StoredProcedure);
            if (result != null) {
                return Convert.ToInt32(result);
            }
            return 0;
        }
        List<FormBItem> IFormB.GetList(int userId, int? pageIndex, int? pageSize) {
            List<FormBItem> result = null;
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_getFormBV2.userId,userId),
                new SqlParameter(Config.Procedures.cp_sp_getFormBV2.pageIndex,pageIndex),
                new SqlParameter(Config.Procedures.cp_sp_getFormBV2.pageSize,pageSize),
            };
            DataSet requests = dbHelper.ExcuteQuery(Config.Procedures.cp_sp_getFormBV2.name, sqlParameters, CommandType.StoredProcedure);
            if (requests != null && requests.Tables != null && requests.Tables.Count > 0) {
                IEnumerable<DataRow> rows = requests.Tables[0].AsEnumerable();
                if (rows != null && rows.Count() > 0) {
                    result = rows.Select(row => new FormBItem {
                        ID = row.Field<int>("ID"),
                        //Title = row.Field<string>("Title"),
                        CaseID = row.Field<string>("CaseID"),
                        Status = row.Field<string>("Status"),
                        //FlowPath = row.Field<string>("FlowPath"),
                        Requestor = row.Field<string>("Requestor"),
                        //CostCenter = row.Field<string>("CostCenter"),
                        //Tax = row.Field<string>("Tax"),
                        //_x0054_ax2 = row.Field<string>("_x0054_ax2"),
                        //TaxManager = row.Field<string>("TaxManager"),
                        //Controlling = row.Field<string>("Controlling"),
                        //Controlling2 = row.Field<string>("Controlling2"),
                        //BenefitTo = row.Field<string>("BenefitTo"),
                        //ExternalNumber = row.Field<string>("ExternalNumber"),
                        //IsEntitled = row.Field<bool?>("IsEntitled").HasValue ? row.Field<bool?>("IsEntitled").Value : false,
                        //RecipientsNumber = row.Field<string>("RecipientsNumber"),
                        //TotalCost = row.Field<string>("TotalCost"),
                        //EIUnitCost1 = row.Field<string>("EIUnitCost1"),
                        //EIUnitCost2 = row.Field<string>("EIUnitCost2"),
                        //EIUnitCost3 = row.Field<string>("EIUnitCost3"),
                        //EIUnitCost4 = row.Field<string>("EIUnitCost4"),
                        //EIUnitCost5 = row.Field<string>("EIUnitCost5"),
                        //EIUnitCost6 = row.Field<string>("EIUnitCost6"),
                        //EIVolume1 = row.Field<string>("EIVolume1"),
                        //EIVolume2 = row.Field<string>("EIVolume2"),
                        //EIVolume3 = row.Field<string>("EIVolume3"),
                        //EIVolume4 = row.Field<string>("EIVolume4"),
                        //EIVolume5 = row.Field<string>("EIVolume5"),
                        //EIVolume6 = row.Field<string>("EIVolume6"),
                        //EITotalCost1 = row.Field<string>("EITotalCost1"),
                        //EITotalCost2 = row.Field<string>("EITotalCost2"),
                        //EITotalCost3 = row.Field<string>("EITotalCost3"),
                        //EITotalCost4 = row.Field<string>("EITotalCost4"),
                        //EITotalCost5 = row.Field<string>("EITotalCost5"),
                        //EITotalCost6 = row.Field<string>("EITotalCost6"),
                        //EEUnitCost1 = row.Field<string>("EEUnitCost1"),
                        //EEUnitCost2 = row.Field<string>("EEUnitCost2"),
                        //EEUnitCost3 = row.Field<string>("EEUnitCost3"),
                        //EEUnitCost4 = row.Field<string>("EEUnitCost4"),
                        //EEUnitCost5 = row.Field<string>("EEUnitCost5"),
                        //EEUnitCost6 = row.Field<string>("EEUnitCost6"),
                        //EEVolume1 = row.Field<string>("EEVolume1"),
                        //EEVolume2 = row.Field<string>("EEVolume2"),
                        //EEVolume3 = row.Field<string>("EEVolume3"),
                        //EEVolume4 = row.Field<string>("EEVolume4"),
                        //EEVolume5 = row.Field<string>("EEVolume5"),
                        //EEVolume6 = row.Field<string>("EEVolume6"),
                        //EETotalCost1 = row.Field<string>("EETotalCost1"),
                        //EETotalCost2 = row.Field<string>("EETotalCost2"),
                        //EETotalCost3 = row.Field<string>("EETotalCost3"),
                        //EETotalCost4 = row.Field<string>("EETotalCost4"),
                        //EETotalCost5 = row.Field<string>("EETotalCost5"),
                        //EETotalCost6 = row.Field<string>("EETotalCost6"),
                        //InternalUnitCost1 = row.Field<string>("InternalUnitCost1"),
                        //InternalUnitCost2 = row.Field<string>("InternalUnitCost2"),
                        //InternalUnitCost3 = row.Field<string>("InternalUnitCost3"),
                        //InternalUnitCost4 = row.Field<string>("InternalUnitCost4"),
                        //InternalUnitCost5 = row.Field<string>("InternalUnitCost5"),
                        //InternalUnitCost6 = row.Field<string>("InternalUnitCost6"),
                        //InternalVolume1 = row.Field<string>("InternalVolume1"),
                        //InternalVolume2 = row.Field<string>("InternalVolume2"),
                        //InternalVolume3 = row.Field<string>("InternalVolume3"),
                        //InternalVolume4 = row.Field<string>("InternalVolume4"),
                        //InternalVolume5 = row.Field<string>("InternalVolume5"),
                        //InternalVolume6 = row.Field<string>("InternalVolume6"),
                        //InternalTotalCost1 = row.Field<string>("InternalTotalCost1"),
                        //InternalTotalCost2 = row.Field<string>("InternalTotalCost2"),
                        //InternalTotalCost3 = row.Field<string>("InternalTotalCost3"),
                        //InternalTotalCost4 = row.Field<string>("InternalTotalCost4"),
                        //InternalTotalCost5 = row.Field<string>("InternalTotalCost5"),
                        //InternalTotalCost6 = row.Field<string>("InternalTotalCost6"),
                        //ExpenditureItem4 = row.Field<string>("ExpenditureItem4"),
                        //ExpenditureItem5 = row.Field<string>("ExpenditureItem5"),
                        //ExpenditureItem6 = row.Field<string>("ExpenditureItem6"),
                        //CalculationMethod = row.Field<string>("CalculationMethod"),
                        //ActualIITAmount = row.Field<string>("ActualIITAmount"),
                        //ActualIITAmountOccasional = row.Field<string>("ActualIITAmountOccasional"),
                        //ActualIITAmountLabor = row.Field<string>("ActualIITAmountLabor"),
                        //ActualOutputAmount = row.Field<string>("ActualOutputAmount"),
                        //ActualInputAmount = row.Field<string>("ActualInputAmount"),
                        //NewComment = row.Field<string>("NewComment"),
                        //CurrentUser = row.Field<string>("CurrentUser"),
                        //IsStatusChange = row.Field<bool?>("IsStatusChange").HasValue ? row.Field<bool?>("IsStatusChange").Value : false,
                        //StatusIndex = row.Field<string>("StatusIndex"),
                        //VATRate = row.Field<string>("VATRate"),
                        //FormACaseID = row.Field<string>("FormACaseID"),
                        //FormB = row.Field<string>("FormB"),
                        //IsReject = row.Field<bool?>("IsReject").HasValue ? row.Field<bool?>("IsReject").Value : false,
                        //FormBComments = row.Field<string>("FormBComments"),
                        //FormBComments_x0028_1_x0029_ = row.Field<string>("FormBComments_x0028_1_x0029_"),
                        //Note = row.Field<string>("Note"),
                        //Pending = row.Field<string>("Pending"),
                        //Pending2 = row.Field<string>("Pending2"),
                        //CostCenterCode = row.Field<string>("CostCenterCode"),
                        //FormASysID = row.Field<string>("FormASysID"),
                        //IIT = Convert.ToString(row.Field<decimal?>("IIT")),
                        //CompletionDate = Convert.ToDateTime(row.Field<DateTime?>("CompletionDate")),
                        Modified = Convert.ToDateTime(row.Field<DateTime?>("Modified")),
                        Created = Convert.ToDateTime(row.Field<DateTime?>("Created")),
                    }).ToList();
                }
            }
            return result;
        }

        SP.BizObject.FormAB.FormBDetail IFormB.GetDetail(int ID) {
            FormBDetail result = null;
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_getFormBDetailV2.ID,ID),
            };
            DataSet requests = dbHelper.ExcuteQuery(Config.Procedures.cp_sp_getFormBDetailV2.name, sqlParameters, CommandType.StoredProcedure);
            if (requests != null && requests.Tables != null && requests.Tables.Count > 0) {
                IEnumerable<DataRow> rows = requests.Tables[0].AsEnumerable();
                if (rows != null && rows.Count() > 0) {
                    DataRow row = rows.FirstOrDefault();
                    if (row != null) {
                        result = new FormBDetail {
                            ID = row.Field<int>("ID"),
                            //Title = row.Field<string>("Title"),
                            CaseID = row.Field<string>("CaseID"),
                            Status = row.Field<string>("Status"),
                            //FlowPath = row.Field<string>("FlowPath"),
                            Requestor = row.Field<string>("Requestor"),
                            CostCenter = row.Field<string>("CostCenter"),
                            //Tax = row.Field<string>("Tax"),
                            //_x0054_ax2 = row.Field<string>("_x0054_ax2"),
                            //TaxManager = row.Field<string>("TaxManager"),
                            //Controlling = row.Field<string>("Controlling"),
                            //Controlling2 = row.Field<string>("Controlling2"),
                            BenefitTo = row.Field<string>("BenefitTo"),
                            //ExternalNumber = row.Field<string>("ExternalNumber"),
                            //IsEntitled = row.Field<bool?>("IsEntitled").HasValue ? row.Field<bool?>("IsEntitled").Value : false,
                            //RecipientsNumber = row.Field<string>("RecipientsNumber"),
                            //TotalCost = row.Field<string>("TotalCost"),
                            EIUnitCost1 = row.Field<string>("EIUnitCost1"),
                            EIUnitCost2 = row.Field<string>("EIUnitCost2"),
                            EIUnitCost3 = row.Field<string>("EIUnitCost3"),
                            EIUnitCost4 = row.Field<string>("EIUnitCost4"),
                            EIUnitCost5 = row.Field<string>("EIUnitCost5"),
                            EIUnitCost6 = row.Field<string>("EIUnitCost6"),
                            EIVolume1 = row.Field<string>("EIVolume1"),
                            EIVolume2 = row.Field<string>("EIVolume2"),
                            EIVolume3 = row.Field<string>("EIVolume3"),
                            EIVolume4 = row.Field<string>("EIVolume4"),
                            EIVolume5 = row.Field<string>("EIVolume5"),
                            EIVolume6 = row.Field<string>("EIVolume6"),
                            EITotalCost1 = row.Field<string>("EITotalCost1"),
                            EITotalCost2 = row.Field<string>("EITotalCost2"),
                            EITotalCost3 = row.Field<string>("EITotalCost3"),
                            EITotalCost4 = row.Field<string>("EITotalCost4"),
                            EITotalCost5 = row.Field<string>("EITotalCost5"),
                            EITotalCost6 = row.Field<string>("EITotalCost6"),
                            EEUnitCost1 = row.Field<string>("EEUnitCost1"),
                            EEUnitCost2 = row.Field<string>("EEUnitCost2"),
                            EEUnitCost3 = row.Field<string>("EEUnitCost3"),
                            EEUnitCost4 = row.Field<string>("EEUnitCost4"),
                            EEUnitCost5 = row.Field<string>("EEUnitCost5"),
                            EEUnitCost6 = row.Field<string>("EEUnitCost6"),
                            EEVolume1 = row.Field<string>("EEVolume1"),
                            EEVolume2 = row.Field<string>("EEVolume2"),
                            EEVolume3 = row.Field<string>("EEVolume3"),
                            EEVolume4 = row.Field<string>("EEVolume4"),
                            EEVolume5 = row.Field<string>("EEVolume5"),
                            EEVolume6 = row.Field<string>("EEVolume6"),
                            EETotalCost1 = row.Field<string>("EETotalCost1"),
                            EETotalCost2 = row.Field<string>("EETotalCost2"),
                            EETotalCost3 = row.Field<string>("EETotalCost3"),
                            EETotalCost4 = row.Field<string>("EETotalCost4"),
                            EETotalCost5 = row.Field<string>("EETotalCost5"),
                            EETotalCost6 = row.Field<string>("EETotalCost6"),
                            InternalUnitCost1 = row.Field<string>("InternalUnitCost1"),
                            InternalUnitCost2 = row.Field<string>("InternalUnitCost2"),
                            InternalUnitCost3 = row.Field<string>("InternalUnitCost3"),
                            InternalUnitCost4 = row.Field<string>("InternalUnitCost4"),
                            InternalUnitCost5 = row.Field<string>("InternalUnitCost5"),
                            InternalUnitCost6 = row.Field<string>("InternalUnitCost6"),
                            InternalVolume1 = row.Field<string>("InternalVolume1"),
                            InternalVolume2 = row.Field<string>("InternalVolume2"),
                            InternalVolume3 = row.Field<string>("InternalVolume3"),
                            InternalVolume4 = row.Field<string>("InternalVolume4"),
                            InternalVolume5 = row.Field<string>("InternalVolume5"),
                            InternalVolume6 = row.Field<string>("InternalVolume6"),
                            InternalTotalCost1 = row.Field<string>("InternalTotalCost1"),
                            InternalTotalCost2 = row.Field<string>("InternalTotalCost2"),
                            InternalTotalCost3 = row.Field<string>("InternalTotalCost3"),
                            InternalTotalCost4 = row.Field<string>("InternalTotalCost4"),
                            InternalTotalCost5 = row.Field<string>("InternalTotalCost5"),
                            InternalTotalCost6 = row.Field<string>("InternalTotalCost6"),
                            ExpenditureItem4 = row.Field<string>("ExpenditureItem4"),
                            ExpenditureItem5 = row.Field<string>("ExpenditureItem5"),
                            ExpenditureItem6 = row.Field<string>("ExpenditureItem6"),
                            //CalculationMethod = row.Field<string>("CalculationMethod"),
                            //ActualIITAmount = row.Field<string>("ActualIITAmount"),
                            //ActualIITAmountOccasional = row.Field<string>("ActualIITAmountOccasional"),
                            //ActualIITAmountLabor = row.Field<string>("ActualIITAmountLabor"),
                            //ActualOutputAmount = row.Field<string>("ActualOutputAmount"),
                            //ActualInputAmount = row.Field<string>("ActualInputAmount"),
                            //NewComment = row.Field<string>("NewComment"),
                            //CurrentUser = row.Field<string>("CurrentUser"),
                            //IsStatusChange = row.Field<bool?>("IsStatusChange").HasValue ? row.Field<bool?>("IsStatusChange").Value : false,
                            StatusIndex = row.Field<string>("StatusIndex"),
                            //VATRate = row.Field<string>("VATRate"),
                            //FormACaseID = row.Field<string>("FormACaseID"),
                            //FormB = row.Field<string>("FormB"),
                            //IsReject = row.Field<bool?>("IsReject").HasValue ? row.Field<bool?>("IsReject").Value : false,
                            //FormBComments = row.Field<string>("FormBComments"),
                            //FormBComments_x0028_1_x0029_ = row.Field<string>("FormBComments_x0028_1_x0029_"),
                            //Note = row.Field<string>("Note"),
                            //Pending = row.Field<string>("Pending"),
                            //Pending2 = row.Field<string>("Pending2"),
                            //CostCenterCode = row.Field<string>("CostCenterCode"),
                            //FormASysID = row.Field<string>("FormASysID"),
                            //IIT = Convert.ToString(row.Field<decimal?>("IIT")),
                            //CompletionDate = Convert.ToDateTime(row.Field<DateTime?>("CompletionDate")),
                            Modified = Convert.ToDateTime(row.Field<DateTime?>("Modified")),
                            Created = Convert.ToDateTime(row.Field<DateTime?>("Created")),
                        };
                    }
                }
            }
            return result;
        }


    }
}
