﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.FormAB;
using Daimler.MicroServices.SPAPI.IDAL;
using Daimler.MicroServices.Utils.Data;

namespace Daimler.MicroServices.SPAPI.SqlServerDAL {
    public class FormA : IFormA {
        DBHelper dbHelper = new DBHelper();
        bool IFormA.SaveInfoes(List<FormAInfo> infoes) {
            DataTable data = DataHelper.CreateDataTable<FormAInfo>(infoes);
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_saveFormA.infoes,data)
            };
            int result = dbHelper.Execute(Config.Procedures.cp_sp_saveFormA.name, sqlParameters, CommandType.StoredProcedure);
            if (result == 0) {
                return false;
            }
            return true;
        }

        bool IFormA.UpdateRequests(List<SP.BizObject.FormAB.FormAInfo> infoes) {
            DataTable data = DataHelper.CreateDataTable<FormAInfo>(infoes);
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_updateFormA.infoes,data)
            };
            int result = dbHelper.Execute(Config.Procedures.cp_sp_updateFormA.name, sqlParameters, CommandType.StoredProcedure);
            if (result == 0) {
                return false;
            }
            return true;
        }
        int IFormA.GetPendingCount(int userId) {
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_getPendingFormACount.userId,userId)
            };
            object result = dbHelper.ExcuteMyScalar(Config.Procedures.cp_sp_getPendingFormACount.name, sqlParameters, CommandType.StoredProcedure);
            if (result != null) {
                return Convert.ToInt32(result);
            }
            return 0;
        }
        List<FormAItem> IFormA.GetList(int userId, int? pageIndex, int? pageSize) {
            List<FormAItem> result = null;
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_getFormAV2.userId,userId),
                new SqlParameter(Config.Procedures.cp_sp_getFormAV2.pageIndex,pageIndex),
                new SqlParameter(Config.Procedures.cp_sp_getFormAV2.pageSize,pageSize),
            };
            DataSet requests = dbHelper.ExcuteQuery(Config.Procedures.cp_sp_getFormAV2.name, sqlParameters, CommandType.StoredProcedure);
            if (requests != null && requests.Tables != null && requests.Tables.Count > 0) {
                IEnumerable<DataRow> rows = requests.Tables[0].AsEnumerable();
                if (rows != null && rows.Count() > 0) {
                    result = rows.Select(row => new FormAItem {
                        ID = row.Field<int>("ID"),
                        //Title = row.Field<string>("Title"),
                        CaseID = row.Field<string>("CaseID"),
                        Status = row.Field<string>("Status"),
                        //FlowPath = row.Field<string>("FlowPath"),
                        Requestor = row.Field<string>("Requestor"),
                        //CostCenter = row.Field<string>("CostCenter"),
                        //Tax = row.Field<string>("Tax"),
                        //TaxManager = row.Field<string>("TaxManager"),
                        //Controlling = row.Field<string>("Controlling"),
                        //ContactNumber = row.Field<string>("ContactNumber"),
                        //IONo = row.Field<string>("IONo"),
                        //PRNo = row.Field<string>("PRNo"),
                        //EventName = row.Field<string>("EventName"),
                        //TargetGroup = row.Field<string>("TargetGroup"),
                        //SelectionCriteria = row.Field<string>("SelectionCriteria"),
                        //ActivityDescription = row.Field<string>("ActivityDescription"),
                        //ActivityNature = row.Field<string>("ActivityNature"),
                        //OtherSelectionCriteria = row.Field<string>("OtherSelectionCriteria"),
                        //BenefitTo = row.Field<string>("BenefitTo"),
                        //BenefitAmount = row.Field<string>("BenefitAmount"),
                        //ExternalNumber = row.Field<string>("ExternalNumber"),
                        //EICost1 = row.Field<string>("EICost1"),
                        //EICost2 = row.Field<string>("EICost2"),
                        //EICost3 = row.Field<string>("EICost3"),
                        //EECost1 = row.Field<string>("EECost1"),
                        //EECost2 = row.Field<string>("EECost2"),
                        //EECost3 = row.Field<string>("EECost3"),
                        //InternalCost1 = row.Field<string>("InternalCost1"),
                        //InternalCost2 = row.Field<string>("InternalCost2"),
                        //InternalCost3 = row.Field<string>("InternalCost3"),
                        //ExpenditureItem4 = row.Field<string>("ExpenditureItem4"),
                        //ExpenditureItem5 = row.Field<string>("ExpenditureItem5"),
                        //ExpenditureItem6 = row.Field<string>("ExpenditureItem6"),
                        //EICost4 = row.Field<string>("EICost4"),
                        //EICost5 = row.Field<string>("EICost5"),
                        //EICost6 = row.Field<string>("EICost6"),
                        //EECost4 = row.Field<string>("EECost4"),
                        //EECost5 = row.Field<string>("EECost5"),
                        //EECost6 = row.Field<string>("EECost6"),
                        //InternalCost4 = row.Field<string>("InternalCost4"),
                        //InternalCost5 = row.Field<string>("InternalCost5"),
                        //InternalCost6 = row.Field<string>("InternalCost6"),
                        //EstimatedIITAmount = row.Field<string>("EstimatedIITAmount"),
                        //EstimatedIITAmountOccasional = row.Field<string>("EstimatedIITAmountOccasional"),
                        //EstimatedIITAmountLabor = row.Field<string>("EstimatedIITAmountLabor"),
                        //EstimatedOutputAmount = row.Field<string>("EstimatedOutputAmount"),
                        //EstimatedInputAmount = row.Field<string>("EstimatedInputAmount"),
                        //Note = row.Field<string>("Note"),
                        //StatusIndex = row.Field<string>("StatusIndex"),
                        //_x0054_ax2 = row.Field<string>("_x0054_ax2"),
                        //Controlling2 = row.Field<string>("Controlling2"),
                        //NewComment = row.Field<string>("NewComment"),
                        //TeamGroup = row.Field<string>("TeamGroup"),
                        //LegalEntity = row.Field<string>("LegalEntity"),
                        //FormBCount = row.Field<string>("FormBCount"),
                        //IsStatusChange = row.Field<bool>("IsStatusChange"),
                        //FormA = row.Field<string>("FormA"),
                        //CalculationMethod = row.Field<string>("CalculationMethod"),
                        //VATRate = row.Field<string>("VATRate"),
                        //CurrentUser = row.Field<string>("CurrentUser"),
                        //FormAComments = row.Field<string>("FormAComments"),
                        //Year = row.Field<string>("Year"),
                        //IDIndex = row.Field<string>("IDIndex"),
                        //IsReject = row.Field<bool>("IsReject"),
                        //AddFormB = row.Field<string>("AddFormB"),
                        //Pending = row.Field<string>("Pending"),
                        //Pending2 = row.Field<string>("Pending2"),
                        //CostCenterCode = row.Field<string>("CostCenterCode"),
                        //FormBAmount = row.Field<string>("FormBAmount"),
                        //SurplusBudget = Convert.ToString(row.Field<decimal>("SurplusBudget")),
                        //EICost1_Submit = row.Field<string>("EICost1_Submit"),
                        //EICost2_Submit = row.Field<string>("EICost2_Submit"),
                        //EICost3_Submit = row.Field<string>("EICost3_Submit"),
                        //EICost4_Submit = row.Field<string>("EICost4_Submit"),
                        //EICost5_Submit = row.Field<string>("EICost5_Submit"),
                        //EICost6_Submit = row.Field<string>("EICost6_Submit"),
                        //EECost1_Submit = row.Field<string>("EECost1_Submit"),
                        //EECost2_Submit = row.Field<string>("EECost2_Submit"),
                        //EECost3_Submit = row.Field<string>("EECost3_Submit"),
                        //EECost4_Submit = row.Field<string>("EECost4_Submit"),
                        //EECost5_Submit = row.Field<string>("EECost5_Submit"),
                        //EECost6_Submit = row.Field<string>("EECost6_Submit"),
                        //InternalCost1_Submit = row.Field<string>("InternalCost1_Submit"),
                        //InternalCost2_Submit = row.Field<string>("InternalCost2_Submit"),
                        //InternalCost3_Submit = row.Field<string>("InternalCost3_Submit"),
                        //InternalCost4_Submit = row.Field<string>("InternalCost4_Submit"),
                        //InternalCost5_Submit = row.Field<string>("InternalCost5_Submit"),
                        //InternalCost6_Submit = row.Field<string>("InternalCost6_Submit"),
                        Modified = row.Field<DateTime>("Modified"),
                        Created = row.Field<DateTime>("Created"),
                    }).ToList();
                }
            }
            return result;
        }

        FormADetail IFormA.GetDetail(int ID) {
            FormADetail result = null;
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_getFormADetailV2.ID,ID),
            };
            DataSet requests = dbHelper.ExcuteQuery(Config.Procedures.cp_sp_getFormADetailV2.name, sqlParameters, CommandType.StoredProcedure);
            if (requests != null && requests.Tables != null && requests.Tables.Count > 0) {
                IEnumerable<DataRow> rows = requests.Tables[0].AsEnumerable();
                if (rows != null && rows.Count() > 0) {
                    DataRow row = rows.FirstOrDefault();
                    if (row != null) {
                        result = new FormADetail {
                            ID = row.Field<int>("ID"),
                            //Title = row.Field<string>("Title"),
                            CaseID = row.Field<string>("CaseID"),
                            Status = row.Field<string>("Status"),
                            //FlowPath = row.Field<string>("FlowPath"),
                            Requestor = row.Field<string>("Requestor"),
                            CostCenter = row.Field<string>("CostCenter"),
                            //Tax = row.Field<string>("Tax"),
                            //TaxManager = row.Field<string>("TaxManager"),
                            //Controlling = row.Field<string>("Controlling"),
                            //ContactNumber = row.Field<string>("ContactNumber"),
                            //IONo = row.Field<string>("IONo"),
                            //PRNo = row.Field<string>("PRNo"),
                            EventName = row.Field<string>("EventName"),
                            TargetGroup = row.Field<string>("TargetGroup"),
                            SelectionCriteria = row.Field<string>("SelectionCriteria"),
                            ActivityDescription = row.Field<string>("ActivityDescription"),
                            ActivityNature = row.Field<string>("ActivityNature"),
                            //OtherSelectionCriteria = row.Field<string>("OtherSelectionCriteria"),
                            BenefitTo = row.Field<string>("BenefitTo"),
                            //BenefitAmount = row.Field<string>("BenefitAmount"),
                            ExternalNumber = row.Field<string>("ExternalNumber"),
                            EICost1 = row.Field<string>("EICost1"),
                            EICost2 = row.Field<string>("EICost2"),
                            EICost3 = row.Field<string>("EICost3"),
                            EECost1 = row.Field<string>("EECost1"),
                            EECost2 = row.Field<string>("EECost2"),
                            EECost3 = row.Field<string>("EECost3"),
                            InternalCost1 = row.Field<string>("InternalCost1"),
                            InternalCost2 = row.Field<string>("InternalCost2"),
                            InternalCost3 = row.Field<string>("InternalCost3"),
                            ExpenditureItem4 = row.Field<string>("ExpenditureItem4"),
                            ExpenditureItem5 = row.Field<string>("ExpenditureItem5"),
                            ExpenditureItem6 = row.Field<string>("ExpenditureItem6"),
                            EICost4 = row.Field<string>("EICost4"),
                            EICost5 = row.Field<string>("EICost5"),
                            EICost6 = row.Field<string>("EICost6"),
                            EECost4 = row.Field<string>("EECost4"),
                            EECost5 = row.Field<string>("EECost5"),
                            EECost6 = row.Field<string>("EECost6"),
                            InternalCost4 = row.Field<string>("InternalCost4"),
                            InternalCost5 = row.Field<string>("InternalCost5"),
                            InternalCost6 = row.Field<string>("InternalCost6"),
                            //EstimatedIITAmount = row.Field<string>("EstimatedIITAmount"),
                            //EstimatedIITAmountOccasional = row.Field<string>("EstimatedIITAmountOccasional"),
                            //EstimatedIITAmountLabor = row.Field<string>("EstimatedIITAmountLabor"),
                            //EstimatedOutputAmount = row.Field<string>("EstimatedOutputAmount"),
                            //EstimatedInputAmount = row.Field<string>("EstimatedInputAmount"),
                            //Note = row.Field<string>("Note"),
                            StatusIndex = row.Field<string>("StatusIndex"),
                            //_x0054_ax2 = row.Field<string>("_x0054_ax2"),
                            //Controlling2 = row.Field<string>("Controlling2"),
                            //NewComment = row.Field<string>("NewComment"),
                            //TeamGroup = row.Field<string>("TeamGroup"),
                            //LegalEntity = row.Field<string>("LegalEntity"),
                            //FormBCount = row.Field<string>("FormBCount"),
                            //IsStatusChange = row.Field<bool>("IsStatusChange"),
                            //FormA = row.Field<string>("FormA"),
                            //CalculationMethod = row.Field<string>("CalculationMethod"),
                            //VATRate = row.Field<string>("VATRate"),
                            //CurrentUser = row.Field<string>("CurrentUser"),
                            //FormAComments = row.Field<string>("FormAComments"),
                            //Year = row.Field<string>("Year"),
                            //IDIndex = row.Field<string>("IDIndex"),
                            //IsReject = row.Field<bool>("IsReject"),
                            //AddFormB = row.Field<string>("AddFormB"),
                            //Pending = row.Field<string>("Pending"),
                            //Pending2 = row.Field<string>("Pending2"),
                            //CostCenterCode = row.Field<string>("CostCenterCode"),
                            //FormBAmount = row.Field<string>("FormBAmount"),
                            //SurplusBudget = Convert.ToString(row.Field<decimal>("SurplusBudget")),
                            //EICost1_Submit = row.Field<string>("EICost1_Submit"),
                            //EICost2_Submit = row.Field<string>("EICost2_Submit"),
                            //EICost3_Submit = row.Field<string>("EICost3_Submit"),
                            //EICost4_Submit = row.Field<string>("EICost4_Submit"),
                            //EICost5_Submit = row.Field<string>("EICost5_Submit"),
                            //EICost6_Submit = row.Field<string>("EICost6_Submit"),
                            //EECost1_Submit = row.Field<string>("EECost1_Submit"),
                            //EECost2_Submit = row.Field<string>("EECost2_Submit"),
                            //EECost3_Submit = row.Field<string>("EECost3_Submit"),
                            //EECost4_Submit = row.Field<string>("EECost4_Submit"),
                            //EECost5_Submit = row.Field<string>("EECost5_Submit"),
                            //EECost6_Submit = row.Field<string>("EECost6_Submit"),
                            //InternalCost1_Submit = row.Field<string>("InternalCost1_Submit"),
                            //InternalCost2_Submit = row.Field<string>("InternalCost2_Submit"),
                            //InternalCost3_Submit = row.Field<string>("InternalCost3_Submit"),
                            //InternalCost4_Submit = row.Field<string>("InternalCost4_Submit"),
                            //InternalCost5_Submit = row.Field<string>("InternalCost5_Submit"),
                            //InternalCost6_Submit = row.Field<string>("InternalCost6_Submit"),
                            Modified = row.Field<DateTime>("Modified"),
                            Created = row.Field<DateTime>("Created"),
                        };
                    }
                }
            }
            return result;
        }



    }
}
