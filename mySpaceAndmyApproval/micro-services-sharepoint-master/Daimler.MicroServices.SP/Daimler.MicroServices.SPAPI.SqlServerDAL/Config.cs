﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daimler.MicroServices.SPAPI.SqlServerDAL {
    public class Config {
        public static string DefaultConnectionString {
            get {
                return ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            }
        }

        public class Procedures {
            public static class cp_sp_saveOfficeStationaryRequests {
                public static readonly string name = "cp_sp_saveOfficeStationaryRequests";
                public static readonly string @infoes = "@infoes";
            }
            public static class cp_sp_updateOfficeStationaryRequests {
                public static readonly string name = "cp_sp_updateOfficeStationaryRequests";
                public static readonly string @infoes = "@infoes";
            }
            public static class cp_sp_saveOSLocationAdminMapping {
                public static readonly string name = "cp_sp_saveOSLocationAdminMapping";
                public static readonly string @infoes = "@infoes";

            }
            public static class cp_sp_saveOfficeStationaryMenu {
                public static readonly string name = "cp_sp_saveOfficeStationaryMenu";
                public static readonly string @infoes = "@infoes";
            }
            public static class cp_sp_saveCommonUser {
                public static readonly string name = "cp_sp_saveCommonUser";
                public static readonly string @infoes = "@infoes";
            }
            public static class cp_sp_saveFormABUser {
                public static readonly string name = "cp_sp_saveFormABUser";
                public static readonly string @infoes = "@infoes";
            }
            public static class cp_sp_saveBadgeUser {
                public static readonly string name = "cp_sp_saveBadgeUser";
                public static readonly string @infoes = "@infoes";
            }
            public static class cp_sp_getPendingStationaryCount {
                public static readonly string name = "cp_sp_getPendingStationaryCount";
                public static readonly string @userId = "@userId";
            }
            public static class cp_sp_getPendingStationarySnapshot {
                public static readonly string name = "cp_sp_getPendingStationarySnapshot";
                public static readonly string @userId = "@userId";
                public static readonly string @pageIndex = "@pageIndex";
                public static readonly string @pageSize = "@pageSize";
            }
            public static class cp_sp_getPendingStationarySnapshotV2 {
                public static readonly string name = "cp_sp_getPendingStationarySnapshotV2";
                public static readonly string @userId = "@userId";
                public static readonly string @pageIndex = "@pageIndex";
                public static readonly string @pageSize = "@pageSize";
            }
            public static class cp_sp_getStationaryDetail {
                public static readonly string name = "cp_sp_getStationaryDetail";
                public static readonly string @ID = "@ID";
            }
            public static class cp_sp_getStationaryMenus {
                public static readonly string name = "cp_sp_getStationaryMenus";
                public static readonly string @ids = "@ids";
            }

            public static class cp_sp_saveWorkflowTasks {
                public static readonly string name = "cp_sp_saveWorkflowTasks";
                public static readonly string @infoes = "@infoes";
            }
            public static class cp_sp_updateWorkflowTasks {
                public static readonly string name = "cp_sp_updateWorkflowTasks";
                public static readonly string @infoes = "@infoes";
            }

            public static class cp_sp_getPendingBCCount {
                public static readonly string name = "cp_sp_getPendingBCCount";
                public static readonly string @userId = "@userId";
            }
            public static class cp_sp_getPendingILCount {
                public static readonly string name = "cp_sp_getPendingILCount";
                public static readonly string @userId = "@userId";
            }
            #region BC
            public static class cp_sp_saveBusinessCardApplication {
                public static readonly string name = "cp_sp_saveBusinessCardApplication";
                public static readonly string @infoes = "@infoes";
            }
            public static class cp_sp_updateBusinessCardApplication {
                public static readonly string name = "cp_sp_updateBusinessCardApplication";
                public static readonly string @infoes = "@infoes";
            }
            public static class cp_sp_getPendingBusinessCard {
                public static readonly string name = "cp_sp_getPendingBusinessCard";
                public static readonly string @userId = "@userId";
                public static readonly string @pageIndex = "@pageIndex";
                public static readonly string @pageSize = "@pageSize";
            }
            public static class cp_sp_getPendingBusinessCardV2 {
                public static readonly string name = "cp_sp_getPendingBusinessCardV2";
                public static readonly string @userId = "@userId";
                public static readonly string @pageIndex = "@pageIndex";
                public static readonly string @pageSize = "@pageSize";
            }
            public static class cp_sp_getPendingBusinessCardDetail {
                public static readonly string name = "cp_sp_getPendingBusinessCardDetail";
                public static readonly string @ID = "@ID";
            }
            public static class cp_sp_getPendingBusinessCardDetailV2 {
                public static readonly string name = "cp_sp_getPendingBusinessCardDetailV2";
                public static readonly string @ID = "@ID";
            }
            #endregion
            public static class cp_sp_getPendingWorkflowTasksByItemId {
                public static readonly string name = "cp_sp_getPendingWorkflowTasksByItemId";
                public static readonly string @userId = "@userId";
                public static readonly string @itemId = "@itemId";
                public static readonly string @workflowName = "@workflowName";
            }

            #region IL
            public static class cp_sp_saveeLOIApplication {
                public static readonly string name = "cp_sp_saveeLOIApplication";
                public static readonly string @infoes = "@infoes";
            }
            public static class cp_sp_updateeLOIApplication {
                public static readonly string name = "cp_sp_updateeLOIApplication";
                public static readonly string @infoes = "@infoes";
            }
            public static class cp_sp_geteLOIApplication {
                public static readonly string name = "cp_sp_geteLOIApplication";
                public static readonly string @userId = "@userId";
                public static readonly string @pageIndex = "@pageIndex";
                public static readonly string @pageSize = "@pageSize";
            }
            public static class cp_sp_geteLOIApplicationV2 {
                public static readonly string name = "cp_sp_geteLOIApplicationV2";
                public static readonly string @userId = "@userId";
                public static readonly string @pageIndex = "@pageIndex";
                public static readonly string @pageSize = "@pageSize";
            }
            public static class cp_sp_geteLOIApplicationDetail {
                public static readonly string name = "cp_sp_geteLOIApplicationDetail";
                public static readonly string @ID = "@ID";
            }
            public static class cp_sp_geteLOIApplicationDetailV2 {
                public static readonly string name = "cp_sp_geteLOIApplicationDetailV2";
                public static readonly string @ID = "@ID";
            }
            #endregion

            #region PC
            public static class cp_sp_savePetrolCard_CreditAdjustment {
                public static readonly string name = "cp_sp_savePetrolCard_CreditAdjustment";
                public static readonly string @infoes = "@infoes";
            }
            public static class cp_sp_updatePetrolCard_CreditAdjustment {
                public static readonly string name = "cp_sp_updatePetrolCard_CreditAdjustment";
                public static readonly string @infoes = "@infoes";
            }
            public static class cp_sp_getPendingPCCACount {
                public static readonly string name = "cp_sp_getPendingPCCACount";
                public static readonly string @supervisor = "@supervisor";
            }
            public static class cp_sp_getPetrolCard_CreditAdjustment {
                public static readonly string name = "cp_sp_getPetrolCard_CreditAdjustment";
                public static readonly string @supervisor = "@supervisor";
                public static readonly string @pageIndex = "@pageIndex";
                public static readonly string @pageSize = "@pageSize";
            }
            public static class cp_sp_getPetrolCard_CreditAdjustmentV2 {
                public static readonly string name = "cp_sp_getPetrolCard_CreditAdjustmentV2";
                public static readonly string @supervisor = "@supervisor";
                public static readonly string @pageIndex = "@pageIndex";
                public static readonly string @pageSize = "@pageSize";
            }
            public static class cp_sp_getPetrolCard_CreditAdjustmentDetail {
                public static readonly string name = "cp_sp_getPetrolCard_CreditAdjustmentDetail";
                public static readonly string @ID = "@ID";
            }
            public static class cp_sp_getPetrolCard_CreditAdjustmentDetailV2 {
                public static readonly string name = "cp_sp_getPetrolCard_CreditAdjustmentDetailV2";
                public static readonly string @ID = "@ID";
            }
            #endregion

            #region FormA
            public static class cp_sp_saveFormA {
                public static readonly string name = "cp_sp_saveFormA";
                public static readonly string @infoes = "@infoes";
            }
            public static class cp_sp_updateFormA {
                public static readonly string name = "cp_sp_updateFormA";
                public static readonly string @infoes = "@infoes";
            }
            public static class cp_sp_getPendingFormACount {
                public static readonly string name = "cp_sp_getPendingFormACount";
                public static readonly string @userId = "@userId";
            }
            public static class cp_sp_saveFormACommentHistory {
                public static readonly string name = "cp_sp_saveFormACommentHistory";
                public static readonly string @infoes = "@infoes";
            }
            public static class cp_sp_getFormA {
                public static readonly string name = "cp_sp_getFormA";
                public static readonly string @userId = "@userId";
                public static readonly string @pageIndex = "@pageIndex";
                public static readonly string @pageSize = "@pageSize";
            }
            public static class cp_sp_getFormAV2 {
                public static readonly string name = "cp_sp_getFormAV2";
                public static readonly string @userId = "@userId";
                public static readonly string @pageIndex = "@pageIndex";
                public static readonly string @pageSize = "@pageSize";
            }
            public static class cp_sp_getFormADetail {
                public static readonly string name = "cp_sp_getFormADetail";
                public static readonly string @ID = "@ID";
            }
            public static class cp_sp_getFormADetailV2 {
                public static readonly string name = "cp_sp_getFormADetailV2";
                public static readonly string @ID = "@ID";
            }
            public static class cp_sp_getFormACommentHistory {
                public static readonly string name = "cp_sp_getFormACommentHistory";
                public static readonly string @caseId = "@caseId";
            }
            #endregion

            #region FormB
            public static class cp_sp_saveFormB {
                public static readonly string name = "cp_sp_saveFormB";
                public static readonly string @infoes = "@infoes";
            }
            public static class cp_sp_updateFormB {
                public static readonly string name = "cp_sp_updateFormB";
                public static readonly string @infoes = "@infoes";
            }
            public static class cp_sp_getPendingFormBCount {
                public static readonly string name = "cp_sp_getPendingFormBCount";
                public static readonly string @userId = "@userId";
            }
            public static class cp_sp_saveFormBCommentHistory {
                public static readonly string name = "cp_sp_saveFormBCommentHistory";
                public static readonly string @infoes = "@infoes";
            }
            public static class cp_sp_getFormB {
                public static readonly string name = "cp_sp_getFormB";
                public static readonly string @userId = "@userId";
                public static readonly string @pageIndex = "@pageIndex";
                public static readonly string @pageSize = "@pageSize";
            }
            public static class cp_sp_getFormBV2 {
                public static readonly string name = "cp_sp_getFormBV2";
                public static readonly string @userId = "@userId";
                public static readonly string @pageIndex = "@pageIndex";
                public static readonly string @pageSize = "@pageSize";
            }
            public static class cp_sp_getFormBDetail {
                public static readonly string name = "cp_sp_getFormBDetail";
                public static readonly string @ID = "@ID";
            }
            public static class cp_sp_getFormBDetailV2 {
                public static readonly string name = "cp_sp_getFormBDetailV2";
                public static readonly string @ID = "@ID";
            }
            public static class cp_sp_getFormBCommentHistory {
                public static readonly string name = "cp_sp_getFormBCommentHistory";
                public static readonly string @caseId = "@caseId";
            }
            #endregion

            #region Badge
            public static class cp_sp_saveCCDeputy {
                public static readonly string name = "cp_sp_saveCCDeputy";
                public static readonly string @infoes = "@infoes";
            }

            public static class cp_sp_getCCDeputys {
                public static readonly string name = "cp_sp_getCCDeputys";
                public static readonly string @deputyId = "@deputyId";
            }
            public static class cp_sp_saveSite {
                public static readonly string name = "cp_sp_saveSite";
                public static readonly string @infoes = "@infoes";
            }
            public static class cp_sp_saveLocation {
                public static readonly string name = "cp_sp_saveLocation";
                public static readonly string @infoes = "@infoes";
            }
            public static class cp_sp_saveArea {
                public static readonly string name = "cp_sp_saveArea";
                public static readonly string @infoes = "@infoes";
            }
            #endregion
            #region EIBM
            public static class cp_sp_saveEIBM {
                public static readonly string name = "cp_sp_saveEIBM";
                public static readonly string @infoes = "@infoes";
            }
            public static class cp_sp_getPendingEIBMCount {
                public static readonly string name = "cp_sp_getPendingEIBMCount";
                public static readonly string @userId = "@userId";
            }
            public static class cp_sp_getPendingEIBM {
                public static readonly string name = "cp_sp_getPendingEIBM";
                public static readonly string @userId = "@userId";
            }
            public static class cp_sp_getEIBMDetail {
                public static readonly string name = "cp_sp_getEIBMDetail";
                public static readonly string @ID = "@ID";
            }
            public static class cp_sp_updateEIBMs {
                public static readonly string name = "cp_sp_updateEIBMs";
                public static readonly string @infoes = "@infoes";
            }
            #endregion

            #region Visitor Badge
            public static class cp_sp_saveVisitorBadge {
                public static readonly string name = "cp_sp_saveVisitorBadge";
                public static readonly string @infoes = "@infoes";
            }
            public static class cp_sp_getPendingVisitorBadgeCount {
                public static readonly string name = "cp_sp_getPendingVisitorBadgeCount";
                public static readonly string @userId = "@userId";
            }
            public static class cp_sp_getPendingVisitorBadge {
                public static readonly string name = "cp_sp_getPendingVisitorBadge";
                public static readonly string @userId = "@userId";
            }
            public static class cp_sp_getVisitorBadgeDetail {
                public static readonly string name = "cp_sp_getVisitorBadgeDetail";
                public static readonly string @ID = "@ID";
            }
            public static class cp_sp_updateVisitorBadges {
                public static readonly string name = "cp_sp_updateVisitorBadges";
                public static readonly string @infoes = "@infoes";
            }
            #endregion

        }
    }
}
