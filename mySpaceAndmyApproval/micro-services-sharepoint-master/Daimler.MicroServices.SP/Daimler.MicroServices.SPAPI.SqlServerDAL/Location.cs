﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.AccessManagement;
using Daimler.MicroServices.SPAPI.IDAL;
using Daimler.MicroServices.Utils.Data;

namespace Daimler.MicroServices.SPAPI.SqlServerDAL {
    public class Location : ILocation {
        DBHelper dbHelper = new DBHelper();
        bool ILocation.SaveInfoes(List<LocationInfo> infoes) {
            DataTable data = DataHelper.CreateDataTable<LocationInfo>(infoes);
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_saveLocation.infoes,data)
            };
            int result = dbHelper.Execute(Config.Procedures.cp_sp_saveLocation.name, sqlParameters, CommandType.StoredProcedure);
            if (result == 0) {
                return false;
            }
            return true;
        }
    }
}
