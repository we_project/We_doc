﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.PetrolCard;
using Daimler.MicroServices.SPAPI.IDAL;
using Daimler.MicroServices.Utils.Data;

namespace Daimler.MicroServices.SPAPI.SqlServerDAL {
    public class PetrolCard_CreditAdjustment : IPetrolCard_CreditAdjustment {
        DBHelper dbHelper = new DBHelper();
        bool IPetrolCard_CreditAdjustment.SaveInfoes(List<PCCreditAdjustmentInfo> infoes) {
            DataTable data = DataHelper.CreateDataTable<PCCreditAdjustmentInfo>(infoes);
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_savePetrolCard_CreditAdjustment.infoes,data)
            };
            int result = dbHelper.Execute(Config.Procedures.cp_sp_savePetrolCard_CreditAdjustment.name, sqlParameters, CommandType.StoredProcedure);
            if (result == 0) {
                return false;
            }
            return true;
        }

        bool IPetrolCard_CreditAdjustment.UpdateRequests(List<PCCreditAdjustmentInfo> infoes) {
            DataTable data = DataHelper.CreateDataTable<PCCreditAdjustmentInfo>(infoes);
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_updatePetrolCard_CreditAdjustment.infoes,data)
            };
            int result = dbHelper.Execute(Config.Procedures.cp_sp_updatePetrolCard_CreditAdjustment.name, sqlParameters, CommandType.StoredProcedure);
            if (result == 0) {
                return false;
            }
            return true;
        }

        List<PCCreditAdjustmentItem> IPetrolCard_CreditAdjustment.GetPCCAApplicatoinList(string supervisor, int? pageIndex, int? pageSize) {
            List<PCCreditAdjustmentItem> result = null;
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_getPetrolCard_CreditAdjustmentV2.supervisor,supervisor),
                new SqlParameter(Config.Procedures.cp_sp_getPetrolCard_CreditAdjustmentV2.pageIndex,pageIndex),
                new SqlParameter(Config.Procedures.cp_sp_getPetrolCard_CreditAdjustmentV2.pageSize,pageSize),
            };
            DataSet requests = dbHelper.ExcuteQuery(Config.Procedures.cp_sp_getPetrolCard_CreditAdjustmentV2.name, sqlParameters, CommandType.StoredProcedure);
            if (requests != null && requests.Tables != null && requests.Tables.Count > 0) {
                IEnumerable<DataRow> rows = requests.Tables[0].AsEnumerable();
                if (rows != null && rows.Count() > 0) {
                    result = rows.Select(row => new PCCreditAdjustmentItem {
                        ID = row.Field<int>("ID"),
                        //Title = row.Field<string>("Title"),
                        UserName = row.Field<string>("UserName"),
                        //EmployeeNumber = row.Field<string>("EmployeeNumber"),
                        //CompanyName = row.Field<string>("CompanyName"),
                        CostCenter = row.Field<string>("CostCenter"),
                        RequestAmount = Convert.ToDouble(row.Field<decimal?>("RequestAmount")),
                        //AdjustmentReason = row.Field<string>("AdjustmentReason"),
                        //ExtensionNumber = row.Field<string>("ExtensionNumber"),
                        //PlateNumber = row.Field<string>("PlateNumber"),
                        //CurrentCredit = Convert.ToDouble(row.Field<decimal?>("CurrentCredit")),
                        //Status = row.Field<string>("Status"),
                        //Comments = row.Field<string>("Comments"),
                        //Supervisor = row.Field<string>("Supervisor"),
                        //CardNo = row.Field<string>("CardNo"),
                        //DisplayName = row.Field<string>("DisplayName"),
                        //PetrolCa = Convert.ToString(row.Field<int?>("PetrolCa")),
                        Modified = row.Field<DateTime>("Modified"),
                        Created = row.Field<DateTime>("Created"),
                    }).ToList();
                }
            }
            return result;
        }

        PCCreditAdjustmentDetail IPetrolCard_CreditAdjustment.GetPCCAApplicatoinDetail(int ID) {
            PCCreditAdjustmentDetail result = null;
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_getPetrolCard_CreditAdjustmentDetailV2.ID,ID),
            };
            DataSet requests = dbHelper.ExcuteQuery(Config.Procedures.cp_sp_getPetrolCard_CreditAdjustmentDetailV2.name, sqlParameters, CommandType.StoredProcedure);
            if (requests != null && requests.Tables != null && requests.Tables.Count > 0) {
                IEnumerable<DataRow> rows = requests.Tables[0].AsEnumerable();
                if (rows != null && rows.Count() > 0) {
                    DataRow row = rows.FirstOrDefault();
                    if (row != null) {
                        result = new PCCreditAdjustmentDetail {
                            ID = row.Field<int>("ID"),
                            //Title = row.Field<string>("Title"),
                            UserName = row.Field<string>("UserName"),
                            EmployeeNumber = row.Field<string>("EmployeeNumber"),
                            CompanyName = row.Field<string>("CompanyName"),
                            CostCenter = row.Field<string>("CostCenter"),
                            RequestAmount = Convert.ToDouble(row.Field<decimal?>("RequestAmount")),
                            AdjustmentReason = row.Field<string>("AdjustmentReason"),
                            ExtensionNumber = row.Field<string>("ExtensionNumber"),
                            PlateNumber = row.Field<string>("PlateNumber"),
                            CurrentCredit = Convert.ToDouble(row.Field<decimal?>("CurrentCredit")),
                            Status = row.Field<string>("Status"),
                            //Comments = row.Field<string>("Comments"),
                            //Supervisor = row.Field<string>("Supervisor"),
                            //CardNo = row.Field<string>("CardNo"),
                            //DisplayName = row.Field<string>("DisplayName"),
                            //PetrolCa = Convert.ToString(row.Field<int?>("PetrolCa")),
                            Modified = row.Field<DateTime>("Modified"),
                            Created = row.Field<DateTime>("Created"),
                        };
                    }
                }
            }
            return result;
        }


        int IPetrolCard_CreditAdjustment.GetPendingCount(string supervisor) {
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_getPendingPCCACount.supervisor,supervisor)
            };
            object result = dbHelper.ExcuteMyScalar(Config.Procedures.cp_sp_getPendingPCCACount.name, sqlParameters, CommandType.StoredProcedure);
            if (result != null) {
                return Convert.ToInt32(result);
            }
            return 0;
        }
    }
}
