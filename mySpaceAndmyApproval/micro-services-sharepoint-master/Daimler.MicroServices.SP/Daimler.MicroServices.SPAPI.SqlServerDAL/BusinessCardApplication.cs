﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.BusinessCard;
using Daimler.MicroServices.SPAPI.IDAL;
using Daimler.MicroServices.Utils.Data;
using Newtonsoft.Json;

namespace Daimler.MicroServices.SPAPI.SqlServerDAL {
    public class BusinessCardApplication : IBusinessCardApplication {
        DBHelper dbHelper = new DBHelper();
        bool IBusinessCardApplication.SaveInfoes(List<BCApplicationInfo> infoes) {
            DataTable data = DataHelper.CreateDataTable<BCApplicationInfo>(infoes);
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_saveBusinessCardApplication.infoes,data)
            };
            int result = dbHelper.Execute(Config.Procedures.cp_sp_saveBusinessCardApplication.name, sqlParameters, CommandType.StoredProcedure);
            if (result == 0) {
                return false;
            }
            return true;
        }

        bool IBusinessCardApplication.UpdateRequests(List<BCApplicationInfo> infoes) {
            DataTable data = DataHelper.CreateDataTable<BCApplicationInfo>(infoes);
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_updateBusinessCardApplication.infoes,data)
            };
            int result = dbHelper.Execute(Config.Procedures.cp_sp_updateBusinessCardApplication.name, sqlParameters, CommandType.StoredProcedure);
            if (result == 0) {
                return false;
            }
            return true;
        }

        List<SP.BizObject.BusinessCard.BusinessCardApplicationItem> IBusinessCardApplication.GetBCApplicatoinList(int userId, int? pageIndex, int? pageSize) {
            List<BusinessCardApplicationItem> result = null;
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_getPendingBusinessCardV2.userId,userId),
                new SqlParameter(Config.Procedures.cp_sp_getPendingBusinessCardV2.pageIndex,pageIndex),
                new SqlParameter(Config.Procedures.cp_sp_getPendingBusinessCardV2.pageSize,pageSize),
            };
            DataSet requests = dbHelper.ExcuteQuery(Config.Procedures.cp_sp_getPendingBusinessCardV2.name, sqlParameters, CommandType.StoredProcedure);
            if (requests != null && requests.Tables != null && requests.Tables.Count > 0) {
                IEnumerable<DataRow> rows = requests.Tables[0].AsEnumerable();
                if (rows != null && rows.Count() > 0) {
                    result = rows.Select(row => new BusinessCardApplicationItem {
                        ID = row.Field<int>("ID"),
                        //Title = row.Field<string>("Title"),
                        UserName = row.Field<string>("UserName"),
                        //EmployeeNumber = row.Field<string>("EmployeeNumber"),
                        //CompanyName = row.Field<string>("CompanyName"),
                        //CostCenter = row.Field<string>("CostCenter"),
                        //ExtensionNumber = row.Field<string>("ExtensionNumber"),
                        //FirstName = row.Field<string>("FirstName"),
                        //LastName = row.Field<string>("LastName"),
                        DepartmentName = row.Field<string>("DepartmentName"),
                        //PhoneNo = row.Field<string>("PhoneNo"),
                        //Email = row.Field<string>("Email"),
                        //Position = row.Field<string>("Position"),
                        //FixNo = row.Field<string>("FixNo"),
                        //Address = row.Field<string>("Address"),
                        Modified = row.Field<DateTime>("Modified"),
                        Created = row.Field<DateTime>("Created"),
                        //Business = row.Field<string>("Business"),
                        //BusinessCardType = row.Field<string>("BusinessCardType"),
                        //FirstNameCN = row.Field<string>("FirstNameCN"),
                        //SecondNameCN = row.Field<string>("SecondNameCN"),
                        //DepartmentCN = row.Field<string>("DepartmentCN"),
                        //PositionCN = row.Field<string>("PositionCN"),
                        //AddressCN = row.Field<string>("AddressCN"),
                        //RequestorLoginName = row.Field<string>("RequestorLoginName"),
                        //Supervisor = row.Field<string>("Supervisor"),
                        State = row.Field<string>("State"),
                        //Location = row.Field<string>("Location"),
                        //EmployeeType = row.Field<string>("EmployeeType"),
                        //HRKA = row.Field<string>("HRKA"),
                        //cWorkflowHistory = row.Field<string>("cWorkflowHistory"),
                        //AdminContact = row.Field<string>("AdminContact"),
                        //Overprint = row.Field<string>("Overprint"),
                        //Business0 = row.Field<string>("Business0"),
                        //Remark = row.Field<string>("Remark"),
                    }).ToList();
                }
            }
            return result;
        }

        BusinessCardApplicationDetail IBusinessCardApplication.GetBCApplicatoinDetail(int ID) {
            BusinessCardApplicationDetail result = null;
            SqlParameter[] sqlParameters ={
                new SqlParameter(Config.Procedures.cp_sp_getPendingBusinessCardDetailV2.ID,ID),
            };
            DataSet requests = dbHelper.ExcuteQuery(Config.Procedures.cp_sp_getPendingBusinessCardDetailV2.name, sqlParameters, CommandType.StoredProcedure);
            if (requests != null && requests.Tables != null && requests.Tables.Count > 0) {
                IEnumerable<DataRow> rows = requests.Tables[0].AsEnumerable();
                if (rows != null && rows.Count() > 0) {
                    DataRow row = rows.FirstOrDefault();
                    if (row != null) {
                        result = new BusinessCardApplicationDetail {
                            ID = row.Field<int>("ID"),
                            //Title = row.Field<string>("Title"),
                            UserName = row.Field<string>("UserName"),
                            EmployeeNumber = row.Field<string>("EmployeeNumber"),
                            CompanyName = row.Field<string>("CompanyName"),
                            CostCenter = row.Field<string>("CostCenter"),
                            ExtensionNumber = row.Field<string>("ExtensionNumber"),
                            FirstName = row.Field<string>("FirstName"),
                            LastName = row.Field<string>("LastName"),
                            DepartmentName = row.Field<string>("DepartmentName"),
                            PhoneNo = row.Field<string>("PhoneNo"),
                            Email = row.Field<string>("Email"),
                            Position = row.Field<string>("Position"),
                            FixNo = row.Field<string>("FixNo"),
                            Address = row.Field<string>("Address"),
                            Modified = row.Field<DateTime>("Modified"),
                            Created = row.Field<DateTime>("Created"),
                            //Business = row.Field<string>("Business"),
                            BusinessCardType = row.Field<string>("BusinessCardType"),
                            FirstNameCN = row.Field<string>("FirstNameCN"),
                            SecondNameCN = row.Field<string>("SecondNameCN"),
                            DepartmentCN = row.Field<string>("DepartmentCN"),
                            PositionCN = row.Field<string>("PositionCN"),
                            AddressCN = row.Field<string>("AddressCN"),
                            //RequestorLoginName = row.Field<string>("RequestorLoginName"),
                            //Supervisor = row.Field<string>("Supervisor"),
                            State = row.Field<string>("State"),
                            Location = row.Field<string>("Location"),
                            EmployeeType = row.Field<string>("EmployeeType"),
                            HRKA = row.Field<string>("HRKA"),
                            //cWorkflowHistory = row.Field<string>("cWorkflowHistory"),
                            //AdminContact = row.Field<string>("AdminContact"),
                            //Overprint = row.Field<string>("Overprint"),
                            //Business0 = row.Field<string>("Business0"),
                        };
                    }
                }
            }
            return result;
        }
    }
}
