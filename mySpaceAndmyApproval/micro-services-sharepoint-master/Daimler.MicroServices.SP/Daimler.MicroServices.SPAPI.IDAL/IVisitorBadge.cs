﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.AccessManagement;

namespace Daimler.MicroServices.SPAPI.IDAL {
    public interface IVisitorBadge {
        bool SaveInfoes(List<VistorBadgeApplicationInfo> infoes);
        int GetPendingCount(int userId);
        List<VisitorBadgeItem> GetPendingList(int userId);

        VisitorBadgeDetail1 GetDetail(int id);

        bool UpdateRequests(List<VistorBadgeApplicationInfo> infoes);
    }
}
