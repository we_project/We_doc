﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.InvitationLetter;

namespace Daimler.MicroServices.SPAPI.IDAL {
    public interface IeLOIApplication {
        bool SaveInfoes(List<eLOIApplicationInfo> infoes);
        bool UpdateRequests(List<eLOIApplicationInfo> infoes);
        List<InvitationLetterApplicationItem> GetILApplicatoinList(int userId, int? pageIndex, int? pageSize);
        InvitationLetterApplicationDetail GetILApplicatoinDetail(int ID);
    }
}
