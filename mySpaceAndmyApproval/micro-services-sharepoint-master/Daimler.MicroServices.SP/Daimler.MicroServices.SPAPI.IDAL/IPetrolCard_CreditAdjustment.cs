﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.PetrolCard;

namespace Daimler.MicroServices.SPAPI.IDAL {
   public interface IPetrolCard_CreditAdjustment {
        bool SaveInfoes(List<PCCreditAdjustmentInfo> infoes);
        bool UpdateRequests(List<PCCreditAdjustmentInfo> infoes);
        int GetPendingCount(string supervisor);
        List<PCCreditAdjustmentItem> GetPCCAApplicatoinList(string supervisor, int? pageIndex, int? pageSize);
        PCCreditAdjustmentDetail GetPCCAApplicatoinDetail(int ID);
    }
}
