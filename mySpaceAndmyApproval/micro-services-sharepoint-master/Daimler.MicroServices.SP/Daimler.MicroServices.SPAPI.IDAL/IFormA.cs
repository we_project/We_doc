﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.FormAB;

namespace Daimler.MicroServices.SPAPI.IDAL {
    public interface IFormA {
        bool SaveInfoes(List<FormAInfo> infoes);
        bool UpdateRequests(List<FormAInfo> infoes);
        int GetPendingCount(int userId);
        List<FormAItem> GetList(int userId, int? pageIndex, int? pageSize);
        FormADetail GetDetail(int ID);
    }
}
