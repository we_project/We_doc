﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.OfficeStationary;

namespace Daimler.MicroServices.SPAPI.IDAL {
    public interface IOfficeStationaryRequests {
        bool SaveInfoes(List<StationaryRequestInfo> infoes);
        bool UpdateRequests(List<StationaryRequestInfo> infoes);
        int GetPendingStationaryCount(int userId);

        List<StationaryRequestItem> GetPendingStationaryList(int userId, int? pageIndex, int? pageSize);
        APIStationaryRequestDetail GetStationaryDetail(int ID);
    }
}
