﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.BusinessCard;

namespace Daimler.MicroServices.SPAPI.IDAL {
    public interface IBusinessCardApplication {
        bool SaveInfoes(List<BCApplicationInfo> infoes);
        bool UpdateRequests(List<BCApplicationInfo> infoes);
        List<BusinessCardApplicationItem> GetBCApplicatoinList(int userId, int? pageIndex, int? pageSize);
        BusinessCardApplicationDetail GetBCApplicatoinDetail(int ID);
    }
}
