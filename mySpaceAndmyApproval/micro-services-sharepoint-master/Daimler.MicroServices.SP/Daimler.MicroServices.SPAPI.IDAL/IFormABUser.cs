﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject;

namespace Daimler.MicroServices.SPAPI.IDAL {
    public interface IFormABUser {
        bool SaveInfoes(List<SPUserInfo> infoes);
        int? GetUserId(string accountName);
        string GetUserName(string accountName);
    }
}
