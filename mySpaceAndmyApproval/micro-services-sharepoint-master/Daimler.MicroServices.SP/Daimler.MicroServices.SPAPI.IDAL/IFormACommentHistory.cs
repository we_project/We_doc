﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.FormAB;

namespace Daimler.MicroServices.SPAPI.IDAL {
   public interface IFormACommentHistory {
        bool SaveInfoes(List<FormACommentHistoryInfo> infoes);
        List<FormAComment> GetCommentList(int caseId);
   }
}
