﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.FormAB;

namespace Daimler.MicroServices.SPAPI.IDAL {
    public interface IFormBCommentHistory {
        bool SaveInfoes(List<FormBCommentHistoryInfo> infoes);
        List<FormBComment> GetCommentList(int caseId);
    }
}
