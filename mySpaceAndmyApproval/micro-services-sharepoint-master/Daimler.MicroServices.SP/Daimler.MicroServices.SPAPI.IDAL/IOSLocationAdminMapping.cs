﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.OfficeStationary;

namespace Daimler.MicroServices.SPAPI.IDAL {
    public interface IOSLocationAdminMapping {
        bool SaveInfoes(List<OSLocationInfo> infoes);
    }
}
