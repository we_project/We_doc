﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.Workflow;

namespace Daimler.MicroServices.SPAPI.IDAL {
    public interface IWorkflowTasks {
        bool SaveInfoes(List<WorkflowTaskInfo> infoes);
        bool UpdateRequests(List<WorkflowTaskInfo> infoes);
        int GetPendingBCCount(int userId);
        int GetPendingILCount(int userId);
        int? GetPendingWorkflowTasksByItemId(int userId, int itemId, string workflowName);
        bool MarkApproved(int ID);
    }
}
