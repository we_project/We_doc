﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.AccessManagement;

namespace Daimler.MicroServices.SPAPI.IDAL {
    public interface IEIBM {
        bool SaveInfoes(List<EIBMInfo> infoes);
        int GetPendingCount(int userId);
        List<EIBMItem> GetPendingList(int userId);

        EIBMDetail1 GetDetail(int id);

        bool UpdateRequests(List<EIBMInfo> infoes);
    }
}
