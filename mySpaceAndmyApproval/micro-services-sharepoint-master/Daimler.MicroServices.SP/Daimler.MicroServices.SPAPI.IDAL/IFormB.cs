﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.FormAB;

namespace Daimler.MicroServices.SPAPI.IDAL {
    public interface IFormB {
        bool SaveInfoes(List<FormBInfo> infoes);
        bool UpdateRequests(List<FormBInfo> infoes);
        int GetPendingCount(int userId);
        List<FormBItem> GetList(int userId, int? pageIndex, int? pageSize);
        FormBDetail GetDetail(int ID);
    }
}
