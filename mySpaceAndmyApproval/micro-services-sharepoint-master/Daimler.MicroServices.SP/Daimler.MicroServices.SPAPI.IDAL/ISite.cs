﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daimler.MicroServices.SP.BizObject.AccessManagement;

namespace Daimler.MicroServices.SPAPI.IDAL {
   public interface ISite {
        bool SaveInfoes(List<SiteInfo> infoes);
    }
}
